<a name="TOC"></a> Previous Chapter: [0 Preface](00-Preface.md)

![TitleImage.png](Illustrations/01/TitleImage.png)

[[_TOC_]]

# 1 Introduction

---

## 1.1 Objective

This document explains some of the finer points about the design of the schema and data definitions of the OSDU™ Data
Platform. It is always possible to use a data model in ways other than the modelers intended. In a system with a goal of
interoperability, such variance can break the operation of the system. A clear understanding of intentions, as provided
in this document, will help prevent such breakage.

## 1.2 Audience

This document is intended for any users in a position to manipulate the design or content of data schema. This includes:

- Data modelers who are designing new schema or modifying existing OSDU designs
- Data loaders who are populating records and need to understand what kind of data is expected in document properties
- Application developers who write applications which consume data and need to predict how data will be delivered

It is assumed that the reader is familiar with JSON Schema syntax. Draft-071 is the version currently in use for OSDU
schema; see [1.4 Available Resources](#14-available-resources).

## 1.3 Overview

The OSDU data schema defines the shape of information that is stored in the OSDU Data Platform, aside from data files.
Due to the nature of the NoSQL cloud design of the platform, certain design choices were made in constructing the schema
that differ from traditional relational data modeling. Because of the flexibility of the NoSQL approach, it is possible
to interpret the schema in many ways, and it is very difficult to impose constraints that limit such interpretations.
Hence, in many cases, it is left to manual business processes to protect the integrity of the design. This document
should help to inform users of the platform of those who can provide that protection. It covers issues such as:

- How to use the facility vertical measurements structure for wellbore reference datums
- How to interpret the properties which describe various lifecycles
- The meaning of group type
- Understanding Seismic projects
- Options for recording provenance
- Properties which record a history of values
- How to use Frames of Reference (FoRs)

## 1.4 Available Resources

For further information and resources, refer to:
https://community.opengroup.org/osdu/data/data-definitions/-/tree/master.

---

## Links

1. Return to the [Beginning of the Chapter](#TOC)
2. Next Chapter: [2 Group Type](02-GroupType.md)
3. Browse to the [Schema Usage Guide Table of Contents](../README.md)