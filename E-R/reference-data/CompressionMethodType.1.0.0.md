<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# CompressionMethodType
A scheme for packing data into a smaller digital space.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--CompressionMethodType:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: None
* Reference value type, governance model: LOCAL
* Link to &rarr; [Authoring Schema CompressionMethodType.1.0.0.json](../../Authoring/reference-data/CompressionMethodType.1.0.0.json)
* Link to &rarr; [Generated Schema CompressionMethodType.1.0.0.json](../../Generated/reference-data/CompressionMethodType.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource CompressionMethodType.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/CompressionMethodType.1.0.0.json)
* Link to &rarr; [Example Record CompressionMethodType.1.0.0.json](../../Examples/reference-data/CompressionMethodType.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams CompressionMethodType


## Outgoing Relationships for CompressionMethodType

![CompressionMethodType](../_diagrams/reference-data/CompressionMethodType.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## CompressionMethodType Referenced by other Entities

![CompressionMethodType](../_diagrams/reference-data/CompressionMethodType.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# CompressionMethodType Properties

## 1. Table of CompressionMethodType System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-CompressionMethodType:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--CompressionMethodType:41723576-596c-54c3-aa5e-fb03421b4f2e|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--CompressionMethodType:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of CompressionMethodType Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of CompressionMethodType Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of CompressionMethodType Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for CompressionMethodType.1.0.0

* Status: Published
* Governance: LOCAL
* The table below shows all 29 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [CompressionMethodType](../../ReferenceValues/Manifests/reference-data/LOCAL/CompressionMethodType.1.0.0.json).

|`id` with prefix `namespace:reference-data--CompressionMethodType:`|Code|Name|Alias|Description|NameAlias|AttributionAuthority|AttributionPublication|Source|CommitDate|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|7z|7z|7z|-|7-Zip|-|Apache Commons|http://commons.apache.org/proper/commons-compress/|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Analogous%20Time-domain|Analogous Time-domain|Analogous Time-domain|-|Compression of the signal in the time domain|-|PPDM Members|https://library.seg.org/doi/10.1190/1.1440443|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Bit%20Resolution%20Compression|Bit Resolution Compression|Bit Resolution Compression|-|In digital audio using pulse-code modulation (PCM), bit depth is the number of bits of information in each sample, and it directly corresponds to the resolution of each sample.|-|Wikipedia|https://en.wikipedia.org/wiki/Audio_bit_depth|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|BitResolutionCompression.16bit|BitResolutionCompression.16bit|Bit Resolution Compression.16-bit|-|Bit resolution compression - 16 bit.|-|PPDM Members|http://help.dugeo.com/m/faq/l/652200-what-should-i-know-about-insight-s-16-bit-data-compression|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|BitResolutionCompression.32bitFloatingPoint|BitResolutionCompression.32bitFloatingPoint|Bit Resolution Compression.32-bit Floating Point|-|Bit resolution compression - 32 bit floating point.|-|PPDM Members|-|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|BitResolutionCompression.32bitInteger|BitResolutionCompression.32bitInteger|Bit Resolution Compression.32-bit Integer|-|Bit resolution compression - 32 bit integer.|-|PPDM Members|-|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|BitResolutionCompression.8bitFloatingPoint|BitResolutionCompression.8bitFloatingPoint|Bit Resolution Compression.8-bit Floating Point|-|Bit resolution compression - 8 bit floating point.|-|PPDM Members|-|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|BitResolutionCompression.8bit|BitResolutionCompression.8bit|Bit Resolution Compression.8-bit|-|Bit resolution compression - 8 bit.|-|PPDM Members|-|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|DeepNeuralNetworks|DeepNeuralNetworks|Deep Neural Networks|-|Various compression methods employing artifical neural networks (machine learning) applied to layers.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"DNN"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://onepetro.org/SEGAM/proceedings-abstract/SEG19/4-SEG19/D043S112R004/105628|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|GZip|GZip|GZip|-|Compression and decompression file format|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"GZIP"<br>&nbsp;&nbsp;}<br>]|Apache Commons|http://commons.apache.org/proper/commons-compress/|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Jar|Jar|Jar|-|Java compression archive|-|Apache Commons|http://commons.apache.org/proper/commons-compress/|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless.ARC|Lossless.ARC|ARC|-|ARC is a lossless data compression and archival format. The file format and the program were both called ARC.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"ARC"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://en.wikipedia.org/wiki/ARC_(file_format)|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless.BurrowsWheelerTransform|Lossless.BurrowsWheelerTransform|Burrows–Wheeler Transform|-|BWT rearranges a character string into runs of similar characters. The process is reversible.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"BWT"<br>&nbsp;&nbsp;},<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Block-sorting&nbsp;Compression"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://en.wikipedia.org/wiki/Burrows%E2%80%93Wheeler_transform|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless.BytePairEncoding|Lossless.BytePairEncoding|Byte-Pair Encoding|-|A simple form of data compression in which the most common pair of consecutive bytes of data is replaced with a byte that does not occur within that data.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Digram&nbsp;Coding"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://en.wikipedia.org/wiki/Byte_pair_encoding|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless.DataDifferencing|Lossless.DataDifferencing|Data Differencing|-|A data differencing algorithm takes as input source data and target data, and produces difference data such that given the source data and the difference data, one can reconstruct the target data|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Differential&nbsp;Compression"<br>&nbsp;&nbsp;}<br>]|Wikipedia|https://en.wikipedia.org/wiki/Data_differencing|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless.DictionaryLearning|Lossless.DictionaryLearning|Dictionary Learning|-|Dictionary learning has shown to provide representations with high level of sparsity. It stores the shape of the redundant events once, and represents each occurrence of these events with a single sparse coefficient.|-|PPDM Members|https://www.earthdoc.org/content/papers/10.3997/2214-4609.202034036|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless.LempelZiv|Lossless.LempelZiv|Lempel-Ziv|-|LZ77 and LZ78 are the two lossless data compression algorithms published in papers by Abraham Lempel and Jacob Ziv in 1977[1] and 1978...They are both theoretically dictionary coders.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"LZ"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://en.wikipedia.org/wiki/LZ77_and_LZ78|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless.LZ77|Lossless.LZ77|LZ77|-|A dictionary coder, LZ77 maintains a sliding window during compression.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Lempel-Ziv&nbsp;77"<br>&nbsp;&nbsp;},<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"LZ1"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://en.wikipedia.org/wiki/LZ77_and_LZ78|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless.LZ78|Lossless.LZ78|LZ78|-|Conceptually, LZ78 decompression could allow random access to the input if the entire dictionary were known in advance. However, in practice the dictionary is created during encoding and decoding by creating a new phrase whenever a token is output|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Lempel-Ziv&nbsp;78"<br>&nbsp;&nbsp;},<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"LZ2"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://en.wikipedia.org/wiki/LZ77_and_LZ78|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless.Zip|Lossless.Zip|Zip|-|Archive format that supports lossless data compression|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"ZIP"<br>&nbsp;&nbsp;}<br>]|Apache Commons|http://commons.apache.org/proper/commons-compress/|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossless|Lossless|Lossless|-|Lossless data compression algorithms use statistical data redundancy to represent data without losing any information, so that the process is reversible.|-|PPDM Members|-|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossy.AdvancedVideoCoding|Lossy.AdvancedVideoCoding|Advanced Video Coding|-|A video compression standard based on block-oriented, motion-compensated integer-DCT coding. It is by far the most commonly used format for the recording, compression, and distribution of video content.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"AVC"<br>&nbsp;&nbsp;},<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"H.264"<br>&nbsp;&nbsp;}<br>]|Wikipedia|https://en.wikipedia.org/wiki/Advanced_Video_Coding|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossy.DimensionalPatternMatching|Lossy.DimensionalPatternMatching|2-Dimensional Pattern Matching|-|A lossy data compression framework based on an approximate two-dimensional (2D) pattern matching (2D-PMC) extension of the Lempel-Ziv (1977, 1978) lossless scheme.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"2D&nbsp;PMC"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://pubmed.ncbi.nlm.nih.gov/18244634/|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossy.DiscreteCosineTransform|Lossy.DiscreteCosineTransform|Discrete Cosine Transform|-|DCT expresses a finite sequence of data points in terms of a sum of cosine functions oscillating at different frequencies. The DCT, first proposed by Nasir Ahmed in 1972, is a widely used transformation technique in signal processing and data compression|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"DCT"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://en.wikipedia.org/wiki/Discrete_cosine_transform|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossy.LinearPredictiveCoding|Lossy.LinearPredictiveCoding|Linear Predictive Coding|-|LPC is a method used mostly in audio signal processing and speech processing for representing the spectral envelope of a digital signal of speech in compressed form, using the information of a linear predictive model.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Linear&nbsp;Predictive&nbsp;compression"<br>&nbsp;&nbsp;},<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"LPC"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://en.wikipedia.org/wiki/Linear_predictive_coding|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossy.SubbandCoding|Lossy.SubbandCoding|Sub-band Coding|-|SBC is any form of transform coding that breaks a signal into a number of different frequency bands, typically by using a fast Fourier transform, and encodes each one independently. This decomposition is often the first step in data compression for audio and video signals.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"SBC"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://en.wikipedia.org/wiki/Sub-band_coding|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossy.WaveletPacket|Lossy.WaveletPacket|Wavelet Packet|-|A wavelet transform where the discrete-time (sampled) signal is passed through more filters than the discrete wavelet transform (DWT).|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"WPC"<br>&nbsp;&nbsp;},<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Sub-band&nbsp;Tree"<br>&nbsp;&nbsp;}<br>]|OSDU Data Definitions subcommittee|https://www.ep.total.com/en/innovations/best-innovators-2019/using-wavelet-packet-compression-seismic-data|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Lossy|Lossy|Lossy|-|Lossy data compression (a general category of methods) eliminates data perceived to be less important or relevant in favor of reducing file size.  Lossy compressions cannot be reversed.|-|PPDM Members|-|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|
|Walsh%20Transform|Walsh Transform|Walsh Transform|-|A class of Fourier transforms.|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Hadamard&nbsp;Transform"<br>&nbsp;&nbsp;}<br>]|PPDM Members|https://library.seg.org/doi/10.1190/1.1440443|Workbook Published/CompressionMethodType.1.0.0.xlsx; commit SHA fa4e1b57.|2022-05-05T08:06:06+02:00|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# CompressionMethodType.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractCompressionInfo.1.0.0](../abstract/AbstractCompressionInfo.1.0.0.md) | CompressionMethodTypeID | one |  |
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._