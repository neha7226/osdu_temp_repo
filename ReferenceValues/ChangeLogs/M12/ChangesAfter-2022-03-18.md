<a name="TOC"></a>

[[_TOC_]]

# Reference Value Changes

## `osdu:wks:reference-data--AgreementType:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|ConstructionOwnershipOperating|2022-04-14|Corrected NAME4|
|InternationalUnitizationAndUnitOperating|2022-04-14|Corrected NAME4 to Upper Camel|
|InternationalUnitizationUnitOperating|2022-04-14|Deprecated NAME4|
|MemorandumOfUnderstanding|2022-04-14|Coorect NAME4 to Upper Camel|
|NonCrossConveyedPooling|2022-04-14|Corrected NAME4 to Upper Camel|
|NonCrossconveyedPoolingAgreement|2022-04-14|Deprecated NAME4|
|NonDisclosure|2022-04-14|Corrected NAME4 to Upper Camel|
|NoticeOfAssignment|2022-04-14|Corrected NAME4 to Upper Camel|
|StudyAndBidGroup|2022-04-14|Corrected NAME4 to Upper Camel|
|StudyBidGroup|2022-04-14|Deprecated NAME4|
|TieIn|2022-04-14|Corrected NAME4 to Upper Camel|
|UnitizationAndOperating|2022-04-14|Corrected NAME4 to Upper Camel|
|UnitizationUnitOperating|2022-04-14|Deprecated NAME4|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--CalculationMethodType:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|Radiusofcurvature|2022-04-13|Deprecated|
|RadiusOfCurvature|2022-04-13|Corrected NAME4|
|CurvatureRadius|2022-04-13|Deprecated|
|SimpsonsRule|2022-04-13|Corrected NAME4, removed space|
|ThreeDimensionalRadiusOfCurvature|2022-04-13|Corrected NAME4, added hyphen|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--OrganisationType:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|AcademicInstitution|2022-04-28|Deprecated, shorten to use Academy|
|Academy|2022-04-28|New value|
|Association|2022-04-28|New value|
|Consortium|2022-04-28|New value|
|Corporation|2022-04-28|New value|
|Government|2022-04-28|New value|
|GovernmentAgency|2022-04-28|Deprecated, shortened to use Government, agency is referenced in the definition|
|IndustryOrganization|2022-04-28|Deprecated, added a new value of Association|
|NationalEnterprise|2022-04-28|New value|
|NonGovernmentalOrganization|2022-04-28|New value|
|OrganisationUnit|2022-04-28|Deprecated, Spelling changed to the NA spelling using Z|
|Proprietorship|2022-04-28|New value|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--PlayType:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|CarbonCaptureStorage|2022-04-16|Added deprecation to use CarbonCaptureAndStorage instead.|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--ProspectType:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|StructuralStratigraphic.HybridTrap|2022-04-22|Corrected NAME4|
|StructuralStratigraphic.Hybrid|2022-04-22|Deprecated|
|Unconventional.Hydrodynamic|2022-04-22|Deprecated|
|Unconventional.HydrodynamicTrap|2022-04-22|Corrected NAME4|

[Back to TOC](#TOC)