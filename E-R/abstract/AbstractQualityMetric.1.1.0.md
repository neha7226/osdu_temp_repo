<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractQualityMetric [Status: Accepted]
Generic quality metric schema fragment containing the universal properties for data profiling and data metrics of OSDU objects.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractQualityMetric:1.1.0`
* Schema status: PUBLISHED
* **Migration Guide (M12)** [`osdu:wks:AbstractQualityMetric:1.0.0` &rarr; `osdu:wks:AbstractQualityMetric:1.1.0`](../../Guides/MigrationGuides/M12/AbstractQualityMetric.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractQualityMetric.1.1.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractQualityMetric.1.1.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractQualityMetric.1.1.0.json](../../Authoring/abstract/AbstractQualityMetric.1.1.0.json)
* Link to &rarr; [Generated Schema AbstractQualityMetric.1.1.0.json](../../Generated/abstract/AbstractQualityMetric.1.1.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractQualityMetric.1.1.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractQualityMetric.1.1.0.json)
* Link to worked examples in context &rarr; [Topic: Usage of `work-product-component--DataQuality`](../../Examples/WorkedExamples/DataQuality/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractQualityMetric


## Outgoing Relationships for AbstractQualityMetric

![AbstractQualityMetric](../_diagrams/abstract/AbstractQualityMetric.1.1.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractQualityMetric Referenced by other Entities

![AbstractQualityMetric](../_diagrams/abstract/AbstractQualityMetric.1.1.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractQualityMetric Properties

## Table of AbstractQualityMetric Properties (Version 1.1.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|RunDateTime| |string|optional|Run Date Time|Run timestamp of the Business data quality ruleset.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataCountTotal| |number|optional|Metadata Count Total|Total number of metadata attributes for object in scope.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataNull| |number|optional|Metadata Null|The count of total number where metadata is not populated.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataCountFormat| |number|optional|Metadata Count Format|Total number of attributes defined in reference value lists.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataFormat| |number|optional|Metadata Format|The count of total number where attribute follows the defined reference value lists.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataCountPattern| |number|optional|Metadata Count Pattern|Total number of attributes with a pattern.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataPattern| |number|optional|Metadata Pattern|The count of total number of attributes that follow pattern.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataCountRequired| |number|optional|Metadata Count Required|Total number required attributes.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataCountRequiredTrue| |number|optional|Metadata Count Required True|The count of required metadata that are not populated.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataCountAdditionalProperty| |number|optional|Metadata Count Additional Property|Total number of additional properties.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataAdditionalPropertiesTrue| |number|optional|Metadata Additional Properties True|The count of total number of additional properties that are not populated.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|MetadataScore| |number|optional|Metadata Score|The score in % for the evaluated record.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractQualityMetric.1.1.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [DataQuality.1.1.0](../work-product-component/DataQuality.1.1.0.md) | QualityMetric | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._