<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractIjkGridPatch [Status: Accepted]
Definition of a collection of hexahedra which are organized and indexable by means of 3 (almost orthogonal) directions and called I, J and K. Only an abstract group type in order to be reused in GPGrid.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractIjkGridPatch:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractIjkGridPatch.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractIjkGridPatch.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractIjkGridPatch.1.0.0.json](../../Authoring/abstract/AbstractIjkGridPatch.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractIjkGridPatch.1.0.0.json](../../Generated/abstract/AbstractIjkGridPatch.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractIjkGridPatch.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractIjkGridPatch.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractIjkGridPatch


## Outgoing Relationships for AbstractIjkGridPatch

![AbstractIjkGridPatch](../_diagrams/abstract/AbstractIjkGridPatch.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractIjkGridPatch Referenced by other Entities

![AbstractIjkGridPatch](../_diagrams/abstract/AbstractIjkGridPatch.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractIjkGridPatch Properties

## 1. Table of AbstractIjkGridPatch Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Nk|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|integer|optional|Count of cells in K direction|Count of cells in the K-direction (aka third and/or slowest and/or vertical direction) in the grid. Must be positive.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|100|
|KDirectionID &rarr; [KDirectionType](../reference-data/KDirectionType.1.0.0.md)|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|string|optional|K Direction ID|Indicate the K direction of the grid : up, down or not monotonic|<code>^[\w\-\.]+:reference-data\-\-KDirectionType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [KDirectionType](../reference-data/KDirectionType.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--KDirectionType:Down:|
|HasCollocatedNodeInKDirection|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|boolean|optional|Has Collocated Node In K Direction|Indicate if at least two adjacent nodes in K Direction are collocated. Also known as pinched node. Usually occur in erosional context.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|True|
|PillarShapeID &rarr; [PillarShapeType](../reference-data/PillarShapeType.1.0.0.md)|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|string|optional|Pillar Shape ID|Indicate the most complex pillar shape of a grid : vertical, straight, curved|<code>^[\w\-\.]+:reference-data\-\-PillarShapeType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [PillarShapeType](../reference-data/PillarShapeType.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--PillarShapeType:Straight:|
|HasLateralGaps|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|boolean|optional|Has Lateral Gaps|Indicate that it exists at least one gap in the lateral direction of the grid. A gap is really a hole, it is not a slice of dead cells.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|False|
|HasKGaps|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|boolean|optional|Has K Gaps|Indicate that it exists at least one gap in the whole K direction of the grid. A gap is really a hole, it is not a layer of dead cells.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|False|
|HasParametricGeometry|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|boolean|optional|Has Parametric Geometry|Indicate that the nodes of the grid are given by means of a parameter along the pillar. Otherwise nodes of the grid are explicitly given by means of an XYZ triplet.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|False|
|HasSplitNode|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|boolean|optional|Has split node|Indicate that the grid contains some split nodes i.e some node which are not on a pillar.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|False|
|HasTruncations|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|boolean|optional|Has Truncations|Indicate that some of the pillars of the grid are truncated (Fault contact in Y shape for example)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|False|
|ExpansionInDirection|[AbstractColumnLayerGridPatch.1.0.0](AbstractColumnLayerGridPatch.1.0.0.md)|enum string|optional|Expansion in direction|Indicate if a grid has been topologically expanded in a particular dimension (K expansion, J expansion, I expansion)|<code>I&#124;J&#124;K</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|I|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of AbstractIjkGridPatch Data Properties, Section AbstractIjkGridPatch

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Ni| |integer|optional|Count of cells in I direction|Count of cells in the I-direction (aka first and/or fastest direction) in the grid. Must be positive.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|25|
|Nj| |integer|optional|Count of cells in J direction|Count of cells in the J-direction (aka second direction) in the grid. Must be positive.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|50|
|IsRightHanded| |boolean|optional|Is Right Handed|Indicates that the IJK grid is right handed, as determined by the triple product of tangent vectors in the I, J, and K directions.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|True|
|IsRadial| |boolean|optional|Is Radial|TRUE if the grid is periodic in J, i.e., has the topology of a complete 360 degree circle.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|False|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractIjkGridPatch.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [GpGridRepresentation.1.0.0](../work-product-component/GpGridRepresentation.1.0.0.md) | IjkGridPatches[] | many (as array) |  |
| Included by | [IjkGridRepresentation.1.0.0](../work-product-component/IjkGridRepresentation.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._