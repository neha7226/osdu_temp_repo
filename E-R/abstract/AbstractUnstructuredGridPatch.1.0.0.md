<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractUnstructuredGridPatch [Status: Accepted]
Definition of a collection of polyhedra which are not organized in any dimension. Only an abstract group type in order to be reused in GPGrid.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractUnstructuredGridPatch:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractUnstructuredGridPatch.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractUnstructuredGridPatch.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractUnstructuredGridPatch.1.0.0.json](../../Authoring/abstract/AbstractUnstructuredGridPatch.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractUnstructuredGridPatch.1.0.0.json](../../Generated/abstract/AbstractUnstructuredGridPatch.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractUnstructuredGridPatch.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractUnstructuredGridPatch.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractUnstructuredGridPatch


## Outgoing Relationships for AbstractUnstructuredGridPatch

![AbstractUnstructuredGridPatch](../_diagrams/abstract/AbstractUnstructuredGridPatch.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractUnstructuredGridPatch Referenced by other Entities

![AbstractUnstructuredGridPatch](../_diagrams/abstract/AbstractUnstructuredGridPatch.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractUnstructuredGridPatch Properties

## Table of AbstractUnstructuredGridPatch Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|FaceCount| |integer|optional|Count of faces|The count of faces in this grid|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2000|
|NodeCount| |integer|optional|Count of nodes|The count of nodes in this grid|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|6500|
|CellShapeID &rarr; [CellShapeType](../reference-data/CellShapeType.1.0.0.md)| |string|optional|Uniform shape of all cells|Indicates the uniform shape of all cells in this grid : tetrahedral, pyramidal, prism, hexahedral, polyhedral|<code>^[\w\-\.]+:reference-data\-\-CellShapeType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [CellShapeType](../reference-data/CellShapeType.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--CellShapeType:Polyhedral:|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractUnstructuredGridPatch.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [GpGridRepresentation.1.0.0](../work-product-component/GpGridRepresentation.1.0.0.md) | UnstructuredGridPatches[] | many (as array) |  |
| Included by | [UnstructuredGridRepresentation.1.0.0](../work-product-component/UnstructuredGridRepresentation.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._