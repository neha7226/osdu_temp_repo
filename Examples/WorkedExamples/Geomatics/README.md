<a name="TOC">Table of Contents</a>

[[_TOC_]]

# Summary of Geomatics in OSDU Schemas

## Catalogs

OSDU R3 will _**not**_ mirror the full
[EPSG GeoRepository](https://apps.epsg.org/api/swagger/ui/index#)
data model. Instead, two catalogs are introduced, which can be populated with content derived from
the [EPSG GeoRepository](https://apps.epsg.org/api/swagger/ui/index#):

### CoordinateReferenceSystem

* [`CoordinateReferenceSystem`](../../../E-R/reference-data/CoordinateReferenceSystem.1.0.0.md)
  is capable of holding the most important spatial reference information and supports simple search functionality. This
  entity describes any of the following sub-types:
    * `BoundCRS`
    * `CompoundCRS`
    * `DerivedCRS`
    * `EngineeringCRS`
    * `GeodeticCRS`
    * `ProjectedCRS`
    * `VerticalCRS`

### CoordinateTransformation

* [`CoordinateTransformation`](../../../E-R/reference-data/CoordinateTransformation.1.0.0.md)
  allows referencing transformations from BoundCRS and creates the prerequisites for a transformation selection. The
  entity describes the following sub-types:
    * `Transformation` aka single coordinate operations
    * `ConcatenatedOperation`, i.e. a chained sequence of single transformations.

Please consider the catalog entities above as part of the Frame of Reference definition. The details are explained in a
separate [FrameOfReference documentation](../FrameOfReference/README.md)

### Coupling to Data Model

The two types provide the complete relationship surface to the rest of the OSDU data schema. The full EPSG data model
can be introduced without changing the consumers, i.e., entity schemas referring to
[`CoordinateReferenceSystem`](../../../E-R/reference-data/CoordinateReferenceSystem.1.0.0.md).

The coupling points to
[`CoordinateReferenceSystem`](../../../E-R/reference-data/CoordinateReferenceSystem.1.0.0.md)
are via - for example:

* [`AbstractSpatialLocation.CoordinateReferenceSystemID`](../../../E-R/abstract/AbstractSpatialLocation.1.0.0.md)
* [`AbstractMetaItem.CoordinateReferenceSystemID`](../../../E-R/abstract/AbstractMetaItem.1.0.0.md)
* [`AbstractFacilityVerticalMeasurement.VerticalCRSID`](../../../E-R/abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)
* [`Well data.DefaultVerticalCRSID`](../../../E-R/master-data/Well.1.0.0.md)
* [`FaultSystem data.HorizontalCRSID`](../../../E-R/work-product-component/FaultSystem.1.0.0.md)
* [`SeismicLineGeometry data.HorizontalCRSID`](../../../E-R/work-product-component/SeismicLineGeometry.1.0.0.md)
* [`SeismicTraceData data.HorizontalCRSID`](../../../E-R/work-product-component/SeismicTraceData.1.0.0.md)
* [`VelocityModeling data.HorizontalCRSID`](../../../E-R/work-product-component/VelocityModeling.1.0.0.md)

The above list is not complete. For a complete list of usage, please refer to the
[`CoordinateReferenceSystem` documentation, Usage table](../../../E-R/reference-data/CoordinateReferenceSystem.1.0.0.md#coordinatereferencesystem100-usage)

### Coupling to Geodetic API

The Geodetic API _**does not**_ yet depend on the CRS descriptions in the catalog records.
Instead `CoordinateReferenceSystem` and `CoordinateTransform` contain a complete, self-contained, engine-consumable CRS
or transform definitions. The definition uses ESRI WKT to ensure backwards compatibility with legacy systems. ESRI WKT
is generally accepted by many spatial engine implementations.

This ESRI WKT dependency can be overcome once solid forward and backwards translations are available.

### Catalog Contents

OSDU will not deliver contents for the CoordinateReferenceSystem and CoordinateTransformation catalogs, except examples.
This allows operators to populate the catalogs with their own naming and usage recommendations.

[&rarr; Back to Table of Contents](#TOC)

# Geo-Spatial Representation

OSDU's standard representation of geo-spatial data is
[`AbstractSpatialLocation`](../../../E-R/abstract/AbstractSpatialLocation.1.0.0.md). It relies on GeoJSON and
GeoJSON-like schemas capable of holding

* `Point`
* `MultiPoint`
* `LineString`
* `MultiLineString`
* `Polygon` (Ring)
* `MultiPolygon`

geometries. The contract comes with additional rules

1. implied by GeoJSON: the order of coordinates is fixed and not depending on the coordinate order defined by the EPSG
   CRS:
   _**Longitude/Easting/Westing/X first, followed by Latitude/Northing/Southing/Y**_
1. added by OSDU: the optional third coordinate is not referring to EPSG 84 ellipsoidal height but to EPSG:5714 MSL
   height unless another vertical CRS is specified via the CompoundCRS.

## FeatureCollection

GeoJSON is exclusively `WGS 84` and replicated in
[`AbstractFeatureCollection`](../../../E-R/abstract/AbstractFeatureCollection.1.0.0.md), which is an exact copy of the
published [GeoJSON schema](https://geojson.org/schema/FeatureCollection.json).

## AnyCrsFeatureCollection

GeoJSON is WGS 84 _**only**_. To overcome this limitation and disambiguate the data structure from GeoJSON a copy of
GeoJSON has been defined as
[`AbstractAnyCrsFeatureCollection`](../../../E-R/abstract/AbstractAnyCrsFeatureCollection.1.0.0.md).  
The alterations are:

1. `AbstractAnyCrsFeatureCollection.persistableReferenceCRS` contains the complete, self-contained, engine-consumable
   CRS. There is a sibling
   `CoordinateReferenceSystemID`, which represents the catalog reference.
1. optional `AbstractAnyCrsFeatureCollection.persistableReferenceVerticalCRS`
   referring to an explicit vertical CRS avoiding the creating of CompoundCRS. There is a
   sibling `VerticalCoordinateReferenceSystemID`, which represents the catalog reference for the optional VerticalCRS
   ID.
1. `AbstractAnyCrsFeatureCollection.persistableReferenceUnitZ` for an optional definition of the third coordinate's unit
   if only a 2D CRS is provided.
1. Disambiguated values for the geometry types:
    * `AnyCrsPoint`
    * `AnyCrsMultiPoint`
    * `AnyCrsLineString`
    * `AnyCrsMultiLineString`
    * `AnyCrsPolygon`
    * `AnyCrsMultiPolygon`

[&rarr; Back to Table of Contents](#TOC)

# Geodesy API (Engine)

The Geodesy API (powered by [Apache SIS](https://sis.apache.org/)) consumes
the [`persistableReference`](../../../E-R/abstract/AbstractPersistableReference.1.0.0.md)
strings - today. It is independent of the content delivered by the
`CoordinateReferenceSystem` catalog.

The "complete, self-contained, persistable reference strings"
contain ESRI WKT. It is independent of the CRS catalog until the WKT translation issues are resolved,
see [Coupling to Geodetic API](#coupling-to-geodetic-api).

## Capabilities

The [CRS Converter](https://community.opengroup.org/osdu/platform/system/reference/crs-conversion-service/-/blob/master/docs/api_spec/crs_converter_openapi.json)
API offers

1. point conversion `/convert`
1. GeoJSON `AbstractAnyCrsFeatureCollection` &harr; `AbstractFeatureCollection`
   using `/convertGeoJson`
1. MD, Inclination, Azimuth Trajectory computations, including azimuth, scale and optionally coning correction (LMP)
   using `/convertTrajectory`

## Limitations

The API (see [Capabilities](#capabilities)) operates with two CRS definitions, one defining the source `fromCRS` and one
for the target `toCRS`. This works well with BoundCRS or late-bound CRS based on the same GeodeticCRS.

To support general coordinate operations including coordinate transformations it is required to create a transient
BoundCRS persistableReference string, which binds the selected transformation to one of the source or target CRSs.

In the next iteration of the Geodetic API development an optional transformation needs to be added to the request
structure. If defined, the specified transformation takes precedence over any bound transformation.

The Apache SIS engine implementation only supports EPSG standard transformations and depend on this being defined in the
ESRI WKT, e.g. `AUTHORITY[\"EPSG\",1613]]`

## Usage Examples

The worked examples for [WellboreTrajectory.1.1.0](../WellboreTrajectory/README.md)
represents a realistic use case. there are worked our examples for request/response payloads for the CRS Converter.

### ED50 / UTM zone 31N * DMA-mean [23031,1133] &rarr; WGS 84

<details>

<summary markdown="span"><code>/convertGeoJson</code> request</summary>

<OsduImportJSON>[request](../WellboreTrajectory/crs_operations/wrp_23031001_to_4326_request.json)</OsduImportJSON>

```json
{
  "toCRS": "{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"4326\"},\"name\":\"GCS_WGS_1984\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433],AUTHORITY[\\\"EPSG\\\",4326]]\"}",
  "featureCollection": {
    "type": "AnyCrsFeatureCollection",
    "persistableReferenceCrs": "{\"authCode\":{\"auth\":\"OSDU\",\"code\":\"23031001\"},\"lateBoundCRS\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"23031\"},\"name\":\"ED_1950_UTM_Zone_31N\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"PROJCS[\\\"ED_1950_UTM_Zone_31N\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",3.0],PARAMETER[\\\"Scale_Factor\\\",0.9996],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0],AUTHORITY[\\\"EPSG\\\",23031]]\"},\"name\":\"ED50 * DMA-mean / UTM zone 31N [23031,1133]\",\"singleCT\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"1133\"},\"name\":\"ED_1950_To_WGS_1984_1\",\"type\":\"ST\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGTRAN[\\\"ED_1950_To_WGS_1984_1\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],METHOD[\\\"Geocentric_Translation\\\"],PARAMETER[\\\"X_Axis_Translation\\\",-87.0],PARAMETER[\\\"Y_Axis_Translation\\\",-98.0],PARAMETER[\\\"Z_Axis_Translation\\\",-121.0],AUTHORITY[\\\"EPSG\\\",1133]]\"},\"type\":\"EBC\",\"ver\":\"PE_10_9_1\"}",
    "persistableReferenceUnitZ": "{\"scaleOffset\":{\"scale\":1.0,\"offset\":0.0},\"symbol\":\"m\",\"baseMeasurement\":{\"ancestry\":\"Length\",\"type\":\"UM\"},\"type\":\"USO\"}",
    "features": [
      {
        "type": "AnyCrsFeature",
        "properties": {},
        "geometry": {
          "type": "AnyCrsPoint",
          "coordinates": [
            622011.96,
            5910706.37
          ]
        }
      }
    ]
  }
}
```

</details>

<details>

<summary markdown="span"><code>/convertGeoJson</code> response</summary>

<OsduImportJSON>[response](../WellboreTrajectory/crs_operations/wrp_23031001_to_4326_response.json)</OsduImportJSON>

```json
{
  "successCount": 1,
  "totalCount": 1,
  "featureCollection": {
    "features": [
      {
        "geometry": {
          "coordinates": [
            4.83073672457101,
            53.32956038067355
          ],
          "type": "Point",
          "bbox": null
        },
        "properties": {},
        "type": "Feature",
        "bbox": null
      }
    ],
    "properties": null,
    "persistableReferenceCrs": null,
    "persistableReferenceUnitZ": "{\"scaleOffset\":{\"scale\":1.0,\"offset\":0.0},\"symbol\":\"m\",\"baseMeasurement\":{\"ancestry\":\"Length\",\"type\":\"UM\"},\"type\":\"USO\"}",
    "type": "FeatureCollection",
    "bbox": null
  },
  "operationsApplied": [
    "conversion from ED_1950_UTM_Zone_31N to GCS_European_1950; 1 points converted",
    "transformation GCS_European_1950 to GCS_WGS_1984 using ED_1950_To_WGS_1984_1; 1 points successfully transformed"
  ]
}
```

</details>

### ED50 / UTM zone 31N * DMA-mean [23031,1133] &rarr; ED50 / TM 5 NE * DMA-mean [23095,1133]

<details>

<summary markdown="span"><code>/convertGeoJson</code> request</summary>

<OsduImportJSON>[request](../WellboreTrajectory/crs_operations/wrp_23031001_to_23095001_request.json)</OsduImportJSON>

```json
{
  "toCRS": "{\"authCode\":{\"auth\":\"OSDU\",\"code\":\"23095001\"},\"lateBoundCRS\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"23095\"},\"name\":\"ED_1950_TM_5_NE\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"PROJCS[\\\"ED_1950_TM_5_NE\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",5.0],PARAMETER[\\\"Scale_Factor\\\",0.9996],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0],AUTHORITY[\\\"EPSG\\\",23095]]\"},\"name\":\"ED50 * DMA-mean / TM 5 NE [23095,1133]\",\"singleCT\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"1133\"},\"name\":\"ED_1950_To_WGS_1984_1\",\"type\":\"ST\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGTRAN[\\\"ED_1950_To_WGS_1984_1\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],METHOD[\\\"Geocentric_Translation\\\"],PARAMETER[\\\"X_Axis_Translation\\\",-87.0],PARAMETER[\\\"Y_Axis_Translation\\\",-98.0],PARAMETER[\\\"Z_Axis_Translation\\\",-121.0],AUTHORITY[\\\"EPSG\\\",1133]]\"},\"type\":\"EBC\",\"ver\":\"PE_10_9_1\"}",
  "featureCollection": {
    "type": "AnyCrsFeatureCollection",
    "persistableReferenceCrs": "{\"authCode\":{\"auth\":\"OSDU\",\"code\":\"23031001\"},\"lateBoundCRS\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"23031\"},\"name\":\"ED_1950_UTM_Zone_31N\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"PROJCS[\\\"ED_1950_UTM_Zone_31N\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",3.0],PARAMETER[\\\"Scale_Factor\\\",0.9996],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0],AUTHORITY[\\\"EPSG\\\",23031]]\"},\"name\":\"ED50 * DMA-mean / UTM zone 31N [23031,1133]\",\"singleCT\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"1133\"},\"name\":\"ED_1950_To_WGS_1984_1\",\"type\":\"ST\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGTRAN[\\\"ED_1950_To_WGS_1984_1\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],METHOD[\\\"Geocentric_Translation\\\"],PARAMETER[\\\"X_Axis_Translation\\\",-87.0],PARAMETER[\\\"Y_Axis_Translation\\\",-98.0],PARAMETER[\\\"Z_Axis_Translation\\\",-121.0],AUTHORITY[\\\"EPSG\\\",1133]]\"},\"type\":\"EBC\",\"ver\":\"PE_10_9_1\"}",
    "features": [
      {
        "type": "AnyCrsFeature",
        "properties": {},
        "geometry": {
          "type": "AnyCrsPoint",
          "coordinates": [
            622011.96,
            5910706.37
          ]
        }
      }
    ]
  }
}
```

</details>

<details>

<summary markdown="span"><code>/convertGeoJson</code> response</summary>

<OsduImportJSON>[response](../WellboreTrajectory/crs_operations/wrp_23031001_to_23095001_response.json)</OsduImportJSON>

```json
{
  "successCount": 1,
  "totalCount": 1,
  "featureCollection": {
    "features": [
      {
        "geometry": {
          "coordinates": [
            488817.3086979347,
            5909154.619253226
          ],
          "type": "AnyCrsPoint",
          "bbox": null
        },
        "properties": {},
        "type": "AnyCrsFeature",
        "bbox": null
      }
    ],
    "properties": null,
    "persistableReferenceCrs": "{\"authCode\":{\"auth\":\"OSDU\",\"code\":\"23095001\"},\"lateBoundCRS\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"23095\"},\"name\":\"ED_1950_TM_5_NE\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"PROJCS[\\\"ED_1950_TM_5_NE\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",5.0],PARAMETER[\\\"Scale_Factor\\\",0.9996],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0],AUTHORITY[\\\"EPSG\\\",23095]]\"},\"name\":\"ED50 * DMA-mean / TM 5 NE [23095,1133]\",\"singleCT\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"1133\"},\"name\":\"ED_1950_To_WGS_1984_1\",\"type\":\"ST\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGTRAN[\\\"ED_1950_To_WGS_1984_1\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],METHOD[\\\"Geocentric_Translation\\\"],PARAMETER[\\\"X_Axis_Translation\\\",-87.0],PARAMETER[\\\"Y_Axis_Translation\\\",-98.0],PARAMETER[\\\"Z_Axis_Translation\\\",-121.0],AUTHORITY[\\\"EPSG\\\",1133]]\"},\"type\":\"EBC\",\"ver\":\"PE_10_9_1\"}",
    "persistableReferenceUnitZ": null,
    "type": "AnyCrsFeatureCollection",
    "bbox": null
  },
  "operationsApplied": [
    "conversion from ED_1950_UTM_Zone_31N to GCS_European_1950; 1 points converted",
    "conversion from GCS_European_1950 to ED_1950_TM_5_NE; 1 points converted"
  ]
}
```

</details>

### Trajectory Computation

* [`/convertTrajectory` request](../WellboreTrajectory/crs_operations/convert_trajectory_request.json)
* [`/convertTrajectory` response](../WellboreTrajectory/crs_operations/convert_trajectory_response.json)


[&rarr; Back to Table of Contents](#TOC)
