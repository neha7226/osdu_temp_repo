# Milestone 7 Migration

* WellboreTrajectory:1.0.0 to [osdu:wks:work-product-component--WellboreTrajectory:1.1.0](WellboreTrajectory.1.0.0.md)
* WellLog:1.0.0 to [osdu:wks:work-product-component--WellLog:1.1.0](WellLog.1.0.0.md)

# Links

* [Back to Migration Guides](../README.md)
* [Back to Guides](../../README.md)