<a name="TOC"></a>

[[_TOC_]]
# UnitQuantity Content Changes
## Value Changes

Total number of changes 0 in total 305 UnitQuantity records.

| Name | ParentName | Changed from | Changed to | ID |
|---|---|---|---|---|

[Back to TOC](#TOC)

## NameAlias Changes

Total number of alias changes 162 in total 305 UnitQuantity records.

| Name | # previous Alias Names | # of current Aliases |
|---|---|---|
| (amount of substance per time) per area|0|2 |
| (energy per mass) per time|0|2 |
| (mass per time) per area|0|3 |
| (mass per time) per length|0|3 |
| (mass per volume) per length|0|3 |
| (volume per time) length|0|2 |
| (volume per time) per (pressure length)|0|2 |
| (volume per time) per area|0|3 |
| (volume per time) per length|0|3 |
| (volume per time) per pressure|0|2 |
| (volume per time) per time|0|2 |
| (volume per time) per volume|0|2 |
| API gamma ray|0|2 |
| API neutron|0|2 |
| absorbed dose|0|2 |
| activity [of radioactivity]|0|2 |
| amount of substance|0|2 |
| amount of substance per area|0|2 |
| amount of substance per time|0|2 |
| amount of substance per volume|0|4 |
| angle per length|0|3 |
| angle per volume|0|2 |
| angular acceleration|0|3 |
| angular velocity|0|8 |
| area|0|3 |
| area per amount of substance|0|2 |
| area per area|0|4 |
| area per mass|0|2 |
| area per time|0|1 |
| area per volume|0|3 |
| attenuation per frequency interval|0|4 |
| capacitance|0|2 |
| data transfer speed|0|2 |
| diffusion coefficient|0|2 |
| digital storage|0|2 |
| dimensionless|0|6 |
| dipole moment|0|2 |
| dose equivalent|0|2 |
| dynamic viscosity|0|2 |
| electric charge|0|8 |
| electric charge per area|0|4 |
| electric charge per mass|0|2 |
| electric charge per volume|0|3 |
| electric conductance|0|8 |
| electric conductivity|0|2 |
| electric current|0|5 |
| electric current density|0|2 |
| electric field strength|0|2 |
| electric potential difference|0|4 |
| electric resistance|0|6 |
| electric resistance per length|0|2 |
| electrical resistivity|0|2 |
| electromagnetic moment|0|3 |
| energy|0|7 |
| energy length per area|0|2 |
| energy length per time area temperature|0|2 |
| energy per area|0|4 |
| energy per length|0|3 |
| energy per mass|0|4 |
| energy per volume|0|12 |
| force|0|2 |
| force area|0|2 |
| force length per length|0|3 |
| force per force|0|2 |
| force per length|0|3 |
| force per volume|0|5 |
| frequency|0|2 |
| frequency interval|0|2 |
| heat capacity|0|2 |
| heat flow rate|0|2 |
| heat transfer coefficient|0|2 |
| illuminance|0|5 |
| inductance|0|5 |
| isothermal compressibility|0|2 |
| kinematic viscosity|0|3 |
| length|0|34 |
| length per length|0|9 |
| length per temperature|0|2 |
| length per time|0|4 |
| length per volume|0|3 |
| light exposure|0|2 |
| linear acceleration|0|2 |
| linear thermal expansion|0|2 |
| logarithmic power ratio|0|2 |
| logarithmic power ratio per length|0|3 |
| luminance|0|2 |
| luminous efficacy|0|2 |
| luminous flux|0|2 |
| luminous intensity|0|2 |
| magnetic dipole moment|0|2 |
| magnetic field strength|0|6 |
| magnetic flux|0|2 |
| magnetic flux density|0|5 |
| magnetic permeability|0|2 |
| magnetic vector potential|0|2 |
| mass|0|2 |
| mass length|0|2 |
| mass per area|0|4 |
| mass per energy|0|2 |
| mass per length|0|4 |
| mass per mass|0|4 |
| mass per time|0|3 |
| mass per volume|0|8 |
| mobility|0|2 |
| molar energy|0|4 |
| molar heat capacity|0|4 |
| molar volume|0|2 |
| moment of force|0|5 |
| moment of inertia|0|2 |
| momentum|0|3 |
| permeability length|0|2 |
| permeability rock|0|3 |
| permittivity|0|2 |
| plane angle|0|2 |
| potential difference per power drop|0|3 |
| power|0|5 |
| power per area|0|14 |
| power per power|0|3 |
| power per volume|0|4 |
| pressure|0|8 |
| pressure per time|0|3 |
| pressure per volume|0|2 |
| pressure squared|0|2 |
| pressure squared per (force time per area)|0|2 |
| pressure time per volume|0|4 |
| quantity of light|0|2 |
| radiance|0|2 |
| radiant intensity|0|2 |
| reciprocal (mass time)|0|2 |
| reciprocal area|0|2 |
| reciprocal electric potential difference|0|2 |
| reciprocal force|0|2 |
| reciprocal length|0|6 |
| reciprocal mass|0|2 |
| reciprocal pressure|0|3 |
| reciprocal time|0|5 |
| reciprocal volume|0|2 |
| reluctance|0|2 |
| second moment of area|0|4 |
| solid angle|0|2 |
| specific heat capacity|0|4 |
| temperature interval|0|2 |
| temperature interval per length|0|2 |
| temperature interval per time|0|2 |
| thermal conductance|0|2 |
| thermal conductivity|0|2 |
| thermal diffusivity|0|3 |
| thermal insulance|0|3 |
| thermal resistance|0|2 |
| thermodynamic temperature|0|2 |
| time|0|7 |
| time per length|0|7 |
| time per time|0|2 |
| time per volume|0|3 |
| volume|0|7 |
| volume per area|0|6 |
| volume per length|0|4 |
| volume per mass|0|8 |
| volume per time|0|7 |
| volume per volume|0|13 |
| volumetric heat transfer coefficient|0|2 |
| volumetric thermal expansion|0|2 |

[Back to TOC](#TOC)