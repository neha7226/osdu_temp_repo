<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractInterpretation [Status: Accepted]
The group of properties shared by different kinds of interpretations
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractInterpretation:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractInterpretation.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractInterpretation.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractInterpretation.1.0.0.json](../../Authoring/abstract/AbstractInterpretation.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractInterpretation.1.0.0.json](../../Generated/abstract/AbstractInterpretation.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractInterpretation.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractInterpretation.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractInterpretation


## Outgoing Relationships for AbstractInterpretation

![AbstractInterpretation](../_diagrams/abstract/AbstractInterpretation.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractInterpretation Referenced by other Entities

![AbstractInterpretation](../_diagrams/abstract/AbstractInterpretation.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractInterpretation Properties

## Table of AbstractInterpretation Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|OlderPossibleAge| |number|optional|OlderPossibleAge|Represents the older possible age for the interpretation. Expressed in geological time as millions of years [Ma], and the value must be positive. The age value is not subject to normalization.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|100|
|YoungerPossibleAge| |number|optional|YoungerPossibleAge|Represents the younger possible age for the interpretation. Expressed in geological time as millions of years [Ma], and the value must be positive. The age value is not subject to normalization.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|80|
|DomainTypeID &rarr; [DomainType](../reference-data/DomainType.1.0.0.md)| |string|optional|DomainTypeID|Describes the domain of the interpretation: Depth, Time, Mixed|<code>^[\w\-\.]+:reference-data\-\-DomainType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [DomainType](../reference-data/DomainType.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--DomainType:Time:|
|FeatureID &rarr; [BoundaryFeature](../master-data/BoundaryFeature.1.0.0.md) &rarr; [ModelFeature](../master-data/ModelFeature.1.0.0.md) &rarr; [RockVolumeFeature](../master-data/RockVolumeFeature.1.0.0.md) &rarr; [LocalBoundaryFeature](../work-product-component/LocalBoundaryFeature.1.0.0.md) &rarr; [LocalModelFeature](../work-product-component/LocalModelFeature.1.0.0.md) &rarr; [LocalRockVolumeFeature](../work-product-component/LocalRockVolumeFeature.1.0.0.md)| |string|optional|FeatureID|Reference to the Feature the Interpretation refers to. The actual type of the target is specified in the specialisation, i.e. the type, which includes this schema fragment.|<code>^[\w\-\.]+:(master-data\-\-BoundaryFeature&#124;master-data\-\-ModelFeature&#124;master-data\-\-RockVolumeFeature&#124;work-product-component\-\-LocalBoundaryFeature&#124;work-product-component\-\-LocalModelFeature&#124;work-product-component\-\-LocalRockVolumeFeature):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [BoundaryFeature](../master-data/BoundaryFeature.1.0.0.md) &rarr; [ModelFeature](../master-data/ModelFeature.1.0.0.md) &rarr; [RockVolumeFeature](../master-data/RockVolumeFeature.1.0.0.md) &rarr; [LocalBoundaryFeature](../work-product-component/LocalBoundaryFeature.1.0.0.md) &rarr; [LocalModelFeature](../work-product-component/LocalModelFeature.1.0.0.md) &rarr; [LocalRockVolumeFeature](../work-product-component/LocalRockVolumeFeature.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FeatureName| |string|optional|FeatureName|Name of the feature the Interpretation refers to|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "FeatureID", "TargetPropertyName": "Name"}|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractInterpretation.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AquiferInterpretation.1.0.0](../work-product-component/AquiferInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [EarthModelInterpretation.1.0.0](../work-product-component/EarthModelInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [FaultInterpretation.1.0.0](../work-product-component/FaultInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [FluidBoundaryInterpretation.1.0.0](../work-product-component/FluidBoundaryInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [GeobodyBoundaryInterpretation.1.0.0](../work-product-component/GeobodyBoundaryInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [GeobodyInterpretation.1.0.0](../work-product-component/GeobodyInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [GeologicUnitOccurrenceInterpretation.1.0.0](../work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [HorizonInterpretation.1.0.0](../work-product-component/HorizonInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [ReservoirCompartmentInterpretation.1.0.0](../work-product-component/ReservoirCompartmentInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [RockFluidOrganizationInterpretation.1.0.0](../work-product-component/RockFluidOrganizationInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [RockFluidUnitInterpretation.1.0.0](../work-product-component/RockFluidUnitInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [StratigraphicColumnRankInterpretation.1.0.0](../work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [StratigraphicColumnRankInterpretation.1.1.0](../work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md) | allOf | one |  |
| Included by | [StratigraphicUnitInterpretation.1.0.0](../work-product-component/StratigraphicUnitInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [StructuralOrganizationInterpretation.1.0.0](../work-product-component/StructuralOrganizationInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [VoidageGroupInterpretation.1.0.0](../work-product-component/VoidageGroupInterpretation.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._