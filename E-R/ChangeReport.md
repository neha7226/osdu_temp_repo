[[_TOC_]]

**Note:** Details as reported for each snapshot. The headlines and links to the auto-generated change reports are
auto-appended when the community schema mirror site changes (new snapshot). The community mirror site is located at
https://community.opengroup.org/osdu/data/data-definitions/-/tree/master. Additional comments can be added after the
link to the auto-generated change report.

---

# Snapshot 2021-04-01

Detailed report of changes _after_ [2021-04-01](./_ChangeReports/ChangeReport-2021-04-01.md).

1. Addressing schema change requests implied by
   [ADR #16 Array of Objects support by Indexer](https://community.opengroup.org/osdu/platform/system/indexer-service/-/issues/16)
1. Removed reference value records with duplicate ids.
    1. Update LogCurveType such that all PWLS values become available. The unique id was constructed by combining
       CompanyName:CurveMnemonic leaving 392 records out because they were dropped. Now duplicate records with the same
       CompanyName:CurveMnemonic are suffixed by a counter, e.g. CompanyName:CurveMnemonic:3 for the third occurrence of
       this CompanyName:CurveMnemonic in the incoming raw "PWLS v3.0 Logs.xlsx" Curves sheet

---

# Snapshot 2021-04-29

Detailed report of changes _after_ [2021-04-29](./_ChangeReports/ChangeReport-2021-04-29.md).

1. **WellLog** <br>
   Added [`osdu:wks:work-product-component--WellLog:1.1.0`](../E-R/work-product-component/WellLog.1.1.0.md), which
   enables WellLogs to support sampling other than measured depth.
    1. Added [`osdu:wks:reference-data--WellLogSamplingDomainType:1.0.0`](../E-R/reference-data/WellLogSamplingDomainType.1.0.0.md), 
       classifying the WellLog sampling type.
2. Correcting the
   [`osdu:wks:work-product-component--TubularUmbilical:1.0.0`](../E-R/work-product-component/TubularUmbilical.1.0.0.md)
   review status to Accepted.
3. **PWLS 3** <br>
   Added [`osdu:wks:reference-data--PropertyType:1.0.0`](../E-R/reference-data/PropertyType.1.0.0.md)
   representing the OPEN Energistics PWLS 3 property types. These reference data records are identified by UUIDs. To
   increase usability via search, [`osdu:wks:AbstractPropertyType:1.0.0`](../E-R/abstract/AbstractPropertyType.1.0.0.md)
   was added, which combines the `id` with the derived `Name` of the referenced record.
4. **Activity Model** <br>
   Major data model contribution informed by Energistics and extended to support workflows. The capabilities are best
   understood via the [WorkedExamples](../Examples/WorkedExamples/Activity/README.md). The main schemas are:
    1. [`osdu:wks:work-product-component--Activity:1.0.0`](../E-R/work-product-component/Activity.1.0.0.md) for normal
       software processing activities,
    1. [`osdu:wks:master-data--ActivityTemplate:1.0.0`](../E-R/master-data/ActivityTemplate.1.0.0.md) for workflows, and
    1. [`osdu:wks:master-data--ActivityTemplateArc:1.0.0`](../E-R/master-data/ActivityTemplateArc.1.0.0.md) to control
       the sequence of activity templates.
5. **WellboreTrajectory** <br>
   Added [`osdu:wks:work-product-component--WellboreTrajectory:1.1.0`](../E-R/work-product-component/WellboreTrajectory.1.1.0.md)
   to support better discoverability about what a trajectory can contribute. This is achieved by an additional array
   of `AvailableTrajectoryStationProperties`.
   <br>**User Story "Trajectory Ingestion and Enrichment"** with detailed instructions on how to use the system services
   to create spatially correct trajectories from raw data:
   [Worked Example for WellboreTrajectory:1.1.0](../Examples/WorkedExamples/WellboreTrajectory/README.md). <br>
   The string property `SurveyToolTypeID` is changed to become a relationship to the reference values SurveyToolType
   populated by the _ISCWSA Set of Generic Tool Codes_. Here the supporting schemas in detail:
    1. Schema
       fragment  [`osdu:wks:AbstractReferencePropertyType:1.0.0`](../E-R/abstract/AbstractReferencePropertyType.1.0.0.md)
       supporting 'columnar' property descriptors, like:
    1. [`osdu:wks:reference-data--TrajectoryStationPropertyType:1.0.0`](../E-R/reference-data/TrajectoryStationPropertyType.1.0.0.md)
       , which is referred to by WellboreTrajectory's `data.AvailableTrajectoryStationProperties`
    1. [`osdu:wks:reference-data--SurveyToolType:1.0.0`](../E-R/reference-data/SurveyToolType.1.0.0.md)
       holding the schema for SurveyToolType reference values.
6. **master-data `XxxID` and reference-data `ID`** <br>
   Updated title and description of the ID properties in master-data and reference-data group-types. These are intended
   to refer to _external IDs_ in e.g., in external master data management systems.
7. ~~**Collection**~~ see PersistedCollection below<br>
    1. ~~Added support for a generic collection entity, which can refer to any master-data, reference-data, work-product
       and/or work-product-component group-type member. Collection is not to be confused with the transient
       [DataCollection](../E-R/data-collection/DataCollection.1.0.0.md). The new entity is documented here:
       [`osdu:wks:master-data--Collection:1.0.0`](work-product-component/PersistedCollection.1.0.0.md).~~
    1. ~~The collection purpose or intent is classified by a new reference value type
       [`osdu:wks:reference-data--CollectionPurpose:1.0.0`](../E-R/reference-data/CollectionPurpose.1.0.0.md).~~
8. **UnitOfMeasureConfiguration** <br>
   A new LOCAL reference value type holding UnitOfMeasure and numeric presentation details - mainly for applications. It
   allows defaulting, customizations and personalization (this is driven by application logic of course) and a common
   persistence mechanism. See
   [`osdu:wks:reference-data--UnitOfMeasureConfiguration:1.0.0`](../E-R/reference-data/UnitOfMeasureConfiguration.1.0.0.md).

---

# Snapshot 2021-07-29 (towards M09)

Detailed report of changes _after_ [2021-07-29](./_ChangeReports/ChangeReport-2021-07-29.md).

1. **Anonymous object fragments**
    1. The up to now anonymous object in the data `allOf` array is programmatically named `IndividualProperties` to
       assist the schema version validation. Objects will be identified via their `title` to perform matching and
       comparison for schema validation.
    1. Also added a `title` for Well and Wellbore's VerticalMeasurementID element.<br>
    1. Also improved: the large table describing the `data` properties in the E-R markdown are split into sections
       by `allOf` array members.
2. **WellboreMarkerSet** <br>
   Similar modernization as for WellLog;1.1.0 and WellboreTrajectory:1.1.0. The content for the 'bulk' data
   (e.g., in the CSV file) ws worked out earlier by Chris Hough and Daniel Perna,
   [see here](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/wikis/Principles-for-Creating-OSDU-CSV-Templates#approved-templates):
    1. Added [`osdu:wks:reference-data--MarkerPropertyType:1.0.0`](../E-R/reference-data/MarkerPropertyType.1.0.0.md)
       to hold the well-known marker property types - or in context of CSV files column definitions. This type is based
       on [`osdu:wks:AbstractReferencePropertyType:1.0.0`](../E-R/abstract/AbstractReferencePropertyType.1.0.0.md).
    2. Created [`osdu:wks:work-product-component--WellboreMarkerSet:1.1.0`](../E-R/work-product-component/WellboreMarkerSet.1.1.0.md)
       to add
         1. `data.AvailableMarkerProperties[]` The array of MarkerProperty definitions describing the available
            properties for this instance of WellboreMarkerSet
         2. `data.Markers[].MarkerSubSeaVerticalDepth` The Marker's TVD
   converted to a Sub-Sea Vertical depth, i.e., below Mean Sea Level. Note that TVD values above MSL are negative. This
   is the same as true vertical depth referenced to the vertical CRS “MSL depth”.
3. **PersistedCollection** <br>
   Moved `master-data--Collection` (created earlier above) to the work-product-component group-type as
   `osdu:wks:work-product-component--PersistedCollection:1.0.0`.
    1. Added support for a generic collection entity, which can refer to any master-data, reference-data, work-product
       and/or work-product-component group-type member. PersistedCollection is not to be confused with the transient
       [DataCollection](../E-R/data-collection/DataCollection.1.0.0.md). The new entity is documented here:
       [`osdu:wks:work-product-component--PersistedCollection:1.0.0`](work-product-component/PersistedCollection.1.0.0.md).
    2. The collection purpose or intent is classified by a new reference value type
       [`osdu:wks:reference-data--CollectionPurpose:1.0.0`](../E-R/reference-data/CollectionPurpose.1.0.0.md).
4. **Generic Geometry Datasets** <br>
   In preparation of Generic Geometry Representations, we need content descriptions, which are specific enough to let
   consumers understand whether they can parse the data content or not.
    1. [`osdu:wks:dataset--File.GeoJSON:1.0.0`](./dataset/File.GeoJSON.1.0.0.md) for WGS 84 vector geometries.
    2. [`osdu:wks:dataset--FileCollection.Esri.Shape:1.0.0`](./dataset/FileCollection.Esri.Shape.1.0.0.md) for
       shapefiles. Shapefiles can refer to any coordinate reference system (CRS) but not necessarily to a complete
       spatial reference allowing the geometry to be placed on the world map. The full CRS specification is provided in
       the dataset via the schema fragment
       [`osdu:wks:AbstractSpatialReference:1.0.0`](./abstract/AbstractSpatialReference.1.0.0.md)
    3. Reference values for EncodingFormatType and SchemaFormatType are delivered in parallel.
    4. For further details and examples, please refer to
       the [WorkedExamples](../Examples/WorkedExamples/GenericGeometries/README.md)
5. **Governance Change** for `EncodingFormatType`, `SchemaFormatType` `AliasNameTypeClass` <br>
   New reference values have been added to support generic geometries and images.
    1. [`osdu:wks:reference-data--EncodingFormatType:1.0.0`](./reference-data/EncodingFormatType.1.0.0.md) changed from
       LOCAL to OPEN.
    2. [`osdu:wks:reference-data--SchemaFormatType:1.0.0`](./reference-data/SchemaFormatType.1.0.0.md) changed from
       LOCAL to OPEN.
    3. [`osdu:wks:reference-data--AliasNameTypeClass:1.0.0`](./reference-data/AliasNameTypeClass.1.0.0.md) changed from
       LOCAL to OPEN to match the governance model for the related types being merged into `ANT_ANTC_OT_SO.json`
       manifest (reference values).
6. **AbstractFacility** and **FacilityType**
    1. Add a description to `AbstractFacility` clarifying the term facility in OSDU.
    2. Update the description for `FacilityType` accordingly.
7. **WellLog:1.1.0 Corrections**<br>
   Based on issue
   [#255](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/issues/255)
   "Well Log Curve depths and units in WellLog:1.1.0", some deprecated properties (`data.Curves[].TopDepth`,
   `data.Curves[].BaseDepth`, `data.Curves[].DepthUnit`) have been revived. The names stay unchanged to avoid 
   disruptions for consumers. The description have been updated to note that the values may refer to other references 
   than only depth.
8. **ActivityTemplate**, **ActivityTemplateArc**
    1. Added `data.Name` and `data.Description` to
       [`osdu:wks:master-data--ActivityTemplate:1.0.0`](./master-data/ActivityTemplate.1.0.0.md), which was an oversight
       during the definition phase. It is expected that this type is not in widespread use; therefore the minor version
       is not incremented.
    2. Added `data.Description` to
       [`osdu:wks:master-data--ActivityTemplateArc:1.0.0`](./master-data/ActivityTemplateArc.1.0.0.md). It is expected
       that this type is not in widespread use; therefore the minor version is not incremented.

---

# Snapshot 2021-11-09 (towards M10)

Detailed report of changes _after_ [2021-11-09](./_ChangeReports/ChangeReport-2021-11-09.md).

1. **VirtualProperties**<br>
   Based on the
   community [search-service ADR #69](https://community.opengroup.org/osdu/platform/system/search-service/-/issues/69)
   the schemas for storable records were extended to carry a JSON extension tag defining virtual properties to resolve
   the following two challenges:
    1. Ambiguous Location information<br>
       A number of schemas offer more than one property with map coordinates.
       The `data.VirtualProperties.DefaultLocation`
       list disambiguates the locations by providing a priority. This way a deterministic map location be achieved.
    2. Default Name<br>
       There is no standard name property for all schemas. While `Name` is present in most of the OSDU schemas there are
       notable exceptions, e.g. Well and Wellbore with `FacilityName` instead. The `data.VirtualProperties.DefaultName`
       definition refers to where the default name property is to be derived from.
2. **VirtualProperties Support**<br>
    1. The script to create new proposal workbooks or proposal workbooks from existing schemas has been modified to
       populate a new fixed sheet 'VirtualPropertiesSpecification'.
    2. The script to parse schema proposals to JSON schema definitions has been updated to create the data block under
       the `x-osdu-virtual-properties` tag.
    3. An overview report about the VirtualProperties has been provided in
       [VirtualPropertiesReport.md](VirtualPropertiesReport.md).
3. **Indexing Hint Changes**<br>
   The following schema fragments were changed such that queries into arrays of objects frequently used return more
   scoped responses. This causes a massive re-indexing of existing records since virtually all storable osdu kinds
   include the following fragments - see more details in the following Migration Guides:
    1. [`osdu:wks:AbstractFacility:1.0.0`](../Guides/MigrationGuides/M10/AbstractFacility.1.0.0.md)
    2. [`osdu:wks:AbstractMaster:1.0.0`](../Guides/MigrationGuides/M10/AbstractMaster.1.0.0.md)
    3. [`osdu:wks:AbstractProject:1.0.0`](../Guides/MigrationGuides/M10/AbstractProject.1.0.0.md)
    4. [`osdu:wks:AbstractReferenceType:1.0.0`](../Guides/MigrationGuides/M10/AbstractReferenceType.1.0.0.md) - also
       adding a new property `CommitDate`, which does not require any migration effort. The population is taken care of
       by the reference-values repository.
    5. [`osdu:wks:AbstractWorkProductComponent:1.0.0`](../Guides/MigrationGuides/M10/AbstractWorkProductComponent.1.0.0.md)
4. **LogCurveType** and **LogCurveFamily** augmented with explicit relationships; the associated OSDU reference value
   manifests for LogCurveType and LogCurveFamily are updated with the new relationships.<br>
    1. `LogCurveType:1.1.0` records now relate to other reference-data types, such as:
        1. &rarr; [`PropertyType`](reference-data/PropertyType.1.0.0.md), via
           [AbstractPropertyType](abstract/AbstractPropertyType.1.0.0.md) `data.PropertyType.PropertyTypeID`
           and `data.PropertyType.Name`
        2. &rarr; [`UnitQuantity`](reference-data/UnitQuantity.1.0.0.md), via `data.UnitQuantityID`
        3. &rarr; [`StandardsOrganisation`](reference-data/StandardsOrganisation.1.0.0.md)
           via `data.AcquisitionCompanyID`
    2. `LogCurveFamily:1.1.0`
        1. &rarr; [`PropertyType`](reference-data/PropertyType.1.0.0.md), via
           [AbstractPropertyType](abstract/AbstractPropertyType.1.0.0.md) `data.PropertyType.PropertyTypeID`
           and `data.PropertyType.Name`
        2. &rarr; [`UnitQuantity`](reference-data/UnitQuantity.1.0.0.md), via `data.UnitQuantityID`
5. **New Schemas for Well Planning DDMS and Drilling Execution**
    1. `abstract`
        1. [`osdu:wks:AbstractContact:1.0.0`](./abstract/AbstractContact.1.0.0.md)
    2. `master-data`
        1. [`osdu:wks:master-data--ActivityPlan:1.0.0`](./master-data/ActivityPlan.1.0.0.md)
        2. [`osdu:wks:master-data--BHARun:1.0.0`](./master-data/BHARun.1.0.0.md)
        3. [`osdu:wks:master-data--CasingDesign:1.0.0`](./master-data/CasingDesign.1.0.0.md)
        4. [`osdu:wks:master-data--EvaluationPlan:1.0.0`](./master-data/EvaluationPlan.1.0.0.md)
        5. [`osdu:wks:master-data--FluidsProgram:1.0.0`](./master-data/FluidsProgram.1.0.0.md)
        6. [`osdu:wks:master-data--FluidsReport:1.0.0`](./master-data/FluidsReport.1.0.0.md) (Execution)
        7. [`osdu:wks:master-data--GeometricTargetSet:1.0.0`](./master-data/GeometricTargetSet.1.0.0.md)
        8. [`osdu:wks:master-data--HoleSection:1.0.0`](./master-data/HoleSection.1.0.0.md)
        9. [`osdu:wks:master-data--OperationsReport:1.0.0`](./master-data/OperationsReport.1.0.0.md) (Execution)
        10. [`osdu:wks:master-data--PlannedCementJob:1.0.0`](./master-data/PlannedCementJob.1.0.0.md)
        11. [`osdu:wks:master-data--Rig:1.0.0`](./master-data/Rig.1.0.0.md)
        12. [`osdu:wks:master-data--RigUtilization:1.0.0`](./master-data/RigUtilization.1.0.0.md)
        13. [`osdu:wks:master-data--Risk:1.0.0`](./master-data/Risk.1.0.0.md)
        14. [`osdu:wks:master-data--SurveyProgram:1.0.0`](./master-data/SurveyProgram.1.0.0.md)
        15. [`osdu:wks:master-data--WellActivityProgram:1.0.0`](./master-data/WellActivityProgram.1.0.0.md)
        16. [`osdu:wks:master-data--WellBarrierElementTest:1.0.0`](./master-data/WellBarrierElementTest.1.0.0.md)
        17. [`osdu:wks:master-data--WellPlanningWell:1.0.0`](./master-data/WellPlanningWell.1.0.0.md)
        18. [`osdu:wks:master-data--WellPlanningWellbore:1.0.0`](./master-data/WellPlanningWellbore.1.0.0.md)
        19. [`osdu:wks:master-data--WellboreArchitecture:1.0.0`](./master-data/WellboreArchitecture.1.0.0.md)
    3. `reference-data`
        1. [`osdu:wks:reference-data--ActivityCode:1.0.0`](./reference-data/ActivityCode.1.0.0.md)
        2. [`osdu:wks:reference-data--ActivityLevel:1.0.0`](./reference-data/ActivityLevel.1.0.0.md)
        3. [`osdu:wks:reference-data--ActivityOutcome:1.0.0`](./reference-data/ActivityOutcome.1.0.0.md) (Execution)
        4. [`osdu:wks:reference-data--ActivityOutcomeDetail:1.0.0`](./reference-data/ActivityOutcomeDetail.1.0.0.md) (
           Execution)
        5. [`osdu:wks:reference-data--AdditiveType:1.0.0`](./reference-data/AdditiveType.1.0.0.md)
        6. [`osdu:wks:reference-data--BhaStatus:1.0.0`](./reference-data/BhaStatus.1.0.0.md)
        7. [`osdu:wks:reference-data--BitDullCode:1.0.0`](./reference-data/BitDullCode.1.0.0.md) (Execution)
        8. [`osdu:wks:reference-data--BitReasonPulled:1.0.0`](./reference-data/BitReasonPulled.1.0.0.md) (Execution)
        9. [`osdu:wks:reference-data--BitType:1.0.0`](./reference-data/BitType.1.0.0.md) (Execution)
        10. [`osdu:wks:reference-data--CementJobType:1.0.0`](./reference-data/CementJobType.1.0.0.md)
        11. [`osdu:wks:reference-data--ChronoStratigraphy:1.0.0`](./reference-data/ChronoStratigraphy.1.0.0.md)
        12. [`osdu:wks:reference-data--ContactRoleType:1.0.0`](./reference-data/ContactRoleType.1.0.0.md)
        13. [`osdu:wks:reference-data--DrillingActivityClassType:1.0.0`](./reference-data/DrillingActivityClassType.1.0.0.md) (
            Execution)
        14. [`osdu:wks:reference-data--FluidPropertyFacetName:1.0.0`](./reference-data/FluidPropertyFacetName.1.0.0.md)
        15. [`osdu:wks:reference-data--FluidPropertyName:1.0.0`](./reference-data/FluidPropertyName.1.0.0.md)
        16. [`osdu:wks:reference-data--FluidRheologicalModelType:1.0.0`](./reference-data/FluidRheologicalModelType.1.0.0.md)
        17. [`osdu:wks:reference-data--FluidRole:1.0.0`](./reference-data/FluidRole.1.0.0.md)
        18. [`osdu:wks:reference-data--FluidType:1.0.0`](./reference-data/FluidType.1.0.0.md)
        19. [`osdu:wks:reference-data--FormationPressureTestType:1.0.0`](./reference-data/FormationPressureTestType.1.0.0.md)
        20. [`osdu:wks:reference-data--GasReadingType:1.0.0`](./reference-data/GasReadingType.1.0.0.md) (Execution)
        21. [`osdu:wks:reference-data--LithoStratigraphy:1.0.0`](./reference-data/LithoStratigraphy.1.0.0.md)
        22. [`osdu:wks:reference-data--LithologyType:1.0.0`](./reference-data/LithologyType.1.0.0.md)
        23. [`osdu:wks:reference-data--MudClass:1.0.0`](./reference-data/MudClass.1.0.0.md) (Execution)
        24. [`osdu:wks:reference-data--PersonnelOrganisationRole:1.0.0`](./reference-data/PersonnelOrganisationRole.1.0.0.md) (
            Execution)
        25. [`osdu:wks:reference-data--PersonnelServiceType:1.0.0`](./reference-data/PersonnelServiceType.1.0.0.md) (
            Execution)
        26. [`osdu:wks:reference-data--PlugType:1.0.0`](./reference-data/PlugType.1.0.0.md)
        27. [`osdu:wks:reference-data--PumpOpType:1.0.0`](./reference-data/PumpOpType.1.0.0.md) (Execution)
        28. [`osdu:wks:reference-data--ReasonTripType:1.0.0`](./reference-data/ReasonTripType.1.0.0.md)
        29. [`osdu:wks:reference-data--RigType:1.0.0`](./reference-data/RigType.1.0.0.md)
        30. [`osdu:wks:reference-data--RiskCategory:1.0.0`](./reference-data/RiskCategory.1.0.0.md)
        31. [`osdu:wks:reference-data--RiskConsequenceCategory:1.0.0`](./reference-data/RiskConsequenceCategory.1.0.0.md)
        32. [`osdu:wks:reference-data--RiskConsequenceSubCategory:1.0.0`](./reference-data/RiskConsequenceSubCategory.1.0.0.md)
        33. [`osdu:wks:reference-data--RiskDiscipline:1.0.0`](./reference-data/RiskDiscipline.1.0.0.md)
        34. [`osdu:wks:reference-data--RiskHierarchyLevel:1.0.0`](./reference-data/RiskHierarchyLevel.1.0.0.md)
        35. [`osdu:wks:reference-data--RiskResponseStatus:1.0.0`](./reference-data/RiskResponseStatus.1.0.0.md)
        36. [`osdu:wks:reference-data--RiskSubCategory:1.0.0`](./reference-data/RiskSubCategory.1.0.0.md)
        37. [`osdu:wks:reference-data--RiskType:1.0.0`](./reference-data/RiskType.1.0.0.md)
        38. [`osdu:wks:reference-data--StratigraphicColumnRankUnitType:1.0.0`](./reference-data/StratigraphicColumnRankUnitType.1.0.0.md)
        39. [`osdu:wks:reference-data--StratigraphicRoleType:1.0.0`](./reference-data/StratigraphicRoleType.1.0.0.md)
        40. [`osdu:wks:reference-data--TargetShape:1.0.0`](./reference-data/TargetShape.1.0.0.md)
        41. [`osdu:wks:reference-data--TargetType:1.0.0`](./reference-data/TargetType.1.0.0.md)
        42. [`osdu:wks:reference-data--TestSubType:1.0.0`](./reference-data/TestSubType.1.0.0.md)
        43. [`osdu:wks:reference-data--TestType:1.0.0`](./reference-data/TestType.1.0.0.md)
        44. [`osdu:wks:reference-data--WeatherType:1.0.0`](./reference-data/WeatherType.1.0.0.md) (Execution)
        45. [`osdu:wks:reference-data--WellActivityPhaseType:1.0.0`](./reference-data/WellActivityPhaseType.1.0.0.md)
        46. [`osdu:wks:reference-data--WellActivityProgramType:1.0.0`](./reference-data/WellActivityProgramType.1.0.0.md)
        47. [`osdu:wks:reference-data--WellSiteProductType:1.0.0`](./reference-data/WellSiteProductType.1.0.0.md) (
            Execution)
    4. `work-product-component`
        1. [`osdu:wks:work-product-component--PlannedLithology:1.0.0`](./work-product-component/PlannedLithology.1.0.0.md)
6. **Technical Assurance**
    * The Schema Usage Guide promised the existence of such a property, which was however never included in the schemas.
      The community found it important enough to add such a governance property to all storable records via
      `AbstractCommonResources`. To avoid disruption it has been decided not to increment the minor version of this
      schema fragment, the usual procedure if schemas are augmented with new properties. There is no record migration
      required since absent values are meant to represent "Unevaluated" technical assurance.<br>
      Changes:
        1. [`osdu:wks:AbstractCommonResources:1.0.0`](./abstract/AbstractCommonResources.1.0.0.md) got `
           referring to
        2. [`osdu:wks:reference-data--TechnicalAssuranceType:1.0.0`](./reference-data/TechnicalAssuranceType.1.0.0.md)
           as a new reference type.
7. **New Schemas related to GeoReferencedImage**<br>
   This milestone adds support for image datasets, generic image and geo-referenced image work-product-components. To
   provide vertical references embedded and shared reference levels are included. The following types are added:
    1. **abstract**
        1. [`osdu:wks:AbstractReferenceLevel:1.0.0`](./abstract/AbstractReferenceLevel.1.0.0.md)
    2. **dataset** group-type:
        1. [`osdu:wks:dataset--File.Image.JPEG:1.0.0`](./dataset/File.Image.JPEG.1.0.0.md)
        2. [`osdu:wks:dataset--File.Image.PNG:1.0.0`](./dataset/File.Image.PNG.1.0.0.md)
        3. [`osdu:wks:dataset--File.Image.TIFF:1.0.0`](./dataset/File.Image.TIFF.1.0.0.md)
        4. [`osdu:wks:dataset--File.Image.WorldFile:1.0.0`](./dataset/File.Image.WorldFile.1.0.0.md)
        5. [`osdu:wks:dataset--File.OGC.GeoTIFF:1.0.0`](./dataset/File.OGC.GeoTIFF.1.0.0.md)
    3. **master-data**
        1. [`osdu:wks:master-data--ReferenceLevel:1.0.0`](./master-data/ReferenceLevel.1.0.0.md)
    4. **reference-data**
        1. [`osdu:wks:reference-data--GeoReferencedImageType:1.0.0`](./reference-data/GeoReferencedImageType.1.0.0.md)
    5. **work-product-component**
        1. [`osdu:wks:work-product-component--GenericImage:1.0.0`](./work-product-component/GenericImage.1.0.0.md)
        2. [`osdu:wks:work-product-component--GeoReferencedImage:1.0.0`](./work-product-component/GeoReferencedImage.1.0.0.md)
8. **Seismic Model Enhancements**
    1. **AbstractProjectActivity** <br>
       The following types include &rarr; `AbstractProjectActivity` enabling these seismic master-data to behave like
       activities:
        1. [`osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0`](./master-data/Seismic2DInterpretationSet.1.1.0.md)
        2. [`osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0`](./master-data/Seismic3DInterpretationSet.1.1.0.md)
        3. [`osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0`](./master-data/SeismicAcquisitionSurvey.1.1.0.md)
        4. [`osdu:wks:master-data--SeismicProcessingProject:1.1.0`](./master-data/SeismicProcessingProject.1.1.0.md)
    2. **Pre-Stack Support** <br>
       Support pre-stack seismic data in acquisition surveys and trace data with reference values:
        1. [`osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0`](./master-data/Seismic3DInterpretationSet.1.1.0.md)
           also contains additional properties to support pre-stack. In order to clarify the model, a number of
           properties were deprecated in favour of more detailed arrays of objects. This requires some **_attention_**
           for upgrades of `SeismicAcquisitionSurvey:1.0.0` records, which are described in a separate migration guide
           for [`SeismicAcquisitionSurvey:1.0.0`](../Guides/MigrationGuides/M10/SeismicAcquisitionSurvey.1.0.0.md).
        2. [`osdu:wks:work-product-component--SeismicTraceData:1.1.0`](./work-product-component/SeismicTraceData.1.1.0.md)
           now offers a number of new pre-stack related properties. Assuming no pre-stack data exist in the platform
           instances, no migration should be required, see
           [`SeismicTraceData:1.0.0` migration](../Guides/MigrationGuides/M10/SeismicTraceData.1.0.0.md).
        3. Pre-Stack related Reference Value Types/Catalogs<br>
           Four new reference value types have been introduced, together with
            1. [`osdu:wks:reference-data--SeismicAcquisitionType:1.0.0`](./reference-data/SeismicAcquisitionType.1.0.0.md)
            2. [`osdu:wks:reference-data--SeismicGatherType:1.0.0`](./reference-data/SeismicGatherType.1.0.0.md)
            3. [`osdu:wks:reference-data--SeismicReceiverType:1.0.0`](./reference-data/SeismicReceiverType.1.0.0.md)
            4. [`osdu:wks:reference-data--SeismicTraceSortOrder:1.0.0`](./reference-data/SeismicTraceSortOrder.1.0.0.md)
9. **'Live' Schema Usage Guide**
    * The PDF version of
      the [Schema Usage Guide](https://gitlab.opengroup.org/osdu/subcommittees/data-def/docs/-/blob/master/OSDU_Schema_Usage_Guide_V1.0.pdf)
      has been updated to describe the current schema status. Coverage is not complete but more complete than the
      original PDF version, which was written long before the March 24th 2021 R3 release materialised. The link to
      the [Table of Contents of the Schema Usage Guide can be found here](../Guides/README.md).
10. **New Schemas for Earth Modeling/Reservoir**
    1. `abstract`
        1. [`osdu:wks:AbstractColumnLayerGridPatch:1.0.0`](./abstract/AbstractColumnLayerGridPatch.1.0.0.md)
        2. [`osdu:wks:AbstractFacet:1.0.0`](./abstract/AbstractFacet.1.0.0.md)
        3. [`osdu:wks:AbstractGeologicUnitInterpretation:1.0.0`](./abstract/AbstractGeologicUnitInterpretation.1.0.0.md)
        4. [`osdu:wks:AbstractGridRepresentation:1.0.0`](./abstract/AbstractGridRepresentation.1.0.0.md)
        5. [`osdu:wks:AbstractIjkGridPatch:1.0.0`](./abstract/AbstractIjkGridPatch.1.0.0.md)
        6. [`osdu:wks:AbstractInterpretation:1.0.0`](./abstract/AbstractInterpretation.1.0.0.md)
        7. [`osdu:wks:AbstractReferencePropertyType:1.1.0`](./abstract/AbstractReferencePropertyType.1.1.0.md)
        8. [`osdu:wks:AbstractRepresentation:1.0.0`](./abstract/AbstractRepresentation.1.0.0.md)
        9. [`osdu:wks:AbstractUnstructuredColumnLayerGridPatch:1.0.0`](./abstract/AbstractUnstructuredColumnLayerGridPatch.1.0.0.md)
        10. [`osdu:wks:AbstractUnstructuredGridPatch:1.0.0`](./abstract/AbstractUnstructuredGridPatch.1.0.0.md)
    2. `master-data`
        1. [`osdu:wks:master-data--BoundaryFeature:1.0.0`](./master-data/BoundaryFeature.1.0.0.md)
        2. [`osdu:wks:master-data--ModelFeature:1.0.0`](./master-data/ModelFeature.1.0.0.md)
        3. [`osdu:wks:master-data--RockVolumeFeature:1.0.0`](./master-data/RockVolumeFeature.1.0.0.md)
    3. `reference-data`
        1. [`osdu:wks:reference-data--BoundaryRelationType:1.0.0`](./reference-data/BoundaryRelationType.1.0.0.md)
        2. [`osdu:wks:reference-data--CellShapeType:1.0.0`](./reference-data/CellShapeType.1.0.0.md)
        3. [`osdu:wks:reference-data--ColumnBasedTableType:1.0.0`](./reference-data/ColumnBasedTableType.1.0.0.md)
        4. [`osdu:wks:reference-data--ColumnShapeType:1.0.0`](./reference-data/ColumnShapeType.1.0.0.md)
        5. [`osdu:wks:reference-data--DepositionGeometryType:1.0.0`](./reference-data/DepositionGeometryType.1.0.0.md)
        6. [`osdu:wks:reference-data--DomainType:1.0.0`](./reference-data/DomainType.1.0.0.md)
        7. [`osdu:wks:reference-data--FacetRole:1.0.0`](./reference-data/FacetRole.1.0.0.md)
        8. [`osdu:wks:reference-data--FacetType:1.0.0`](./reference-data/FacetType.1.0.0.md)
        9. [`osdu:wks:reference-data--FaultThrowType:1.0.0`](./reference-data/FaultThrowType.1.0.0.md)
        10. [`osdu:wks:reference-data--FluidPhaseType:1.0.0`](./reference-data/FluidPhaseType.1.0.0.md)
        11. [`osdu:wks:reference-data--GeologicUnitShapeType:1.0.0`](./reference-data/GeologicUnitShapeType.1.0.0.md)
        12. [`osdu:wks:reference-data--IndexableElement:1.0.0`](./reference-data/IndexableElement.1.0.0.md)
        13. [`osdu:wks:reference-data--KDirectionType:1.0.0`](./reference-data/KDirectionType.1.0.0.md)
        14. [`osdu:wks:reference-data--OrderingCriteriaType:1.0.0`](./reference-data/OrderingCriteriaType.1.0.0.md)
        15. [`osdu:wks:reference-data--PillarShapeType:1.0.0`](./reference-data/PillarShapeType.1.0.0.md)
        16. [`osdu:wks:reference-data--RepresentationRole:1.0.0`](./reference-data/RepresentationRole.1.0.0.md)
        17. [`osdu:wks:reference-data--RepresentationType:1.0.0`](./reference-data/RepresentationType.1.0.0.md)
        18. [`osdu:wks:reference-data--SequenceStratigraphicSchemaType:1.0.0`](./reference-data/SequenceStratigraphicSchemaType.1.0.0.md)
        19. [`osdu:wks:reference-data--SequenceStratigraphySurfaceType:1.0.0`](./reference-data/SequenceStratigraphySurfaceType.1.0.0.md)
        20. [`osdu:wks:reference-data--StratigraphicColumnValidityAreaType:1.0.0`](./reference-data/StratigraphicColumnValidityAreaType.1.0.0.md)
        21. [`osdu:wks:reference-data--ValueChainStatusType:1.0.0`](./reference-data/ValueChainStatusType.1.0.0.md)
    4. `work-product-component`
        1. [`osdu:wks:work-product-component--ColumnBasedTable:1.0.0`](./work-product-component/ColumnBasedTable.1.0.0.md)
        2. [`osdu:wks:work-product-component--EarthModelInterpretation:1.0.0`](./work-product-component/EarthModelInterpretation.1.0.0.md)
        3. [`osdu:wks:work-product-component--FaultInterpretation:1.0.0`](./work-product-component/FaultInterpretation.1.0.0.md)
        4. [`osdu:wks:work-product-component--GenericProperty:1.0.0`](./work-product-component/GenericProperty.1.0.0.md)
        5. [`osdu:wks:work-product-component--GenericRepresentation:1.0.0`](./work-product-component/GenericRepresentation.1.0.0.md)
        6. [`osdu:wks:work-product-component--GeobodyBoundaryInterpretation:1.0.0`](./work-product-component/GeobodyBoundaryInterpretation.1.0.0.md)
        7. [`osdu:wks:work-product-component--GeobodyInterpretation:1.0.0`](./work-product-component/GeobodyInterpretation.1.0.0.md)
        8. [`osdu:wks:work-product-component--GeologicUnitOccurrenceInterpretation:1.0.0`](./work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md)
        9. [`osdu:wks:work-product-component--GpGridRepresentation:1.0.0`](./work-product-component/GpGridRepresentation.1.0.0.md)
        10. [`osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0`](./work-product-component/GridConnectionSetRepresentation.1.0.0.md)
        11. [`osdu:wks:work-product-component--HorizonInterpretation:1.0.0`](./work-product-component/HorizonInterpretation.1.0.0.md)
        12. [`osdu:wks:work-product-component--IjkGridRepresentation:1.0.0`](./work-product-component/IjkGridRepresentation.1.0.0.md)
        13. [`osdu:wks:work-product-component--LocalBoundaryFeature:1.0.0`](./work-product-component/LocalBoundaryFeature.1.0.0.md)
        14. [`osdu:wks:work-product-component--LocalModelCompoundCrs:1.0.0`](./work-product-component/LocalModelCompoundCrs.1.0.0.md)
        15. [`osdu:wks:work-product-component--LocalModelFeature:1.0.0`](./work-product-component/LocalModelFeature.1.0.0.md)
        16. [`osdu:wks:work-product-component--LocalRockVolumeFeature:1.0.0`](./work-product-component/LocalRockVolumeFeature.1.0.0.md)
        17. [`osdu:wks:work-product-component--RockFluidOrganizationInterpretation:1.0.0`](./work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)
        18. [`osdu:wks:work-product-component--RockFluidUnitInterpretation:1.0.0`](./work-product-component/RockFluidUnitInterpretation.1.0.0.md)
        19. [`osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0`](./work-product-component/SealedSurfaceFramework.1.0.0.md)
        20. [`osdu:wks:work-product-component--StratigraphicColumn:1.0.0`](./work-product-component/StratigraphicColumn.1.0.0.md)
        21. [`osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.0.0`](./work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md)
        22. [`osdu:wks:work-product-component--StratigraphicUnitInterpretation:1.0.0`](./work-product-component/StratigraphicUnitInterpretation.1.0.0.md)
        23. [`osdu:wks:work-product-component--StructuralOrganizationInterpretation:1.0.0`](./work-product-component/StructuralOrganizationInterpretation.1.0.0.md)
        24. [`osdu:wks:work-product-component--SubRepresentation:1.0.0`](./work-product-component/SubRepresentation.1.0.0.md)
        25. [`osdu:wks:work-product-component--TimeSeries:1.0.0`](./work-product-component/TimeSeries.1.0.0.md)
        26. [`osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0`](./work-product-component/UnsealedSurfaceFramework.1.0.0.md)
        27. [`osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0`](./work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)
        28. [`osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0`](./work-product-component/UnstructuredGridRepresentation.1.0.0.md)

---

# Snapshot 2022-01-14 (towards M11

Detailed report of changes _after_ [2022-01-14](./_ChangeReports/ChangeReport-2022-01-14.md).

1. **Earth Modeling/Reservoir**
    1. Minor changes to property descriptions
       of [`osdu:wks:work-product-component--ColumnBasedTable:1.0.0`](./work-product-component/ColumnBasedTable.1.0.0.md)
    2. **Schema Change for Earth Modeling/Reservoir**<br>
       Allow a representation to reference an AquiferInterpretation which is important in case of
       IjkGridNumericalAquiferRepresentation. The change has no impact on existing representations because the newly
       permitted type does not exist in M10.
        1. `abstract`
            1. [`osdu:wks:AbstractRepresentation:1.0.0`](./abstract/AbstractRepresentation.1.0.0.md)
    3. **New Schemas for Earth Modeling/Reservoir**<br>
       Adds support for aquifer. For now, it only allows aquifer to be defined as a numerical one on an IJK Grid
       Representation.
        1. `abstract`
            1. [`osdu:wks:AbstractIjkGridFlowSimulationBoundaryConnection:1.0.0`](./abstract/AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.md)
        2. `reference-data`
            1. [`osdu:wks:reference-data--IjkCellFace:1.0.0`](./reference-data/IjkCellFace.1.0.0.md)
        3. `work-product-component`
            1. [`osdu:wks:work-product-component--AquiferInterpretation:1.0.0`](./work-product-component/AquiferInterpretation.1.0.0.md)
            2. [`osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0`](./work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)
2. **Organisation**
    1. A new minor version of Organisation is created to support hierarchical organisation. `Organisation:1.0.0` only
       supports flat organisations. With the self-reference `ParentOrganisationID` in `Organisation:1.1.0` it becomes
       possible to express multi-level hierarchical organisation structures.
        1. `master-data`
            1. [`osdu:wks:master-data--Organisation:1.1.0`](./master-data/Organisation.1.1.0.md)
3. **Drilling Execution**
    1. Minor correction to the `data.Fluid[]` object description,
       see [`osdu:wks:master-data--FluidsReport:1.1.0`](./master-data/FluidsReport.1.1.0.md).
    2. A new minor version of FluidsReport is created to add optional property `data.Fluid[].BrandName`,
       see [`osdu:wks:master-data--FluidsReport:1.1.0`](./master-data/FluidsReport.1.1.0.md).
       [Migration Notes](../Guides/MigrationGuides/M11/FluidsReport.1.0.0.md).
        1. `master-data`
            1. [`osdu:wks:master-data--FluidsReport:1.1.0`](./master-data/FluidsReport.1.1.0.md)
    4. Mark numeric properties subject to Frame of Reference treatment with `x-osdu-frame-of-reference`
       for `osdu:wks:master-data--BHARun:1.0.0`.
    5. Add data.VirtualProperties.DefaultName priority for `osdu:wks:master-data--FluidsReport:1.0.0`,
       `osdu:wks:master-data--FluidsReport:1.1.0`, `osdu:wks:master-data--OperationsReport:1.0.0`.
4. **Reference Values**
    1. [`osdu:wks:reference-data--AzimuthReferenceType:1.0.0`](./reference-data/AzimuthReferenceType.1.0.0.md): Adjusted
       reference value catalog description to the PPDM definition. This is related to the PPDM synchronized content.
    2. [`osdu:wks.:reference-data--OperatingEnvironment:1.0.0`](./reference-data/OperatingEnvironment.1.0.0.md):
       Adjusted reference value catalog description to the PPDM definition. This is related to the PPDM synchronized
       content.
5. **Unit Mapping for Ingestion**<br>
   The following types are added to support selective unit symbol mappings to the Energistics UnitOfMeasure platform
   standard. More details in the [worked examples](../Examples/WorkedExamples/UnitOfMeasureLookUp/README.md) section.
    1. &rarr; [`osdu:wks:reference-data--CatalogMapStateType:1.0.0`](./reference-data/CatalogMapStateType.1.0.0.md) new
       FIXED reference data catalog for mapping quality states.
    2. &rarr; [`osdu:wks:reference-data--ExternalCatalogNamespace:1.0.0`](./reference-data/ExternalCatalogNamespace.1.0.0.md)
   bounded catalog context for the following two catalogs.
    3. &rarr; [`osdu:wks:reference-data--ExternalUnitOfMeasure:1.0.0`](./reference-data/ExternalUnitOfMeasure.1.0.0.md)
       mappings to platform standard UnitOfMeasure.
    4. &rarr; [`osdu:wks:reference-data--ExternalUnitQuantity:1.0.0`](./reference-data/ExternalUnitQuantity.1.0.0.md)
       mappings to platform standard UnitQuantity.
6. **Seismic to Earth Modelling Integration**<br>
   After M10 with all the new Earth Modelling types it became necessary to review the domains and establish optional
   relationships between the domains. See also [migration instructions for M11](../Guides/MigrationGuides/M11/README.md)
   . The following changes were done:
    1. Create a new version of
       SeismicHorizon, [`osdu:wks:work-product-component--SeismicHorizon:1.1.0`](./work-product-component/SeismicHorizon.1.1.0.md)
       which adds [AbstractRepresentation](./abstract/AbstractRepresentation.1.0.0.md) properties as well as
       representation `Type` and `Role` properties to make the seismic horizon interpretation representation equivalent
       to [GenericRepresentation](./work-product-component/GenericRepresentation.1.0.0.md).
       [Migration Notes](../Guides/MigrationGuides/M11/SeismicHorizon.1.0.0.md).
    2. Create a new version of
       FaultSystem [`osdu:wks:work-product-component--FaultSystem:1.1.0`](./work-product-component/FaultSystem.1.0.0.md).
       This version encompasses multiple representations. Each member of the `data.Faults[]` array can refer to a
       FaultInterpretation and declare representation `Type` and `Role`. 
       [Migration Notes](../Guides/MigrationGuides/M11/FaultSystem.1.0.0.md).
    3. For full symmetry with SeismicHorizon a
       new  [`osdu:wks:work-product-component--SeismicFault:1.0.0`](./work-product-component/SeismicFault.1.0.0.md) was
       created.
    4. The new version
       of [`osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0`](./work-product-component/UnsealedSurfaceFramework.1.1.0.md)
       has been extended to relate to a FaultSystemID or SeismicFaultIDs - only one of the properties should be
       populated. [Migration Notes](../Guides/MigrationGuides/M11/UnsealedSurfaceFramework.1.0.0.md).
7. **Wellbore to Earth Modelling Integration**<br>
   Similar to the **_Seismic to Earth Modelling Integration_** above, here the result of the activity to link from the
   wellbore domain to "interpretations", particularly to HorizonInterpretation and StratigraphicUnitInterpretation. 
    1. abstract schema fragment
       1. [`osdu:wks:AbstractColumnBasedTable:1.0.0`](./abstract/AbstractColumnBasedTable.1.0.0.md) used by WellboreIntervalSet. Description: _The ColumnBasedTable is a set of columns, which have equal length (data.ColumnSize) included by types carrying embedded table properties. Columns have a Property Kind, UnitOfMeasure and Facet. There are KeyColumns (index columns) and Columns (for look-up values). Examples are KrPc, PVT and Facies tables._
    2. work-product-component
       1. [`osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0`](./work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md) - extending to [`ChronoStratigraphy`](./reference-data/ChronoStratigraphy.1.0.0.md) 
       2. [`osdu:wks:work-product-component--WellboreIntervalSet:1.0.0`](./work-product-component/WellboreIntervalSet.1.0.0.md) entirely new type with relationships to earth modelling domain.
       3. [`osdu:wks:work-product-component--WellboreMarkerSet:1.2.0`](./work-product-component/WellboreMarkerSet.1.2.0.md) new minor version with relationships to the earth modelling domain.
8. **DataQuality**<br>Indexing behaviour changes:
    1. The indexing behaviour of `work-product-component--DataQuality` is changed such that the
       arrays `data.BusinessRules.DataRuleSets[]`
       and `data.BusinessRules.DataRules[]` are now indexed as `nested` to enable queries with logical operators.
    2. The indexing behaviour of `reference-data--QualityDataRuleSet` is changed such that `data.DateRules` become
       searchable (`flattened` indexing).
    > **Attention:** Re-indexing required!
9. **WellLog**<br>
   A new version of WellLog is created to be able to support including Log Remark, Curve Description, and different
   Curve Data Formats (from [LAS 3.0 File Structure Specifications](https://www.cwls.org/wp-content/uploads/2014/09/LAS_3_File_Structure.pdf))
   inside the well log.
    1. `work-product-component`
        1. [`osdu:wks:work-product-component--WellLog:1.2.0`](./work-product-component/WellLog.1.2.0.md) with 
           [migration instructions here](../Guides/MigrationGuides/M11/WellLog.1.1.0.md).
    2. `reference-data`
        1. [`osdu:wks:reference-data--CurveSampleType:1.0.0`](./reference-data/CurveSampleType.1.0.0.md)
10. **Documentation**<br>
    The following [WorkedExamples](../Examples/WorkedExamples) folders have been renamed, dropping the explicit version
    name. Generally, the worked examples demonstrate the latest schema version. In the following list the previously
    used name is hyperlinked to the new location's README:
    1. [GeoReferencedImage.1.0.0](../Examples/WorkedExamples/GeoReferencedImage/README.md)
    2. [Organisation.1.1.0](../Examples/WorkedExamples/Organisation/README.md)
    3. [WellboreMarkerSet.1.1.0](../Examples/WorkedExamples/WellboreMarkerSet/README.md)
    4. [WellboreTrajectory.1.1.0](../Examples/WorkedExamples/WellboreTrajectory/README.md)
    5. [WellLog.1.1.0](../Examples/WorkedExamples/WellLog/README.md)

---

# Snapshot 2022-03-18 (towards M12)

Detailed report of changes _after_ [2022-03-18](./_ChangeReports/ChangeReport-2022-03-18.md).

1. **Cross-cutting Changes**<br>
   1. The use of `const` in schemas is problematic.<br>
      The `const` keyword is not supported in Open API Specification (OAS3). The indexer code expects a `type` keyword
      to figure out _what_ to index. This milestone adds a `type` keyword in addition to `const`, which is redundant
      from a schema perspective, however, compatible with non-breaking schema change if the `const` keyword stays. The
      affected schema fragments are
      * [AbstractGeoFieldContext](../Generated/abstract/AbstractGeoFieldContext.1.0.0.json#L22) and 
      * [AbstractMetaItem](../Generated/abstract/AbstractMetaItem.1.0.0.json#L15) in four different places.
   2. New Keys properties in [AbstractActivityParameter](abstract/AbstractActivityParameter.1.0.0.md):
      1. `IntegerParameterKey`: Integer value from "ParameterKey" parameter, associated with this parameter. Example: {"ParameterKey": "index", "StringParameterKey: 2}.
      2. `StringParameterKey`: String value from "ParameterKey" parameter, associated with this parameter. Can be used to associate with parameter values of type string or data quantity. In the later case, the string representation of the quantity value will be used. Example: {"ParameterKey": "facies", "StringParameterKey: "shale"}, {"ParameterKey":"depth", "StringParameterKey":"1545.43m"}
   3. `AbstractFacilityVerticalMeasurement`<br> This schema fragment is included as part of the `data.VerticalMeasurements[]` array in Well and Wellbore, as well as in numerous work-product-component and mater-data types, which hold vertical measurements/references. 
       1. In order to support **_shared_** vertical references via the `VerticalReferenceID` property,
          a `VerticalReferenceEntityID` has been added to declare the record where the `VerticalReferenceID` is to be
          looked up. For further details
          1. see the fragment documentation [AbstractFacilityVerticalMeasurement](abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)
          2. see the [migration guide](../Guides/MigrationGuides/M12/AbstractFacilityVerticalMeasurement.1.0.0.md),
          3. see [Schema Usage Guide](../Guides/Chapters/03-VerticalMeasurements.md) with updated recommendations. 
   4. Revision of **Technical Assurance**<br>It is paramount for the data platform to capture context for further usage
      of data. Originally `data.TechnicalAssuranceID` was added for that purpose in `AbstractCommonResources`. This
      provides insufficient details whether a particular data item is accepted for usage or not. Therefore, the
      TechnicalAssuranceID in `AbstractCommonResources` was deprecated. Instead:
       1. The new [AbstractTechnicalAssurance](abstract/AbstractTechnicalAssurance.1.0.0.md) was created, which combines
          the technical assurance classification with 'acceptable usage' and 'unacceptable usage', including review documentation.
       2. [AbstractWPCGroupType](abstract/AbstractWPCGroupType.1.0.0.md) is then extended to include an array
          of `AbstractTechnicalAssurance` to provide the usage guidance and context.
       3. Considering that 'mastering' master data is different, only a single reference TechnicalAssuranceTypeID is added to [AbstractMaster](abstract/AbstractMaster.1.1.0.md).
       4. Finally, two reference value types are added. The governance for M12 is set to LOCAL because of the lists just
          have example values and no comprehensive list of values. The intended governance for the two reference value
          lists is OPEN to enable sharing accepted and unaccepted usage for workflows and user roles. The two lists are:
          1. [WorkflowUsageType](reference-data/WorkflowUsageType.1.0.0.md) Describes the workflows that the technical
             assurance value is valid or not valid for.
          2. [WorkflowPersonaType](reference-data/WorkflowPersonaType.1.0.0.md) Name of the role or personas that the
             record is technical assurance value is valid or not valid for.
2. **Earth Modeling/Reservoir**
   1. Added support for 'faulted reservoirs' by introducing reservoir compartments and voidage groups. More details in
      the [Worked Examples section](../Examples/WorkedExamples/Reservoir%20Data/ReservoirCompartments/README.md). The
      following new types are introduced:
      1. [`osdu:wks:work-product-component--ReservoirCompartmentInterpretation:1.0.0`](../E-R/work-product-component/ReservoirCompartmentInterpretation.1.0.0.md): defining compartments, which link `StratigraphicUnitInterpretation`s to `RockFluidUnitInterpretation` 'members'.
      2. [`osdu:wks:work-product-component--VoidageGroupInterpretation:1.0.0`](../E-R/work-product-component/VoidageGroupInterpretation.1.0.0.md): a collection of `ReservoirCompartmentInterpretation`, `RockFluidOrganizationInterpretation` and `GeologicUnitOrganizationInterpretationIDs`.
   2. **ETPDataspace**
      1. [osdu:wks:dataset--ETPDataspace:1.0.0](../E-R/dataset/ETPDataspace.1.0.0.md) describes the location and
         attributes of a Dataspace accessible through ETP (Energistics Transfer Protocol) APIs, for ETP 1.2 and above.
3. **Geodetic Catalog**<br>
   New versions for CoordinateReferenceSystem and CoordinateTransformation are created to add missing extent description
   and bounding box Wgs84Coordinates GeoJSON to enable spatial discovery.
   1. [`osdu:wks:reference-data--CoordinateReferenceSystem:1.1.0`](../E-R/reference-data/CoordinateReferenceSystem.1.1.0.md): [change description](../Guides/MigrationGuides/M12/CoordinateReferenceSystem.1.0.0.md).
   1. [`osdu:wks:reference-data--CoordinateTransformation:1.1.0`](../E-R/reference-data/CoordinateTransformation.1.1.0.md): [change description](../Guides/MigrationGuides/M12/CoordinateTransformation.1.0.0.md).
4. **UnitOfMeasure References** from `AbstractAnyCrsFeatureCollection`<br>
   The [ADR - Change of CRS Catalog and Convert Services to dynamically read CRS/CT reference data using Record Id](https://community.opengroup.org/osdu/platform/system/home/-/issues/94)
   plans to use the GeoJSON-like fragment `AbstractAnyCrsFeatureCollection` in the `/convertGeoJson` endpoint.
   The `AbstractAnyCrsFeatureCollection` was missing a relationship to UnitOfMeasure for the vertical unit if the data
   structure contains 3-dimensional data. Such a property, `VerticalUnitID` was added
   to [`AbstractAnyCrsFeatureCollection:1.1.0`](abstract/AbstractAnyCrsFeatureCollection.1.1.0.md). The new version is
   immediately adopted in the CRS Converter API (see ADR). For ordinary data records the adoption is more difficult due
   to the chaining of schema fragments. Example: an ordinary `master-data--Basin` includes `AbstractMaster`, which
   includes `AbstractSpatialLocation`, which finally includes the `AbstractAnyCrsFeatureCollection`. We want to avoid
   unnecessary schema updates (incremental semantic version numbers). Instead, we plan **_opportunistic adoption_**,
   i.e. if a schema extension is carries out, which requires a type to get an incremented version number, the schema is
   then simultaneously changed to include the latest schema fragments.<br>
   This should not stop consumers/operators from starting to populate e.g.
   the `data.SpatialLocation.AsIngestedCoordinates.VerticalUnitID`. This extension is permitted due to the default
   setting for nested dictionaries `"additionalProperties": true`.<br>
   The following fragments are **_prepared_** to be used by opportunistic adoption:
    * [`AbstractAnyCrsFeatureCollection:1.1.0`](abstract/AbstractAnyCrsFeatureCollection.1.1.0.md) - the main cause for
      this change. It is included by:
      * [`AbstractSpatialLocation:1.1.0`](abstract/AbstractSpatialLocation.1.1.0.md), which is included by
        * [`AbstractMasterData:1.1.0`](abstract/AbstractMaster.1.1.0.md)
        * [`AbstractWorkProductComponent:1.1.0`](abstract/AbstractWorkProductComponent.1.1.0.md)
5. **DataQuality** schema changes<br>
   The previously defined Data Quality related schemas in OSDU were very basic, and therefore, have been updated to
   include certain additional attributes to make the Business rule creation, execution, and score generation workflows
   suitable for broader use cases.
   1. `abstract`
       1. [`osdu:wks:AbstractQualityMetric:1.1.0`](./abstract/AbstractQualityMetric.1.1.0.md) schema updated to
           add `data.QualityMetric.MetadataScore` new property to allow storing of the score in % for the evaluated OSDU
           data type record. [Migration Notes](../Guides/MigrationGuides/M12/AbstractQualityMetric.1.0.0.md).
   2. `reference-data`
       1. [`osdu:wks:reference-data--DataRuleDimensionType:1.0.0`](./reference-data/DataRuleDimensionType.1.0.0.md) -
          this is a new reference-data type to be able to assess quality dimensions of the data and clearly distinguish
          different Business rules, such as Accuracy, Availability, Completeness, Conformance, etc.
       2. [`osdu:wks:reference-data--QualityDataRule:1.1.0`](./reference-data/QualityDataRule.1.1.0.md) schema updated
          to support having DataRuleDimensionType associated with each Business rule. [Migration Notes](../Guides/MigrationGuides/M12/QualityDataRule.1.0.0.md).
       3. [`osdu:wks:reference-data--QualityDataRuleSet:2.0.0`](./reference-data/QualityDataRuleSet.2.0.0.md) schema
          updated to add [`osdu:wks:AbstractReferenceType:1.0.0`](./abstract/AbstractReferenceType.1.0.0.md) schema
          fragment, which was missing in
          previous [`osdu:wks:reference-data--QualityDataRuleSet:1.0.0`](./reference-data/QualityDataRuleSet.1.0.0.md).
          It also now supports specifying `data.EvaluatedKind` new property to be able to know
          which OSDU data type this rule-set applies to. [Migration Notes](../Guides/MigrationGuides/M12/QualityDataRuleSet.1.0.0.md).
   3. `work-product-component`
       1. [`osdu:wks:work-product-component--DataQuality:1.1.0`](./work-product-component/DataQuality.1.1.0.md). 
          [Migration Notes](../Guides/MigrationGuides/M12/DataQuality.1.0.0.md).
6. **Well Status and Classification V3 (PPDM)**
   1. **_Changed reference-data types_**
      1. [`osdu:wks:reference-data--FacilityStateType:1.0.0`](../E-R/reference-data/FacilityStateType.1.0.0.md) changed
         from LOCAL to OPEN governance and sourced from PPDM. No migration needed since all the codes remain only with
         PPDM quality
         descriptions.
      2. [`osdu:wks:reference-data--PlayType:1.0.0`](../E-R/reference-data/PlayType.1.0.0.md) changed from LOCAL to 
         OPEN governance and sourced from PPDM. [Migration notes](../Guides/MigrationGuides/M12/PlayType.1.0.0.md).
      3. [`osdu:wks:reference-data--WellboreTrajectoryType:1.0.0`](../E-R/reference-data/WellboreTrajectoryType.1.0.0.md), 
         now sourced from PPDM. [Migration notes](../Guides/MigrationGuides/M12/WellboreTrajectoryType.1.0.0.md).
      4. [`osdu:wks:reference-data--WellInterestType:1.0.0`](../E-R/reference-data/WellInterestType.1.0.0.md) changed 
         from LOCAL to OPEN governance and sourced from PPDM. 
         [Migration notes](../Guides/MigrationGuides/M12/WellInterestType.1.0.0.md).
   2. **_New Schema Versions_**
      1. [`osdu:wks:reference-data--WellBusinessIntention:1.0.0`](../E-R/reference-data/WellBusinessIntention.1.0.0.md), 
         a new reference-data type introduced by Well Status and Classification.
      2. [`osdu:wks:reference-data--WellBusinessIntentionOutcome:1.0.0`](../E-R/reference-data/WellBusinessIntentionOutcome.1.0.0.md), 
         a new reference-data type introduced by Well Status and Classification.
      3. [`osdu:wks:reference-data--WellCondition:1.0.0`](../E-R/reference-data/WellCondition.1.0.0.md), 
         a new reference-data type introduced by Well Status and Classification.
      4. [`osdu:wks:reference-data--WellProductType:1.0.0`](../E-R/reference-data/WellProductType.1.0.0.md), 
         superseding MaterialType, which is now deprecated due to previously mixed usage by well product and tubular 
         component types.
      5. [`osdu:wks:reference-data--WellRole:1.0.0`](../E-R/reference-data/WellRole.1.0.0.md), 
         a new reference-data type introduced by Well Status and Classification.
      6. [`osdu:wks:reference-data--WellStatusSummary:1.0.0`](../E-R/reference-data/WellStatusSummary.1.0.0.md), a new
         summary configuration, which allows custom combinations of standard Well Status and Classification states.
   3. **_Main master-data changes for Well and Wellbore_**
      1. [`osdu:wks:master-data--Well:1.1.0`](../E-R/master-data/Well.1.1.0.md) with new properties for Well Status
         and Classification V3 (PPDM). 
         * `data.VerticalMeasurements[]` got extended by an optional relationship to `Rig`.
      2. [`osdu:wks:master-data--Wellbore:1.1.0`](../E-R/master-data/Wellbore.1.1.0.md) with new properties for Well
         Status and Classification V3 (PPDM). 
         * `data.VerticalMeasurements[]` got extended by an optional relationship to `Rig`.
7. **Seismic Domain, VSP Acquisition** 
   * Extension of the [`osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0`](../E-R/master-data/SeismicAcquisitionSurvey.1.2.0.md) 
     to record the Wellbore relationships in the SourceConfigurations and ReceiverConfigurations, as well as the 
     depth range for sources and receivers. There is no migration effort expected because only new features are 
     enabled. Details in the [SeismicAcquisitionSurvey migration page](../Guides/MigrationGuides/M12/SeismicAcquisitionSurvey.1.1.0.md).
8. **ProcessedInSAR** provided by the Carbon Capture and Storage work-stream.<br>
   This milestone introduces a work-product-component for ProcessedInSAR measurements: "Interferometric Synthetic Aperture Radar (InSAR) is a representation of surface or ground deformation from satellite radar image data over time"
   1. [`osdu:wks:work-product-component--ProcessedInSAR:1.0.0`](../E-R/work-product-component/ProcessedInSAR.1.0.0.md) 
      as the main new entity type,<br>supported by the following reference value types:
   2. [`osdu:wks:reference-data--InSARApplication:1.0.0`](../E-R/reference-data/InSARApplication.1.0.0.md) (LOCAL governance)
   3. [`osdu:wks:reference-data--InSARFrequencyBand:1.0.0`](../E-R/reference-data/InSARFrequencyBand.1.0.0.md) (FIXED governance)
   4. [`osdu:wks:reference-data--InSARImageMode:1.0.0`](../E-R/reference-data/InSARImageMode.1.0.0.md) (LOCAL governance)
   5. [`osdu:wks:reference-data--InSARPolarisation:1.0.0`](../E-R/reference-data/InSARPolarisation.1.0.0.md) (FIXED governance)
   6. [`osdu:wks:reference-data--InSARProcessingType:1.0.0`](../E-R/reference-data/InSARProcessingType.1.0.0.md) (OPEN governance)
   7. [`osdu:wks:reference-data--SatelliteGeometry:1.0.0`](../E-R/reference-data/SatelliteGeometry.1.0.0.md) (OPEN governance)
   8. [`osdu:wks:reference-data--SatelliteMission:1.0.0`](../E-R/reference-data/SatelliteMission.1.0.0.md) (LOCAL governance)
9. **Rock Sample Analysis**<br>
   This milestone introduces types to support first level of Coring, RockSample and RockSampleAnalysis, focussing on 
   supporting Routine Core Analysis (RCA). The following types are introduced:
   1. New **master-data** types
      1. [`osdu:wks:master-data--Coring:1.0.0`](./master-data/Coring.1.0.0.md) _The activity of acquiring a core from
         within a wellbore. The two predominant types are by drilling, or by sidewall. In drilling a cylindrical sample
         of rock is collected using a core bit in conjunction with a core barrel and core catcher. Sidewall cores are
         taken from the side of the borehole, usually by a wireline tool. Sidewall cores may be taken using percussion
         or mechanical drilling._
      2. [`osdu:wks:master-data--RockSample:1.0.0`](./master-data/RockSample.1.0.0.md) _A rock sample retrieved from an
         outcrop or Well. It can be core, sample cut from core, cutting, outcrop, slide etc_
      3. [`osdu:wks:master-data--StorageFacility:1.0.0`](./master-data/StorageFacility.1.0.0.md) _A generic storage
         facility for e.g., core and rock or fluid samples, seismic tapes, etc._
   2. New work-product-component type
       1. [`osdu:wks:work-product-component--RockSampleAnalysis:1.0.0`](./work-product-component/RockSampleAnalysis.1.0.0.md) 
          _The meta-data about a rock sample analysis related to core or outcrop rock samples._
   3. New reference-data types
       1. [`osdu:wks:reference-data--ConventionalCoreType:1.0.0`](./reference-data/ConventionalCoreType.1.0.0.md) _A
          reference value type for Conventional Cores._
       2. [`osdu:wks:reference-data--CorePreservationType:1.0.0`](./reference-data/CorePreservationType.1.0.0.md) _The
          core preservation classification type, such as Wax-sealed, Resin-coated, Cling-wrap, etc._
       3. [`osdu:wks:reference-data--GrainDensityMeasurementType:1.0.0`](./reference-data/GrainDensityMeasurementType.1.0.0.md) 
          _The kind of grain density measurement, which is applied in, e.g. routine core analysis._
       4. [`osdu:wks:reference-data--PermeabilityMeasurementType:1.0.0`](./reference-data/PermeabilityMeasurementType.1.0.0.md) 
          _The kind of permeability measurement applied in, e.g., routine core analysis._
       5. [`osdu:wks:reference-data--PorosityMeasurementType:1.0.0`](./reference-data/PorosityMeasurementType.1.0.0.md)
          _The kind of porosity measurement applied in, e.g., routine core analysis._
       6. [`osdu:wks:reference-data--PressureMeasurementType:1.0.0`](./reference-data/PressureMeasurementType.1.0.0.md) 
          _The enumeration for PressureMeasurement types like Ambient or Overburden._
       7. [`osdu:wks:reference-data--RockSampleType:1.0.0`](./reference-data/RockSampleType.1.0.0.md) _The classification
          of rock samples._
       8. [`osdu:wks:reference-data--SampleOrientationType:1.0.0`](./reference-data/SampleOrientationType.1.0.0.md) _The
          sample orientation direction, which describes the core sample's relationship to the bedding or drilling
          direction._
       9. [`osdu:wks:reference-data--SaturationMethodType:1.0.0`](./reference-data/SaturationMethodType.1.0.0.md) _The
          method used to obtain the saturation measurement values, e.g., Dean Stark, Retort, Karl Fischer._
       10. [`osdu:wks:reference-data--SidewallCoreType:1.0.0`](./reference-data/SidewallCoreType.1.0.0.md) _A reference
           value type for Sidewall Cores._
   4. New schema fragment
      1. [`osdu:wks:AbstractStorageLocation:1.0.0`](./abstract/AbstractStorageLocation.1.0.0.md) _A record about the
           storage location of an item, e.g. a rock or fluid sample, seismic tape, where (facility), by whom 
           (organisation), when (dates) and how (description)._

---

# Snapshot 2022-05-20 (towards M13)

Detailed report of changes _after_ [2022-05-20](./_ChangeReports/ChangeReport-2022-05-20.md).

1. **Reference Value Changes/Updates**
    1. `osdu:wks:reference-data--BitType:1.0.1` schema patch to update the entity type description to
       match PPDM's definition. PPDM reference values are also added, earlier OSDU values are deprecated with
       instructions to replacements in the
       respective [migration guide](../Guides/MigrationGuides/M13/BitType.1.0.0.md).
       Migration is voluntary unless PPDM compliance is required. See also
       [documentation](./reference-data/BitType.1.0.1.md).
    2. `osdu:wks:reference-data--OperatingEnvironment:1.0.0` content is updated (no schema changes). The updated
       reference value manifest contains more alias definitions, which were earlier lumped together. See
       [documentation](./reference-data/OperatingEnvironment.1.0.0.md).
    3. `osdu:wks:reference-data--SchemaFormatType:1.0.0` content is augmented with new values to support SEG-Y and SEG-D
       formats.
    4. `osdu:wks:reference-data--SeismicMigrationType:1.0.1` schema patch to update the entity type description to
       match PPDM's definition. PPDM reference values are also added, earlier OSDU values are deprecated with
       instructions to replacements in the
       respective [migration guide](../Guides/MigrationGuides/M13/SeismicMigrationType.1.0.0.md).
       Migration is voluntary unless PPDM compliance is required. See also
       [documentation](./reference-data/SeismicMigrationType.1.0.1.md).
    5. `osdu:wks:reference-data--SeismicPickingType:1.0.1` schema patch to update the entity type description to
       match PPDM's definition. PPDM reference values are also added, earlier OSDU values are deprecated with
       instructions to replacements in the
       respective [migration guide](../Guides/MigrationGuides/M13/SeismicPickingType.1.0.0.md).
       Migration is voluntary unless PPDM compliance is required. See also
       [documentation](./reference-data/SeismicPickingType.1.0.1.md).
    6. `osdu:wks:reference-data--SeismicWaveType:1.0.1` schema patch to update the entity type description to
       match PPDM's definition. PPDM reference values are also added, earlier OSDU values are deprecated with
       instructions to replacements in the
       respective [migration guide](../Guides/MigrationGuides/M13/SeismicWaveType.1.0.0.md).
       Migration is voluntary unless PPDM compliance is required. See also
       [documentation](./reference-data/SeismicWaveType.1.0.1.md).
    7. `osdu:wks:reference-data--VerticalMeasurementType:1.0.1` schema patch to update the entity type description to
       match PPDM's definition. PPDM reference values are also added, earlier OSDU values are deprecated with
       instructions to replacements in the
       respective [migration guide](../Guides/MigrationGuides/M13/VerticalMeasurementType.1.0.0.md).
       Migration is voluntary unless PPDM compliance is required. See also
       [documentation](./reference-data/VerticalMeasurementType.1.0.1.md).

2. **Seismic Domain**
    1. [`osdu:wks:work-product-component--SeismicTraceData:1.2.0`](./work-product-component/SeismicTraceData.1.2.0.md)
        1. Additional property `data.TimeLapse`, which allows an association to
           a [TimeSeries](./work-product-component/TimeSeries.1.0.0.md) record, which holds the different times (or
           rather dates) and sequence of 4D seismic.<br>Migration
           instructions [are provided](../Guides/MigrationGuides/M13/SeismicTraceData.1.1.0.md) but only needed in cases
           of SeismicTraceData being part of a time-lapse series.
        2. [WorkedExamples](../Examples/WorkedExamples/Seismic4D/README.md) demonstrating the combined use of
           activities, seismic projects and seismic trace data.
    2. 2D Support for FaultSystem and VelocityModeling:
        1. [`osdu:wks:work-product-component--FaultSystem:1.2.0`](./work-product-component/FaultSystem.1.2.0.md) got
           augmented by relationships to `SeismicLineGeometries` for the 2D line navigation
           context. [Migration notes](../Guides/MigrationGuides/M13/FaultSystem.1.1.0.md).
        2. [`osdu:wks:work-product-component--VelocityModeling:1.2.0`](./work-product-component/VelocityModeling.1.1.0.md) got
           augmented by relationships to `SeismicLineGeometries` for the 2D line navigation
           context. [Migration notes](../Guides/MigrationGuides/M13/VelocityModeling.1.0.0.md).
    3. `BinGridID` relationship added to [`osdu:wks:master-data--SeismicProcessingProject:1.2.0`](./master-data/SeismicProcessingProject.1.2.0.md) 
        with [migrations instructions](../Guides/MigrationGuides/M13/SeismicProcessingProject.1.1.0.md).
3. **External Data Sources, also known as Connected Data Sources**<br>
   Adding the required entities to support connected, external data sources. The way the connection to external data
   source is expected to work and how work-product-component records will refer to connected data sources is described
   in the [WorkedExamples/ExternalDataServices](../Examples/WorkedExamples/ExternalDataServices/README.md).
    1. New master-data entities:
        1. [`osdu:wks:master-data--ConnectedSourceDataJob:1.0.0`](master-data/ConnectedSourceDataJob.1.0.0.md):
           Scheduling, data fetch, and ingestion configuration for automated jobs against a registered connected data
           source.
        2. [`osdu:wks:master-data--ConnectedSourceRegistryEntry:1.0.0`](master-data/ConnectedSourceRegistryEntry.1.0.0.md):
           System-level object containing business and technical metadata for an external OSDU-compliant data source,
           used for registration of that source within the OSDU external data framework.
    2. New dataset:
        1. [`osdu:wks:dataset--ConnectedSource.Generic:1.0.0`](dataset/ConnectedSource.Generic.1.0.0.md): The dataset
           linked to a connected source. This schema may act as a template for more specialized dataset implementations
           requiring more context to the connected source.
    3. New reference-data entities:
        1. [`osdu:wks:reference-data--OAuth2FlowType:1.0.0`](reference-data/OAuth2FlowType.1.0.0.md): Flows, or grants,
           supported within the OAuth 2.0 authorization framework.
        2. [`osdu:wks:reference-data--SecuritySchemeType:1.0.0`](reference-data/SecuritySchemeType.1.0.0.md): A type of
           security scheme, such as OAuth2, used to generate access credentials to a resource.
4. **Perforation Types**<br>
   Extending support to **PerforationJob** and **PerforationInterval** and a large number of reference value types:
    1. New master-data entity types:
        1. [`osdu:wks:master-data--PerforationJob:1.0.0`](./master-data/PerforationJob.1.0.0.md): A Perforation Job
           describes an activity performed on a well for the purpose of firing perforation guns or puncher inside
           Casing, Liner or Tubing to create a flow path between the reservoir outside the pipe and the production
           annulus within the pipe. A Squeeze job is a remedial effort performed to create a temporary flow path to
           allow cement to be pumped outside the casing to repair the annular seal. Perf Intervals result in the
           creation of equivalent Wellbore Openings where they are managed as part of the Completion. A Squeeze job
           would not result in creation of Wellbore Openings as these are temporary to better seal that interval.
        2. [`osdu:wks:master-data--PerforationInterval:1.0.0`](./master-data/PerforationInterval.1.0.0.md): A
           Perforation Interval is associated to a Perforation
           Job. An interval is the Top & Base depth range that a specific Perforating Gun suite was fired in unison to
           produce Openings. An interval should be created for each Gun suite fired. A interval should also be created
           for a specific Tubing Punch firing also.
    2. New reference-data entity-types:
        1. [`osdu:wks:reference-data--AnnularFluidType:1.0.0`](./reference-data/AnnularFluidType.1.0.0.md): Type of
           fluid in wellbore at time of perforation job.
        2. [`osdu:wks:reference-data--BottomHolePressureType:1.0.0`](./reference-data/BottomHolePressureType.1.0.0.md):
           The Method used to calculate or measure bottomhole pressure during perforation job.
        3. [`osdu:wks:reference-data--PerforationCentralizationMethodType:1.0.0`](./reference-data/PerforationCentralizationMethodType.1.0.0.md): 
           The Perforating Gun Centralization Method Type used in a perforation interval.
        4. [`osdu:wks:reference-data--PerforationConveyedMethod:1.0.0`](./reference-data/PerforationConveyedMethod.1.0.0.md): 
           Equipment type used to run perforation equipment downhole.
        5. [`osdu:wks:reference-data--PerforationGunCarrierCategory:1.0.0`](./reference-data/PerforationGunCarrierCategory.1.0.0.md): 
           The Perforating Gun Category used in a perforation interval.
        6. [`osdu:wks:reference-data--PerforationGunCarrierModel:1.0.0`](./reference-data/PerforationGunCarrierModel.1.0.0.md):
           The Perforating Gun Carrier Model used in a perforation interval.
        7. [`osdu:wks:reference-data--PerforationGunCarrierType:1.0.0`](./reference-data/PerforationGunCarrierType.1.0.0.md):
           The Perforating Gun Carrier Type  used in a perforation interval.
        8. [`osdu:wks:reference-data--PerforationGunChargeShape:1.0.0`](./reference-data/PerforationGunChargeShape.1.0.0.md):
           The Perforating Gun Charge Shape used in a perforation interval.
        9. [`osdu:wks:reference-data--PerforationGunChargeSize:1.0.0`](./reference-data/PerforationGunChargeSize.1.0.0.md):
           The Perforating Gun Charge Size used in a perforation interval. The value is a number as string, always
           measured in grams (g = 0.001 kg).
        10. [`osdu:wks:reference-data--PerforationGunChargeType:1.0.0`](./reference-data/PerforationGunChargeType.1.0.0.md):
            The Perforating Gun Charge Type used in a perforation interval.
        11. [`osdu:wks:reference-data--PerforationGunFiringHeadType:1.0.0`](./reference-data/PerforationGunFiringHeadType.1.0.0.md):
            The Perforating Gun Firing Head Type used in a perforation interval.
        12. [`osdu:wks:reference-data--PerforationGunMetallurgyType:1.0.0`](./reference-data/PerforationGunMetallurgyType.1.0.0.md):
            The Perforating Gun Metallurgy Type used in a perforation interval.
        13. [`osdu:wks:reference-data--PerforationGunPhasingType:1.0.0`](./reference-data/PerforationGunPhasingType.1.0.0.md):
            The Perforating Gun Phasing (the angle between perforations), always measured in degrees of arc.
        14. [`osdu:wks:reference-data--PerforationIntervalReason:1.0.0`](./reference-data/PerforationIntervalReason.1.0.0.md):
            Reason why the perforation interval was constructed.
        15. [`osdu:wks:reference-data--PerforationIntervalType:1.0.0`](./reference-data/PerforationIntervalType.1.0.0.md):
            The method type used during the perforation of an interval.
        16. [`osdu:wks:reference-data--PerforationIntervalWLSize:1.0.0`](./reference-data/PerforationIntervalWLSize.1.0.0.md):
            Wireline Diameter as string value, always measured in inches  (=0.0254 m).
        17. [`osdu:wks:reference-data--PerforationPillType:1.0.0`](./reference-data/PerforationPillType.1.0.0.md):
            Liquid pill pumped to optimise perforation job, e.g., debris prevention.


# Snapshot 2022-07-22 (towards M14)

Detailed report of changes _after_ [2022-07-22](./_ChangeReports/ChangeReport-2022-07-22.md).

1. **Reference Value Changes/Updates**
    1. [`osdu:wks:reference-data--SeismicProcessingStageType:1.0.1`](./reference-data/SeismicProcessingStageType.1.0.1.md) 
       schema patch to update the entity type description to
       match PPDM's definition. PPDM reference values are also added, earlier OSDU values are deprecated with
       instructions to replacements in the
       respective [migration guide](../Guides/MigrationGuides/M14/SeismicProcessingStageType.1.0.0.md).
       Migration is voluntary unless PPDM compliance is required. See also
       [documentation](./reference-data/SeismicProcessingStageType.1.0.1.md).
    2. [`osdu:wks:reference-data--VerticalMeasurementType:1.0.2`](./reference-data/VerticalMeasurementType.1.0.2.md) -
       new description by PPDM required a new patch version. No value changes.
2. **Activity**
   1. With the new reference value [`osdu:wks:reference-data--ActivityStatus:1.0.0`](./reference-data/ActivityStatus.1.0.0.md)
      the following fragments have been updated to include a new property `ActivityStatusID`:
       1. [`osdu:wks:AbstractActivityState:1.0.0`](./abstract/AbstractActivityState.1.0.0.md), a member of an
          `ActivityStates[]` array for activity state history and the `LastActivityState` in the following two fragments:
       2. [`osdu:wks:AbstractProjectActivity:1.1.0`](./abstract/AbstractProjectActivity.1.1.0.md) and
       3. [`osdu:wks:AbstractWPCActivity:1.1.0`](./abstract/AbstractWPCActivity.1.1.0.md)
   2. [`osdu:wks:work-product-component--Activity:1.1.0`](./work-product-component/Activity.1.1.0.md) was added
      including the [`osdu:wks:AbstractWPCActivity:1.1.0`](./abstract/AbstractWPCActivity.1.1.0.md). See also
      the [migration guide](../Guides/MigrationGuides/M14/Activity.1.0.0.md).
   3. The master-data types including [`osdu:wks:AbstractProjectActivity:1.0.0`](./abstract/AbstractProjectActivity.1.0.0.md) have
      **_not_** been updated yet.
3. **External Data Sources, also known as Connected Data Sources**<br>
    * The [`osdu:wks:master-data--ConnectedSourceRegistryEntry:1.1.0`](./master-data/ConnectedSourceRegistryEntry.1.1.0.md) 
      is augmented to contain `SmtpSchemes[]` for the
      email notifications and `AirflowStableAPIUrl`. The `ClientID` is deprecated in favor of `ClientIDKeyName`.
      The temporary storage `ReferenceValueMappings` is added as object without search support.  
        * [Documentation](./master-data/ConnectedSourceRegistryEntry.1.1.0.md)
        * [Migration guide](../Guides/MigrationGuides/M14/ConnectedSourceRegistryEntry.1.0.0.md).
    * The [`osdu:wks:master-data--ConnectedSourceDataJob:1.1.0`](./master-data/ConnectedSourceDataJob.1.1.0.md)  is 
      augmented with a temporary list of `FailedRecords[]` capturing the record ids, which failed synchronization. This 
      list may be kept externally in future releases. No [migration](../Guides/MigrationGuides/M14/ConnectedSourceDataJob.1.0.0.md) required.
4. **RockSampleAnalysis Enhancements**<br>
   M14 extends support to cover GrainSizeAnalysis in addition to the previously supported RoutineCoreAnalysis. This
   introduces a new version of
   * [`osdu:wks:work-product-component--RockSampleAnalysis:1.1.0`](./work-product-component/RockSampleAnalysis.1.1.0.md), 
     which has the following [migration instructions ](../Guides/MigrationGuides/M14/RockSampleAnalysis.1.0.0.md)
     (defaulting `data.AnalysisTypeIDs[]`),
     <br>and the following **_new_** reference value lists:
   * [`osdu:wks:reference-data--GrainSizeAnalysisMethod:1.0.0`](./reference-data/GrainSizeAnalysisMethod.1.0.0.md) _A
     reference-value record describing a GrainSizeAnalysis method._
   * [`osdu:wks:reference-data--GrainSizeClassification:1.0.0`](./reference-data/GrainSizeClassification.1.0.0.md) _The
     textual label of a grain size classification interval. GrainSizeClassification codes are referred to by
     GrainSizeClassificationSchemes, which will associate an actual grain size range and optionally ASTM sieve number to
     a GrainSizeClassification._
   * [`osdu:wks:reference-data--GrainSizeClassificationScheme:1.0.0`](./reference-data/GrainSizeClassificationScheme.1.0.0.md) _
     GrainSizeClassificationScheme contains a table of particle size to GrainSizeClassification associations as well as
     optionally ASTM sieve numbers._
   * [`osdu:wks:reference-data--RockSampleAnalysisType:1.0.0`](./reference-data/RockSampleAnalysisType.1.0.0.md) _A
     reference value catalog containing the names of the nested analysis objects in the
     work-product-component--RockSampleAnalysis. Examples are RoutineCoreAnalysis, GrainSizeAnalysis. Using these values
     it will be possible to query for RockSampleAnalysis records containing a specific kind of analysis. This list is
     fixed and governed by OSDU._
5. **Seismic Domain**
   * Improved interoperability by new FIXED governance reference value type and list:
     1. [SeismicPhase](./reference-data/SeismicPhase.1.0.0.md), and
     2. [SeismicPolarity](./reference-data/SeismicPolarity.1.0.0.md), which are used by a revised
     3. [osdu:wks:work-product-component--SeismicTraceData:1.3.0](./work-product-component/SeismicTraceData.1.3.0.md) with new reference value relationships. [Migration details here](../Guides/MigrationGuides/M14/SeismicTraceData.1.2.0.md).
     4. [osdu:wks:work-product-component--SeismicTraceData:1.0.0](./work-product-component/SeismicTraceData.1.0.0.md) was **_updated_** to meet the requirements of the indexer that changes to `x-osdu-indexing` are considered breaking changes. Unfortunately, the ProcessingParameter array was declared to be indexed as flattened structure, while in version 1.1.0 and higher the ProcessingParameter array is indexed just as object and not accessible via search queries. The 1.0.0 version is not changed in existing environments because of the `PUBLISHED` status. New environments will bootstrap the corrected version. 
   * Additional two reference values for [SeismicTraceSortOrder](./reference-data/SeismicTraceSortOrder.1.0.0.md) to support Crossline and Inline sort order for completeness.
6. **WellCompletions**<br>
   _PerforationIntervals_ (M13) are now 'connected' to the surrounding reservoir via **_WellboreOpening_** and
   inside the wellbore to the **_IsolatedInterval_**. The usage of the new types is explained in
   the [WorkedExamples/WellCompletions](../Examples/WorkedExamples/WellCompletions/README.md). The new types are:
    1. [`osdu:wks:master-data--IsolatedInterval:1.0.0`](../E-R/master-data/IsolatedInterval.1.0.0.md) _A
       hydraulic/pressure isolated interval in one or more Wellbores that functions to produce or inject fluids. The
       isolated interval must be capable of isolating a fluid flow for continuous measurement._
    2. [`osdu:wks:master-data--WellboreOpening:1.0.0`](../E-R/master-data/WellboreOpening.1.0.0.md) _A measured depth
       range within a Wellbore that is constructed to put the Wellbore annulus in contact with one or more stratigraphic
       zones for the purpose of injection, production or service. WellboreOpening interval ranges are always
       stratigraphically aligned, regardless of type._
    3. **_Experimental, preliminary_** [`osdu:wks:master-data--Reservoir:0.0.0`](../E-R/master-data/Reservoir.0.0.0.md)
       for the purpose of defining a "Reservoir" relationship target for `WellboreOpening`. This version 0.0.0 is
       expected to undergo potentially breaking changes until the first revision is published (Schema status
       DEVELOPMENT).
    4. **_Experimental,
       preliminary_** [`osdu:wks:master-data--ReservoirSegment:0.0.0`](../E-R/master-data/ReservoirSegment.0.0.0.md)
       for the purpose of defining a "ReservoirSegment" relationship target for `WellboreOpening`. This version 0.0.0 is
       expected to undergo potentially breaking changes until the first revision is published (Schema status
       DEVELOPMENT).
    5. [`osdu:wks:reference-data--IsolatedIntervalType:1.0.0`](../E-R/reference-data/IsolatedIntervalType.1.0.0.md)
       _The classification of an IsolatedInterval (aka. Completion). Values are CasedHole.FullString,
       CaseHole.ProductionLiner and OpenHole._
    6. [`osdu:wks:reference-data--WellboreOpeningStateType:1.0.0`](../E-R/reference-data/WellboreOpeningStateType.1.0.0.md)
       _Wellbore Opening State Type describes the physical state history of the Wellbore Opening that is significant for
       monitoring the completion and inform regulators and/or business stakeholders. Values are Open, Closed and
       Squeezed._
7. **Well Execution**
   1. New  [`osdu:wks:master-data--WellActivity:1.0.0`](./master-data/WellActivity.1.0.0.md) _A Well Activity is a operation performed on a well to accomplish a predetermined objective. This may be as short as a few hours in duration, or may last several days, weeks or even months. It will have one or more associated daily Operations Reports, and may have other associated data as well._
   2. Revised [`osdu:wks:master-data--OperationsReport:1.1.0`](./master-data/OperationsReport.1.1.0.md) _A standard report of all drilling, completion or well work events conducted on a well during a given period, usually a 24-hour day._ [Migration information](../Guides/MigrationGuides/M14/OperationsReport.1.0.0.md).
   3. Revised [`osdu:wks:master-data--ActivityPlan:1.1.0`](./master-data/ActivityPlan.1.1.0.md) _Information about a series of planned activities._ [Migration information](../Guides/MigrationGuides/M14/ActivityPlan.1.0.0.md).
   4. New Reference Value Types:
      1. [`osdu:wks:reference-data--WellActivityType:1.0.0`](./reference-data/WellActivityType.1.0.0.md)
      2. [`osdu:wks:reference-data--WellheadConnection:1.0.0`](./reference-data/WellheadConnection.1.0.0.md)
      3. [`osdu:wks:reference-data--WellTechnologyApplied:1.0.0`](./reference-data/WellTechnologyApplied.1.0.0.md)
8. **Patch Changes in Well Domain** 
   1. **WellboreMarkerSet** patch: [`osdu:wks:work-product-component--WellboreMarkerSet:1.2.1`](./work-product-component/WellboreMarkerSet.1.2.1.md) was patched with description string (`data.Markers[].Missing`), which was previously absent. In M14 v0.17.0 this schema contained breaking changes, which have been fixed in v0.17.1.
   2. **Wellbore** patch: the duplicate `data.WellboreTrajectoryTypeID` is deprecated in [`osdu:wks:master-data--Wellbore:1.1.1`](./master-data/Wellbore.1.1.1.md). Only the original `data.TrajectoryTypeID` (now with the PPDM description) should be used.



# Snapshot 2022-09-23 (towards M15)

Detailed report of changes _after_ [2022-09-23](./_ChangeReports/ChangeReport-2022-09-23.md).

The E-R diagrams for all the entity reports have been revised and split into two separate views: 

1. Given an entity or schema fragment, provide an **_Outgoing Relationships View_** focussing on the entities the entity
   relates to. Reference-data relationships are grouped into a single block.
2. Given an entity or schema fragment, provide a **_Referenced-by View_** focussing on the entities using/referencing
   the entity/fragment in context. The using entities are organized by group-type with the property names holding the
   references listed in the diagram.

Changes to entity schemas:
1. **Reference Value Changes/Updates**
    1. [`osdu:wks:reference-data--BitType:1.0.1`](./reference-data/BitType.1.0.1.md)
       schema patch to update the entity type description to
       match PPDM's definition. PPDM reference values are also added, earlier OSDU values are deprecated with
       instructions to replacements in the
       respective [migration guide](../Guides/MigrationGuides/M15/BitType.1.0.1.md).
       Migration is voluntary unless PPDM compliance is required.
    2. [`osdu:wks:reference-data--SeismicEnergySourceType:1.0.1`](./reference-data/SeismicEnergySourceType.1.0.1.md)
       schema patch to update the entity type description to match PPDM's definition and the governance mode is changed
       to OPEN. New, refined PPDM reference values are also added, earlier OSDU values are deprecated
       with instructions to replacements in the
       respective [migration guide](../Guides/MigrationGuides/M15/SeismicEnergySourceType.1.0.0.md).
       Migration is voluntary unless PPDM compliance is required.
    3. [`osdu:wks:reference-data--VerticalMeasurmentPath:1.0.1`](./reference-data/VerticalMeasurementPath.1.0.1.md)
       schema patch to update the entity type description to match PPDM's definition. PPDM standard reference values are
       added, earlier OSDU values are deprecated with instructions to replacements in the
       respective [migration guide](../Guides/MigrationGuides/M15/VerticalMeasurementPath.1.0.0.md).
       Migration is voluntary unless PPDM compliance is required.
    4. [`osdu:wks:reference-data--AdditiveType:1.0.1`](./reference-data/AdditiveType.1.0.1.md) has been patched to take
       the PPDM definition of the reference list as description. The list is governed by PPDM and is changed to OPEN
       governance. The previously used reference values were considered a mixture of types and roles. As a consequence,
       a new PPDM governed reference list AdditiveRole has been prepared. The upgrade instructions are provided
       in [this migration guide](../Guides/MigrationGuides/M15/AdditiveType.1.0.0.md).
    5. New [`osdu:wks:reference-data--AddditiveRole:1.0.0`](./reference-data/AdditiveRole.1.0.0.md) was added containing
       the role specific values. The list is OPEN governance by PPDM.
    6. The reference value list content
       of [`osdu:wks:reference-data--DrillingReasonType:1.0.0`](./reference-data/DrillingReasonType.1.0.0.md)
       has been deprecated. The codes are transferred to the new
       LaheeClass. [Upgrade information](../Guides/MigrationGuides/M15/DrillingReasonType.1.0.0.md).
    7. New [`osdu:wks:reference-data--LaheeClass:1.0.0`](./reference-data/LaheeClass.1.0.0.md):
       _A traditional, commonly accepted, scheme to categorize wells by the general degree of risk assumed by the
       operator at the time of drilling._ This list replaces DrillingReasonType.
    8. [`osdu:wks:reference-data--TubularAssemblyStatusType:1.0.0`](./reference-data/TubularAssemblyStatusType.1.0.0.md)
       contained by accident a copy of the `TubularAssemblyType` values. The TubularAssemblyStatusType values have been
       reduced to the three values `Planned`, `Installed` and `Pulled`.
2. **External data Sources**
    1. [`osdu:wks:master-data--ConnectedSourceDataJob:1.2.0`](./master-data/ConnectedSourceDataJob.1.2.0.md) got
       augmented by additional properties compared to the previous version 1.1.0. Detailed migration notes explaining
       the changes are provided [here](../Guides/MigrationGuides/M15/ConnectedSourceDataJob.1.1.0.md).
3. **Well Delivery**
    1. [`osdu:wks:master-data--PlannedCementJob:1.1.0`](./master-data/PlannedCementJob.1.1.0.md) has been created with
       an additional `AdditiveRoleID` referring to the new
       [`osdu:wks:reference-data--AddditiveRole:1.0.0`](./reference-data/AdditiveRole.1.0.0.md).
    2. **DrillingReasons, LaheeClass** usage<br>
       Effectively, the DrillingReasonType hs been deprecated and superseded by a new type, LaheeClass. LaheeClass has
       the same codes as DrillingReasonType and a few new codes. As a consequence, Wellbore:1.2.0 is created, including
       the schema fragment with the new LaheeClassID reference.
        1. [`osdu:wks:AbstractWellboreDrillingReason:1.1.0`](./abstract/AbstractWellboreDrillingReason.1.1.0.md)
           got `DrillingReasonTypeID` deprecated and superseded by the new `LaheeClassID`. `Remarks` were also added.
        2. [`osdu:wks:master-data--Wellbore:1.2.0`](./master-data/Wellbore.1.2.0.md) was created to
           include `AbstractWellboreDrillingReason:1.1.0`; this makes the LaheeClassID property available. In the same
           revision, `AbstractFacility:1.1.0` is included making the Remark string in `AbstractFacilityEvent:1.1.0`,
           `AbstractFacilityOperator:1.1.0` and `AbstractFacilityState:1.1.0` available.
    3. **OperationsReport** with value type corrections<br>
       Updated minor version [`osdu-wks-master-data--OperationsReport:1.2.0`](./master-data/OperationsReport.1.2.0.md) 
       (non-breaking) enabling the migration of three string properties to number properties. The number properties have 
       new property names. Migration instructions and a detailed overview of the changes are given in the 
       [M15 migration guides](../Guides/MigrationGuides/M15/OperationsReport.1.1.0.md). 
    4. **HoleSection**, augmented<br>
       A VerticalMeasurement was added to [`osdu-wks-master-data--HoleSection:1.1.0`](./master-data/HoleSection.1.1.0.md)
       to provide a firm zero-depth point.
4. **Seismic Domain**
   1. A new [`osdu:wks:reference-data--SEGY-HeaderMappingTemplate:1.0.0`](./reference-data/SEGY-HeaderMappingTemplate.1.0.0.md)
      is added to enable definitions of frequently used header parameter settings for SEG-Y file parsing. The
      template reuses the [AbstractVectorHeaderMapping](./abstract/AbstractVectorHeaderMapping.1.0.0.md), which is
      included in [dataset--FileCollection.SEGY](./dataset/FileCollection.SEGY.1.0.0.md). The 
      [worked example](../Examples/WorkedExamples/SeismicLoadingManifests/README.md) has been
      extended to cover the usage of this reference value type.
   2. The recommended usage for BinGrid has been clarified. [AbstractBinGrid:1.1.0](./abstract/AbstractBinGrid.1.1.0.md)
      captures these new usage. The improved bin grid definition is included by new schema versions of:
      1. [`osdu:wks:work-product-component--GeoReferencedImage:1.1.0`](./work-product-component/GeoReferencedImage.1.1.0.md)
      2. [`osdu:wks:work-product-component--SeismicBinGrid:1.1.0`](./work-product-component/SeismicBinGrid.1.1.0.md)
5. **Facilities**<br>
   AbstractFacility includes a number of Facility-specific schema fragments, which carry status, event and operator
   history records. These fragments did not contain a free text `Remark` string capturing additional context. In M15 
   `Remark` strings are added. This causes a number if incremental versions for the consuming entity types:
   1. [`osdu:wks:AbstractFacilityEvent:1.1.0`](./abstract/AbstractFacilityEvent.1.1.0.md) with added `Remark` string.
   2. [`osdu:wks:AbstractFacilityOperator:1.1.0`](./abstract/AbstractFacilityOperator.1.1.0.md) with added `Remark` string.  
   3. [`osdu:wks:AbstractFacilityState:1.1.0`](./abstract/AbstractFacilityState.1.1.0.md) with added `Remark` string.
   4. [`osdu:wks:AbstractFacility:1.1.0`](./abstract/AbstractFacility.1.1.0.md) including the 1.1.0 fragments from 1. to 3.
   5. [`osdu:wks:master-data--Rig:1.1.0`](./master-data/Rig.1.1.0.md) including AbstractFacility:1.1.0.
   6. [`osdu:wks:master-data--StorageFacility:1.1.0`](./master-data/StorageFacility.1.1.0.md) including AbstractFacility:1.1.0.  
   7. [`osdu:wks:master-data--Well:1.2.0`](./master-data/Well.1.2.0.md) including AbstractFacility:1.1.0.
6. **Schema and Reference Value Upgrades**<br>
   In preparation for the schema and reference-value upgrade 'tool' or 'service', Data Definitions paved the way by
   adding reference value types, which can hold the upgrade instructions for records (structural changes) and reference
   value upgrades (reference-value relationship changes). Default resources are generated by OSDU Data Definitions but
   can be augmented by operators 'injecting contextual knowledge'. Thus, the reference value lists are declared under
   LOCAL governance.<br>
   Worked examples are presented in [Examples/WorkedExamples/Upgrades](../Examples/WorkedExamples/Upgrades/README.md).
   <br>The following types are introduced:
   1. Schema Upgrade Support:
      1. [`osdu:wks:reference-data--SchemaUpgradeSpecification:1.0.0`](./reference-data/SchemaUpgradeSpecification.1.0.0.md)
         the reference value list holding the JOLT operations list. Default resources are generated during schema
         generation and located in this [sub-folder tree SchemaUpgradeResources](../SchemaUpgradeResources). By
         convention, the reference-data record gets the target schema kind associated as record id.
      2. [`osdu:wks:reference-data--SchemaUpgradeChain:1.0.0`](./reference-data/SchemaUpgradeChain.1.0.0.md) allows to
         figure out which kinds can be upgraded to a target kind since there can be chains.
   2. Reference Value Upgrade Support:
      1. [`osdu:wks:reference-data--ReferenceValueUpgradeLookUp:1.0.0`](./reference-data/ReferenceValueUpgradeLookUp.1.0.0.md)
         provides reference-data type usage information and look-up tables for deprecated values to be superseded by.
         The main schema component of this reference-data type is :
      2. [`osdu:wks:AbstractReferenceValueUpgrade:1.0.0`](./abstract/AbstractReferenceValueUpgrade.1.0.0.md) documenting
         the reference-value upgrade usages and value look-up tables (deprecated to superseded by).
      3. [`osdu:wks:reference-data--DiscoverabilityBySearch:1.0.0`](./reference-data/DiscoverabilityBySearch.1.0.0.md)
         is a new, FIXED reference list enumerating the discoverability of a given property in the UsingKinds of
         reference-data--ReferenceValueUpgradeLookUp.

# Snapshot 2022-11-09 (towards M16)

Detailed report of changes _after_ [2022-11-09](./_ChangeReports/ChangeReport-2022-11-09.md).

Changes to entity schemas:

1. **Reference Value Changes/Updates**
    1. [`osdu:wks:reference-data--ConventionalCoreType:1.0.1`](./reference-data/ConventionalCoreType.1.0.1.md): PPDM has taken over
       governance for this reference value list. The description has been improved and a revised reference value list
       has been published. Deprecation information is available
       in [this migration note](../Guides/MigrationGuides/M16/ConventionalCoreType.1.0.0.md).
    2. [`osdu:wks:reference-data--MarkerType:1.0.1`](./reference-data/Markertype.1.0.1.md): PPDM has taken over
       governance for this reference value list. The description has been improved and a revised reference value list
       has been published. Deprecation information is available
       in [this migration note](../Guides/MigrationGuides/M16/MarkerType.1.0.0.md).
    3. [`osdu:wks:reference-data--VelocityAnalysisMethod:1.0.1`](./reference-data/VelocityAnalysisMethod.1.0.1.md): PPDM
       has taken over governance for this value list. The description has been improved and the reference value
       list revised. One value has been
       deprecated. [Details in the migration guide](../Guides/MigrationGuides/M16/VelocityAnalysisMethod.1.0.0.md).
    4. CRS_CT, consisting of CoordinateTransformation and CoordinateReferenceSystem sub-catalogs have been updated to
       IOGP version 10.081 dated
       2023-01-17. [Details are listed here](../ReferenceValues/ChangeLogs/M16/ChangesAfter-2022-11-09.md). No schema 
       changes, content changes only.
2. **Connected Source/External Data Services**
    1. New [`osdu:wks:reference-data--ExternalReferenceValueMapping:1.0.0`](./reference-data/ExternalReferenceValueMapping.1.0.0.md)
       enabling re-usable reference value look-ups, scoped by [ExternalCatalogNamespace](./reference-data/ExternalCatalogNamespace.1.0.0.md),
       which can be used both in context of EDS and general ingestion use cases. This mechanism supersedes 
       `data.ReferenceValueMappings` in [ConnectedSourceRegistryEntry](./master-data/ConnectedSourceRegistryEntry.1.1.0.md).
    2. [`osdu:wks:master-data--ConnectedSourceDataJob:1.3.0`](./master-data/ConnectedSourceDataJob.1.3.0.md) includes
       now [AbstractProjectActivity:1.1.0](./abstract/AbstractProjectActivity.1.1.0.md) for dynamic extensions and 
       governance via ActivityTemplate, see [note](../Guides/MigrationGuides/M16/ConnectedSourceDataJob.1.2.0.md). 
    3. [`osdu:wks:master-data--ConnectedSourceRegistryEntry:1.2.0`](./master-data/ConnectedSourceRegistryEntry.1.2.0.md) 
       includes now
       [AbstractProjectActivity:1.1.0](./abstract/AbstractProjectActivity.1.1.0.md) for dynamic extensions and 
       governance via ActivityTemplate, see [note](../Guides/MigrationGuides/M16/ConnectedSourceRegistryEntry.1.1.0.md). 
3. **Earth Modelling/Reservoir**
    1. New work-product-component types:
        1. [`osdu:wks:work-product-component--FluidBoundaryInterpretation:1.0.0`](work-product-component/FluidBoundaryInterpretation.1.0.0.md)
           _FluidBoundary information associated to Fluid boundary-related objects  (planes, triangulated surfaces)._
        2. [`osdu:wks:work-product-component--SealedVolumeFramework:1.0.0`](work-product-component/SealedVolumeFramework.1.0.0.md)
           _Sealed volume model representations associated to geometric elements such as faults, horizons, and
           intrusions on a scale of meters to kilometers._
    2. Reference values for:
        1. [`osdu:wks:reference-data--FluidContactType:1.0.0`](reference-data/FluidContactType.1.0.0.md) _Describes the
           kind of contact of this boundary._
4. **Rock Image**
    1. New [`osdu:wks:work-product-component--RockImage:1.0.0`](./work-product-component/RockImage.1.0.0.md): An image
       taken of a rock sample such as a core, sidewall core or thin section slide. An image could be a photograph, scan,
       etc., representing a visual representation of the rock sample. <br>Worked examples
       are [provided](../Examples/WorkedExamples/RockImage/README.md).<br>RockImage refers to the following new
       reference-data types:
    2. [`osdu:wks:reference-data--ImageLightingCondition:1.0.0`](./reference-data/ImageLightingCondition.1.0.0.md) The
       type of lighting conditions captured in the image.
    3. [`osdu:wks:reference-data--RockImageType:1.0.0`](./reference-data/RockImageType.1.0.0.md) The type of image taken
       of a rock sample. For example a photograph or CT image.
5. **Formation Integrity Test (FIT/LOT)**
    1. A new [`osdu:wks:work-product-component--FormationIntegrityTest:1.0.0`](./work-product-component/FormationIntegrityTest.1.0.0.md)
       has been added: _A downhole pressure test performed in open hole to measure formation strength._<br>
       The new type uses new reference value catalogs:
        1. [FormationIntegritySurfacePressureDataSource.1.0.0](reference-data/FormationIntegritySurfacePressureDataSource.1.0.0.md)
           &mdash;_The measurement source for Formation Integrity Surface Pressure readings._
        2. [FormationIntegrityTestResult.1.0.0](reference-data/FormationIntegrityTestResult.1.0.0.md)&mdash;_The set of
           outcome types from a completed Formation Integrity Test._
        3. [FormationIntegrityTestType.1.0.0](reference-data/FormationIntegrityTestType.1.0.0.md)&mdash;_The list of
           different types of Formation Integrity Tests that can be performed._
        4. [FormationPressureTestType.1.0.0](reference-data/FormationPressureTestType.1.0.0.md)&mdash;_FIT, LOT, XLOT
           etc. Proposed reference values: Formation Integrity Test, Leak Off Test, Extended Leak Off Test_
        5. [MudBaseType.1.0.0](reference-data/MudBaseType.1.0.0.md)&mdash;_The base fluid type of the Mud/Drilling Fluid
           in well at time of the Formation Integrity test._
6. **Well Activity (Well Execution)**
    1. [osdu:wks-master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md) is extended by an array of 
       RigAssignments. See also [migration notes](../Guides/MigrationGuides/M16/WellActivity.1.0.0.md).
7. **Wellbore**
    1. [`osdu:wks:master-data--Wellbore:1.3.0`](master-data/Wellbore.1.3.0.md) is extended by a `WellboreReasonID`, an
       informal `FormationNameAtTotalDepth` and an array of `WellboreCosts[]`. Details are listed in
       the [migration notes](../Guides/MigrationGuides/M16/Wellbore.1.2.0.md). The WellboreReasonID refers to a new
       reference value list:
    2. [`osdu:wks:reference-data--WellboreReason:1.0.0`](reference-data/WellboreReason.1.0.0.md)
       with [change notes](../ReferenceValues/ChangeLogs/M16/README.md#wellborereason100). The catalog content is
       subject to change and reviewed by PPDM.
