<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractGridRepresentation [Status: Accepted]
The group of elements contained by all different kind of grids
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractGridRepresentation:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractGridRepresentation.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractGridRepresentation.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractGridRepresentation.1.0.0.json](../../Authoring/abstract/AbstractGridRepresentation.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractGridRepresentation.1.0.0.json](../../Generated/abstract/AbstractGridRepresentation.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractGridRepresentation.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractGridRepresentation.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractGridRepresentation


## Outgoing Relationships for AbstractGridRepresentation

![AbstractGridRepresentation](../_diagrams/abstract/AbstractGridRepresentation.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractGridRepresentation Referenced by other Entities

![AbstractGridRepresentation](../_diagrams/abstract/AbstractGridRepresentation.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractGridRepresentation Properties

## 1. Table of AbstractGridRepresentation Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|InterpretationID &rarr; [EarthModelInterpretation](../work-product-component/EarthModelInterpretation.1.0.0.md) &rarr; [GeobodyBoundaryInterpretation](../work-product-component/GeobodyBoundaryInterpretation.1.0.0.md) &rarr; [GeobodyInterpretation](../work-product-component/GeobodyInterpretation.1.0.0.md) &rarr; [HorizonInterpretation](../work-product-component/HorizonInterpretation.1.0.0.md) &rarr; [RockFluidOrganizationInterpretation](../work-product-component/RockFluidOrganizationInterpretation.1.0.0.md) &rarr; [RockFluidUnitInterpretation](../work-product-component/RockFluidUnitInterpretation.1.0.0.md) &rarr; [StratigraphicUnitInterpretation](../work-product-component/StratigraphicUnitInterpretation.1.0.0.md) &rarr; [StructuralOrganizationInterpretation](../work-product-component/StructuralOrganizationInterpretation.1.0.0.md) &rarr; [FaultInterpretation](../work-product-component/FaultInterpretation.1.0.0.md) &rarr; [AquiferInterpretation](../work-product-component/AquiferInterpretation.1.0.0.md)|[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)|string|optional|ID of the interpretation|Allow to link an interpretation with this representation|<code>^[\w\-\.]+:(work-product-component\-\-EarthModelInterpretation&#124;work-product-component\-\-GeobodyBoundaryInterpretation&#124;work-product-component\-\-GeobodyInterpretation&#124;work-product-component\-\-HorizonInterpretation&#124;work-product-component\-\-RockFluidOrganizationInterpretation&#124;work-product-component\-\-RockFluidUnitInterpretation&#124;work-product-component\-\-StratigraphicUnitInterpretation&#124;work-product-component\-\-StructuralOrganizationInterpretation&#124;work-product-component\-\-FaultInterpretation&#124;work-product-component\-\-AquiferInterpretation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EarthModelInterpretation](../work-product-component/EarthModelInterpretation.1.0.0.md) &rarr; [GeobodyBoundaryInterpretation](../work-product-component/GeobodyBoundaryInterpretation.1.0.0.md) &rarr; [GeobodyInterpretation](../work-product-component/GeobodyInterpretation.1.0.0.md) &rarr; [HorizonInterpretation](../work-product-component/HorizonInterpretation.1.0.0.md) &rarr; [RockFluidOrganizationInterpretation](../work-product-component/RockFluidOrganizationInterpretation.1.0.0.md) &rarr; [RockFluidUnitInterpretation](../work-product-component/RockFluidUnitInterpretation.1.0.0.md) &rarr; [StratigraphicUnitInterpretation](../work-product-component/StratigraphicUnitInterpretation.1.0.0.md) &rarr; [StructuralOrganizationInterpretation](../work-product-component/StructuralOrganizationInterpretation.1.0.0.md) &rarr; [FaultInterpretation](../work-product-component/FaultInterpretation.1.0.0.md) &rarr; [AquiferInterpretation](../work-product-component/AquiferInterpretation.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|InterpretationName|[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)|string|optional|Name of the interpretation|Name of the interpretation the representation refers to|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "InterpretationID", "TargetPropertyName": "Name"}|(No natural key)|My Interpretation|
|_TimeSeries_|_[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)_|_object_|_optional_|_Time Series_|_Allow to link the geometry of the representation to a particular index of a time series. This is particularly useful for IJK grids used in geomechanical or basin context where the topology and geometry varies against the time._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|TimeSeries.TimeIndex|[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)|integer|required|TimeIndices|Index of the timestamp of the representation in the associated TimeSeries|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|3|
|TimeSeries.TimeSeriesID &rarr; [TimeSeries](../work-product-component/TimeSeries.1.0.0.md)|[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)|string|required|TimeSeriesID|Time series the representation is associated to|<code>^[\w\-\.]+:work-product-component\-\-TimeSeries:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [TimeSeries](../work-product-component/TimeSeries.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:work-product-component--TimeSeries:85348741-3433-406B-9189-22B298C3E2D2:|
|RealizationIndex|[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)|integer|optional|Realization Index|The index of the realization of this representation|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|10|
|_IndexableElementCount[]_|_[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)_|_object_|_optional_|_IndexableElementCount_|_Several optional indexable element counts Fragment Description: Defines the count of a particular indexable element in a representation_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|IndexableElementCount[].Count|[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)|integer|required|Count|The count of indexable element|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|10000|
|IndexableElementCount[].IndexableElementID &rarr; [IndexableElement](../reference-data/IndexableElement.1.0.0.md)|[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)|string|optional|IndexableElementID|The indexable element which is counted|<code>^[\w\-\.]+:reference-data\-\-IndexableElement:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [IndexableElement](../reference-data/IndexableElement.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|LocalModelCompoundCrsID &rarr; [LocalModelCompoundCrs](../work-product-component/LocalModelCompoundCrs.1.0.0.md)|[AbstractRepresentation.1.0.0](AbstractRepresentation.1.0.0.md)|string|optional|ID of the compound CRS where this representation is given|Allow to link a local CRS with this representation|<code>^[\w\-\.]+:work-product-component\-\-LocalModelCompoundCrs:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [LocalModelCompoundCrs](../work-product-component/LocalModelCompoundCrs.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of AbstractGridRepresentation Data Properties, Section AbstractGridRepresentation

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|InterpretationID &rarr; [EarthModelInterpretation](../work-product-component/EarthModelInterpretation.1.0.0.md)| |string|optional|ID of the interpretation|The reference to the EarthModelInterpretation this grid belongs to.|<code>^[\w\-\.]+:work-product-component\-\-EarthModelInterpretation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EarthModelInterpretation](../work-product-component/EarthModelInterpretation.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ActiveCellCount| |integer|optional|Total count of active cells|Total count of active cells in the grid. It is an approximation since the real count of active cells can be modified with some properties.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|800000|
|HasNaNGeometry| |boolean|optional|Has NaN Geometry|Indicate that at least on coordinate of one node of the grid is NaN. It is different than a dead cell which may have a geometry while it is disabled.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|False|
|ParentGridID &rarr; [GpGridRepresentation](../work-product-component/GpGridRepresentation.1.0.0.md) &rarr; [IjkGridRepresentation](../work-product-component/IjkGridRepresentation.1.0.0.md) &rarr; [UnstructuredColumnLayerGridRepresentation](../work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md) &rarr; [UnstructuredGridRepresentation](../work-product-component/UnstructuredGridRepresentation.1.0.0.md)| |string|optional|Parent Grid ID|Reference to the parent grid. If present, it means that this grid is a Local GridRefinement.|<code>^[\w\-\.]+:(work-product-component\-\-GpGridRepresentation&#124;work-product-component\-\-IjkGridRepresentation&#124;work-product-component\-\-UnstructuredColumnLayerGridRepresentation&#124;work-product-component\-\-UnstructuredGridRepresentation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [GpGridRepresentation](../work-product-component/GpGridRepresentation.1.0.0.md) &rarr; [IjkGridRepresentation](../work-product-component/IjkGridRepresentation.1.0.0.md) &rarr; [UnstructuredColumnLayerGridRepresentation](../work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md) &rarr; [UnstructuredGridRepresentation](../work-product-component/UnstructuredGridRepresentation.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:work-product-component--IjkGridRepresentation:85348741-3433-406B-9189-22B298C3E2D2:|
|_StratigraphicUnits_| |_object_|_optional_|_Stratigraphic Units_|_Allow to link the K layers (or the "geologic k" property for example in case of K expansion or unstructured grid) of this grid with some stratigraphic units of a stratigraphic organization._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|StratigraphicUnits.StratigraphicUnitsIndices[][]| |integer|required|Stratigraphic Units Indices|For each K layer, indicate the corresponding stratigraphic unit indices within the associated Stratigraphic Organization. A negative value means that a K layer is not related to any stratigraphic unit (salt for example)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|StratigraphicUnits.StratigraphicColumnRankInterpretationID &rarr; [StratigraphicColumnRankInterpretation](../work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)| |string|required|Stratigraphic Column Rank Interpretation ID|Reference to the stratigraphic column rank interpretation which this grid is derived from.|<code>^[\w\-\.]+:work-product-component\-\-StratigraphicColumnRankInterpretation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StratigraphicColumnRankInterpretation](../work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:work-product-component--StratigraphicColumnRankInterpretation:85348741-3433-406B-9189-22B298C3E2D2:|
|RockFluidOrganizationInterpretationIDS[] &rarr; [RockFluidOrganizationInterpretation](../work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)| |string|optional|Array of references to RockFluidOrganizationInterpretation|References to the RockFluid organization interpretations which this grid relates to.|<code>^[\w\-\.]+:work-product-component\-\-RockFluidOrganizationInterpretation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [RockFluidOrganizationInterpretation](../work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|["namespace:work-product-component--RockFluidOrganizationInterpretation:85348741-3433-406B-9189-22B298C3E2D2:"]|
|HasFiniteElementSubnodes| |boolean|optional|Has Finite Element Subnodes|Indicate if the grid has got some finite element subnodes for defining higher order grid geometry and properties. This is mainly used in geomechanical context.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|False|
|HasNoGeometry| |boolean|optional|Has no geometry|Indicate that the grid has no geometry. It is common when one wants a grid only as a collection of cell connections (i.e. pipe network).|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|False|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractGridRepresentation.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [GpGridRepresentation.1.0.0](../work-product-component/GpGridRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [IjkGridRepresentation.1.0.0](../work-product-component/IjkGridRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [UnstructuredColumnLayerGridRepresentation.1.0.0](../work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [UnstructuredGridRepresentation.1.0.0](../work-product-component/UnstructuredGridRepresentation.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._