<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractTechnicalAssurance [Status: Accepted]
Describes a record's overall suitability for general business consumption based on level of trust.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractTechnicalAssurance:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractTechnicalAssurance.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractTechnicalAssurance.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractTechnicalAssurance.1.0.0.json](../../Authoring/abstract/AbstractTechnicalAssurance.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractTechnicalAssurance.1.0.0.json](../../Generated/abstract/AbstractTechnicalAssurance.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractTechnicalAssurance.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractTechnicalAssurance.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractTechnicalAssurance


## Outgoing Relationships for AbstractTechnicalAssurance

![AbstractTechnicalAssurance](../_diagrams/abstract/AbstractTechnicalAssurance.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractTechnicalAssurance Referenced by other Entities

![AbstractTechnicalAssurance](../_diagrams/abstract/AbstractTechnicalAssurance.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractTechnicalAssurance Properties

## Table of AbstractTechnicalAssurance Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|TechnicalAssuranceTypeID &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)| |string|required|Technical Assurance Type ID|Describes a record's overall suitability for general business consumption based on data quality. Clarifications: Since Certified is the highest classification of suitable quality, any further change or versioning of a Certified record should be carefully considered and justified. If a Technical Assurance value is not populated then one can assume the data has not been evaluated or its quality is unknown (=Unevaluated). Technical Assurance values are not intended to be used for the identification of a single "preferred" or "definitive" record by comparison with other records.|<code>^[\w\-\.]+:reference-data\-\-TechnicalAssuranceType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--TechnicalAssuranceType:Trusted:|
|_Reviewers[]_|_[AbstractContact.1.0.0](AbstractContact.1.0.0.md)_|_object_|_optional_|_Abstract Contact_|_The individuals, or roles, that reviewed and determined the technical assurance value Fragment Description: An object with properties that describe a specific person or other point-of-contact (like an email distribution list) that is relevant in this context (like a given data set or business project). The contact specified may be either internal or external to the organisation (something denoted via the Organisation object that is referenced). Note that some properties contain personally identifiable information, so it might not be appropriate to populate all properties in all scenarios._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Reviewers[].EmailAddress|[AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Email Address|Contact email address. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|support@company.com|
|Reviewers[].PhoneNumber|[AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Phone Number|Contact phone number. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1-555-281-5555|
|Reviewers[].RoleTypeID &rarr; [ContactRoleType](../reference-data/ContactRoleType.1.0.0.md)|[AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Role Type ID|The identifier of a reference value for the role of the contact within the associated organisation, such as Account owner, Sales Representative, Technical Support, Project Manager, Party Chief, Client Representative, Senior Observer.|<code>^[\w\-\.]+:reference-data\-\-ContactRoleType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ContactRoleType](../reference-data/ContactRoleType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Reviewers[].Comment|[AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Comment|Additional information about the contact|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Reviewers[].OrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Organisation ID|Reference to the company the contact is associated with.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Reviewers[].Name|[AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Name|Name of the individual contact. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_AcceptableUsage[]_| |_object_|_optional_|_AcceptableUsage_|_List of workflows and/or personas that the technical assurance value is valid for (e.g., This data is trusted for Seismic Processing) Fragment Description: Describes the workflows and/or personas that the technical assurance value is valid for (e.g., This data has a technical assurance property of "trusted" and it is suitable for Seismic Interpretation)._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_[{"WorkflowUsage": "namespace:reference-data--WorkflowUsageType:SeismicProcessing:", "WorkflowPersona": "namespace:reference-data--WorkflowPersonaType:SeismicProcessor:"}]_|
|AcceptableUsage[].WorkflowUsage &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)| |string|optional|Workflow Usage|Name of the business activities, processes, and/or workflows that the record is technical assurance value is valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowUsageType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowUsageType:SeismicProcessing:|
|AcceptableUsage[].WorkflowPersona &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)| |string|optional|Workflow Persona|Name of the role or personas that the record is technical assurance value is valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowPersonaType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowPersonaType:SeismicProcessor:|
|_UnacceptableUsage[]_| |_object_|_optional_|_UnacceptableUsage_|_List of workflows and/or personas that the technical assurance value is not valid for (e.g., This data is not trusted for seismic interpretation) Fragment Description: Describes the workflows and/or personas that the technical assurance value is not valid for (e.g., This data has a technical assurance property of "trusted", but it is not suitable for Seismic Interpretation)._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_[{"WorkflowUsage": "namespace:reference-data--WorkflowUsageType:SeismicInterpretation:", "WorkflowPersona": "namespace:reference-data--WorkflowPersonaType:SeismicInterpreter:"}]_|
|UnacceptableUsage[].WorkflowUsage &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)| |string|optional|Workflow Usage|Name of the business activities, processes, and/or workflows that the record is technical assurance value is not valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowUsageType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowUsageType:SeismicInterpretation:|
|UnacceptableUsage[].WorkflowPersona &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)| |string|optional|Workflow Persona|Name of the role or personas that the record is technical assurance value is not valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowPersonaType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowPersonaType:SeismicInterpreter:|
|EffectiveDate| |string|optional|Effective Date|Date when the technical assurance determination for this record has taken place|(No pattern)|date|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-02-13|
|Comment| |string|optional|Comment|Any additional context to support the determination of technical assurance|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|This is free form text from reviewer, e.g. restrictions on use|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractTechnicalAssurance.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractWPCGroupType.1.0.0](AbstractWPCGroupType.1.0.0.md) | TechnicalAssurances[] | many (as array) |  |
| Included by | [GenericWorkProductComponent.1.0.0](../manifest/GenericWorkProductComponent.1.0.0.md) | TechnicalAssurances[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._