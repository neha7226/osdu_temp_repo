~Version Information Block
VERS .                  2.0:  CWLS LOG ASCII STANDARD - VERSION 2.0
WRAP .                  YES:  Multiple lines per depth step
~Well Information Block
#MNEM.UNIT        Data Type   Information
#---------- ----------------  -----------
STRT .M             3146.10:  START DEPTH
STOP .M             4770.50:  STOP DEPTH
STEP .M                0.10:  STEP
NULL .              -999.25:  NULL VALUE
COMP .              Statoil:  COMPANY
WELL .          15/9-F-11 B:  WELL
FLD  .                VOLVE:  FIELD
LOC  .      Maersk Inspirer:  LOCATION
CTRY .               NORWAY:  COUNTRY
STAT .                     :  STATE
CNTY .      MAERSK INSPIRER:  COUNTY
SRVC .              STATOIL:  SERVICE COMPANY
DATE .                     :  DATE
API  .               F-11 B:  API NUMBER
UWI  .       NO 15/9-F-11 B:  UNIQUE WELL ID
X    .           435049.095:  X OR EAST-WEST COORDINATE
Y    .          6478568.172:  Y OR NORTH-SOUTH COORDINATE
LATI .     58 26' 29.9566" N:  LATITUDE
LONG .     01 53' 14.8667" E:  LONGITUDE
~Curve Information Block
#MNEM.UNIT         API CODE   Curve Description
#---------- ----------------  -----------------
DEPTH.M                    :  0.100000 m frame 0 depth
ABDC01M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 01
ABDC02M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 02
ABDC03M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 03
ABDC04M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 04
ABDC05M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 05
ABDC06M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 06
ABDC07M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 07
ABDC08M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 08
ABDC09M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 09
ABDC10M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 10
ABDC11M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 11
ABDC12M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 12
ABDC13M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 13
ABDC14M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 14
ABDC15M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 15
ABDC16M.G/CM3                 :  Azimuthal Bulk Density - Compensated - Sector 16
ABDCQF01.G/CM3                 :  Azimuthal Bulk Density - Compensated - Quadrant 1
ABDCQF02.G/CM3                 :  Azimuthal Bulk Density - Compensated - Quadrant 2
ABDCQF03.G/CM3                 :  Azimuthal Bulk Density - Compensated - Quadrant 3
ABDCQF04.G/CM3                 :  Azimuthal Bulk Density - Compensated - Quadrant 4
ACTECDM.SG                   :  Actual ECD
APRA01M.                     :
APRA02M.                     :
APRA03M.                     :
APRA04M.                     :
APRA05M.                     :
APRA06M.                     :
APRA07M.                     :
APRA08M.                     :
APRA09M.                     :
APRA10M.                     :
APRA11M.                     :
APRA12M.                     :
APRA13M.                     :
APRA14M.                     :
APRA15M.                     :
APRA16M.                     :
APRESM.BAR                  :  Annular Pressure
AT2GTFM.DEG                  :
AZRIT1T2.                     :
AZRTBM.                     :
BDCFM.G/CM3                 :  Bulk Density Filtered
BDSIM.                     :  Bulk Density Sliding Indicator
BITSIZE.IN                   :  Bitsize
BPRESM.BAR                  :  Bore Pressure
CALCM.IN                   :  Acoustic Caliper - Corrected
DLAFM.G/CM3                 :  Density Long Spaced Apparent Filtered
DPEFM.B/E                  :  Density PE Filtered
DRHFM.G/CM3                 :  Density Delta Rho Filtered
DSAFM.G/CM3                 :  Density Short Spaced Apparent Filtered
FLOWIN.L/MIN                 :  Mud Flow In
GR1CFM.GAPI                 :  Gamma Ray sensor #1 , borehole corrected
GR2CFM.GAPI                 :  Gamma Ray sensor # 2, borehole corrected
GRCDFM.GAPI                 :  Azimuthal Gamma Ray Down Quadrant Corrected
GRCFM.GAPI                 :  Gamma Ray - Corrected Filtered
GRCLFM.GAPI                 :  Azimuthal Gamma Ray Left Quadrant Corrected
GRCRFM.GAPI                 :  Azimuthal Gamma Ray Right Quadrant Corrected
GRCS01M.GAPI                 :
GRCS02M.GAPI                 :
GRCS03M.GAPI                 :
GRCS04M.GAPI                 :
GRCS05M.GAPI                 :
GRCS06M.GAPI                 :
GRCS07M.GAPI                 :
GRCS08M.GAPI                 :
GRCUFM.GAPI                 :  Azimuthal Gamma Ray Up Quadrant Corrected
GRSIM.                     :  Gamma Ray Sliding Indicator
INNM .DEG                  :  Inclination - Non-Rotating
NFBFM.1/S                  :  Neutron Far Base Filtered
NNBFM.1/S                  :  Neutron Near Base Filtered
NPCKLFM.                     :  Neutron Porosity Caliper Potassium Filtered
NPCKSFM.                     :  Neutron Porosity Sandstone Matrix,salinity and caliper corrected
NPCLFM.                     :  Neutron Porosity Caliper Filtered
NPKLFM.                     :  Neutron Porosity Potassium Filtered
NPLFM.                     :  Neutron Porosity Limestone Filtered
NPSFM.                     :  Neutron Porosity Sandstone Matrix
PUMPAVG.BAR                  :
RAAEHM.OHMM                 :  Resistivity Attenuation - Apparent - CRIM - 2MHz
RAAELM.OHMM                 :  Resistivity Attenuation - Apparent - CRIM - 400kHz
RAAESHM.OHMM                 :  Resistivity Attenuation Short Spaced - Apparent - CRIM - 2HMz
RAAESLM.OHMM                 :  Resistivity Attenuation Short Spaced - Apparent - CRIM - 400kHz
RACEHM.OHMM                 :  Resistivity Attenuation - Corrected - CRIM - 2MHz
RACELM.OHMM                 :  Resistivity Attenuation - Corrected - CRIM - 400kHz
RACESHM.OHMM                 :  Resistivity Attenuation Short Spaced - Corrected - CRIM - 2MHz
RACESLM.OHMM                 :  Resistivity Attenuation Short Spaced - Corrected - CRIM - 400kHz
ROPAVG.M/HR                 :  Rate of Penetration
RPAEHM.OHMM                 :  Resistivity Phase - Apparent - CRIM - 2MHz
RPAELM.OHMM                 :  Resistivity Phase - Apparent - CRIM - 400kHz
RPAESHM.OHMM                 :  Resistivity Phase Short Spaced - Apparent - CRIM - 2MHz
RPAESLM.OHMM                 :  Resistivity Phase Short Spaced - Apparent - CRIM - 400kHz
RPCEHM.OHMM                 :  Resistivity Phase - Corrected - CRIM - 2MHz
RPCELM.OHMM                 :  Resistivity Phase - Corrected - CRIM - 400kHz
RPCESHM.OHMM                 :  Resistivity Phase Short Spaced - Corrected - CRIM - 2MHz
RPCESLM.OHMM                 :  Resistivity Phase Short Spaced - Corrected - CRIM - 400kHz
RPTHM.M                    :  Resistivity Time Since Drilled
SSRPMAVM.RPM                  :  Stick Slip RPM Average
SSRPMAXM.RPM                  :  Stick Slip RPM Maximum
SSRPMINM.RPM                  :  Stick Slip RPM Minimum
SURFRPM.RPM                  :
TCDM .DEGC                 :  Downhole Temperature
TORQUEAV.KN.M                 :
TVD  .M                    :  True Vertical Depth
V2AVGXYM.                     :  Valid2 Average Lateral Vibration - RMS
V2AVGZM2.                     :
V2MAXXYM.                     :  Valid2 Maximum Lateral Vibration - RMS
V2MAXZM2.                     :
V2MINXYM.                     :  Valid2 Minimum Lateral Vibration - RMS
V2MINZM2.                     :
V2SEVXYM.                     :  Valid2 Lateral Vibration Severity Level
V2SEVZM.                     :  Valid2 Axial Vibration Severity Level
WOBAVG.T                    :
~Parameter Information Block
#MNEM.UNIT            Value   Description
#---------- ----------------  -----------
ABSE_FVAL.              -999.25:  Absent Value - Floating Point Data
BS   .IN                8.5:  Bitsize
CN   .              Statoil:  Company Name
COUN .               Unused:  County
FL   .       M�rsk Inspirer:  Field Location
FN   .                VOLVE:  Field Name
ID   .           00001_TEMP:
RM   .OHMM               10:  Mud resistivity (assumed)
SEQUENCE_NUMBER.                    1:
SRC_FILE.             temp.xtf:  Input Source File Name
SRC_NAME.             temp.xtf:  Original Source File Name
SRC_UNIT.               meters:  Input Source Index Units
TAPE_NAME.     WL_RAW_BHPR-CAL-DEN-GR-MECH-NEU-REMP_MWD_1.DLIS:
WN   .          15/9-F-11 B:  Well Name
PROJECT.     VOLVE_PUBLIC@st-vlinapp01:
SET  .     RAW11_AZTK_CCN_ORD_MWD85:
DATUM_ELEVATION.                  MSL:  Datum_elevation
DEPTH_BOTTOM.M                4771:  Downhole depth
DIRECTIONAL_SET.             DEV_SURV:  Preferred directional survey set
DRILLED_DEPTH.M                4770:  Total Depth - Driller
DRILL_MEAS_REF.                   DF:
ELEV_MEAS_REF.M                54.9:  Elevation of Measurement Reference
ELEV_PERM_DATUM.M                   0:
FAULTS_SET.                     :  Preferred faults set
LOGGED_DEPTH.M                4770:  Total Depth - Logger
MEASUREMENT_REF.                   KB:  Measurement Reference
PERMANENT_DATUM.                  MSL:  Permanent Datum
SURFACE_ELEV.M                 -91:  Elevation of Ground Level
SYMBOL.                 BALL:  Symbol
TVD_BOTTOM.M      3258.099408611:  True Vertical Depth at well bottom
TVD_MIN_REPEAT.M                    :  TVD repeat section tolerance
XOFFSET_BOTTOM.M      1503.006022038:  Amount of E/W distance from KB at TD
YOFFSET_BOTTOM.M     -168.2041758403:  Amount of N/S distance from KB at TD
~Other Information Block
HLD:
Any opinion and / or recommendation expressed orally or written
herein, has been prepared carefully and may be used if the user
so elects, however no representative or warranty is made by
ourselves or our agents as resulting from use of the same. INTEQ's
liabilities and obligations shall be governed by INTEQ's Standard
Terms and Conditions.
LOG_REMARK1:
Borehole size correction applied using fixed borehole size.
LOG_REMARK2:
Set comment test. Poor data quality expected from depth interval xxx0 to xxx6 mMD, as indicated by large caliper overgauge.
~Ascii Log Data
       3146.10
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500     -999.25000      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500        87.6585      1363.5287
      0.323300       0.371000       0.329700       0.337800       0.345400
      0.392800      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500    -999.250000      -999.2500
   -999.250000      -999.2500   -999.2500000      -999.2500      -999.2500
     -999.2500
       3146.20
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500     -999.25000      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500        87.4289      1363.3319
      0.324600       0.372300       0.331100       0.339100       0.346800
      0.394200      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500    -999.250000      -999.2500
   -999.250000      -999.2500   -999.2500000      -999.2500      -999.2500
     -999.2500
       3146.30
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500     -999.25000      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500        87.1994      1363.1351
      0.325900       0.373600       0.332500       0.340400       0.348100
      0.395500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500    -999.250000      -999.2500
   -999.250000      -999.2500   -999.2500000      -999.2500      -999.2500
     -999.2500
       3146.40
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500     -999.25000      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500        86.9019      1362.8802
      0.327700       0.375300       0.334200       0.342100       0.349900
      0.397200      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500    -999.250000      -999.2500
   -999.250000      -999.2500   -999.2500000      -999.2500      -999.2500
     -999.2500
       4770.50
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
        1.5604      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500       496.3341      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500         8.5000         0.0000
     -999.2500      -999.2500      -999.2500     -999.25000      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
   -999.250000    -999.250000    -999.250000    -999.250000    -999.250000
   -999.250000      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
     -999.2500      -999.2500      -999.2500      -999.2500      -999.2500
      159.7595       162.6664       157.1654      -999.2500       115.2409
     -999.2500      -999.2500         1.1533       0.046700         1.2246
      0.052500         1.0713      0.0421001         2.0000         0.0000
     -999.2500
