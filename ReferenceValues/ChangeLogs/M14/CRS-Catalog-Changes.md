# Name Changes for CoordinateReferenceSystem Records

Changes are limited to `"CoordinateReferenceSystemType": "BoundCRS"`. Out of 1277 BoundCRS records 421 got their names
revised. Changes occurred where the projection method name was **_not_** part of the CRS name. The table below lists the
old name and the revised name for each of the 421 changed records. The changes were made in response to
[issue reference-values#149](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/reference-values/-/issues/149)
recorded in the OSDU member GitLab.

| `id` | Old Name | New Name |
|------|----------|----------|
|BoundProjected:EPSG::24600_EPSG::1059|KOC * WGC-Kwt / Iraq zone [24600,1059]|KOC * WGC-Kwt Lambert [24600,1059]|
|BoundProjected:EPSG::3006_EPSG::1879|SWEREF99 * EPSG-Swe / SWEREF99 TM [3006,1879]|SWEREF99 * EPSG-Swe TM [3006,1879]|
|BoundProjected:EPSG::20249_EPSG::1668|AGD66 * EPSG-NT 1m / Australian Map Grid zone 49 [20249,1668]|AGD66 * EPSG-NT 1m / AMG zone 49 [20249,1668]|
|BoundProjected:EPSG::20249_EPSG::15788|AGD66 * OGP-Aus 5m / Australian Map Grid zone 49 [20249,15788]|AGD66 * OGP-Aus 5m / AMG zone 49 [20249,15788]|
|BoundProjected:EPSG::20249_EPSG::15980|AGD66 * EPSG-Aus off / Australian Map Grid zone 49 [20249,15980]|AGD66 * EPSG-Aus off / AMG zone 49 [20249,15980]|
|BoundProjected:EPSG::20250_EPSG::15788|AGD66 * OGP-Aus 5m / Australian Map Grid zone 50 [20250,15788]|AGD66 * OGP-Aus 5m / AMG zone 50 [20250,15788]|
|BoundProjected:EPSG::20250_EPSG::15786|AGD66 * OGP-Aus 0.1m / Australian Map Grid zone 50 [20250,15786]|AGD66 * OGP-Aus 0.1m / AMG zone 50 [20250,15786]|
|BoundProjected:EPSG::20250_EPSG::15980|AGD66 * EPSG-Aus off / Australian Map Grid zone 50 [20250,15980]|AGD66 * EPSG-Aus off / AMG zone 50 [20250,15980]|
|BoundProjected:EPSG::20251_EPSG::1108|AGD66 * DMA-Aus / Australian Map Grid zone 51 [20251,1108]|AGD66 * DMA-Aus / AMG zone 51 [20251,1108]|
|BoundProjected:EPSG::20251_EPSG::15788|AGD66 * OGP-Aus 5m / Australian Map Grid zone 51 [20251,15788]|AGD66 * OGP-Aus 5m / AMG zone 51 [20251,15788]|
|BoundProjected:EPSG::20251_EPSG::15786|AGD66 * OGP-Aus 0.1m / Australian Map Grid zone 51 [20251,15786]|AGD66 * OGP-Aus 0.1m / AMG zone 51 [20251,15786]|
|BoundProjected:EPSG::20251_EPSG::15980|AGD66 * EPSG-Aus off / Australian Map Grid zone 51 [20251,15980]|AGD66 * EPSG-Aus off / AMG zone 51 [20251,15980]|
|BoundProjected:EPSG::20252_EPSG::1108|AGD66 * DMA-Aus / Australian Map Grid zone 52 [20252,1108]|AGD66 * DMA-Aus / AMG zone 52 [20252,1108]|
|BoundProjected:EPSG::20252_EPSG::15788|AGD66 * OGP-Aus 5m / Australian Map Grid zone 52 [20252,15788]|AGD66 * OGP-Aus 5m / AMG zone 52 [20252,15788]|
|BoundProjected:EPSG::20252_EPSG::15980|AGD66 * EPSG-Aus off / Australian Map Grid zone 52 [20252,15980]|AGD66 * EPSG-Aus off / AMG zone 52 [20252,15980]|
|BoundProjected:EPSG::20253_EPSG::1108|AGD66 * DMA-Aus / Australian Map Grid zone 53 [20253,1108]|AGD66 * DMA-Aus / AMG zone 53 [20253,1108]|
|BoundProjected:EPSG::20253_EPSG::15788|AGD66 * OGP-Aus 5m / Australian Map Grid zone 53 [20253,15788]|AGD66 * OGP-Aus 5m / AMG zone 53 [20253,15788]|
|BoundProjected:EPSG::20253_EPSG::15786|AGD66 * OGP-Aus 0.1m / Australian Map Grid zone 53 [20253,15786]|AGD66 * OGP-Aus 0.1m / AMG zone 53 [20253,15786]|
|BoundProjected:EPSG::20254_EPSG::15788|AGD66 * OGP-Aus 5m / Australian Map Grid zone 54 [20254,15788]|AGD66 * OGP-Aus 5m / AMG zone 54 [20254,15788]|
|BoundProjected:EPSG::20254_EPSG::15980|AGD66 * EPSG-Aus off / Australian Map Grid zone 54 [20254,15980]|AGD66 * EPSG-Aus off / AMG zone 54 [20254,15980]|
|BoundProjected:EPSG::20254_EPSG::6943|AGD66 * Qcl-Png Mainland / Australian Map Grid zone 54 [20254,6943]|AGD66 * Qcl-Png Mainland / AMG zone 54 [20254,6943]|
|BoundProjected:EPSG::20255_EPSG::15788|AGD66 * OGP-Aus 5m / Australian Map Grid zone 55 [20255,15788]|AGD66 * OGP-Aus 5m / AMG zone 55 [20255,15788]|
|BoundProjected:EPSG::20255_EPSG::15786|AGD66 * OGP-Aus 0.1m / Australian Map Grid zone 55 [20255,15786]|AGD66 * OGP-Aus 0.1m / AMG zone 55 [20255,15786]|
|BoundProjected:EPSG::20255_EPSG::15980|AGD66 * EPSG-Aus off / Australian Map Grid zone 55 [20255,15980]|AGD66 * EPSG-Aus off / AMG zone 55 [20255,15980]|
|BoundProjected:EPSG::20255_EPSG::6943|AGD66 * Qcl-Png Mainland / Australian Map Grid zone 55 [20255,6943]|AGD66 * Qcl-Png Mainland / AMG zone 55 [20255,6943]|
|BoundProjected:EPSG::20256_EPSG::15788|AGD66 * OGP-Aus 5m / Australian Map Grid zone 56 [20256,15788]|AGD66 * OGP-Aus 5m / AMG zone 56 [20256,15788]|
|BoundProjected:EPSG::20349_EPSG::1109|AGD84 * DMA-Aus / Australian Map Grid zone 49 [20349,1109]|AGD84 * DMA-Aus / AMG zone 49 [20349,1109]|
|BoundProjected:EPSG::20349_EPSG::1669|AGD84 * EPSG-Aus 1m / Australian Map Grid zone 49 [20349,1669]|AGD84 * EPSG-Aus 1m / AMG zone 49 [20349,1669]|
|BoundProjected:EPSG::20350_EPSG::1109|AGD84 * DMA-Aus / Australian Map Grid zone 50 [20350,1109]|AGD84 * DMA-Aus / AMG zone 50 [20350,1109]|
|BoundProjected:EPSG::20350_EPSG::1236|AGD84 * Auslig-Aus old / Australian Map Grid zone 50 [20350,1236]|AGD84 * Auslig-Aus old / AMG zone 50 [20350,1236]|
|BoundProjected:EPSG::20350_EPSG::1669|AGD84 * EPSG-Aus 1m / Australian Map Grid zone 50 [20350,1669]|AGD84 * EPSG-Aus 1m / AMG zone 50 [20350,1669]|
|BoundProjected:EPSG::20351_EPSG::1109|AGD84 * DMA-Aus / Australian Map Grid zone 51 [20351,1109]|AGD84 * DMA-Aus / AMG zone 51 [20351,1109]|
|BoundProjected:EPSG::20351_EPSG::1236|AGD84 * Auslig-Aus old / Australian Map Grid zone 51 [20351,1236]|AGD84 * Auslig-Aus old / AMG zone 51 [20351,1236]|
|BoundProjected:EPSG::20351_EPSG::1669|AGD84 * EPSG-Aus 1m / Australian Map Grid zone 51 [20351,1669]|AGD84 * EPSG-Aus 1m / AMG zone 51 [20351,1669]|
|BoundProjected:EPSG::20351_EPSG::15785|AGD84 * OGP-Aus 1m / Australian Map Grid zone 51 [20351,15785]|AGD84 * OGP-Aus 1m / AMG zone 51 [20351,15785]|
|BoundProjected:EPSG::20352_EPSG::1109|AGD84 * DMA-Aus / Australian Map Grid zone 52 [20352,1109]|AGD84 * DMA-Aus / AMG zone 52 [20352,1109]|
|BoundProjected:EPSG::20352_EPSG::1236|AGD84 * Auslig-Aus old / Australian Map Grid zone 52 [20352,1236]|AGD84 * Auslig-Aus old / AMG zone 52 [20352,1236]|
|BoundProjected:EPSG::20352_EPSG::1669|AGD84 * EPSG-Aus 1m / Australian Map Grid zone 52 [20352,1669]|AGD84 * EPSG-Aus 1m / AMG zone 52 [20352,1669]|
|BoundProjected:EPSG::20353_EPSG::1109|AGD84 * DMA-Aus / Australian Map Grid zone 53 [20353,1109]|AGD84 * DMA-Aus / AMG zone 53 [20353,1109]|
|BoundProjected:EPSG::20353_EPSG::1669|AGD84 * EPSG-Aus 1m / Australian Map Grid zone 53 [20353,1669]|AGD84 * EPSG-Aus 1m / AMG zone 53 [20353,1669]|
|BoundProjected:EPSG::20354_EPSG::1236|AGD84 * Auslig-Aus old / Australian Map Grid zone 54 [20354,1236]|AGD84 * Auslig-Aus old / AMG zone 54 [20354,1236]|
|BoundProjected:EPSG::20354_EPSG::1669|AGD84 * EPSG-Aus 1m / Australian Map Grid zone 54 [20354,1669]|AGD84 * EPSG-Aus 1m / AMG zone 54 [20354,1669]|
|BoundProjected:EPSG::20355_EPSG::1109|AGD84 * DMA-Aus / Australian Map Grid zone 55 [20355,1109]|AGD84 * DMA-Aus / AMG zone 55 [20355,1109]|
|BoundProjected:EPSG::20355_EPSG::1669|AGD84 * EPSG-Aus 1m / Australian Map Grid zone 55 [20355,1669]|AGD84 * EPSG-Aus 1m / AMG zone 55 [20355,1669]|
|BoundProjected:EPSG::20356_EPSG::1669|AGD84 * EPSG-Aus 1m / Australian Map Grid zone 56 [20356,1669]|AGD84 * EPSG-Aus 1m / AMG zone 56 [20356,1669]|
|BoundProjected:EPSG::20499_EPSG::1110|Ain el Abd * DMA-Bhr / Bahrain State Grid [20499,1110]|Ain el Abd * DMA-Bhr / Bahrain Grid [20499,1110]|
|BoundProjected:EPSG::2432_EPSG::15918|Beijing 1954 * BGP-Chn Ord / Gauss-Kruger CM 105E [2432,15918]|Beijing 1954 * BGP-Chn Ord / 3-degree Gauss-Kruger CM 105E [2432,15918]|
|BoundProjected:EPSG::2432_EPSG::15921|Beijing 1954 * BGP-Chn Tarim / Gauss-Kruger CM 105E [2432,15921]|Beijing 1954 * BGP-Chn Tarim / 3-degree Gauss-Kruger CM 105E [2432,15921]|
|BoundProjected:EPSG::2436_EPSG::15921|Beijing 1954 * BGP-Chn Tarim / Gauss-Kruger CM 117E [2436,15921]|Beijing 1954 * BGP-Chn Tarim / 3-degree Gauss-Kruger CM 117E [2436,15921]|
|BoundProjected:EPSG::2426_EPSG::15921|Beijing 1954 * BGP-Chn Tarim / Gauss-Kruger CM 87E [2426,15921]|Beijing 1954 * BGP-Chn Tarim / 3-degree Gauss-Kruger CM 87E [2426,15921]|
|BoundProjected:EPSG::21415_EPSG::15918|Beijing 1954 * BGP-Chn Ord / 6-degree Gauss-Kruger zone 15 [21415,15918]|Beijing 1954 * BGP-Chn Ord / Gauss-Kruger zone 15 [21415,15918]|
|BoundProjected:EPSG::21415_EPSG::15919|Beijing 1954 * BP-Chn YS / 6-degree Gauss-Kruger zone 15 [21415,15919]|Beijing 1954 * BP-Chn YS / Gauss-Kruger zone 15 [21415,15919]|
|BoundProjected:EPSG::21415_EPSG::15921|Beijing 1954 * BGP-Chn Tarim / 6-degree Gauss-Kruger zone 15 [21415,15921]|Beijing 1954 * BGP-Chn Tarim / Gauss-Kruger zone 15 [21415,15921]|
|BoundProjected:EPSG::21416_EPSG::15921|Beijing 1954 * BGP-Chn Tarim / 6-degree Gauss-Kruger zone 16 [21416,15921]|Beijing 1954 * BGP-Chn Tarim / Gauss-Kruger zone 16 [21416,15921]|
|BoundProjected:EPSG::21418_EPSG::15918|Beijing 1954 * BGP-Chn Ord / 6-degree Gauss-Kruger zone 18 [21418,15918]|Beijing 1954 * BGP-Chn Ord / Gauss-Kruger zone 18 [21418,15918]|
|BoundProjected:EPSG::21418_EPSG::15920|Beijing 1954 * GSI-Chn SCS / 6-degree Gauss-Kruger zone 18 [21418,15920]|Beijing 1954 * GSI-Chn SCS / Gauss-Kruger zone 18 [21418,15920]|
|BoundProjected:EPSG::21419_EPSG::15919|Beijing 1954 * BP-Chn YS / 6-degree Gauss-Kruger zone 19 [21419,15919]|Beijing 1954 * BP-Chn YS / Gauss-Kruger zone 19 [21419,15919]|
|BoundProjected:EPSG::21420_EPSG::15919|Beijing 1954 * BP-Chn YS / 6-degree Gauss-Kruger zone 20 [21420,15919]|Beijing 1954 * BP-Chn YS / Gauss-Kruger zone 20 [21420,15919]|
|BoundProjected:EPSG::21420_EPSG::15921|Beijing 1954 * BGP-Chn Tarim / 6-degree Gauss-Kruger zone 20 [21420,15921]|Beijing 1954 * BGP-Chn Tarim / Gauss-Kruger zone 20 [21420,15921]|
|BoundProjected:EPSG::21421_EPSG::15919|Beijing 1954 * BP-Chn YS / 6-degree Gauss-Kruger zone 21 [21421,15919]|Beijing 1954 * BP-Chn YS / Gauss-Kruger zone 21 [21421,15919]|
|BoundProjected:EPSG::22191_EPSG::1127|Campo Inchauspe * DMA-Arg / Argentina zone 1 [22191,1127]|Campo Inchauspe * DMA-Arg / Argentina 1 [22191,1127]|
|BoundProjected:EPSG::22192_EPSG::1127|Campo Inchauspe * DMA-Arg / Argentina zone 2 [22192,1127]|Campo Inchauspe * DMA-Arg / Argentina 2 [22192,1127]|
|BoundProjected:EPSG::22193_EPSG::1127|Campo Inchauspe * DMA-Arg / Argentina zone 3 [22193,1127]|Campo Inchauspe * DMA-Arg / Argentina 3 [22193,1127]|
|BoundProjected:EPSG::22194_EPSG::1127|Campo Inchauspe * DMA-Arg / Argentina zone 4 [22194,1127]|Campo Inchauspe * DMA-Arg / Argentina 4 [22194,1127]|
|BoundProjected:EPSG::22195_EPSG::1127|Campo Inchauspe * DMA-Arg / Argentina zone 5 [22195,1127]|Campo Inchauspe * DMA-Arg / Argentina 5 [22195,1127]|
|BoundProjected:EPSG::22196_EPSG::1127|Campo Inchauspe * DMA-Arg / Argentina zone 6 [22196,1127]|Campo Inchauspe * DMA-Arg / Argentina 6 [22196,1127]|
|BoundProjected:EPSG::22197_EPSG::1127|Campo Inchauspe * DMA-Arg / Argentina zone 7 [22197,1127]|Campo Inchauspe * DMA-Arg / Argentina 7 [22197,1127]|
|BoundProjected:EPSG::21781_EPSG::1766|CH1903 * BfL-CH 2 / Swiss Oblique Mercator 1903M [21781,1766]|CH1903 * BfL-CH 2 / LV03 [21781,1766]|
|BoundProjected:EPSG::2081_EPSG::8517|Chos Malal 1914 * TOT-Arg Neu / Argentina zone 2 [2081,8517]|Chos Malal 1914 * TOT-Arg Neu / Argentina 2 [2081,8517]|
|BoundProjected:EPSG::3119_EPSG::15873|Douala 1948 * Tot-Cmr / French Equatorial Africa west zone [3119,15873]|Douala 1948 * Tot-Cmr / AEF west [3119,15873]|
|BoundProjected:EPSG::3066_EPSG::1087|ED50 * RJGC-Jor / Jordan Transverse Mercator [3066,1087]|ED50 * RJGC-Jor / Jordan TM [3066,1087]|
|BoundProjected:EPSG::5643_EPSG::1311|ED50 * UKOOA-CO / Southern Permian Basin Atlas Lambert [5643,1311]|ED50 * UKOOA-CO / SPBA LCC [5643,1311]|
|BoundProjected:EPSG::2319_EPSG::1784|ED50 * EPSG-Tur / Gauss-Kruger CM 27E [2319,1784]|ED50 * EPSG-Tur / TM27 [2319,1784]|
|BoundProjected:EPSG::2320_EPSG::1784|ED50 * EPSG-Tur / 3-degree Gauss-Kruger CM 30E [2320,1784]|ED50 * EPSG-Tur / TM30 [2320,1784]|
|BoundProjected:EPSG::2321_EPSG::1784|ED50 * EPSG-Tur / Gauss-Kruger CM 33E [2321,1784]|ED50 * EPSG-Tur / TM33 [2321,1784]|
|BoundProjected:EPSG::2322_EPSG::1784|ED50 * EPSG-Tur / 3-degree Gauss-Kruger CM 36E [2322,1784]|ED50 * EPSG-Tur / TM36 [2322,1784]|
|BoundProjected:EPSG::2323_EPSG::1784|ED50 * EPSG-Tur / Gauss-Kruger CM 39E [2323,1784]|ED50 * EPSG-Tur / TM39 [2323,1784]|
|BoundProjected:EPSG::2324_EPSG::1784|ED50 * EPSG-Tur / 3-degree Gauss-Kruger CM 42E [2324,1784]|ED50 * EPSG-Tur / TM42 [2324,1784]|
|BoundProjected:EPSG::2325_EPSG::1784|ED50 * EPSG-Tur / Gauss-Kruger CM 45E [2325,1784]|ED50 * EPSG-Tur / TM45 [2325,1784]|
|BoundProjected:EPSG::22991_EPSG::1148|Egypt 1907 * DMA-Egy / Egypt Blue Belt [22991,1148]|Egypt 1907 * DMA-Egy / Blue Belt [22991,1148]|
|BoundProjected:EPSG::22994_EPSG::1148|Egypt 1907 * DMA-Egy / Egypt Extended Purple Belt [22994,1148]|Egypt 1907 * DMA-Egy / Extended Purple Belt [22994,1148]|
|BoundProjected:EPSG::22993_EPSG::1148|Egypt 1907 * DMA-Egy / Egypt Purple Belt [22993,1148]|Egypt 1907 * DMA-Egy / Purple Belt [22993,1148]|
|BoundProjected:EPSG::22993_EPSG::8537|Egypt 1907 * MCE-Egy / Egypt Purple Belt [22993,8537]|Egypt 1907 * MCE-Egy / Purple Belt [22993,8537]|
|BoundProjected:EPSG::22992_EPSG::1148|Egypt 1907 * DMA-Egy / Egypt Red Belt [22992,1148]|Egypt 1907 * DMA-Egy / Red Belt [22992,1148]|
|BoundProjected:EPSG::22992_EPSG::8537|Egypt 1907 * MCE-Egy / Egypt Red Belt [22992,8537]|Egypt 1907 * MCE-Egy / Red Belt [22992,8537]|
|BoundProjected:EPSG::3355_EPSG::15846|Egypt Gulf of Suez S-650 TL * Racal-Egy GoS / Egypt Red Belt [3355,15846]|Egypt Gulf of Suez S-650 TL * Racal-Egy GoS / Red Belt [3355,15846]|
|BoundProjected:EPSG::6069_EPSG::1149|ETRS89 * EPSG-eur / EPSG Arctic LCC zone 2-22 [6069,1149]|ETRS89 * EPSG-eur / EPSG Arctic zone 2-22 [6069,1149]|
|BoundProjected:EPSG::6070_EPSG::1149|ETRS89 * EPSG-eur / EPSG Arctic LCC zone 3-11 [6070,1149]|ETRS89 * EPSG-eur / EPSG Arctic zone 3-11 [6070,1149]|
|BoundProjected:EPSG::6071_EPSG::1149|ETRS89 * EPSG-eur / EPSG Arctic LCC zone 4-26 [6071,1149]|ETRS89 * EPSG-eur / EPSG Arctic zone 4-26 [6071,1149]|
|BoundProjected:EPSG::6072_EPSG::1149|ETRS89 * EPSG-eur / EPSG Arctic LCC zone 4-28 [6072,1149]|ETRS89 * EPSG-eur / EPSG Arctic zone 4-28 [6072,1149]|
|BoundProjected:EPSG::6073_EPSG::1149|ETRS89 * EPSG-eur / EPSG Arctic LCC zone 5-11 [6073,1149]|ETRS89 * EPSG-eur / EPSG Arctic zone 5-11 [6073,1149]|
|BoundProjected:EPSG::6074_EPSG::1149|ETRS89 * EPSG-eur / EPSG Arctic LCC zone 5-13 [6074,1149]|ETRS89 * EPSG-eur / EPSG Arctic zone 5-13 [6074,1149]|
|BoundProjected:EPSG::6125_EPSG::1149|ETRS89 * EPSG-eur / EPSG Arctic LCC zone 5-47 [6125,1149]|ETRS89 * EPSG-eur / EPSG Arctic zone 5-47 [6125,1149]|
|BoundProjected:EPSG::2180_EPSG::1149|ETRF2000-PL * EPSG-eur / Poland CS92 [2180,1149]|ETRF2000-PL * EPSG-eur / CS92 [2180,1149]|
|BoundProjected:EPSG::5649_EPSG::1149|ETRS89 * EPSG-eur / UTM zone 31N with prefix [5649,1149]|ETRS89 * EPSG-eur / UTM zone 31N (zE-N) [5649,1149]|
|BoundProjected:EPSG::4647_EPSG::1149|ETRS89 * EPSG-eur / UTM zone 32N with prefix [4647,1149]|ETRS89 * EPSG-eur / UTM zone 32N (zE-N) [4647,1149]|
|BoundProjected:EPSG::5650_EPSG::1149|ETRS89 * EPSG-eur / UTM zone 33N with prefix [5650,1149]|ETRS89 * EPSG-eur / UTM zone 33N (zE-N) [5650,1149]|
|BoundProjected:EPSG::3034_EPSG::1149|ETRS89 * EPSG-eur / Europe Conformal 2001 [3034,1149]|ETRS89-extended * EPSG-eur / LCC Europe [3034,1149]|
|BoundProjected:EPSG::7849_EPSG::8450|GDA2020 * GA-Aus 3m / Map Grid of Australia zone 49 [7849,8450]|GDA2020 * GA-Aus 3m / MGA zone 49 [7849,8450]|
|BoundProjected:EPSG::7850_EPSG::8450|GDA2020 * GA-Aus 3m / Map Grid of Australia zone 50 [7850,8450]|GDA2020 * GA-Aus 3m / MGA zone 50 [7850,8450]|
|BoundProjected:EPSG::7851_EPSG::8450|GDA2020 * GA-Aus 3m / Map Grid of Australia zone 51 [7851,8450]|GDA2020 * GA-Aus 3m / MGA zone 51 [7851,8450]|
|BoundProjected:EPSG::7852_EPSG::8450|GDA2020 * GA-Aus 3m / Map Grid of Australia zone 52 [7852,8450]|GDA2020 * GA-Aus 3m / MGA zone 52 [7852,8450]|
|BoundProjected:EPSG::7853_EPSG::8450|GDA2020 * GA-Aus 3m / Map Grid of Australia zone 53 [7853,8450]|GDA2020 * GA-Aus 3m / MGA zone 53 [7853,8450]|
|BoundProjected:EPSG::7854_EPSG::8450|GDA2020 * GA-Aus 3m / Map Grid of Australia zone 54 [7854,8450]|GDA2020 * GA-Aus 3m / MGA zone 54 [7854,8450]|
|BoundProjected:EPSG::7855_EPSG::8450|GDA2020 * GA-Aus 3m / Map Grid of Australia zone 55 [7855,8450]|GDA2020 * GA-Aus 3m / MGA zone 55 [7855,8450]|
|BoundProjected:EPSG::7856_EPSG::8450|GDA2020 * GA-Aus 3m / Map Grid of Australia zone 56 [7856,8450]|GDA2020 * GA-Aus 3m / MGA zone 56 [7856,8450]|
|BoundProjected:EPSG::3112_EPSG::1150|GDA94 * EPSG-Aus / Geoscience Australia Standard National Scale Lambert Projection [3112,1150]|GDA94 * EPSG-Aus / Geoscience Australia Lambert [3112,1150]|
|BoundProjected:EPSG::28348_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 48 [28348,1150]|GDA94 * EPSG-Aus / MGA zone 48 [28348,1150]|
|BoundProjected:EPSG::28349_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 49 [28349,1150]|GDA94 * EPSG-Aus / MGA zone 49 [28349,1150]|
|BoundProjected:EPSG::28350_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 50 [28350,1150]|GDA94 * EPSG-Aus / MGA zone 50 [28350,1150]|
|BoundProjected:EPSG::28351_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 51 [28351,1150]|GDA94 * EPSG-Aus / MGA zone 51 [28351,1150]|
|BoundProjected:EPSG::28352_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 52 [28352,1150]|GDA94 * EPSG-Aus / MGA zone 52 [28352,1150]|
|BoundProjected:EPSG::28353_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 53 [28353,1150]|GDA94 * EPSG-Aus / MGA zone 53 [28353,1150]|
|BoundProjected:EPSG::28354_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 54 [28354,1150]|GDA94 * EPSG-Aus / MGA zone 54 [28354,1150]|
|BoundProjected:EPSG::28355_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 55 [28355,1150]|GDA94 * EPSG-Aus / MGA zone 55 [28355,1150]|
|BoundProjected:EPSG::28356_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 56 [28356,1150]|GDA94 * EPSG-Aus / MGA zone 56 [28356,1150]|
|BoundProjected:EPSG::28357_EPSG::1150|GDA94 * EPSG-Aus / Map Grid of Australia zone 57 [28357,1150]|GDA94 * EPSG-Aus / MGA zone 57 [28357,1150]|
|BoundProjected:EPSG::3107_EPSG::1150|GDA94 * EPSG-Aus / South Australia Lambert [3107,1150]|GDA94 * EPSG-Aus / SA Lambert [3107,1150]|
|BoundProjected:EPSG::5247_EPSG::6701|GDBD2009 * OGP-Brn / Borneo RSO [5247,6701]|GDBD2009 * OGP-Brn / Brunei BRSO [5247,6701]|
|BoundProjected:EPSG::2044_EPSG::1544|Hanoi 1972 * BP-Vnm / 6-degree Gauss-Kruger zone 18 [2044,1544]|Hanoi 1972 * BP-Vnm / Gauss-Kruger zone 18 [2044,1544]|
|BoundProjected:EPSG::2045_EPSG::1544|Hanoi 1972 * BP-Vnm / 6-degree Gauss-Kruger zone 19 [2045,1544]|Hanoi 1972 * BP-Vnm / Gauss-Kruger zone 19 [2045,1544]|
|BoundProjected:EPSG::2048_EPSG::1505|Hartebeesthoek94 * EPSG-Zaf / South African Survey Grid zone 19 [2048,1505]|Hartebeesthoek94 * EPSG-Zaf / Lo19 [2048,1505]|
|BoundProjected:EPSG::2051_EPSG::1505|Hartebeesthoek94 * EPSG-Zaf / South African Survey Grid zone 25 [2051,1505]|Hartebeesthoek94 * EPSG-Zaf / Lo25 [2051,1505]|
|BoundProjected:EPSG::23700_EPSG::1830|HD72 * EPSG-Hun / Egyseges Orszagos Vetuleti [23700,1830]|HD72 * EPSG-Hun / EOV [23700,1830]|
|BoundProjected:EPSG::2083_EPSG::1892|Hito XVIII 1963 * NIMA-Chl / Argentina zone 2 [2083,1892]|Hito XVIII 1963 * NIMA-Chl / Argentina 2 [2083,1892]|
|BoundProjected:EPSG::3340_EPSG::15787|IGCB 1955 * Tot-Cod / Congo Transverse Mercator zone 14 [3340,15787]|IGCB 1955 * Tot-Cod / Congo TM zone 14 [3340,15787]|
|BoundProjected:EPSG::3057_EPSG::1952|ISN93 * EPSG-Isl / Iceland Lambert 1993 [3057,1952]|ISN93 * EPSG-Isl / Lambert 1993 [3057,1952]|
|BoundProjected:EPSG::2039_EPSG::1073|Israel 1993 * SoI-Isr / Israeli TM [2039,1073]|Israel 1993 * SoI-Isr / Israeli TM Grid [2039,1073]|
|BoundProjected:EPSG::24375_EPSG::1155|Kalianpur 1937 * DMA-Bgd / India zone IIb (1937 metres) [24375,1155]|Kalianpur 1937 * DMA-Bgd / India zone IIb [24375,1155]|
|BoundProjected:EPSG::24376_EPSG::1247|Kalianpur 1962 * DMA-Pak / India zone I (1962 metres) [24376,1247]|Kalianpur 1962 * DMA-Pak / India zone I [24376,1247]|
|BoundProjected:EPSG::24377_EPSG::1247|Kalianpur 1962 * DMA-Pak / India zone IIa (1962 metres) [24377,1247]|Kalianpur 1962 * DMA-Pak / India zone IIa [24377,1247]|
|BoundProjected:EPSG::24377_EPSG::15701|Kalianpur 1962 * TFE-Pak Indus / India zone IIa (1962 metres) [24377,15701]|Kalianpur 1962 * TFE-Pak Indus / India zone IIa [24377,15701]|
|BoundProjected:EPSG::24377_EPSG::15703|Kalianpur 1962 * utp-Pak Karachi / India zone IIa (1962 metres) [24377,15703]|Kalianpur 1962 * utp-Pak Karachi / India zone IIa [24377,15703]|
|BoundProjected:EPSG::24379_EPSG::1156|Kalianpur 1975 * DMA-Ind Npl / India zone IIa (1975 metres) [24379,1156]|Kalianpur 1975 * DMA-Ind Npl / India zone IIa [24379,1156]|
|BoundProjected:EPSG::3116_EPSG::15738|MAGNA-SIRGAS * EPSG / Colombia MAGNA Bogota zone [3116,15738]|MAGNA-SIRGAS * EPSG / Colombia Bogota zone [3116,15738]|
|BoundProjected:EPSG::3117_EPSG::15738|MAGNA-SIRGAS * EPSG / Colombia MAGNA East Central zone [3117,15738]|MAGNA-SIRGAS * EPSG / Colombia East Central zone [3117,15738]|
|BoundProjected:EPSG::3118_EPSG::15738|MAGNA-SIRGAS * EPSG / Colombia MAGNA East zone [3118,15738]|MAGNA-SIRGAS * EPSG / Colombia East zone [3118,15738]|
|BoundProjected:EPSG::3114_EPSG::15738|MAGNA-SIRGAS * EPSG / Colombia MAGNA Far West zone [3114,15738]|MAGNA-SIRGAS * EPSG / Colombia Far West zone [3114,15738]|
|BoundProjected:EPSG::3115_EPSG::15738|MAGNA-SIRGAS * EPSG / Colombia MAGNA West zone [3115,15738]|MAGNA-SIRGAS * EPSG / Colombia West zone [3115,15738]|
|BoundProjected:EPSG::6372_EPSG::6373|Mexico ITRF2008 * OGP-Mex / Mexico LCC [6372,6373]|Mexico ITRF2008 * OGP-Mex / LCC [6372,6373]|
|BoundProjected:EPSG::26729_EPSG::15851|NAD27 * OGP-Usa Conus / Alabama CS27 East zone [26729,15851]|NAD27 * OGP-Usa Conus / Alabama East [26729,15851]|
|BoundProjected:EPSG::26730_EPSG::15851|NAD27 * OGP-Usa Conus / Alabama CS27 West zone [26730,15851]|NAD27 * OGP-Usa Conus / Alabama West [26730,15851]|
|BoundProjected:EPSG::26732_EPSG::15864|NAD27 * OGP-Usa AK / Alaska CS27 zone 2 [26732,15864]|NAD27 * OGP-Usa AK / Alaska zone 2 [26732,15864]|
|BoundProjected:EPSG::26733_EPSG::15864|NAD27 * OGP-Usa AK / Alaska CS27 zone 3 [26733,15864]|NAD27 * OGP-Usa AK / Alaska zone 3 [26733,15864]|
|BoundProjected:EPSG::26734_EPSG::15864|NAD27 * OGP-Usa AK / Alaska CS27 zone 4 [26734,15864]|NAD27 * OGP-Usa AK / Alaska zone 4 [26734,15864]|
|BoundProjected:EPSG::26735_EPSG::15864|NAD27 * OGP-Usa AK / Alaska CS27 zone 5 [26735,15864]|NAD27 * OGP-Usa AK / Alaska zone 5 [26735,15864]|
|BoundProjected:EPSG::26736_EPSG::15864|NAD27 * OGP-Usa AK / Alaska CS27 zone 6 [26736,15864]|NAD27 * OGP-Usa AK / Alaska zone 6 [26736,15864]|
|BoundProjected:EPSG::26737_EPSG::15864|NAD27 * OGP-Usa AK / Alaska CS27 zone 7 [26737,15864]|NAD27 * OGP-Usa AK / Alaska zone 7 [26737,15864]|
|BoundProjected:EPSG::26738_EPSG::15864|NAD27 * OGP-Usa AK / Alaska CS27 zone 8 [26738,15864]|NAD27 * OGP-Usa AK / Alaska zone 8 [26738,15864]|
|BoundProjected:EPSG::26749_EPSG::15851|NAD27 * OGP-Usa Conus / Arizona Coordinate System Central zone [26749,15851]|NAD27 * OGP-Usa Conus / Arizona Central [26749,15851]|
|BoundProjected:EPSG::26748_EPSG::15851|NAD27 * OGP-Usa Conus / Arizona Coordinate System East zone [26748,15851]|NAD27 * OGP-Usa Conus / Arizona East [26748,15851]|
|BoundProjected:EPSG::26750_EPSG::15851|NAD27 * OGP-Usa Conus / Arizona Coordinate System West zone [26750,15851]|NAD27 * OGP-Usa Conus / Arizona West [26750,15851]|
|BoundProjected:EPSG::26751_EPSG::15851|NAD27 * OGP-Usa Conus / Arkansas CS27 North [26751,15851]|NAD27 * OGP-Usa Conus / Arkansas North [26751,15851]|
|BoundProjected:EPSG::26752_EPSG::15851|NAD27 * OGP-Usa Conus / Arkansas CS27 South [26752,15851]|NAD27 * OGP-Usa Conus / Arkansas South [26752,15851]|
|BoundProjected:EPSG::4410_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 10N (US survey feet) [4410,15851]|NAD27 * OGP-Usa Conus / BLM 10N (ftUS) [4410,15851]|
|BoundProjected:EPSG::4411_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 11N (US survey feet) [4411,15851]|NAD27 * OGP-Usa Conus / BLM 11N (ftUS) [4411,15851]|
|BoundProjected:EPSG::4412_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 12N (US survey feet) [4412,15851]|NAD27 * OGP-Usa Conus / BLM 12N (ftUS) [4412,15851]|
|BoundProjected:EPSG::4413_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 13N (US survey feet) [4413,15851]|NAD27 * OGP-Usa Conus / BLM 13N (ftUS) [4413,15851]|
|BoundProjected:EPSG::32064_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 14N (US survey feet) [32064,15851]|NAD27 * OGP-Usa Conus / BLM 14N (ftUS) [32064,15851]|
|BoundProjected:EPSG::32065_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 15N (US survey feet) [32065,15851]|NAD27 * OGP-Usa Conus / BLM 15N (ftUS) [32065,15851]|
|BoundProjected:EPSG::32066_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 16N (US survey feet) [32066,15851]|NAD27 * OGP-Usa Conus / BLM 16N (ftUS) [32066,15851]|
|BoundProjected:EPSG::32067_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 17N (US survey feet) [32067,15851]|NAD27 * OGP-Usa Conus / BLM 17N (ftUS) [32067,15851]|
|BoundProjected:EPSG::4418_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 18N (US survey feet) [4418,15851]|NAD27 * OGP-Usa Conus / BLM 18N (ftUS) [4418,15851]|
|BoundProjected:EPSG::4419_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 19N (US survey feet) [4419,15851]|NAD27 * OGP-Usa Conus / BLM 19N (ftUS) [4419,15851]|
|BoundProjected:EPSG::4401_EPSG::15864|NAD27 * OGP-Usa AK / BLM zone 1N (US survey feet) [4401,15864]|NAD27 * OGP-Usa AK / BLM 1N (ftUS) [4401,15864]|
|BoundProjected:EPSG::4402_EPSG::15864|NAD27 * OGP-Usa AK / BLM zone 2N (US survey feet) [4402,15864]|NAD27 * OGP-Usa AK / BLM 2N (ftUS) [4402,15864]|
|BoundProjected:EPSG::4403_EPSG::15864|NAD27 * OGP-Usa AK / BLM zone 3N (US survey feet) [4403,15864]|NAD27 * OGP-Usa AK / BLM 3N (ftUS) [4403,15864]|
|BoundProjected:EPSG::4404_EPSG::15864|NAD27 * OGP-Usa AK / BLM zone 4N (US survey feet) [4404,15864]|NAD27 * OGP-Usa AK / BLM 4N (ftUS) [4404,15864]|
|BoundProjected:EPSG::4399_EPSG::15864|NAD27 * OGP-Usa AK / BLM zone 59N (US survey feet) [4399,15864]|NAD27 * OGP-Usa AK / BLM 59N (ftUS) [4399,15864]|
|BoundProjected:EPSG::4405_EPSG::15864|NAD27 * OGP-Usa AK / BLM zone 5N (US survey feet) [4405,15864]|NAD27 * OGP-Usa AK / BLM 5N (ftUS) [4405,15864]|
|BoundProjected:EPSG::4400_EPSG::15864|NAD27 * OGP-Usa AK / BLM zone 60N (US survey feet) [4400,15864]|NAD27 * OGP-Usa AK / BLM 60N (ftUS) [4400,15864]|
|BoundProjected:EPSG::4406_EPSG::1693|NAD27 * EPSG-Can / BLM zone 6N (US survey feet) [4406,1693]|NAD27 * EPSG-Can / BLM 6N (ftUS) [4406,1693]|
|BoundProjected:EPSG::4406_EPSG::15864|NAD27 * OGP-Usa AK / BLM zone 6N (US survey feet) [4406,15864]|NAD27 * OGP-Usa AK / BLM 6N (ftUS) [4406,15864]|
|BoundProjected:EPSG::4407_EPSG::15864|NAD27 * OGP-Usa AK / BLM zone 7N (US survey feet) [4407,15864]|NAD27 * OGP-Usa AK / BLM 7N (ftUS) [4407,15864]|
|BoundProjected:EPSG::4408_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 8N (US survey feet) [4408,15851]|NAD27 * OGP-Usa Conus / BLM 8N (ftUS) [4408,15851]|
|BoundProjected:EPSG::4409_EPSG::15851|NAD27 * OGP-Usa Conus / BLM zone 9N (US survey feet) [4409,15851]|NAD27 * OGP-Usa Conus / BLM 9N (ftUS) [4409,15851]|
|BoundProjected:EPSG::26741_EPSG::15851|NAD27 * OGP-Usa Conus / California CS27 zone I [26741,15851]|NAD27 * OGP-Usa Conus / California zone I [26741,15851]|
|BoundProjected:EPSG::26742_EPSG::15851|NAD27 * OGP-Usa Conus / California CS27 zone II [26742,15851]|NAD27 * OGP-Usa Conus / California zone II [26742,15851]|
|BoundProjected:EPSG::26743_EPSG::15851|NAD27 * OGP-Usa Conus / California CS27 zone III [26743,15851]|NAD27 * OGP-Usa Conus / California zone III [26743,15851]|
|BoundProjected:EPSG::26744_EPSG::15851|NAD27 * OGP-Usa Conus / California CS27 zone IV [26744,15851]|NAD27 * OGP-Usa Conus / California zone IV [26744,15851]|
|BoundProjected:EPSG::26745_EPSG::15851|NAD27 * OGP-Usa Conus / California CS27 zone V [26745,15851]|NAD27 * OGP-Usa Conus / California zone V [26745,15851]|
|BoundProjected:EPSG::26746_EPSG::15851|NAD27 * OGP-Usa Conus / California CS27 zone VI [26746,15851]|NAD27 * OGP-Usa Conus / California zone VI [26746,15851]|
|BoundProjected:EPSG::26754_EPSG::15851|NAD27 * OGP-Usa Conus / Colorado CS27 Central zone [26754,15851]|NAD27 * OGP-Usa Conus / Colorado Central [26754,15851]|
|BoundProjected:EPSG::26753_EPSG::15851|NAD27 * OGP-Usa Conus / Colorado CS27 North zone [26753,15851]|NAD27 * OGP-Usa Conus / Colorado North [26753,15851]|
|BoundProjected:EPSG::26755_EPSG::15851|NAD27 * OGP-Usa Conus / Colorado CS27 South zone [26755,15851]|NAD27 * OGP-Usa Conus / Colorado South [26755,15851]|
|BoundProjected:EPSG::26757_EPSG::15851|NAD27 * OGP-Usa Conus / Delaware CS27 [26757,15851]|NAD27 * OGP-Usa Conus / Delaware [26757,15851]|
|BoundProjected:EPSG::26758_EPSG::15851|NAD27 * OGP-Usa Conus / Florida CS27 East zone [26758,15851]|NAD27 * OGP-Usa Conus / Florida East [26758,15851]|
|BoundProjected:EPSG::26760_EPSG::15851|NAD27 * OGP-Usa Conus / Florida CS27 North zone [26760,15851]|NAD27 * OGP-Usa Conus / Florida North [26760,15851]|
|BoundProjected:EPSG::26759_EPSG::15851|NAD27 * OGP-Usa Conus / Florida CS27 West zone [26759,15851]|NAD27 * OGP-Usa Conus / Florida West [26759,15851]|
|BoundProjected:EPSG::26766_EPSG::15851|NAD27 * OGP-Usa Conus / Georgia CS27 East zone [26766,15851]|NAD27 * OGP-Usa Conus / Georgia East [26766,15851]|
|BoundProjected:EPSG::26767_EPSG::15851|NAD27 * OGP-Usa Conus / Georgia CS27 West zone [26767,15851]|NAD27 * OGP-Usa Conus / Georgia West [26767,15851]|
|BoundProjected:EPSG::26771_EPSG::15851|NAD27 * OGP-Usa Conus / Illinois CS27 East zone [26771,15851]|NAD27 * OGP-Usa Conus / Illinois East [26771,15851]|
|BoundProjected:EPSG::26772_EPSG::15851|NAD27 * OGP-Usa Conus / Illinois CS27 West zone [26772,15851]|NAD27 * OGP-Usa Conus / Illinois West [26772,15851]|
|BoundProjected:EPSG::26773_EPSG::15851|NAD27 * OGP-Usa Conus / Indiana CS27 East zone [26773,15851]|NAD27 * OGP-Usa Conus / Indiana East [26773,15851]|
|BoundProjected:EPSG::26774_EPSG::15851|NAD27 * OGP-Usa Conus / Indiana CS27 West zone [26774,15851]|NAD27 * OGP-Usa Conus / Indiana West [26774,15851]|
|BoundProjected:EPSG::26775_EPSG::15851|NAD27 * OGP-Usa Conus / Iowa CS27 North zone [26775,15851]|NAD27 * OGP-Usa Conus / Iowa North [26775,15851]|
|BoundProjected:EPSG::26776_EPSG::15851|NAD27 * OGP-Usa Conus / Iowa CS27 South zone [26776,15851]|NAD27 * OGP-Usa Conus / Iowa South [26776,15851]|
|BoundProjected:EPSG::26777_EPSG::15851|NAD27 * OGP-Usa Conus / Kansas CS27 North zone [26777,15851]|NAD27 * OGP-Usa Conus / Kansas North [26777,15851]|
|BoundProjected:EPSG::26778_EPSG::15851|NAD27 * OGP-Usa Conus / Kansas CS27 South zone [26778,15851]|NAD27 * OGP-Usa Conus / Kansas South [26778,15851]|
|BoundProjected:EPSG::26779_EPSG::15851|NAD27 * OGP-Usa Conus / Kentucky CS27 North zone [26779,15851]|NAD27 * OGP-Usa Conus / Kentucky North [26779,15851]|
|BoundProjected:EPSG::26780_EPSG::15851|NAD27 * OGP-Usa Conus / Kentucky CS27 South zone [26780,15851]|NAD27 * OGP-Usa Conus / Kentucky South [26780,15851]|
|BoundProjected:EPSG::26781_EPSG::15851|NAD27 * OGP-Usa Conus / Louisiana CS27 North zone [26781,15851]|NAD27 * OGP-Usa Conus / Louisiana North [26781,15851]|
|BoundProjected:EPSG::32099_EPSG::15851|NAD27 * OGP-Usa Conus / Louisiana CS27 Offshore zone [32099,15851]|NAD27 * OGP-Usa Conus / Louisiana Offshore [32099,15851]|
|BoundProjected:EPSG::26782_EPSG::15851|NAD27 * OGP-Usa Conus / Louisiana CS27 South zone [26782,15851]|NAD27 * OGP-Usa Conus / Louisiana South [26782,15851]|
|BoundProjected:EPSG::6201_EPSG::15851|NAD27 * OGP-Usa Conus / Michigan CS27 Central zone [6201,15851]|NAD27 * OGP-Usa Conus / Michigan Central [6201,15851]|
|BoundProjected:EPSG::6202_EPSG::15851|NAD27 * OGP-Usa Conus / Michigan CS27 South zone [6202,15851]|NAD27 * OGP-Usa Conus / Michigan South [6202,15851]|
|BoundProjected:EPSG::26794_EPSG::15851|NAD27 * OGP-Usa Conus / Mississippi CS27 East zone [26794,15851]|NAD27 * OGP-Usa Conus / Mississippi East [26794,15851]|
|BoundProjected:EPSG::26795_EPSG::15851|NAD27 * OGP-Usa Conus / Mississippi CS27 West zone [26795,15851]|NAD27 * OGP-Usa Conus / Mississippi West [26795,15851]|
|BoundProjected:EPSG::32002_EPSG::15851|NAD27 * OGP-Usa Conus / Montana CS27 Central zone [32002,15851]|NAD27 * OGP-Usa Conus / Montana Central [32002,15851]|
|BoundProjected:EPSG::32001_EPSG::15851|NAD27 * OGP-Usa Conus / Montana CS27 North zone [32001,15851]|NAD27 * OGP-Usa Conus / Montana North [32001,15851]|
|BoundProjected:EPSG::32003_EPSG::15851|NAD27 * OGP-Usa Conus / Montana CS27 South zone [32003,15851]|NAD27 * OGP-Usa Conus / Montana South [32003,15851]|
|BoundProjected:EPSG::32005_EPSG::15851|NAD27 * OGP-Usa Conus / Nebraska CS27 North zone [32005,15851]|NAD27 * OGP-Usa Conus / Nebraska North [32005,15851]|
|BoundProjected:EPSG::32006_EPSG::15851|NAD27 * OGP-Usa Conus / Nebraska CS27 South zone [32006,15851]|NAD27 * OGP-Usa Conus / Nebraska South [32006,15851]|
|BoundProjected:EPSG::32007_EPSG::15851|NAD27 * OGP-Usa Conus / Nevada CS27 East zone [32007,15851]|NAD27 * OGP-Usa Conus / Nevada East [32007,15851]|
|BoundProjected:EPSG::32013_EPSG::15851|NAD27 * OGP-Usa Conus / New Mexico CS27 Central zone [32013,15851]|NAD27 * OGP-Usa Conus / New Mexico Central [32013,15851]|
|BoundProjected:EPSG::32012_EPSG::15851|NAD27 * OGP-Usa Conus / New Mexico CS27 East zone [32012,15851]|NAD27 * OGP-Usa Conus / New Mexico East [32012,15851]|
|BoundProjected:EPSG::32014_EPSG::15851|NAD27 * OGP-Usa Conus / New Mexico CS27 West zone [32014,15851]|NAD27 * OGP-Usa Conus / New Mexico West [32014,15851]|
|BoundProjected:EPSG::32016_EPSG::15851|NAD27 * OGP-Usa Conus / New York CS27 Central zone [32016,15851]|NAD27 * OGP-Usa Conus / New York Central [32016,15851]|
|BoundProjected:EPSG::32020_EPSG::15851|NAD27 * OGP-Usa Conus / North Dakota CS27 North zone [32020,15851]|NAD27 * OGP-Usa Conus / North Dakota North [32020,15851]|
|BoundProjected:EPSG::32021_EPSG::15851|NAD27 * OGP-Usa Conus / North Dakota CS27 South zone [32021,15851]|NAD27 * OGP-Usa Conus / North Dakota South [32021,15851]|
|BoundProjected:EPSG::32022_EPSG::15851|NAD27 * OGP-Usa Conus / Ohio CS27 North zone [32022,15851]|NAD27 * OGP-Usa Conus / Ohio North [32022,15851]|
|BoundProjected:EPSG::32023_EPSG::15851|NAD27 * OGP-Usa Conus / Ohio CS27 South zone [32023,15851]|NAD27 * OGP-Usa Conus / Ohio South [32023,15851]|
|BoundProjected:EPSG::32024_EPSG::15851|NAD27 * OGP-Usa Conus / Oklahoma CS27 North zone [32024,15851]|NAD27 * OGP-Usa Conus / Oklahoma North [32024,15851]|
|BoundProjected:EPSG::32025_EPSG::15851|NAD27 * OGP-Usa Conus / Oklahoma CS27 South zone [32025,15851]|NAD27 * OGP-Usa Conus / Oklahoma South [32025,15851]|
|BoundProjected:EPSG::32028_EPSG::15851|NAD27 * OGP-Usa Conus / Pennsylvania CS27 North zone [32028,15851]|NAD27 * OGP-Usa Conus / Pennsylvania North [32028,15851]|
|BoundProjected:EPSG::4455_EPSG::15851|NAD27 * OGP-Usa Conus / Pennsylvania CS27 South zone [4455,15851]|NAD27 * OGP-Usa Conus / Pennsylvania South [4455,15851]|
|BoundProjected:EPSG::32031_EPSG::15851|NAD27 * OGP-Usa Conus / South Carolina CS27 North zone [32031,15851]|NAD27 * OGP-Usa Conus / South Carolina North [32031,15851]|
|BoundProjected:EPSG::32033_EPSG::15851|NAD27 * OGP-Usa Conus / South Carolina CS27 South zone [32033,15851]|NAD27 * OGP-Usa Conus / South Carolina South [32033,15851]|
|BoundProjected:EPSG::32034_EPSG::15851|NAD27 * OGP-Usa Conus / South Dakota CS27 North zone [32034,15851]|NAD27 * OGP-Usa Conus / South Dakota North [32034,15851]|
|BoundProjected:EPSG::32035_EPSG::15851|NAD27 * OGP-Usa Conus / South Dakota CS27 South zone [32035,15851]|NAD27 * OGP-Usa Conus / South Dakota South [32035,15851]|
|BoundProjected:EPSG::32039_EPSG::15851|NAD27 * OGP-Usa Conus / Texas CS27 Central zone [32039,15851]|NAD27 * OGP-Usa Conus / Texas Central [32039,15851]|
|BoundProjected:EPSG::32037_EPSG::15851|NAD27 * OGP-Usa Conus / Texas CS27 North zone [32037,15851]|NAD27 * OGP-Usa Conus / Texas North [32037,15851]|
|BoundProjected:EPSG::32038_EPSG::15851|NAD27 * OGP-Usa Conus / Texas CS27 North Central zone [32038,15851]|NAD27 * OGP-Usa Conus / Texas North Central [32038,15851]|
|BoundProjected:EPSG::32041_EPSG::15851|NAD27 * OGP-Usa Conus / Texas CS27 South zone [32041,15851]|NAD27 * OGP-Usa Conus / Texas South [32041,15851]|
|BoundProjected:EPSG::32040_EPSG::15851|NAD27 * OGP-Usa Conus / Texas CS27 South Central zone [32040,15851]|NAD27 * OGP-Usa Conus / Texas South Central [32040,15851]|
|BoundProjected:EPSG::32043_EPSG::15851|NAD27 * OGP-Usa Conus / Utah CS27 Central zone [32043,15851]|NAD27 * OGP-Usa Conus / Utah Central [32043,15851]|
|BoundProjected:EPSG::32042_EPSG::15851|NAD27 * OGP-Usa Conus / Utah CS27 North zone [32042,15851]|NAD27 * OGP-Usa Conus / Utah North [32042,15851]|
|BoundProjected:EPSG::32044_EPSG::15851|NAD27 * OGP-Usa Conus / Utah CS27 South zone [32044,15851]|NAD27 * OGP-Usa Conus / Utah South [32044,15851]|
|BoundProjected:EPSG::32046_EPSG::15851|NAD27 * OGP-Usa Conus / Virginia CS27 North zone [32046,15851]|NAD27 * OGP-Usa Conus / Virginia North [32046,15851]|
|BoundProjected:EPSG::32047_EPSG::15851|NAD27 * OGP-Usa Conus / Virginia CS27 South zone [32047,15851]|NAD27 * OGP-Usa Conus / Virginia South [32047,15851]|
|BoundProjected:EPSG::32048_EPSG::15851|NAD27 * OGP-Usa Conus / Washington CS27 North zone [32048,15851]|NAD27 * OGP-Usa Conus / Washington North [32048,15851]|
|BoundProjected:EPSG::32050_EPSG::15851|NAD27 * OGP-Usa Conus / West Virginia CS27 North zone [32050,15851]|NAD27 * OGP-Usa Conus / West Virginia North [32050,15851]|
|BoundProjected:EPSG::32051_EPSG::15851|NAD27 * OGP-Usa Conus / West Virginia CS27 South zone [32051,15851]|NAD27 * OGP-Usa Conus / West Virginia South [32051,15851]|
|BoundProjected:EPSG::32055_EPSG::15851|NAD27 * OGP-Usa Conus / Wyoming CS27 East zone [32055,15851]|NAD27 * OGP-Usa Conus / Wyoming East [32055,15851]|
|BoundProjected:EPSG::32056_EPSG::15851|NAD27 * OGP-Usa Conus / Wyoming CS27 East Central zone [32056,15851]|NAD27 * OGP-Usa Conus / Wyoming East Central [32056,15851]|
|BoundProjected:EPSG::32058_EPSG::15851|NAD27 * OGP-Usa Conus / Wyoming CS27 West zone [32058,15851]|NAD27 * OGP-Usa Conus / Wyoming West [32058,15851]|
|BoundProjected:EPSG::32057_EPSG::15851|NAD27 * OGP-Usa Conus / Wyoming CS27 West Central zone [32057,15851]|NAD27 * OGP-Usa Conus / Wyoming West Central [32057,15851]|
|BoundProjected:EPSG::26929_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Alabama East zone (meters) [26929,1188]|NAD83 * DMA-N Am / Alabama East [26929,1188]|
|BoundProjected:EPSG::26930_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Alabama West zone (meters) [26930,1188]|NAD83 * DMA-N Am / Alabama West [26930,1188]|
|BoundProjected:EPSG::3338_EPSG::1188|NAD83 * DMA-N Am / Alaska Albers (meters) [3338,1188]|NAD83 * DMA-N Am / Alaska Albers [3338,1188]|
|BoundProjected:EPSG::26934_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Alaska zone 4 (meters) [26934,1188]|NAD83 * DMA-N Am / Alaska zone 4 [26934,1188]|
|BoundProjected:EPSG::26935_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Alaska zone 5 (meters) [26935,1188]|NAD83 * DMA-N Am / Alaska zone 5 [26935,1188]|
|BoundProjected:EPSG::3400_EPSG::1188|NAD83 * DMA-N Am / Alberta 10-degree TM (Forest) [3400,1188]|NAD83 * DMA-N Am / Alberta 10-TM (Forest) [3400,1188]|
|BoundProjected:EPSG::26949_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Arizona Central zone (meters) [26949,1188]|NAD83 * DMA-N Am / Arizona Central [26949,1188]|
|BoundProjected:EPSG::26948_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Arizona East zone (meters) [26948,1188]|NAD83 * DMA-N Am / Arizona East [26948,1188]|
|BoundProjected:EPSG::26950_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Arizona West zone (meters) [26950,1188]|NAD83 * DMA-N Am / Arizona West [26950,1188]|
|BoundProjected:EPSG::4433_EPSG::1188|NAD83 * DMA-N Am / BLM zone 13N (US survey feet) [4433,1188]|NAD83 * DMA-N Am / BLM 13N (ftUS) [4433,1188]|
|BoundProjected:EPSG::32164_EPSG::1188|NAD83 * DMA-N Am / BLM zone 14N (US survey feet) [32164,1188]|NAD83 * DMA-N Am / BLM 14N (ftUS) [32164,1188]|
|BoundProjected:EPSG::32165_EPSG::1188|NAD83 * DMA-N Am / BLM zone 15N (US survey feet) [32165,1188]|NAD83 * DMA-N Am / BLM 15N (ftUS) [32165,1188]|
|BoundProjected:EPSG::32166_EPSG::1188|NAD83 * DMA-N Am / BLM zone 16N (US survey feet) [32166,1188]|NAD83 * DMA-N Am / BLM 16N (ftUS) [32166,1188]|
|BoundProjected:EPSG::32167_EPSG::1188|NAD83 * DMA-N Am / BLM zone 17N (US survey feet) [32167,1188]|NAD83 * DMA-N Am / BLM 17N (ftUS) [32167,1188]|
|BoundProjected:EPSG::4438_EPSG::1188|NAD83 * DMA-N Am / BLM zone 18N (US survey feet) [4438,1188]|NAD83 * DMA-N Am / BLM 18N (ftUS) [4438,1188]|
|BoundProjected:EPSG::4439_EPSG::1188|NAD83 * DMA-N Am / BLM zone 19N (US survey feet) [4439,1188]|NAD83 * DMA-N Am / BLM 19N (ftUS) [4439,1188]|
|BoundProjected:EPSG::4423_EPSG::1188|NAD83 * DMA-N Am / BLM zone 3N (US survey feet) [4423,1188]|NAD83 * DMA-N Am / BLM 3N (ftUS) [4423,1188]|
|BoundProjected:EPSG::4426_EPSG::1188|NAD83 * DMA-N Am / BLM zone 6N (US survey feet) [4426,1188]|NAD83 * DMA-N Am / BLM 6N (ftUS) [4426,1188]|
|BoundProjected:EPSG::26941_EPSG::1188|NAD83 * DMA-N Am / SPCS83 California zone 1 (meters) [26941,1188]|NAD83 * DMA-N Am / California zone 1 [26941,1188]|
|BoundProjected:EPSG::26942_EPSG::1188|NAD83 * DMA-N Am / SPCS83 California zone 2 (meters) [26942,1188]|NAD83 * DMA-N Am / California zone 2 [26942,1188]|
|BoundProjected:EPSG::26943_EPSG::1188|NAD83 * DMA-N Am / SPCS83 California zone 3 (meters) [26943,1188]|NAD83 * DMA-N Am / California zone 3 [26943,1188]|
|BoundProjected:EPSG::2227_EPSG::1188|NAD83 * DMA-N Am / SPCS83 California zone 3 (US Survey feet) [2227,1188]|NAD83 * DMA-N Am / California zone 3 (ftUS) [2227,1188]|
|BoundProjected:EPSG::26944_EPSG::1188|NAD83 * DMA-N Am / SPCS83 California zone 4 (meters) [26944,1188]|NAD83 * DMA-N Am / California zone 4 [26944,1188]|
|BoundProjected:EPSG::26945_EPSG::1188|NAD83 * DMA-N Am / SPCS83 California zone 5 (meters) [26945,1188]|NAD83 * DMA-N Am / California zone 5 [26945,1188]|
|BoundProjected:EPSG::2229_EPSG::1188|NAD83 * DMA-N Am / SPCS83 California zone 5 (US Survey feet) [2229,1188]|NAD83 * DMA-N Am / California zone 5 (ftUS) [2229,1188]|
|BoundProjected:EPSG::26946_EPSG::1188|NAD83 * DMA-N Am / SPCS83 California zone 6 (meters) [26946,1188]|NAD83 * DMA-N Am / California zone 6 [26946,1188]|
|BoundProjected:EPSG::26954_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Colorado Central zone (meters) [26954,1188]|NAD83 * DMA-N Am / Colorado Central [26954,1188]|
|BoundProjected:EPSG::26953_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Colorado North zone (meters) [26953,1188]|NAD83 * DMA-N Am / Colorado North [26953,1188]|
|BoundProjected:EPSG::2231_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Colorado North zone (US Survey feet) [2231,1188]|NAD83 * DMA-N Am / Colorado North (ftUS) [2231,1188]|
|BoundProjected:EPSG::26955_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Colorado South zone (meters) [26955,1188]|NAD83 * DMA-N Am / Colorado South [26955,1188]|
|BoundProjected:EPSG::26958_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Florida East zone (meters) [26958,1188]|NAD83 * DMA-N Am / Florida East [26958,1188]|
|BoundProjected:EPSG::26960_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Florida North zone (meters) [26960,1188]|NAD83 * DMA-N Am / Florida North [26960,1188]|
|BoundProjected:EPSG::26959_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Florida West zone (meters) [26959,1188]|NAD83 * DMA-N Am / Florida West [26959,1188]|
|BoundProjected:EPSG::26966_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Georgia East zone (meters) [26966,1188]|NAD83 * DMA-N Am / Georgia East [26966,1188]|
|BoundProjected:EPSG::26967_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Georgia West zone (meters) [26967,1188]|NAD83 * DMA-N Am / Georgia West [26967,1188]|
|BoundProjected:EPSG::3435_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Illinois East zone (US Survey feet) [3435,1188]|NAD83 * DMA-N Am / Illinois East (ftUS) [3435,1188]|
|BoundProjected:EPSG::3436_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Illinois West zone (US Survey feet) [3436,1188]|NAD83 * DMA-N Am / Illinois West (ftUS) [3436,1188]|
|BoundProjected:EPSG::2966_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Indiana West zone (US Survey feet) [2966,1188]|NAD83 * DMA-N Am / Indiana West (ftUS) [2966,1188]|
|BoundProjected:EPSG::26975_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Iowa North zone (meters) [26975,1188]|NAD83 * DMA-N Am / Iowa North [26975,1188]|
|BoundProjected:EPSG::26976_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Iowa South zone (meters) [26976,1188]|NAD83 * DMA-N Am / Iowa South [26976,1188]|
|BoundProjected:EPSG::26977_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Kansas North zone (meters) [26977,1188]|NAD83 * DMA-N Am / Kansas North [26977,1188]|
|BoundProjected:EPSG::3419_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Kansas North zone (US Survey feet) [3419,1188]|NAD83 * DMA-N Am / Kansas North (ftUS) [3419,1188]|
|BoundProjected:EPSG::26978_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Kansas South zone (meters) [26978,1188]|NAD83 * DMA-N Am / Kansas South [26978,1188]|
|BoundProjected:EPSG::3420_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Kansas South zone (US Survey feet) [3420,1188]|NAD83 * DMA-N Am / Kansas South (ftUS) [3420,1188]|
|BoundProjected:EPSG::2205_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Kentucky North zone (meters) [2205,1188]|NAD83 * DMA-N Am / Kentucky North [2205,1188]|
|BoundProjected:EPSG::26980_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Kentucky South zone (meters) [26980,1188]|NAD83 * DMA-N Am / Kentucky South [26980,1188]|
|BoundProjected:EPSG::26981_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Louisiana North zone (meters) [26981,1188]|NAD83 * DMA-N Am / Louisiana North [26981,1188]|
|BoundProjected:EPSG::3451_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Louisiana North zone (US Survey feet) [3451,1188]|NAD83 * DMA-N Am / Louisiana North (ftUS) [3451,1188]|
|BoundProjected:EPSG::26982_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Louisiana South zone (meters) [26982,1188]|NAD83 * DMA-N Am / Louisiana South [26982,1188]|
|BoundProjected:EPSG::3452_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Louisiana South zone (US Survey feet) [3452,1188]|NAD83 * DMA-N Am / Louisiana South (ftUS) [3452,1188]|
|BoundProjected:EPSG::26994_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Mississippi East zone (meters) [26994,1188]|NAD83 * DMA-N Am / Mississippi East [26994,1188]|
|BoundProjected:EPSG::2254_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Mississippi East zone (US Survey feet) [2254,1188]|NAD83 * DMA-N Am / Mississippi East (ftUS) [2254,1188]|
|BoundProjected:EPSG::26995_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Mississippi West zone (meters) [26995,1188]|NAD83 * DMA-N Am / Mississippi West [26995,1188]|
|BoundProjected:EPSG::32100_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Montana zone (meters) [32100,1188]|NAD83 * DMA-N Am / Montana [32100,1188]|
|BoundProjected:EPSG::32104_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Nebraska zone (meters) [32104,1188]|NAD83 * DMA-N Am / Nebraska [32104,1188]|
|BoundProjected:EPSG::26852_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Nebraska zone (US Survey feet) [26852,1188]|NAD83 * DMA-N Am / Nebraska (ftUS) [26852,1188]|
|BoundProjected:EPSG::32113_EPSG::1188|NAD83 * DMA-N Am / SPCS83 New Mexico Central zone (meters) [32113,1188]|NAD83 * DMA-N Am / New Mexico Central [32113,1188]|
|BoundProjected:EPSG::32112_EPSG::1188|NAD83 * DMA-N Am / SPCS83 New Mexico East zone (meters) [32112,1188]|NAD83 * DMA-N Am / New Mexico East [32112,1188]|
|BoundProjected:EPSG::32114_EPSG::1188|NAD83 * DMA-N Am / SPCS83 New Mexico West zone (meters) [32114,1188]|NAD83 * DMA-N Am / New Mexico West [32114,1188]|
|BoundProjected:EPSG::32120_EPSG::1188|NAD83 * DMA-N Am / SPCS83 North Dakota North zone (meters) [32120,1188]|NAD83 * DMA-N Am / North Dakota North [32120,1188]|
|BoundProjected:EPSG::32121_EPSG::1188|NAD83 * DMA-N Am / SPCS83 North Dakota South zone (meters) [32121,1188]|NAD83 * DMA-N Am / North Dakota South [32121,1188]|
|BoundProjected:EPSG::3580_EPSG::1188|NAD83 * DMA-N Am / Northwest Territories Lambert [3580,1188]|NAD83 * DMA-N Am / NWT Lambert [3580,1188]|
|BoundProjected:EPSG::32122_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Ohio North zone (meters) [32122,1188]|NAD83 * DMA-N Am / Ohio North [32122,1188]|
|BoundProjected:EPSG::32123_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Ohio South zone (meters) [32123,1188]|NAD83 * DMA-N Am / Ohio South [32123,1188]|
|BoundProjected:EPSG::32124_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Oklahoma North zone (meters) [32124,1188]|NAD83 * DMA-N Am / Oklahoma North [32124,1188]|
|BoundProjected:EPSG::32125_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Oklahoma South zone (meters) [32125,1188]|NAD83 * DMA-N Am / Oklahoma South [32125,1188]|
|BoundProjected:EPSG::32128_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Pennsylvania North zone (meters) [32128,1188]|NAD83 * DMA-N Am / Pennsylvania North [32128,1188]|
|BoundProjected:EPSG::2271_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Pennsylvania North zone (US Survey feet) [2271,1188]|NAD83 * DMA-N Am / Pennsylvania North (ftUS) [2271,1188]|
|BoundProjected:EPSG::2272_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Pennsylvania South zone (US Survey feet) [2272,1188]|NAD83 * DMA-N Am / Pennsylvania South (ftUS) [2272,1188]|
|BoundProjected:EPSG::32133_EPSG::1188|NAD83 * DMA-N Am / SPCS83 South Carolina zone (meters) [32133,1188]|NAD83 * DMA-N Am / South Carolina [32133,1188]|
|BoundProjected:EPSG::32134_EPSG::1188|NAD83 * DMA-N Am / SPCS83 South Dakota North zone (meters) [32134,1188]|NAD83 * DMA-N Am / South Dakota North [32134,1188]|
|BoundProjected:EPSG::32135_EPSG::1188|NAD83 * DMA-N Am / SPCS83 South Dakota South zone (meters) [32135,1188]|NAD83 * DMA-N Am / South Dakota South [32135,1188]|
|BoundProjected:EPSG::32139_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Texas Central zone (meters) [32139,1188]|NAD83 * DMA-N Am / Texas Central [32139,1188]|
|BoundProjected:EPSG::2277_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Texas Central zone (US Survey feet) [2277,1188]|NAD83 * DMA-N Am / Texas Central (ftUS) [2277,1188]|
|BoundProjected:EPSG::32137_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Texas North zone (meters) [32137,1188]|NAD83 * DMA-N Am / Texas North [32137,1188]|
|BoundProjected:EPSG::32138_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Texas North Central zone (meters) [32138,1188]|NAD83 * DMA-N Am / Texas North Central [32138,1188]|
|BoundProjected:EPSG::2276_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Texas North Central zone (US Survey feet) [2276,1188]|NAD83 * DMA-N Am / Texas North Central (ftUS) [2276,1188]|
|BoundProjected:EPSG::32141_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Texas South zone (meters) [32141,1188]|NAD83 * DMA-N Am / Texas South [32141,1188]|
|BoundProjected:EPSG::2279_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Texas South zone (US Survey feet) [2279,1188]|NAD83 * DMA-N Am / Texas South (ftUS) [2279,1188]|
|BoundProjected:EPSG::32140_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Texas South Central zone (meters) [32140,1188]|NAD83 * DMA-N Am / Texas South Central [32140,1188]|
|BoundProjected:EPSG::2278_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Texas South Central zone (US Survey feet) [2278,1188]|NAD83 * DMA-N Am / Texas South Central (ftUS) [2278,1188]|
|BoundProjected:EPSG::32143_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Utah Central zone (meters) [32143,1188]|NAD83 * DMA-N Am / Utah Central [32143,1188]|
|BoundProjected:EPSG::32142_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Utah North zone (meters) [32142,1188]|NAD83 * DMA-N Am / Utah North [32142,1188]|
|BoundProjected:EPSG::32144_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Utah South zone (meters) [32144,1188]|NAD83 * DMA-N Am / Utah South [32144,1188]|
|BoundProjected:EPSG::32146_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Virginia North zone (meters) [32146,1188]|NAD83 * DMA-N Am / Virginia North [32146,1188]|
|BoundProjected:EPSG::32147_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Virginia South zone (meters) [32147,1188]|NAD83 * DMA-N Am / Virginia South [32147,1188]|
|BoundProjected:EPSG::2285_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Washington North zone (US Survey feet) [2285,1188]|NAD83 * DMA-N Am / Washington North (ftUS) [2285,1188]|
|BoundProjected:EPSG::2286_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Washington South zone (US Survey feet) [2286,1188]|NAD83 * DMA-N Am / Washington South (ftUS) [2286,1188]|
|BoundProjected:EPSG::32150_EPSG::1188|NAD83 * DMA-N Am / SPCS83 West Virginia North zone (meters) [32150,1188]|NAD83 * DMA-N Am / West Virginia North [32150,1188]|
|BoundProjected:EPSG::32151_EPSG::1188|NAD83 * DMA-N Am / SPCS83 West Virginia South zone (meters) [32151,1188]|NAD83 * DMA-N Am / West Virginia South [32151,1188]|
|BoundProjected:EPSG::32155_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Wyoming East zone (meters) [32155,1188]|NAD83 * DMA-N Am / Wyoming East [32155,1188]|
|BoundProjected:EPSG::3736_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Wyoming East zone (US Survey feet) [3736,1188]|NAD83 * DMA-N Am / Wyoming East (ftUS) [3736,1188]|
|BoundProjected:EPSG::3737_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Wyoming East Central zone (US Survey feet) [3737,1188]|NAD83 * DMA-N Am / Wyoming East Central (ftUS) [3737,1188]|
|BoundProjected:EPSG::32158_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Wyoming West zone (meters) [32158,1188]|NAD83 * DMA-N Am / Wyoming West [32158,1188]|
|BoundProjected:EPSG::3739_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Wyoming West zone (US Survey feet) [3739,1188]|NAD83 * DMA-N Am / Wyoming West (ftUS) [3739,1188]|
|BoundProjected:EPSG::32157_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Wyoming West Central zone (meters) [32157,1188]|NAD83 * DMA-N Am / Wyoming West Central [32157,1188]|
|BoundProjected:EPSG::3738_EPSG::1188|NAD83 * DMA-N Am / SPCS83 Wyoming West Central zone (US Survey feet) [3738,1188]|NAD83 * DMA-N Am / Wyoming West Central (ftUS) [3738,1188]|
|BoundProjected:EPSG::3402_EPSG::1842|NAD83(CSRS) * EPSG-Can / Alberta 10-degree TM (Forest) [3402,1842]|NAD83(CSRS) * EPSG-Can / Alberta 10-TM (Forest) [3402,1842]|
|BoundProjected:EPSG::3851_EPSG::1565|NZGD2000 * OSG-Nzl / New Zealand Continental Shelf Lambert Conformal 2000 [3851,1565]|NZGD2000 * OSG-Nzl / NZCS2000 [3851,1565]|
|BoundProjected:EPSG::5344_EPSG::5351|POSGAR 2007 * EPSG-Arg / Argentina zone 2 [5344,5351]|POSGAR 2007 * EPSG-Arg / Argentina 2 [5344,5351]|
|BoundProjected:EPSG::5348_EPSG::5351|POSGAR 2007 * EPSG-Arg / Argentina zone 6 [5348,5351]|POSGAR 2007 * EPSG-Arg / Argentina 6 [5348,5351]|
|BoundProjected:EPSG::22181_EPSG::1210|POSGAR 94 * EPSG-Arg / Argentina zone 1 [22181,1210]|POSGAR 94 * EPSG-Arg / Argentina 1 [22181,1210]|
|BoundProjected:EPSG::22182_EPSG::1210|POSGAR 94 * EPSG-Arg / Argentina zone 2 [22182,1210]|POSGAR 94 * EPSG-Arg / Argentina 2 [22182,1210]|
|BoundProjected:EPSG::22183_EPSG::1210|POSGAR 94 * EPSG-Arg / Argentina zone 3 [22183,1210]|POSGAR 94 * EPSG-Arg / Argentina 3 [22183,1210]|
|BoundProjected:EPSG::22184_EPSG::1210|POSGAR 94 * EPSG-Arg / Argentina zone 4 [22184,1210]|POSGAR 94 * EPSG-Arg / Argentina 4 [22184,1210]|
|BoundProjected:EPSG::22185_EPSG::1210|POSGAR 94 * EPSG-Arg / Argentina zone 5 [22185,1210]|POSGAR 94 * EPSG-Arg / Argentina 5 [22185,1210]|
|BoundProjected:EPSG::22186_EPSG::1210|POSGAR 94 * EPSG-Arg / Argentina zone 6 [22186,1210]|POSGAR 94 * EPSG-Arg / Argentina 6 [22186,1210]|
|BoundProjected:EPSG::22187_EPSG::1210|POSGAR 94 * EPSG-Arg / Argentina zone 7 [22187,1210]|POSGAR 94 * EPSG-Arg / Argentina 7 [22187,1210]|
|BoundProjected:EPSG::3121_EPSG::15708|PRS92 * CGS-Phl / Philippines zone I [3121,15708]|PRS92 * CGS-Phl / Philippines zone 1 [3121,15708]|
|BoundProjected:EPSG::3122_EPSG::15708|PRS92 * CGS-Phl / Philippines zone II [3122,15708]|PRS92 * CGS-Phl / Philippines zone 2 [3122,15708]|
|BoundProjected:EPSG::3123_EPSG::15708|PRS92 * CGS-Phl / Philippines zone III [3123,15708]|PRS92 * CGS-Phl / Philippines zone 3 [3123,15708]|
|BoundProjected:EPSG::3124_EPSG::15708|PRS92 * CGS-Phl / Philippines zone IV [3124,15708]|PRS92 * CGS-Phl / Philippines zone 4 [3124,15708]|
|BoundProjected:EPSG::3125_EPSG::15708|PRS92 * CGS-Phl / Philippines zone V [3125,15708]|PRS92 * CGS-Phl / Philippines zone 5 [3125,15708]|
|BoundProjected:EPSG::2611_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 105E [2611,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 105E [2611,1254]|
|BoundProjected:EPSG::2613_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 111E [2613,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 111E [2613,1254]|
|BoundProjected:EPSG::2613_EPSG::5044|Pulkovo 1942 * GOST-Rus 2008 / Gauss-Kruger CM 111E [2613,5044]|Pulkovo 1942 * GOST-Rus 2008 / 3-degree Gauss-Kruger CM 111E [2613,5044]|
|BoundProjected:EPSG::2615_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 117E [2615,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 117E [2615,1254]|
|BoundProjected:EPSG::2617_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 123E [2617,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 123E [2617,1254]|
|BoundProjected:EPSG::2617_EPSG::5044|Pulkovo 1942 * GOST-Rus 2008 / Gauss-Kruger CM 123E [2617,5044]|Pulkovo 1942 * GOST-Rus 2008 / 3-degree Gauss-Kruger CM 123E [2617,5044]|
|BoundProjected:EPSG::2588_EPSG::1807|Pulkovo 1942 * BP-Aze Aioc95 / Gauss-Kruger CM 39E [2588,1807]|Pulkovo 1942 * BP-Aze Aioc95 / 3-degree Gauss-Kruger CM 39E [2588,1807]|
|BoundProjected:EPSG::2590_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 45E [2590,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 45E [2590,1254]|
|BoundProjected:EPSG::2592_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 51E [2592,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 51E [2592,1254]|
|BoundProjected:EPSG::2592_EPSG::1303|Pulkovo 1942 * KCS-Kaz Cas / Gauss-Kruger CM 51E [2592,1303]|Pulkovo 1942 * KCS-Kaz Cas / 3-degree Gauss-Kruger CM 51E [2592,1303]|
|BoundProjected:EPSG::2592_EPSG::1808|Pulkovo 1942 * BP-Aze Aioc97 / Gauss-Kruger CM 51E [2592,1808]|Pulkovo 1942 * BP-Aze Aioc97 / 3-degree Gauss-Kruger CM 51E [2592,1808]|
|BoundProjected:EPSG::2594_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 57E [2594,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 57E [2594,1254]|
|BoundProjected:EPSG::2596_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 63E [2596,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 63E [2596,1254]|
|BoundProjected:EPSG::2596_EPSG::1807|Pulkovo 1942 * BP-Aze Aioc95 / Gauss-Kruger CM 63E [2596,1807]|Pulkovo 1942 * BP-Aze Aioc95 / 3-degree Gauss-Kruger CM 63E [2596,1807]|
|BoundProjected:EPSG::2596_EPSG::1808|Pulkovo 1942 * BP-Aze Aioc97 / Gauss-Kruger CM 63E [2596,1808]|Pulkovo 1942 * BP-Aze Aioc97 / 3-degree Gauss-Kruger CM 63E [2596,1808]|
|BoundProjected:EPSG::2598_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 69E [2598,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 69E [2598,1254]|
|BoundProjected:EPSG::2601_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 75E [2601,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 75E [2601,1254]|
|BoundProjected:EPSG::2603_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 81E [2603,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 81E [2603,1254]|
|BoundProjected:EPSG::2605_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 87E [2605,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 87E [2605,1254]|
|BoundProjected:EPSG::2609_EPSG::1254|Pulkovo 1942 * DMA-Rus / Gauss-Kruger CM 99E [2609,1254]|Pulkovo 1942 * DMA-Rus / 3-degree Gauss-Kruger CM 99E [2609,1254]|
|BoundProjected:EPSG::28410_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 10 [28410,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 10 [28410,1254]|
|BoundProjected:EPSG::28410_EPSG::1303|Pulkovo 1942 * KCS-Kaz Cas / 6-degree Gauss-Kruger zone 10 [28410,1303]|Pulkovo 1942 * KCS-Kaz Cas / Gauss-Kruger zone 10 [28410,1303]|
|BoundProjected:EPSG::28411_EPSG::15865|Pulkovo 1942 * OGP-Rus / 6-degree Gauss-Kruger zone 11 [28411,15865]|Pulkovo 1942 * OGP-Rus / Gauss-Kruger zone 11 [28411,15865]|
|BoundProjected:EPSG::28412_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 12 [28412,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 12 [28412,1254]|
|BoundProjected:EPSG::28412_EPSG::15865|Pulkovo 1942 * OGP-Rus / 6-degree Gauss-Kruger zone 12 [28412,15865]|Pulkovo 1942 * OGP-Rus / Gauss-Kruger zone 12 [28412,15865]|
|BoundProjected:EPSG::28413_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 13 [28413,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 13 [28413,1254]|
|BoundProjected:EPSG::28413_EPSG::15865|Pulkovo 1942 * OGP-Rus / 6-degree Gauss-Kruger zone 13 [28413,15865]|Pulkovo 1942 * OGP-Rus / Gauss-Kruger zone 13 [28413,15865]|
|BoundProjected:EPSG::28414_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 14 [28414,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 14 [28414,1254]|
|BoundProjected:EPSG::28419_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 19 [28419,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 19 [28419,1254]|
|BoundProjected:EPSG::28419_EPSG::5044|Pulkovo 1942 * GOST-Rus 2008 / 6-degree Gauss-Kruger zone 19 [28419,5044]|Pulkovo 1942 * GOST-Rus 2008 / Gauss-Kruger zone 19 [28419,5044]|
|BoundProjected:EPSG::28420_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 20 [28420,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 20 [28420,1254]|
|BoundProjected:EPSG::28421_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 21 [28421,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 21 [28421,1254]|
|BoundProjected:EPSG::28421_EPSG::5044|Pulkovo 1942 * GOST-Rus 2008 / 6-degree Gauss-Kruger zone 21 [28421,5044]|Pulkovo 1942 * GOST-Rus 2008 / Gauss-Kruger zone 21 [28421,5044]|
|BoundProjected:EPSG::28406_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 6 [28406,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 6 [28406,1254]|
|BoundProjected:EPSG::28407_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 7 [28407,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 7 [28407,1254]|
|BoundProjected:EPSG::28407_EPSG::5044|Pulkovo 1942 * GOST-Rus 2008 / 6-degree Gauss-Kruger zone 7 [28407,5044]|Pulkovo 1942 * GOST-Rus 2008 / Gauss-Kruger zone 7 [28407,5044]|
|BoundProjected:EPSG::28408_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 8 [28408,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 8 [28408,1254]|
|BoundProjected:EPSG::28408_EPSG::5044|Pulkovo 1942 * GOST-Rus 2008 / 6-degree Gauss-Kruger zone 8 [28408,5044]|Pulkovo 1942 * GOST-Rus 2008 / Gauss-Kruger zone 8 [28408,5044]|
|BoundProjected:EPSG::28409_EPSG::1254|Pulkovo 1942 * DMA-Rus / 6-degree Gauss-Kruger zone 9 [28409,1254]|Pulkovo 1942 * DMA-Rus / Gauss-Kruger zone 9 [28409,1254]|
|BoundProjected:EPSG::28409_EPSG::1303|Pulkovo 1942 * KCS-Kaz Cas / 6-degree Gauss-Kruger zone 9 [28409,1303]|Pulkovo 1942 * KCS-Kaz Cas / Gauss-Kruger zone 9 [28409,1303]|
|BoundProjected:EPSG::28409_EPSG::1807|Pulkovo 1942 * BP-Aze Aioc95 / 6-degree Gauss-Kruger zone 9 [28409,1807]|Pulkovo 1942 * BP-Aze Aioc95 / Gauss-Kruger zone 9 [28409,1807]|
|BoundProjected:EPSG::28409_EPSG::5044|Pulkovo 1942 * GOST-Rus 2008 / 6-degree Gauss-Kruger zone 9 [28409,5044]|Pulkovo 1942 * GOST-Rus 2008 / Gauss-Kruger zone 9 [28409,5044]|
|BoundProjected:EPSG::5663_EPSG::15997|Pulkovo 1942(58) * NIMA-Pol / 6-degree Gauss-Kruger zone 3 [5663,15997]|Pulkovo 1942(58) * NIMA-Pol / Gauss-Kruger zone 3 (E-N) [5663,15997]|
|BoundProjected:EPSG::3844_EPSG::15995|Pulkovo 1942(58) * OGP-Rom / Stereo 70 [3844,15995]|Pulkovo 1942(58) * OGP-Rom / Stereo70 [3844,15995]|
|BoundProjected:EPSG::3942_EPSG::1671|RGF93 v1 * EPSG-Fra / France Conic Conformal zone 1 [3942,1671]|RGF93 v1 * EPSG-Fra / CC42 [3942,1671]|
|BoundProjected:EPSG::3943_EPSG::1671|RGF93 v1 * EPSG-Fra / France Conic Conformal zone 2 [3943,1671]|RGF93 v1 * EPSG-Fra / CC43 [3943,1671]|
|BoundProjected:EPSG::3944_EPSG::1671|RGF93 v1 * EPSG-Fra / France Conic Conformal zone 3 [3944,1671]|RGF93 v1 * EPSG-Fra / CC44 [3944,1671]|
|BoundProjected:EPSG::3945_EPSG::1671|RGF93 v1 * EPSG-Fra / France Conic Conformal zone 4 [3945,1671]|RGF93 v1 * EPSG-Fra / CC45 [3945,1671]|
|BoundProjected:EPSG::3946_EPSG::1671|RGF93 v1 * EPSG-Fra / France Conic Conformal zone 5 [3946,1671]|RGF93 v1 * EPSG-Fra / CC46 [3946,1671]|
|BoundProjected:EPSG::3947_EPSG::1671|RGF93 v1 * EPSG-Fra / France Conic Conformal zone 6 [3947,1671]|RGF93 v1 * EPSG-Fra / CC47 [3947,1671]|
|BoundProjected:EPSG::3948_EPSG::1671|RGF93 v1 * EPSG-Fra / France Conic Conformal zone 7 [3948,1671]|RGF93 v1 * EPSG-Fra / CC48 [3948,1671]|
|BoundProjected:EPSG::3949_EPSG::1671|RGF93 v1 * EPSG-Fra / France Conic Conformal zone 8 [3949,1671]|RGF93 v1 * EPSG-Fra / CC49 [3949,1671]|
|BoundProjected:EPSG::3950_EPSG::1671|RGF93 v1 * EPSG-Fra / France Conic Conformal zone 9 [3950,1671]|RGF93 v1 * EPSG-Fra / CC50 [3950,1671]|
|BoundProjected:EPSG::29872_EPSG::1852|Timbalai 1948 * SSB-Mys E / Rectified Skew Orthomorphic Borneo Grid (feet) [29872,1852]|Timbalai 1948 * SSB-Mys E / RSO Borneo (ftSe) [29872,1852]|
|BoundProjected:EPSG::29873_EPSG::1228|Timbalai 1948 * DMA-Borneo / Rectified Skew Orthomorphic Borneo Grid (metres) [29873,1228]|Timbalai 1948 * DMA-Borneo / RSO Borneo (m) [29873,1228]|
|BoundProjected:EPSG::29873_EPSG::1592|Timbalai 1948 * BSP-Brn / Rectified Skew Orthomorphic Borneo Grid (metres) [29873,1592]|Timbalai 1948 * BSP-Brn / RSO Borneo (m) [29873,1592]|
|BoundProjected:EPSG::29873_EPSG::1852|Timbalai 1948 * SSB-Mys E / Rectified Skew Orthomorphic Borneo Grid (metres) [29873,1852]|Timbalai 1948 * SSB-Mys E / RSO Borneo (m) [29873,1852]|
|BoundProjected:EPSG::5562_EPSG::5840|UCS-2000 * OGP-Ukr SSGC / 6-degree Gauss-Kruger zone 4 [5562,5840]|UCS-2000 * OGP-Ukr SSGC / Gauss-Kruger zone 4 [5562,5840]|
|BoundProjected:EPSG::5563_EPSG::5840|UCS-2000 * OGP-Ukr SSGC / 6-degree Gauss-Kruger zone 5 [5563,5840]|UCS-2000 * OGP-Ukr SSGC / Gauss-Kruger zone 5 [5563,5840]|
|BoundProjected:EPSG::5564_EPSG::5840|UCS-2000 * OGP-Ukr SSGC / 6-degree Gauss-Kruger zone 6 [5564,5840]|UCS-2000 * OGP-Ukr SSGC / Gauss-Kruger zone 6 [5564,5840]|
|BoundProjected:EPSG::5565_EPSG::5840|UCS-2000 * OGP-Ukr SSGC / 6-degree Gauss-Kruger zone 7 [5565,5840]|UCS-2000 * OGP-Ukr SSGC / Gauss-Kruger zone 7 [5565,5840]|

[Back to M14 Change Overview](README.md)