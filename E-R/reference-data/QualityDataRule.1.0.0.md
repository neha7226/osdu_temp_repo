<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# QualityDataRule
Generic reference object quality rule
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--QualityDataRule:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: namespace:reference-data/OrganisationType:osdu
* Reference value type, governance model: LOCAL
* **Superseded by** &rarr; [`osdu:wks:reference-data--QualityDataRule:1.1.0`](QualityDataRule.1.1.0.md)
* **Migration Guide (M12)** [`osdu:wks:reference-data--QualityDataRule:1.0.0` &rarr; `osdu:wks:reference-data--QualityDataRule:1.1.0`](../../Guides/MigrationGuides/M12/QualityDataRule.1.0.0.md)
* Link to &rarr; [Authoring Schema QualityDataRule.1.0.0.json](../../Authoring/reference-data/QualityDataRule.1.0.0.json)
* Link to &rarr; [Generated Schema QualityDataRule.1.0.0.json](../../Generated/reference-data/QualityDataRule.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource QualityDataRule.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/QualityDataRule.1.0.0.json)
* Link to &rarr; [Example Record QualityDataRule.1.0.0.json](../../Examples/reference-data/QualityDataRule.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams QualityDataRule


## Outgoing Relationships for QualityDataRule

![QualityDataRule](../_diagrams/reference-data/QualityDataRule.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## QualityDataRule Referenced by other Entities

![QualityDataRule](../_diagrams/reference-data/QualityDataRule.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# QualityDataRule Properties

## 1. Table of QualityDataRule System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-QualityDataRule:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--QualityDataRule:2bf14867-0512-5a60-89cd-be38d423c126|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--QualityDataRule:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of QualityDataRule Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of QualityDataRule Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of QualityDataRule Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.ExternalRuleID| |string|optional|External Rule Id|Unique identifier to an external data rule, e.g. PPDM rule ID number.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.DataRuleStatement| |string|optional|Data Rule Statement|Expression of data rule|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.DataRuleRevision| |string|optional|Data Rule Revision|Revision version number|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.DataRuleStatus| |string|optional|Data Rule Status|status of the business rule such as Published|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.DataRuleCreatedOn| |string|optional|Data Rule Created On|Date of creation of data rule independent of OSDU|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.DataRuleCreatedBy| |string|optional|Data Rule Created By|User that created the rule independent of OSDU|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.DataRulePublishedOn| |string|optional|Data Rule Published On|Timestamp of the time when the data rule was published independent of OSDU.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.DataRuleUpdatedOn| |string|optional|Data Rule Updated On|Timestamp of the time when the data rule was updated independent of OSDU.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of QualityDataRule Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for QualityDataRule.1.1.0

* **Note:** The example records refer to a newer schema version QualityDataRule.1.1.0.
* Status: Published
* Governance: LOCAL
* The table below shows all 24 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [QualityDataRule](../../ReferenceValues/Manifests/reference-data/LOCAL/QualityDataRule.1.1.0.json).

|`id` with prefix `namespace:reference-data--QualityDataRule:`|Code|Name|Alias|Description|AttributionAuthority|AttributionPublication|DataRuleCreatedBy|DataRuleStatement|DataRuleStatus|ExternalRuleID|Source|CommitDate|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|A%20seismic%20bin%20grid%20angle%20of%20rotation%20is%20less%20than%20360%20degrees.|A seismic bin grid angle of rotation is less than 360 degrees.|A seismic bin grid angle of rotation is less than 360 degrees.|-|The bin grid orientation can be related to the map grid North by the clockwise rotation of the J axis.|PPDM|PPDM Rules Library|OSDU|SeismicBinGrid.P6MapGridBearingOfBinGridJAxis &gt;=0 and SeismicBinGrid.P6MapGridBearingOfBinGridJAxis &lt; 360|Preliminary|3052|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|For%20a%20group%20of%20seismic%20points%2C%20the%20datum%20to%20which%20the%20elevations%20or%20depths%20have%20been%20referenced%20should%20be%20recorded.|For a group of seismic points, the datum to which the elevations or depths have been referenced should be recorded.|For a group of seismic points, the datum to which the elevations or depths have been referenced should be recorded.|-|Seismic point groups should have the reference datum specified.|PPDM|PPDM Rules Library|OSDU|if SeismicTraceData.VerticalDatumOffset is not null then SeismicTraceData.VerticalMeasurementTypeID is not null|Preliminary|1852|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|Ground%20elevation%20should%20be%20less%20than%202000%20m.%20Remark:%20Make%20it%20a%20bigger%20number%2C%20to%20be%20on%20the%20safe%20side|Ground elevation should be less than 2000 m. Remark: Make it a bigger number, to be on the safe side|Ground elevation should be less than 2000 m. Remark: Make it a bigger number, to be on the safe side|-|The elevation must be reasonable for the well&#x27;s geographic locality|PPDM|PPDM Rules Library|OSDU|VerticalMeasurement for a wellbore must be less than 2000 meters when VerticalMeasurementType is Ground Level|Preliminary|100|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|If%20a%20well%20has%20a%20status%20of%20completed%2C%20the%20completion%20date%20must%20not%20be%20null|If a well has a status of completed, the completion date must not be null|If a well has a status of completed, the completion date must not be null|-|A completion date is necessary information for any completed well|PPDM|PPDM Rules Library|OSDU|If WELLBORE.FacilityState indicates production then FacilityEventType.LAST_COMPLETION must be not be null|Preliminary|53|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|If%20the%20well%20status%20indicates%20production%2C%20the%20on-production%20date%20must%20not%20be%20null|If the well status indicates production, the on-production date must not be null|If the well status indicates production, the on-production date must not be null|-|If a well or wellbore has reported production, there must be a date for the start of this production|PPDM|PPDM Rules Library|OSDU|If WELLBORE.FacilityState indicates production then FacilityEventType.ON_PRODUCTION must be not be null|Preliminary|5|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|In%20defining%20a%20seismic%20bin%20grid%20each%20corner%20must%20have%20a%20latitude%20and%20a%20longitude|In defining a seismic bin grid each corner must have a latitude and a longitude|In defining a seismic bin grid each corner must have a latitude and a longitude|-|As a minimum set of spatial information about a geographic area, the key locations should be reported. A rectangular bin grid can be defined by 3 corners. The details of the shape and extent may be stored elsewhere.|PPDM|PPDM Rules Library|OSDU|Count( SeismicBinGrid.ABCDBinGridSpatialLocation.Coordinates ) = 4|Preliminary|2357|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|PPDM|PPDM|PPDM|-|ppdm but adjust height|PPDM|PPDM Rules Library|OSDU|VerticalMeasurement for a wellbore must be less than 3000 meters if OperatingEnvironment indicates onshore and VerticalMeasurementType is Derrick Floor, Kelly Bushing, or Rotary Table|Preliminary|-|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|Seismic%20acquisition%20start%20date%20is%20less%20than%20completed%20date|Seismic acquisition start date is less than completed date|Seismic acquisition start date is less than completed date|-|As in all date pairs, the start date is less than the end date|PPDM|PPDM Rules Library|OSDU|SeismicAcquisitionProject.ProjectBeginDate &lt; SeismicAcquisitionProject.ProjectEndDate|Preliminary|904|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|Spud%20date%20must%20be%20less%20than%20current%20date|Spud date must be less than current date|Spud date must be less than current date|-|The start of drilling (spud) is always a past event, unless predicted at the planning stage|PPDM|PPDM Rules Library|OSDU|For a well FacilityEventType Spud must not be later than current date|Preliminary|146|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|Spud%20date%20must%20be%20less%20than%20rig%20release%20date.|Spud date must be less than rig release date.|Spud date must be less than rig release date.|-|The rule checks for the logical sequence of dates. The drilling rig cannot be released until after drilling has started (spud.)|PPDM|PPDM Rules Library|OSDU|For a well FacilityEventType Spud must be less than FacilityEventType Rig Release|Preliminary|151|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|The%20basin%20name%20or%20code%20for%20a%20well%20should%20not%20be%20null.|The basin name or code for a well should not be null.|The basin name or code for a well should not be null.|-|The name or code for a geologic basin is useful information for any well|PPDM|PPDM Rules Library|OSDU|For a well facility the name or code for AbstractGeoBasinContext must not be null|Preliminary|92|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|The%20drilling%20floor%20elevation%20for%20an%20offshore%20well%20should%20be%20between%201%20and%2040%20m.|The drilling floor elevation for an offshore well should be between 1 and 40 m.|The drilling floor elevation for an offshore well should be between 1 and 40 m.|-|The drilling floor elevation for an offshore well should be between 1 and 40 m.|PPDM|PPDM Rules Library|OSDU|VerticalMeasurement for a wellbore must be between  1 meter and 40 meters if OperatingEnvironment indicates offshore and VerticalMeasurementType is Derrick Floor, Kelly Bushing, or Rotary Table|Preliminary|76|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|The%20end%20time%20of%20a%20seismic%20activity%20must%20have%20the%20time%20zone.|The end time of a seismic activity must have the time zone.|The end time of a seismic activity must have the time zone.|-|Clock time can be wrong or uncertain without the time zone.|PPDM|PPDM Rules Library|OSDU|if match (SeismicProcessingProject.ProjectEndDate, &quot;T&quot;) then match( SeismicProcessingProject.ProjectEndDate, &quot;T*[Z-]&quot; )|Preliminary|2421|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|The%20geopolitical%20context%20for%20a%20facility%20must%20not%20be%20null.|The geopolitical context for a facility must not be null.|The geopolitical context for a facility must not be null.|-|The name or code for administrative (country), licensing, surveying, or operating area must be populated for any facility.|PPDM|PPDM Rules Library|OSDU|GeoPoliticalContext must not be NULL.|Preliminary|60|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|The%20ground%20elevation%20must%20be%20less%20than%20the%20kelly%20bushing%20elevation%20%28KB%29.|The ground elevation must be less than the kelly bushing elevation (KB).|The ground elevation must be less than the kelly bushing elevation (KB).|-|The kelly bushing (KB) is a part of the drilling rig and therefore is always above the ground surface.|PPDM|PPDM Rules Library|OSDU|VerticalMeasurementType Ground Level must be less than VerticalMeasurementType Kelly Bushing|Preliminary|95|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|The%20inline%20minimum%20number%20for%20a%20seismic%20bin%20grid%20is%20less%20than%20the%20inline%20maximum%20number|The inline minimum number for a seismic bin grid is less than the inline maximum number|The inline minimum number for a seismic bin grid is less than the inline maximum number|-|For any value tracked, the minimum should be less than the average which is less than the maximum|PPDM|PPDM Rules Library|OSDU|SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[0] = SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[1] and SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[1] &lt; SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[2] and SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[2] = SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[3] and SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.Y[0] &lt; SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[1] and SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.Y[1] &gt; SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[2] and SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.Y[0] &lt; SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[1] and SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.Y[2] &lt; SeismicBinGrid.ABCDBinGridLocalCoordinates.Coordinates.X[3]|Preliminary|2618|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|The%20name%20of%20the%20company%20that%20conducted%20a%20seismic%20survey%20should%20not%20be%20null|The name of the company that conducted a seismic survey should not be null|The name of the company that conducted a seismic survey should not be null|-|The company that acquired the seismic data in the field is valuable information|PPDM|PPDM Rules Library|OSDU|SeismicAcquisitionProject.Contractors.ContractorOrganisationID is not null and SeismicAcquisitionProject.ContractorsContractorTypeID = Recording|Preliminary|27|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|The%20seismic%20survey%20client%20company%20should%20not%20be%20null|The seismic survey client company should not be null|The seismic survey client company should not be null|-|The company that contracted for the seismic survey is valuable information|PPDM|PPDM Rules Library|OSDU|SeismicAcquisitionProject.Operator is not null|Preliminary|28|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|The%20well%20spud%20date%20should%20be%20later%20than%20year%201900.|The well spud date should be later than year 1900.|The well spud date should be later than year 1900.|-|This is a simple test for to check for an obvious error in date format or range. It is a range validity rule. A date before 1859 is probably a data entry error or a date format error. The rule can be adapted to a specific region where the date of the earliest well is known, e.g. Ohio 1859, Norway 1966.|PPDM|PPDM Rules Library|OSDU|For a well FacilityEventType any date must not be before year 1900 and not later than year 2100|Preliminary|145|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|Total%20Depth%20must%20be%20a%20positive%20number%20greater%20than%20zero%20and%20less%20than%2050%2C000%20feet%20or%2015240%20meters.|Total Depth must be a positive number greater than zero and less than 50,000 feet or 15240 meters.|Total Depth must be a positive number greater than zero and less than 50,000 feet or 15240 meters.|-|Ensure that well total depth is not a negative number and falls within generally agreed upon maximum depth based on technology|PPDM|PPDM Rules Library|OSDU|VerticalMeasurement for a wellbore must be less than 50,000 feet or 15240 meters when VerticalMeasurementType is Total Depth|Preliminary|66|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|Well%20final%20drill%20date%20should%20be%20greater%20than%201900|Well final drill date should be greater than 1900|Well final drill date should be greater than 1900|-|A date that is too old is an indicator of bad data. The date should be adjusted to the locality of interest|PPDM|PPDM Rules Library|OSDU|For a well FacilityEventType TD Reached must be later than 1900|Preliminary|183|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|Well%20location%20type%20should%20not%20be%20null.%20Remark:%20with%20enum%20%3D%20INFORMATION|Well location type should not be null. Remark: with enum = INFORMATION|Well location type should not be null. Remark: with enum = INFORMATION|-|A location associated with a well could be any point in 3D space, on the surface or in the subsurface. The geographic coordinates or other survey information for a well should be explicitly connected to a type of location e.g. well origin, subsea slot.|PPDM|PPDM Rules Library|OSDU|For a well AbstractSpatialLocation must be populated|Preliminary|117|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|Well%20name%20should%20not%20be%20null|Well name should not be null|Well name should not be null|-|Most wells are labelled with a name even if they also have other identifiers|PPDM|PPDM Rules Library|OSDU|FacilityName must not be null|Preliminary|188|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|
|Well%20profile%20type%20should%20not%20be%20null.%20Remark:%20with%20enum%20%3D%20INFORMATION|Well profile type should not be null. Remark: with enum = INFORMATION|Well profile type should not be null. Remark: with enum = INFORMATION|-|The well profile described the general construction of a well or wellbore, e.g. vertical, deviated, horizontal|PPDM|PPDM Rules Library|OSDU|For a wellbore TrajectoryType must not be null for drilled wellbores|Preliminary|127|Workbook ReferenceValues/Published/QualityDataRule.1.1.0.xlsx; commit SHA 0a344d17.|2023-01-03T11:28:05+01:00|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# QualityDataRule.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractBusinessRule.1.0.0](../abstract/AbstractBusinessRule.1.0.0.md) | DataRules[].DataRuleID | many (as array) |  |
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [QualityDataRuleSet.1.0.0](QualityDataRuleSet.1.0.0.md) | DataRules[].DataRuleID | many (as array) |  |
| Referred to by | [QualityDataRuleSet.2.0.0](QualityDataRuleSet.2.0.0.md) | DataRules[].DataRuleID | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._