<a name="TOC"></a>

[[_TOC_]]

# External Data Services

## Definitions and Terminology

The following terms are used throughout the document:

- **Administrator**:  a privileged user of the OSDU platform that is entitled to use the External Data Sources (EDS)
  module within the OSDU Admin UI.

- **Connected Source Registry Entry**:  an OSDU master-data type that defines high-level business and technical
  information about an External Data Source. Administrators use the EDS module to register external data sources, which
  in turn creates a Connected Source Registry Entry in the Administrator’s OSDU Platform.

- **Connected Source Data Job**:  an OSDU master-data type that defines technical configuration for scheduled data jobs
  that will be executed by OSDU EDS workflow services to retrieve and ingest master data, work product component data
  and/or reference data from an external source specified in a parent Connected Source Registry Entry. There is a
  zero-to-many relationship between a Connected Source Registry Entry and a Connected Source Data Job; given one
  Connected Source Registry Entry record, there can be zero or many associated Connected Source Data Job records.
  Administrators use the EDS module to create scheduled data jobs, which in turn creates one or more Connected Source
  Data Job records in the Administrator’s OSDU Platform.

- **Core External Data Workflow Services**:  Software on the Administrator’s OSDU Platform that automatically consumes
  Connected Source Registry Entry and Connected Source Data Job records to schedule automated data jobs, retrieve
  metadata from an external data source, and ingest it into an Administrator’s OSDU platform.

- **External Data Source**:  any data source that is external to the Administrator’s OSDU Platform. The information
  contained in the Connected Source Registry Entry and associated Connected Source Data Job records for this source are
  used by automated EDS Core Workflow services to retrieve and ingest data.

- **JSON**:  JavaScript Object Notation, which is a structure for data exchange and is the current method of data
  exchange for OSDU APIs.

- **JSON payload**:  the request or response body of an OSDU API call, in JSON format.

- **JSON schema**:  a JSON document that describes the accepted representation of an object in JSON format.

- **Record**: a JSON document of a certain data type persisted in the OSDU platform.

- **Source Registry**: a term used to describe entire collection of records of type ConnectedSourceRegistryEntry and
  ConnectedSourceDataJob persisted in the Consuming OSDU Platform.

[Back to TOC](#TOC)

---

## High-level Overview

![HighLevelOverview](./Illustrations/HighLevelOverview.png)

The objectives of the External Data Service (EDS) functionality are to retrieve metadata from a registered External Data
Source and ingest it into the Administrator’s OSDU Platform so that the data can be discoverable and accessible to
consuming OSDU applications through the OSDU Search API (just like any other data ingested into the system) while
leaving the associated bulk data, or files, stored at the external source for delivery on demand.

From a consumer’s perspective (the perspective of the Administrator’s organization), one of the most valuable use cases
this functionality satisfies is the ability to search for data that the Administrator’s organization has licensed from
an External Data Source from its own OSDU platform without enduring the overhead of storing the bulk data files the
ingested metadata refers to. From an external supplier’s perspective, the most valuable use case this functionality
satisfies is the ability to participate in the OSDU ecosystem to supply and advertise offerings to their customers even
if they do not have an OSDU implementation. For those without a full OSDU implementation, all that is required is an
OSDU-compliant “wrapper” that presents an OSDU-compliant interface for Search and Dataset services.

Connected Source Registry and Connected Source Data Job records, collectively known as the Source Registry, are created
by an Administrator using the External Data Service module in the OSDU Admin UI. This is the only part of the solution
that requires human intervention. From there the Source Registry information is automatically consumed by the Core
External Workflow Service engine to schedule data jobs and begin automatically fetching the desired master data, work
product component, and/or reference data from the specified external source and ingesting it into the Administrator’s
OSDU Platform.

Upon ingestion, the data is search-indexed just like any other piece of data and is accessible to end-users using the
Search API (entitlements permitting). If a user decides to retrieve the bulk data file or files associated with a work
product component the Platform will detect whether the file is stored externally or not. If it is, the Proxy Dataset
Service is used to retrieve the file from the registered external source and deliver it to the user. This process is
transparent to the end user.

## Example Records

The following workflow delivers the context to the example records:

![img.png](Illustrations/Workflow.png)

1. a `ConnectedSourceRegistryEntry` is set up to represent the external data
   source ([example record](./master-data/ConnectedSourceRegistryEntry-Example.json)).
2. a job (`ConnectedSourceDataJob`) to synchronize/make the external data discoverable is executed, represented by
   this [example record](./master-data/ConnectedSourceDataJob-Example.json). This job creates
    1. the discoverable work-product-component - here a [WellLog](./work-product-component/WellLog.fromTGS.json), which
       is of course referring to a `Wellbore` and `Well`, and
    2. the dataset with the relations to the `ConnectedSourceRegistryEntry`
       and `ConnectedSourceDataJob` [example record](./dataset/ConnectedSource.Generic-Example.json).

Consumers will want to discover all accessible records, search for kind `work-product-component--WellLog` using the
Search service. Assume that Search will return a record of interest:

* `work-product-component--WellLog` with
  id: [`partition-id:work-product-component--WellLog:c2c79f1c-90ca-5c92-b8df-04dbe438f414`](./work-product-component/WellLog.fromTGS.json)
  .

This record refers to a dataset via the `data.Datasets[]` property. This leads on to:

* `dataset--ConnectedSource.Generic` with
  id: [`partition-id:dataset--ConnectedSource.Generic:72e981d6-aa5f-51ea-a76b-f3f2f01eb53a`](./dataset/ConnectedSource.Generic-Example.json)

The `dataset--ConnectedSource.Generic` refers back to the

* `master-data--ConnectedSourceRegistryEntry`, the critical information block to get access to the content, with
  id: [`partition-id:master-data--ConnectedSourceRegistryEntry:0ae7be1c-5df7-5656-98c3-f7c5509a7dfa`](./master-data/ConnectedSourceRegistryEntry-Example.json)
  , and
* the historical record, which created the `work-product-component--WellLog` and `dataset--ConnectedSource.Generic`
  , `master-data--ConnectedSourceDataJob`
  with [`partition-id:master-data--ConnectedSourceDataJob:77f02eed-9c3a-5795-8eda-84d91cbb2614`](./master-data/ConnectedSourceDataJob-Example.json).

[Back to TOC](#TOC)

---

### ConnectedSourceDataJob as Activity

The use of OSDU [the activity concepts](../../../E-R/abstract/AbstractProjectActivity.1.1.0.md) offers flexible
adaptation and customizations of ConnectedSourceDataJob instances. Frequently used properties are conveniently modelled
as properties while others are expressed via named parameters. 

For some applications it turned out that an additional schema authority identifier for the source system/platform
instance. for this a `data.Parameter` with the Title "ConnectedSourceSchemaAuthority" was added.

<details>

<summary markdown="span">ConnectedSourceDataJob - Supplementary ConnectedSourceSchemaAuthority</summary>

<OsduImportJSON>[ConnectedSourceDataJob, full record](master-data/ConnectedSourceDataJob-Example.json#only.data.ActivityTemplateID|Parameters)</OsduImportJSON>

```json
{
  "data": {
    "ActivityTemplateID": "partition-id:master-data--ActivityTemplate:ConnectedDataSourceJob-ActivityTemplate-001:",
    "Parameters": [
      {
        "Title": "ConnectedSourceSchemaAuthority",
        "Selection": "Selected by default",
        "StringParameter": "osdu",
        "ParameterKindID": "partition-id:reference-data--ParameterKind:String:",
        "ParameterRoleID": "partition-id:reference-data--ParameterRole:Input:"
      },
      {
        "Title": "FetchStartDateTime",
        "Selection": "Set by the Job",
        "StringParameter": "2020-02-01T09:28:56.321-10:00",
        "ParameterKindID": "partition-id:reference-data--ParameterKind:String:",
        "ParameterRoleID": "partition-id:reference-data--ParameterRole:Output:"
      },
      {
        "Title": "FetchEndDateTime",
        "Selection": "Set by the Job",
        "StringParameter": "2020-02-01T09:28:56.321-10:00",
        "ParameterKindID": "partition-id:reference-data--ParameterKind:String:",
        "ParameterRoleID": "partition-id:reference-data--ParameterRole:Output:"
      }
    ]
  }
}
```

</details>

Please note the `data.ActivityTemplateID` in the example above — this reference identifies an ActivityTemplate record,
which defines the expected parameter details — which value type, cardinality, and potential defaults. The example
ActivityTemplate record contains only one parameter definition, the parameter with title 
"ConnectedSourceSchemaAuthority". 

<details>

<summary markdown="span">ActivityTemplate - Defining the expected ConnectedSourceSchemaAuthority parameter</summary>

<OsduImportJSON>[ActivityTemplate, full record](master-data/ConnectedSourceDataJob-ActivityTemplate-001.json#only.data.Parameters)</OsduImportJSON>

```json
{
  "data": {
    "Parameters": [
      {
        "Title": "ConnectedSourceSchemaAuthority",
        "AllowedParameterKind": "partition-id:reference-data--ParameterKind:String:",
        "IsInput": true,
        "IsOutput": false,
        "MaxOccurs": 1,
        "MinOccurs": 1,
        "DefaultValue": {
          "Title": "ConnectedSourceSchemaAuthority",
          "Selection": "Selected by default",
          "StringParameter": "osdu",
          "ParameterKindID": "partition-id:reference-data--ParameterKind:String:",
          "ParameterRoleID": "partition-id:reference-data--ParameterRole:Input:"
        }
      },
      {
        "Title": "FetchStartDateTime",
        "AllowedParameterKind": "partition-id:reference-data--ParameterKind:String:",
        "IsInput": false,
        "IsOutput": true,
        "MaxOccurs": 1,
        "MinOccurs": 0
      },
      {
        "Title": "FetchEndDateTime",
        "AllowedParameterKind": "partition-id:reference-data--ParameterKind:String:",
        "IsInput": false,
        "IsOutput": true,
        "MaxOccurs": 1,
        "MinOccurs": 0
      }
    ]
  }
}
```

</details>

### ConnectedSourceRegistryEntry as Activity

Similar to ConnectedSourceDataJob, the use of OSDU 
[the activity concepts](../../../E-R/abstract/AbstractProjectActivity.1.1.0.md) offers flexible
adaptation and customizations of ConnectedSourceRegistryEntry instances. Frequently used properties are conveniently 
modelled as properties while others are expressed via named parameters. 

At the moment no concrete examples are provided, they would, however, work similarly to the example in 
[ConnectedSourceDataJob](#connectedsourcedatajob-as-activity).

[Back to TOC](#TOC)

---
