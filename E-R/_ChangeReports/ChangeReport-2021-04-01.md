[[_TOC_]]

# Change Report


|Reference Information|Community Repository|Branch|
|----|----|----|
|Repository|url: [https://community.opengroup.org/osdu/data/data-definitions](https://community.opengroup.org/osdu/data/data-definitions)|Data Definitions : 209-indexer-hints</br>url: [https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema)|
|Last updated|2021-04-01 11:25:00+02:00| 2021-04-29 10:49:00+02:00 [DD_Snapshot=R3_Apr_29](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tags/DD_Snapshot=R3_Apr_29) |
|Last changed by|Thomas Gehrmann| Thomas Gehrmann |
|Commit SHA|d85ab877101c007e33cc579e4bd679751d38e6c1| `c773c699b01adb70e69578dc40cf03c2fbb7a8c1` |



# `abstract` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:AbstractAccessControlList:1.0.0`](../abstract/AbstractAccessControlList.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractAliasNames:1.0.0`](../abstract/AbstractAliasNames.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractAnyCrsFeatureCollection:1.0.0`](../abstract/AbstractAnyCrsFeatureCollection.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractBinGrid:1.0.0`](../abstract/AbstractBinGrid.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractBusinessRule:1.0.0`](../abstract/AbstractBusinessRule.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractCommonResources:1.0.0`](../abstract/AbstractCommonResources.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractCompressionInfo:1.0.0`](../abstract/AbstractCompressionInfo.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractCoordinates:1.0.0`](../abstract/AbstractCoordinates.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractDataset:1.0.0`](../abstract/AbstractDataset.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacility:1.0.0`](../abstract/AbstractFacility.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilityEvent:1.0.0`](../abstract/AbstractFacilityEvent.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilityOperator:1.0.0`](../abstract/AbstractFacilityOperator.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilitySpecification:1.0.0`](../abstract/AbstractFacilitySpecification.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilityState:1.0.0`](../abstract/AbstractFacilityState.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilityVerticalMeasurement:1.0.0`](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFeatureCollection:1.0.0`](../abstract/AbstractFeatureCollection.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFile:1.0.0`](../abstract/AbstractFile.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFileCollection:1.0.0`](../abstract/AbstractFileCollection.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFileSourceInfo:1.0.0`](../abstract/AbstractFileSourceInfo.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoBasinContext:1.0.0`](../abstract/AbstractGeoBasinContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoContext:1.0.0`](../abstract/AbstractGeoContext.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoFieldContext:1.0.0`](../abstract/AbstractGeoFieldContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoPlayContext:1.0.0`](../abstract/AbstractGeoPlayContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoPoliticalContext:1.0.0`](../abstract/AbstractGeoPoliticalContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoProspectContext:1.0.0`](../abstract/AbstractGeoProspectContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractLegalParentList:1.0.0`](../abstract/AbstractLegalParentList.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractLegalTags:1.0.0`](../abstract/AbstractLegalTags.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractMaster:1.0.0`](../abstract/AbstractMaster.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractMetaItem:1.0.0`](../abstract/AbstractMetaItem.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractPersistableReference:1.0.0`](../abstract/AbstractPersistableReference.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractProject:1.0.0`](../abstract/AbstractProject.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractQualityMetric:1.0.0`](../abstract/AbstractQualityMetric.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractReferenceType:1.0.0`](../abstract/AbstractReferenceType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractSpatialLocation:1.0.0`](../abstract/AbstractSpatialLocation.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractVectorHeaderMapping:1.0.0`](../abstract/AbstractVectorHeaderMapping.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractWPCGroupType:1.0.0`](../abstract/AbstractWPCGroupType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractWellboreDrillingReason:1.0.0`](../abstract/AbstractWellboreDrillingReason.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractWorkProductComponent:1.0.0`](../abstract/AbstractWorkProductComponent.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|

# `data-collection` Schemas

| Kind                                                                                 |Status|Status Local|Status Community|
|--------------------------------------------------------------------------------------|----|----|----|
| &rarr; [`osdu:wks:DataCollection:1.0.0`](../data-collection/DataCollection.1.0.0.md) |No Changes|DEVELOPMENT|DEVELOPMENT|

# `dataset` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:dataset--File.CompressedVectorHeaders:1.0.0`](../dataset/File.CompressedVectorHeaders.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.Generic:1.0.0`](../dataset/File.Generic.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.WITSML:1.0.0`](../dataset/File.WITSML.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--FileCollection.Bluware.OpenVDS:1.0.0`](../dataset/FileCollection.Bluware.OpenVDS.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--FileCollection.Generic:1.0.0`](../dataset/FileCollection.Generic.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--FileCollection.SEGY:1.0.0`](../dataset/FileCollection.SEGY.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--FileCollection.Slb.OpenZGY:1.0.0`](../dataset/FileCollection.Slb.OpenZGY.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|

# `manifest` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:dataset--GenericDataset:1.0.0`](../manifest/GenericDataset.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--GenericMasterData:1.0.0`](../manifest/GenericMasterData.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--GenericReferenceData:1.0.0`](../manifest/GenericReferenceData.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product--GenericWorkProduct:1.0.0`](../manifest/GenericWorkProduct.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GenericWorkProductComponent:1.0.0`](../manifest/GenericWorkProductComponent.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:Manifest:1.0.0`](../manifest/Manifest.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|

# `master-data` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:master-data--Agreement:1.0.0`](../master-data/Agreement.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Basin:1.0.0`](../master-data/Basin.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Field:1.0.0`](../master-data/Field.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--GeoPoliticalEntity:1.0.0`](../master-data/GeoPoliticalEntity.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Organisation:1.0.0`](../master-data/Organisation.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Play:1.0.0`](../master-data/Play.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Prospect:1.0.0`](../master-data/Prospect.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0`](../master-data/Seismic2DInterpretationSet.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0`](../master-data/Seismic3DInterpretationSet.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0`](../master-data/SeismicAcquisitionSurvey.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--SeismicProcessingProject:1.0.0`](../master-data/SeismicProcessingProject.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Well:1.0.0`](../master-data/Well.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Wellbore:1.0.0`](../master-data/Wellbore.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|

# `reference-data` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:reference-data--ActivityType:1.0.0`](../reference-data/ActivityType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ActualIndicatorType:1.0.0`](../reference-data/ActualIndicatorType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AgreementType:1.0.0`](../reference-data/AgreementType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AliasNameType:1.0.0`](../reference-data/AliasNameType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AliasNameTypeClass:1.0.0`](../reference-data/AliasNameTypeClass.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AnisotropyType:1.0.0`](../reference-data/AnisotropyType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ArtefactRole:1.0.0`](../reference-data/ArtefactRole.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ArtificialLiftType:1.0.0`](../reference-data/ArtificialLiftType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AzimuthReferenceType:1.0.0`](../reference-data/AzimuthReferenceType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--BasinType:1.0.0`](../reference-data/BasinType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--BinGridDefinitionMethodType:1.0.0`](../reference-data/BinGridDefinitionMethodType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CalculationMethodType:1.0.0`](../reference-data/CalculationMethodType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CompressionMethodType:1.0.0`](../reference-data/CompressionMethodType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ContractorType:1.0.0`](../reference-data/ContractorType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CoordinateReferenceSystem:1.0.0`](../reference-data/CoordinateReferenceSystem.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CoordinateTransformation:1.0.0`](../reference-data/CoordinateTransformation.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--Currency:1.0.0`](../reference-data/Currency.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CurveIndexDimensionType:1.0.0`](../reference-data/CurveIndexDimensionType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DataRulePurposeType:1.0.0`](../reference-data/DataRulePurposeType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DimensionType:1.0.0`](../reference-data/DimensionType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DiscretisationSchemeType:1.0.0`](../reference-data/DiscretisationSchemeType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DocumentType:1.0.0`](../reference-data/DocumentType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DrillingReasonType:1.0.0`](../reference-data/DrillingReasonType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--EncodingFormatType:1.0.0`](../reference-data/EncodingFormatType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ExistenceKind:1.0.0`](../reference-data/ExistenceKind.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FacilityEventType:1.0.0`](../reference-data/FacilityEventType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FacilityStateType:1.0.0`](../reference-data/FacilityStateType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FacilityType:1.0.0`](../reference-data/FacilityType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FeatureType:1.0.0`](../reference-data/FeatureType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--GeoPoliticalEntityType:1.0.0`](../reference-data/GeoPoliticalEntityType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--GeologicalFormation:1.0.0`](../reference-data/GeologicalFormation.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--HeaderKeyName:1.0.0`](../reference-data/HeaderKeyName.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--InterpolationMethod:1.0.0`](../reference-data/InterpolationMethod.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LegalStatus:1.0.0`](../reference-data/LegalStatus.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LicenseState:1.0.0`](../reference-data/LicenseState.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LineageRelationshipType:1.0.0`](../reference-data/LineageRelationshipType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LinerType:1.0.0`](../reference-data/LinerType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveBusinessValue:1.0.0`](../reference-data/LogCurveBusinessValue.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveFamily:1.0.0`](../reference-data/LogCurveFamily.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveMainFamily:1.0.0`](../reference-data/LogCurveMainFamily.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveType:1.0.0`](../reference-data/LogCurveType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogType:1.0.0`](../reference-data/LogType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--MarkerType:1.0.0`](../reference-data/MarkerType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--MaterialType:1.0.0`](../reference-data/MaterialType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OSDUJsonExtensions:1.0.0`](../reference-data/OSDUJsonExtensions.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OSDURegion:1.0.0`](../reference-data/OSDURegion.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ObjectiveType:1.0.0`](../reference-data/ObjectiveType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ObligationType:1.0.0`](../reference-data/ObligationType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OperatingEnvironment:1.0.0`](../reference-data/OperatingEnvironment.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OrganisationType:1.0.0`](../reference-data/OrganisationType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGContextType:1.0.0`](../reference-data/PPFGContextType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveFamily:1.0.0`](../reference-data/PPFGCurveFamily.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveLithoType:1.0.0`](../reference-data/PPFGCurveLithoType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveMainFamily:1.0.0`](../reference-data/PPFGCurveMainFamily.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveMnemonic:1.0.0`](../reference-data/PPFGCurveMnemonic.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveProbability:1.0.0`](../reference-data/PPFGCurveProbability.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveProcessingType:1.0.0`](../reference-data/PPFGCurveProcessingType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveTransformModelType:1.0.0`](../reference-data/PPFGCurveTransformModelType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ParameterType:1.0.0`](../reference-data/ParameterType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PetroleumSystemElementType:1.0.0`](../reference-data/PetroleumSystemElementType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PlayType:1.0.0`](../reference-data/PlayType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ProcessingParameterType:1.0.0`](../reference-data/ProcessingParameterType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ProjectRole:1.0.0`](../reference-data/ProjectRole.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ProjectStateType:1.0.0`](../reference-data/ProjectStateType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PropertyFieldRepresentationType:1.0.0`](../reference-data/PropertyFieldRepresentationType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PropertyNameType:1.0.0`](../reference-data/PropertyNameType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ProspectType:1.0.0`](../reference-data/ProspectType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QualitativeSpatialAccuracyType:1.0.0`](../reference-data/QualitativeSpatialAccuracyType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QualityDataRule:1.0.0`](../reference-data/QualityDataRule.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QualityDataRuleSet:1.0.0`](../reference-data/QualityDataRuleSet.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QuantitativeAccuracyBand:1.0.0`](../reference-data/QuantitativeAccuracyBand.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ResourceCurationStatus:1.0.0`](../reference-data/ResourceCurationStatus.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ResourceLifecycleStatus:1.0.0`](../reference-data/ResourceLifecycleStatus.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ResourceSecurityClassification:1.0.0`](../reference-data/ResourceSecurityClassification.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SchemaFormatType:1.0.0`](../reference-data/SchemaFormatType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SectionType:1.0.0`](../reference-data/SectionType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicAttributeType:1.0.0`](../reference-data/SeismicAttributeType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicBinGridType:1.0.0`](../reference-data/SeismicBinGridType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicDomainType:1.0.0`](../reference-data/SeismicDomainType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicEnergySourceType:1.0.0`](../reference-data/SeismicEnergySourceType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicFaultType:1.0.0`](../reference-data/SeismicFaultType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicFilteringType:1.0.0`](../reference-data/SeismicFilteringType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicGeometryType:1.0.0`](../reference-data/SeismicGeometryType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicHorizonType:1.0.0`](../reference-data/SeismicHorizonType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicMigrationType:1.0.0`](../reference-data/SeismicMigrationType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicPickingType:1.0.0`](../reference-data/SeismicPickingType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicProcessingStageType:1.0.0`](../reference-data/SeismicProcessingStageType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicStackingType:1.0.0`](../reference-data/SeismicStackingType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicTraceDataDimensionalityType:1.0.0`](../reference-data/SeismicTraceDataDimensionalityType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicWaveType:1.0.0`](../reference-data/SeismicWaveType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SpatialGeometryType:1.0.0`](../reference-data/SpatialGeometryType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SpatialParameterType:1.0.0`](../reference-data/SpatialParameterType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--StandardsOrganisation:1.0.0`](../reference-data/StandardsOrganisation.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--StringClass:1.0.0`](../reference-data/StringClass.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TectonicSettingType:1.0.0`](../reference-data/TectonicSettingType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularAssemblyStatusType:1.0.0`](../reference-data/TubularAssemblyStatusType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularAssemblyType:1.0.0`](../reference-data/TubularAssemblyType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularComponentConnectionType:1.0.0`](../reference-data/TubularComponentConnectionType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularComponentGrade:1.0.0`](../reference-data/TubularComponentGrade.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularComponentPinBoxType:1.0.0`](../reference-data/TubularComponentPinBoxType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularComponentType:1.0.0`](../reference-data/TubularComponentType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularUmbilicalServiceType:1.0.0`](../reference-data/TubularUmbilicalServiceType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularUmbilicalType:1.0.0`](../reference-data/TubularUmbilicalType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--UnitOfMeasure:1.0.0`](../reference-data/UnitOfMeasure.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--UnitQuantity:1.0.0`](../reference-data/UnitQuantity.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VelocityAnalysisMethod:1.0.0`](../reference-data/VelocityAnalysisMethod.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VelocityDirectionType:1.0.0`](../reference-data/VelocityDirectionType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VelocityType:1.0.0`](../reference-data/VelocityType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VerticalMeasurementPath:1.0.0`](../reference-data/VerticalMeasurementPath.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VerticalMeasurementSource:1.0.0`](../reference-data/VerticalMeasurementSource.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VerticalMeasurementType:1.0.0`](../reference-data/VerticalMeasurementType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WellInterestType:1.0.0`](../reference-data/WellInterestType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WellboreTrajectoryType:1.0.0`](../reference-data/WellboreTrajectoryType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WordFormatType:1.0.0`](../reference-data/WordFormatType.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|

# `type` Schemas

| Kind                                                        |Status|Status Local|Status Community|
|-------------------------------------------------------------|----|----|----|
| &rarr; [`osdu:wks:type--Type:1.0.0`](../type/Type.1.0.0.md) |Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|

# `work-product-component` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:work-product-component--DataQuality:1.0.0`](../work-product-component/DataQuality.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--Document:1.0.0`](../work-product-component/Document.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--FaultSystem:1.0.0`](../work-product-component/FaultSystem.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--NotionalSeismicLine:1.0.0`](../work-product-component/NotionalSeismicLine.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--PPFGDataset:1.0.0`](../work-product-component/PPFGDataset.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SeismicBinGrid:1.0.0`](../work-product-component/SeismicBinGrid.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SeismicHorizon:1.0.0`](../work-product-component/SeismicHorizon.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SeismicLineGeometry:1.0.0`](../work-product-component/SeismicLineGeometry.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SeismicTraceData:1.0.0`](../work-product-component/SeismicTraceData.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--TubularAssembly:1.0.0`](../work-product-component/TubularAssembly.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--TubularComponent:1.0.0`](../work-product-component/TubularComponent.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--TubularUmbilical:1.0.0`](../work-product-component/TubularUmbilical.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--VelocityModeling:1.0.0`](../work-product-component/VelocityModeling.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellLog:1.0.0`](../work-product-component/WellLog.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellboreMarkerSet:1.0.0`](../work-product-component/WellboreMarkerSet.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellboreTrajectory:1.0.0`](../work-product-component/WellboreTrajectory.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|

# `work-product` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:work-product--WorkProduct:1.0.0`](../work-product/WorkProduct.1.0.0.md)|Changes: Differences in schema |DEVELOPMENT|DEVELOPMENT|