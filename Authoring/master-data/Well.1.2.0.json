{
  "x-osdu-review-status": "Accepted",
  "$id": "https://schema.osdu.opengroup.org/json/master-data/Well.1.2.0.json",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "Well",
  "description": "The origin of a set of wellbores.",
  "type": "object",
  "x-osdu-virtual-properties": {
    "data.VirtualProperties.DefaultLocation": {
      "type": "object",
      "priority": [
        {
          "path": "data.SpatialLocation"
        }
      ]
    },
    "data.VirtualProperties.DefaultName": {
      "type": "string",
      "priority": [
        {
          "path": "data.FacilityName"
        }
      ]
    }
  },
  "allOf": [
    {
      "$ref": "../abstract/AbstractMaster.1.1.0.json"
    },
    {
      "$ref": "../abstract/AbstractFacility.1.1.0.json"
    },
    {
      "type": "object",
      "properties": {
        "DefaultVerticalMeasurementID": {
          "type": "string",
          "description": "The default datum reference point, or zero depth point, used to determine other points vertically in a well.  References an entry in the VerticalMeasurements array."
        },
        "DefaultVerticalCRSID": {
          "type": "string",
          "description": "The default vertical coordinate reference system used in the vertical measurements for a well or wellbore if absent from input vertical measurements and there is no other recourse for obtaining a valid CRS.",
          "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-CoordinateReferenceSystem:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "VerticalMeasurements": {
          "type": "array",
          "description": "List of all depths and elevations pertaining to the well, like, water depth, mud line elevation, etc.",
          "x-osdu-indexing": {
            "type": "nested"
          },
          "items": {
            "allOf": [
              {
                "type": "object",
                "title": "Vertical Measurement ID",
                "properties": {
                  "VerticalMeasurementID": {
                    "type": "string",
                    "description": "The ID for a distinct vertical measurement within the Wellbore VerticalMeasurements array so that it may be referenced by other vertical measurements if necessary."
                  },
                  "RigID": {
                    "type": "string",
                    "title": "Rig ID",
                    "description": "The relationship to the rig, which was used while this vertical measurement was in active use.",
                    "pattern": "^[\\w\\-\\.]+:master-data\\-\\-Rig:[\\w\\-\\.\\:\\%]+:[0-9]*$"
                  }
                }
              },
              {
                "$ref": "../abstract/AbstractFacilityVerticalMeasurement.1.0.0.json"
              }
            ],
            "title": "Vertical Measurement ID"
          }
        },
        "InterestTypeID": {
          "type": "string",
          "description": "Business Interest [Well Interest Type] describes whether a company currently considers a well or its data to be a real or planned asset, and if so, the nature of and motivation for that company's interest.",
          "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellInterestType:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "BusinessIntentionID": {
          "type": "string",
          "title": "Business Intention ID",
          "description": "Business Intention [Well Business Intention] is the general purpose for which resources are approved for drilling a new well or subsequent wellbore(s).",
          "x-osdu-attribution-authority": "PPDM Association",
          "x-osdu-attribution-publication": "Well Status and Classification",
          "x-osdu-attribution-revision": "v3 (June 2020)",
          "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellBusinessIntention:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "RoleID": {
          "type": "string",
          "title": "Role ID",
          "description": "Role [Well Role] is the current purpose, whether planned or actual. If there are multiple Roles among a well's components, the well may be assigned the facet value with the highest significance. The value of Role may change over the Life Cycle.",
          "x-osdu-attribution-authority": "PPDM Association",
          "x-osdu-attribution-publication": "Well Status and Classification",
          "x-osdu-attribution-revision": "v3 (June 2020)",
          "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellRole:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "HistoricalInterests": {
          "type": "array",
          "title": "Historical Interests",
          "description": "The list of past and present interests associated with the time period they were/are valid",
          "items": {
            "type": "object",
            "title": "HistoricalInterest",
            "description": "A 'well interest' at some time period as defined by effective and termination date.",
            "properties": {
              "InterestTypeID": {
                "type": "string",
                "title": "Interest Type ID",
                "description": "Business Interest [Well Interest Type] describes whether a company currently considers a well or its data to be a real or planned asset, and if so, the nature of and motivation for that company's interest.",
                "x-osdu-attribution-authority": "PPDM Association",
                "x-osdu-attribution-publication": "Well Status and Classification",
                "x-osdu-attribution-revision": "v3 (June 2020)",
                "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellInterestType:[\\w\\-\\.\\:\\%]+:[0-9]*$"
              },
              "EffectiveDateTime": {
                "type": "string",
                "title": "Effective Date Time",
                "description": "The date and time at which the well interest type becomes effective.",
                "format": "date",
                "x-osdu-frame-of-reference": "DateTime"
              },
              "TerminationDateTime": {
                "type": "string",
                "title": "Termination Date Time",
                "description": "The date and time at which the well interest type is no longer in effect.",
                "format": "date",
                "x-osdu-frame-of-reference": "DateTime"
              }
            }
          }
        },
        "WasBusinessInterestFinancialOperated": {
          "type": "boolean",
          "title": "BusinessInterestFinancialOperatedFormerly",
          "description": "Identifies, for the purpose of current use, if the Business Interest [Well Interest Type] for this well has ever been FinancialOperated in the past.",
          "x-osdu-attribution-authority": "PPDM Association",
          "x-osdu-attribution-publication": "Well Status and Classification",
          "x-osdu-attribution-revision": "v3 (June 2020)"
        },
        "WasBusinessInterestFinancialNonOperated": {
          "type": "boolean",
          "title": "BusinessInterestFinancialNonOperatedFormerly",
          "description": "Identifies, for the purpose of current use, if the Business Interest [Well Interest Type] for this well has ever been FinancialNonOperated in the past.",
          "x-osdu-attribution-authority": "PPDM Association",
          "x-osdu-attribution-publication": "Well Status and Classification",
          "x-osdu-attribution-revision": "v3 (June 2020)"
        },
        "WasBusinessInterestObligatory": {
          "type": "boolean",
          "title": "BusinessInterestObligatoryFormerly",
          "description": "Identifies, for the purpose of current use, if the Business Interest [Well Interest Type] for this well has ever been Obligatory in the past.",
          "x-osdu-attribution-authority": "PPDM Association",
          "x-osdu-attribution-publication": "Well Status and Classification",
          "x-osdu-attribution-revision": "v3 (June 2020)"
        },
        "WasBusinessInterestTechnical": {
          "type": "boolean",
          "title": "BusinessInterestTechnicalFormerly",
          "description": "Identifies, for the purpose of current use, if the Business Interest [Well Interest Type] for this well has ever been Technical in the past.",
          "x-osdu-attribution-authority": "PPDM Association",
          "x-osdu-attribution-publication": "Well Status and Classification",
          "x-osdu-attribution-revision": "v3 (June 2020)"
        },
        "ConditionID": {
          "type": "string",
          "title": "ConditionID",
          "description": "Condition [Well Condition] is the operational state of a well component relative to the Role [Well Role].",
          "x-osdu-attribution-authority": "PPDM Association",
          "x-osdu-attribution-publication": "Well Status and Classification",
          "x-osdu-attribution-revision": "v3 (June 2020)",
          "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellCondition:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "OutcomeID": {
          "type": "string",
          "title": "OutcomeID",
          "description": "Outcome [Well Drilling Outcome] is the result of attempting to accomplish the Business Intention [Well Business Intention].",
          "x-osdu-attribution-authority": "PPDM Association",
          "x-osdu-attribution-publication": "Well Status and Classification",
          "x-osdu-attribution-revision": "v3 (June 2020)",
          "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellBusinessIntentionOutcome:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "StatusSummaryID": {
          "type": "string",
          "title": "StatusSummaryID",
          "description": "Identifies the status of a well component in a way that may combine and-or summarize concepts found in other status facets. For example, a Well Status Summary of Gas Injector Shut-in, which contains commonly desired business information, combines concepts from Product Type, Fluid Direction, and Condition.",
          "x-osdu-attribution-authority": "OSDU",
          "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellStatusSummary:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        }
      }
    }
  ]
}