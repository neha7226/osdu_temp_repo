<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# WellActivityPhaseType [Status: Accepted]
Key milestones in a well activity program, usually based on the major sections of the well or non well-related work
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--WellActivityPhaseType:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: osdu
* Reference value type, governance model: OPEN
* Link to &rarr; Proposal workbook [schema-WellActivityPhaseType.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/reference-data/schema-WellActivityPhaseType.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema WellActivityPhaseType.1.0.0.json](../../Authoring/reference-data/WellActivityPhaseType.1.0.0.json)
* Link to &rarr; [Generated Schema WellActivityPhaseType.1.0.0.json](../../Generated/reference-data/WellActivityPhaseType.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource WellActivityPhaseType.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/WellActivityPhaseType.1.0.0.json)
* Link to &rarr; [Example Record WellActivityPhaseType.1.0.0.json](../../Examples/reference-data/WellActivityPhaseType.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams WellActivityPhaseType


## Outgoing Relationships for WellActivityPhaseType

![WellActivityPhaseType](../_diagrams/reference-data/WellActivityPhaseType.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## WellActivityPhaseType Referenced by other Entities

![WellActivityPhaseType](../_diagrams/reference-data/WellActivityPhaseType.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# WellActivityPhaseType Properties

## 1. Table of WellActivityPhaseType System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-WellActivityPhaseType:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WellActivityPhaseType:5b67266e-8ead-5954-b16a-ae383e4b869e|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--WellActivityPhaseType:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of WellActivityPhaseType Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of WellActivityPhaseType Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of WellActivityPhaseType Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of WellActivityPhaseType Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for WellActivityPhaseType.1.0.0

* Status: Published
* Governance: OPEN
* The table below shows all 23 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [WellActivityPhaseType](../../ReferenceValues/Manifests/reference-data/OPEN/WellActivityPhaseType.1.0.0.json).

|`id` with prefix `namespace:reference-data--WellActivityPhaseType:`|Code|Name|Alias|Description|AttributionAuthority|Source|CommitDate|
|-----|-----|-----|-----|-----|-----|-----|-----|
|CLNDSP|CLNDSP|Clean/Displace|-|Includes all activities associated with wellbore clean-out or displacing the well with completion fluid.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|COMP|COMP|Completion|-|All activities related to the running and installation of completions. Includes installing equipment, perforating, running sand face completion equipment, stimulating the well, running tubing, running upper and lower completion strings, running and installing equipment from the completion to the well head.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|COND|COND|Conductor|-|Includes all activities performed in the conductor hole section|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|DEMOB|DEMOB|Demobilise|-|Tasks associated with rigging down and demobilising the rig/unit at the end of operations|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|INTERV|INTERV|Intervention/Workover|-|Activities carried out on a well, or at the end of, its productive life that alters the state of the well or well geometry, provides well diagnostics, or manages the production of the well|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|INT|INT|Intermediate|-|Includes all activities performed in the intermediate hole interval, including but not restricted to the time to pick up the next BHA, trip-in hole, drill-out cement, and float equipment, pressure test the formation, drill the section, case/line the section and cement the casing/liner.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|LCOMP|LCOMP|Lower Completion|-|All activities related to runing and installing completions in the portion of the well across the production or injection zone. Includes all activities associated with installing the completion zones and/or intervals including perforating, running sand face completion equipment, stimulating the well, and recovering the wash pipe and running string|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|LOC|LOC|Location/Site Prep|-|The site preparation involves building clearing land for use by the rig, building access roads to the well site or well pad, construct infrastructure for water, water disposal, and electricity, dig and line all mud pits to prevent ground water or water table contamination, dig reserve pits for cutting storage (for eventual disposal), install rat hole and the mousehole.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|MOB|MOB|Mobilisation|-|Tasks associated with securing and rigging up the rig/unit to the point it is ready to begin operations|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|NONWELL|NONWELL|Non-Well|-|Non-well related maintenance and upgrade activities performed to the rig/platform and associated surface equipment.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PABAN|PABAN|Permanent Abandonment|-|Permanent abandonment of a well usually involves setting and testing cement plugs across distinct permeable zones (e.g., open hydrocarbon bearing formations, casing shoes, freshwater aquifers, and other areas near the surface of the wellbore)|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PREP|PREP|Prepare|-|Activities after mobilisation of rig or unit including slot recovery operations, preparation for sidetracking and diagnostic work prior to the main work starting.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PROD|PROD|Production|-|Includes all tasks and activities performed in the production hole interval (e.g., expected hydrocarbon producing interval). Including, but not restricted to the time to pick up the next BHA, trip in hole, drill-out cement and float equipment, pressure test the formation, drill the section / well to TD|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|RES|RES|Resume|-|Activities undertaken to return a well to an operational status following a suspension|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA ff4b1de9.|2022-01-14T08:52:01+01:00|
|RMOVE|RMOVE|Rig Move|-|Used to capture time spent moving the rig to a new well location or within operational proximity (e.g., safe zone). Rig move can include moving from yard to location, between locations, or skidding the rig on a location|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|SUBS|SUBS|Subsea|-|All activities associated with installing and maintaining subsea equipment and system.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA ff4b1de9.|2022-01-14T08:52:01+01:00|
|SURF|SURF|Surface|-|Includes all activities performed in the surface hole interval, including, but not restricted to the time to pick up the next BHA, trip in hole, drill-out cement and float equipment, pressure test the formation, drill the section, case the section and cement the casing. Typically, this string follows the conductor casing and precedes the intermediate casing.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|SUS|SUS|Suspend|-|Activities undertaken to isolate fluids in the wellbore while providing a means to easily return to the wellbore for ongoing operations. Also includes operations such as unlatching BOP, installation of THS and/or horizontal Xtree, latch and testing BOPs|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|TABAN|TABAN|Temporary Abandonment|-|Includes all activities involved in the isolation of the completed interval or intervals within a wellbore from the surface by means of a cement retainer, cast iron bridge plug, cement plug, tubing, and packer with tubing plug, or any combination. Note: Temporary abandonment applies to developing and active Wells.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|TIEB|TIEB|Tieback|-|Includes all activities performed in support of tying back casing. A tieback string is a partial string used to tieback liner or mudline suspended casing strings to the surface.|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|UCOMP|UCOMP|Upper Completion|-|All activities related to running and installing all equipment from the lower completion to the well head at the surface. includes attaching the tubing to the lower packer to allow flow through the tubing while isolating the casing from the pressure and fluids flowing from the produced reservoir. The upper Completion also includes the tubing and all other accessories that allow the well to flow|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|WELTST|WELTST|Well Test|-|All activities associated with establishing key production measurements such as flow rate, fluid properties and composition, pressure, and temperature at live well conditions|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|WHDTRE|WHDTRE|Wellhead/Tree|-|Includes time associated with installation of the wellhead and/or Xtree equipment for production. Includes time spent: Installing the THS or the horizontal Xtree during other events; Running the completion riser to remove the plugs; Running and testing supplemental equipment (e.g., lock-down sleeves)|OSDU|Workbook Published/WellActivityPhaseType.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# WellActivityPhaseType.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [OperationsReport.1.0.0](../master-data/OperationsReport.1.0.0.md) | Cost[].ActivityPhaseID | many (as array) |  |
| Referred to by | [OperationsReport.1.1.0](../master-data/OperationsReport.1.1.0.md) | Cost[].ActivityPhaseID | many (as array) |  |
| Referred to by | [OperationsReport.1.2.0](../master-data/OperationsReport.1.2.0.md) | Cost[].ActivityPhaseID | many (as array) |  |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [WellActivityProgram.1.0.0](../master-data/WellActivityProgram.1.0.0.md) | Phases[].TypeID | many (as array) |  |
| Referred to by | [Wellbore.1.3.0](../master-data/Wellbore.1.3.0.md) | WellboreCosts[].ActivityTypeID | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._