<a name="TOC"></a>

[[_TOC_]]
# UnitOfMeasure Content Changes
## Value Changes

Total number of changes 18 in total 1442 UnitOfMeasure records.

| Name | ParentName | Changed from | Changed to | ID |
|---|---|---|---|---|
| API gamma ray unit|non-dimensional|UnitQuantity:none|UnitQuantity:API gamma ray|{{NAMESPACE}}:reference-data--UnitOfMeasure:gAPI |
| API gravity unit|non-dimensional|UnitQuantity:none|UnitQuantity:API gravity|{{NAMESPACE}}:reference-data--UnitOfMeasure:dAPI |
| API neutron unit|non-dimensional|UnitQuantity:none|UnitQuantity:API neutron|{{NAMESPACE}}:reference-data--UnitOfMeasure:nAPI |
| bel|non-dimensional|UnitQuantity:none|UnitQuantity:logarithmic power ratio|{{NAMESPACE}}:reference-data--UnitOfMeasure:B |
| bel per metre|non-dimensional|UnitQuantity:none|UnitQuantity:logarithmic power ratio per length|{{NAMESPACE}}:reference-data--UnitOfMeasure:B%2Fm |
| bel per octave|non-dimensional|UnitQuantity:none|UnitQuantity:attenuation per frequency interval|{{NAMESPACE}}:reference-data--UnitOfMeasure:B%2FO |
| bel watt|non-dimensional|UnitQuantity:none|UnitQuantity:normalized power|{{NAMESPACE}}:reference-data--UnitOfMeasure:B.W |
| decibel|non-dimensional|UnitQuantity:none|UnitQuantity:logarithmic power ratio|{{NAMESPACE}}:reference-data--UnitOfMeasure:dB |
| decibel megawatt|non-dimensional|UnitQuantity:none|UnitQuantity:normalized power|{{NAMESPACE}}:reference-data--UnitOfMeasure:dB.MW |
| decibel milliwatt|non-dimensional|UnitQuantity:none|UnitQuantity:normalized power|{{NAMESPACE}}:reference-data--UnitOfMeasure:dB.mW |
| decibel per foot|non-dimensional|UnitQuantity:none|UnitQuantity:logarithmic power ratio per length|{{NAMESPACE}}:reference-data--UnitOfMeasure:dB%2Fft |
| decibel per kilometre|non-dimensional|UnitQuantity:none|UnitQuantity:logarithmic power ratio per length|{{NAMESPACE}}:reference-data--UnitOfMeasure:dB%2Fkm |
| decibel per metre|non-dimensional|UnitQuantity:none|UnitQuantity:logarithmic power ratio per length|{{NAMESPACE}}:reference-data--UnitOfMeasure:dB%2Fm |
| decibel per octave|non-dimensional|UnitQuantity:none|UnitQuantity:attenuation per frequency interval|{{NAMESPACE}}:reference-data--UnitOfMeasure:dB%2FO |
| decibel watt|non-dimensional|UnitQuantity:none|UnitQuantity:normalized power|{{NAMESPACE}}:reference-data--UnitOfMeasure:dB.W |
| octave|non-dimensional|UnitQuantity:none|UnitQuantity:frequency interval|{{NAMESPACE}}:reference-data--UnitOfMeasure:O |
| volt per bel|non-dimensional|UnitQuantity:none|UnitQuantity:potential difference per power drop|{{NAMESPACE}}:reference-data--UnitOfMeasure:V%2FB |
| volt per decibel|non-dimensional|UnitQuantity:none|UnitQuantity:potential difference per power drop|{{NAMESPACE}}:reference-data--UnitOfMeasure:V%2FdB |

[Back to TOC](#TOC)

## NameAlias Changes

Total number of alias changes 1148 in total 1442 UnitOfMeasure records.

| Name | # previous Alias Names | # of current Aliases |
|---|---|---|
| (BTU per hour) per square foot|0|1 |
| (BTU per second) per cubic foot|0|1 |
| (BTU per second) per cubic foot delta Fahrenheit|0|1 |
| (BTU per second) per square foot delta Fahrenheit|0|1 |
| (UK gallon per hour) per hour|0|1 |
| (UK gallon per minute) per minute|0|1 |
| (US gallon per hour) per hour|0|1 |
| (US gallon per minute) per minute|0|1 |
| (barrel per day) per (barrel per day)|0|1 |
| (barrel per day) per day|0|1 |
| (barrel per hour) per hour|0|1 |
| (cubic decimetre per second) per second|0|3 |
| (cubic foot per day) per day|0|1 |
| (cubic foot per day) per foot|0|2 |
| (cubic foot per hour) per hour|0|1 |
| (cubic foot per minute) per minute|0|2 |
| (cubic foot per second) per second|0|2 |
| (cubic metre per day) per (cubic metre per day)|0|2 |
| (cubic metre per day) per bar|0|3 |
| (cubic metre per day) per day|0|3 |
| (cubic metre per day) per kilopascal|0|5 |
| (cubic metre per day) per metre|0|3 |
| (cubic metre per day) per psi|0|1 |
| (cubic metre per hour) per bar|0|3 |
| (cubic metre per hour) per kilopascal|0|3 |
| (cubic metre per hour) per metre|0|3 |
| (cubic metre per million cubic metre) per delta Celsius|0|1 |
| (cubic metre per million cubic metre) per delta Fahrenheit|0|1 |
| (cubic metre per minute) per bar|0|3 |
| (cubic metre per second) per (cubic metre per second)|0|1 |
| (cubic metre per second) per foot|0|3 |
| (litre per minute) per bar|0|2 |
| (litre per second) per second|0|3 |
| (million cubic foot per day) per (barrel per day)|0|1 |
| (part per million [volume basis)] per delta Fahrenheit|0|2 |
| (part per million [volume basis]) per delta Celsius|0|2 |
| (thousand cubic foot per day) per foot|0|3 |
| (thousand cubic foot per day) per psi|0|1 |
| (thousand cubic metre per day) per metre|0|3 |
| (thousand cubic metre per hour) per metre|0|3 |
| (thousand psi) squared|0|1 |
| 10 kilometre|0|1 |
| 100 kilometre|0|1 |
| API gamma ray unit|0|4 |
| API gravity unit|0|3 |
| API neutron unit|0|4 |
| BTU per (hour square foot delta Fahrenheit per inch)|0|1 |
| BTU per cubic foot|0|1 |
| BTU per hour cubic foot|0|1 |
| BTU per hour cubic foot delta Fahrenheit|0|1 |
| BTU per hour square foot delta Fahrenheit|0|1 |
| BTU per hour square foot delta Rankine|0|1 |
| BTU per hour square metre delta Celsius|0|1 |
| BTU per pound-mass|0|2 |
| BTU per pound-mass delta Fahrenheit|0|2 |
| BTU per pound-mass delta Rankine|0|2 |
| BTU per pound-mass-mole|0|1 |
| BTU per pound-mass-mole delta Fahrenheit|0|1 |
| BTU per second square foot|0|1 |
| British chain [Benoit 1895 A]|0|3 |
| British chain [Benoit 1895 B]|0|3 |
| British chain [Sears 1922 truncated]|0|2 |
| British chain [Sears 1922]|0|3 |
| British foot [1865]|0|2 |
| British foot [1936]|0|3 |
| British foot [Benoit 1895 A]|0|2 |
| British foot [Benoit 1895 B]|0|3 |
| British foot [Sears 1922 truncated]|0|1 |
| British foot [Sears 1922]|0|3 |
| British link [Benoit 1895 A]|0|3 |
| British link [Benoit 1895 B]|0|3 |
| British link [Sears 1922 truncated]|0|2 |
| British link [Sears 1922]|0|3 |
| British thermal unit|0|2 |
| British yard [Benoit 1895 A]|0|3 |
| British yard [Benoit 1895 B]|0|3 |
| British yard [Sears 1922 truncated]|0|2 |
| British yard [Sears 1922]|0|3 |
| Clarke chain|0|1 |
| Clarke foot|0|1 |
| Clarke link|0|1 |
| Clarke yard|0|1 |
| German legal metre|0|2 |
| Gold Coast foot|0|3 |
| Indian Chain [1937]|0|1 |
| Indian yard|0|3 |
| Indian yard [1937]|0|3 |
| Indian yard [1962]|0|3 |
| Indian yard [1975]|0|3 |
| UK gallon|0|1 |
| UK gallon per cubic foot|0|1 |
| UK gallon per hour square foot|0|1 |
| UK gallon per hour square inch|0|1 |
| UK gallon per minute square foot|0|1 |
| UK gallon per thousand UK gallon|0|2 |
| UK ton-force per square foot|0|1 |
| UK ton-force square foot|0|1 |
| UK ton-mass|0|1 |
| US gallon|0|2 |
| US gallon per cubic foot|0|1 |
| US gallon per hour square foot|0|1 |
| US gallon per hour square inch|0|1 |
| US gallon per minute square foot|0|1 |
| US gallon per thousand US gallon|0|2 |
| US gallon per thousand cubic foot|0|1 |
| US survey chain|0|3 |
| US survey foot|0|4 |
| US survey inch|0|3 |
| US survey link|0|3 |
| US survey mile|0|3 |
| US ton-force per square foot|0|1 |
| US ton-force per square inch|0|1 |
| US ton-force square foot|0|1 |
| US ton-mass|0|2 |
| US ton-mass per square foot|0|1 |
| acre|0|2 |
| ampere|0|4 |
| ampere hour|0|2 |
| ampere per metre|0|2 |
| ampere per millimetre|0|2 |
| ampere per square centimetre|0|3 |
| ampere per square foot|0|3 |
| ampere per square metre|0|3 |
| ampere per square millimetre|0|3 |
| ampere second|0|1 |
| ampere second per cubic metre|0|2 |
| ampere second per kilogram|0|1 |
| ampere square metre|0|3 |
| angstrom|0|2 |
| angular degree|0|2 |
| angular degree per foot|0|1 |
| angular degree per hour|0|1 |
| angular degree per hundred foot|0|1 |
| angular degree per metre|0|1 |
| angular degree per minute|0|1 |
| angular degree per second|0|1 |
| angular degree per thirty foot|0|1 |
| angular degree per thirty metre|0|1 |
| angular minute|0|2 |
| angular second|0|2 |
| attogram|0|3 |
| attojoule|0|3 |
| bar|0|4 |
| bar per hour|0|2 |
| bar per kilometre|0|2 |
| bar per metre|0|2 |
| bar squared|0|3 |
| bar squared per centipoise|0|3 |
| barn|0|4 |
| barn per cubic centimetre|0|3 |
| barrel|0|2 |
| barrel per barrel|0|2 |
| barrel per cubic foot|0|1 |
| barrel per cubic metre|0|1 |
| barrel per hundred barrel|0|2 |
| barrel per million cubic foot|0|1 |
| barrel per million cubic metre|0|1 |
| barrel per thousand cubic foot|0|1 |
| barrel per thousand cubic metre|0|1 |
| baud|0|1 |
| becquerel|0|3 |
| becquerel per kilogram|0|2 |
| bel|0|3 |
| bel per metre|0|2 |
| bel per octave|0|2 |
| bit|0|2 |
| bit per second|0|1 |
| byte|0|2 |
| byte per second|0|1 |
| calorie|0|2 |
| calorie [International Table]|0|2 |
| calorie per cubic centimetre|0|3 |
| calorie per cubic millimetre|0|3 |
| calorie per gram|0|2 |
| calorie per gram delta kelvin|0|2 |
| calorie per gram-mole delta Celsius|0|1 |
| calorie per hour cubic centimetre|0|1 |
| calorie per hour square centimetre|0|1 |
| calorie per hour square centimetre delta Celsius|0|1 |
| calorie per kilogram|0|2 |
| calorie per millilitre|0|2 |
| calorie per second centimetre delta Celsius|0|2 |
| calorie per second cubic centimetre|0|3 |
| calorie per second square centimetre delta Celsius|0|3 |
| candela|0|4 |
| candela per square metre|0|3 |
| capture unit|0|6 |
| carat|0|2 |
| centesimal-minute|0|1 |
| centesimal-second|0|1 |
| centiampere|0|1 |
| centicoulomb|0|1 |
| centieuclid|0|2 |
| centifarad|0|1 |
| centigauss|0|1 |
| centigram|0|1 |
| centigray|0|1 |
| centihenry|0|1 |
| centijoule|0|1 |
| centimetre|0|4 |
| centimetre of water at 4 degree Celsius|0|3 |
| centimetre per second|0|2 |
| centimetre per square second|0|3 |
| centimetre to the fourth power|0|2 |
| centinewton|0|1 |
| centiohm|0|1 |
| centipascal|0|1 |
| centipoise|0|4 |
| centisecond|0|2 |
| centisiemens|0|2 |
| centistokes|0|2 |
| centitesla|0|1 |
| centivolt|0|1 |
| centiwatt|0|1 |
| centiweber|0|1 |
| chain|0|2 |
| coulomb|0|4 |
| coulomb metre|0|2 |
| coulomb per cubic centimetre|0|3 |
| coulomb per cubic metre|0|3 |
| coulomb per cubic millimetre|0|3 |
| coulomb per gram|0|2 |
| coulomb per kilogram|0|2 |
| coulomb per square centimetre|0|3 |
| coulomb per square metre|0|3 |
| coulomb per square millimetre|0|3 |
| cubic centimetre|0|3 |
| cubic centimetre per cubic centimetre|0|3 |
| cubic centimetre per cubic metre|0|3 |
| cubic centimetre per gram|0|3 |
| cubic centimetre per hour|0|3 |
| cubic centimetre per litre|0|1 |
| cubic centimetre per minute|0|3 |
| cubic centimetre per second|0|3 |
| cubic centimetre per thirty minute|0|2 |
| cubic decimetre|0|3 |
| cubic decimetre per cubic metre|0|3 |
| cubic decimetre per hundred kilometre|0|5 |
| cubic decimetre per kilogram|0|3 |
| cubic decimetre per kilogram-mole|0|2 |
| cubic decimetre per kilowatt hour|0|1 |
| cubic decimetre per megajoule|0|3 |
| cubic decimetre per metre|0|3 |
| cubic decimetre per second|0|3 |
| cubic decimetre per ton|0|3 |
| cubic foot|0|3 |
| cubic foot per 94-pound-sack|0|1 |
| cubic foot per barrel|0|1 |
| cubic foot per cubic foot|0|3 |
| cubic foot per day|0|2 |
| cubic foot per day foot psi|0|1 |
| cubic foot per foot|0|3 |
| cubic foot per hour|0|2 |
| cubic foot per kilogram|0|2 |
| cubic foot per minute|0|2 |
| cubic foot per minute square foot|0|3 |
| cubic foot per pound-mass|0|1 |
| cubic foot per pound-mass-mole|0|1 |
| cubic foot per radian|0|1 |
| cubic foot per second|0|2 |
| cubic foot per second square foot|0|3 |
| cubic foot per square foot|0|1 |
| cubic inch|0|1 |
| cubic inch per foot|0|3 |
| cubic kilometre|0|3 |
| cubic metre|0|3 |
| cubic metre per Pascal|0|2 |
| cubic metre per UK ton-mass|0|1 |
| cubic metre per US ton-mass|0|1 |
| cubic metre per barrel|0|1 |
| cubic metre per cubic metre|0|4 |
| cubic metre per cubic metre delta kelvin|0|1 |
| cubic metre per day|0|3 |
| cubic metre per gram|0|3 |
| cubic metre per gram-mole|0|3 |
| cubic metre per hectare metre|0|3 |
| cubic metre per hour|0|3 |
| cubic metre per joule|0|3 |
| cubic metre per kilogram|0|3 |
| cubic metre per kilogram-mole|0|2 |
| cubic metre per kilometre|0|3 |
| cubic metre per kilopascal|0|1 |
| cubic metre per kilowatt hour|0|2 |
| cubic metre per metre|0|3 |
| cubic metre per minute|0|3 |
| cubic metre per pascal second|0|5 |
| cubic metre per radian|0|2 |
| cubic metre per revolution|0|1 |
| cubic metre per second|0|3 |
| cubic metre per second metre|0|3 |
| cubic metre per second square metre|0|3 |
| cubic metre per second squared|0|3 |
| cubic metre per square metre|0|3 |
| cubic metre per time cubic metre|0|1 |
| cubic metre per tonne|0|3 |
| cubic mile|0|1 |
| cubic millimetre|0|3 |
| cubic millimetre per joule|0|3 |
| cubic yard|0|1 |
| curie|0|5 |
| day|0|4 |
| day per cubic foot|0|2 |
| day per cubic metre|0|3 |
| day per thousand cubic foot|0|3 |
| deciampere|0|1 |
| decibel|0|4 |
| decibel per foot|0|2 |
| decibel per kilometre|0|2 |
| decibel per metre|0|2 |
| decibel per octave|0|2 |
| decibel watt|0|1 |
| decicoulomb|0|1 |
| decieuclid|0|1 |
| decifarad|0|1 |
| decigauss|0|1 |
| decigray|0|1 |
| decihenry|0|1 |
| decijoule|0|1 |
| decimetre|0|5 |
| decimetre per second|0|2 |
| decinewton|0|1 |
| decinewton metre|0|2 |
| deciohm|0|1 |
| decipascal|0|1 |
| decipoise|0|1 |
| decisecond|0|1 |
| decisiemens|0|2 |
| decitesla|0|1 |
| decivolt|0|1 |
| deciwatt|0|1 |
| deciweber|0|1 |
| degree Celsius|0|4 |
| degree Fahrenheit|0|4 |
| degree Rankine|0|3 |
| degree kelvin|0|4 |
| dekametre|0|2 |
| dekanewton|0|6 |
| dekanewton metre|0|2 |
| delta Celsius|0|2 |
| delta Celsius per foot|0|2 |
| delta Celsius per hectometre|0|2 |
| delta Celsius per hour|0|2 |
| delta Celsius per kilometre|0|2 |
| delta Celsius per metre|0|2 |
| delta Celsius per minute|0|2 |
| delta Celsius per second|0|2 |
| delta Celsius square metre hour per thousand calorie|0|1 |
| delta Fahrenheit|0|2 |
| delta Fahrenheit per hour|0|2 |
| delta Fahrenheit per metre|0|2 |
| delta Fahrenheit per minute|0|2 |
| delta Fahrenheit per second|0|2 |
| delta Fahrenheit square foot hour per BTU|0|1 |
| delta Rankine|0|2 |
| delta kelvin|0|2 |
| delta kelvin per Pascal|0|1 |
| delta kelvin per kilometre|0|2 |
| delta kelvin per metre|0|2 |
| delta kelvin per second|0|2 |
| delta kelvin per watt|0|2 |
| delta kelvin square metre per kilowatt|0|3 |
| delta kelvin square metre per watt|0|3 |
| dyne|0|2 |
| dyne per centimetre|0|2 |
| dyne per square centimetre|0|3 |
| dyne second per square centimetre|0|3 |
| dyne square centimetre|0|3 |
| electric-horsepower|0|3 |
| erg|0|2 |
| erg per cubic centimetre|0|3 |
| erg per cubic metre|0|3 |
| erg per gram|0|2 |
| erg per kilogram|0|2 |
| erg per square centimetre|0|3 |
| euclid|0|4 |
| exaampere|0|1 |
| exacoulomb|0|1 |
| exaeuclid|0|1 |
| exafarad|0|1 |
| exagauss|0|1 |
| exagram|0|1 |
| exagray|0|1 |
| exahenry|0|1 |
| exajoule|0|3 |
| exametre|0|1 |
| exanewton|0|1 |
| exaohm|0|1 |
| exapascal|0|1 |
| exapoise|0|1 |
| exasiemens|0|2 |
| exatesla|0|1 |
| exawatt|0|1 |
| exaweber|0|1 |
| farad|0|3 |
| farad per metre|0|2 |
| femtoampere|0|1 |
| femtocalorie|0|1 |
| femtocoulomb|0|4 |
| femtoeuclid|0|1 |
| femtofarad|0|1 |
| femtogauss|0|1 |
| femtogram|0|1 |
| femtogray|0|1 |
| femtohenry|0|1 |
| femtojoule|0|1 |
| femtometre|0|3 |
| femtonewton|0|1 |
| femtoohm|0|1 |
| femtopascal|0|1 |
| femtopoise|0|1 |
| femtorad|0|1 |
| femtosiemens|0|2 |
| femtotesla|0|1 |
| femtovolt|0|1 |
| femtowatt|0|1 |
| femtoweber|0|1 |
| foot|0|5 |
| foot per cubic foot|0|1 |
| foot per day|0|2 |
| foot per delta Fahrenheit|0|2 |
| foot per foot|0|2 |
| foot per hour|0|2 |
| foot per hundred foot|0|2 |
| foot per inch|0|2 |
| foot per metre|0|2 |
| foot per microsecond|0|2 |
| foot per mile|0|2 |
| foot per millisecond|0|2 |
| foot per minute|0|2 |
| foot per second|0|2 |
| foot per second squared|0|3 |
| foot pound-force per square inch|0|1 |
| galileo|0|4 |
| gauss|0|2 |
| gigaampere|0|1 |
| gigabecquerel|0|3 |
| gigacoulomb|0|1 |
| gigaelectronvolt|0|1 |
| gigaeuclid|0|1 |
| gigafarad|0|1 |
| gigagauss|0|1 |
| gigagram|0|1 |
| gigagray|0|1 |
| gigahenry|0|1 |
| gigahertz|0|1 |
| gigajoule|0|3 |
| gigametre|0|1 |
| giganewton|0|1 |
| gigaohm|0|3 |
| gigapascal|0|4 |
| gigapascal per centimetre|0|2 |
| gigapascal squared|0|3 |
| gigapoise|0|1 |
| gigasiemens|0|4 |
| gigatesla|0|1 |
| gigavolt|0|1 |
| gigawatt|0|3 |
| gigawatt hour|0|2 |
| gigaweber|0|1 |
| grain|0|2 |
| grain per cubic foot|0|1 |
| grain per hundred cubic foot|0|1 |
| gram|0|4 |
| gram foot per cubic centimetre second|0|3 |
| gram metre per cubic centimetre second|0|1 |
| gram per centimetre to the fourth power|0|2 |
| gram per cubic centimetre|0|3 |
| gram per cubic decimetre|0|3 |
| gram per cubic metre|0|3 |
| gram per kilogram|0|2 |
| gram per litre|0|2 |
| gram per second|0|2 |
| gram-force|0|3 |
| gram-mole|0|6 |
| gram-mole per cubic metre|0|3 |
| gram-mole per second|0|2 |
| gram-mole per second square metre|0|3 |
| gram-mole per square metre|0|3 |
| gravity|0|2 |
| gray|0|3 |
| half of Foot|0|2 |
| half of millisecond|0|3 |
| hectare|0|3 |
| hectare metre|0|2 |
| hectolitre|0|3 |
| hectometre|0|2 |
| hectonewton|0|1 |
| hectosecond|0|2 |
| henry|0|3 |
| henry per metre|0|2 |
| hertz|0|3 |
| horsepower|0|1 |
| horsepower per cubic foot|0|1 |
| horsepower per square inch|0|1 |
| hour|0|4 |
| hour per cubic foot|0|3 |
| hour per cubic metre|0|3 |
| hour per kilometre|0|2 |
| hour per thousand foot|0|2 |
| hundred bar|0|3 |
| hundred foot|0|1 |
| hundredth of calorie|0|1 |
| hundredth of rad|0|1 |
| hydraulic-horsepower|0|3 |
| hydraulic-horsepower per square inch|0|3 |
| inch|0|5 |
| inch of mercury at 32 degree Fahrenheit|0|2 |
| inch of mercury at 60 degree Fahrenheit|0|2 |
| inch of water at 39.2 degree Fahrenheit|0|2 |
| inch of water at 60 degree Fahrenheit|0|3 |
| inch per inch delta Fahrenheit|0|2 |
| inch per minute|0|2 |
| inch per second|0|2 |
| inch per second squared|0|2 |
| indian foot|0|3 |
| indian foot [1937]|0|3 |
| indian foot [1975]|0|3 |
| indian foot ]1962]|0|3 |
| international fathom|0|3 |
| international nautical mile|0|3 |
| joule|0|4 |
| joule metre per second square metre delta kelvin|0|1 |
| joule metre per square metre|0|1 |
| joule per cubic decimetre|0|3 |
| joule per cubic metre|0|3 |
| joule per delta kelvin|0|2 |
| joule per gram|0|2 |
| joule per gram delta kelvin|0|2 |
| joule per gram-mole|0|2 |
| joule per gram-mole delta kelvin|0|2 |
| joule per kilogram|0|2 |
| joule per kilogram delta kelvin|0|2 |
| joule per metre|0|2 |
| joule per second square metre delta Celsius|0|3 |
| joule per square centimetre|0|3 |
| joule per square metre|0|3 |
| kibibyte|0|2 |
| killowatt per cubic metre delta kelvin|0|3 |
| kiloampere|0|3 |
| kilocandela|0|3 |
| kilocoulomb|0|3 |
| kilodyne|0|2 |
| kiloelectronvolt|0|2 |
| kiloeuclid|0|1 |
| kilofarad|0|1 |
| kilogauss|0|1 |
| kilogram|0|4 |
| kilogram metre|0|2 |
| kilogram metre per second|0|2 |
| kilogram metre per square centimetre|0|3 |
| kilogram per cubic decimetre|0|3 |
| kilogram per cubic metre|0|3 |
| kilogram per day|0|2 |
| kilogram per decimetre to the fourth power|0|2 |
| kilogram per hour|0|2 |
| kilogram per joule|0|2 |
| kilogram per kilogram|0|2 |
| kilogram per kilowatt hour|0|1 |
| kilogram per litre|0|2 |
| kilogram per megajoule|0|2 |
| kilogram per metre|0|2 |
| kilogram per metre second|0|3 |
| kilogram per metre to the fourth power|0|2 |
| kilogram per min|0|2 |
| kilogram per second|0|2 |
| kilogram per square metre|0|3 |
| kilogram per square metre second|0|3 |
| kilogram per tonne|0|1 |
| kilogram square metre|0|3 |
| kilogram-mole|0|3 |
| kilogram-mole per cubic metre|0|2 |
| kilogram-mole per hour|0|1 |
| kilogram-mole per second|0|1 |
| kilogray|0|1 |
| kilohenry|0|1 |
| kilohertz|0|2 |
| kilohm|0|3 |
| kilojoule|0|3 |
| kilojoule metre per hour square metre delta kelvin|0|3 |
| kilojoule per cubic decimetre|0|3 |
| kilojoule per cubic metre|0|3 |
| kilojoule per hour square metre delta kelvin|0|3 |
| kilojoule per kilogram|0|2 |
| kilojoule per kilogram delta kelvin|0|2 |
| kilojoule per kilogram-mole|0|1 |
| kilojoule per kilogram-mole delta kelvin|0|1 |
| kilolux|0|3 |
| kilometre|0|6 |
| kilometre per centimetre|0|2 |
| kilometre per cubic decimetre|0|3 |
| kilometre per hour|0|2 |
| kilometre per litre|0|2 |
| kilometre per second|0|2 |
| kilonewton|0|3 |
| kilonewton metre|0|2 |
| kilonewton per metre|0|2 |
| kilonewton per square metre|0|3 |
| kilonewton square metre|0|3 |
| kiloohm metre|0|2 |
| kilopascal|0|4 |
| kilopascal per hectometre|0|2 |
| kilopascal per hour|0|2 |
| kilopascal per metre|0|2 |
| kilopascal per min|0|2 |
| kilopascal second per metre|0|2 |
| kilopascal squared|0|3 |
| kilopascal squared per centipoise|0|3 |
| kilopascal squared per thousand centipoise|0|3 |
| kilopoise|0|1 |
| kiloradian|0|3 |
| kilosiemens|0|4 |
| kilosiemens per metre|0|1 |
| kilotesla|0|1 |
| kilovolt|0|4 |
| kilowatt|0|3 |
| kilowatt hour|0|2 |
| kilowatt hour per cubic decimetre|0|3 |
| kilowatt hour per cubic metre|0|3 |
| kilowatt hour per kilogram|0|2 |
| kilowatt hour per kilogram delta Celsius|0|2 |
| kilowatt per cubic metre|0|3 |
| kilowatt per square centimetre|0|3 |
| kilowatt per square metre|0|3 |
| kilowatt per square metre delta kelvin|0|3 |
| kiloweber|0|1 |
| knot|0|2 |
| link|0|2 |
| litre|0|4 |
| litre per cubic metre|0|3 |
| litre per gram-mole|0|1 |
| litre per hour|0|2 |
| litre per hundred kilogram|0|2 |
| litre per hundred kilometre|0|1 |
| litre per kilogram|0|2 |
| litre per kilogram-mole|0|1 |
| litre per metre|0|2 |
| litre per minute|0|2 |
| litre per second|0|2 |
| litre per tonne|0|2 |
| lumen|0|3 |
| lumen per square metre|0|3 |
| lumen per watt|0|2 |
| lumen second|0|4 |
| lux|0|3 |
| lux second|0|2 |
| mebibyte|0|2 |
| megaampere|0|3 |
| megabecquerel|0|3 |
| megacoulomb|0|1 |
| megaelectronvolt|0|2 |
| megaeuclid|0|1 |
| megafarad|0|1 |
| megagauss|0|1 |
| megagram|0|4 |
| megagram per cubic metre|0|1 |
| megagram per day|0|2 |
| megagram per hour|0|2 |
| megagram per inch|0|2 |
| megagram per square metre|0|3 |
| megagray|0|1 |
| megahenry|0|1 |
| megahertz|0|2 |
| megajoule|0|3 |
| megajoule per cubic metre|0|3 |
| megajoule per kilogram|0|2 |
| megajoule per kilogram-mole|0|1 |
| megajoule per metre|0|2 |
| megametre|0|2 |
| meganewton|0|3 |
| megapascal|0|4 |
| megapascal per hour|0|2 |
| megapascal per metre|0|2 |
| megapascal second per metre|0|2 |
| megapoise|0|1 |
| megaradian|0|3 |
| megasiemens|0|1 |
| megavolt|0|3 |
| megawatt|0|3 |
| megawatt hour|0|2 |
| megawatt hour per cubic metre|0|3 |
| megawatt hour per kilogram|0|2 |
| megaweber|0|1 |
| megohm|0|3 |
| metre|0|6 |
| metre per Pascal|0|1 |
| metre per centimetre|0|2 |
| metre per cubic metre|0|3 |
| metre per day|0|2 |
| metre per delta kelvin|0|2 |
| metre per hour|0|2 |
| metre per kilogram|0|1 |
| metre per kilometre|0|2 |
| metre per metre|0|2 |
| metre per metre delta kelvin|0|2 |
| metre per millisecond|0|2 |
| metre per minute|0|2 |
| metre per second|0|2 |
| metre per second squared|0|3 |
| metre per thirty metre|0|2 |
| metre to the fourth power|0|2 |
| metre to the fourth power per second|0|2 |
| microampere|0|4 |
| microampere per square centimetre|0|3 |
| microampere per square inch|0|3 |
| microcoulomb|0|4 |
| microeuclid|0|2 |
| microfarad|0|3 |
| microfarad per metre|0|2 |
| microgauss|0|1 |
| microgram|0|3 |
| microgram per cubic centimetre|0|3 |
| microgram-mole|0|3 |
| microgray|0|1 |
| microhenry|0|3 |
| microhenry per metre|0|2 |
| microhertz|0|1 |
| microhm per foot|0|2 |
| microhm per metre|0|2 |
| microjoule|0|3 |
| micrometre|0|4 |
| micrometre per second|0|2 |
| micronewton|0|3 |
| microohm|0|4 |
| micropascal|0|4 |
| micropoise|0|1 |
| microradian|0|4 |
| microsecond|0|4 |
| microsecond per foot|0|2 |
| microsecond per inch|0|1 |
| microsecond per metre|0|2 |
| microsiemens|0|3 |
| microtesla|0|3 |
| microvolt|0|4 |
| microvolt per foot|0|2 |
| microvolt per metre|0|2 |
| microwatt|0|3 |
| microwatt per cubic metre|0|3 |
| microweber|0|3 |
| mil|0|2 |
| mile|0|4 |
| mile per hour|0|2 |
| mile per inch|0|2 |
| milliampere|0|4 |
| milliampere per square centimetre|0|3 |
| milliampere per square foot|0|3 |
| millicoulomb|0|4 |
| millicoulomb per square metre|0|3 |
| millidarcy square foot per pound-force second|0|1 |
| millidarcy square inch per pound-force second|0|1 |
| millieuclid|0|2 |
| millifarad|0|1 |
| milligalileo|0|4 |
| milligauss|0|2 |
| milligram|0|4 |
| milligram per cubic decimetre|0|3 |
| milligram per cubic metre|0|3 |
| milligram per joule|0|2 |
| milligram per kilogram|0|2 |
| milligram per litre|0|2 |
| milligram-mole|0|3 |
| milligray|0|3 |
| millihenry|0|3 |
| millihertz|0|1 |
| millijoule|0|3 |
| millijoule per square centimetre|0|3 |
| millijoule per square metre|0|3 |
| millilitre|0|4 |
| millilitre per millilitre|0|2 |
| millimetre|0|4 |
| millimetre per millimetre delta kelvin|0|2 |
| millimetre per second|0|2 |
| millinewton|0|3 |
| millinewton per kilometre|0|2 |
| millinewton per metre|0|2 |
| millinewton square metre|0|3 |
| milliohm|0|4 |
| million calorie|0|1 |
| million cubic foot|0|4 |
| million cubic foot per acre foot|0|1 |
| million cubic foot per barrel|0|1 |
| million cubic foot per day|0|2 |
| million cubic metre|0|1 |
| million cubic metre per day|0|3 |
| million million calorie|0|1 |
| million million cubic foot|0|1 |
| million million million calorie|0|1 |
| million million million rad|0|1 |
| million million rad|0|1 |
| million psi|0|2 |
| million rad|0|1 |
| millionth of bar|0|3 |
| millionth of calorie|0|1 |
| millionth of calorie per second square centimetre|0|3 |
| millionth of curie|0|5 |
| millionth of psi|0|2 |
| millionth of rad|0|1 |
| millipascal|0|3 |
| millipascal second|0|2 |
| millipoise|0|1 |
| milliradian|0|3 |
| millisecond|0|4 |
| millisecond per centimetre|0|2 |
| millisecond per foot|0|2 |
| millisecond per inch|0|2 |
| millisecond per metre|0|2 |
| millisecond per second|0|2 |
| millisiemens|0|5 |
| millisiemens per centimetre|0|1 |
| millisiemens per metre|0|4 |
| millisievert|0|3 |
| millisievert per hour|0|2 |
| millitesla|0|4 |
| millivolt|0|4 |
| millivolt per foot|0|2 |
| millivolt per metre|0|2 |
| milliwatt|0|4 |
| milliwatt per square metre|0|3 |
| milliweber|0|3 |
| minute|0|4 |
| minute per foot|0|2 |
| minute per metre|0|2 |
| mole square metre per mole second|0|1 |
| nanoampere|0|3 |
| nanocalorie|0|1 |
| nanocoulomb|0|4 |
| nanocurie|0|5 |
| nanoeuclid|0|2 |
| nanofarad|0|1 |
| nanogauss|0|1 |
| nanogram|0|1 |
| nanogray|0|1 |
| nanohenry|0|3 |
| nanojoule|0|3 |
| nanometre|0|3 |
| nanometre per second|0|2 |
| nanonewton|0|1 |
| nanoohm|0|3 |
| nanoohm square mil per foot|0|1 |
| nanoohm square milimetre per metre|0|1 |
| nanopascal|0|1 |
| nanopoise|0|1 |
| nanorad|0|1 |
| nanosecond|0|4 |
| nanosecond per foot|0|1 |
| nanosecond per metre|0|2 |
| nanosiemens|0|2 |
| nanotesla|0|3 |
| nanovolt|0|1 |
| nanowatt|0|4 |
| nanoweber|0|1 |
| newton|0|4 |
| newton metre|0|3 |
| newton metre per metre|0|2 |
| newton per cubic metre|0|3 |
| newton per metre|0|2 |
| newton per square metre|0|3 |
| newton per square millimetre|0|3 |
| newton per thirty metre|0|2 |
| newton second per square metre|0|3 |
| newton square metre|0|3 |
| octave|0|2 |
| ohm|0|4 |
| ohm centimetre|0|2 |
| ohm metre|0|2 |
| ohm per metre|0|2 |
| ohm square metre per metre|0|1 |
| ounce-force|0|1 |
| ounce-mass|0|1 |
| part per million|0|5 |
| part per million [mass basis]|0|2 |
| part per million [volume basis]|0|2 |
| part per thousand|0|7 |
| pascal|0|4 |
| pascal per cubic metre|0|3 |
| pascal per hour|0|2 |
| pascal per metre|0|2 |
| pascal per second|0|2 |
| pascal second|0|3 |
| pascal second per cubic metre|0|3 |
| pascal second square metre per kilogram|0|1 |
| pascal second squared per cubic metre|0|3 |
| pascal squared|0|3 |
| pascal squared per pascal second|0|1 |
| per (kilogram per second)|0|1 |
| per Newton|0|2 |
| per angstrom|0|2 |
| per bar|0|1 |
| per centimetre|0|2 |
| per cubic foot|0|2 |
| per cubic metre|0|3 |
| per day|0|2 |
| per delta Celsius|0|2 |
| per delta Fahrenheit|0|2 |
| per delta Rankine|0|2 |
| per delta kelvin|0|2 |
| per foot|0|2 |
| per gram|0|2 |
| per henry|0|2 |
| per hour|0|2 |
| per inch|0|2 |
| per kilogram|0|2 |
| per kilopascal|0|2 |
| per litre|0|2 |
| per metre|0|2 |
| per microsecond|0|2 |
| per microvolt|0|2 |
| per mile|0|2 |
| per millimetre|0|2 |
| per millisecond|0|2 |
| per minute|0|2 |
| per nanometre|0|2 |
| per pascal|0|2 |
| per picopascal|0|2 |
| per second|0|4 |
| per square foot|0|3 |
| per square kilometre|0|3 |
| per square metre|0|3 |
| per square mile|0|1 |
| per volt|0|2 |
| per week|0|2 |
| per yard|0|2 |
| percent|0|3 |
| percent [mass basis]|0|2 |
| percent [volume basis]|0|4 |
| picoampere|0|3 |
| picocalorie|0|1 |
| picocoulomb|0|4 |
| picocurie|0|5 |
| picocurie per gram|0|2 |
| picoeuclid|0|1 |
| picofarad|0|3 |
| picogauss|0|1 |
| picogram|0|1 |
| picogray|0|1 |
| picojoule|0|1 |
| picometre|0|3 |
| piconewton|0|1 |
| picoohm|0|1 |
| picopascal|0|4 |
| picopoise|0|1 |
| picorad|0|1 |
| picosecond|0|3 |
| picosiemens|0|4 |
| picotesla|0|1 |
| picovolt|0|1 |
| picowatt|0|1 |
| picoweber|0|1 |
| poise|0|4 |
| pound-force|0|2 |
| pound-force per cubic foot|0|1 |
| pound-force per hundred square foot|0|1 |
| pound-force per pound-force|0|2 |
| pound-force per square foot|0|1 |
| pound-force per square inch|0|2 |
| pound-force second per square foot|0|1 |
| pound-force second per square inch|0|1 |
| pound-force square inch|0|1 |
| pound-mass|0|2 |
| pound-mass per cubic foot|0|1 |
| pound-mass per cubic inch|0|1 |
| pound-mass per hundred square foot|0|1 |
| pound-mass per square foot|0|1 |
| pound-mass per square foot hour|0|1 |
| pound-mass per square foot second|0|1 |
| pound-mass square foot|0|1 |
| pound-mass square foot per second squared|0|1 |
| pound-mass-mole per cubic foot|0|1 |
| pound-mass-mole per hour square foot|0|1 |
| pound-mass-mole per second square foot|0|1 |
| poundal square centimetre|0|1 |
| psi squared|0|1 |
| psi squared day per centipoise cubic foot|0|1 |
| psi squared per centipoise|0|1 |
| rad|0|2 |
| radian|0|5 |
| radian per cubic foot|0|3 |
| radian per cubic metre|0|3 |
| radian per foot|0|2 |
| radian per metre|0|2 |
| radian per second|0|2 |
| radian per second squared|0|3 |
| rem|0|2 |
| rem per hour|0|2 |
| revolution|0|3 |
| second|0|4 |
| second per centimetre|0|2 |
| second per cubic foot|0|3 |
| second per cubic metre|0|3 |
| second per foot|0|2 |
| second per inch|0|2 |
| second per kilogram|0|1 |
| second per litre|0|2 |
| second per metre|0|2 |
| siemens|0|6 |
| siemens per metre|0|4 |
| sievert|0|3 |
| sievert per hour|0|2 |
| sievert per second|0|2 |
| sixteenth of inch|0|4 |
| sixty-fourth of inch|0|4 |
| square US survey mile|0|1 |
| square centimetre|0|3 |
| square centimetre per gram|0|3 |
| square centimetre per second|0|3 |
| square foot|0|5 |
| square foot per cubic inch|0|1 |
| square foot per hour|0|3 |
| square foot per pound-mass|0|1 |
| square foot per second|0|3 |
| square inch|0|5 |
| square inch per second|0|3 |
| square inch per square foot|0|3 |
| square inch per square inch|0|3 |
| square kilometre|0|3 |
| square metre|0|3 |
| square metre per cubic centimetre|0|3 |
| square metre per cubic metre|0|3 |
| square metre per day|0|1 |
| square metre per gram|0|3 |
| square metre per gram-mole|0|3 |
| square metre per hour|0|3 |
| square metre per kilogram|0|3 |
| square metre per kilopascal day|0|3 |
| square metre per pascal second|0|3 |
| square metre per second|0|3 |
| square metre per square metre|0|3 |
| square micrometre|0|3 |
| square micrometre metre|0|3 |
| square mile|0|1 |
| square millimetre|0|3 |
| square millimetre per second|0|3 |
| square millimetre per square millimetre|0|3 |
| square yard|0|3 |
| standard atmosphere|0|4 |
| standard atmosphere per foot|0|2 |
| standard atmosphere per hour|0|2 |
| standard atmosphere per hundred metre|0|2 |
| standard atmosphere per metre|0|2 |
| steradian|0|4 |
| technical atmosphere|0|2 |
| ten foot|0|1 |
| ten inch|0|2 |
| ten kilonewton|0|1 |
| ten thousand kilogram per cubic metre|0|3 |
| tenth of US survey foot|0|1 |
| tenth of calorie|0|1 |
| tenth of foot|0|1 |
| tenth of inch|0|5 |
| tenth of rad|0|1 |
| tenth of yard|0|1 |
| teraampere|0|1 |
| terabecquerel|0|3 |
| teracoulomb|0|1 |
| teraelectronvolt|0|1 |
| teraeuclid|0|1 |
| terafarad|0|1 |
| teragauss|0|1 |
| teragram|0|1 |
| teragray|0|1 |
| terahenry|0|1 |
| terajoule|0|3 |
| terametre|0|1 |
| teranewton|0|1 |
| teraohm|0|3 |
| terapascal|0|1 |
| terapoise|0|1 |
| terasiemens|0|2 |
| teratesla|0|1 |
| teravolt|0|1 |
| terawatt|0|3 |
| teraweber|0|1 |
| terrawatt hour|0|2 |
| tesla|0|4 |
| tesla per metre|0|1 |
| thirty foot|0|1 |
| thirty metres|0|1 |
| thirty-second of inch|0|4 |
| thousand (cubic metre per day) metre|0|2 |
| thousand calorie|0|2 |
| thousand calorie metre per square centimetre|0|3 |
| thousand calorie per cubic centimetre|0|3 |
| thousand calorie per cubic metre|0|3 |
| thousand calorie per gram|0|2 |
| thousand calorie per gram-mole|0|1 |
| thousand calorie per hour square metre delta Celsius|0|1 |
| thousand calorie per kilogram|0|2 |
| thousand calorie per kilogram delta Celsius|0|2 |
| thousand cubic foot|0|5 |
| thousand cubic foot per barrel|0|1 |
| thousand cubic foot per day|0|1 |
| thousand cubic metre|0|1 |
| thousand cubic metre per cubic metre|0|1 |
| thousand cubic metre per day|0|3 |
| thousand cubic metre per hour|0|3 |
| thousand foot|0|1 |
| thousand foot per hour|0|2 |
| thousand foot per second|0|2 |
| thousand gram-force|0|4 |
| thousand gram-force metre|0|2 |
| thousand gram-force metre per metre|0|2 |
| thousand gram-force metre per square centimetre|0|3 |
| thousand gram-force per centimetre|0|2 |
| thousand gram-force per kilogram-force|0|2 |
| thousand gram-force per square centimetre|0|3 |
| thousand gram-force per square metre|0|1 |
| thousand gram-force per square millimetre|0|3 |
| thousand gram-force second per square metre|0|3 |
| thousand gram-force square metre|0|3 |
| thousand million calorie|0|1 |
| thousand million cubic foot|0|2 |
| thousand million rad|0|1 |
| thousand rad|0|1 |
| thousandth of bar|0|3 |
| thousandth of calorie|0|1 |
| thousandth of curie|0|3 |
| thousandth of gravity|0|2 |
| thousandth of irem per hour|0|2 |
| thousandth of rad|0|1 |
| thousandth of rem|0|2 |
| tonne|0|4 |
| tonne per cubic metre|0|1 |
| tonne per day|0|2 |
| tonne per hour|0|2 |
| tonne per minute|0|2 |
| volt|0|4 |
| volt per bel|0|2 |
| volt per decibel|0|2 |
| volt per metre|0|2 |
| watt|0|4 |
| watt per cubic metre|0|3 |
| watt per cubic metre delta kelvin|0|3 |
| watt per delta kelvin|0|2 |
| watt per kilowatt|0|2 |
| watt per metre delta kelvin|0|2 |
| watt per square centimetre|0|3 |
| watt per square metre|0|3 |
| watt per square metre delta kelvin|0|3 |
| watt per square metre steradian|0|3 |
| watt per square millimetre|0|3 |
| watt per steradian|0|2 |
| watt per watt|0|2 |
| watt square metre delta kelvin per joule delta kelvin|0|1 |
| weber|0|4 |
| weber metre|0|2 |
| weber per metre|0|2 |
| weber per millimetre|0|2 |
| week|0|2 |
| yard|0|3 |

[Back to TOC](#TOC)