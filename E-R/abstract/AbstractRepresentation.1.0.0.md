<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractRepresentation [Status: Accepted]
The group of properties shared by all different kinds of representations.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractRepresentation:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractRepresentation.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractRepresentation.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractRepresentation.1.0.0.json](../../Authoring/abstract/AbstractRepresentation.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractRepresentation.1.0.0.json](../../Generated/abstract/AbstractRepresentation.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractRepresentation.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractRepresentation.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractRepresentation


## Outgoing Relationships for AbstractRepresentation

![AbstractRepresentation](../_diagrams/abstract/AbstractRepresentation.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractRepresentation Referenced by other Entities

![AbstractRepresentation](../_diagrams/abstract/AbstractRepresentation.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractRepresentation Properties

## Table of AbstractRepresentation Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|InterpretationID &rarr; [EarthModelInterpretation](../work-product-component/EarthModelInterpretation.1.0.0.md) &rarr; [GeobodyBoundaryInterpretation](../work-product-component/GeobodyBoundaryInterpretation.1.0.0.md) &rarr; [GeobodyInterpretation](../work-product-component/GeobodyInterpretation.1.0.0.md) &rarr; [HorizonInterpretation](../work-product-component/HorizonInterpretation.1.0.0.md) &rarr; [RockFluidOrganizationInterpretation](../work-product-component/RockFluidOrganizationInterpretation.1.0.0.md) &rarr; [RockFluidUnitInterpretation](../work-product-component/RockFluidUnitInterpretation.1.0.0.md) &rarr; [StratigraphicUnitInterpretation](../work-product-component/StratigraphicUnitInterpretation.1.0.0.md) &rarr; [StructuralOrganizationInterpretation](../work-product-component/StructuralOrganizationInterpretation.1.0.0.md) &rarr; [FaultInterpretation](../work-product-component/FaultInterpretation.1.0.0.md) &rarr; [AquiferInterpretation](../work-product-component/AquiferInterpretation.1.0.0.md)| |string|optional|ID of the interpretation|Allow to link an interpretation with this representation|<code>^[\w\-\.]+:(work-product-component\-\-EarthModelInterpretation&#124;work-product-component\-\-GeobodyBoundaryInterpretation&#124;work-product-component\-\-GeobodyInterpretation&#124;work-product-component\-\-HorizonInterpretation&#124;work-product-component\-\-RockFluidOrganizationInterpretation&#124;work-product-component\-\-RockFluidUnitInterpretation&#124;work-product-component\-\-StratigraphicUnitInterpretation&#124;work-product-component\-\-StructuralOrganizationInterpretation&#124;work-product-component\-\-FaultInterpretation&#124;work-product-component\-\-AquiferInterpretation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EarthModelInterpretation](../work-product-component/EarthModelInterpretation.1.0.0.md) &rarr; [GeobodyBoundaryInterpretation](../work-product-component/GeobodyBoundaryInterpretation.1.0.0.md) &rarr; [GeobodyInterpretation](../work-product-component/GeobodyInterpretation.1.0.0.md) &rarr; [HorizonInterpretation](../work-product-component/HorizonInterpretation.1.0.0.md) &rarr; [RockFluidOrganizationInterpretation](../work-product-component/RockFluidOrganizationInterpretation.1.0.0.md) &rarr; [RockFluidUnitInterpretation](../work-product-component/RockFluidUnitInterpretation.1.0.0.md) &rarr; [StratigraphicUnitInterpretation](../work-product-component/StratigraphicUnitInterpretation.1.0.0.md) &rarr; [StructuralOrganizationInterpretation](../work-product-component/StructuralOrganizationInterpretation.1.0.0.md) &rarr; [FaultInterpretation](../work-product-component/FaultInterpretation.1.0.0.md) &rarr; [AquiferInterpretation](../work-product-component/AquiferInterpretation.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|InterpretationName| |string|optional|Name of the interpretation|Name of the interpretation the representation refers to|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "InterpretationID", "TargetPropertyName": "Name"}|(No natural key)|My Interpretation|
|_TimeSeries_| |_object_|_optional_|_Time Series_|_Allow to link the geometry of the representation to a particular index of a time series. This is particularly useful for IJK grids used in geomechanical or basin context where the topology and geometry varies against the time._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|TimeSeries.TimeIndex| |integer|required|TimeIndices|Index of the timestamp of the representation in the associated TimeSeries|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|3|
|TimeSeries.TimeSeriesID &rarr; [TimeSeries](../work-product-component/TimeSeries.1.0.0.md)| |string|required|TimeSeriesID|Time series the representation is associated to|<code>^[\w\-\.]+:work-product-component\-\-TimeSeries:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [TimeSeries](../work-product-component/TimeSeries.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:work-product-component--TimeSeries:85348741-3433-406B-9189-22B298C3E2D2:|
|RealizationIndex| |integer|optional|Realization Index|The index of the realization of this representation|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|10|
|_IndexableElementCount[]_| |_object_|_optional_|_IndexableElementCount_|_Several optional indexable element counts Fragment Description: Defines the count of a particular indexable element in a representation_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|IndexableElementCount[].Count| |integer|required|Count|The count of indexable element|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|10000|
|IndexableElementCount[].IndexableElementID &rarr; [IndexableElement](../reference-data/IndexableElement.1.0.0.md)| |string|optional|IndexableElementID|The indexable element which is counted|<code>^[\w\-\.]+:reference-data\-\-IndexableElement:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [IndexableElement](../reference-data/IndexableElement.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|LocalModelCompoundCrsID &rarr; [LocalModelCompoundCrs](../work-product-component/LocalModelCompoundCrs.1.0.0.md)| |string|optional|ID of the compound CRS where this representation is given|Allow to link a local CRS with this representation|<code>^[\w\-\.]+:work-product-component\-\-LocalModelCompoundCrs:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [LocalModelCompoundCrs](../work-product-component/LocalModelCompoundCrs.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractRepresentation.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractGridRepresentation.1.0.0](AbstractGridRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [GenericRepresentation.1.0.0](../work-product-component/GenericRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [GridConnectionSetRepresentation.1.0.0](../work-product-component/GridConnectionSetRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [IjkGridNumericalAquiferRepresentation.1.0.0](../work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [SealedSurfaceFramework.1.0.0](../work-product-component/SealedSurfaceFramework.1.0.0.md) | allOf | one |  |
| Included by | [SealedVolumeFramework.1.0.0](../work-product-component/SealedVolumeFramework.1.0.0.md) | allOf | one |  |
| Included by | [SeismicFault.1.0.0](../work-product-component/SeismicFault.1.0.0.md) | allOf | one |  |
| Included by | [SeismicHorizon.1.1.0](../work-product-component/SeismicHorizon.1.1.0.md) | allOf | one |  |
| Included by | [SubRepresentation.1.0.0](../work-product-component/SubRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [UnsealedSurfaceFramework.1.0.0](../work-product-component/UnsealedSurfaceFramework.1.0.0.md) | allOf | one |  |
| Included by | [UnsealedSurfaceFramework.1.1.0](../work-product-component/UnsealedSurfaceFramework.1.1.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._