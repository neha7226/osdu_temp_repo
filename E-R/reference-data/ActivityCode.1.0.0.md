<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# ActivityCode [Status: Accepted]
ActivityCode for Well Construction and Intervention (for L1), Construct well (for L2), Intervene well (for L2), …
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--ActivityCode:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: osdu
* Reference value type, governance model: FIXED
* Link to &rarr; Proposal workbook [schema-ActivityCode.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/reference-data/schema-ActivityCode.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema ActivityCode.1.0.0.json](../../Authoring/reference-data/ActivityCode.1.0.0.json)
* Link to &rarr; [Generated Schema ActivityCode.1.0.0.json](../../Generated/reference-data/ActivityCode.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource ActivityCode.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/ActivityCode.1.0.0.json)
* Link to &rarr; [Example Record ActivityCode.1.0.0.json](../../Examples/reference-data/ActivityCode.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams ActivityCode


## Outgoing Relationships for ActivityCode

![ActivityCode](../_diagrams/reference-data/ActivityCode.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## ActivityCode Referenced by other Entities

![ActivityCode](../_diagrams/reference-data/ActivityCode.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# ActivityCode Properties

## 1. Table of ActivityCode System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-ActivityCode:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--ActivityCode:e0abcb92-e586-5815-9981-8be9c51d5bc1|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--ActivityCode:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of ActivityCode Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of ActivityCode Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of ActivityCode Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.ActivityLevelID &rarr; [ActivityLevel](ActivityLevel.1.0.0.md)| |string|optional|Activity Level ID|The relationship to the ActivityLevel record describing the ActivityCode's position in the hierarchy.|<code>^[\w\-\.]+:reference-data\-\-ActivityLevel:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ActivityLevel](ActivityLevel.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--ActivityLevel:L4:|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of ActivityCode Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for ActivityCode.1.0.0

* Status: Published
* Governance: FIXED
* The table below **is truncated** and shows only 100 of total 7394 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [ActivityCode](../../ReferenceValues/Manifests/reference-data/FIXED/ActivityCode.1.0.0.json).

|`id` with prefix `namespace:reference-data--ActivityCode:`|Code|Name|Alias|Description|ActivityLevelID|AttributionAuthority|Source|CommitDate|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|adfl_cnds_cw_wcic|adfl_cnds_cw_wcic|Analyse and design field location|-|Analyse and design field location; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cas_cs_cw_wcic|cas_cs_cw_wcic|Case section|-|Case section; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cas_cs_iw_wcic|cas_cs_iw_wcic|Case section|-|Case section; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ccr_ds_cs_cw_wcic|ccr_ds_cs_cw_wcic|Cut casing run|-|Cut casing run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ces_cs_cw_wcic|ces_cs_cw_wcic|Cement section|-|Cement section; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ces_cs_iw_wcic|ces_cs_iw_wcic|Cement section|-|Cement section; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|chwcr_ds_cs_cw_wcic|chwcr_ds_cs_cw_wcic|Cased hole window cutting run|-|Cased hole window cutting run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|chwr_ds_cs_cw_wcic|chwr_ds_cs_cw_wcic|Cased hole whipstock run|-|Cased hole whipstock run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cnds_cw_wcic|cnds_cw_wcic|Construct new drill site|-|Construct new drill site; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cnl_ild_cnds_cw_wcic|cnl_ild_cnds_cw_wcic|Construct new location|-|Construct new location; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cor_cas_cs_cw_wcic|cor_cas_cs_cw_wcic|Clean out run|-|Clean out run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cor_ds_cs_cw_wcic|cor_ds_cs_cw_wcic|Clean out run|-|Clean out run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cpcr_ds_cs_cw_wcic|cpcr_ds_cs_cw_wcic|Cut and pull casing run|-|Cut and pull casing run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cr_ds_cs_cw_wcic|cr_ds_cs_cw_wcic|Coring run|-|Coring run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cs_cw_wcic|cs_cw_wcic|Construct section|-|Construct section; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cs_iw_wcic|cs_iw_wcic|Construct section|-|Construct section; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cw_cw_wcic|cw_cw_wcic|Complete well|-|Complete well; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cw_iw_wcic|cw_iw_wcic|Complete well|-|Complete well; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|cw_wcic|cw_wcic|Construct well|-|Construct well; level: L2|ActivityLevel:L2:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|d_cw_wcic|d_cw_wcic|Demobilization|-|Demobilization; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|d_iw_wcic|d_iw_wcic|Demobilization|-|Demobilization; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|dr_ds_cs_cw_wcic|dr_ds_cs_cw_wcic|Drilling run|-|Drilling run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|driftr_cas_cs_cw_wcic|driftr_cas_cs_cw_wcic|Drift run|-|Drift run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|driftr_ds_cs_cw_wcic|driftr_ds_cs_cw_wcic|Drift run|-|Drift run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ds_cs_cw_wcic|ds_cs_cw_wcic|Drill section|-|Drill section; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ds_cs_iw_wcic|ds_cs_iw_wcic|Drill section|-|Drill section; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|dstr_ds_cs_cw_wcic|dstr_ds_cs_cw_wcic|Drill stem test run|-|Drill stem test run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|fr_ds_cs_cw_wcic|fr_ds_cs_cw_wcic|Fishing run|-|Fishing run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|hor_cas_cs_cw_wcic|hor_cas_cs_cw_wcic|Hole opening run|-|Hole opening run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|hor_ds_cs_cw_wcic|hor_ds_cs_cw_wcic|Hole opening run|-|Hole opening run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ilcioh_cw_cw_wcic|ilcioh_cw_cw_wcic|Install lower completion in open hole|-|Install lower completion in open hole; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ilcipc_cw_cw_wcic|ilcipc_cw_cw_wcic|Install lower completion inside production casing|-|Install lower completion inside production casing; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ilcipc_cw_iw_wcic|ilcipc_cw_iw_wcic|Install lower completion inside production casing|-|Install lower completion inside production casing; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ilcipc_ww_iw_wcic|ilcipc_ww_iw_wcic|Install lower completion inside production casing|-|Install lower completion inside production casing; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ilcwpc_cw_cw_wcic|ilcwpc_cw_cw_wcic|Install lower completion with production casing|-|Install lower completion with production casing; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ilcwpc_cw_iw_wcic|ilcwpc_cw_iw_wcic|Install lower completion with production casing|-|Install lower completion with production casing; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ild_cnds_cw_wcic|ild_cnds_cw_wcic|Implement location design|-|Implement location design; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|irm_cw_wcic|irm_cw_wcic|Interwell rig move|-|Interwell rig move; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|irm_iw_wcic|irm_iw_wcic|Interwell rig move|-|Interwell rig move; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|istp_cw_cw_wcic|istp_cw_cw_wcic|Install single trip completion|-|Install single trip completion; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|istp_cw_iw_wcic|istp_cw_iw_wcic|Install single trip completion|-|Install single trip completion; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|istp_ww_iw_wcic|istp_ww_iw_wcic|Install single trip completion|-|Install single trip completion; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|iuc_cw_cw_wcic|iuc_cw_cw_wcic|Install upper completion|-|Install upper completion; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|iuc_cw_iw_wcic|iuc_cw_iw_wcic|Install upper completion|-|Install upper completion; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|iuc_ww_iw_wcic|iuc_ww_iw_wcic|Install upper completion|-|Install upper completion; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|iw_wcic|iw_wcic|Intervene well|-|Intervene well; level: L2|ActivityLevel:L2:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|lnl_adfl_cnds_cw_wcic|lnl_adfl_cnds_cw_wcic|Layout new location|-|Layout new location; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|m_cw_wcic|m_cw_wcic|Mobilization|-|Mobilization; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|m_iw_wcic|m_iw_wcic|Mobilization|-|Mobilization; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|mr_ds_cs_cw_wcic|mr_ds_cs_cw_wcic|Milling run|-|Milling run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|mrnl_rmnl_irm_cw_wcic|mrnl_rmnl_irm_cw_wcic|Move rig to new location|-|Move rig to new location; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|mrnl_rmsnl_m_cw_wcic|mrnl_rmsnl_m_cw_wcic|Move rig to new location|-|Move rig to new location; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ohsr_ds_cs_cw_wcic|ohsr_ds_cs_cw_wcic|Open hole sidetrack run|-|Open hole sidetrack run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|pcr_ds_cs_cw_wcic|pcr_ds_cs_cw_wcic|Pull casing run|-|Pull casing run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|pdtltr_cas_cs_cw_wcic|pdtltr_cas_cs_cw_wcic|Polish dress test liner top run|-|Polish dress test liner top run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|pdtltr_ds_cs_cw_wcic|pdtltr_ds_cs_cw_wcic|Polish dress test liner top run|-|Polish dress test liner top run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|pnl_pnlprm_m_cw_wcic|pnl_pnlprm_m_cw_wcic|Prepare new location|-|Prepare new location; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|pnlprm_m_cw_wcic|pnlprm_m_cw_wcic|Prepare new location prior to rig move|-|Prepare new location prior to rig move; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|pnlprm_m_iw_wcic|pnlprm_m_iw_wcic|Prepare new location prior to rig move|-|Prepare new location prior to rig move; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|pslprm_d_cw_wcic|pslprm_d_cw_wcic|Prepare storage location prior to rig move|-|Prepare storage location prior to rig move; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|pslprm_d_iw_wcic|pslprm_d_iw_wcic|Prepare storage location prior to rig move|-|Prepare storage location prior to rig move; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|pww_ww_iw_wcic|pww_ww_iw_wcic|Prepare well for workover|-|Prepare well for workover; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rc_cas_cs_cw_wcic|rc_cas_cs_cw_wcic|Run casing|-|Run casing; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rchwr_cas_cs_cw_wcic|rchwr_cas_cs_cw_wcic|Retrieve cased hole whipstock run|-|Retrieve cased hole whipstock run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rchwr_ds_cs_cw_wcic|rchwr_ds_cs_cw_wcic|Retrieve cased hole whipstock run|-|Retrieve cased hole whipstock run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rl_cas_cs_cw_wcic|rl_cas_cs_cw_wcic|Run liner|-|Run liner; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rlc_ww_iw_wcic|rlc_ww_iw_wcic|Remove lower completion|-|Remove lower completion; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rmcl_irm_cw_wcic|rmcl_irm_cw_wcic|Rig move within current location|-|Rig move within current location; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rmcl_irm_iw_wcic|rmcl_irm_iw_wcic|Rig move within current location|-|Rig move within current location; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rmnl_irm_cw_wcic|rmnl_irm_cw_wcic|Rig move to new location|-|Rig move to new location; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rmnl_irm_iw_wcic|rmnl_irm_iw_wcic|Rig move to new location|-|Rig move to new location; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rmpr_ds_cs_cw_wcic|rmpr_ds_cs_cw_wcic|Recover mechanical plug run|-|Recover mechanical plug run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rms_d_cw_wcic|rms_d_cw_wcic|Rig move to storage|-|Rig move to storage; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rms_d_iw_wcic|rms_d_iw_wcic|Rig move to storage|-|Rig move to storage; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rmsnl_m_cw_wcic|rmsnl_m_cw_wcic|Rig move from storage to new location|-|Rig move from storage to new location; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rmsnl_m_iw_wcic|rmsnl_m_iw_wcic|Rig move from storage to new location|-|Rig move from storage to new location; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rohwr_ds_cs_cw_wcic|rohwr_ds_cs_cw_wcic|Retrieve open hole whipstock run|-|Retrieve open hole whipstock run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|root|root|Well Construct &amp; Intervene|-|Well Construct &amp; Intervene; level: L1|ActivityLevel:L1:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rt_cas_cs_cw_wcic|rt_cas_cs_cw_wcic|Run tubing|-|Run tubing; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ruc_ww_iw_wcic|ruc_ww_iw_wcic|Remove upper completion|-|Remove upper completion; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rusfbs_cw_cw_wcic|rusfbs_cw_cw_wcic|Rig up surface flow back system|-|Rig up surface flow back system; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rusfbs_cw_iw_wcic|rusfbs_cw_iw_wcic|Rig up surface flow back system|-|Rig up surface flow back system; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rusfbs_ww_iw_wcic|rusfbs_ww_iw_wcic|Rig up surface flow back system|-|Rig up surface flow back system; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|rwo_ww_iw_wcic|rwo_ww_iw_wcic|Remedial well operations|-|Remedial well operations; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|scpr_ds_cs_cw_wcic|scpr_ds_cs_cw_wcic|Set cement plug run|-|Set cement plug run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|smpr_ds_cs_cw_wcic|smpr_ds_cs_cw_wcic|Set mechanical plug run|-|Set mechanical plug run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|sohwr_ds_cs_cw_wcic|sohwr_ds_cs_cw_wcic|Set open hole whipstock run|-|Set open hole whipstock run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|sr_ds_cs_cw_wcic|sr_ds_cs_cw_wcic|Slickline run|-|Slickline run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|sw_cs_cw_wcic|sw_cs_cw_wcic|Secure well|-|Secure well; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|sw_cs_iw_wcic|sw_cs_iw_wcic|Secure well|-|Secure well; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|sw_cw_cw_wcic|sw_cw_cw_wcic|Secure well|-|Secure well; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|sw_cw_iw_wcic|sw_cw_iw_wcic|Secure well|-|Secure well; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|sw_ww_iw_wcic|sw_ww_iw_wcic|Secure well|-|Secure well; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|swr_rmcl_irm_cw_wcic|swr_rmcl_irm_cw_wcic|Skid or walk rig|-|Skid or walk rig; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|tlr_ds_cs_cw_wcic|tlr_ds_cs_cw_wcic|ThruBit logging run|-|ThruBit logging run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|wco_cw_cw_wcic|wco_cw_cw_wcic|Wellbore clean out|-|Wellbore clean out; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|wco_cw_iw_wcic|wco_cw_iw_wcic|Wellbore clean out|-|Wellbore clean out; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|wco_ww_iw_wcic|wco_ww_iw_wcic|Wellbore clean out|-|Wellbore clean out; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|wlc_ww_iw_wcic|wlc_ww_iw_wcic|Workover lower completion|-|Workover lower completion; level: L4|ActivityLevel:L4:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|wr_ds_cs_cw_wcic|wr_ds_cs_cw_wcic|Wireline run|-|Wireline run; level: L5|ActivityLevel:L5:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ww_iw_wcic|ww_iw_wcic|Workover well|-|Workover well; level: L3|ActivityLevel:L3:|SLB|Workbook Published/ActivityCode.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|

**_This reference value type contains more than 100 entries and is therefore truncated. See the reference-values repository for the complete list._**


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# ActivityCode.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ActivityPlan.1.0.0](../master-data/ActivityPlan.1.0.0.md) | WellPlanningActivities[].ActivityCodeID | many (as array) |  |
| Referred to by | [ActivityPlan.1.1.0](../master-data/ActivityPlan.1.1.0.md) | WellPlanningActivities[].ActivityCodeID | many (as array) |  |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [OperationsReport.1.0.0](../master-data/OperationsReport.1.0.0.md) | DrillActivity[].ActivityCodeID | many (as array) |  |
| Referred to by | [OperationsReport.1.1.0](../master-data/OperationsReport.1.1.0.md) | DrillActivity[].ActivityCodeID | many (as array) |  |
| Referred to by | [OperationsReport.1.1.0](../master-data/OperationsReport.1.1.0.md) | OperationsActivity[].ActivityCodeID | many (as array) |  |
| Referred to by | [OperationsReport.1.2.0](../master-data/OperationsReport.1.2.0.md) | DrillActivity[].ActivityCodeID | many (as array) |  |
| Referred to by | [OperationsReport.1.2.0](../master-data/OperationsReport.1.2.0.md) | OperationsActivity[].ActivityCodeID | many (as array) |  |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._