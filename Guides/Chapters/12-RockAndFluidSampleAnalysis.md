<a name="TOC"></a> Previous Chapter: [11 Well Planning and Execution](11-WellPlanningExecution.md)

[[_TOC_]]

# 12 Rock and Fluid Sample Analysis

## 12.1 Rock Samples

There are number of ways to obtain actual rock samples:

1. Coring operations during drilling.
2. Side-wall coring of an open wellbore by wireline after drilling.
3. Cuttings collected during drilling by screening the mud at the surface.
4. Outcrop samples, rock samples representing an analog of subsurface rocks of interest.

### 12.1.1 Coring, Rock Samples and Analyses

The first cases, 1., 2., 3., are covered by a [master-data--RockSample](../../E-R/master-data/RockSample.1.0.0.md), which
relates to a [master-data--Coring](../../E-R/master-data/Coring.1.0.0.md). Coring  is a specialized project activity
(i.e., it includes the [AbstractProjectActivity](../../E-R/abstract/AbstractProjectActivity.1.0.0.md)). In case 4. the
RockSample is not related to a Coring, neither to a Wellbore, but carries its own location in `data.SpatialLocation`

The [work-product-component--RockSampleAnalysis](../../E-R/work-product-component/RockSampleAnalysis.1.1.0.md) and  [work-product-component--RockImage](../../E-R/work-product-component/RockImage.1.0.0.md) are
specialized activities including [AbstractWPCActivity](../../E-R/abstract/AbstractWPCActivity.1.0.0.md).
RockSampleAnalysis and RockImage holds analysis results relevant for data discovery. Detailed content is — as usual for
work-product-component — managed via the `data.Datasets[]` relationship to dataset records. Spreadsheets, CSV or PDF
documents can hold the original analysis results, and JPEG and TIFF image files hold the images.

The conceptual model is shown below

![RockSampleAnalysisConceptualModel.png](Illustrations/12/RockSampleAnalysisConceptualModel.png)

The diagram is simplified. It does not show the relationships to
* Field
* GeoPoliticalEntity (Country, County, Block,...)
* Basin
* Play
* Prospect

Each of the master-data and work-product-component entities include those relationships by default via the
data.GeoContexts[] array.

Also not shown are relations to sample storage locations. All these details are available via the individual entity
documentations.

Many of the properties are reference-list controlled, the diagram simply lists them next to the entity referring to it.

### 12.1.2 RockSampleAnalysis

[work-product-component--RockSampleAnalysis](../../E-R/work-product-component/RockSampleAnalysis.1.1.0.md) can represent
multiple types of analyses. Each analysis is represented with its own structure under a specific property name. If a
laboratory is conduction multiple types of analyses on the same sample, it is possible to record this in a single
RockSampleAnalysis record. The analysis type coverage is searchable via `data.AnalysisTypeIDs[]`. The coverage of
analysis types continues to grow.

### 12.1.3 RockImage

[work-product-component--RockImage](../../E-R/work-product-component/RockImage.1.0.0.md) can represent
multiple types of rock images. Each image can be associated to a rock sample (preferred), wellbore or coring event. Associated datasets include [JPEG](../../E-R/dataset/File.Image.JPEG.1.0.0.md) and [TIFF](../../E-R/dataset/File.Image.TIFF.1.0.0.md) and the coverage of
image types and associated datasets continues to grow.


### 12.1.4 Worked Examples

The power and flexibility of the model is best explained in action. The
[WorkedExamples/RockSampleAnalysis](../../Examples/WorkedExamples/RockSampleAnalysis/README.md) and [WorkedExamples/RockImage](../../Examples/WorkedExamples/RockImage/README.md) uses public data to
create example records.

---

## Links

1. Return to the [Beginning of the Chapter](#TOC)
2. Next Chapter: [13 Earth Modelling (Static Subsurface Model Data, Geomodelling)](13-EarthModelling.md)
3. Browse to the [Schema Usage Guide Table of Contents](../README.md)

---

