<a name="TOC"></a> Previous Chapter: [10 Well Domain](10-WellBasics.md)

[[_TOC_]]

# 11 Well Planning and Execution

This is a placeholder for a more detailed introduction into the Well Planning and Execution related schemas. For the
time being, please refer to the following worked examples:

1. [Well Planning](../../Examples/WorkedExamples/WellPlanning/README.md)
2. [Well Execution](../../Examples/WorkedExamples/WellExecution/README.md)

---

## Links

1. Return to the [Beginning of the Chapter](#TOC)
2. Next Chapter: [12 Rock and Fluid Sample Analysis](12-RockAndFluidSampleAnalysis.md)
3. Browse to the [Schema Usage Guide Table of Contents](../README.md)

---

