[[_TOC_]]

# Change Report


|Reference Information|Community Repository|Branch|
|----|----|----|
|Repository|url: [https://community.opengroup.org/osdu/data/data-definitions.git](https://community.opengroup.org/osdu/data/data-definitions.git)|Data Definitions : master</br>url: [https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema.git](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema.git)|
|Last updated|2022-01-14 10:33:27+00:00|2022-03-18 15:56:03+00:00|
|Last changed by|Thomas Gehrmann [SLB]|Tartan Collier|
|Commit SHA|f6208166f3ddb21b473f722dce3d27283946fee1|6d963eb0c9b4fb10304a18db99ebf0336f4799a5|



# `abstract` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:AbstractAccessControlList:1.0.0`](../abstract/AbstractAccessControlList.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractActivityParameter:1.0.0`](../abstract/AbstractActivityParameter.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractAliasNames:1.0.0`](../abstract/AbstractAliasNames.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractAnyCrsFeatureCollection:1.0.0`](../abstract/AbstractAnyCrsFeatureCollection.1.0.0.md)|Changes: Differences: Different values for CoordinateReferenceSystemID.example; <br />Different values for VerticalCoordinateReferenceSystemID.example; <br />Different values for persistableReferenceCrs.example; <br />Different values for persistableReferenceVerticalCrs.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractBinGrid:1.0.0`](../abstract/AbstractBinGrid.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractBusinessRule:1.0.0`](../abstract/AbstractBusinessRule.1.0.0.md)|Changes: Differences: DataRuleSets.title added to baseline schema; <br />items.title added to baseline schema; <br />items.description added to baseline schema; <br />DataRuleSetID.title added to baseline schema; <br />DataRuleSetRunStatus.title added to baseline schema; <br />DataRules.title added to baseline schema; <br />items.title added to baseline schema; <br />items.description added to baseline schema; <br />DataRuleID.title added to baseline schema; <br />DataRuleRunStatus.title added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractColumnBasedTable:1.0.0`](../abstract/AbstractColumnBasedTable.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:AbstractColumnLayerGridPatch:1.0.0`](../abstract/AbstractColumnLayerGridPatch.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractCommonResources:1.0.0`](../abstract/AbstractCommonResources.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractCompressionInfo:1.0.0`](../abstract/AbstractCompressionInfo.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractContact:1.0.0`](../abstract/AbstractContact.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractCoordinates:1.0.0`](../abstract/AbstractCoordinates.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractDataset:1.0.0`](../abstract/AbstractDataset.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacet:1.0.0`](../abstract/AbstractFacet.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacility:1.0.0`](../abstract/AbstractFacility.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilityEvent:1.0.0`](../abstract/AbstractFacilityEvent.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilityOperator:1.0.0`](../abstract/AbstractFacilityOperator.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilitySpecification:1.0.0`](../abstract/AbstractFacilitySpecification.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilityState:1.0.0`](../abstract/AbstractFacilityState.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFacilityVerticalMeasurement:1.0.0`](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFeatureCollection:1.0.0`](../abstract/AbstractFeatureCollection.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFile:1.0.0`](../abstract/AbstractFile.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFileCollection:1.0.0`](../abstract/AbstractFileCollection.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractFileSourceInfo:1.0.0`](../abstract/AbstractFileSourceInfo.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoBasinContext:1.0.0`](../abstract/AbstractGeoBasinContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoContext:1.0.0`](../abstract/AbstractGeoContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoFieldContext:1.0.0`](../abstract/AbstractGeoFieldContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoPlayContext:1.0.0`](../abstract/AbstractGeoPlayContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoPoliticalContext:1.0.0`](../abstract/AbstractGeoPoliticalContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeoProspectContext:1.0.0`](../abstract/AbstractGeoProspectContext.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGeologicUnitInterpretation:1.0.0`](../abstract/AbstractGeologicUnitInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractGridRepresentation:1.0.0`](../abstract/AbstractGridRepresentation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractIjkGridFlowSimulationBoundaryConnection:1.0.0`](../abstract/AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:AbstractIjkGridPatch:1.0.0`](../abstract/AbstractIjkGridPatch.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractInterpretation:1.0.0`](../abstract/AbstractInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractLegalParentList:1.0.0`](../abstract/AbstractLegalParentList.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractLegalTags:1.0.0`](../abstract/AbstractLegalTags.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractMaster:1.0.0`](../abstract/AbstractMaster.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractMetaItem:1.0.0`](../abstract/AbstractMetaItem.1.0.0.md)|Changes: Differences: Different values for name.example; <br />Different values for persistableReference.example; <br />Different values for coordinateReferenceSystemID.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractPersistableReference:1.0.0`](../abstract/AbstractPersistableReference.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractProject:1.0.0`](../abstract/AbstractProject.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractProjectActivity:1.0.0`](../abstract/AbstractProjectActivity.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractPropertyType:1.0.0`](../abstract/AbstractPropertyType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractQualityMetric:1.0.0`](../abstract/AbstractQualityMetric.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractReferenceLevel:1.0.0`](../abstract/AbstractReferenceLevel.1.0.0.md)|Changes: Differences: Different values for VerticalCoordinateReferenceSystemID.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractReferencePropertyType:1.0.0`](../abstract/AbstractReferencePropertyType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractReferencePropertyType:1.1.0`](../abstract/AbstractReferencePropertyType.1.1.0.md)|No Changes; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractReferenceType:1.0.0`](../abstract/AbstractReferenceType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractRepresentation:1.0.0`](../abstract/AbstractRepresentation.1.0.0.md)|Changes: Differences: Different values for InterpretationID.pattern; <br />Different array length; current schema 10, baseline 9|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractSpatialLocation:1.0.0`](../abstract/AbstractSpatialLocation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractSpatialReference:1.0.0`](../abstract/AbstractSpatialReference.1.0.0.md)|Changes: Differences: Different values for CoordinateReferenceSystemID.example; <br />Different values for VerticalCoordinateReferenceSystemID.example; <br />Different values for PersistableReferenceCRS.example; <br />Different values for PersistableReferenceVerticalCRS.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractUnstructuredColumnLayerGridPatch:1.0.0`](../abstract/AbstractUnstructuredColumnLayerGridPatch.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractUnstructuredGridPatch:1.0.0`](../abstract/AbstractUnstructuredGridPatch.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractVectorHeaderMapping:1.0.0`](../abstract/AbstractVectorHeaderMapping.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractWPCActivity:1.0.0`](../abstract/AbstractWPCActivity.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractWPCGroupType:1.0.0`](../abstract/AbstractWPCGroupType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractWellboreDrillingReason:1.0.0`](../abstract/AbstractWellboreDrillingReason.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractWorkProductComponent:1.0.0`](../abstract/AbstractWorkProductComponent.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|

# `data-collection` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:DataCollection:1.0.0`](../data-collection/DataCollection.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|

# `dataset` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:dataset--File.CompressedVectorHeaders:1.0.0`](../dataset/File.CompressedVectorHeaders.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.Generic:1.0.0`](../dataset/File.Generic.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.GeoJSON:1.0.0`](../dataset/File.GeoJSON.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.Image.JPEG:1.0.0`](../dataset/File.Image.JPEG.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.Image.PNG:1.0.0`](../dataset/File.Image.PNG.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.Image.TIFF:1.0.0`](../dataset/File.Image.TIFF.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.Image.WorldFile:1.0.0`](../dataset/File.Image.WorldFile.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.OGC.GeoTIFF:1.0.0`](../dataset/File.OGC.GeoTIFF.1.0.0.md)|Changes: Differences: Different values for CoordinateReferenceSystemID.example; <br />Different values for VerticalCoordinateReferenceSystemID.example; <br />Different values for PersistableReferenceCrs.example; <br />Different values for PersistableReferenceVerticalCrs.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.WITSML:1.0.0`](../dataset/File.WITSML.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--FileCollection.Bluware.OpenVDS:1.0.0`](../dataset/FileCollection.Bluware.OpenVDS.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--FileCollection.Esri.Shape:1.0.0`](../dataset/FileCollection.Esri.Shape.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--FileCollection.Generic:1.0.0`](../dataset/FileCollection.Generic.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--FileCollection.SEGY:1.0.0`](../dataset/FileCollection.SEGY.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--FileCollection.Slb.OpenZGY:1.0.0`](../dataset/FileCollection.Slb.OpenZGY.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|

# `manifest` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:dataset--GenericDataset:1.0.0`](../manifest/GenericDataset.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--GenericMasterData:1.0.0`](../manifest/GenericMasterData.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--GenericReferenceData:1.0.0`](../manifest/GenericReferenceData.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product--GenericWorkProduct:1.0.0`](../manifest/GenericWorkProduct.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GenericWorkProductComponent:1.0.0`](../manifest/GenericWorkProductComponent.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:Manifest:1.0.0`](../manifest/Manifest.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|

# `master-data` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:master-data--ActivityPlan:1.0.0`](../master-data/ActivityPlan.1.0.0.md)|Changes: Differences: Different array length; current schema 2, baseline 1; <br />Different values for priority.path|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--ActivityTemplate:1.0.0`](../master-data/ActivityTemplate.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--ActivityTemplateArc:1.0.0`](../master-data/ActivityTemplateArc.1.0.0.md)|Changes: Differences: Different values for description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Agreement:1.0.0`](../master-data/Agreement.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--BHARun:1.0.0`](../master-data/BHARun.1.0.0.md)|Changes:  in `x-osdu` extensions tags|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Basin:1.0.0`](../master-data/Basin.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--BoundaryFeature:1.0.0`](../master-data/BoundaryFeature.1.0.0.md)|Changes: Differences: Name added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--CasingDesign:1.0.0`](../master-data/CasingDesign.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--EvaluationPlan:1.0.0`](../master-data/EvaluationPlan.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Field:1.0.0`](../master-data/Field.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--FluidsProgram:1.0.0`](../master-data/FluidsProgram.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--FluidsReport:1.0.0`](../master-data/FluidsReport.1.0.0.md)|Changes: Differences: Different values for items.description; <br />Name added to baseline schema; <br />Different values for x-osdu-review-status|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--FluidsReport:1.1.0`](../master-data/FluidsReport.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:master-data--GeoPoliticalEntity:1.0.0`](../master-data/GeoPoliticalEntity.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--GeometricTargetSet:1.0.0`](../master-data/GeometricTargetSet.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--HoleSection:1.0.0`](../master-data/HoleSection.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--ModelFeature:1.0.0`](../master-data/ModelFeature.1.0.0.md)|Changes: Differences: Name added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--OperationsReport:1.0.0`](../master-data/OperationsReport.1.0.0.md)|Changes: Differences: Name added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Organisation:1.0.0`](../master-data/Organisation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Organisation:1.1.0`](../master-data/Organisation.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:master-data--PlannedCementJob:1.0.0`](../master-data/PlannedCementJob.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Play:1.0.0`](../master-data/Play.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Prospect:1.0.0`](../master-data/Prospect.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--ReferenceLevel:1.0.0`](../master-data/ReferenceLevel.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Rig:1.0.0`](../master-data/Rig.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--RigUtilization:1.0.0`](../master-data/RigUtilization.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Risk:1.0.0`](../master-data/Risk.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--RockVolumeFeature:1.0.0`](../master-data/RockVolumeFeature.1.0.0.md)|Changes: Differences: Name added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0`](../master-data/Seismic2DInterpretationSet.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0`](../master-data/Seismic2DInterpretationSet.1.1.0.md)|Changes: Differences: Different values for priority.path; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0`](../master-data/Seismic3DInterpretationSet.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0`](../master-data/Seismic3DInterpretationSet.1.1.0.md)|Changes: Differences: Different values for priority.path; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0`](../master-data/SeismicAcquisitionSurvey.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0`](../master-data/SeismicAcquisitionSurvey.1.1.0.md)|Changes: Differences: Different values for priority.path; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--SeismicProcessingProject:1.0.0`](../master-data/SeismicProcessingProject.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--SeismicProcessingProject:1.1.0`](../master-data/SeismicProcessingProject.1.1.0.md)|Changes: Differences: Different values for priority.path; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--SurveyProgram:1.0.0`](../master-data/SurveyProgram.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Well:1.0.0`](../master-data/Well.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--WellActivityProgram:1.0.0`](../master-data/WellActivityProgram.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--WellBarrierElementTest:1.0.0`](../master-data/WellBarrierElementTest.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--WellPlanningWell:1.0.0`](../master-data/WellPlanningWell.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--WellPlanningWellbore:1.0.0`](../master-data/WellPlanningWellbore.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Wellbore:1.0.0`](../master-data/Wellbore.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--WellboreArchitecture:1.0.0`](../master-data/WellboreArchitecture.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|

# `reference-data` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:reference-data--ActivityCode:1.0.0`](../reference-data/ActivityCode.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ActivityLevel:1.0.0`](../reference-data/ActivityLevel.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ActivityOutcome:1.0.0`](../reference-data/ActivityOutcome.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ActivityOutcomeDetail:1.0.0`](../reference-data/ActivityOutcomeDetail.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ActivityType:1.0.0`](../reference-data/ActivityType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ActualIndicatorType:1.0.0`](../reference-data/ActualIndicatorType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AdditiveType:1.0.0`](../reference-data/AdditiveType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AgreementType:1.0.0`](../reference-data/AgreementType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AliasNameType:1.0.0`](../reference-data/AliasNameType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AliasNameTypeClass:1.0.0`](../reference-data/AliasNameTypeClass.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AnisotropyType:1.0.0`](../reference-data/AnisotropyType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ArtefactRole:1.0.0`](../reference-data/ArtefactRole.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ArtificialLiftType:1.0.0`](../reference-data/ArtificialLiftType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AzimuthReferenceType:1.0.0`](../reference-data/AzimuthReferenceType.1.0.0.md)|Changes: Differences: Different values for description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--BasinType:1.0.0`](../reference-data/BasinType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--BhaStatus:1.0.0`](../reference-data/BhaStatus.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--BinGridDefinitionMethodType:1.0.0`](../reference-data/BinGridDefinitionMethodType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--BitDullCode:1.0.0`](../reference-data/BitDullCode.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--BitReasonPulled:1.0.0`](../reference-data/BitReasonPulled.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--BitType:1.0.0`](../reference-data/BitType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--BoundaryRelationType:1.0.0`](../reference-data/BoundaryRelationType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CalculationMethodType:1.0.0`](../reference-data/CalculationMethodType.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "OSDU"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CatalogMapStateType:1.0.0`](../reference-data/CatalogMapStateType.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--CellShapeType:1.0.0`](../reference-data/CellShapeType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CementJobType:1.0.0`](../reference-data/CementJobType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ChronoStratigraphy:1.0.0`](../reference-data/ChronoStratigraphy.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CollectionPurpose:1.0.0`](../reference-data/CollectionPurpose.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ColumnBasedTableType:1.0.0`](../reference-data/ColumnBasedTableType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ColumnShapeType:1.0.0`](../reference-data/ColumnShapeType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CompressionMethodType:1.0.0`](../reference-data/CompressionMethodType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ContactRoleType:1.0.0`](../reference-data/ContactRoleType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ContractorType:1.0.0`](../reference-data/ContractorType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CoordinateReferenceSystem:1.0.0`](../reference-data/CoordinateReferenceSystem.1.0.0.md)|Changes: Differences: Different values for AuthorityCode.description; <br />Different values for HorizontalCRSID.example; <br />Different values for AuthorityCode.description; <br />Different values for VerticalCRSID.example; <br />Different values for TransformationID.example; <br />Different values for AuthorityCode.description; <br />Different values for BaseCRSID.example; <br />Different values for SourceCRSID.example; <br />Different values for TargetCRSID.example; <br />Different values for BoundingBoxWestBoundLongitude.description; <br />Different values for BoundingBoxEastBoundLongitude.description; <br />Different values for AuthorityCode.description; <br />Different values for AuthorityCode.description; <br />Different values for AuthorityCode.description; <br />Different values for BoundingBoxWestBoundLongitude.description; <br />Different values for BoundingBoxEastBoundLongitude.description; <br />Different values for AuthorityCode.description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CoordinateTransformation:1.0.0`](../reference-data/CoordinateTransformation.1.0.0.md)|Changes: Differences: Different values for TransformationID.example; <br />Different values for SourceCRSID.example; <br />Different values for TargetCRSID.example; <br />Different values for BoundingBoxWestBoundLongitude.description; <br />Different values for BoundingBoxEastBoundLongitude.description; <br />Different values for BoundingBoxWestBoundLongitude.description; <br />Different values for BoundingBoxEastBoundLongitude.description; <br />Different values for PersistableReference.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--Currency:1.0.0`](../reference-data/Currency.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CurveIndexDimensionType:1.0.0`](../reference-data/CurveIndexDimensionType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CurveSampleType:1.0.0`](../reference-data/CurveSampleType.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--DataRulePurposeType:1.0.0`](../reference-data/DataRulePurposeType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DepositionGeometryType:1.0.0`](../reference-data/DepositionGeometryType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DimensionType:1.0.0`](../reference-data/DimensionType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DiscretisationSchemeType:1.0.0`](../reference-data/DiscretisationSchemeType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DocumentType:1.0.0`](../reference-data/DocumentType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DomainType:1.0.0`](../reference-data/DomainType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DrillingActivityClassType:1.0.0`](../reference-data/DrillingActivityClassType.1.0.0.md)|Changes: Differences: Different values for x-osdu-review-status|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DrillingReasonType:1.0.0`](../reference-data/DrillingReasonType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--EncodingFormatType:1.0.0`](../reference-data/EncodingFormatType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ExistenceKind:1.0.0`](../reference-data/ExistenceKind.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ExternalCatalogNamespace:1.0.0`](../reference-data/ExternalCatalogNamespace.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--ExternalUnitOfMeasure:1.0.0`](../reference-data/ExternalUnitOfMeasure.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--ExternalUnitQuantity:1.0.0`](../reference-data/ExternalUnitQuantity.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--FacetRole:1.0.0`](../reference-data/FacetRole.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FacetType:1.0.0`](../reference-data/FacetType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FacilityEventType:1.0.0`](../reference-data/FacilityEventType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FacilityStateType:1.0.0`](../reference-data/FacilityStateType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FacilityType:1.0.0`](../reference-data/FacilityType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FaultThrowType:1.0.0`](../reference-data/FaultThrowType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FeatureType:1.0.0`](../reference-data/FeatureType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FluidPhaseType:1.0.0`](../reference-data/FluidPhaseType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FluidPropertyFacetName:1.0.0`](../reference-data/FluidPropertyFacetName.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FluidPropertyName:1.0.0`](../reference-data/FluidPropertyName.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FluidRheologicalModelType:1.0.0`](../reference-data/FluidRheologicalModelType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FluidRole:1.0.0`](../reference-data/FluidRole.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FluidType:1.0.0`](../reference-data/FluidType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--FormationPressureTestType:1.0.0`](../reference-data/FormationPressureTestType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--GasReadingType:1.0.0`](../reference-data/GasReadingType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--GeoPoliticalEntityType:1.0.0`](../reference-data/GeoPoliticalEntityType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--GeoReferencedImageType:1.0.0`](../reference-data/GeoReferencedImageType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--GeologicUnitShapeType:1.0.0`](../reference-data/GeologicUnitShapeType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--GeologicalFormation:1.0.0`](../reference-data/GeologicalFormation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--HeaderKeyName:1.0.0`](../reference-data/HeaderKeyName.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--IjkCellFace:1.0.0`](../reference-data/IjkCellFace.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--IndexableElement:1.0.0`](../reference-data/IndexableElement.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--InterpolationMethod:1.0.0`](../reference-data/InterpolationMethod.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--KDirectionType:1.0.0`](../reference-data/KDirectionType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LegalStatus:1.0.0`](../reference-data/LegalStatus.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LicenseState:1.0.0`](../reference-data/LicenseState.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LineageRelationshipType:1.0.0`](../reference-data/LineageRelationshipType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LinerType:1.0.0`](../reference-data/LinerType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LithoStratigraphy:1.0.0`](../reference-data/LithoStratigraphy.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LithologyType:1.0.0`](../reference-data/LithologyType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveBusinessValue:1.0.0`](../reference-data/LogCurveBusinessValue.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveFamily:1.0.0`](../reference-data/LogCurveFamily.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveFamily:1.1.0`](../reference-data/LogCurveFamily.1.1.0.md)|No Changes; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveMainFamily:1.0.0`](../reference-data/LogCurveMainFamily.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveType:1.0.0`](../reference-data/LogCurveType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogCurveType:1.1.0`](../reference-data/LogCurveType.1.1.0.md)|No Changes; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--LogType:1.0.0`](../reference-data/LogType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--MarkerPropertyType:1.0.0`](../reference-data/MarkerPropertyType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--MarkerType:1.0.0`](../reference-data/MarkerType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--MaterialType:1.0.0`](../reference-data/MaterialType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--MudClass:1.0.0`](../reference-data/MudClass.1.0.0.md)|Changes: Differences: Different values for x-osdu-review-status|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OSDUJsonExtensions:1.0.0`](../reference-data/OSDUJsonExtensions.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OSDURegion:1.0.0`](../reference-data/OSDURegion.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ObjectiveType:1.0.0`](../reference-data/ObjectiveType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ObligationType:1.0.0`](../reference-data/ObligationType.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "None"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OperatingEnvironment:1.0.0`](../reference-data/OperatingEnvironment.1.0.0.md)|Changes: Differences: Different values for description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OrderingCriteriaType:1.0.0`](../reference-data/OrderingCriteriaType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OrganisationType:1.0.0`](../reference-data/OrganisationType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGContextType:1.0.0`](../reference-data/PPFGContextType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveFamily:1.0.0`](../reference-data/PPFGCurveFamily.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveLithoType:1.0.0`](../reference-data/PPFGCurveLithoType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveMainFamily:1.0.0`](../reference-data/PPFGCurveMainFamily.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveMnemonic:1.0.0`](../reference-data/PPFGCurveMnemonic.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveProbability:1.0.0`](../reference-data/PPFGCurveProbability.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveProcessingType:1.0.0`](../reference-data/PPFGCurveProcessingType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PPFGCurveTransformModelType:1.0.0`](../reference-data/PPFGCurveTransformModelType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ParameterKind:1.0.0`](../reference-data/ParameterKind.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ParameterRole:1.0.0`](../reference-data/ParameterRole.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ParameterType:1.0.0`](../reference-data/ParameterType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PersonnelOrganisationRole:1.0.0`](../reference-data/PersonnelOrganisationRole.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PersonnelServiceType:1.0.0`](../reference-data/PersonnelServiceType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PetroleumSystemElementType:1.0.0`](../reference-data/PetroleumSystemElementType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PillarShapeType:1.0.0`](../reference-data/PillarShapeType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PlayType:1.0.0`](../reference-data/PlayType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PlugType:1.0.0`](../reference-data/PlugType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ProcessingParameterType:1.0.0`](../reference-data/ProcessingParameterType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ProjectRole:1.0.0`](../reference-data/ProjectRole.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ProjectStateType:1.0.0`](../reference-data/ProjectStateType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PropertyFieldRepresentationType:1.0.0`](../reference-data/PropertyFieldRepresentationType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PropertyNameType:1.0.0`](../reference-data/PropertyNameType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PropertyType:1.0.0`](../reference-data/PropertyType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ProspectType:1.0.0`](../reference-data/ProspectType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--PumpOpType:1.0.0`](../reference-data/PumpOpType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QualitativeSpatialAccuracyType:1.0.0`](../reference-data/QualitativeSpatialAccuracyType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QualityDataRule:1.0.0`](../reference-data/QualityDataRule.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QualityDataRuleSet:1.0.0`](../reference-data/QualityDataRuleSet.1.0.0.md)|Changes:  in `x-osdu` extensions tags|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QuantitativeAccuracyBand:1.0.0`](../reference-data/QuantitativeAccuracyBand.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "OSDU"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ReasonTripType:1.0.0`](../reference-data/ReasonTripType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RepresentationRole:1.0.0`](../reference-data/RepresentationRole.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RepresentationType:1.0.0`](../reference-data/RepresentationType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ResourceCurationStatus:1.0.0`](../reference-data/ResourceCurationStatus.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ResourceLifecycleStatus:1.0.0`](../reference-data/ResourceLifecycleStatus.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ResourceSecurityClassification:1.0.0`](../reference-data/ResourceSecurityClassification.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RigType:1.0.0`](../reference-data/RigType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RiskCategory:1.0.0`](../reference-data/RiskCategory.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RiskConsequenceCategory:1.0.0`](../reference-data/RiskConsequenceCategory.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RiskConsequenceSubCategory:1.0.0`](../reference-data/RiskConsequenceSubCategory.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RiskDiscipline:1.0.0`](../reference-data/RiskDiscipline.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RiskHierarchyLevel:1.0.0`](../reference-data/RiskHierarchyLevel.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RiskResponseStatus:1.0.0`](../reference-data/RiskResponseStatus.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RiskSubCategory:1.0.0`](../reference-data/RiskSubCategory.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--RiskType:1.0.0`](../reference-data/RiskType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SchemaFormatType:1.0.0`](../reference-data/SchemaFormatType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SectionType:1.0.0`](../reference-data/SectionType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicAcquisitionType:1.0.0`](../reference-data/SeismicAcquisitionType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicAttributeType:1.0.0`](../reference-data/SeismicAttributeType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicBinGridType:1.0.0`](../reference-data/SeismicBinGridType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicDomainType:1.0.0`](../reference-data/SeismicDomainType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicEnergySourceType:1.0.0`](../reference-data/SeismicEnergySourceType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicFaultType:1.0.0`](../reference-data/SeismicFaultType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicFilteringType:1.0.0`](../reference-data/SeismicFilteringType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicGatherType:1.0.0`](../reference-data/SeismicGatherType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicGeometryType:1.0.0`](../reference-data/SeismicGeometryType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicHorizonType:1.0.0`](../reference-data/SeismicHorizonType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicMigrationType:1.0.0`](../reference-data/SeismicMigrationType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicPickingType:1.0.0`](../reference-data/SeismicPickingType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicProcessingStageType:1.0.0`](../reference-data/SeismicProcessingStageType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicReceiverType:1.0.0`](../reference-data/SeismicReceiverType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicStackingType:1.0.0`](../reference-data/SeismicStackingType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicTraceDataDimensionalityType:1.0.0`](../reference-data/SeismicTraceDataDimensionalityType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicTraceSortOrder:1.0.0`](../reference-data/SeismicTraceSortOrder.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SeismicWaveType:1.0.0`](../reference-data/SeismicWaveType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SequenceStratigraphicSchemaType:1.0.0`](../reference-data/SequenceStratigraphicSchemaType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SequenceStratigraphySurfaceType:1.0.0`](../reference-data/SequenceStratigraphySurfaceType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SpatialGeometryType:1.0.0`](../reference-data/SpatialGeometryType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SpatialParameterType:1.0.0`](../reference-data/SpatialParameterType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--StandardsOrganisation:1.0.0`](../reference-data/StandardsOrganisation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--StratigraphicColumnRankUnitType:1.0.0`](../reference-data/StratigraphicColumnRankUnitType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--StratigraphicColumnValidityAreaType:1.0.0`](../reference-data/StratigraphicColumnValidityAreaType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--StratigraphicRoleType:1.0.0`](../reference-data/StratigraphicRoleType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--StringClass:1.0.0`](../reference-data/StringClass.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--SurveyToolType:1.0.0`](../reference-data/SurveyToolType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TargetShape:1.0.0`](../reference-data/TargetShape.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TargetType:1.0.0`](../reference-data/TargetType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TechnicalAssuranceType:1.0.0`](../reference-data/TechnicalAssuranceType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TectonicSettingType:1.0.0`](../reference-data/TectonicSettingType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TestSubType:1.0.0`](../reference-data/TestSubType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TestType:1.0.0`](../reference-data/TestType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TrajectoryStationPropertyType:1.0.0`](../reference-data/TrajectoryStationPropertyType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularAssemblyStatusType:1.0.0`](../reference-data/TubularAssemblyStatusType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularAssemblyType:1.0.0`](../reference-data/TubularAssemblyType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularComponentConnectionType:1.0.0`](../reference-data/TubularComponentConnectionType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularComponentGrade:1.0.0`](../reference-data/TubularComponentGrade.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularComponentPinBoxType:1.0.0`](../reference-data/TubularComponentPinBoxType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularComponentType:1.0.0`](../reference-data/TubularComponentType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularUmbilicalServiceType:1.0.0`](../reference-data/TubularUmbilicalServiceType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--TubularUmbilicalType:1.0.0`](../reference-data/TubularUmbilicalType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--UnitOfMeasure:1.0.0`](../reference-data/UnitOfMeasure.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--UnitOfMeasureConfiguration:1.0.0`](../reference-data/UnitOfMeasureConfiguration.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--UnitQuantity:1.0.0`](../reference-data/UnitQuantity.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ValueChainStatusType:1.0.0`](../reference-data/ValueChainStatusType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VelocityAnalysisMethod:1.0.0`](../reference-data/VelocityAnalysisMethod.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VelocityDirectionType:1.0.0`](../reference-data/VelocityDirectionType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VelocityType:1.0.0`](../reference-data/VelocityType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VerticalMeasurementPath:1.0.0`](../reference-data/VerticalMeasurementPath.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "OSDU"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VerticalMeasurementSource:1.0.0`](../reference-data/VerticalMeasurementSource.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VerticalMeasurementType:1.0.0`](../reference-data/VerticalMeasurementType.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "OSDU"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WeatherType:1.0.0`](../reference-data/WeatherType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WellActivityPhaseType:1.0.0`](../reference-data/WellActivityPhaseType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WellActivityProgramType:1.0.0`](../reference-data/WellActivityProgramType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WellInterestType:1.0.0`](../reference-data/WellInterestType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WellLogSamplingDomainType:1.0.0`](../reference-data/WellLogSamplingDomainType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WellSiteProductType:1.0.0`](../reference-data/WellSiteProductType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WellboreTrajectoryType:1.0.0`](../reference-data/WellboreTrajectoryType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--WordFormatType:1.0.0`](../reference-data/WordFormatType.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|

# `type` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:type--Type:1.0.0`](../type/Type.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|

# `work-product-component` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:work-product-component--Activity:1.0.0`](../work-product-component/Activity.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--AquiferInterpretation:1.0.0`](../work-product-component/AquiferInterpretation.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--ColumnBasedTable:1.0.0`](../work-product-component/ColumnBasedTable.1.0.0.md)|Changes: Differences: Different values for items.description; <br />Different values for BooleanColumn.description; <br />Different values for IntegerColumn.description; <br />Different values for NumberColumn.description; <br />Different values for StringColumn.description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--DataQuality:1.0.0`](../work-product-component/DataQuality.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--Document:1.0.0`](../work-product-component/Document.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--EarthModelInterpretation:1.0.0`](../work-product-component/EarthModelInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--FaultInterpretation:1.0.0`](../work-product-component/FaultInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--FaultSystem:1.0.0`](../work-product-component/FaultSystem.1.0.0.md)|Changes: Differences: Different array length; current schema 0, baseline 2|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--FaultSystem:1.1.0`](../work-product-component/FaultSystem.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--GenericImage:1.0.0`](../work-product-component/GenericImage.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GenericProperty:1.0.0`](../work-product-component/GenericProperty.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GenericRepresentation:1.0.0`](../work-product-component/GenericRepresentation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GeoReferencedImage:1.0.0`](../work-product-component/GeoReferencedImage.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GeobodyBoundaryInterpretation:1.0.0`](../work-product-component/GeobodyBoundaryInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GeobodyInterpretation:1.0.0`](../work-product-component/GeobodyInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GeologicUnitOccurrenceInterpretation:1.0.0`](../work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GpGridRepresentation:1.0.0`](../work-product-component/GpGridRepresentation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0`](../work-product-component/GridConnectionSetRepresentation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--HorizonInterpretation:1.0.0`](../work-product-component/HorizonInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0`](../work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--IjkGridRepresentation:1.0.0`](../work-product-component/IjkGridRepresentation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--LocalBoundaryFeature:1.0.0`](../work-product-component/LocalBoundaryFeature.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--LocalModelCompoundCrs:1.0.0`](../work-product-component/LocalModelCompoundCrs.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--LocalModelFeature:1.0.0`](../work-product-component/LocalModelFeature.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--LocalRockVolumeFeature:1.0.0`](../work-product-component/LocalRockVolumeFeature.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--NotionalSeismicLine:1.0.0`](../work-product-component/NotionalSeismicLine.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--PPFGDataset:1.0.0`](../work-product-component/PPFGDataset.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--PersistedCollection:1.0.0`](../work-product-component/PersistedCollection.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--PlannedLithology:1.0.0`](../work-product-component/PlannedLithology.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--RockFluidOrganizationInterpretation:1.0.0`](../work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--RockFluidUnitInterpretation:1.0.0`](../work-product-component/RockFluidUnitInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0`](../work-product-component/SealedSurfaceFramework.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SeismicBinGrid:1.0.0`](../work-product-component/SeismicBinGrid.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SeismicFault:1.0.0`](../work-product-component/SeismicFault.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--SeismicHorizon:1.0.0`](../work-product-component/SeismicHorizon.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SeismicHorizon:1.1.0`](../work-product-component/SeismicHorizon.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--SeismicLineGeometry:1.0.0`](../work-product-component/SeismicLineGeometry.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SeismicTraceData:1.0.0`](../work-product-component/SeismicTraceData.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SeismicTraceData:1.1.0`](../work-product-component/SeismicTraceData.1.1.0.md)|No Changes; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--StratigraphicColumn:1.0.0`](../work-product-component/StratigraphicColumn.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.0.0`](../work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0`](../work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--StratigraphicUnitInterpretation:1.0.0`](../work-product-component/StratigraphicUnitInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--StructuralOrganizationInterpretation:1.0.0`](../work-product-component/StructuralOrganizationInterpretation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--SubRepresentation:1.0.0`](../work-product-component/SubRepresentation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--TimeSeries:1.0.0`](../work-product-component/TimeSeries.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--TubularAssembly:1.0.0`](../work-product-component/TubularAssembly.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--TubularComponent:1.0.0`](../work-product-component/TubularComponent.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--TubularUmbilical:1.0.0`](../work-product-component/TubularUmbilical.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0`](../work-product-component/UnsealedSurfaceFramework.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0`](../work-product-component/UnsealedSurfaceFramework.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0`](../work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0`](../work-product-component/UnstructuredGridRepresentation.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--VelocityModeling:1.0.0`](../work-product-component/VelocityModeling.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellLog:1.0.0`](../work-product-component/WellLog.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellLog:1.1.0`](../work-product-component/WellLog.1.1.0.md)|No Changes; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellLog:1.2.0`](../work-product-component/WellLog.1.2.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--WellboreIntervalSet:1.0.0`](../work-product-component/WellboreIntervalSet.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--WellboreMarkerSet:1.0.0`](../work-product-component/WellboreMarkerSet.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellboreMarkerSet:1.1.0`](../work-product-component/WellboreMarkerSet.1.1.0.md)|No Changes; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellboreMarkerSet:1.2.0`](../work-product-component/WellboreMarkerSet.1.2.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--WellboreTrajectory:1.0.0`](../work-product-component/WellboreTrajectory.1.0.0.md)|Changes: Differences: Different values for ProjectedCRSID.example; <br />Different values for GeographicCRSID.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellboreTrajectory:1.1.0`](../work-product-component/WellboreTrajectory.1.1.0.md)|Changes: Differences: Different values for ProjectedCRSID.example; <br />Different values for GeographicCRSID.example; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|

# `work-product` Schemas

|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:work-product--WorkProduct:1.0.0`](../work-product/WorkProduct.1.0.0.md)|No Changes|DEVELOPMENT|DEVELOPMENT|
 
# Summary of Changed Schemas
**Number of changed Schemas: 35**
 
|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:AbstractAnyCrsFeatureCollection:1.0.0`](../abstract/AbstractAnyCrsFeatureCollection.1.0.0.md)|Changes: Differences: Different values for CoordinateReferenceSystemID.example; <br />Different values for VerticalCoordinateReferenceSystemID.example; <br />Different values for persistableReferenceCrs.example; <br />Different values for persistableReferenceVerticalCrs.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractBusinessRule:1.0.0`](../abstract/AbstractBusinessRule.1.0.0.md)|Changes: Differences: DataRuleSets.title added to baseline schema; <br />items.title added to baseline schema; <br />items.description added to baseline schema; <br />DataRuleSetID.title added to baseline schema; <br />DataRuleSetRunStatus.title added to baseline schema; <br />DataRules.title added to baseline schema; <br />items.title added to baseline schema; <br />items.description added to baseline schema; <br />DataRuleID.title added to baseline schema; <br />DataRuleRunStatus.title added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractMetaItem:1.0.0`](../abstract/AbstractMetaItem.1.0.0.md)|Changes: Differences: Different values for name.example; <br />Different values for persistableReference.example; <br />Different values for coordinateReferenceSystemID.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractReferenceLevel:1.0.0`](../abstract/AbstractReferenceLevel.1.0.0.md)|Changes: Differences: Different values for VerticalCoordinateReferenceSystemID.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractRepresentation:1.0.0`](../abstract/AbstractRepresentation.1.0.0.md)|Changes: Differences: Different values for InterpretationID.pattern; <br />Different array length; current schema 10, baseline 9|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:AbstractSpatialReference:1.0.0`](../abstract/AbstractSpatialReference.1.0.0.md)|Changes: Differences: Different values for CoordinateReferenceSystemID.example; <br />Different values for VerticalCoordinateReferenceSystemID.example; <br />Different values for PersistableReferenceCRS.example; <br />Different values for PersistableReferenceVerticalCRS.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:dataset--File.OGC.GeoTIFF:1.0.0`](../dataset/File.OGC.GeoTIFF.1.0.0.md)|Changes: Differences: Different values for CoordinateReferenceSystemID.example; <br />Different values for VerticalCoordinateReferenceSystemID.example; <br />Different values for PersistableReferenceCrs.example; <br />Different values for PersistableReferenceVerticalCrs.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--ActivityPlan:1.0.0`](../master-data/ActivityPlan.1.0.0.md)|Changes: Differences: Different array length; current schema 2, baseline 1; <br />Different values for priority.path|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--ActivityTemplateArc:1.0.0`](../master-data/ActivityTemplateArc.1.0.0.md)|Changes: Differences: Different values for description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--BHARun:1.0.0`](../master-data/BHARun.1.0.0.md)|Changes:  in `x-osdu` extensions tags|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--BoundaryFeature:1.0.0`](../master-data/BoundaryFeature.1.0.0.md)|Changes: Differences: Name added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--FluidsReport:1.0.0`](../master-data/FluidsReport.1.0.0.md)|Changes: Differences: Different values for items.description; <br />Name added to baseline schema; <br />Different values for x-osdu-review-status|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--ModelFeature:1.0.0`](../master-data/ModelFeature.1.0.0.md)|Changes: Differences: Name added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--OperationsReport:1.0.0`](../master-data/OperationsReport.1.0.0.md)|Changes: Differences: Name added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--RockVolumeFeature:1.0.0`](../master-data/RockVolumeFeature.1.0.0.md)|Changes: Differences: Name added to baseline schema|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0`](../master-data/Seismic2DInterpretationSet.1.1.0.md)|Changes: Differences: Different values for priority.path; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0`](../master-data/Seismic3DInterpretationSet.1.1.0.md)|Changes: Differences: Different values for priority.path; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0`](../master-data/SeismicAcquisitionSurvey.1.1.0.md)|Changes: Differences: Different values for priority.path; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:master-data--SeismicProcessingProject:1.1.0`](../master-data/SeismicProcessingProject.1.1.0.md)|Changes: Differences: Different values for priority.path; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--AzimuthReferenceType:1.0.0`](../reference-data/AzimuthReferenceType.1.0.0.md)|Changes: Differences: Different values for description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CalculationMethodType:1.0.0`](../reference-data/CalculationMethodType.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "OSDU"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CoordinateReferenceSystem:1.0.0`](../reference-data/CoordinateReferenceSystem.1.0.0.md)|Changes: Differences: Different values for AuthorityCode.description; <br />Different values for HorizontalCRSID.example; <br />Different values for AuthorityCode.description; <br />Different values for VerticalCRSID.example; <br />Different values for TransformationID.example; <br />Different values for AuthorityCode.description; <br />Different values for BaseCRSID.example; <br />Different values for SourceCRSID.example; <br />Different values for TargetCRSID.example; <br />Different values for BoundingBoxWestBoundLongitude.description; <br />Different values for BoundingBoxEastBoundLongitude.description; <br />Different values for AuthorityCode.description; <br />Different values for AuthorityCode.description; <br />Different values for AuthorityCode.description; <br />Different values for BoundingBoxWestBoundLongitude.description; <br />Different values for BoundingBoxEastBoundLongitude.description; <br />Different values for AuthorityCode.description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--CoordinateTransformation:1.0.0`](../reference-data/CoordinateTransformation.1.0.0.md)|Changes: Differences: Different values for TransformationID.example; <br />Different values for SourceCRSID.example; <br />Different values for TargetCRSID.example; <br />Different values for BoundingBoxWestBoundLongitude.description; <br />Different values for BoundingBoxEastBoundLongitude.description; <br />Different values for BoundingBoxWestBoundLongitude.description; <br />Different values for BoundingBoxEastBoundLongitude.description; <br />Different values for PersistableReference.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--DrillingActivityClassType:1.0.0`](../reference-data/DrillingActivityClassType.1.0.0.md)|Changes: Differences: Different values for x-osdu-review-status|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--MudClass:1.0.0`](../reference-data/MudClass.1.0.0.md)|Changes: Differences: Different values for x-osdu-review-status|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--ObligationType:1.0.0`](../reference-data/ObligationType.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "None"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--OperatingEnvironment:1.0.0`](../reference-data/OperatingEnvironment.1.0.0.md)|Changes: Differences: Different values for description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QualityDataRuleSet:1.0.0`](../reference-data/QualityDataRuleSet.1.0.0.md)|Changes:  in `x-osdu` extensions tags|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--QuantitativeAccuracyBand:1.0.0`](../reference-data/QuantitativeAccuracyBand.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "OSDU"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VerticalMeasurementPath:1.0.0`](../reference-data/VerticalMeasurementPath.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "OSDU"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:reference-data--VerticalMeasurementType:1.0.0`](../reference-data/VerticalMeasurementType.1.0.0.md)|Changes: Differences: Different values for description; <br />Different values for index 0: current "PPDM" versus baseline "OSDU"|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--ColumnBasedTable:1.0.0`](../work-product-component/ColumnBasedTable.1.0.0.md)|Changes: Differences: Different values for items.description; <br />Different values for BooleanColumn.description; <br />Different values for IntegerColumn.description; <br />Different values for NumberColumn.description; <br />Different values for StringColumn.description|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--FaultSystem:1.0.0`](../work-product-component/FaultSystem.1.0.0.md)|Changes: Differences: Different array length; current schema 0, baseline 2|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellboreTrajectory:1.0.0`](../work-product-component/WellboreTrajectory.1.0.0.md)|Changes: Differences: Different values for ProjectedCRSID.example; <br />Different values for GeographicCRSID.example|DEVELOPMENT|DEVELOPMENT|
|&rarr; [`osdu:wks:work-product-component--WellboreTrajectory:1.1.0`](../work-product-component/WellboreTrajectory.1.1.0.md)|Changes: Differences: Different values for ProjectedCRSID.example; <br />Different values for GeographicCRSID.example; Conformal with semantic versioning|DEVELOPMENT|DEVELOPMENT|
 
# Summary of New Schemas
**Number of new Schemas: 20**
 
|Kind|Status|Status Local|Status Community|
|----|----|----|----|
|&rarr; [`osdu:wks:AbstractColumnBasedTable:1.0.0`](../abstract/AbstractColumnBasedTable.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:AbstractIjkGridFlowSimulationBoundaryConnection:1.0.0`](../abstract/AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:master-data--FluidsReport:1.1.0`](../master-data/FluidsReport.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:master-data--Organisation:1.1.0`](../master-data/Organisation.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--CatalogMapStateType:1.0.0`](../reference-data/CatalogMapStateType.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--CurveSampleType:1.0.0`](../reference-data/CurveSampleType.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--ExternalCatalogNamespace:1.0.0`](../reference-data/ExternalCatalogNamespace.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--ExternalUnitOfMeasure:1.0.0`](../reference-data/ExternalUnitOfMeasure.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--ExternalUnitQuantity:1.0.0`](../reference-data/ExternalUnitQuantity.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:reference-data--IjkCellFace:1.0.0`](../reference-data/IjkCellFace.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--AquiferInterpretation:1.0.0`](../work-product-component/AquiferInterpretation.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--FaultSystem:1.1.0`](../work-product-component/FaultSystem.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0`](../work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--SeismicFault:1.0.0`](../work-product-component/SeismicFault.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--SeismicHorizon:1.1.0`](../work-product-component/SeismicHorizon.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0`](../work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0`](../work-product-component/UnsealedSurfaceFramework.1.1.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--WellLog:1.2.0`](../work-product-component/WellLog.1.2.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--WellboreIntervalSet:1.0.0`](../work-product-component/WellboreIntervalSet.1.0.0.md)|**New schema**|DEVELOPMENT|-|
|&rarr; [`osdu:wks:work-product-component--WellboreMarkerSet:1.2.0`](../work-product-component/WellboreMarkerSet.1.2.0.md)|**New schema**; Conformal with semantic versioning|DEVELOPMENT|-|