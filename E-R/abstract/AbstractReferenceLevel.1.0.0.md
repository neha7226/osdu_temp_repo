<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractReferenceLevel [Status: Accepted]
A reference level or horizontal reference surface definition, which can be used embedded in other schemas.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractReferenceLevel:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractReferenceLevel.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractReferenceLevel.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractReferenceLevel.1.0.0.json](../../Authoring/abstract/AbstractReferenceLevel.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractReferenceLevel.1.0.0.json](../../Generated/abstract/AbstractReferenceLevel.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractReferenceLevel.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractReferenceLevel.1.0.0.json)
* Link to worked examples in context &rarr; [Topic: Geo-Referenced Images](../../Examples/WorkedExamples/GeoReferencedImage/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractReferenceLevel


## Outgoing Relationships for AbstractReferenceLevel

![AbstractReferenceLevel](../_diagrams/abstract/AbstractReferenceLevel.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractReferenceLevel Referenced by other Entities

![AbstractReferenceLevel](../_diagrams/abstract/AbstractReferenceLevel.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractReferenceLevel Properties

## Table of AbstractReferenceLevel Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Height| |number|optional|Height|The height above the reference surface defined by the VerticalCoordinateReferenceSystemID positive upwards.|(No pattern)|(No format)|UOM:length|(No indexing hint)|(Not derived)|(No natural key)|0.0|
|VerticalCoordinateReferenceSystemID &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)| |string|optional|Vertical Coordinate Reference System ID|The relationship to the vertical CRS defining the absolute reference surface.|<code>^[\w\-\.]+:reference-data\-\-CoordinateReferenceSystem:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--CoordinateReferenceSystem:Vertical:EPSG::5714:|
|EffectiveDateTime| |string|optional|Effective Date Time|The date and time at which this reference level instance becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TerminationDateTime| |string|optional|Termination Date Time|The date and time at which a reference level instance is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalMeasurementTypeID &rarr; [VerticalMeasurementType](../reference-data/VerticalMeasurementType.1.0.2.md)| |string|optional|Vertical Measurement Type ID|Specifies the type of vertical measurement (SRD, ES, GR, MSL,and many more).|<code>^[\w\-\.]+:reference-data\-\-VerticalMeasurementType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [VerticalMeasurementType](../reference-data/VerticalMeasurementType.1.0.2.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--VerticalMeasurementType:SRD:|
|VerticalMeasurementPathID &rarr; [VerticalMeasurementPath](../reference-data/VerticalMeasurementPath.1.0.1.md)| |string|optional|Vertical Measurement Path ID|When used in context of a Wellbore, this specifies Measured Depth, True Vertical Depth, or Elevation.|<code>^[\w\-\.]+:reference-data\-\-VerticalMeasurementPath:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [VerticalMeasurementPath](../reference-data/VerticalMeasurementPath.1.0.1.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalMeasurementSourceID &rarr; [VerticalMeasurementSource](../reference-data/VerticalMeasurementSource.1.0.0.md)| |string|optional|Vertical Measurement Source ID|When used in context of a Wellbore this specifies Driller vs Logger measurements.|<code>^[\w\-\.]+:reference-data\-\-VerticalMeasurementSource:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [VerticalMeasurementSource](../reference-data/VerticalMeasurementSource.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|WellboreTVDTrajectoryID &rarr; [WellboreTrajectory](../work-product-component/WellboreTrajectory.1.1.0.md)| |string|optional|Wellbore TVD Trajectory ID|When used in context of a Wellbore this specifies what directional survey or wellpath was used to calculate the TVD.|<code>^[\w\-\.]+:work-product-component\-\-WellboreTrajectory:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WellboreTrajectory](../work-product-component/WellboreTrajectory.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalUncertainty| |number|optional|Vertical Uncertainty|The positional uncertainty in the vertical direction.|(No pattern)|(No format)|UOM:length|(No indexing hint)|(Not derived)|(No natural key)|0.3|
|SeismicReplacementVelocity| |number|optional|Seismic Replacement Velocity|The replacement velocity value used to produce vertical static shifts in seismic data.|(No pattern)|(No format)|UOM:length per time|(No indexing hint)|(Not derived)|(No natural key)|1480|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractReferenceLevel.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | EmbeddedReferenceLevel | one |  |
| Included by | [GeoReferencedImage.1.1.0](../work-product-component/GeoReferencedImage.1.1.0.md) | EmbeddedReferenceLevel | one |  |
| Included by | [ReferenceLevel.1.0.0](../master-data/ReferenceLevel.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._