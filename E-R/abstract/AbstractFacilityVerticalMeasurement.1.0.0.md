<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractFacilityVerticalMeasurement [Status: Accepted]
A location along a wellbore, _usually_ associated with some aspect of the drilling of the wellbore, but not with any intersecting _subsurface_ natural surfaces.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractFacilityVerticalMeasurement:1.0.0`
* Schema status: PUBLISHED
* **Migration Guide (M12)** [`osdu:wks:AbstractFacilityVerticalMeasurement:1.0.0`](../../Guides/MigrationGuides/M12/AbstractFacilityVerticalMeasurement.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractFacilityVerticalMeasurement.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractFacilityVerticalMeasurement.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractFacilityVerticalMeasurement.1.0.0.json](../../Authoring/abstract/AbstractFacilityVerticalMeasurement.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractFacilityVerticalMeasurement.1.0.0.json](../../Generated/abstract/AbstractFacilityVerticalMeasurement.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractFacilityVerticalMeasurement.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractFacilityVerticalMeasurement.1.0.0.json)
* Link to worked examples in context &rarr; [Topic: Summary of Geomatics in OSDU Schemas](../../Examples/WorkedExamples/Geomatics/README.md)
* Link to worked examples in context &rarr; [Topic: Usage of `work-product-component--WellLog`](../../Examples/WorkedExamples/WellLog/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractFacilityVerticalMeasurement


## Outgoing Relationships for AbstractFacilityVerticalMeasurement

![AbstractFacilityVerticalMeasurement](../_diagrams/abstract/AbstractFacilityVerticalMeasurement.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractFacilityVerticalMeasurement Referenced by other Entities

![AbstractFacilityVerticalMeasurement](../_diagrams/abstract/AbstractFacilityVerticalMeasurement.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacilityVerticalMeasurement Properties

## Table of AbstractFacilityVerticalMeasurement Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|EffectiveDateTime| |string|optional|Effective Date Time|The date and time at which a vertical measurement instance becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalMeasurement| |number|optional|Vertical Measurement|The value of the elevation or depth. Depth is positive downwards from a vertical reference or geodetic datum along a path, which can be vertical; elevation is positive upwards from a geodetic datum along a vertical path. Either can be negative.|(No pattern)|(No format)|UOM_via_property:VerticalMeasurementUnitOfMeasureID|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TerminationDateTime| |string|optional|Termination Date Time|The date and time at which a vertical measurement instance is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalMeasurementTypeID &rarr; [VerticalMeasurementType](../reference-data/VerticalMeasurementType.1.0.2.md)| |string|optional|Vertical Measurement Type Id|Specifies the type of vertical measurement (TD, Plugback, Kickoff, Drill Floor, Rotary Table...).|<code>^[\w\-\.]+:reference-data\-\-VerticalMeasurementType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [VerticalMeasurementType](../reference-data/VerticalMeasurementType.1.0.2.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalMeasurementPathID &rarr; [VerticalMeasurementPath](../reference-data/VerticalMeasurementPath.1.0.1.md)| |string|optional|Vertical Measurement Path Id|Specifies Measured Depth, True Vertical Depth, or Elevation.|<code>^[\w\-\.]+:reference-data\-\-VerticalMeasurementPath:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [VerticalMeasurementPath](../reference-data/VerticalMeasurementPath.1.0.1.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalMeasurementSourceID &rarr; [VerticalMeasurementSource](../reference-data/VerticalMeasurementSource.1.0.0.md)| |string|optional|Vertical Measurement Source Id|Specifies Driller vs Logger.|<code>^[\w\-\.]+:reference-data\-\-VerticalMeasurementSource:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [VerticalMeasurementSource](../reference-data/VerticalMeasurementSource.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|WellboreTVDTrajectoryID &rarr; [WellboreTrajectory](../work-product-component/WellboreTrajectory.1.1.0.md)| |string|optional|Wellbore Tvd Trajectory Id|Specifies what directional survey or wellpath was used to calculate the TVD.|<code>^[\w\-\.]+:work-product-component\-\-WellboreTrajectory:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WellboreTrajectory](../work-product-component/WellboreTrajectory.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalMeasurementUnitOfMeasureID &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)| |string|optional|Vertical Measurement Unit Of Measure Id|The unit of measure for the vertical measurement. If a unit of measure and a vertical CRS are provided, the unit of measure provided is taken over the unit of measure from the CRS.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalCRSID &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)| |string|optional|Vertical Crsid|A vertical coordinate reference system defines the origin for height or depth values. It is expected that either VerticalCRSID or VerticalReferenceID reference is provided in a given vertical measurement array object, but not both.|<code>^[\w\-\.]+:reference-data\-\-CoordinateReferenceSystem:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalReferenceID| |string|optional|Vertical Reference Id|The reference point from which the relative vertical measurement is made. This is only populated if the measurement has no VerticalCRSID specified. The value entered must match the VerticalMeasurementID for another vertical measurement array element in Wellbore or Well or in a related parent facility. The relationship should be  declared explicitly in VerticalReferenceEntityID. Any chain of measurements must ultimately resolve to a Vertical CRS. It is expected that a VerticalCRSID or a VerticalReferenceID is provided in a given vertical measurement array object, but not both.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalReferenceEntityID &rarr; [Wellbore](../master-data/Wellbore.1.3.0.md) &rarr; [Well](../master-data/Well.1.2.0.md) &rarr; [Rig](../master-data/Rig.1.1.0.md)| |string|optional|Vertical Reference Entity Id|This relationship identifies the entity (aka record) in which the VerticalReferenceID is found; It could be a different OSDU entity or a self-reference. For example, a Wellbore VerticalMeasurement may reference a member of a VerticalMeasurements[] array in its parent Well record. Alternatively, VerticalReferenceEntityID may be populated with the ID of its own Wellbore record to make explicit that VerticalReferenceID is intended to be found in this record, not another.|<code>^[\w\-\.]+:(master-data\-\-Wellbore&#124;master-data\-\-Well&#124;master-data\-\-Rig):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Wellbore](../master-data/Wellbore.1.3.0.md) &rarr; [Well](../master-data/Well.1.2.0.md) &rarr; [Rig](../master-data/Rig.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VerticalMeasurementDescription| |string|optional|Vertical Measurement Description|Text which describes a vertical measurement in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacilityVerticalMeasurement.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [Coring.1.0.0](../master-data/Coring.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [FormationIntegrityTest.1.0.0](../work-product-component/FormationIntegrityTest.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [HoleSection.1.1.0](../master-data/HoleSection.1.1.0.md) | VerticalMeasurement | one |  |
| Included by | [IsolatedInterval.1.0.0](../master-data/IsolatedInterval.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [PerforationInterval.1.0.0](../master-data/PerforationInterval.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [PerforationJob.1.0.0](../master-data/PerforationJob.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [PPFGDataset.1.0.0](../work-product-component/PPFGDataset.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [RockSample.1.0.0](../master-data/RockSample.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [RockSampleAnalysis.1.0.0](../work-product-component/RockSampleAnalysis.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [RockSampleAnalysis.1.1.0](../work-product-component/RockSampleAnalysis.1.1.0.md) | VerticalMeasurement | one |  |
| Included by | [SeismicAcquisitionSurvey.1.2.0](../master-data/SeismicAcquisitionSurvey.1.2.0.md) | VerticalMeasurement | one |  |
| Included by | [Well.1.0.0](../master-data/Well.1.0.0.md) | VerticalMeasurements[].allOf | one |  |
| Included by | [Well.1.1.0](../master-data/Well.1.1.0.md) | VerticalMeasurements[].allOf | one |  |
| Included by | [Well.1.2.0](../master-data/Well.1.2.0.md) | VerticalMeasurements[].allOf | one |  |
| Included by | [Wellbore.1.0.0](../master-data/Wellbore.1.0.0.md) | VerticalMeasurements[].allOf | one |  |
| Included by | [Wellbore.1.1.0](../master-data/Wellbore.1.1.0.md) | VerticalMeasurements[].allOf | one |  |
| Included by | [Wellbore.1.1.1](../master-data/Wellbore.1.1.1.md) | VerticalMeasurements[].allOf | one |  |
| Included by | [Wellbore.1.2.0](../master-data/Wellbore.1.2.0.md) | VerticalMeasurements[].allOf | one |  |
| Included by | [Wellbore.1.3.0](../master-data/Wellbore.1.3.0.md) | VerticalMeasurements[].allOf | one |  |
| Included by | [WellboreIntervalSet.1.0.0](../work-product-component/WellboreIntervalSet.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [WellboreMarkerSet.1.0.0](../work-product-component/WellboreMarkerSet.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [WellboreMarkerSet.1.1.0](../work-product-component/WellboreMarkerSet.1.1.0.md) | VerticalMeasurement | one |  |
| Included by | [WellboreMarkerSet.1.2.0](../work-product-component/WellboreMarkerSet.1.2.0.md) | VerticalMeasurement | one |  |
| Included by | [WellboreMarkerSet.1.2.1](../work-product-component/WellboreMarkerSet.1.2.1.md) | VerticalMeasurement | one |  |
| Included by | [WellboreOpening.1.0.0](../master-data/WellboreOpening.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [WellboreTrajectory.1.0.0](../work-product-component/WellboreTrajectory.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [WellboreTrajectory.1.1.0](../work-product-component/WellboreTrajectory.1.1.0.md) | VerticalMeasurement | one |  |
| Included by | [WellLog.1.0.0](../work-product-component/WellLog.1.0.0.md) | VerticalMeasurement | one |  |
| Included by | [WellLog.1.1.0](../work-product-component/WellLog.1.1.0.md) | VerticalMeasurement | one |  |
| Included by | [WellLog.1.1.0](../work-product-component/WellLog.1.1.0.md) | SeismicReferenceElevation | one |  |
| Included by | [WellLog.1.2.0](../work-product-component/WellLog.1.2.0.md) | VerticalMeasurement | one |  |
| Included by | [WellLog.1.2.0](../work-product-component/WellLog.1.2.0.md) | SeismicReferenceElevation | one |  |
| Included by | [WellPlanningWellbore.1.0.0](../master-data/WellPlanningWellbore.1.0.0.md) | VerticalMeasurement | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._