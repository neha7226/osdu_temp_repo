<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractBusinessRule [Status: Accepted]
The business rule is a collection of one or more data rule sets with their run status as well as a collection of individual rules with their run status.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractBusinessRule:1.0.0`
* Schema status: PUBLISHED
* **Migration Guide (M11)** [`osdu:wks:AbstractBusinessRule:1.0.0`](../../Guides/MigrationGuides/M11/AbstractBusinessRule.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractBusinessRule.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractBusinessRule.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractBusinessRule.1.0.0.json](../../Authoring/abstract/AbstractBusinessRule.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractBusinessRule.1.0.0.json](../../Generated/abstract/AbstractBusinessRule.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractBusinessRule.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractBusinessRule.1.0.0.json)
* Link to worked examples in context &rarr; [Topic: Usage of `work-product-component--DataQuality`](../../Examples/WorkedExamples/DataQuality/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractBusinessRule


## Outgoing Relationships for AbstractBusinessRule

![AbstractBusinessRule](../_diagrams/abstract/AbstractBusinessRule.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractBusinessRule Referenced by other Entities

![AbstractBusinessRule](../_diagrams/abstract/AbstractBusinessRule.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractBusinessRule Properties

## Table of AbstractBusinessRule Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_DataRuleSets[]_| |_object_|_optional_|_DataRuleSets_|_The list of data rule sets that is relevant for this business process. Each data rule set reference is associated with a run-status._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|DataRuleSets[].DataRuleSetID &rarr; [QualityDataRuleSet](../reference-data/QualityDataRuleSet.2.0.0.md)| |string|optional|DataRuleSet ID|The relationship to the QualityDataRuleSet.|<code>^[\w\-\.]+:reference-data\-\-QualityDataRuleSet:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [QualityDataRuleSet](../reference-data/QualityDataRuleSet.2.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|DataRuleSets[].DataRuleSetRunStatus| |boolean|optional|DataRuleSet Run Status|True if data ruleset rule has passed, False if data ruleset rule dit not pass.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_DataRules[]_| |_object_|_optional_|_DataRules_|_The list of individual data rules that is relevant for this business process. Each data rule reference is associated with a run-status._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|DataRules[].DataRuleID &rarr; [QualityDataRule](../reference-data/QualityDataRule.1.1.0.md)| |string|optional|DataRule ID|The relationship to the individual QualityDataRule.|<code>^[\w\-\.]+:reference-data\-\-QualityDataRule:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [QualityDataRule](../reference-data/QualityDataRule.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|DataRules[].DataRuleRunStatus| |boolean|optional|DataRule Run Status|True if data rule has passed, False if data rule did not pass.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractBusinessRule.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [DataQuality.1.0.0](../work-product-component/DataQuality.1.0.0.md) | BusinessRules | one |  |
| Included by | [DataQuality.1.1.0](../work-product-component/DataQuality.1.1.0.md) | BusinessRules | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._