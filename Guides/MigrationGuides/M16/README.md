<a name="TOC"></a>

[[_TOC_]]

# Milestone 16 Migration

## PPDM reference-data Updates

Patched schemas to adopt the PPDM description:

* [ConventionalCoreType:1.0.1](ConventionalCoreType.1.0.0.md) 
* [MarkerType:1.0.1](MarkerType.1.0.0.md) 
* [VelocityAnalysisMethod:1.0.1](VelocityAnalysisMethod.1.0.0.md)

## Connected Data Sources/External Data Services EDS

1. Augmented schema for [ConnectedSourceDataJob:1.3.0](ConnectedSourceDataJob.1.2.0.md) including AbstractProjectActivity.
2. Augmented schema for [ConnectedSourceRegistryEntry:1.2.0](ConnectedSourceRegistryEntry.1.1.0.md) including AbstractProjectActivity.

## Well Delivery

1. Augmented [WellActivity:1.1.0](WellActivity.1.0.0.md) with support for rig assignments.
2. Augmented [Wellbore:1.3.0](Wellbore.1.2.0.md)

# Links

* [Back to Migration Guides](../README.md)
* [Back to Guides](../../README.md)
