<a name="TOC"></a>

[[_TOC_]]

# Reference Value Changes

## osdu:wks:reference-data--CoordinateTransformation:1.1.0

```text
IOGP Revision status as per last VersionHistory record:
  IOGP VersionHistory Code: 315
  IOGP VersionHistory Name: 10.081
  IOGP VersionHistory Date: 2023-01-17T00:00:00
  IOGP VersionHistory URL : https://apps.epsg.org/api/v1/VersionHistory/315
```

Total number of changed records: 32 of 353

| `Code` | Name | Changes |
|--------|------|---------|
|EPSG::1107|Afgooye to WGS 84 (1)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1672|Amersfoort to WGS 84 (2)|CoordTfmVersion|
|EPSG::4833|Amersfoort to WGS 84 (4)|CoordTfmVersion|
|EPSG::15918|Beijing 1954 to WGS 84 (1)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1777|DHDN to WGS 84 (2)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1133|ED50 to WGS 84 (1)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::15753|ED50 to ED87 (1)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1146|ED87 to WGS 84 (1)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::8653|ED50 to WGS 84 (14)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1147|ED50 to ED87 (2)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::8047|ED50 to WGS 84 (15)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1275|ED50 to WGS 84 (17)|PreferredUsage<br>Usages|
|EPSG::1612|ED50 to WGS 84 (23)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1613|ED50 to WGS 84 (24)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1627|ED50 to WGS 84 (25)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1784|ED50 to WGS 84 (30)|PreferredUsage<br>Usages|
|EPSG::1075|ED50 to WGS 84 (38)|PreferredUsage<br>Usages|
|EPSG::1139|ED50 to WGS 84 (7)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::15752|ED79 to WGS 84 (1)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::15779|Gulshan 303 to WGS 84 (1)|CoordTfmVersion<br>Description|
|EPSG::1241|NAD27 to NAD83 (1)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::15851|NAD27 to WGS 84 (79)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::5630|Nord Sahara 1959 to WGS 84 (8)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1314|OSGB36 to WGS 84 (6)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1195|OSGB36 to WGS 84 (1)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::5339|OSGB36 to WGS 84 (7)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::1671|RGF93 v1 to WGS 84 (1)|PreferredUsage<br>Usages|
|EPSG::1877|SAD69 to WGS 84 (14)|CoordTfmVersion|
|EPSG::5882|SAD69 to WGS 84 (16)|CoordTfmVersion|
|EPSG::1226|Schwarzeck to WGS 84 (1)|Description|
|EPSG::1899|Segara to WGS 84 (3)|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|EPSG::5840|UCS-2000 to WGS 84 (2)|Description|

[Back to TOC](#TOC)

## osdu:wks:reference-data--CoordinateReferenceSystem:1.1.0

```text
IOGP Revision status as per last VersionHistory record:
  IOGP VersionHistory Code: 315
  IOGP VersionHistory Name: 10.081
  IOGP VersionHistory Date: 2023-01-17T00:00:00
  IOGP VersionHistory URL : https://apps.epsg.org/api/v1/VersionHistory/315
```

See also [M16 change overview](./README.md#coordinatereferencesystem110-crs_ct)

Total number of changed records: 296 of 2541

| `Code` | Name | Changes |
|--------|------|---------|
|Geographic2D:EPSG::4205|Afgooye|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Geographic2D:EPSG::4314|DHDN|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Geographic2D:EPSG::4230|ED50|PreferredUsage<br>Usages|
|Geographic2D:EPSG::4668|ED79|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Geographic2D:EPSG::4267|NAD27|Description|
|Geographic2D:EPSG::4269|NAD83|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Geographic2D:EPSG::4277|OSGB36|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Geocentric:EPSG::5341|POSGAR 2007|Description|
|Geographic3D:EPSG::5342|POSGAR 2007|Description|
|Geocentric:EPSG::4928|POSGAR 94|Description|
|Geographic3D:EPSG::4929|POSGAR 94|Description|
|Geographic2D:EPSG::4694|POSGAR 94|Description|
|Geocentric:EPSG::4964|RGF93 v1|PreferredUsage<br>Usages|
|Geographic3D:EPSG::4965|RGF93 v1|PreferredUsage<br>Usages|
|Geographic2D:EPSG::4171|RGF93 v1|PreferredUsage<br>Usages|
|Geographic3D:EPSG::4979|WGS 84|PreferredUsage<br>Usages|
|Geographic2D:EPSG::4326|WGS 84|PreferredUsage<br>Usages|
|Projected:EPSG::2319|ED50 / TM27|PreferredUsage<br>Usages|
|Projected:EPSG::2320|ED50 / TM30|PreferredUsage<br>Usages|
|Projected:EPSG::2321|ED50 / TM33|PreferredUsage<br>Usages|
|Projected:EPSG::2322|ED50 / TM36|PreferredUsage<br>Usages|
|Projected:EPSG::2323|ED50 / TM39|PreferredUsage<br>Usages|
|Projected:EPSG::2324|ED50 / TM42|PreferredUsage<br>Usages|
|Projected:EPSG::2325|ED50 / TM45|PreferredUsage<br>Usages|
|Projected:EPSG::23032|ED50 / UTM zone 32N|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::23033|ED50 / UTM zone 33N|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::23034|ED50 / UTM zone 34N|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::23035|ED50 / UTM zone 35N|PreferredUsage<br>Usages|
|Projected:EPSG::23036|ED50 / UTM zone 36N|PreferredUsage<br>Usages|
|Projected:EPSG::23037|ED50 / UTM zone 37N|PreferredUsage<br>Usages|
|Projected:EPSG::23038|ED50 / UTM zone 38N|PreferredUsage<br>Usages|
|Projected:EPSG::6070|ETRS89 / EPSG Arctic zone 3-11|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::25832|ETRS89 / UTM zone 32N|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::25833|ETRS89 / UTM zone 33N|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::3034|ETRS89-extended / LCC Europe|PreferredUsage<br>Usages|
|Projected:EPSG::28357|GDA94 / MGA zone 57|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::3003|Monte Mario / Italy zone 1|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::32016|NAD27 / New York Central|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::32041|NAD27 / Texas South|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::26710|NAD27 / UTM zone 10N|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::26712|NAD27 / UTM zone 12N|Description|
|Projected:EPSG::26714|NAD27 / UTM zone 14N|Description|
|Projected:EPSG::26720|NAD27 / UTM zone 20N|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::26722|NAD27 / UTM zone 22N|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::26922|NAD83 / UTM zone 22N|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::27700|OSGB36 / British National Grid|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::5344|POSGAR 2007 / Argentina 2|Description|
|Projected:EPSG::5348|POSGAR 2007 / Argentina 6|Description|
|Projected:EPSG::22181|POSGAR 94 / Argentina 1|Description|
|Projected:EPSG::22182|POSGAR 94 / Argentina 2|Description|
|Projected:EPSG::22183|POSGAR 94 / Argentina 3|Description|
|Projected:EPSG::22184|POSGAR 94 / Argentina 4|Description|
|Projected:EPSG::22185|POSGAR 94 / Argentina 5|Description|
|Projected:EPSG::22186|POSGAR 94 / Argentina 6|Description|
|Projected:EPSG::22187|POSGAR 94 / Argentina 7|Description|
|Projected:EPSG::2154|RGF93 v1 / Lambert-93|PreferredUsage<br>Usages|
|Projected:EPSG::3415|WGS 72BE / South China Sea Lambert|Kind|
|Projected:EPSG::2094|WGS 72BE / TM 106 NE|Kind|
|Projected:EPSG::3994|WGS 84 / Mercator 41|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Projected:EPSG::32610|WGS 84 / UTM zone 10N|PreferredUsage<br>Usages|
|Projected:EPSG::32710|WGS 84 / UTM zone 10S|PreferredUsage<br>Usages|
|Projected:EPSG::32611|WGS 84 / UTM zone 11N|PreferredUsage<br>Usages|
|Projected:EPSG::32711|WGS 84 / UTM zone 11S|PreferredUsage<br>Usages|
|Projected:EPSG::32612|WGS 84 / UTM zone 12N|PreferredUsage<br>Usages|
|Projected:EPSG::32712|WGS 84 / UTM zone 12S|PreferredUsage<br>Usages|
|Projected:EPSG::32613|WGS 84 / UTM zone 13N|PreferredUsage<br>Usages|
|Projected:EPSG::32713|WGS 84 / UTM zone 13S|PreferredUsage<br>Usages|
|Projected:EPSG::32614|WGS 84 / UTM zone 14N|PreferredUsage<br>Usages|
|Projected:EPSG::32714|WGS 84 / UTM zone 14S|PreferredUsage<br>Usages|
|Projected:EPSG::32615|WGS 84 / UTM zone 15N|PreferredUsage<br>Usages|
|Projected:EPSG::32715|WGS 84 / UTM zone 15S|PreferredUsage<br>Usages|
|Projected:EPSG::32616|WGS 84 / UTM zone 16N|PreferredUsage<br>Usages|
|Projected:EPSG::32716|WGS 84 / UTM zone 16S|PreferredUsage<br>Usages|
|Projected:EPSG::32617|WGS 84 / UTM zone 17N|PreferredUsage<br>Usages|
|Projected:EPSG::32717|WGS 84 / UTM zone 17S|PreferredUsage<br>Usages|
|Projected:EPSG::32618|WGS 84 / UTM zone 18N|PreferredUsage<br>Usages|
|Projected:EPSG::32718|WGS 84 / UTM zone 18S|PreferredUsage<br>Usages|
|Projected:EPSG::32619|WGS 84 / UTM zone 19N|PreferredUsage<br>Usages|
|Projected:EPSG::32719|WGS 84 / UTM zone 19S|PreferredUsage<br>Usages|
|Projected:EPSG::32601|WGS 84 / UTM zone 1N|PreferredUsage<br>Usages|
|Projected:EPSG::32701|WGS 84 / UTM zone 1S|PreferredUsage<br>Usages|
|Projected:EPSG::32620|WGS 84 / UTM zone 20N|PreferredUsage<br>Usages|
|Projected:EPSG::32720|WGS 84 / UTM zone 20S|PreferredUsage<br>Usages|
|Projected:EPSG::32621|WGS 84 / UTM zone 21N|PreferredUsage<br>Usages|
|Projected:EPSG::32721|WGS 84 / UTM zone 21S|PreferredUsage<br>Usages|
|Projected:EPSG::32622|WGS 84 / UTM zone 22N|PreferredUsage<br>Usages|
|Projected:EPSG::32722|WGS 84 / UTM zone 22S|PreferredUsage<br>Usages|
|Projected:EPSG::32623|WGS 84 / UTM zone 23N|PreferredUsage<br>Usages|
|Projected:EPSG::32723|WGS 84 / UTM zone 23S|PreferredUsage<br>Usages|
|Projected:EPSG::32624|WGS 84 / UTM zone 24N|PreferredUsage<br>Usages|
|Projected:EPSG::32724|WGS 84 / UTM zone 24S|PreferredUsage<br>Usages|
|Projected:EPSG::32625|WGS 84 / UTM zone 25N|PreferredUsage<br>Usages|
|Projected:EPSG::32725|WGS 84 / UTM zone 25S|PreferredUsage<br>Usages|
|Projected:EPSG::32626|WGS 84 / UTM zone 26N|PreferredUsage<br>Usages|
|Projected:EPSG::32726|WGS 84 / UTM zone 26S|PreferredUsage<br>Usages|
|Projected:EPSG::32627|WGS 84 / UTM zone 27N|PreferredUsage<br>Usages|
|Projected:EPSG::32727|WGS 84 / UTM zone 27S|PreferredUsage<br>Usages|
|Projected:EPSG::32628|WGS 84 / UTM zone 28N|PreferredUsage<br>Usages|
|Projected:EPSG::32728|WGS 84 / UTM zone 28S|PreferredUsage<br>Usages|
|Projected:EPSG::32629|WGS 84 / UTM zone 29N|PreferredUsage<br>Usages|
|Projected:EPSG::32729|WGS 84 / UTM zone 29S|PreferredUsage<br>Usages|
|Projected:EPSG::32602|WGS 84 / UTM zone 2N|PreferredUsage<br>Usages|
|Projected:EPSG::32702|WGS 84 / UTM zone 2S|PreferredUsage<br>Usages|
|Projected:EPSG::32630|WGS 84 / UTM zone 30N|PreferredUsage<br>Usages|
|Projected:EPSG::32730|WGS 84 / UTM zone 30S|PreferredUsage<br>Usages|
|Projected:EPSG::32631|WGS 84 / UTM zone 31N|PreferredUsage<br>Usages|
|Projected:EPSG::32731|WGS 84 / UTM zone 31S|PreferredUsage<br>Usages|
|Projected:EPSG::32632|WGS 84 / UTM zone 32N|PreferredUsage<br>Usages|
|Projected:EPSG::32732|WGS 84 / UTM zone 32S|PreferredUsage<br>Usages|
|Projected:EPSG::32633|WGS 84 / UTM zone 33N|PreferredUsage<br>Usages|
|Projected:EPSG::32733|WGS 84 / UTM zone 33S|PreferredUsage<br>Usages|
|Projected:EPSG::32634|WGS 84 / UTM zone 34N|PreferredUsage<br>Usages|
|Projected:EPSG::32734|WGS 84 / UTM zone 34S|PreferredUsage<br>Usages|
|Projected:EPSG::32635|WGS 84 / UTM zone 35N|PreferredUsage<br>Usages|
|Projected:EPSG::32735|WGS 84 / UTM zone 35S|PreferredUsage<br>Usages|
|Projected:EPSG::32636|WGS 84 / UTM zone 36N|PreferredUsage<br>Usages|
|Projected:EPSG::32736|WGS 84 / UTM zone 36S|PreferredUsage<br>Usages|
|Projected:EPSG::32637|WGS 84 / UTM zone 37N|PreferredUsage<br>Usages|
|Projected:EPSG::32737|WGS 84 / UTM zone 37S|PreferredUsage<br>Usages|
|Projected:EPSG::32638|WGS 84 / UTM zone 38N|PreferredUsage<br>Usages|
|Projected:EPSG::32738|WGS 84 / UTM zone 38S|PreferredUsage<br>Usages|
|Projected:EPSG::32639|WGS 84 / UTM zone 39N|PreferredUsage<br>Usages|
|Projected:EPSG::32739|WGS 84 / UTM zone 39S|PreferredUsage<br>Usages|
|Projected:EPSG::32603|WGS 84 / UTM zone 3N|PreferredUsage<br>Usages|
|Projected:EPSG::32703|WGS 84 / UTM zone 3S|PreferredUsage<br>Usages|
|Projected:EPSG::32640|WGS 84 / UTM zone 40N|PreferredUsage<br>Usages|
|Projected:EPSG::32740|WGS 84 / UTM zone 40S|PreferredUsage<br>Usages|
|Projected:EPSG::32641|WGS 84 / UTM zone 41N|PreferredUsage<br>Usages|
|Projected:EPSG::32741|WGS 84 / UTM zone 41S|PreferredUsage<br>Usages|
|Projected:EPSG::32642|WGS 84 / UTM zone 42N|PreferredUsage<br>Usages|
|Projected:EPSG::32742|WGS 84 / UTM zone 42S|PreferredUsage<br>Usages|
|Projected:EPSG::32643|WGS 84 / UTM zone 43N|PreferredUsage<br>Usages|
|Projected:EPSG::32743|WGS 84 / UTM zone 43S|PreferredUsage<br>Usages|
|Projected:EPSG::32644|WGS 84 / UTM zone 44N|PreferredUsage<br>Usages|
|Projected:EPSG::32744|WGS 84 / UTM zone 44S|PreferredUsage<br>Usages|
|Projected:EPSG::32645|WGS 84 / UTM zone 45N|PreferredUsage<br>Usages|
|Projected:EPSG::32745|WGS 84 / UTM zone 45S|PreferredUsage<br>Usages|
|Projected:EPSG::32646|WGS 84 / UTM zone 46N|PreferredUsage<br>Usages|
|Projected:EPSG::32746|WGS 84 / UTM zone 46S|PreferredUsage<br>Usages|
|Projected:EPSG::32647|WGS 84 / UTM zone 47N|PreferredUsage<br>Usages|
|Projected:EPSG::32747|WGS 84 / UTM zone 47S|PreferredUsage<br>Usages|
|Projected:EPSG::32648|WGS 84 / UTM zone 48N|PreferredUsage<br>Usages|
|Projected:EPSG::32748|WGS 84 / UTM zone 48S|PreferredUsage<br>Usages|
|Projected:EPSG::32649|WGS 84 / UTM zone 49N|PreferredUsage<br>Usages|
|Projected:EPSG::32749|WGS 84 / UTM zone 49S|PreferredUsage<br>Usages|
|Projected:EPSG::32604|WGS 84 / UTM zone 4N|PreferredUsage<br>Usages|
|Projected:EPSG::32704|WGS 84 / UTM zone 4S|PreferredUsage<br>Usages|
|Projected:EPSG::32650|WGS 84 / UTM zone 50N|PreferredUsage<br>Usages|
|Projected:EPSG::32750|WGS 84 / UTM zone 50S|PreferredUsage<br>Usages|
|Projected:EPSG::32651|WGS 84 / UTM zone 51N|PreferredUsage<br>Usages|
|Projected:EPSG::32751|WGS 84 / UTM zone 51S|PreferredUsage<br>Usages|
|Projected:EPSG::32652|WGS 84 / UTM zone 52N|PreferredUsage<br>Usages|
|Projected:EPSG::32752|WGS 84 / UTM zone 52S|PreferredUsage<br>Usages|
|Projected:EPSG::32653|WGS 84 / UTM zone 53N|PreferredUsage<br>Usages|
|Projected:EPSG::32753|WGS 84 / UTM zone 53S|PreferredUsage<br>Usages|
|Projected:EPSG::32654|WGS 84 / UTM zone 54N|PreferredUsage<br>Usages|
|Projected:EPSG::32754|WGS 84 / UTM zone 54S|PreferredUsage<br>Usages|
|Projected:EPSG::32655|WGS 84 / UTM zone 55N|PreferredUsage<br>Usages|
|Projected:EPSG::32755|WGS 84 / UTM zone 55S|PreferredUsage<br>Usages|
|Projected:EPSG::32656|WGS 84 / UTM zone 56N|PreferredUsage<br>Usages|
|Projected:EPSG::32756|WGS 84 / UTM zone 56S|PreferredUsage<br>Usages|
|Projected:EPSG::32657|WGS 84 / UTM zone 57N|PreferredUsage<br>Usages|
|Projected:EPSG::32757|WGS 84 / UTM zone 57S|PreferredUsage<br>Usages|
|Projected:EPSG::32658|WGS 84 / UTM zone 58N|PreferredUsage<br>Usages|
|Projected:EPSG::32758|WGS 84 / UTM zone 58S|PreferredUsage<br>Usages|
|Projected:EPSG::32659|WGS 84 / UTM zone 59N|PreferredUsage<br>Usages|
|Projected:EPSG::32759|WGS 84 / UTM zone 59S|PreferredUsage<br>Usages|
|Projected:EPSG::32605|WGS 84 / UTM zone 5N|PreferredUsage<br>Usages|
|Projected:EPSG::32705|WGS 84 / UTM zone 5S|PreferredUsage<br>Usages|
|Projected:EPSG::32660|WGS 84 / UTM zone 60N|PreferredUsage<br>Usages|
|Projected:EPSG::32760|WGS 84 / UTM zone 60S|PreferredUsage<br>Usages|
|Projected:EPSG::32606|WGS 84 / UTM zone 6N|PreferredUsage<br>Usages|
|Projected:EPSG::32706|WGS 84 / UTM zone 6S|PreferredUsage<br>Usages|
|Projected:EPSG::32607|WGS 84 / UTM zone 7N|PreferredUsage<br>Usages|
|Projected:EPSG::32707|WGS 84 / UTM zone 7S|PreferredUsage<br>Usages|
|Projected:EPSG::32608|WGS 84 / UTM zone 8N|PreferredUsage<br>Usages|
|Projected:EPSG::32708|WGS 84 / UTM zone 8S|PreferredUsage<br>Usages|
|Projected:EPSG::32609|WGS 84 / UTM zone 9N|PreferredUsage<br>Usages|
|Projected:EPSG::32709|WGS 84 / UTM zone 9S|PreferredUsage<br>Usages|
|Vertical:EPSG::6647|CGVD2013(CGG2013) height|Description|
|Vertical:EPSG::5861|LAT depth|Description|
|Vertical:EPSG::5866|MLLW depth|Description|
|Vertical:EPSG::5714|MSL height|Description|
|Vertical:EPSG::5715|MSL depth|Description|
|Vertical:EPSG::8051|MSL depth (ft)|Description|
|Vertical:EPSG::8053|MSL depth (ftUS)|Description|
|Vertical:EPSG::8050|MSL height (ft)|Description|
|Vertical:EPSG::8052|MSL height (ftUS)|Description|
|Vertical:EPSG::5776|NN54 height|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Vertical:EPSG::5730|EVRF2000 height|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|Geographic2D:EPSG::4231|ED87|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4205_EPSG::1107|Afgooye * DMA-Som [4205,1107]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4289_EPSG::1672|Old: "Amersfoort * EPSG-Nld [4289,1672]"<br>New: "Amersfoort * EPSG-Nld 2000 [4289,1672]"|Name<br>PersistableReference|
|BoundGeographic2D:EPSG::4289_EPSG::4833|Old: "Amersfoort * OGP-Nld [4289,4833]"<br>New: "Amersfoort * OGP-Nld 2008 [4289,4833]"|Name<br>PersistableReference|
|BoundGeographic2D:EPSG::4214_EPSG::15918|Beijing 1954 * BGP-Chn Ord [4214,15918]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4314_EPSG::1777|DHDN * EPSG-Deu W 3m [4314,1777]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4230_EPSG::1133|ED50 * DMA-mean [4230,1133]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4230_EPSG::8047|ED50 * NMA-Nor N65 1991 [4230,8047]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4230_EPSG::1275|ED50 * IGN-Fra [4230,1275]|PreferredUsage<br>Usages|
|BoundGeographic2D:EPSG::4230_EPSG::1135|ED50 * DMA-midEast [4230,1135]|PreferredUsage<br>Usages|
|BoundGeographic2D:EPSG::4230_EPSG::1612|ED50 * EPSG-Nor N62 2001 [4230,1612]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4230_EPSG::1613|ED50 * EPSG-Nor S62 2001 [4230,1613]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4230_EPSG::1784|ED50 * EPSG-Tur [4230,1784]|PreferredUsage<br>Usages|
|BoundGeographic2D:EPSG::4230_EPSG::1075|ED50 * TPAO-Tur [4230,1075]|PreferredUsage<br>Usages|
|BoundGeographic2D:EPSG::4230_EPSG::1139|ED50 * DMA-Fin Nor [4230,1139]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4668_EPSG::15752|ED79 * NIMA-Eur [4668,15752]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4682_EPSG::15779|Old: "Gulshan 303 * SB-Bgd [4682,15779]"<br>New: "Gulshan 303 * OGP-Bgd rounded [4682,15779]"|Name<br>PersistableReference|
|BoundGeographic2D:EPSG::4267_EPSG::15851|NAD27 * OGP-Usa Conus [4267,15851]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4269_EPSG::1188|NAD83 * DMA-N Am [4269,1188]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4277_EPSG::1314|OSGB36 * UKOOA-Pet [4277,1314]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4277_EPSG::1195|OSGB36 * DMA-Gbr [4277,1195]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4277_EPSG::5339|OSGB36 * OGP-UK Gbr02 NT [4277,5339]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundGeographic2D:EPSG::4171_EPSG::1671|RGF93 v1 * EPSG-Fra [4171,1671]|PreferredUsage<br>Usages|
|BoundGeographic2D:EPSG::4618_EPSG::1877|Old: "SAD69 * IGBE-Bra [4618,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 [4618,1877]"|Name<br>PersistableReference|
|BoundGeographic2D:EPSG::4618_EPSG::5882|Old: "SAD69 * OGP-Bra [4618,5882]"<br>New: "SAD69 * OGP-Bra 5m 1994 [4618,5882]"|Name<br>PersistableReference|
|BoundGeographic2D:EPSG::4613_EPSG::1899|Segara * Shl-Idn Kal NE [4613,1899]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::28992_EPSG::1672|Old: "Amersfoort * EPSG-Nld / RD New [28992,1672]"<br>New: "Amersfoort * EPSG-Nld 2000 / RD New [28992,1672]"|Name<br>PersistableReference|
|BoundProjected:EPSG::28992_EPSG::4833|Old: "Amersfoort * OGP-Nld / RD New [28992,4833]"<br>New: "Amersfoort * OGP-Nld 2008 / RD New [28992,4833]"|Name<br>PersistableReference|
|BoundProjected:EPSG::31466_EPSG::1777|DHDN * EPSG-Deu W 3m / 3-degree Gauss-Kruger zone 2 [31466,1777]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::2319_EPSG::1784|ED50 * EPSG-Tur / TM27 [2319,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::2320_EPSG::1784|ED50 * EPSG-Tur / TM30 [2320,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::2321_EPSG::1784|ED50 * EPSG-Tur / TM33 [2321,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::2322_EPSG::1784|ED50 * EPSG-Tur / TM36 [2322,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::2323_EPSG::1784|ED50 * EPSG-Tur / TM39 [2323,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::2324_EPSG::1784|ED50 * EPSG-Tur / TM42 [2324,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::2325_EPSG::1784|ED50 * EPSG-Tur / TM45 [2325,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23030_EPSG::1275|ED50 * IGN-Fra / UTM zone 30N [23030,1275]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23031_EPSG::1133|ED50 * DMA-mean / UTM zone 31N [23031,1133]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23031_EPSG::1275|ED50 * IGN-Fra / UTM zone 31N [23031,1275]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23031_EPSG::1613|ED50 * EPSG-Nor S62 2001 / UTM zone 31N [23031,1613]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23032_EPSG::1133|ED50 * DMA-mean / UTM zone 32N [23032,1133]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23032_EPSG::8047|ED50 * NMA-Nor N65 1991 / UTM zone 32N [23032,8047]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23032_EPSG::1612|ED50 * EPSG-Nor N62 2001 / UTM zone 32N [23032,1612]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23032_EPSG::1613|ED50 * EPSG-Nor S62 2001 / UTM zone 32N [23032,1613]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23032_EPSG::1627|ED50 * EPSG-Dnk / UTM zone 32N [23032,1627]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23032_EPSG::1810|ED50 * wgc72-Egy / UTM zone 32N [23032,1810]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23033_EPSG::1133|ED50 * DMA-mean / UTM zone 33N [23033,1133]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23033_EPSG::1612|ED50 * EPSG-Nor N62 2001 / UTM zone 33N [23033,1612]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23033_EPSG::1627|ED50 * EPSG-Dnk / UTM zone 33N [23033,1627]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23033_EPSG::1810|ED50 * wgc72-Egy / UTM zone 33N [23033,1810]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23034_EPSG::1133|ED50 * DMA-mean / UTM zone 34N [23034,1133]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23034_EPSG::1144|ED50 * DMA-Mlt / UTM zone 34N [23034,1144]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23034_EPSG::1612|ED50 * EPSG-Nor N62 2001 / UTM zone 34N [23034,1612]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23034_EPSG::1139|ED50 * DMA-Fin Nor / UTM zone 34N [23034,1139]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23035_EPSG::1133|ED50 * DMA-mean / UTM zone 35N [23035,1133]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23035_EPSG::8047|ED50 * NMA-Nor N65 1991 / UTM zone 35N [23035,8047]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23035_EPSG::1440|ED50 * HEL-Grc / UTM zone 35N [23035,1440]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23035_EPSG::1134|ED50 * DMA-cenEur / UTM zone 35N [23035,1134]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23035_EPSG::1612|ED50 * EPSG-Nor N62 2001 / UTM zone 35N [23035,1612]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23035_EPSG::1784|ED50 * EPSG-Tur / UTM zone 35N [23035,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23035_EPSG::1075|ED50 * TPAO-Tur / UTM zone 35N [23035,1075]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23036_EPSG::1133|ED50 * DMA-mean / UTM zone 36N [23036,1133]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23036_EPSG::1612|ED50 * EPSG-Nor N62 2001 / UTM zone 36N [23036,1612]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23036_EPSG::1784|ED50 * EPSG-Tur / UTM zone 36N [23036,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23036_EPSG::1810|ED50 * wgc72-Egy / UTM zone 36N [23036,1810]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23036_EPSG::1075|ED50 * TPAO-Tur / UTM zone 36N [23036,1075]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23037_EPSG::1133|ED50 * DMA-mean / UTM zone 37N [23037,1133]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23037_EPSG::1612|ED50 * EPSG-Nor N62 2001 / UTM zone 37N [23037,1612]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::23037_EPSG::1784|ED50 * EPSG-Tur / UTM zone 37N [23037,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23037_EPSG::1087|ED50 * RJGC-Jor / UTM zone 37N [23037,1087]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23037_EPSG::1075|ED50 * TPAO-Tur / UTM zone 37N [23037,1075]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23038_EPSG::1133|ED50 * DMA-mean / UTM zone 38N [23038,1133]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23038_EPSG::1135|ED50 * DMA-midEast / UTM zone 38N [23038,1135]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::23038_EPSG::1784|ED50 * EPSG-Tur / UTM zone 38N [23038,1784]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::6070_EPSG::1149|ETRS89 * EPSG-eur / EPSG Arctic zone 3-11 [6070,1149]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::25832_EPSG::1149|ETRS89 * EPSG-eur / UTM zone 32N [25832,1149]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::25833_EPSG::1149|ETRS89 * EPSG-eur / UTM zone 33N [25833,1149]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::28357_EPSG::1150|GDA94 * EPSG-Aus / MGA zone 57 [28357,1150]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::3106_EPSG::15779|Old: "Gulshan 303 * SB-Bgd / TM 90 NE [3106,15779]"<br>New: "Gulshan 303 * OGP-Bgd rounded / TM 90 NE [3106,15779]"|Name<br>PersistableReference|
|BoundProjected:EPSG::4409_EPSG::15851|NAD27 * OGP-Usa Conus / BLM 9N (ftUS) [4409,15851]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::32016_EPSG::15851|NAD27 * OGP-Usa Conus / New York Central [32016,15851]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::32041_EPSG::15851|NAD27 * OGP-Usa Conus / Texas South [32041,15851]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::26710_EPSG::1172|NAD27 * DMA-Can / UTM zone 10N [26710,1172]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::26710_EPSG::1693|NAD27 * EPSG-Can / UTM zone 10N [26710,1693]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::26720_EPSG::1172|NAD27 * DMA-Can / UTM zone 20N [26720,1172]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::26720_EPSG::1693|NAD27 * EPSG-Can / UTM zone 20N [26720,1693]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::26722_EPSG::1172|NAD27 * DMA-Can / UTM zone 22N [26722,1172]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::26722_EPSG::1693|NAD27 * EPSG-Can / UTM zone 22N [26722,1693]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::26722_EPSG::1173|NAD27 * DMA-Conus / UTM zone 22N [26722,1173]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::26922_EPSG::1188|NAD83 * DMA-N Am / UTM zone 22N [26922,1188]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::27700_EPSG::1314|OSGB36 * UKOOA-Pet / British National Grid [27700,1314]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::27700_EPSG::1195|OSGB36 * DMA-Gbr / British National Grid [27700,1195]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::27700_EPSG::5339|OSGB36 * OGP-UK Gbr02 NT / British National Grid [27700,5339]|PreferredUsage<br>Usages<br>Wgs84Coordinates|
|BoundProjected:EPSG::2596_EPSG::1807|Old: "Pulkovo 1942 * BP-Aze Aioc95 / 3-degree Gauss-Kruger CM 63E [2596,1807]"<br>New: "Record removed"|Record removed|
|BoundProjected:EPSG::2596_EPSG::1808|Old: "Pulkovo 1942 * BP-Aze Aioc97 / 3-degree Gauss-Kruger CM 63E [2596,1808]"<br>New: "Record removed"|Record removed|
|BoundProjected:EPSG::2154_EPSG::1671|RGF93 v1 * EPSG-Fra / Lambert-93 [2154,1671]|PreferredUsage<br>Usages|
|BoundProjected:EPSG::29101_EPSG::1877|Old: "SAD69 * IGBE-Bra / Brazil Polyconic [29101,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 / Brazil Polyconic [29101,1877]"|Name<br>PersistableReference|
|BoundProjected:EPSG::29188_EPSG::1877|Old: "SAD69 * IGBE-Bra / UTM zone 18S [29188,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 / UTM zone 18S [29188,1877]"|Name<br>PersistableReference|
|BoundProjected:EPSG::29190_EPSG::1877|Old: "SAD69 * IGBE-Bra / UTM zone 20S [29190,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 / UTM zone 20S [29190,1877]"|Name<br>PersistableReference|
|BoundProjected:EPSG::29191_EPSG::1877|Old: "SAD69 * IGBE-Bra / UTM zone 21S [29191,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 / UTM zone 21S [29191,1877]"|Name<br>PersistableReference|
|BoundProjected:EPSG::29172_EPSG::1877|Old: "SAD69 * IGBE-Bra / UTM zone 22N [29172,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 / UTM zone 22N [29172,1877]"|Name<br>PersistableReference|
|BoundProjected:EPSG::29192_EPSG::1877|Old: "SAD69 * IGBE-Bra / UTM zone 22S [29192,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 / UTM zone 22S [29192,1877]"|Name<br>PersistableReference|
|BoundProjected:EPSG::29193_EPSG::1877|Old: "SAD69 * IGBE-Bra / UTM zone 23S [29193,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 / UTM zone 23S [29193,1877]"|Name<br>PersistableReference|
|BoundProjected:EPSG::29194_EPSG::1877|Old: "SAD69 * IGBE-Bra / UTM zone 24S [29194,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 / UTM zone 24S [29194,1877]"|Name<br>PersistableReference|
|BoundProjected:EPSG::29194_EPSG::5882|Old: "SAD69 * OGP-Bra / UTM zone 24S [29194,5882]"<br>New: "SAD69 * OGP-Bra 5m 1994 / UTM zone 24S [29194,5882]"|Name<br>PersistableReference|
|BoundProjected:EPSG::29195_EPSG::1877|Old: "SAD69 * IGBE-Bra / UTM zone 25S [29195,1877]"<br>New: "SAD69 * IBGE-Bra 5m 1989 / UTM zone 25S [29195,1877]"|Name<br>PersistableReference|

[Back to TOC](#TOC)
