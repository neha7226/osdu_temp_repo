<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractSpatialLocation [Status: Accepted]
A geographic object which can be described by a set of points.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractSpatialLocation:1.1.0`
* Schema status: PUBLISHED
* **Migration Guide (M12)** [`osdu:wks:AbstractSpatialLocation:1.0.0` &rarr; `osdu:wks:AbstractSpatialLocation:1.1.0`](../../Guides/MigrationGuides/M12/AbstractSpatialLocation.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractSpatialLocation.1.1.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractSpatialLocation.1.1.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractSpatialLocation.1.1.0.json](../../Authoring/abstract/AbstractSpatialLocation.1.1.0.json)
* Link to &rarr; [Generated Schema AbstractSpatialLocation.1.1.0.json](../../Generated/abstract/AbstractSpatialLocation.1.1.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractSpatialLocation.1.1.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractSpatialLocation.1.1.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractSpatialLocation


## Outgoing Relationships for AbstractSpatialLocation

![AbstractSpatialLocation](../_diagrams/abstract/AbstractSpatialLocation.1.1.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractSpatialLocation Referenced by other Entities

![AbstractSpatialLocation](../_diagrams/abstract/AbstractSpatialLocation.1.1.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractSpatialLocation Properties

## Table of AbstractSpatialLocation Properties (Version 1.1.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|SpatialLocationCoordinatesDate| |string|optional|Spatial Location Coordinates Date|Date when coordinates were measured or retrieved.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|QuantitativeAccuracyBandID &rarr; [QuantitativeAccuracyBand](../reference-data/QuantitativeAccuracyBand.1.0.0.md)| |string|optional|Quantitative Accuracy Band Id|An approximate quantitative assessment of the quality of a location (accurate to > 500 m (i.e. not very accurate)), to < 1 m, etc.|<code>^[\w\-\.]+:reference-data\-\-QuantitativeAccuracyBand:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [QuantitativeAccuracyBand](../reference-data/QuantitativeAccuracyBand.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|QualitativeSpatialAccuracyTypeID &rarr; [QualitativeSpatialAccuracyType](../reference-data/QualitativeSpatialAccuracyType.1.0.0.md)| |string|optional|Qualitative Spatial Accuracy Type Id|A qualitative description of the quality of a spatial location, e.g. unverifiable, not verified, basic validation.|<code>^[\w\-\.]+:reference-data\-\-QualitativeSpatialAccuracyType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [QualitativeSpatialAccuracyType](../reference-data/QualitativeSpatialAccuracyType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|CoordinateQualityCheckPerformedBy| |string|optional|Coordinate Quality Check Performed By|The user who performed the Quality Check.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|CoordinateQualityCheckDateTime| |string|optional|Coordinate Quality Check Date Time|The date of the Quality Check.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|CoordinateQualityCheckRemarks[]| |string|optional|Coordinate Quality Check Remarks|Freetext remarks on Quality Check.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_AsIngestedCoordinates_|_[AbstractAnyCrsFeatureCollection.1.1.0](AbstractAnyCrsFeatureCollection.1.1.0.md) (nested structure, follow link for details)_|_object_|_optional_|_AbstractAnyCrsFeatureCollection_|_The original or 'as ingested' coordinates (Point, MultiPoint, LineString, MultiLineString, Polygon or MultiPolygon). The name 'AsIngestedCoordinates' was chosen to contrast it to 'OriginalCoordinates', which carries the uncertainty whether any coordinate operations took place before ingestion. In cases where the original CRS is different from the as-ingested CRS, the AppliedOperations can also contain the list of operations applied to the coordinate prior to ingestion. The data structure is similar to GeoJSON FeatureCollection, however in a CRS context explicitly defined within the AbstractAnyCrsFeatureCollection. The coordinate sequence follows GeoJSON standard, i.e. 'eastward/longitude', 'northward/latitude' {, 'upward/height' unless overridden by an explicit direction in the AsIngestedCoordinates.VerticalCoordinateReferenceSystemID}. Fragment Description: A schema like GeoJSON FeatureCollection with a non-WGS 84 CRS context; based on https://geojson.org/schema/FeatureCollection.json. Attention: the coordinate order is fixed: Longitude/Easting/Westing/X first, followed by Latitude/Northing/Southing/Y, optionally height as third coordinate._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|_Wgs84Coordinates_|_[AbstractFeatureCollection.1.0.0](AbstractFeatureCollection.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_GeoJSON FeatureCollection_|_The normalized coordinates (Point, MultiPoint, LineString, MultiLineString, Polygon or MultiPolygon) based on WGS 84 (EPSG:4326 for 2-dimensional coordinates, EPSG:4326 + EPSG:5714 (MSL) for 3-dimensional coordinates). This derived coordinate representation is intended for global discoverability only. The schema of this substructure is identical to the GeoJSON FeatureCollection https://geojson.org/schema/FeatureCollection.json. The coordinate sequence follows GeoJSON standard, i.e. longitude, latitude {, height} Fragment Description: GeoJSON feature collection as originally published in https://geojson.org/schema/FeatureCollection.json. Attention: the coordinate order is fixed: Longitude first, followed by Latitude, optionally height above MSL (EPSG:5714) as third coordinate._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|AppliedOperations[]| |string|optional|Operations Applied|The audit trail of operations applied to the coordinates from the original state to the current state. The list may contain operations applied prior to ingestion as well as the operations applied to produce the Wgs84Coordinates. The text elements refer to ESRI style CRS and Transformation names, which may have to be translated to EPSG standard names.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|["conversion from ED_1950_UTM_Zone_31N to GCS_European_1950; 1 points converted", "transformation GCS_European_1950 to GCS_WGS_1984 using ED_1950_To_WGS_1984_24; 1 points successfully transformed"]|
|SpatialParameterTypeID &rarr; [SpatialParameterType](../reference-data/SpatialParameterType.1.0.0.md)| |string|optional|Spatial Parameter Type Id|A type of spatial representation of an object, often general (e.g. an Outline, which could be applied to Field, Reservoir, Facility, etc.) or sometimes specific (e.g. Onshore Outline, State Offshore Outline, Federal Offshore Outline, 3 spatial representations that may be used by Countries).|<code>^[\w\-\.]+:reference-data\-\-SpatialParameterType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SpatialParameterType](../reference-data/SpatialParameterType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SpatialGeometryTypeID &rarr; [SpatialGeometryType](../reference-data/SpatialGeometryType.1.0.0.md)| |string|optional|Spatial Geometry Type Id|Indicates the expected look of the SpatialParameterType, e.g. Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon. The value constrains the type of geometries in the GeoJSON Wgs84Coordinates and AsIngestedCoordinates.|<code>^[\w\-\.]+:reference-data\-\-SpatialGeometryType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SpatialGeometryType](../reference-data/SpatialGeometryType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractSpatialLocation.1.1.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractBinGrid.1.1.0](AbstractBinGrid.1.1.0.md) | ABCDBinGridSpatialLocation | one |  |
| Included by | [AbstractMaster.1.1.0](AbstractMaster.1.1.0.md) | SpatialLocation | one |  |
| Included by | [AbstractWorkProductComponent.1.1.0](AbstractWorkProductComponent.1.1.0.md) | SpatialPoint | one |  |
| Included by | [AbstractWorkProductComponent.1.1.0](AbstractWorkProductComponent.1.1.0.md) | SpatialArea | one |  |
| Included by | [SeismicTraceData.1.2.0](../work-product-component/SeismicTraceData.1.2.0.md) | LiveTraceOutline | one |  |
| Included by | [SeismicTraceData.1.3.0](../work-product-component/SeismicTraceData.1.3.0.md) | LiveTraceOutline | one |  |
| Included by | [Wellbore.1.1.0](../master-data/Wellbore.1.1.0.md) | ProjectedBottomHoleLocation | one |  |
| Included by | [Wellbore.1.1.0](../master-data/Wellbore.1.1.0.md) | GeographicBottomHoleLocation | one |  |
| Included by | [Wellbore.1.1.1](../master-data/Wellbore.1.1.1.md) | ProjectedBottomHoleLocation | one |  |
| Included by | [Wellbore.1.1.1](../master-data/Wellbore.1.1.1.md) | GeographicBottomHoleLocation | one |  |
| Included by | [Wellbore.1.2.0](../master-data/Wellbore.1.2.0.md) | ProjectedBottomHoleLocation | one |  |
| Included by | [Wellbore.1.2.0](../master-data/Wellbore.1.2.0.md) | GeographicBottomHoleLocation | one |  |
| Included by | [Wellbore.1.3.0](../master-data/Wellbore.1.3.0.md) | ProjectedBottomHoleLocation | one |  |
| Included by | [Wellbore.1.3.0](../master-data/Wellbore.1.3.0.md) | GeographicBottomHoleLocation | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._