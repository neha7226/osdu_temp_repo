<a name="TOC"></a>

[[_TOC_]]

# Migration to `osdu:wks:master-data--Well:1.2.0`

[`osdu:wks:master-data--Well:1.2.0`](../../../E-R/master-data/Well.1.2.0.md).  

* `Well:1.2.0` includes `AbstractFacility:1.1.0` including `AbstractFacilityEvent:1.1.0`, 
   `AbstractFacilityOperator:1.1.0` and `AbstractFacilityState:1.1.0`. No migration effort expected (new properties).

[Back to TOC](#TOC)

---

# Links

* [Back to Milestone](README.md)
* [Back to Migration Guides](../README.md)
* [Back to Guides](../../README.md)
