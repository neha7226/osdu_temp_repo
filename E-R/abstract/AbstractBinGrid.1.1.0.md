<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractBinGrid [Status: Accepted]
The shared properties for a bin grid.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractBinGrid:1.1.0`
* Schema status: PUBLISHED
* **Migration Guide (M15)** [`osdu:wks:AbstractBinGrid:1.0.0` &rarr; `osdu:wks:AbstractBinGrid:1.1.0`](../../Guides/MigrationGuides/M15/AbstractBinGrid.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractBinGrid.1.1.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractBinGrid.1.1.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractBinGrid.1.1.0.json](../../Authoring/abstract/AbstractBinGrid.1.1.0.json)
* Link to &rarr; [Generated Schema AbstractBinGrid.1.1.0.json](../../Generated/abstract/AbstractBinGrid.1.1.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractBinGrid.1.1.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractBinGrid.1.1.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractBinGrid


## Outgoing Relationships for AbstractBinGrid

![AbstractBinGrid](../_diagrams/abstract/AbstractBinGrid.1.1.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractBinGrid Referenced by other Entities

![AbstractBinGrid](../_diagrams/abstract/AbstractBinGrid.1.1.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractBinGrid Properties

## Table of AbstractBinGrid Properties (Version 1.1.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|BinGridName| |string|optional|Bin Grid Name|Name of bin grid (e.g., GEOCO_GREENCYN_PHV_2012).  Probably the name as it exists in a separate corporate store if OSDU is not main system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|BinGridTypeID &rarr; [SeismicBinGridType](../reference-data/SeismicBinGridType.1.0.0.md)| |string|optional|Bin Grid Type Id|Type of bin grid (Acquisition, Processing, Velocity, MagGrav, Magnetics, Gravity, GeologicModel, Reprojected, etc.)|<code>^[\w\-\.]+:reference-data\-\-SeismicBinGridType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SeismicBinGridType](../reference-data/SeismicBinGridType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SourceBinGridID| |integer|optional|Source Bin Grid Id|Identifier of the source bin grid as stored in a corporate database/application if OSDU is not main system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SourceBinGridAppID| |string|optional|Source Bin Grid App Id|Identifier (name) of the corporate database/application that stores the source bin grid definitions if OSDU is not main system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|CoveragePercent| |number|optional|Coverage Percent|Nominal design fold as intended by the bin grid definition, expressed as the mode in percentage points (60 fold = 6000%).|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|BinGridDefinitionMethodTypeID &rarr; [BinGridDefinitionMethodType](../reference-data/BinGridDefinitionMethodType.1.0.0.md)| |string|optional|Bin Grid Definition Method Type Id|This identifies how the Bin Grid is defined:  4=ABCD four-points method was used to define the grid (P6 parameters are optional and can contain derived values; P6BinNodeIncrementOnIAxis and P6BinNodeIncrementOnJaxis can be used as part of four-point method).  Use a perspective transformation to map between map coordinates and bin coordinates. Note point order.  6=P6 definition method was used to define the bin grid (ABCD points are optional and can contain derived values; ABCDBinGridSpatialLocation must specify the projected CRS).  Use an affine transformation to map between map coordinates and bin coordinates.|<code>^[\w\-\.]+:reference-data\-\-BinGridDefinitionMethodType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [BinGridDefinitionMethodType](../reference-data/BinGridDefinitionMethodType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|~~_ABCDBinGridLocalCoordinates[]_~~|~~_[AbstractCoordinates.1.0.0](AbstractCoordinates.1.0.0.md)_~~|~~_object_~~|~~_optional_~~|~~_AbstractCoordinates_~~|~~_DEPRECATED: Use  AbstractGeoJson.PropertiesBinGridCorners properties inside the ABCDBinGridSpatialLocation. Previously:  Array of 4 corner points for bin grid in local coordinates: Point A (min inline, min crossline); Point B (min inline, max crossline); Point C (max inline, min crossline); Point D (max inline, max crossline).  If Point D is not given and BinGridDefinitionMethodTypeID=4, it must be supplied, with its spatial location, before ingestion to create a parallelogram in map coordinate space.  Note correspondence of inline=x, crossline=y. Fragment Description: A geographic position on the surface of the earth._~~|~~_(No pattern)_~~|~~_(No format)_~~|~~_(No frame of reference)_~~|~~_object (default)_~~|~~_(Not derived)_~~|~~_(No natural key)_~~|~~_(No example)_~~|
|~~ABCDBinGridLocalCoordinates[].X~~|~~[AbstractCoordinates.1.0.0](AbstractCoordinates.1.0.0.md)~~|~~number~~|~~optional~~|~~X~~|~~x is Easting or Longitude.~~|~~(No pattern)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|~~ABCDBinGridLocalCoordinates[].Y~~|~~[AbstractCoordinates.1.0.0](AbstractCoordinates.1.0.0.md)~~|~~number~~|~~optional~~|~~Y~~|~~y is Northing or Latitude.~~|~~(No pattern)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|_ABCDBinGridSpatialLocation_|_[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)_|_object_|_optional_|_AbstractSpatialLocation_|_Bin Grid ABCD points containing the projected coordinates, projected CRS and quality metadata.  This attribute is required also for the P6 definition method to define the projected CRS, even if the ABCD coordinates would be optional (recommended to be always calculated). It is recommended to populate the GeoJSON/AnyCrsGeoJSON with properties according to the AbstractGeoJson.PropertiesBinGridCorners schema fragment. Fragment Description: A geographic object which can be described by a set of points._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ABCDBinGridSpatialLocation.SpatialLocationCoordinatesDate|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Spatial Location Coordinates Date|Date when coordinates were measured or retrieved.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ABCDBinGridSpatialLocation.QuantitativeAccuracyBandID &rarr; [QuantitativeAccuracyBand](../reference-data/QuantitativeAccuracyBand.1.0.0.md)|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Quantitative Accuracy Band Id|An approximate quantitative assessment of the quality of a location (accurate to > 500 m (i.e. not very accurate)), to < 1 m, etc.|<code>^[\w\-\.]+:reference-data\-\-QuantitativeAccuracyBand:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [QuantitativeAccuracyBand](../reference-data/QuantitativeAccuracyBand.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ABCDBinGridSpatialLocation.QualitativeSpatialAccuracyTypeID &rarr; [QualitativeSpatialAccuracyType](../reference-data/QualitativeSpatialAccuracyType.1.0.0.md)|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Qualitative Spatial Accuracy Type Id|A qualitative description of the quality of a spatial location, e.g. unverifiable, not verified, basic validation.|<code>^[\w\-\.]+:reference-data\-\-QualitativeSpatialAccuracyType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [QualitativeSpatialAccuracyType](../reference-data/QualitativeSpatialAccuracyType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ABCDBinGridSpatialLocation.CoordinateQualityCheckPerformedBy|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Coordinate Quality Check Performed By|The user who performed the Quality Check.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ABCDBinGridSpatialLocation.CoordinateQualityCheckDateTime|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Coordinate Quality Check Date Time|The date of the Quality Check.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ABCDBinGridSpatialLocation.CoordinateQualityCheckRemarks[]|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Coordinate Quality Check Remarks|Freetext remarks on Quality Check.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_ABCDBinGridSpatialLocation.AsIngestedCoordinates_|_[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md) [AbstractAnyCrsFeatureCollection.1.1.0](AbstractAnyCrsFeatureCollection.1.1.0.md) (nested structure, follow link for details)_|_object_|_optional_|_AbstractAnyCrsFeatureCollection_|_The original or 'as ingested' coordinates (Point, MultiPoint, LineString, MultiLineString, Polygon or MultiPolygon). The name 'AsIngestedCoordinates' was chosen to contrast it to 'OriginalCoordinates', which carries the uncertainty whether any coordinate operations took place before ingestion. In cases where the original CRS is different from the as-ingested CRS, the AppliedOperations can also contain the list of operations applied to the coordinate prior to ingestion. The data structure is similar to GeoJSON FeatureCollection, however in a CRS context explicitly defined within the AbstractAnyCrsFeatureCollection. The coordinate sequence follows GeoJSON standard, i.e. 'eastward/longitude', 'northward/latitude' {, 'upward/height' unless overridden by an explicit direction in the AsIngestedCoordinates.VerticalCoordinateReferenceSystemID}. Fragment Description: A schema like GeoJSON FeatureCollection with a non-WGS 84 CRS context; based on https://geojson.org/schema/FeatureCollection.json. Attention: the coordinate order is fixed: Longitude/Easting/Westing/X first, followed by Latitude/Northing/Southing/Y, optionally height as third coordinate._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|_ABCDBinGridSpatialLocation.Wgs84Coordinates_|_[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md) [AbstractFeatureCollection.1.0.0](AbstractFeatureCollection.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_GeoJSON FeatureCollection_|_The normalized coordinates (Point, MultiPoint, LineString, MultiLineString, Polygon or MultiPolygon) based on WGS 84 (EPSG:4326 for 2-dimensional coordinates, EPSG:4326 + EPSG:5714 (MSL) for 3-dimensional coordinates). This derived coordinate representation is intended for global discoverability only. The schema of this substructure is identical to the GeoJSON FeatureCollection https://geojson.org/schema/FeatureCollection.json. The coordinate sequence follows GeoJSON standard, i.e. longitude, latitude {, height} Fragment Description: GeoJSON feature collection as originally published in https://geojson.org/schema/FeatureCollection.json. Attention: the coordinate order is fixed: Longitude first, followed by Latitude, optionally height above MSL (EPSG:5714) as third coordinate._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ABCDBinGridSpatialLocation.AppliedOperations[]|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Operations Applied|The audit trail of operations applied to the coordinates from the original state to the current state. The list may contain operations applied prior to ingestion as well as the operations applied to produce the Wgs84Coordinates. The text elements refer to ESRI style CRS and Transformation names, which may have to be translated to EPSG standard names.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|["conversion from ED_1950_UTM_Zone_31N to GCS_European_1950; 1 points converted", "transformation GCS_European_1950 to GCS_WGS_1984 using ED_1950_To_WGS_1984_24; 1 points successfully transformed"]|
|ABCDBinGridSpatialLocation.SpatialParameterTypeID &rarr; [SpatialParameterType](../reference-data/SpatialParameterType.1.0.0.md)|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Spatial Parameter Type Id|A type of spatial representation of an object, often general (e.g. an Outline, which could be applied to Field, Reservoir, Facility, etc.) or sometimes specific (e.g. Onshore Outline, State Offshore Outline, Federal Offshore Outline, 3 spatial representations that may be used by Countries).|<code>^[\w\-\.]+:reference-data\-\-SpatialParameterType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SpatialParameterType](../reference-data/SpatialParameterType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ABCDBinGridSpatialLocation.SpatialGeometryTypeID &rarr; [SpatialGeometryType](../reference-data/SpatialGeometryType.1.0.0.md)|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Spatial Geometry Type Id|Indicates the expected look of the SpatialParameterType, e.g. Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon. The value constrains the type of geometries in the GeoJSON Wgs84Coordinates and AsIngestedCoordinates.|<code>^[\w\-\.]+:reference-data\-\-SpatialGeometryType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SpatialGeometryType](../reference-data/SpatialGeometryType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6TransformationMethod| |integer|optional|P6 Transformation Method|EPSG code: 9666 for right-handed, 1049 for left-handed.  See IOGP Guidance Note 373-07-2 and 483-6.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6BinGridOriginI| |number|optional|P6 Bin Grid Origin I|Inline coordinate of tie point (e.g., center or A point)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6BinGridOriginJ| |number|optional|P6 Bin Grid Origin J|Crossline coordinate of tie point (e.g., center or A point)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6BinGridOriginEasting| |number|optional|P6 Bin Grid Origin Easting|Easting coordinate of tie point (e.g., center or A point)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6BinGridOriginNorthing| |number|optional|P6 Bin Grid Origin Northing|Northing coordinate of tie point (e.g., center or A point)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6ScaleFactorOfBinGrid| |number|optional|P6 Scale Factor Of Bin Grid|Scale factor for Bin Grid.  If not provided then 1 is assumed. Unit is unity.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6BinWidthOnIaxis| |number|optional|P6 Bin Width On Iaxis|Distance between two inlines at the given increment apart, e.g., 30 m with P6BinNodeIncrementOnIaxis=1.  Unit from projected CRS in ABCDBinGridSpatialLocation|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6BinWidthOnJaxis| |number|optional|P6 Bin Width On Jaxis|Distance between two crosslines at the given increment apart, e.g., 25 m with P6BinNodeIncrementOnJaxis=4.  Unit from projected CRS in ABCDBinGridSpatialLocation|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6MapGridBearingOfBinGridJaxis| |number|optional|P6 Map Grid Bearing Of Bin Grid Jaxis|Clockwise angle from grid north (in projCRS) in degrees from 0 to 360 of the direction of increasing crosslines (constant inline), i.e., of the vector from point A to B.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6BinNodeIncrementOnIaxis| |integer|optional|P6 Bin Node Increment On Iaxis|Increment (positive integer) for the inline coordinate. If not provided then 1 is assumed.  The bin grid definition is expected to have increment 1 and the increment stored with the SeismicTraceData (“inline increment”) takes precedence over the increment set at the BinGrid.  Alternatively the increments are allowed to be defined with the BinGrid, but this should be avoided to allow for variations in sampling in trace data sets.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|P6BinNodeIncrementOnJaxis| |integer|optional|P6 Bin Node Increment On Jaxis|Increment (positive integer) for the crossline coordinate. If not provided then 1 is assumed.  The bin grid definition is expected to have increment 1 and the increment stored with the SeismicTraceData (“crossline increment”) takes precedence over the increment set at the BinGrid. Alternatively the increments are allowed to be defined with the BinGrid, but this should be avoided to allow for variations in sampling in trace data sets.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractBinGrid.1.1.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [GeoReferencedImage.1.1.0](../work-product-component/GeoReferencedImage.1.1.0.md) | EmbeddedBinGrid | one |  |
| Included by | [SeismicBinGrid.1.1.0](../work-product-component/SeismicBinGrid.1.1.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._