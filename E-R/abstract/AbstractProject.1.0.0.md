<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractProject [Status: Accepted]
A Project is a business activity that consumes financial and human resources and produces (digital) work products.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractProject:1.0.0`
* Schema status: PUBLISHED
* **Migration Guide (M10)** [`osdu:wks:AbstractProject:1.0.0`](../../Guides/MigrationGuides/M10/AbstractProject.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractProject.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractProject.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractProject.1.0.0.json](../../Authoring/abstract/AbstractProject.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractProject.1.0.0.json](../../Generated/abstract/AbstractProject.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractProject.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractProject.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractProject


## Outgoing Relationships for AbstractProject

![AbstractProject](../_diagrams/abstract/AbstractProject.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractProject Referenced by other Entities

![AbstractProject](../_diagrams/abstract/AbstractProject.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractProject Properties

## Table of AbstractProject Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|ProjectID| |string|optional|External Project Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectName| |string|optional|Project Name|The common or preferred name of a Project.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|~~_ProjectNames[]_~~|~~_[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)_~~|~~_object_~~|~~_optional_~~|~~_AbstractAliasNames_~~|~~_DEPRECATED: please use data.NameAliases. The history of Project names, codes, and other business identifiers. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._~~|~~_(No pattern)_~~|~~_(No format)_~~|~~_(No frame of reference)_~~|~~_object (default)_~~|~~_(Not derived)_~~|~~_(No natural key)_~~|~~_(No example)_~~|
|~~ProjectNames[].AliasName~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Alias Name~~|~~Alternative Name value of defined name type for an object.~~|~~(No pattern)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|~~ProjectNames[].AliasNameTypeID &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Alias Name Type Id~~|~~A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.~~|~~<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|~~ProjectNames[].DefinitionOrganisationID &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Definition Organisation Id~~|~~The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).~~|~~<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|~~ProjectNames[].EffectiveDateTime~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Effective Date Time~~|~~The date and time when an alias name becomes effective.~~|~~(No pattern)~~|~~date-time~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|~~ProjectNames[].TerminationDateTime~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Termination Date Time~~|~~The data and time when an alias name is no longer in effect.~~|~~(No pattern)~~|~~date-time~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|Purpose| |string|optional|Purpose|Description of the objectives of a Project.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectBeginDate| |string|optional|Project Begin Date|The date and time when the Project was initiated.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectEndDate| |string|optional|Project End Date|The date and time when the Project was completed.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_FundsAuthorizations[]_| |_object_|_optional_|_FundsAuthorizations_|_The history of expenditure approvals._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|FundsAuthorizations[].AuthorizationID| |string|optional|Authorization Id|Internal Company control number which identifies the allocation of funds to the Project.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FundsAuthorizations[].EffectiveDateTime| |string|optional|Effective Date Time|The date and time when the funds were approved.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FundsAuthorizations[].FundsAmount| |number|optional|Funds Amount|The level of expenditure approved.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FundsAuthorizations[].CurrencyID &rarr; [Currency](../reference-data/Currency.1.0.0.md)| |string|optional|Currency Id|Type of currency for the authorized expenditure.|<code>^[\w\-\.]+:reference-data\-\-Currency:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Currency](../reference-data/Currency.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ContractIDs[] &rarr; [Agreement](../master-data/Agreement.1.0.0.md)| |string|optional|Contract I Ds|References to applicable agreements in external contract database system of record.|<code>^[\w\-\.]+:master-data\-\-Agreement:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Agreement](../master-data/Agreement.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Operator &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Operator|The organisation which controlled the conduct of the project.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_Contractors[]_| |_object_|_optional_|_Contractors_|_References to organisations which supplied services to the Project._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Contractors[].ContractorOrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Contractor Organisation Id|Reference to a company that provided services.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Contractors[].ContractorCrew| |string|optional|Contractor Crew|Name of the team, unit, crew, party, or other subdivision of the Contractor that provided services.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Contractors[].ContractorTypeID &rarr; [ContractorType](../reference-data/ContractorType.1.0.0.md)| |string|optional|Contractor Type Id|The identifier of a reference value for the role of a contractor providing services, such as Recording, Line Clearing, Positioning, Data Processing.|<code>^[\w\-\.]+:reference-data\-\-ContractorType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ContractorType](../reference-data/ContractorType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_Personnel[]_| |_object_|_optional_|_Personnel_|_List of key individuals supporting the Project.  This could be Abstracted for re-use, and could reference a separate Persons master data object._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Personnel[].PersonName| |string|optional|Person Name|Name of an individual supporting the Project.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Personnel[].CompanyOrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Company Organisation Id|Reference to the company which employs Personnel.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Personnel[].ProjectRoleID &rarr; [ProjectRole](../reference-data/ProjectRole.1.0.0.md)| |string|optional|Project Role Id|The identifier of a reference value for the role of an individual supporting a Project, such as Project Manager, Party Chief, Client Representative, Senior Observer.|<code>^[\w\-\.]+:reference-data\-\-ProjectRole:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ProjectRole](../reference-data/ProjectRole.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_ProjectSpecifications[]_| |_object_|_optional_|_ProjectSpecifications_|_General parameters defining the configuration of the Project.  In the case of a seismic acquisition project it is like receiver interval, source depth, source type.  In the case of a processing project, it is like replacement velocity, reference datum above mean sea level._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ProjectSpecifications[].EffectiveDateTime| |string|optional|Effective Date Time|The date and time at which a ProjectSpecification becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectSpecifications[].TerminationDateTime| |string|optional|Termination Date Time|The date and time at which a ProjectSpecification is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectSpecifications[].ProjectSpecificationQuantity| |number|optional|Project Specification Quantity|The value for the specified parameter type.|(No pattern)|(No format)|UOM_via_property:UnitOfMeasureID|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectSpecifications[].ProjectSpecificationDateTime| |string|optional|Project Specification Date Time|The actual date and time value of the parameter.  ISO format permits specification of time or date only.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectSpecifications[].ProjectSpecificationIndicator| |boolean|optional|Project Specification Indicator|The actual indicator value of the parameter.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectSpecifications[].ProjectSpecificationText| |string|optional|Project Specification Text|The actual text value of the parameter.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectSpecifications[].UnitOfMeasureID &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)| |string|optional|Unit Of Measure Id|The unit for the quantity parameter if overriding the default for this ParameterType, like metre (m in SI units system) for quantity Length.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectSpecifications[].ParameterTypeID &rarr; [ParameterType](../reference-data/ParameterType.1.0.0.md)| |string|optional|Parameter Type Id|Parameter type of property or characteristic.|<code>^[\w\-\.]+:reference-data\-\-ParameterType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ParameterType](../reference-data/ParameterType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_ProjectStates[]_| |_object_|_optional_|_ProjectStates_|_The history of life cycle states that the Project has been through.._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ProjectStates[].EffectiveDateTime| |string|optional|Effective Date Time|The date and time at which the state becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectStates[].TerminationDateTime| |string|optional|Termination Date Time|The date and time at which the state is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ProjectStates[].ProjectStateTypeID &rarr; [ProjectStateType](../reference-data/ProjectStateType.1.0.0.md)| |string|optional|Project State Type Id|The Project life cycle state from planning to completion.|<code>^[\w\-\.]+:reference-data\-\-ProjectStateType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ProjectStateType](../reference-data/ProjectStateType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractProject.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [ActivityPlan.1.0.0](../master-data/ActivityPlan.1.0.0.md) | allOf | one |  |
| Included by | [ActivityPlan.1.1.0](../master-data/ActivityPlan.1.1.0.md) | allOf | one |  |
| Included by | [BHARun.1.0.0](../master-data/BHARun.1.0.0.md) | allOf | one |  |
| Included by | [EvaluationPlan.1.0.0](../master-data/EvaluationPlan.1.0.0.md) | allOf | one |  |
| Included by | [RigUtilization.1.0.0](../master-data/RigUtilization.1.0.0.md) | allOf | one |  |
| Included by | [Seismic2DInterpretationSet.1.0.0](../master-data/Seismic2DInterpretationSet.1.0.0.md) | allOf | one |  |
| Included by | [Seismic2DInterpretationSet.1.1.0](../master-data/Seismic2DInterpretationSet.1.1.0.md) | allOf | one |  |
| Included by | [Seismic3DInterpretationSet.1.0.0](../master-data/Seismic3DInterpretationSet.1.0.0.md) | allOf | one |  |
| Included by | [Seismic3DInterpretationSet.1.1.0](../master-data/Seismic3DInterpretationSet.1.1.0.md) | allOf | one |  |
| Included by | [SeismicAcquisitionSurvey.1.0.0](../master-data/SeismicAcquisitionSurvey.1.0.0.md) | allOf | one |  |
| Included by | [SeismicAcquisitionSurvey.1.1.0](../master-data/SeismicAcquisitionSurvey.1.1.0.md) | allOf | one |  |
| Included by | [SeismicAcquisitionSurvey.1.2.0](../master-data/SeismicAcquisitionSurvey.1.2.0.md) | allOf | one |  |
| Included by | [SeismicProcessingProject.1.0.0](../master-data/SeismicProcessingProject.1.0.0.md) | allOf | one |  |
| Included by | [SeismicProcessingProject.1.1.0](../master-data/SeismicProcessingProject.1.1.0.md) | allOf | one |  |
| Included by | [SeismicProcessingProject.1.2.0](../master-data/SeismicProcessingProject.1.2.0.md) | allOf | one |  |
| Included by | [SurveyProgram.1.0.0](../master-data/SurveyProgram.1.0.0.md) | allOf | one |  |
| Included by | [WellActivity.1.0.0](../master-data/WellActivity.1.0.0.md) | allOf | one |  |
| Included by | [WellActivity.1.1.0](../master-data/WellActivity.1.1.0.md) | allOf | one |  |
| Included by | [WellActivityProgram.1.0.0](../master-data/WellActivityProgram.1.0.0.md) | allOf | one |  |
| Included by | [WellBarrierElementTest.1.0.0](../master-data/WellBarrierElementTest.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._