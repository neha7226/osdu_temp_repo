<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AdditiveRole [Status: Accepted]
The chief purpose or reason for adding a substance to a fluid used in a downhole operation.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--AdditiveRole:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: PPDM
* Reference value type, governance model: OPEN
* Link to &rarr; Proposal workbook [schema-AdditiveRole.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/reference-data/schema-AdditiveRole.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AdditiveRole.1.0.0.json](../../Authoring/reference-data/AdditiveRole.1.0.0.json)
* Link to &rarr; [Generated Schema AdditiveRole.1.0.0.json](../../Generated/reference-data/AdditiveRole.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AdditiveRole.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/AdditiveRole.1.0.0.json)
* Link to &rarr; [Example Record AdditiveRole.1.0.0.json](../../Examples/reference-data/AdditiveRole.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AdditiveRole


## Outgoing Relationships for AdditiveRole

![AdditiveRole](../_diagrams/reference-data/AdditiveRole.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AdditiveRole Referenced by other Entities

![AdditiveRole](../_diagrams/reference-data/AdditiveRole.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AdditiveRole Properties

## 1. Table of AdditiveRole System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-AdditiveRole:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--AdditiveRole:a0380217-5f1e-53f9-8246-ed46496ccba3|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--AdditiveRole:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of AdditiveRole Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of AdditiveRole Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of AdditiveRole Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of AdditiveRole Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for AdditiveRole.1.0.0

* Status: Published
* Governance: OPEN
* The table below shows all 84 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [AdditiveRole](../../ReferenceValues/Manifests/reference-data/OPEN/AdditiveRole.1.0.0.json).

|`id` with prefix `namespace:reference-data--AdditiveRole:`|Code|Name|Alias|Description|AttributionAuthority|Source|CommitDate|AttributionPublication|NameAlias|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Accelerator|Accelerator|Accelerator|-|A chemical that speeds up the rate of a reaction, especially in cement.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|AcidCrosslinker|AcidCrosslinker|Acid crosslinker|-|An acid that promotes a connection between polymer resin chains.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|AcidEmulsifier|AcidEmulsifier|Acid emulsifier|-|An acid that encourages the suspension of one liquid in another.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|AcidGelling|AcidGelling|Acid gelling|-|An acid that increases the viscosity of a liquid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|AcidInhibitor|AcidInhibitor|Acid inhibitor|-|An acid that slows a reaction between a reactive fluid and a material.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Activator|Activator|Activator|-|A substance that starts or accelerates a chemical reaction.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|AnionicSurfactant|AnionicSurfactant|Anionic surfactant|-|A chemical that modifies the surface tension of a liquid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Antifoam|Antifoam|Antifoam|-|A material that destabilizes a foam.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Antifreeze|Antifreeze|Antifreeze|-|A material that lowers the freezing point of a liquid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|AntiGasMigration|AntiGasMigration|Anti-gas migration|-|A material that prevents the movement of gas during the setting of cement.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Latex",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|AntiGelation|AntiGelation|Anti gelation|-|A substance that prevents or retards the formation of a gel.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|AntiGelling|AntiGelling|Anti-gelling|-|A material that stabilizes the viscosity of a liquid by preventing the formation of a gel.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|AntiSettling|AntiSettling|Anti-settling|-|A substance that prevents or retards the gravity separation of a solid from a liquid.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Antisludging|Antisludging|Antisludging|-|A surfactant used to prevent the formation of sludge, an emulsion very high in solids.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Antistatic|Antistatic|Antistatic|-|A substance that reduces or eliminates the buildup of static electricity.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Biocide|Biocide|Biocide|-|A chemical or treatment that kills bacteria.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Bactericide",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|BlendedSurfactant|BlendedSurfactant|Blended surfactant|-|A combination of additives to change the surface tension of a liquid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|BondAccelerator|BondAccelerator|Bond accelerator|-|A substance that reduces the reaction time for materials to bond together, especially in cement.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|BondingAgent|BondingAgent|Bonding agent|-|A substance that induces materials to bond together, especially in cement.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|BreakerActivator|BreakerActivator|Breaker activator|-|A substance that stimulates the breakdown of the structure of a gel.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|CationicSurfactant|CationicSurfactant|Cationic surfactant|-|A substance with a positive charge that changes the surface tension of a liquid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Chelant|Chelant|Chelant|-|A substance that prevents or slows the precipitation of iron. In the oil field, chelating agents are used in stimulation treatments and for cleaning surface facilities.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Sequestrant",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|ChemicalTracer|ChemicalTracer|Chemical tracer|-|A substance used to track the flow path of a fluid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|ClayDispersant|ClayDispersant|Clay dispersant|-|A substance that aids in breaking up a mass of clay particles.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|ClayStabilizer|ClayStabilizer|Clay stabilizer|-|A substance to inhibit the movement or swelling of clay minerals.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Shale&nbsp;Control&nbsp;Inhibitor",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|ConductivityEnhancer|ConductivityEnhancer|Conductivity enhancer|-|A substance to increase the conductivity of fractures in the reservoir.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|CorrosionInhibitor|CorrosionInhibitor|Corrosion inhibitor|-|A substance to prevent or slow down the reaction of a metal with a fluid.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Crosslinker|Crosslinker|Crosslinker|-|A substance to increase the viscosity of a polymer gel.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Defoamer|Defoamer|Defoamer|-|A chemical that reduces the stability of the bubble skin in a foam and cause the foam to break.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Demulsifier|Demulsifier|Demulsifier|-|A substance that causes the separation of oil from water.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|DiagnosticTracer|DiagnosticTracer|Diagnostic tracer|-|A substance added to a fluid to enable the remote detection of the fluid location or flow path.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Dispersant|Dispersant|Dispersant|-|A substance that aids in breaking up a mass of particles, bubbles or droplets.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://petrowiki.spe.org/Glossary:Dispersant|-|
|DivertingAgent|DivertingAgent|Diverting agent|-|A material that forces acid to enter another zone by having a higher viscosity or building a filter cake.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Emulsifier|Emulsifier|Emulsifier|-|A substance that creates or stabilizes an emulsion.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|EncapBreaker|EncapBreaker|Encap breaker|-|A substance in pill form that stays with the polymer and helps break the mud cake.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://petrowiki.spe.org/Glossary:Encapsulated_breaker|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Encapsulated&nbsp;Gel&nbsp;Breaker",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|Energizer|Energizer|Energizer|-|-|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Expansion|Expansion|Expansion|-|Developed to reduce and compensate the hydraulic shrinkage of cement|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|-|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Expander",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|Extender|Extender|Extender|-|A substance that decreasing the density of a cement slurry.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|FinesSuspender|FinesSuspender|Fines suspender|-|A chemical that keeps fine particles in suspension in a liquid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|FlowEnhancer|FlowEnhancer|Flow enhancer|-|A substance that improves the rate of flow of a liquid.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|FluidLoss|FluidLoss|Fluid loss|-|A substance that reduces the transfer of drilling fluid into the rock.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|FoamingAgent|FoamingAgent|Foaming agent|-|A substance that encourages an emulsion, being the suspension of a gas in a liquid.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|FormationSealer|FormationSealer|Formation sealer|-|A substance that improves the ability of a fluid to close a formation zone, thereby preventing lost circulation or improving zone abandonment.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|FreeWaterControl|FreeWaterControl|Free Water Control|-|An additive to the cement slurry to address the potential problem of particle sedimentation.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|FrictionReducer|FrictionReducer|Friction reducer|-|Dispersants, also known as friction reducers, are used extensively in cement slurries to improve the rheological properties that relate to the flow behavior of the slurry|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://petrowiki.spe.org/Glossary:Friction_reducer|-|
|GasHydrateControl|GasHydrateControl|Gas hydrate control|-|A substance to reduce or prevent the occurrence of gas hydrates in a flow line.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|GasMigration|GasMigration|Gas migration|-|Primarily used as an additive in a cement slurry|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Gellant|Gellant|Gellant|-|A substance that increases the viscosity of a liquid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|GelStabilizer|GelStabilizer|Gel stabilizer|-|-|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|HydrogenSulphideScavenger|HydrogenSulphideScavenger|Hydrogen sulphide Scavenger|-|A material that takes H2S out of solution or the flow stream.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|IronControl|IronControl|Iron control|-|A chemical that control the precipitation of iron from solution.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Lightweight|Lightweight|Lightweight|-|A substance that decreasing the density of a cement slurry.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|LiquidExtender|LiquidExtender|Liquid extender|-|A substance that decreasing the density of a liquid.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|LostCirculation|LostCirculation|Lost circulation|-|-|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Lubricant|Lubricant|Lubricant|-|A material that reduces torque and drag.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://petrowiki.spe.org/Glossary:Lubricants|-|
|MixingFluid|MixingFluid|Mixing Fluid|-|-|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|ModulusOfElasticityAdjustor|ModulusOfElasticityAdjustor|Modulus of elasticity adjustor|-|A substance that changes the compressibility of a liquid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Mothball|Mothball|Mothball|-|Is this about naphthalene balls or &quot;mothballing&quot; as in preserving equipment for future reactivation?|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|MudAcid|MudAcid|Mud acid|-|-|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Mud&nbsp;cleanup&nbsp;acid",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|NonemulsifiedIronControl|NonemulsifiedIronControl|Nonemulsified iron control|-|-|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Nonemulsifier|Nonemulsifier|Non-emulsifier|-|A surfactant to inhibit the emulsion tendency of a treatment fluid with residual oil in the formation.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|NonionicSurfactant|NonionicSurfactant|Nonionic surfactant|-|A chemical that modifies the surface tension of a liquid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://petrowiki.spe.org/Glossary:Nonionic_surfactant|-|
|Odorizer|Odorizer|Odorizer|-|A substance that imparts an odor to natural gas.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Oxidizer|Oxidizer|Oxidizer|-|A water treatment reactant to oxidize or release oxygen.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|OxygenScavenger|OxygenScavenger|Oxygen scavenger|-|A chemical to remove dissolved oxygen from water-flow streams.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Scavenger",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|pHBuffer|pHBuffer|pH buffer|-|A substance to keep the pH of a liquid within the desired range.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"pH&nbsp;adjusting&nbsp;agent",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|Preflush|Preflush|Preflush|-|A substance to dissolve carbonate or to displace and isolate incompatible formation fluids.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|ProppantTransport|ProppantTransport|Proppant transport|-|A proppant transport additive is used to adjust the viscosity or other property of the transport fluid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|RadioactiveTracer|RadioactiveTracer|Radioactive tracer|-|A radioactive substance added to a fluid to enable the remote detection of the fluid location or flow path.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|ResinActivator|ResinActivator|Resin activator|-|A catalyst for setting resin.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Retarder|Retarder|Retarder|-|A chemical that slows a reaction, such as the set time of cement.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|ScaleInhibitor|ScaleInhibitor|Scale inhibitor|-|A substance that prevents the formation of iron scale.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|SetEnhancer|SetEnhancer|Set enhancer|-|A substance that encourages the setting of cement.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Spacer|Spacer|Spacer|-|In the context of pumping, a spacer is a fluid used between two fluids to prevent contamination.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://petrowiki.spe.org/Glossary:Spacer|-|
|Stabilizer|Stabilizer|Stabilizer|-|A chemical or physical effect that prevents separation of two or more, normally immiscible phases.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://petrowiki.spe.org/Glossary:Emulsion_stabilizer|-|
|SulfideInhibitor|SulfideInhibitor|Sulfide inhibitor|-|A chemical that reduces or prevents the formation of iron sulfide scale.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Surfactant|Surfactant|Surfactant|-|A chemical that modifies the surface tension of a liquid.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Foaming&nbsp;Agent",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|SuspendingAgent|SuspendingAgent|Suspending agent|-|A surfactant used in acid-treating solutions and fracturing fluids to suspend insoluble fines.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|TemperatureStabilizer|TemperatureStabilizer|Temperature stabilizer|-|A substance that reduces the degradation of polymers at higher thermal conditions. It stabilizes the polymer not the temperature.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|TensileStrengthAdjustor|TensileStrengthAdjustor|Tensile strength adjustor|-|A substance to increase the tensile strength of a fluid.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Thixotropic|Thixotropic|Thixotropic|-|A substance that changes the thixotropic property of a fluid.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|Viscosifier|Viscosifier|Viscosifier|-|A substance that increases the viscosity of a fluid.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|
|WaxInhibitor|WaxInhibitor|Wax inhibitor|-|A chemical that prevents wax from being deposited in pipes.|PPDM Members|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Paraffin&nbsp;inhibitor",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|
|WeightingAgent|WeightingAgent|Weighting agent|-|A substance that increases the density of a fluid.|OSDU|Workbook Published/AdditiveRole.1.0.0.xlsx; commit SHA b7b9b912.|2022-10-19T17:59:07+02:00|-|-|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AdditiveRole.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [PlannedCementJob.1.1.0](../master-data/PlannedCementJob.1.1.0.md) | CementStages[].CementingFluid.CementAdditives[].AdditiveRoleID | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._