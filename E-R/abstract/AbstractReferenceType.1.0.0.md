<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractReferenceType [Status: Accepted]
Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractReferenceType:1.0.0`
* Schema status: PUBLISHED
* **Migration Guide (M10)** [`osdu:wks:AbstractReferenceType:1.0.0`](../../Guides/MigrationGuides/M10/AbstractReferenceType.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractReferenceType.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractReferenceType.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractReferenceType.1.0.0.json](../../Authoring/abstract/AbstractReferenceType.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractReferenceType.1.0.0.json](../../Generated/abstract/AbstractReferenceType.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractReferenceType.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractReferenceType.1.0.0.json)
* Link to worked examples in context &rarr; [Topic: Usage of `work-product-component--DataQuality`](../../Examples/WorkedExamples/DataQuality/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractReferenceType


## Outgoing Relationships for AbstractReferenceType

![AbstractReferenceType](../_diagrams/abstract/AbstractReferenceType.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractReferenceType Referenced by other Entities

![AbstractReferenceType](../_diagrams/abstract/AbstractReferenceType.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractReferenceType Properties

## Table of AbstractReferenceType Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Name| |string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_NameAlias[]_|_[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|NameAlias[].AliasName|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|NameAlias[].AliasNameTypeID &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|NameAlias[].EffectiveDateTime|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|NameAlias[].TerminationDateTime|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ID| |string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|InactiveIndicator| |boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Description| |string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Code| |string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|AttributionAuthority| |string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|AttributionPublication| |string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|AttributionRevision| |string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|CommitDate| |string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractReferenceType.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [ActivityCode.1.0.0](../reference-data/ActivityCode.1.0.0.md) | allOf | one |  |
| Included by | [ActivityLevel.1.0.0](../reference-data/ActivityLevel.1.0.0.md) | allOf | one |  |
| Included by | [ActivityOutcome.1.0.0](../reference-data/ActivityOutcome.1.0.0.md) | allOf | one |  |
| Included by | [ActivityOutcomeDetail.1.0.0](../reference-data/ActivityOutcomeDetail.1.0.0.md) | allOf | one |  |
| Included by | [ActivityStatus.1.0.0](../reference-data/ActivityStatus.1.0.0.md) | allOf | one |  |
| Included by | [ActivityType.1.0.0](../reference-data/ActivityType.1.0.0.md) | allOf | one |  |
| Included by | [ActualIndicatorType.1.0.0](../reference-data/ActualIndicatorType.1.0.0.md) | allOf | one | Deprecated |
| Included by | [AdditiveRole.1.0.0](../reference-data/AdditiveRole.1.0.0.md) | allOf | one |  |
| Included by | [AdditiveType.1.0.0](../reference-data/AdditiveType.1.0.0.md) | allOf | one |  |
| Included by | [AdditiveType.1.0.1](../reference-data/AdditiveType.1.0.1.md) | allOf | one |  |
| Included by | [AgreementType.1.0.0](../reference-data/AgreementType.1.0.0.md) | allOf | one |  |
| Included by | [AliasNameType.1.0.0](../reference-data/AliasNameType.1.0.0.md) | allOf | one |  |
| Included by | [AliasNameTypeClass.1.0.0](../reference-data/AliasNameTypeClass.1.0.0.md) | allOf | one |  |
| Included by | [AnisotropyType.1.0.0](../reference-data/AnisotropyType.1.0.0.md) | allOf | one |  |
| Included by | [AnnularFluidType.1.0.0](../reference-data/AnnularFluidType.1.0.0.md) | allOf | one |  |
| Included by | [ArtefactRole.1.0.0](../reference-data/ArtefactRole.1.0.0.md) | allOf | one |  |
| Included by | [ArtificialLiftType.1.0.0](../reference-data/ArtificialLiftType.1.0.0.md) | allOf | one |  |
| Included by | [AzimuthReferenceType.1.0.0](../reference-data/AzimuthReferenceType.1.0.0.md) | allOf | one |  |
| Included by | [BasinType.1.0.0](../reference-data/BasinType.1.0.0.md) | allOf | one |  |
| Included by | [BhaStatus.1.0.0](../reference-data/BhaStatus.1.0.0.md) | allOf | one |  |
| Included by | [BinGridDefinitionMethodType.1.0.0](../reference-data/BinGridDefinitionMethodType.1.0.0.md) | allOf | one |  |
| Included by | [BitDullCode.1.0.0](../reference-data/BitDullCode.1.0.0.md) | allOf | one |  |
| Included by | [BitReasonPulled.1.0.0](../reference-data/BitReasonPulled.1.0.0.md) | allOf | one |  |
| Included by | [BitType.1.0.0](../reference-data/BitType.1.0.0.md) | allOf | one |  |
| Included by | [BitType.1.0.1](../reference-data/BitType.1.0.1.md) | allOf | one |  |
| Included by | [BottomHolePressureType.1.0.0](../reference-data/BottomHolePressureType.1.0.0.md) | allOf | one |  |
| Included by | [BoundaryRelationType.1.0.0](../reference-data/BoundaryRelationType.1.0.0.md) | allOf | one |  |
| Included by | [CalculationMethodType.1.0.0](../reference-data/CalculationMethodType.1.0.0.md) | allOf | one |  |
| Included by | [CatalogMapStateType.1.0.0](../reference-data/CatalogMapStateType.1.0.0.md) | allOf | one |  |
| Included by | [CellShapeType.1.0.0](../reference-data/CellShapeType.1.0.0.md) | allOf | one |  |
| Included by | [CementJobType.1.0.0](../reference-data/CementJobType.1.0.0.md) | allOf | one |  |
| Included by | [ChronoStratigraphy.1.0.0](../reference-data/ChronoStratigraphy.1.0.0.md) | allOf | one |  |
| Included by | [CollectionPurpose.1.0.0](../reference-data/CollectionPurpose.1.0.0.md) | allOf | one |  |
| Included by | [ColumnBasedTableType.1.0.0](../reference-data/ColumnBasedTableType.1.0.0.md) | allOf | one |  |
| Included by | [ColumnShapeType.1.0.0](../reference-data/ColumnShapeType.1.0.0.md) | allOf | one |  |
| Included by | [CompressionMethodType.1.0.0](../reference-data/CompressionMethodType.1.0.0.md) | allOf | one |  |
| Included by | [ContactRoleType.1.0.0](../reference-data/ContactRoleType.1.0.0.md) | allOf | one |  |
| Included by | [ContractorType.1.0.0](../reference-data/ContractorType.1.0.0.md) | allOf | one |  |
| Included by | [ConventionalCoreType.1.0.0](../reference-data/ConventionalCoreType.1.0.0.md) | allOf | one |  |
| Included by | [ConventionalCoreType.1.0.1](../reference-data/ConventionalCoreType.1.0.1.md) | allOf | one |  |
| Included by | [CoordinateReferenceSystem.1.0.0](../reference-data/CoordinateReferenceSystem.1.0.0.md) | allOf | one |  |
| Included by | [CoordinateReferenceSystem.1.1.0](../reference-data/CoordinateReferenceSystem.1.1.0.md) | allOf | one |  |
| Included by | [CoordinateTransformation.1.0.0](../reference-data/CoordinateTransformation.1.0.0.md) | allOf | one |  |
| Included by | [CoordinateTransformation.1.1.0](../reference-data/CoordinateTransformation.1.1.0.md) | allOf | one |  |
| Included by | [CorePreservationType.1.0.0](../reference-data/CorePreservationType.1.0.0.md) | allOf | one |  |
| Included by | [Currency.1.0.0](../reference-data/Currency.1.0.0.md) | allOf | one |  |
| Included by | [CurveIndexDimensionType.1.0.0](../reference-data/CurveIndexDimensionType.1.0.0.md) | allOf | one | Deprecated |
| Included by | [CurveSampleType.1.0.0](../reference-data/CurveSampleType.1.0.0.md) | allOf | one |  |
| Included by | [DataRuleDimensionType.1.0.0](../reference-data/DataRuleDimensionType.1.0.0.md) | allOf | one |  |
| Included by | [DataRulePurposeType.1.0.0](../reference-data/DataRulePurposeType.1.0.0.md) | allOf | one |  |
| Included by | [DepositionGeometryType.1.0.0](../reference-data/DepositionGeometryType.1.0.0.md) | allOf | one |  |
| Included by | [DimensionType.1.0.0](../reference-data/DimensionType.1.0.0.md) | allOf | one |  |
| Included by | [DiscoverabilityBySearch.1.0.0](../reference-data/DiscoverabilityBySearch.1.0.0.md) | allOf | one |  |
| Included by | [DiscretisationSchemeType.1.0.0](../reference-data/DiscretisationSchemeType.1.0.0.md) | allOf | one |  |
| Included by | [DocumentType.1.0.0](../reference-data/DocumentType.1.0.0.md) | allOf | one |  |
| Included by | [DomainType.1.0.0](../reference-data/DomainType.1.0.0.md) | allOf | one |  |
| Included by | [DrillingActivityClassType.1.0.0](../reference-data/DrillingActivityClassType.1.0.0.md) | allOf | one |  |
| Included by | [DrillingReasonType.1.0.0](../reference-data/DrillingReasonType.1.0.0.md) | allOf | one |  |
| Included by | [EncodingFormatType.1.0.0](../reference-data/EncodingFormatType.1.0.0.md) | allOf | one |  |
| Included by | [ExistenceKind.1.0.0](../reference-data/ExistenceKind.1.0.0.md) | allOf | one |  |
| Included by | [ExternalCatalogNamespace.1.0.0](../reference-data/ExternalCatalogNamespace.1.0.0.md) | allOf | one |  |
| Included by | [ExternalReferenceValueMapping.1.0.0](../reference-data/ExternalReferenceValueMapping.1.0.0.md) | allOf | one |  |
| Included by | [ExternalUnitOfMeasure.1.0.0](../reference-data/ExternalUnitOfMeasure.1.0.0.md) | allOf | one |  |
| Included by | [ExternalUnitQuantity.1.0.0](../reference-data/ExternalUnitQuantity.1.0.0.md) | allOf | one |  |
| Included by | [FacetRole.1.0.0](../reference-data/FacetRole.1.0.0.md) | allOf | one |  |
| Included by | [FacetType.1.0.0](../reference-data/FacetType.1.0.0.md) | allOf | one |  |
| Included by | [FacilityEventType.1.0.0](../reference-data/FacilityEventType.1.0.0.md) | allOf | one |  |
| Included by | [FacilityStateType.1.0.0](../reference-data/FacilityStateType.1.0.0.md) | allOf | one |  |
| Included by | [FacilityType.1.0.0](../reference-data/FacilityType.1.0.0.md) | allOf | one |  |
| Included by | [FaultThrowType.1.0.0](../reference-data/FaultThrowType.1.0.0.md) | allOf | one |  |
| Included by | [FeatureType.1.0.0](../reference-data/FeatureType.1.0.0.md) | allOf | one |  |
| Included by | [FluidContactType.1.0.0](../reference-data/FluidContactType.1.0.0.md) | allOf | one |  |
| Included by | [FluidPhaseType.1.0.0](../reference-data/FluidPhaseType.1.0.0.md) | allOf | one |  |
| Included by | [FluidPropertyFacetName.1.0.0](../reference-data/FluidPropertyFacetName.1.0.0.md) | allOf | one |  |
| Included by | [FluidPropertyName.1.0.0](../reference-data/FluidPropertyName.1.0.0.md) | allOf | one |  |
| Included by | [FluidRheologicalModelType.1.0.0](../reference-data/FluidRheologicalModelType.1.0.0.md) | allOf | one |  |
| Included by | [FluidRole.1.0.0](../reference-data/FluidRole.1.0.0.md) | allOf | one |  |
| Included by | [FluidType.1.0.0](../reference-data/FluidType.1.0.0.md) | allOf | one |  |
| Included by | [FormationIntegrityPressureDataSource.1.0.0](../reference-data/FormationIntegrityPressureDataSource.1.0.0.md) | allOf | one |  |
| Included by | [FormationIntegritySurfacePressureDataSource.1.0.0](../reference-data/FormationIntegritySurfacePressureDataSource.1.0.0.md) | allOf | one |  |
| Included by | [FormationIntegrityTestResult.1.0.0](../reference-data/FormationIntegrityTestResult.1.0.0.md) | allOf | one |  |
| Included by | [FormationIntegrityTestType.1.0.0](../reference-data/FormationIntegrityTestType.1.0.0.md) | allOf | one |  |
| Included by | [FormationPressureTestType.1.0.0](../reference-data/FormationPressureTestType.1.0.0.md) | allOf | one |  |
| Included by | [GasReadingType.1.0.0](../reference-data/GasReadingType.1.0.0.md) | allOf | one |  |
| Included by | [GeologicalFormation.1.0.0](../reference-data/GeologicalFormation.1.0.0.md) | allOf | one |  |
| Included by | [GeologicUnitShapeType.1.0.0](../reference-data/GeologicUnitShapeType.1.0.0.md) | allOf | one |  |
| Included by | [GeoPoliticalEntityType.1.0.0](../reference-data/GeoPoliticalEntityType.1.0.0.md) | allOf | one |  |
| Included by | [GeoReferencedImageType.1.0.0](../reference-data/GeoReferencedImageType.1.0.0.md) | allOf | one |  |
| Included by | [GrainDensityMeasurementType.1.0.0](../reference-data/GrainDensityMeasurementType.1.0.0.md) | allOf | one |  |
| Included by | [GrainSizeAnalysisMethod.1.0.0](../reference-data/GrainSizeAnalysisMethod.1.0.0.md) | allOf | one |  |
| Included by | [GrainSizeClassification.1.0.0](../reference-data/GrainSizeClassification.1.0.0.md) | allOf | one |  |
| Included by | [GrainSizeClassificationScheme.1.0.0](../reference-data/GrainSizeClassificationScheme.1.0.0.md) | allOf | one |  |
| Included by | [HeaderKeyName.1.0.0](../reference-data/HeaderKeyName.1.0.0.md) | allOf | one |  |
| Included by | [IjkCellFace.1.0.0](../reference-data/IjkCellFace.1.0.0.md) | allOf | one |  |
| Included by | [ImageLightingCondition.1.0.0](../reference-data/ImageLightingCondition.1.0.0.md) | allOf | one |  |
| Included by | [IndexableElement.1.0.0](../reference-data/IndexableElement.1.0.0.md) | allOf | one |  |
| Included by | [InSARApplication.1.0.0](../reference-data/InSARApplication.1.0.0.md) | allOf | one |  |
| Included by | [InSARFrequencyBand.1.0.0](../reference-data/InSARFrequencyBand.1.0.0.md) | allOf | one |  |
| Included by | [InSARImageMode.1.0.0](../reference-data/InSARImageMode.1.0.0.md) | allOf | one |  |
| Included by | [InSARPolarisation.1.0.0](../reference-data/InSARPolarisation.1.0.0.md) | allOf | one |  |
| Included by | [InSARProcessingType.1.0.0](../reference-data/InSARProcessingType.1.0.0.md) | allOf | one |  |
| Included by | [InterpolationMethod.1.0.0](../reference-data/InterpolationMethod.1.0.0.md) | allOf | one |  |
| Included by | [IsolatedIntervalType.1.0.0](../reference-data/IsolatedIntervalType.1.0.0.md) | allOf | one |  |
| Included by | [KDirectionType.1.0.0](../reference-data/KDirectionType.1.0.0.md) | allOf | one |  |
| Included by | [LaheeClass.1.0.0](../reference-data/LaheeClass.1.0.0.md) | allOf | one |  |
| Included by | [LegalStatus.1.0.0](../reference-data/LegalStatus.1.0.0.md) | allOf | one |  |
| Included by | [LicenseState.1.0.0](../reference-data/LicenseState.1.0.0.md) | allOf | one |  |
| Included by | [LineageRelationshipType.1.0.0](../reference-data/LineageRelationshipType.1.0.0.md) | allOf | one |  |
| Included by | [LinerType.1.0.0](../reference-data/LinerType.1.0.0.md) | allOf | one |  |
| Included by | [LithologyType.1.0.0](../reference-data/LithologyType.1.0.0.md) | allOf | one |  |
| Included by | [LithoStratigraphy.1.0.0](../reference-data/LithoStratigraphy.1.0.0.md) | allOf | one |  |
| Included by | [LogCurveBusinessValue.1.0.0](../reference-data/LogCurveBusinessValue.1.0.0.md) | allOf | one |  |
| Included by | [LogCurveFamily.1.0.0](../reference-data/LogCurveFamily.1.0.0.md) | allOf | one |  |
| Included by | [LogCurveFamily.1.1.0](../reference-data/LogCurveFamily.1.1.0.md) | allOf | one |  |
| Included by | [LogCurveMainFamily.1.0.0](../reference-data/LogCurveMainFamily.1.0.0.md) | allOf | one |  |
| Included by | [LogCurveType.1.0.0](../reference-data/LogCurveType.1.0.0.md) | allOf | one |  |
| Included by | [LogCurveType.1.1.0](../reference-data/LogCurveType.1.1.0.md) | allOf | one |  |
| Included by | [LogType.1.0.0](../reference-data/LogType.1.0.0.md) | allOf | one |  |
| Included by | [MarkerPropertyType.1.0.0](../reference-data/MarkerPropertyType.1.0.0.md) | allOf | one |  |
| Included by | [MarkerType.1.0.0](../reference-data/MarkerType.1.0.0.md) | allOf | one |  |
| Included by | [MarkerType.1.0.1](../reference-data/MarkerType.1.0.1.md) | allOf | one |  |
| Included by | [MaterialType.1.0.0](../reference-data/MaterialType.1.0.0.md) | allOf | one | Deprecated |
| Included by | [MudBaseType.1.0.0](../reference-data/MudBaseType.1.0.0.md) | allOf | one |  |
| Included by | [MudClass.1.0.0](../reference-data/MudClass.1.0.0.md) | allOf | one |  |
| Included by | [OAuth2FlowType.1.0.0](../reference-data/OAuth2FlowType.1.0.0.md) | allOf | one |  |
| Included by | [ObjectiveType.1.0.0](../reference-data/ObjectiveType.1.0.0.md) | allOf | one |  |
| Included by | [ObligationType.1.0.0](../reference-data/ObligationType.1.0.0.md) | allOf | one |  |
| Included by | [OperatingEnvironment.1.0.0](../reference-data/OperatingEnvironment.1.0.0.md) | allOf | one |  |
| Included by | [OrderingCriteriaType.1.0.0](../reference-data/OrderingCriteriaType.1.0.0.md) | allOf | one |  |
| Included by | [OrganisationType.1.0.0](../reference-data/OrganisationType.1.0.0.md) | allOf | one |  |
| Included by | [OSDUJsonExtensions.1.0.0](../reference-data/OSDUJsonExtensions.1.0.0.md) | allOf | one |  |
| Included by | [OSDURegion.1.0.0](../reference-data/OSDURegion.1.0.0.md) | allOf | one |  |
| Included by | [ParameterKind.1.0.0](../reference-data/ParameterKind.1.0.0.md) | allOf | one |  |
| Included by | [ParameterRole.1.0.0](../reference-data/ParameterRole.1.0.0.md) | allOf | one |  |
| Included by | [ParameterType.1.0.0](../reference-data/ParameterType.1.0.0.md) | allOf | one |  |
| Included by | [PerforationCentralizationMethodType.1.0.0](../reference-data/PerforationCentralizationMethodType.1.0.0.md) | allOf | one |  |
| Included by | [PerforationConveyedMethod.1.0.0](../reference-data/PerforationConveyedMethod.1.0.0.md) | allOf | one |  |
| Included by | [PerforationGunCarrierCategory.1.0.0](../reference-data/PerforationGunCarrierCategory.1.0.0.md) | allOf | one |  |
| Included by | [PerforationGunCarrierModel.1.0.0](../reference-data/PerforationGunCarrierModel.1.0.0.md) | allOf | one |  |
| Included by | [PerforationGunCarrierType.1.0.0](../reference-data/PerforationGunCarrierType.1.0.0.md) | allOf | one |  |
| Included by | [PerforationGunChargeShape.1.0.0](../reference-data/PerforationGunChargeShape.1.0.0.md) | allOf | one |  |
| Included by | [PerforationGunChargeSize.1.0.0](../reference-data/PerforationGunChargeSize.1.0.0.md) | allOf | one |  |
| Included by | [PerforationGunChargeType.1.0.0](../reference-data/PerforationGunChargeType.1.0.0.md) | allOf | one |  |
| Included by | [PerforationGunFiringHeadType.1.0.0](../reference-data/PerforationGunFiringHeadType.1.0.0.md) | allOf | one |  |
| Included by | [PerforationGunMetallurgyType.1.0.0](../reference-data/PerforationGunMetallurgyType.1.0.0.md) | allOf | one |  |
| Included by | [PerforationGunPhasingType.1.0.0](../reference-data/PerforationGunPhasingType.1.0.0.md) | allOf | one |  |
| Included by | [PerforationIntervalReason.1.0.0](../reference-data/PerforationIntervalReason.1.0.0.md) | allOf | one |  |
| Included by | [PerforationIntervalType.1.0.0](../reference-data/PerforationIntervalType.1.0.0.md) | allOf | one |  |
| Included by | [PerforationIntervalWLSize.1.0.0](../reference-data/PerforationIntervalWLSize.1.0.0.md) | allOf | one |  |
| Included by | [PerforationPillType.1.0.0](../reference-data/PerforationPillType.1.0.0.md) | allOf | one |  |
| Included by | [PermeabilityMeasurementType.1.0.0](../reference-data/PermeabilityMeasurementType.1.0.0.md) | allOf | one |  |
| Included by | [PersonnelOrganisationRole.1.0.0](../reference-data/PersonnelOrganisationRole.1.0.0.md) | allOf | one |  |
| Included by | [PersonnelServiceType.1.0.0](../reference-data/PersonnelServiceType.1.0.0.md) | allOf | one |  |
| Included by | [PetroleumSystemElementType.1.0.0](../reference-data/PetroleumSystemElementType.1.0.0.md) | allOf | one |  |
| Included by | [PillarShapeType.1.0.0](../reference-data/PillarShapeType.1.0.0.md) | allOf | one |  |
| Included by | [PlayType.1.0.0](../reference-data/PlayType.1.0.0.md) | allOf | one |  |
| Included by | [PlugType.1.0.0](../reference-data/PlugType.1.0.0.md) | allOf | one |  |
| Included by | [PorosityMeasurementType.1.0.0](../reference-data/PorosityMeasurementType.1.0.0.md) | allOf | one |  |
| Included by | [PPFGContextType.1.0.0](../reference-data/PPFGContextType.1.0.0.md) | allOf | one |  |
| Included by | [PPFGCurveFamily.1.0.0](../reference-data/PPFGCurveFamily.1.0.0.md) | allOf | one |  |
| Included by | [PPFGCurveLithoType.1.0.0](../reference-data/PPFGCurveLithoType.1.0.0.md) | allOf | one |  |
| Included by | [PPFGCurveMainFamily.1.0.0](../reference-data/PPFGCurveMainFamily.1.0.0.md) | allOf | one |  |
| Included by | [PPFGCurveMnemonic.1.0.0](../reference-data/PPFGCurveMnemonic.1.0.0.md) | allOf | one |  |
| Included by | [PPFGCurveProbability.1.0.0](../reference-data/PPFGCurveProbability.1.0.0.md) | allOf | one |  |
| Included by | [PPFGCurveProcessingType.1.0.0](../reference-data/PPFGCurveProcessingType.1.0.0.md) | allOf | one |  |
| Included by | [PPFGCurveTransformModelType.1.0.0](../reference-data/PPFGCurveTransformModelType.1.0.0.md) | allOf | one |  |
| Included by | [PressureMeasurementType.1.0.0](../reference-data/PressureMeasurementType.1.0.0.md) | allOf | one |  |
| Included by | [ProcessingParameterType.1.0.0](../reference-data/ProcessingParameterType.1.0.0.md) | allOf | one |  |
| Included by | [ProjectRole.1.0.0](../reference-data/ProjectRole.1.0.0.md) | allOf | one |  |
| Included by | [ProjectStateType.1.0.0](../reference-data/ProjectStateType.1.0.0.md) | allOf | one |  |
| Included by | [PropertyFieldRepresentationType.1.0.0](../reference-data/PropertyFieldRepresentationType.1.0.0.md) | allOf | one |  |
| Included by | [PropertyNameType.1.0.0](../reference-data/PropertyNameType.1.0.0.md) | allOf | one |  |
| Included by | [PropertyType.1.0.0](../reference-data/PropertyType.1.0.0.md) | allOf | one |  |
| Included by | [ProspectType.1.0.0](../reference-data/ProspectType.1.0.0.md) | allOf | one |  |
| Included by | [PumpOpType.1.0.0](../reference-data/PumpOpType.1.0.0.md) | allOf | one |  |
| Included by | [QualitativeSpatialAccuracyType.1.0.0](../reference-data/QualitativeSpatialAccuracyType.1.0.0.md) | allOf | one |  |
| Included by | [QualityDataRule.1.0.0](../reference-data/QualityDataRule.1.0.0.md) | allOf | one |  |
| Included by | [QualityDataRule.1.1.0](../reference-data/QualityDataRule.1.1.0.md) | allOf | one |  |
| Included by | [QualityDataRuleSet.2.0.0](../reference-data/QualityDataRuleSet.2.0.0.md) | allOf | one |  |
| Included by | [QuantitativeAccuracyBand.1.0.0](../reference-data/QuantitativeAccuracyBand.1.0.0.md) | allOf | one |  |
| Included by | [ReasonTripType.1.0.0](../reference-data/ReasonTripType.1.0.0.md) | allOf | one |  |
| Included by | [ReferenceValueUpgradeLookUp.1.0.0](../reference-data/ReferenceValueUpgradeLookUp.1.0.0.md) | allOf | one |  |
| Included by | [RepresentationRole.1.0.0](../reference-data/RepresentationRole.1.0.0.md) | allOf | one |  |
| Included by | [RepresentationType.1.0.0](../reference-data/RepresentationType.1.0.0.md) | allOf | one |  |
| Included by | [ResourceCurationStatus.1.0.0](../reference-data/ResourceCurationStatus.1.0.0.md) | allOf | one |  |
| Included by | [ResourceLifecycleStatus.1.0.0](../reference-data/ResourceLifecycleStatus.1.0.0.md) | allOf | one |  |
| Included by | [ResourceSecurityClassification.1.0.0](../reference-data/ResourceSecurityClassification.1.0.0.md) | allOf | one |  |
| Included by | [RigType.1.0.0](../reference-data/RigType.1.0.0.md) | allOf | one |  |
| Included by | [RiskCategory.1.0.0](../reference-data/RiskCategory.1.0.0.md) | allOf | one |  |
| Included by | [RiskConsequenceCategory.1.0.0](../reference-data/RiskConsequenceCategory.1.0.0.md) | allOf | one |  |
| Included by | [RiskConsequenceSubCategory.1.0.0](../reference-data/RiskConsequenceSubCategory.1.0.0.md) | allOf | one |  |
| Included by | [RiskDiscipline.1.0.0](../reference-data/RiskDiscipline.1.0.0.md) | allOf | one |  |
| Included by | [RiskHierarchyLevel.1.0.0](../reference-data/RiskHierarchyLevel.1.0.0.md) | allOf | one |  |
| Included by | [RiskResponseStatus.1.0.0](../reference-data/RiskResponseStatus.1.0.0.md) | allOf | one |  |
| Included by | [RiskSubCategory.1.0.0](../reference-data/RiskSubCategory.1.0.0.md) | allOf | one |  |
| Included by | [RiskType.1.0.0](../reference-data/RiskType.1.0.0.md) | allOf | one |  |
| Included by | [RockImageType.1.0.0](../reference-data/RockImageType.1.0.0.md) | allOf | one |  |
| Included by | [RockSampleAnalysisType.1.0.0](../reference-data/RockSampleAnalysisType.1.0.0.md) | allOf | one |  |
| Included by | [RockSampleType.1.0.0](../reference-data/RockSampleType.1.0.0.md) | allOf | one |  |
| Included by | [SampleOrientationType.1.0.0](../reference-data/SampleOrientationType.1.0.0.md) | allOf | one |  |
| Included by | [SatelliteGeometry.1.0.0](../reference-data/SatelliteGeometry.1.0.0.md) | allOf | one |  |
| Included by | [SatelliteMission.1.0.0](../reference-data/SatelliteMission.1.0.0.md) | allOf | one |  |
| Included by | [SaturationMethodType.1.0.0](../reference-data/SaturationMethodType.1.0.0.md) | allOf | one |  |
| Included by | [SchemaFormatType.1.0.0](../reference-data/SchemaFormatType.1.0.0.md) | allOf | one |  |
| Included by | [SchemaUpgradeChain.1.0.0](../reference-data/SchemaUpgradeChain.1.0.0.md) | allOf | one |  |
| Included by | [SchemaUpgradeSpecification.1.0.0](../reference-data/SchemaUpgradeSpecification.1.0.0.md) | allOf | one |  |
| Included by | [SectionType.1.0.0](../reference-data/SectionType.1.0.0.md) | allOf | one |  |
| Included by | [SecuritySchemeType.1.0.0](../reference-data/SecuritySchemeType.1.0.0.md) | allOf | one |  |
| Included by | [SEGY-HeaderMappingTemplate.1.0.0](../reference-data/SEGY-HeaderMappingTemplate.1.0.0.md) | allOf | one |  |
| Included by | [SeismicAcquisitionType.1.0.0](../reference-data/SeismicAcquisitionType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicAttributeType.1.0.0](../reference-data/SeismicAttributeType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicBinGridType.1.0.0](../reference-data/SeismicBinGridType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicDomainType.1.0.0](../reference-data/SeismicDomainType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicEnergySourceType.1.0.0](../reference-data/SeismicEnergySourceType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicEnergySourceType.1.0.1](../reference-data/SeismicEnergySourceType.1.0.1.md) | allOf | one |  |
| Included by | [SeismicFaultType.1.0.0](../reference-data/SeismicFaultType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicFilteringType.1.0.0](../reference-data/SeismicFilteringType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicGatherType.1.0.0](../reference-data/SeismicGatherType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicGeometryType.1.0.0](../reference-data/SeismicGeometryType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicHorizonType.1.0.0](../reference-data/SeismicHorizonType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicMigrationType.1.0.0](../reference-data/SeismicMigrationType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicMigrationType.1.0.1](../reference-data/SeismicMigrationType.1.0.1.md) | allOf | one |  |
| Included by | [SeismicPhase.1.0.0](../reference-data/SeismicPhase.1.0.0.md) | allOf | one |  |
| Included by | [SeismicPickingType.1.0.0](../reference-data/SeismicPickingType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicPickingType.1.0.1](../reference-data/SeismicPickingType.1.0.1.md) | allOf | one |  |
| Included by | [SeismicPolarity.1.0.0](../reference-data/SeismicPolarity.1.0.0.md) | allOf | one |  |
| Included by | [SeismicProcessingStageType.1.0.0](../reference-data/SeismicProcessingStageType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicProcessingStageType.1.0.1](../reference-data/SeismicProcessingStageType.1.0.1.md) | allOf | one |  |
| Included by | [SeismicReceiverType.1.0.0](../reference-data/SeismicReceiverType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicStackingType.1.0.0](../reference-data/SeismicStackingType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicTraceDataDimensionalityType.1.0.0](../reference-data/SeismicTraceDataDimensionalityType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicTraceSortOrder.1.0.0](../reference-data/SeismicTraceSortOrder.1.0.0.md) | allOf | one |  |
| Included by | [SeismicWaveType.1.0.0](../reference-data/SeismicWaveType.1.0.0.md) | allOf | one |  |
| Included by | [SeismicWaveType.1.0.1](../reference-data/SeismicWaveType.1.0.1.md) | allOf | one |  |
| Included by | [SequenceStratigraphicSchemaType.1.0.0](../reference-data/SequenceStratigraphicSchemaType.1.0.0.md) | allOf | one |  |
| Included by | [SequenceStratigraphySurfaceType.1.0.0](../reference-data/SequenceStratigraphySurfaceType.1.0.0.md) | allOf | one |  |
| Included by | [SidewallCoreType.1.0.0](../reference-data/SidewallCoreType.1.0.0.md) | allOf | one |  |
| Included by | [SpatialGeometryType.1.0.0](../reference-data/SpatialGeometryType.1.0.0.md) | allOf | one |  |
| Included by | [SpatialParameterType.1.0.0](../reference-data/SpatialParameterType.1.0.0.md) | allOf | one |  |
| Included by | [StandardsOrganisation.1.0.0](../reference-data/StandardsOrganisation.1.0.0.md) | allOf | one |  |
| Included by | [StratigraphicColumnRankUnitType.1.0.0](../reference-data/StratigraphicColumnRankUnitType.1.0.0.md) | allOf | one |  |
| Included by | [StratigraphicColumnValidityAreaType.1.0.0](../reference-data/StratigraphicColumnValidityAreaType.1.0.0.md) | allOf | one |  |
| Included by | [StratigraphicRoleType.1.0.0](../reference-data/StratigraphicRoleType.1.0.0.md) | allOf | one |  |
| Included by | [StringClass.1.0.0](../reference-data/StringClass.1.0.0.md) | allOf | one |  |
| Included by | [SurveyToolType.1.0.0](../reference-data/SurveyToolType.1.0.0.md) | allOf | one |  |
| Included by | [TargetShape.1.0.0](../reference-data/TargetShape.1.0.0.md) | allOf | one |  |
| Included by | [TargetType.1.0.0](../reference-data/TargetType.1.0.0.md) | allOf | one |  |
| Included by | [TechnicalAssuranceType.1.0.0](../reference-data/TechnicalAssuranceType.1.0.0.md) | allOf | one |  |
| Included by | [TectonicSettingType.1.0.0](../reference-data/TectonicSettingType.1.0.0.md) | allOf | one |  |
| Included by | [TestSubType.1.0.0](../reference-data/TestSubType.1.0.0.md) | allOf | one |  |
| Included by | [TestType.1.0.0](../reference-data/TestType.1.0.0.md) | allOf | one |  |
| Included by | [TrajectoryStationPropertyType.1.0.0](../reference-data/TrajectoryStationPropertyType.1.0.0.md) | allOf | one |  |
| Included by | [TubularAssemblyStatusType.1.0.0](../reference-data/TubularAssemblyStatusType.1.0.0.md) | allOf | one |  |
| Included by | [TubularAssemblyType.1.0.0](../reference-data/TubularAssemblyType.1.0.0.md) | allOf | one |  |
| Included by | [TubularComponentConnectionType.1.0.0](../reference-data/TubularComponentConnectionType.1.0.0.md) | allOf | one |  |
| Included by | [TubularComponentGrade.1.0.0](../reference-data/TubularComponentGrade.1.0.0.md) | allOf | one |  |
| Included by | [TubularComponentPinBoxType.1.0.0](../reference-data/TubularComponentPinBoxType.1.0.0.md) | allOf | one |  |
| Included by | [TubularComponentType.1.0.0](../reference-data/TubularComponentType.1.0.0.md) | allOf | one |  |
| Included by | [TubularMaterialType.1.0.0](../reference-data/TubularMaterialType.1.0.0.md) | allOf | one |  |
| Included by | [TubularUmbilicalServiceType.1.0.0](../reference-data/TubularUmbilicalServiceType.1.0.0.md) | allOf | one |  |
| Included by | [TubularUmbilicalType.1.0.0](../reference-data/TubularUmbilicalType.1.0.0.md) | allOf | one |  |
| Included by | [UnitOfMeasure.1.0.0](../reference-data/UnitOfMeasure.1.0.0.md) | allOf | one |  |
| Included by | [UnitOfMeasureConfiguration.1.0.0](../reference-data/UnitOfMeasureConfiguration.1.0.0.md) | allOf | one |  |
| Included by | [UnitQuantity.1.0.0](../reference-data/UnitQuantity.1.0.0.md) | allOf | one |  |
| Included by | [ValueChainStatusType.1.0.0](../reference-data/ValueChainStatusType.1.0.0.md) | allOf | one |  |
| Included by | [VelocityAnalysisMethod.1.0.0](../reference-data/VelocityAnalysisMethod.1.0.0.md) | allOf | one |  |
| Included by | [VelocityAnalysisMethod.1.0.1](../reference-data/VelocityAnalysisMethod.1.0.1.md) | allOf | one |  |
| Included by | [VelocityDirectionType.1.0.0](../reference-data/VelocityDirectionType.1.0.0.md) | allOf | one |  |
| Included by | [VelocityType.1.0.0](../reference-data/VelocityType.1.0.0.md) | allOf | one |  |
| Included by | [VerticalMeasurementPath.1.0.0](../reference-data/VerticalMeasurementPath.1.0.0.md) | allOf | one |  |
| Included by | [VerticalMeasurementPath.1.0.1](../reference-data/VerticalMeasurementPath.1.0.1.md) | allOf | one |  |
| Included by | [VerticalMeasurementSource.1.0.0](../reference-data/VerticalMeasurementSource.1.0.0.md) | allOf | one |  |
| Included by | [VerticalMeasurementType.1.0.0](../reference-data/VerticalMeasurementType.1.0.0.md) | allOf | one |  |
| Included by | [VerticalMeasurementType.1.0.1](../reference-data/VerticalMeasurementType.1.0.1.md) | allOf | one |  |
| Included by | [VerticalMeasurementType.1.0.2](../reference-data/VerticalMeasurementType.1.0.2.md) | allOf | one |  |
| Included by | [WeatherType.1.0.0](../reference-data/WeatherType.1.0.0.md) | allOf | one |  |
| Included by | [WellActivityPhaseType.1.0.0](../reference-data/WellActivityPhaseType.1.0.0.md) | allOf | one |  |
| Included by | [WellActivityProgramType.1.0.0](../reference-data/WellActivityProgramType.1.0.0.md) | allOf | one |  |
| Included by | [WellActivityType.1.0.0](../reference-data/WellActivityType.1.0.0.md) | allOf | one |  |
| Included by | [WellboreOpeningStateType.1.0.0](../reference-data/WellboreOpeningStateType.1.0.0.md) | allOf | one |  |
| Included by | [WellboreReason.1.0.0](../reference-data/WellboreReason.1.0.0.md) | allOf | one |  |
| Included by | [WellboreTrajectoryType.1.0.0](../reference-data/WellboreTrajectoryType.1.0.0.md) | allOf | one |  |
| Included by | [WellBusinessIntention.1.0.0](../reference-data/WellBusinessIntention.1.0.0.md) | allOf | one |  |
| Included by | [WellBusinessIntentionOutcome.1.0.0](../reference-data/WellBusinessIntentionOutcome.1.0.0.md) | allOf | one |  |
| Included by | [WellCondition.1.0.0](../reference-data/WellCondition.1.0.0.md) | allOf | one |  |
| Included by | [WellFluidDirection.1.0.0](../reference-data/WellFluidDirection.1.0.0.md) | allOf | one |  |
| Included by | [WellheadConnection.1.0.0](../reference-data/WellheadConnection.1.0.0.md) | allOf | one |  |
| Included by | [WellInterestType.1.0.0](../reference-data/WellInterestType.1.0.0.md) | allOf | one |  |
| Included by | [WellLogSamplingDomainType.1.0.0](../reference-data/WellLogSamplingDomainType.1.0.0.md) | allOf | one |  |
| Included by | [WellProductType.1.0.0](../reference-data/WellProductType.1.0.0.md) | allOf | one |  |
| Included by | [WellRole.1.0.0](../reference-data/WellRole.1.0.0.md) | allOf | one |  |
| Included by | [WellSiteProductType.1.0.0](../reference-data/WellSiteProductType.1.0.0.md) | allOf | one |  |
| Included by | [WellStatusSummary.1.0.0](../reference-data/WellStatusSummary.1.0.0.md) | allOf | one |  |
| Included by | [WellTechnologyApplied.1.0.0](../reference-data/WellTechnologyApplied.1.0.0.md) | allOf | one |  |
| Included by | [WordFormatType.1.0.0](../reference-data/WordFormatType.1.0.0.md) | allOf | one |  |
| Included by | [WorkflowPersonaType.1.0.0](../reference-data/WorkflowPersonaType.1.0.0.md) | allOf | one |  |
| Included by | [WorkflowUsageType.1.0.0](../reference-data/WorkflowUsageType.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._