# Change Log

## Schema Changes

Details as reported for each snapshot, please see [ChangeReport.md](./E-R/ChangeReport.md).

## Reference Value Change Log

Details as reported for each snapshot, please see this [README.md](ReferenceValues/ChangeLogs/README.md)