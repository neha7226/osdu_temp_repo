<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractVectorHeaderMapping [Status: Accepted]
Array of objects which define the meaning and format of a tabular structure used in a binary file as a header.  The initial use case is the trace headers of a SEG-Y file.  Note that some of this information may be repeated in the SEG-Y EBCDIC header.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractVectorHeaderMapping:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractVectorHeaderMapping.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractVectorHeaderMapping.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractVectorHeaderMapping.1.0.0.json](../../Authoring/abstract/AbstractVectorHeaderMapping.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractVectorHeaderMapping.1.0.0.json](../../Generated/abstract/AbstractVectorHeaderMapping.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractVectorHeaderMapping.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractVectorHeaderMapping.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractVectorHeaderMapping


## Outgoing Relationships for AbstractVectorHeaderMapping

![AbstractVectorHeaderMapping](../_diagrams/abstract/AbstractVectorHeaderMapping.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractVectorHeaderMapping Referenced by other Entities

![AbstractVectorHeaderMapping](../_diagrams/abstract/AbstractVectorHeaderMapping.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractVectorHeaderMapping Properties

## Table of AbstractVectorHeaderMapping Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_VectorHeaderMapping[]_| |_object_|_optional_|_Vector Header Mapping_|_Array of objects which define the meaning and format of a tabular structure used in a binary file as a header.  The initial use case is the trace headers of a SEG-Y file.  Note that some of this information may be repeated in the SEG-Y EBCDIC header._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|VectorHeaderMapping[].KeyName &rarr; [HeaderKeyName](../reference-data/HeaderKeyName.1.0.0.md)| |string|optional|Key Name|Relationship to a reference value for a name of a property header such as INLINE, CDPX.|<code>^[\w\-\.]+:reference-data\-\-HeaderKeyName:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [HeaderKeyName](../reference-data/HeaderKeyName.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VectorHeaderMapping[].WordFormat &rarr; [WordFormatType](../reference-data/WordFormatType.1.0.0.md)| |string|optional|Word Format|Relationship to a reference value for binary data types, such as INT, UINT, FLOAT, IBM_FLOAT, ASCII, EBCDIC.|<code>^[\w\-\.]+:reference-data\-\-WordFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WordFormatType](../reference-data/WordFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VectorHeaderMapping[].WordWidth| |integer|optional|Word Width|Size of the word in bytes.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VectorHeaderMapping[].Position| |integer|optional|Position|Beginning byte position of header value, 1 indexed.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VectorHeaderMapping[].UoM &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)| |string|optional|Unit of Measure|Relationship to units of measure reference if header standard is not followed.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VectorHeaderMapping[].ScalarIndicator| |enum string|optional|Scalar Indicator|Enumerated string indicating whether to use the normal scalar field for scaling this field (STANDARD), no scaling (NOSCALE), or override scalar (OVERRIDE).  Default is current STANDARD (such as SEG-Y rev2).|<code>STANDARD&#124;NOSCALE&#124;OVERRIDE</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|STANDARD|
|VectorHeaderMapping[].ScalarOverride| |number|optional|Scalar Override|Scalar value (as defined by standard) when a value present in the header needs to be overwritten for this value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractVectorHeaderMapping.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [File.CompressedVectorHeaders.1.0.0](../dataset/File.CompressedVectorHeaders.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.SEGY.1.0.0](../dataset/FileCollection.SEGY.1.0.0.md) | allOf | one |  |
| Included by | [SEGY-HeaderMappingTemplate.1.0.0](../reference-data/SEGY-HeaderMappingTemplate.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._