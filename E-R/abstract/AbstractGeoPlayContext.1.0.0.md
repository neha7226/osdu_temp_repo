<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractGeoPlayContext [Status: Accepted]
A single, typed Play entity reference, which is 'abstracted' to AbstractGeoContext and then aggregated by GeoContexts properties.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractGeoPlayContext:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractGeoPlayContext.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractGeoPlayContext.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractGeoPlayContext.1.0.0.json](../../Authoring/abstract/AbstractGeoPlayContext.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractGeoPlayContext.1.0.0.json](../../Generated/abstract/AbstractGeoPlayContext.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractGeoPlayContext.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractGeoPlayContext.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractGeoPlayContext


## Outgoing Relationships for AbstractGeoPlayContext

![AbstractGeoPlayContext](../_diagrams/abstract/AbstractGeoPlayContext.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractGeoPlayContext Referenced by other Entities

![AbstractGeoPlayContext](../_diagrams/abstract/AbstractGeoPlayContext.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractGeoPlayContext Properties

## Table of AbstractGeoPlayContext Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|PlayID &rarr; [Play](../master-data/Play.1.0.0.md)| |string|optional|Play Id|Reference to the play.|<code>^[\w\-\.]+:master-data\-\-Play:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Play](../master-data/Play.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoTypeID &rarr; [PlayType](../reference-data/PlayType.1.0.0.md)| |string|optional|Geo Type Id|The PlayType reference of the Play (via PlayID) for application convenience.|<code>^[\w\-\.]+:reference-data\-\-PlayType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [PlayType](../reference-data/PlayType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "PlayID", "TargetPropertyName": "PlayTypeID"}|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractGeoPlayContext.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) | oneOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._