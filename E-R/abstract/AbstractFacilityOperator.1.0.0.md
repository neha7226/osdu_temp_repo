<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractFacilityOperator
The organisation that was responsible for a facility at some point in time.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractFacilityOperator:1.0.0`
* Schema status: PUBLISHED
* **Superseded by** &rarr; [`osdu:wks:AbstractFacilityOperator:1.1.0`](AbstractFacilityOperator.1.1.0.md)
* Link to &rarr; [Authoring Schema AbstractFacilityOperator.1.0.0.json](../../Authoring/abstract/AbstractFacilityOperator.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractFacilityOperator.1.0.0.json](../../Generated/abstract/AbstractFacilityOperator.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractFacilityOperator.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractFacilityOperator.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractFacilityOperator


## Outgoing Relationships for AbstractFacilityOperator

![AbstractFacilityOperator](../_diagrams/abstract/AbstractFacilityOperator.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractFacilityOperator Referenced by other Entities

![AbstractFacilityOperator](../_diagrams/abstract/AbstractFacilityOperator.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacilityOperator Properties

## Table of AbstractFacilityOperator Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|FacilityOperatorID| |string|optional|Facility Operator ID|Internal, unique identifier for an item 'AbstractFacilityOperator'. This identifier is used by 'AbstractFacility.CurrentOperatorID' and 'AbstractFacility.InitialOperatorID'.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityOperatorOrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Facility Operator Organisation Id|The company that currently operates, or previously operated the facility|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|EffectiveDateTime| |string|optional|Effective Date Time|The date and time at which the facility operator becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TerminationDateTime| |string|optional|Termination Date Time|The date and time at which the facility operator is no longer in effect. If the operator is still effective, the 'TerminationDateTime' is left absent.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacilityOperator.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractFacility.1.0.0](AbstractFacility.1.0.0.md) | FacilityOperators[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._