<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractWPCGroupType [Status: Accepted]
Generic reference object containing the universal group-type properties of a Work Product Component for inclusion in data type specific Work Product Component objects
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractWPCGroupType:1.0.0`
* Schema status: PUBLISHED
* **Migration Guide (M12)** [`osdu:wks:AbstractWPCGroupType:1.0.0`](../../Guides/MigrationGuides/M12/AbstractWPCGroupType.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractWPCGroupType.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractWPCGroupType.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractWPCGroupType.1.0.0.json](../../Authoring/abstract/AbstractWPCGroupType.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractWPCGroupType.1.0.0.json](../../Generated/abstract/AbstractWPCGroupType.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractWPCGroupType.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractWPCGroupType.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractWPCGroupType


## Outgoing Relationships for AbstractWPCGroupType

![AbstractWPCGroupType](../_diagrams/abstract/AbstractWPCGroupType.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractWPCGroupType Referenced by other Entities

![AbstractWPCGroupType](../_diagrams/abstract/AbstractWPCGroupType.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractWPCGroupType Properties

## Table of AbstractWPCGroupType Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Datasets[]| |string|optional|Datasets|The record id, which identifies this OSDU File or dataset resource.|<code>^[\w\-\.]+:dataset\-\-[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_Artefacts[]_| |_object_|_optional_|_Artefacts_|_An array of Artefacts - each artefact has a Role, Resource tuple. An artefact is distinct from the file, in the sense certain valuable information is generated during loading process (Artefact generation process). Examples include retrieving location data, performing an OCR which may result in the generation of artefacts which need to be preserved distinctly_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Artefacts[].RoleID &rarr; [ArtefactRole](../reference-data/ArtefactRole.1.0.0.md)| |string|optional|Role Id|The SRN of this artefact's role.|<code>^[\w\-\.]+:reference-data\-\-ArtefactRole:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ArtefactRole](../reference-data/ArtefactRole.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Artefacts[].ResourceKind| |string|optional|Resource Kind|The kind or schema ID of the artefact. Resolvable with the Schema Service.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Artefacts[].ResourceID| |string|optional|Resource Id|The SRN which identifies this OSDU Artefact resource.|<code>^[\w\-\.]+:dataset\-\-[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|IsExtendedLoad| |boolean|optional|Is Extended Load|A flag that indicates if the work product component is undergoing an extended load.  It reflects the fact that the work product component is in an early stage and may be updated before finalization.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|IsDiscoverable| |boolean|optional|Is Discoverable|A flag that indicates if the work product component is searchable, which means covered in the search index.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_TechnicalAssurances[]_|_[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)_|_object_|_optional_|_AbstractTechnicalAssurance_|_Describes a record's overall suitability for general business consumption based on data quality. Clarifications: Since Certified is the highest classification of suitable quality, any further change or versioning of a Certified record should be carefully considered and justified. If a Technical Assurance value is not populated then one can assume the data has not been evaluated or its quality is unknown (=Unevaluated). Technical Assurance values are not intended to be used for the identification of a single "preferred" or "definitive" record by comparison with other records. Fragment Description: Describes a record's overall suitability for general business consumption based on level of trust._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|TechnicalAssurances[].TechnicalAssuranceTypeID &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)|string|required|Technical Assurance Type ID|Describes a record's overall suitability for general business consumption based on data quality. Clarifications: Since Certified is the highest classification of suitable quality, any further change or versioning of a Certified record should be carefully considered and justified. If a Technical Assurance value is not populated then one can assume the data has not been evaluated or its quality is unknown (=Unevaluated). Technical Assurance values are not intended to be used for the identification of a single "preferred" or "definitive" record by comparison with other records.|<code>^[\w\-\.]+:reference-data\-\-TechnicalAssuranceType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--TechnicalAssuranceType:Trusted:|
|_TechnicalAssurances[].Reviewers[]_|_[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](AbstractContact.1.0.0.md)_|_object_|_optional_|_Abstract Contact_|_The individuals, or roles, that reviewed and determined the technical assurance value Fragment Description: An object with properties that describe a specific person or other point-of-contact (like an email distribution list) that is relevant in this context (like a given data set or business project). The contact specified may be either internal or external to the organisation (something denoted via the Organisation object that is referenced). Note that some properties contain personally identifiable information, so it might not be appropriate to populate all properties in all scenarios._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|TechnicalAssurances[].Reviewers[].EmailAddress|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Email Address|Contact email address. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|support@company.com|
|TechnicalAssurances[].Reviewers[].PhoneNumber|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Phone Number|Contact phone number. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1-555-281-5555|
|TechnicalAssurances[].Reviewers[].RoleTypeID &rarr; [ContactRoleType](../reference-data/ContactRoleType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Role Type ID|The identifier of a reference value for the role of the contact within the associated organisation, such as Account owner, Sales Representative, Technical Support, Project Manager, Party Chief, Client Representative, Senior Observer.|<code>^[\w\-\.]+:reference-data\-\-ContactRoleType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ContactRoleType](../reference-data/ContactRoleType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TechnicalAssurances[].Reviewers[].Comment|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Comment|Additional information about the contact|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TechnicalAssurances[].Reviewers[].OrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Organisation ID|Reference to the company the contact is associated with.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TechnicalAssurances[].Reviewers[].Name|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](AbstractContact.1.0.0.md)|string|optional|Name|Name of the individual contact. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_TechnicalAssurances[].AcceptableUsage[]_|_[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)_|_object_|_optional_|_AcceptableUsage_|_List of workflows and/or personas that the technical assurance value is valid for (e.g., This data is trusted for Seismic Processing) Fragment Description: Describes the workflows and/or personas that the technical assurance value is valid for (e.g., This data has a technical assurance property of "trusted" and it is suitable for Seismic Interpretation)._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_[{"WorkflowUsage": "namespace:reference-data--WorkflowUsageType:SeismicProcessing:", "WorkflowPersona": "namespace:reference-data--WorkflowPersonaType:SeismicProcessor:"}]_|
|TechnicalAssurances[].AcceptableUsage[].WorkflowUsage &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)|string|optional|Workflow Usage|Name of the business activities, processes, and/or workflows that the record is technical assurance value is valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowUsageType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowUsageType:SeismicProcessing:|
|TechnicalAssurances[].AcceptableUsage[].WorkflowPersona &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)|string|optional|Workflow Persona|Name of the role or personas that the record is technical assurance value is valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowPersonaType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowPersonaType:SeismicProcessor:|
|_TechnicalAssurances[].UnacceptableUsage[]_|_[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)_|_object_|_optional_|_UnacceptableUsage_|_List of workflows and/or personas that the technical assurance value is not valid for (e.g., This data is not trusted for seismic interpretation) Fragment Description: Describes the workflows and/or personas that the technical assurance value is not valid for (e.g., This data has a technical assurance property of "trusted", but it is not suitable for Seismic Interpretation)._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_[{"WorkflowUsage": "namespace:reference-data--WorkflowUsageType:SeismicInterpretation:", "WorkflowPersona": "namespace:reference-data--WorkflowPersonaType:SeismicInterpreter:"}]_|
|TechnicalAssurances[].UnacceptableUsage[].WorkflowUsage &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)|string|optional|Workflow Usage|Name of the business activities, processes, and/or workflows that the record is technical assurance value is not valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowUsageType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowUsageType:SeismicInterpretation:|
|TechnicalAssurances[].UnacceptableUsage[].WorkflowPersona &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)|string|optional|Workflow Persona|Name of the role or personas that the record is technical assurance value is not valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowPersonaType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowPersonaType:SeismicInterpreter:|
|TechnicalAssurances[].EffectiveDate|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)|string|optional|Effective Date|Date when the technical assurance determination for this record has taken place|(No pattern)|date|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-02-13|
|TechnicalAssurances[].Comment|[AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md)|string|optional|Comment|Any additional context to support the determination of technical assurance|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|This is free form text from reviewer, e.g. restrictions on use|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractWPCGroupType.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [Activity.1.0.0](../work-product-component/Activity.1.0.0.md) | allOf | one |  |
| Included by | [Activity.1.1.0](../work-product-component/Activity.1.1.0.md) | allOf | one |  |
| Included by | [AquiferInterpretation.1.0.0](../work-product-component/AquiferInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [ColumnBasedTable.1.0.0](../work-product-component/ColumnBasedTable.1.0.0.md) | allOf | one |  |
| Included by | [DataQuality.1.0.0](../work-product-component/DataQuality.1.0.0.md) | allOf | one |  |
| Included by | [DataQuality.1.1.0](../work-product-component/DataQuality.1.1.0.md) | allOf | one |  |
| Included by | [Document.1.0.0](../work-product-component/Document.1.0.0.md) | allOf | one |  |
| Included by | [EarthModelInterpretation.1.0.0](../work-product-component/EarthModelInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [FaultInterpretation.1.0.0](../work-product-component/FaultInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [FaultSystem.1.0.0](../work-product-component/FaultSystem.1.0.0.md) | allOf | one |  |
| Included by | [FaultSystem.1.1.0](../work-product-component/FaultSystem.1.1.0.md) | allOf | one |  |
| Included by | [FaultSystem.1.2.0](../work-product-component/FaultSystem.1.2.0.md) | allOf | one |  |
| Included by | [FluidBoundaryInterpretation.1.0.0](../work-product-component/FluidBoundaryInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [FormationIntegrityTest.1.0.0](../work-product-component/FormationIntegrityTest.1.0.0.md) | allOf | one |  |
| Included by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | allOf | one |  |
| Included by | [GenericProperty.1.0.0](../work-product-component/GenericProperty.1.0.0.md) | allOf | one |  |
| Included by | [GenericRepresentation.1.0.0](../work-product-component/GenericRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [GeobodyBoundaryInterpretation.1.0.0](../work-product-component/GeobodyBoundaryInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [GeobodyInterpretation.1.0.0](../work-product-component/GeobodyInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [GeologicUnitOccurrenceInterpretation.1.0.0](../work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | allOf | one |  |
| Included by | [GeoReferencedImage.1.1.0](../work-product-component/GeoReferencedImage.1.1.0.md) | allOf | one |  |
| Included by | [GpGridRepresentation.1.0.0](../work-product-component/GpGridRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [GridConnectionSetRepresentation.1.0.0](../work-product-component/GridConnectionSetRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [HorizonInterpretation.1.0.0](../work-product-component/HorizonInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [IjkGridNumericalAquiferRepresentation.1.0.0](../work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [IjkGridRepresentation.1.0.0](../work-product-component/IjkGridRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [LocalBoundaryFeature.1.0.0](../work-product-component/LocalBoundaryFeature.1.0.0.md) | allOf | one |  |
| Included by | [LocalModelCompoundCrs.1.0.0](../work-product-component/LocalModelCompoundCrs.1.0.0.md) | allOf | one |  |
| Included by | [LocalModelFeature.1.0.0](../work-product-component/LocalModelFeature.1.0.0.md) | allOf | one |  |
| Included by | [LocalRockVolumeFeature.1.0.0](../work-product-component/LocalRockVolumeFeature.1.0.0.md) | allOf | one |  |
| Included by | [NotionalSeismicLine.1.0.0](../work-product-component/NotionalSeismicLine.1.0.0.md) | allOf | one |  |
| Included by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | allOf | one |  |
| Included by | [PlannedLithology.1.0.0](../work-product-component/PlannedLithology.1.0.0.md) | allOf | one |  |
| Included by | [PPFGDataset.1.0.0](../work-product-component/PPFGDataset.1.0.0.md) | allOf | one |  |
| Included by | [ProcessedInSAR.1.0.0](../work-product-component/ProcessedInSAR.1.0.0.md) | allOf | one |  |
| Included by | [ReservoirCompartmentInterpretation.1.0.0](../work-product-component/ReservoirCompartmentInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [RockFluidOrganizationInterpretation.1.0.0](../work-product-component/RockFluidOrganizationInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [RockFluidUnitInterpretation.1.0.0](../work-product-component/RockFluidUnitInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [RockImage.1.0.0](../work-product-component/RockImage.1.0.0.md) | allOf | one |  |
| Included by | [RockSampleAnalysis.1.0.0](../work-product-component/RockSampleAnalysis.1.0.0.md) | allOf | one |  |
| Included by | [RockSampleAnalysis.1.1.0](../work-product-component/RockSampleAnalysis.1.1.0.md) | allOf | one |  |
| Included by | [SealedSurfaceFramework.1.0.0](../work-product-component/SealedSurfaceFramework.1.0.0.md) | allOf | one |  |
| Included by | [SealedVolumeFramework.1.0.0](../work-product-component/SealedVolumeFramework.1.0.0.md) | allOf | one |  |
| Included by | [SeismicBinGrid.1.0.0](../work-product-component/SeismicBinGrid.1.0.0.md) | allOf | one |  |
| Included by | [SeismicBinGrid.1.1.0](../work-product-component/SeismicBinGrid.1.1.0.md) | allOf | one |  |
| Included by | [SeismicFault.1.0.0](../work-product-component/SeismicFault.1.0.0.md) | allOf | one |  |
| Included by | [SeismicHorizon.1.0.0](../work-product-component/SeismicHorizon.1.0.0.md) | allOf | one |  |
| Included by | [SeismicHorizon.1.1.0](../work-product-component/SeismicHorizon.1.1.0.md) | allOf | one |  |
| Included by | [SeismicLineGeometry.1.0.0](../work-product-component/SeismicLineGeometry.1.0.0.md) | allOf | one |  |
| Included by | [SeismicTraceData.1.0.0](../work-product-component/SeismicTraceData.1.0.0.md) | allOf | one |  |
| Included by | [SeismicTraceData.1.1.0](../work-product-component/SeismicTraceData.1.1.0.md) | allOf | one |  |
| Included by | [SeismicTraceData.1.2.0](../work-product-component/SeismicTraceData.1.2.0.md) | allOf | one |  |
| Included by | [SeismicTraceData.1.3.0](../work-product-component/SeismicTraceData.1.3.0.md) | allOf | one |  |
| Included by | [StratigraphicColumn.1.0.0](../work-product-component/StratigraphicColumn.1.0.0.md) | allOf | one |  |
| Included by | [StratigraphicColumnRankInterpretation.1.0.0](../work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [StratigraphicColumnRankInterpretation.1.1.0](../work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md) | allOf | one |  |
| Included by | [StratigraphicUnitInterpretation.1.0.0](../work-product-component/StratigraphicUnitInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [StructuralOrganizationInterpretation.1.0.0](../work-product-component/StructuralOrganizationInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [SubRepresentation.1.0.0](../work-product-component/SubRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [TimeSeries.1.0.0](../work-product-component/TimeSeries.1.0.0.md) | allOf | one |  |
| Included by | [TubularAssembly.1.0.0](../work-product-component/TubularAssembly.1.0.0.md) | allOf | one |  |
| Included by | [TubularComponent.1.0.0](../work-product-component/TubularComponent.1.0.0.md) | allOf | one |  |
| Included by | [TubularComponent.1.1.0](../work-product-component/TubularComponent.1.1.0.md) | allOf | one |  |
| Included by | [TubularUmbilical.1.0.0](../work-product-component/TubularUmbilical.1.0.0.md) | allOf | one |  |
| Included by | [UnsealedSurfaceFramework.1.0.0](../work-product-component/UnsealedSurfaceFramework.1.0.0.md) | allOf | one |  |
| Included by | [UnsealedSurfaceFramework.1.1.0](../work-product-component/UnsealedSurfaceFramework.1.1.0.md) | allOf | one |  |
| Included by | [UnstructuredColumnLayerGridRepresentation.1.0.0](../work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [UnstructuredGridRepresentation.1.0.0](../work-product-component/UnstructuredGridRepresentation.1.0.0.md) | allOf | one |  |
| Included by | [VelocityModeling.1.0.0](../work-product-component/VelocityModeling.1.0.0.md) | allOf | one |  |
| Included by | [VelocityModeling.1.1.0](../work-product-component/VelocityModeling.1.1.0.md) | allOf | one |  |
| Included by | [VoidageGroupInterpretation.1.0.0](../work-product-component/VoidageGroupInterpretation.1.0.0.md) | allOf | one |  |
| Included by | [WellboreIntervalSet.1.0.0](../work-product-component/WellboreIntervalSet.1.0.0.md) | allOf | one |  |
| Included by | [WellboreMarkerSet.1.0.0](../work-product-component/WellboreMarkerSet.1.0.0.md) | allOf | one |  |
| Included by | [WellboreMarkerSet.1.1.0](../work-product-component/WellboreMarkerSet.1.1.0.md) | allOf | one |  |
| Included by | [WellboreMarkerSet.1.2.0](../work-product-component/WellboreMarkerSet.1.2.0.md) | allOf | one |  |
| Included by | [WellboreMarkerSet.1.2.1](../work-product-component/WellboreMarkerSet.1.2.1.md) | allOf | one |  |
| Included by | [WellboreTrajectory.1.0.0](../work-product-component/WellboreTrajectory.1.0.0.md) | allOf | one |  |
| Included by | [WellboreTrajectory.1.1.0](../work-product-component/WellboreTrajectory.1.1.0.md) | allOf | one |  |
| Included by | [WellLog.1.0.0](../work-product-component/WellLog.1.0.0.md) | allOf | one |  |
| Included by | [WellLog.1.1.0](../work-product-component/WellLog.1.1.0.md) | allOf | one |  |
| Included by | [WellLog.1.2.0](../work-product-component/WellLog.1.2.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._