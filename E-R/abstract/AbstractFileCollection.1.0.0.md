<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractFileCollection [Status: Accepted]
The schema fragment representing file collections.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractFileCollection:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractFileCollection.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractFileCollection.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractFileCollection.1.0.0.json](../../Authoring/abstract/AbstractFileCollection.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractFileCollection.1.0.0.json](../../Generated/abstract/AbstractFileCollection.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractFileCollection.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractFileCollection.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractFileCollection


## Outgoing Relationships for AbstractFileCollection

![AbstractFileCollection](../_diagrams/abstract/AbstractFileCollection.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractFileCollection Referenced by other Entities

![AbstractFileCollection](../_diagrams/abstract/AbstractFileCollection.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFileCollection Properties

## Table of AbstractFileCollection Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_DatasetProperties_| |_object_|_required_|_Dataset Properties_|_The dataset properties for a file collection._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|DatasetProperties.FileCollectionPath| |string|required|File Collection Path|The mandatory path to the file collection. A FileCollectionPath should represent folder level access to a set of files.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://default_bucket/opendes/data/vds-dataset/|
|DatasetProperties.IndexFilePath| |string|optional|Index File Path|An optional path to an index file.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://default_bucket/opendes/data/vds-dataset/vds-dataset.index|
|_DatasetProperties.FileSourceInfos[]_|_[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)_|_object_|_optional_|_AbstractFileSourceInfo_|_Array of file collection members as FileSourceInfo. Fragment Description: A schema fragment to describe file source information._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|DatasetProperties.FileSourceInfos[].FileSource|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|required|File Source|The location of the file. It can be a relative path. The actual access is provided via the File Service. When used in context of a FileCollection (dataset--FileCollection*) FileSource is a relative path from the FileCollectionPath. It can be used by consumers to pull an individual file if they so choose by concatenating the FileCollectionPath with the FileSource. This property is required.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://default_bucket/r1/data/provided/documents/1000.witsml|
|DatasetProperties.FileSourceInfos[].PreloadFilePath|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Path|File system path to the data file as it existed before loading to the data platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://staging-area/r7/raw-data/provided/documents/1000.witsml|
|DatasetProperties.FileSourceInfos[].PreloadFileCreateUser|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Create User|Optional user name or reference, who created the file prior to up-loading to the platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|somebody@acme.org|
|DatasetProperties.FileSourceInfos[].PreloadFileCreateDate|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Create Date|Optional create date and time of the file prior to up-loading to the platform.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|2019-12-16T11:46:20.163Z|
|DatasetProperties.FileSourceInfos[].PreloadFileModifyUser|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Modify User|Optional user name or reference, who last modified the file prior to up-loading to the platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|somebody.else@acme.org|
|DatasetProperties.FileSourceInfos[].PreloadFileModifyDate|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Modify Date|Optional last modified date and time of the file prior to up-loading to the platform.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|2019-12-20T17:20:05.356Z|
|DatasetProperties.FileSourceInfos[].Name|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|Name|Optional, user-friendly file name.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1000.witsml|
|DatasetProperties.FileSourceInfos[].FileSize|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|File Size|Length of file in bytes. Implemented as string. The value must be convertible to a long integer (sizes can become very large).|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|95463|
|DatasetProperties.FileSourceInfos[].EncodingFormatTypeID &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|Encoding Format Type ID|Only used in FileCollection where the EncodingFormatType differs from data DatasetDefault specified in data.EncodingFormatTypeID: the media type specification for this dataset.|<code>^[\w\-\.]+:reference-data\-\-EncodingFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--EncodingFormatType:application%2Fgeo%2Bjson:|
|DatasetProperties.FileSourceInfos[].Checksum|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|Checksum|Checksum of file bytes - a hexadecimal number with even number of bytes.|<code>^([0-9a-fA-F]{2})+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|d41d8cd98f00b204e9800998ecf8427e|
|DatasetProperties.FileSourceInfos[].ChecksumAlgorithm|[AbstractFileSourceInfo.1.0.0](AbstractFileSourceInfo.1.0.0.md)|string|optional|Checksum Algorithm|The name of the checksum algorithm e.g. MD5, SHA-256.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|SHA-256|
|DatasetProperties.Checksum| |string|optional|MD5 Checksum|MD5 checksum of file bytes - a 32 byte hexadecimal number.|<code>^[0-9a-fA-F]{32}</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|d41d8cd98f00b204e9800998ecf8427e|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFileCollection.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [FileCollection.Bluware.OpenVDS.1.0.0](../dataset/FileCollection.Bluware.OpenVDS.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.Esri.Shape.1.0.0](../dataset/FileCollection.Esri.Shape.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.Generic.1.0.0](../dataset/FileCollection.Generic.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.SEGY.1.0.0](../dataset/FileCollection.SEGY.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.Slb.OpenZGY.1.0.0](../dataset/FileCollection.Slb.OpenZGY.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._