<a name="TOC"></a>

[[_TOC_]]

# Seismic Pre-Stack

## Seismic Acquisition Survey Updates

There have been updates to seismic schema to support pre-stack seismic. The updates are :
Seismic Acquisition Survey:
Added arrays Source Configurations and Receiver Configurations. Both are arrays to support multi-source vessel and
receiver vessels. New attributes are added:

### SourceConfiguration:

* VesselName,
* ShotpointSpacing,
* EnergySourceTypeID,
* SourceArrayCount,
* SourceArraySpacing,
* SourceArrayDepth,
* SourceArrayVolume,
* SourceArraySweepFreqMin,
* SourceArraySweepFreqMax,
* SourceArraySweepLength,
* Remarks

### ReceiverConfiguration:

* VesselName,
* CableCount,
* CableLength,
* CableSpacing,
* CableDepth,
* ReceiverTypeID,
* ReceiverCount,
* ReceiverSpacingInterval,
* ReceiverGroupSpacing,
* Remarks

### Deprecated:

These are deprecated in the main body, as they are now in `SourceConfigurations` or `ReceiverConfigurations` arrays:

* ShotpointIncrementDistance,
* EnergySourceTypeID,
* SourceArrayCount,
* SourceArraySeparationDistance,
* CableCount,
* CableLength,
* CableSpacingDistance,
* VesselNames.

For data migration, please see
the [migration guide for SeismicAcquisitionSurvey](../../../Guides/MigrationGuides/M10/SeismicAcquisitionSurvey.1.0.0.md)

[Back to TOC](#TOC)

### Manifest

The Volve acquisition survey manifest shows use of the source and receiver
arrays: 

<details>

<summary markdown="span">See Volve Acquisition Survey manifest example.</summary>

<OsduImportJSON>[Volve_acquisition_survey.json](manifests/Volve_acquisition_survey.json)</OsduImportJSON>

```json
{
  "kind": "osdu:wks:Manifest:1.0.0",
  "MasterData": [
    {
      "id": "osdu:master-data--SeismicAcquisitionSurvey:ST0202R08",
      "kind": "osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0",
      "acl": {
        "owners": [],
        "viewers": []
      },
      "legal": {
        "legaltags": [],
        "otherRelevantDataCountries": []
      },
      "meta": [
        {
          "kind": "Unit",
          "name": "s",
          "persistableReference": "{\\\"abcd\\\":{\\\"a\\\":0.0,\\\"b\\\":1.0,\\\"c\\\":1.0,\\\"d\\\":0.0},\\\"symbol\\\":\\\"s\\\",\\\"baseMeasurement\\\":{\\\"ancestry\\\":\\\"T\\\",\\\"type\\\":\\\"UM\\\"},\\\"type\\\":\\\"UAD\\\"}",
          "unitOfMeasureID": "osdu:reference-data--UnitOfMeasure:s:",
          "propertyNames": [
            "RecordLength"
          ]
        },
        {
          "kind": "Unit",
          "name": "m",
          "persistableReference": "{\\\"abcd\\\":{\\\"a\\\":0.0,\\\"b\\\":1.0,\\\"c\\\":1.0,\\\"d\\\":0.0},\\\"symbol\\\":\\\"m\\\",\\\"baseMeasurement\\\":{\\\"ancestry\\\":\\\"L\\\",\\\"type\\\":\\\"UM\\\"},\\\"type\\\":\\\"UAD\\\"}",
          "unitOfMeasureID": "osdu:reference-data--UnitOfMeasure:m:",
          "propertyNames": [
            "ShotpointIncrementDistance",
            "CableLength",
            "CableSpacingDistance",
            "MaxOffsetDistance"
          ]
        }
      ],
      "data": {
        "GeoContexts": [
          {
            "GeoPoliticalEntityID": "osdu:master-data--GeoPoliticalEntity:Norway_Country:",
            "GeoTypeID": "osdu:reference-data--GeoPoliticalEntityType:Country:"
          }
        ],
        "Purpose": "Acquisition for Volve",
        "ProjectEndDate": "2008-02-01T00:00:00",
        "Operator": "osdu:master-data--Organisation:Statoil:",
        "Contractors": [
          {
            "ContractorOrganisationID": "osdu:master-data--Organisation:WesternGeco:",
            "ContractorTypeID": "osdu:reference-data--ContractorType:Recording:"
          }
        ],
        "SeismicGeometryTypeID": "osdu:reference-data--SeismicGeometryType:3D:",
        "OperatingEnvironmentID": "osdu:reference-data--OperatingEnvironment:Offshore:",
        "SampleInterval": 4.0,
        "RecordLength": 10.2,
        "MaxOffsetDistance": 6000.0,
        "FoldCount": 120,
        "SourceConfigurations": [
          {
            "VesselName": "Geco Angler",
            "ShotpointSpacing": 25.0,
            "EnergySourceTypeID": "namespace:reference-data--SeismicEnergySourceType:AirGun:",
            "SourceArrayCount": 2,
            "SourceArraySpacing": 100.0,
            "Remarks": "Source configuration description"
          }
        ],
        "ReceiverConfigurations": [
          {
            "VesselName": "Geco Angler",
            "CableCount": 4,
            "CableLength": 6000.0,
            "CableSpacing": 400.0,
            "ReceiverTypeID": "namespace:reference-data--SeismicReceiverType:MarineStreamer:",
            "ReceiverCount": 2,
            "ReceiverGroupSpacing": 25.0,
            "Remarks": "Example Remarks"
          }
        ]
      }
    }
  ]
}
```

</details>

[Back to TOC](#TOC)

## Seismic Trace Data

### Schema

The [`osdu:wks:work-product-component--SeismicTraceData:1.1.0`](../../../E-R/work-product-component/SeismicTraceData.1.1.0.md)
has added attributes for use with pre-stack. The `GatherTypeID` and `SortOrderID` are specific to pre-stack.

Three arrays have been added to accommodate pre-stack interpretation. These are StackingAngleRanges,
StackingOffsetRanges, and StackingAzimuthRanges are arrays, containing Minimum and Maximum values.

The SeismicTraceData 1.1.0 schema also added values that are used only for post-stack. These are

* StackAngleRangeMax,
* StackAngleRangeMin,
* StackAzimuthRangeMax,
* StackAzimuthRangeMin,
* StackOffsetRangeMax,
* StackOffsetRangeMin.

[Back to TOC](#TOC)

### Manifest - Shot Gathers

The Shot Gathers manifest is used as an example to illustrate a shot gather manifest. The actual shot gathers are not
loaded.

The Volve manifest has the [SeismicGatherType](../../../E-R/reference-data/SeismicGatherType.1.0.0.md) set
to `CommonShot`. The [SeismicTraceSortOrder](../../../E-R/reference-data/SeismicTraceSortOrder.1.0.0.md) is set to
Shot.DetectLine.DetectPoint. (This indicates Traces are sorted by shot number, detect line or cable and detector point
or receiver, depending on land or marine acquisition.)
[Shot_gathers.json](manifests/Shot_gathers.json)

The illustration below shows the live trace outline of the shot gathers with the outline of the post-stack survey
inside. The pre-stack area is extends by 240 bins, which corresponds to 3000 m.

![LiveTraceOutlineVolve.png](Illustrations/LiveTraceOutlineVolve.png)

[Back to TOC](#TOC)

### Manifest - CMP Gathers

This is also a manifest based upon the Volve post-stack volume, but is not associated with an actual volume. The
manifest for CMP gathers has the `GatherTypeID` set to CMP, and the `SortOrderID` set to CMP.AbsoluteOffset. The
manifest also includes a reference to the processing project, and includes the processing parameters.
[See a CMP_gather manifest record](manifests/CMP_gathers.json)


[Back to TOC](#TOC)

### Manifest - Stacked and Migrated Volume

The manifest for the stacked and migrated volume includes
a [StackingType](../../../E-R/reference-data/SeismicStackingType.1.0.0.md)
and a [MigrationType](../../../E-R/reference-data/SeismicMigrationType.1.0.0.md). The processing parameters include
additional steps which include Tau-P filtering, pre-stack depth migration, and then conversion to time.
[stacked_migrated.json](manifests/stacked_migrated.json)

<details>

<summary markdown="span">See stacked_migrated.json</summary>

<OsduImportJSON>[stacked_migrated.json](manifests/stacked_migrated.json)</OsduImportJSON>

```json
{
  "kind": "osdu:wks:Manifest:1.0.0",
  "ReferenceData": [],
  "MasterData": [],
  "Data": {
    "WorkProduct": {
      "id": "surrogate-key:wp-1",
      "kind": "osdu:wks:work-product--WorkProduct:1.0.0",
      "acl": {
        "owners": [
          "data.volve.owners@volve.volve-company.com"
        ],
        "viewers": [
          "data.volve-shared.viewers@volve.volve-company.com"
        ]
      },
      "legal": {
        "legaltags": [
          "ExampleLegalTag"
        ],
        "otherRelevantDataCountries": [
          "NO",
          "US"
        ]
      },
      "data": {
        "ResourceSecurityClassification": "osdu:reference-data--ResourceSecurityClassification:RESTRICTED:",
        "Name": "ST0202R08_PS_PSDM_RAW_PP_TIME.MIG_RAW.POST_Stack.3D.JS-017534",
        "Description": "Volve - Seismic Trace Data - RAW PS PSDM Stack IN PP TIME",
        "Components": [
          "surrogate-key:wpc-1"
        ]
      }
    },
    "WorkProductComponents": [
      {
        "id": "surrogate-key:wpc-1",
        "kind": "osdu:wks:work-product-component--SeismicTraceData:1.3.0",
        "acl": {
          "owners": [
            "data.volve.owners@volve.volve-company.com"
          ],
          "viewers": [
            "data.volve-shared.viewers@volve.volve-company.com"
          ]
        },
        "legal": {
          "legaltags": [
            "ExampleLegalTag"
          ],
          "otherRelevantDataCountries": [
            "NO",
            "US"
          ]
        },
        "meta": [
          {
            "kind": "Unit",
            "name": "ms",
            "persistableReference": "{\"abcd\":{\"a\":0.0,\"b\":0.001,\"c\":1.0,\"d\":0.0},\"symbol\":\"ms\",\"baseMeasurement\":{\"ancestry\":\"T\",\"type\":\"UM\"},\"type\":\"UAD\"}",
            "unitOfMeasureID": "osdu:reference-data--UnitOfMeasure:ms:",
            "propertyNames": [
              "StartTime",
              "EndTime"
            ]
          },
          {
            "kind": "Unit",
            "name": "Amplitude",
            "persistableReference": "{\"abcd\":{\"a\":0.0,\"b\":1.0,\"c\":1.0,\"d\":0.0},\"symbol\":\"Euc\",\"baseMeasurement\":{\"ancestry\":\"1\",\"type\":\"UM\"},\"type\":\"UAD\"}",
            "unitOfMeasureID": "osdu:reference-data--UnitOfMeasure:Euc:",
            "propertyNames": [
              "RangeAmplitudeMax",
              "RangeAmplitudeMin"
            ]
          }
        ],
        "data": {
          "ResourceSecurityClassification": "osdu:reference-data--ResourceSecurityClassification:RESTRICTED:",
          "Name": "ST0202R08_PS_PSDM_RAW_PP_TIME.MIG_RAW.POST_Stack.3D.JS-017534",
          "Description": "Volve - Seismic Trace Data - RAW PS PSDM Stack IN PP TIME",
          "Datasets": [
            "surrogate-key:file-1"
          ],
          "SpatialArea": {
            "AsIngestedCoordinates": {
              "type": "AnyCrsFeatureCollection",
              "CoordinateReferenceSystemID": "osdu:reference-data--CoordinateReferenceSystem:BoundProjected:EPSG::23031_EPSG::1613:",
              "persistableReferenceCrs": "{\"authCode\":{\"auth\":\"OSDU\",\"code\":\"23031024\"},\"lateBoundCRS\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"23031\"},\"name\":\"ED_1950_UTM_Zone_31N\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"PROJCS[\\\"ED_1950_UTM_Zone_31N\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",3.0],PARAMETER[\\\"Scale_Factor\\\",0.9996],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0],AUTHORITY[\\\"EPSG\\\",23031]]\"},\"name\":\"ED50 * EPSG-Nor S62 2001 / UTM zone 31N [23031,1613]\",\"singleCT\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"1613\"},\"name\":\"ED_1950_To_WGS_1984_24\",\"type\":\"ST\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGTRAN[\\\"ED_1950_To_WGS_1984_24\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],METHOD[\\\"Position_Vector\\\"],PARAMETER[\\\"X_Axis_Translation\\\",-90.365],PARAMETER[\\\"Y_Axis_Translation\\\",-101.13],PARAMETER[\\\"Z_Axis_Translation\\\",-123.384],PARAMETER[\\\"X_Axis_Rotation\\\",0.333],PARAMETER[\\\"Y_Axis_Rotation\\\",0.077],PARAMETER[\\\"Z_Axis_Rotation\\\",0.894],PARAMETER[\\\"Scale_Difference\\\",1.994],AUTHORITY[\\\"EPSG\\\",1613]]\"},\"type\":\"EBC\",\"ver\":\"PE_10_9_1\"}",
              "features": [
                {
                  "type": "AnyCrsFeature",
                  "properties": {},
                  "geometry": {
                    "type": "AnyCrsPolygon",
                    "coordinates": [
                      [
                        [
                          438727.125,
                          6475514.5
                        ],
                        [
                          439888.34375,
                          6480172.0
                        ],
                        [
                          432562.59375,
                          6481998.5
                        ],
                        [
                          431401.375,
                          6477341.0
                        ],
                        [
                          438727.125,
                          6475514.5
                        ]
                      ]
                    ]
                  }
                }
              ]
            },
            "Wgs84Coordinates": {
              "type": "FeatureCollection",
              "features": [
                {
                  "type": "Feature",
                  "properties": {},
                  "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                      [
                        [
                          1.9496878,
                          58.4141503
                        ],
                        [
                          1.9683366,
                          58.4561357
                        ],
                        [
                          1.8422866,
                          58.4714655
                        ],
                        [
                          1.8237804,
                          58.4294624
                        ],
                        [
                          1.9496878,
                          58.4141503
                        ]
                      ]
                    ]
                  }
                }
              ]
            },
            "AppliedOperations": [
              "AsIngestedCoordinates converted to Wgs84Coordinates: Input CRS EPSG 23031 (ED50 / UTM zone 31N) to Target CRS EPSG 4326 (WGS84) using CT EPSG 1613 (ED50 to WGS 84 (24) - Norway - offshore south of 62°N - North Sea.)"
            ],
            "SpatialParameterTypeID": "osdu:reference-data--SpatialParameterType:Outline:",
            "SpatialGeometryTypeID": "osdu:reference-data--SpatialGeometryType:Polygon:"
          },
          "LiveTraceOutline": {
            "AsIngestedCoordinates": {
              "type": "AnyCrsFeatureCollection",
              "CoordinateReferenceSystemID": "osdu:reference-data--CoordinateReferenceSystem:BoundProjected:EPSG::23031_EPSG::1613:",
              "persistableReferenceCrs": "{\"authCode\":{\"auth\":\"OSDU\",\"code\":\"23031024\"},\"lateBoundCRS\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"23031\"},\"name\":\"ED_1950_UTM_Zone_31N\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"PROJCS[\\\"ED_1950_UTM_Zone_31N\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",3.0],PARAMETER[\\\"Scale_Factor\\\",0.9996],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0],AUTHORITY[\\\"EPSG\\\",23031]]\"},\"name\":\"ED50 * EPSG-Nor S62 2001 / UTM zone 31N [23031,1613]\",\"singleCT\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"1613\"},\"name\":\"ED_1950_To_WGS_1984_24\",\"type\":\"ST\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGTRAN[\\\"ED_1950_To_WGS_1984_24\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],METHOD[\\\"Position_Vector\\\"],PARAMETER[\\\"X_Axis_Translation\\\",-90.365],PARAMETER[\\\"Y_Axis_Translation\\\",-101.13],PARAMETER[\\\"Z_Axis_Translation\\\",-123.384],PARAMETER[\\\"X_Axis_Rotation\\\",0.333],PARAMETER[\\\"Y_Axis_Rotation\\\",0.077],PARAMETER[\\\"Z_Axis_Rotation\\\",0.894],PARAMETER[\\\"Scale_Difference\\\",1.994],AUTHORITY[\\\"EPSG\\\",1613]]\"},\"type\":\"EBC\",\"ver\":\"PE_10_9_1\"}",
              "features": [
                {
                  "type": "AnyCrsFeature",
                  "properties": {},
                  "geometry": {
                    "type": "AnyCrsPolygon",
                    "coordinates": [
                      [
                        [
                          438727.125,
                          6475514.5
                        ],
                        [
                          439888.34375,
                          6480172.0
                        ],
                        [
                          432562.59375,
                          6481998.5
                        ],
                        [
                          431401.375,
                          6477341.0
                        ],
                        [
                          438727.125,
                          6475514.5
                        ]
                      ]
                    ]
                  }
                }
              ]
            },
            "Wgs84Coordinates": {
              "type": "FeatureCollection",
              "features": [
                {
                  "type": "Feature",
                  "properties": {},
                  "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                      [
                        [
                          1.9496878,
                          58.4141503
                        ],
                        [
                          1.9683366,
                          58.4561357
                        ],
                        [
                          1.8422866,
                          58.4714655
                        ],
                        [
                          1.8237804,
                          58.4294624
                        ],
                        [
                          1.9496878,
                          58.4141503
                        ]
                      ]
                    ]
                  }
                }
              ]
            },
            "AppliedOperations": [
              "AsIngestedCoordinates converted to Wgs84Coordinates: Input CRS EPSG 23031 (ED50 / UTM zone 31N) to Target CRS EPSG 4326 (WGS84) using CT EPSG 1613 (ED50 to WGS 84 (24) - Norway - offshore south of 62°N - North Sea.)"
            ],
            "SpatialParameterTypeID": "osdu:reference-data--SpatialParameterType:Outline:",
            "SpatialGeometryTypeID": "osdu:reference-data--SpatialGeometryType:Polygon:"
          },
          "HorizontalCRSID": "osdu:reference-data--CoordinateReferenceSystem:23031:",
          "Preferred3DInterpretationSetID": "osdu:master-data--Seismic3DInterpretationSet:ST0202R08:",
          "PrincipalAcquisitionProjectID": "osdu:master-data--SeismicAcquisitionSurvey:ST0202R08:",
          "ProcessingProjectID": "osdu:master-data--SeismicProcessingProject:ST0202R08:",
          "SeismicTraceDataDimensionalityTypeID": "osdu:reference-data--SeismicTraceDataDimensionalityType:3D:",
          "SeismicDomainTypeID": "osdu:reference-data--SeismicDomainType:Time:",
          "SeismicMigrationTypeID": "osdu:reference-data--SeismicMigrationType:PSDM-Kirch:",
          "SeismicStackingTypeID": "osdu:reference-data--SeismicStackingType:Stk:",
          "SeismicFilteringTypeID": "osdu:reference-data--SeismicFilteringType:TauP:",
          "SeismicPhaseID": "osdu:reference-data--SeismicPhase:ZeroPhase:",
          "SeismicPolarityID": "osdu:reference-data--SeismicPolarity:PositiveSEG:",
          "SampleInterval": 4.0,
          "SampleCount": 1126,
          "Difference": false,
          "StartTime": 0.0,
          "EndTime": 4500.0,
          "TraceCount": 58479,
          "TraceLength": 4500.0,
          "TraceDomainUOM": "osdu:reference-data--UnitOfMeasure:ms:",
          "InlineMin": 9985,
          "InlineMax": 10369,
          "CrosslineMin": 1932,
          "CrosslineMax": 2536,
          "InlineIncrement": 2,
          "CrosslineIncrement": 2,
          "Precision": {
            "WordFormat": "osdu:reference-data--WordFormatType:IBMFLOAT:",
            "WordWidth": 4
          },
          "ProcessingParameters": [
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:Reformat:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:NavigationMerge:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:TraceEditing:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:Designature-Zerophase:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:TidalStatics:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:Resample:",
              "ProcessingParameterValue": "4 ms"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:GeophoneMatchingCorrection:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:GeophoneOrientationCorrection:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:RotationToRadial:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:NoiseBandEditing:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:ReceiverShearStaticCorrection:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:TaupDeconvolution:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:3DKirchhoffDepthMigration:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:VelocityAnalysisandRMOCorrection:",
              "ProcessingParameterValue": "200m X 200m"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:ConversionToPPTWT:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:Pre-RadonMildDipFilter:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:ParabolicRadonTransformDemultiple:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:AngleMute:",
              "ProcessingParameterValue": "3-37 Degrees"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:Stack:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:OutputSEGY:"
            }
          ],
          "TextualFileHeader": [
            "C 1 CLIENT=STATOIL,      PROCESSED BY=WESTERNGECO                               C 2 LINE ST0202D04-9985                                                         C 3 SURVEY:ST0202 ,   AREA: VOLVE 15/9                                          C 4 DATAFORMAT: SEG-Y,     DATE:02012008                                        C 5 DATATYPE: RAW PS PSDM Stack IN PP TIME                                      C 6 DATA SHOT BY/VESSEL:GECO ANGLER,  CABLE LENGTH:6000 M  NO OF GROUPS: 240x4  C 7 NO OF CABLES 2,      SOURCE:2,  POP INTERVAL 25.0 M (FLIP-FLOP)             C 8 RCV LINE SPACING: 400 M,    SRC LINE SPACING: 100 M, RECORD LENGTH: 10.2 S  C 9 GEODETIC DATUM: ED-50,      SPHEROID: INTERNAT., PROJECTION: UTM            C10 CENTR. MERID.: 03,          UTM ZONE: 31 N, FALSE EASTING: 500000           C11 FIRST SAMPLE= 0,            LAST SAMPLE= 4500, SAMPLE INTERVAL= 4 ms        C12 DIST. BETWEEN INLINES=25.0M, XLINES=25.0M BIN SIZE (I x X):12.5M x 12.5M    C13 INLINE DIRECTION (GRID): 284.0000000 DEGREES (CLOCKWISE FROM NORTH);        C14 X-LINE DIRECTION (GRID): 014.0000000 DEGREES (CLOCKWISE FROM NORTH);        C15 ORIGO(1.1) UTMX:431955.70E;    UTMY:6348582.15N  ;                          C16 DATA RANGE INLINES=9985-10369 (EVERY 2ND), X-LINES=1932-2536 (EVERY 2ND)    C17 CORNER1:3D INLINE  9985, 3D XLINE 1932, UTM-X 438727.0, UTM-Y 6475514.4     C18 CORNER2:3D INLINE  9985, 3D XLINE 2536, UTM-X 431401.3, UTM-Y 6477341.0     C19 CORNER3:3D INLINE 10369, 3D XLINE 2536, UTM-X 432562.5, UTM-Y 6481998.4     C20 CORNER4:3D INLINE 10369, 3D XLINE 1932, UTM-X 439888.3, UTM-Y 6480171.9     C21 LIVE DATA POLYGON: (9985,1932);(9985,2536);(10369,2536);(10369,1932);       C22 NAVIGATION SOURCE: P1/90 UKOOA BIN CENTER CELL GRID,                        C23 PROCESSING SEQUENCE:                                                        C24 1) Reformat 2) Navigation Merge 3) Trace Editing 4) Designature-Zerophase   C25 5) Tidal Statics 6) Resample 4MS 7) Geophone Matching Correction            C26 8) Geophone Orientation Correction 9) Rotation To Radial                    C27 10) Noise Band Editing 11) Receiver Shear Static Correction                 C28 12) Taup Deconvolution 13) 3D Kirchhoff Depth Migration                     C29 14) Velocity Analysis and RMO Correction 200m X 200m 15) Conversion To PP TWT    C30 16) Pre-Radon Mild Dip Filter 17) Parabolic Radon Transform Demultiple      C31 18) 3-37 DEGREE Angle Mute 19) Stack 20) OUTPUT TO SEGY FORMAT              C32                                                                             C33                                                                             C34                                                                             C35 HEADER WORD POSITIONS:                                                      C36 INLINE: 189-192             ;  X-LINE: 193-196;                             C37 BINX (CDPX): 181-184,          BINY (CDPY): 185-188,                        C38 MERID.: 3.0E, SPHEROID: INT.;  ROTATION (AMS): 1245600000,                  C39 A POSITIVE SAMPLE CORRESPONDS TO A INCREASE IN ACOUSTIC IMPEDANCE.          C40 END EBCDIC.                                                                 "
          ],
          "RangeAmplitudeMax": 0.07441109418869019,
          "RangeAmplitudeMin": -0.10446560382843018
        }
      }
    ],
    "Datasets": [
      {
        "id": "surrogate-key:file-1",
        "kind": "osdu:wks:dataset--FileCollection.SEGY:1.0.0",
        "acl": {
          "owners": [
            "data.volve.owners@volve.volve-company.com"
          ],
          "viewers": [
            "data.volve-shared.viewers@volve.volve-company.com"
          ]
        },
        "legal": {
          "legaltags": [
            "ExampleLegalTag"
          ],
          "otherRelevantDataCountries": [
            "NO",
            "US"
          ]
        },
        "data": {
          "ResourceSecurityClassification": "osdu:reference-data--ResourceSecurityClassification:RESTRICTED:",
          "Name": "ST0202R08_PS_PSDM_RAW_PP_TIME.MIG_RAW.POST_Stack.3D.JS-017534",
          "Description": "Volve - Seismic Trace Data - RAW PS PSDM Stack IN PP TIME",
          "TotalSize": "277427976",
          "Endian": "BIG",
          "DatasetProperties": {
            "FileCollectionPath": "s3://osdu-seismic-test-data/volve/seismic/st0202/stacks/",
            "FileSourceInfos": [
              {
                "FileSource": "",
                "PreloadFilePath": "s3://osdu-seismic-test-data/volve/seismic/st0202/stacks/ST0202R08_PS_PSDM_RAW_PP_TIME.MIG_RAW.POST_Stack.3D.JS-017534.segy",
                "Name": "ST0202R08_PS_PSDM_RAW_PP_TIME.MIG_RAW.POST_Stack.3D.JS-017534.segy",
                "FileSize": "277427976"
              }
            ],
            "Checksum": "c9df7234d5d0a7a2c2676ee2e2aa48b6"
          },
          "VectorHeaderMapping": [
            {
              "KeyName": "osdu:reference-data--HeaderKeyName:Inline:",
              "WordFormat": "osdu:reference-data--WordFormatType:INT:",
              "WordWidth": 4,
              "Position": 189
            },
            {
              "KeyName": "osdu:reference-data--HeaderKeyName:Crossline:",
              "WordFormat": "osdu:reference-data--WordFormatType:INT:",
              "WordWidth": 4,
              "Position": 193
            },
            {
              "KeyName": "osdu:reference-data--HeaderKeyName:CMPX:",
              "WordFormat": "osdu:reference-data--WordFormatType:INT:",
              "WordWidth": 4,
              "Position": 181,
              "UoM": "osdu:reference-data--UnitOfMeasure:m:",
              "ScalarIndicator": "OVERRIDE",
              "ScalarOverride": -100.0
            },
            {
              "KeyName": "osdu:reference-data--HeaderKeyName:CMPY:",
              "WordFormat": "osdu:reference-data--WordFormatType:INT:",
              "WordWidth": 4,
              "Position": 185,
              "UoM": "osdu:reference-data--UnitOfMeasure:m:",
              "ScalarIndicator": "OVERRIDE",
              "ScalarOverride": -100.0
            }
          ],
          "SEGYRevision": "Rev 1.0"
        }
      }
    ]
  }
}

```

</details>

[Back to TOC](#TOC)

### Manifest - Actual Gathers from Volve

The manifest for gathers from Volve is [here](manifests/CIP_prestkZ_PPTime_gathers.json). These gathers were pre-stack
depth-migrated and then converted back to time. The gather type is CIP, Common Image Point. The sorting is set to CMP:
Offset. The gathers are available for testing.

<details>

<summary markdown="span">See manifest for gathers from Volve</summary>

<OsduImportJSON>[manifest for gathers from Volve](manifests/CIP_prestkZ_PPTime_gathers.json)</OsduImportJSON>

```json
{
  "kind": "osdu:wks:Manifest:1.0.0",
  "ReferenceData": [],
  "MasterData": [],
  "Data": {
    "WorkProduct": {
      "id": "surrogate-key:wp-1",
      "kind": "osdu:wks:work-product--WorkProduct:1.0.0",
      "acl": {
        "owners": [
          "data.volve.owners@volve.volve-company.com"
        ],
        "viewers": [
          "data.volve-shared.viewers@volve.volve-company.com"
        ]
      },
      "legal": {
        "legaltags": [
          "ExampleLegalTag"
        ],
        "otherRelevantDataCountries": [
          "NO",
          "US"
        ]
      },
      "data": {
        "ResourceSecurityClassification": "osdu:reference-data--ResourceSecurityClassification:RESTRICTED:",
        "Name": "ST0202R08_PZ_PrSDM_CIP_gathers_in_PP_Time.segy",
        "Description": "Volve - Seismic Trace Data CIP Depth Gathers in time",
        "Components": [
          "surrogate-key:wpc-1"
        ]
      }
    },
    "WorkProductComponents": [
      {
        "id": "surrogate-key:wpc-1",
        "kind": "osdu:wks:work-product-component--SeismicTraceData:1.3.0",
        "acl": {
          "owners": [
            "data.volve.owners@volve.volve-company.com"
          ],
          "viewers": [
            "data.volve-shared.viewers@volve.volve-company.com"
          ]
        },
        "legal": {
          "legaltags": [
            "ExampleLegalTag"
          ],
          "otherRelevantDataCountries": [
            "NO",
            "US"
          ]
        },
        "meta": [
          {
            "kind": "Unit",
            "name": "ms",
            "persistableReference": "{\"abcd\":{\"a\":0.0,\"b\":0.001,\"c\":1.0,\"d\":0.0},\"symbol\":\"ms\",\"baseMeasurement\":{\"ancestry\":\"T\",\"type\":\"UM\"},\"type\":\"UAD\"}",
            "unitOfMeasureID": "osdu:reference-data--UnitOfMeasure:ms:",
            "propertyNames": [
              "StartTime",
              "EndTime"
            ]
          },
          {
            "kind": "Unit",
            "name": "Amplitude",
            "persistableReference": "{\"abcd\":{\"a\":0.0,\"b\":1.0,\"c\":1.0,\"d\":0.0},\"symbol\":\"Euc\",\"baseMeasurement\":{\"ancestry\":\"1\",\"type\":\"UM\"},\"type\":\"UAD\"}",
            "unitOfMeasureID": "osdu:reference-data--UnitOfMeasure:Euc:",
            "propertyNames": [
              "RangeAmplitudeMax",
              "RangeAmplitudeMin"
            ]
          }
        ],
        "data": {
          "ResourceSecurityClassification": "osdu:reference-data--ResourceSecurityClassification:RESTRICTED:",
          "Name": "ST0202R08_PZ_PrSDM_CIP_gathers_in_PP_Time.segy",
          "Description": "Volve - Seismic Trace Data - RST0202R08 3D Line 9985 PZ PrstkDM gathers in  PP TIME",
          "Datasets": [
            "surrogate-key:file-1"
          ],
          "SpatialArea": {
            "AsIngestedCoordinates": {
              "type": "AnyCrsFeatureCollection",
              "CoordinateReferenceSystemID": "osdu:reference-data--CoordinateReferenceSystem:BoundProjected:EPSG::23031_EPSG::1613:",
              "persistableReferenceCrs": "{\"authCode\":{\"auth\":\"OSDU\",\"code\":\"23031024\"},\"lateBoundCRS\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"23031\"},\"name\":\"ED_1950_UTM_Zone_31N\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"PROJCS[\\\"ED_1950_UTM_Zone_31N\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",3.0],PARAMETER[\\\"Scale_Factor\\\",0.9996],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0],AUTHORITY[\\\"EPSG\\\",23031]]\"},\"name\":\"ED50 * EPSG-Nor S62 2001 / UTM zone 31N [23031,1613]\",\"singleCT\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"1613\"},\"name\":\"ED_1950_To_WGS_1984_24\",\"type\":\"ST\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGTRAN[\\\"ED_1950_To_WGS_1984_24\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],METHOD[\\\"Position_Vector\\\"],PARAMETER[\\\"X_Axis_Translation\\\",-90.365],PARAMETER[\\\"Y_Axis_Translation\\\",-101.13],PARAMETER[\\\"Z_Axis_Translation\\\",-123.384],PARAMETER[\\\"X_Axis_Rotation\\\",0.333],PARAMETER[\\\"Y_Axis_Rotation\\\",0.077],PARAMETER[\\\"Z_Axis_Rotation\\\",0.894],PARAMETER[\\\"Scale_Difference\\\",1.994],AUTHORITY[\\\"EPSG\\\",1613]]\"},\"type\":\"EBC\",\"ver\":\"PE_10_9_1\"}",
              "features": [
                {
                  "type": "AnyCrsFeature",
                  "properties": {},
                  "geometry": {
                    "type": "AnyCrsPolygon",
                    "coordinates": [
                      [
                        [
                          438727.125,
                          6475514.5
                        ],
                        [
                          439888.34375,
                          6480172.0
                        ],
                        [
                          432562.59375,
                          6481998.5
                        ],
                        [
                          431401.375,
                          6477341.0
                        ],
                        [
                          438727.125,
                          6475514.5
                        ]
                      ]
                    ]
                  }
                }
              ]
            },
            "Wgs84Coordinates": {
              "type": "FeatureCollection",
              "features": [
                {
                  "type": "Feature",
                  "properties": {},
                  "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                      [
                        [
                          1.9496878,
                          58.4141503
                        ],
                        [
                          1.9683366,
                          58.4561357
                        ],
                        [
                          1.8422866,
                          58.4714655
                        ],
                        [
                          1.8237804,
                          58.4294624
                        ],
                        [
                          1.9496878,
                          58.4141503
                        ]
                      ]
                    ]
                  }
                }
              ]
            },
            "AppliedOperations": [
              "AsIngestedCoordinates converted to Wgs84Coordinates: Input CRS EPSG 23031 (ED50 / UTM zone 31N) to Target CRS EPSG 4326 (WGS84) using CT EPSG 1613 (ED50 to WGS 84 (24) - Norway - offshore south of 62°N - North Sea.)"
            ],
            "SpatialParameterTypeID": "osdu:reference-data--SpatialParameterType:Outline:",
            "SpatialGeometryTypeID": "osdu:reference-data--SpatialGeometryType:Polygon:"
          },
          "LiveTraceOutline": {
            "AsIngestedCoordinates": {
              "type": "AnyCrsFeatureCollection",
              "CoordinateReferenceSystemID": "osdu:reference-data--CoordinateReferenceSystem:BoundProjected:EPSG::23031_EPSG::1613:",
              "persistableReferenceCrs": "{\"authCode\":{\"auth\":\"OSDU\",\"code\":\"23031024\"},\"lateBoundCRS\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"23031\"},\"name\":\"ED_1950_UTM_Zone_31N\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"PROJCS[\\\"ED_1950_UTM_Zone_31N\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",3.0],PARAMETER[\\\"Scale_Factor\\\",0.9996],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0],AUTHORITY[\\\"EPSG\\\",23031]]\"},\"name\":\"ED50 * EPSG-Nor S62 2001 / UTM zone 31N [23031,1613]\",\"singleCT\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"1613\"},\"name\":\"ED_1950_To_WGS_1984_24\",\"type\":\"ST\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGTRAN[\\\"ED_1950_To_WGS_1984_24\\\",GEOGCS[\\\"GCS_European_1950\\\",DATUM[\\\"D_European_1950\\\",SPHEROID[\\\"International_1924\\\",6378388.0,297.0]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],METHOD[\\\"Position_Vector\\\"],PARAMETER[\\\"X_Axis_Translation\\\",-90.365],PARAMETER[\\\"Y_Axis_Translation\\\",-101.13],PARAMETER[\\\"Z_Axis_Translation\\\",-123.384],PARAMETER[\\\"X_Axis_Rotation\\\",0.333],PARAMETER[\\\"Y_Axis_Rotation\\\",0.077],PARAMETER[\\\"Z_Axis_Rotation\\\",0.894],PARAMETER[\\\"Scale_Difference\\\",1.994],AUTHORITY[\\\"EPSG\\\",1613]]\"},\"type\":\"EBC\",\"ver\":\"PE_10_9_1\"}",
              "features": [
                {
                  "type": "AnyCrsFeature",
                  "properties": {},
                  "geometry": {
                    "type": "AnyCrsPolygon",
                    "coordinates": [
                      [
                        [
                          438727.125,
                          6475514.5
                        ],
                        [
                          439888.34375,
                          6480172.0
                        ],
                        [
                          432562.59375,
                          6481998.5
                        ],
                        [
                          431401.375,
                          6477341.0
                        ],
                        [
                          438727.125,
                          6475514.5
                        ]
                      ]
                    ]
                  }
                }
              ]
            },
            "Wgs84Coordinates": {
              "type": "FeatureCollection",
              "features": [
                {
                  "type": "Feature",
                  "properties": {},
                  "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                      [
                        [
                          1.9496878,
                          58.4141503
                        ],
                        [
                          1.9683366,
                          58.4561357
                        ],
                        [
                          1.8422866,
                          58.4714655
                        ],
                        [
                          1.8237804,
                          58.4294624
                        ],
                        [
                          1.9496878,
                          58.4141503
                        ]
                      ]
                    ]
                  }
                }
              ]
            },
            "AppliedOperations": [
              "AsIngestedCoordinates converted to Wgs84Coordinates: Input CRS EPSG 23031 (ED50 / UTM zone 31N) to Target CRS EPSG 4326 (WGS84) using CT EPSG 1613 (ED50 to WGS 84 (24) - Norway - offshore south of 62°N - North Sea.)"
            ],
            "SpatialParameterTypeID": "osdu:reference-data--SpatialParameterType:Outline:",
            "SpatialGeometryTypeID": "osdu:reference-data--SpatialGeometryType:Polygon:"
          },
          "HorizontalCRSID": "osdu:reference-data--CoordinateReferenceSystem:23031:",
          "PrincipalAcquisitionProjectID": "osdu:master-data--SeismicAcquisitionSurvey:ST0202R08:",
          "ProcessingProjectID": "osdu:master-data--SeismicProcessingProject:ST0202R08:",
          "SeismicTraceDataDimensionalityTypeID": "osdu:reference-data--SeismicTraceDataDimensionalityType:3D:",
          "SeismicDomainTypeID": "osdu:reference-data--SeismicDomainType:Time:",
          "SeismicFilteringTypeID": "osdu:reference-data--SeismicFilteringType:TauP:",
          "SeismicPhaseID": "osdu:reference-data--SeismicPhase:ZeroPhase:",
          "SeismicPolarityID": "osdu:reference-data--SeismicPolarity:PositiveSEG:",
          "SampleInterval": 4.0,
          "SampleCount": 1126,
          "Difference": false,
          "StartTime": 0.0,
          "EndTime": 10200.0,
          "TraceCount": 4281512,
          "TraceLength": 4500.0,
          "TraceDomainUOM": "osdu:reference-data--UnitOfMeasure:ms:",
          "InlineMin": 9985,
          "InlineMax": 10369,
          "CrosslineMin": 1932,
          "CrosslineMax": 2536,
          "InlineIncrement": 2,
          "CrosslineIncrement": 2,
          "Precision": {
            "WordFormat": "osdu:reference-data--WordFormatType:IBMFLOAT:",
            "WordWidth": 4
          },
          "SortOrderID": "namespace:reference-data--SeismicTraceSortOrder:CIP.AbsoluteOffset:",
          "GatherTypeID": "namespace:reference-data--SeismicGatherType:CIP:",
          "ProcessingParameters": [
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:Reformat:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:NavigationMerge:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:TraceEditing:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:Designature-Zerophase:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:TidalStatics:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:Resample:",
              "ProcessingParameterValue": "4 ms"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:GeophoneMatchingCorrection:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:GeophoneOrientationCorrection:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:GatherCMP:"
            },
            {
              "ProcessingParameterTypeID": "osdu:reference-data--ProcessingParameterType:OutputSEGY:"
            }
          ],
          "TextualFileHeader": [
            "C 1 CLIENT=STATOIL,      PROCESSED BY=WESTERNGECO                               C 2 LINE ST0202D04-9985                                                         C 3 SURVEY:ST0202 ,   AREA: VOLVE 15/9                                          C 4 DATAFORMAT: SEG-Y,     DATE:02012008                                        C 5 DATATYPE: RAW PS PSDM Stack IN PP TIME                                      C 6 DATA SHOT BY/VESSEL:GECO ANGLER,  CABLE LENGTH:6000 M  NO OF GROUPS: 240x4  C 7 NO OF CABLES 2,      SOURCE:2,  POP INTERVAL 25.0 M (FLIP-FLOP)             C 8 RCV LINE SPACING: 400 M,    SRC LINE SPACING: 100 M, RECORD LENGTH: 10.2 S  C 9 GEODETIC DATUM: ED-50,      SPHEROID: INTERNAT., PROJECTION: UTM            C10 CENTR. MERID.: 03,          UTM ZONE: 31 N, FALSE EASTING: 500000           C11 FIRST SAMPLE= 0,            LAST SAMPLE= 4500, SAMPLE INTERVAL= 4 ms        C12 DIST. BETWEEN INLINES=25.0M, XLINES=25.0M BIN SIZE (I x X):12.5M x 12.5M    C13 INLINE DIRECTION (GRID): 284.0000000 DEGREES (CLOCKWISE FROM NORTH);        C14 X-LINE DIRECTION (GRID): 014.0000000 DEGREES (CLOCKWISE FROM NORTH);        C15 ORIGO(1.1) UTMX:431955.70E;    UTMY:6348582.15N  ;                          C16 DATA RANGE INLINES=9985-10369 (EVERY 2ND), X-LINES=1932-2536 (EVERY 2ND)    C17 CORNER1:3D INLINE  9985, 3D XLINE 1932, UTM-X 438727.0, UTM-Y 6475514.4     C18 CORNER2:3D INLINE  9985, 3D XLINE 2536, UTM-X 431401.3, UTM-Y 6477341.0     C19 CORNER3:3D INLINE 10369, 3D XLINE 2536, UTM-X 432562.5, UTM-Y 6481998.4     C20 CORNER4:3D INLINE 10369, 3D XLINE 1932, UTM-X 439888.3, UTM-Y 6480171.9     C21 LIVE DATA POLYGON: (9985,1932);(9985,2536);(10369,2536);(10369,1932);       C22 NAVIGATION SOURCE: P1/90 UKOOA BIN CENTER CELL GRID,                        C23 PROCESSING SEQUENCE:                                                        C24 1) Reformat 2) Navigation Merge 3) Trace Editing 4) Designature-Zerophase   C25 5) Tidal Statics 6) Resample 4MS 7) Geophone Matching Correction            C26 8) Geophone Orientation Correction 9) Rotation To Radial                    C27 10) Noise Band Editing 11) Receiver Shear Static Correction                 C28 12) Taup Deconvolution 13) 3D Kirchhoff Depth Migration                     C29 14) Velocity Analysis and RMO Correction 200m X 200m 15) Conversion To PP TWT    C30 16) Pre-Radon Mild Dip Filter 17) Parabolic Radon Transform Demultiple      C31 18) 3-37 DEGREE Angle Mute 19) Stack 20) OUTPUT TO SEGY FORMAT              C32                                                                             C33                                                                             C34                                                                             C35 HEADER WORD POSITIONS:                                                      C36 INLINE: 189-192             ;  X-LINE: 193-196;                             C37 BINX (CDPX): 181-184,          BINY (CDPY): 185-188,                        C38 MERID.: 3.0E, SPHEROID: INT.;  ROTATION (AMS): 1245600000,                  C39 A POSITIVE SAMPLE CORRESPONDS TO A INCREASE IN ACOUSTIC IMPEDANCE.          C40 END EBCDIC.                                                                 "
          ],
          "RangeAmplitudeMax": 0.07441109418869019,
          "RangeAmplitudeMin": -0.10446560382843018
        }
      }
    ],
    "Datasets": [
      {
        "id": "surrogate-key:file-1",
        "kind": "osdu:wks:dataset--FileCollection.SEGY:1.0.0",
        "acl": {
          "owners": [
            "data.volve.owners@volve.volve-company.com"
          ],
          "viewers": [
            "data.volve-shared.viewers@volve.volve-company.com"
          ]
        },
        "legal": {
          "legaltags": [
            "ExampleLegalTag"
          ],
          "otherRelevantDataCountries": [
            "NO",
            "US"
          ]
        },
        "data": {
          "ResourceSecurityClassification": "osdu:reference-data--ResourceSecurityClassification:RESTRICTED:",
          "Name": "ST0202R08_PS_PSDM_RAW_PP_TIME.MIG_RAW.POST_Stack.3D.JS-017534",
          "Description": "Volve - Seismic Trace Data - PSDM Line 9985 CIP Gathers in PP time",
          "TotalSize": "277427976",
          "Endian": "BIG",
          "DatasetProperties": {
            "FileCollectionPath": "s3://osdu-seismic-test-data/volve/seismic/st0202/stacks/",
            "FileSourceInfos": [
              {
                "FileSource": "",
                "PreloadFilePath": "s3://osdu-seismic-test-data/volve/seismic/st0202/prestacks/ ST0202R08_PZ_PrSDM_CIP_gathers_in_PP_Time.segy",
                "Name": "ST0202R08_PS_PSDM_RAW_PP_TIME.MIG_RAW.POST_Stack.3D.JS-017534.segy",
                "FileSize": "277427976"
              }
            ]
          },
          "VectorHeaderMapping": [
            {
              "KeyName": "osdu:reference-data--HeaderKeyName:Inline:",
              "WordFormat": "osdu:reference-data--WordFormatType:INT:",
              "WordWidth": 4,
              "Position": 189
            },
            {
              "KeyName": "osdu:reference-data--HeaderKeyName:Crossline:",
              "WordFormat": "osdu:reference-data--WordFormatType:INT:",
              "WordWidth": 4,
              "Position": 193
            },
            {
              "KeyName": "osdu:reference-data--HeaderKeyName:CMPX:",
              "WordFormat": "osdu:reference-data--WordFormatType:INT:",
              "WordWidth": 4,
              "Position": 181,
              "UoM": "osdu:reference-data--UnitOfMeasure:m:",
              "ScalarIndicator": "OVERRIDE",
              "ScalarOverride": -100.0
            },
            {
              "KeyName": "osdu:reference-data--HeaderKeyName:CMPY:",
              "WordFormat": "osdu:reference-data--WordFormatType:INT:",
              "WordWidth": 4,
              "Position": 185,
              "UoM": "osdu:reference-data--UnitOfMeasure:m:",
              "ScalarIndicator": "OVERRIDE",
              "ScalarOverride": -100.0
            }
          ],
          "SEGYRevision": "Rev 1.0"
        }
      }
    ]
  }
}

```

</details>

When preparing manifests, the first step is extracting the ebcdic header and trace headers. The header report for the
gathers is [here](manifests/ST0202R08_PZ_PrSDM_CIP_gathers_in_PP_Time.header.txt).

## Schema Usage Guide

A more general explanation of the OSDU seismic entities is provided in the 
[Schema Usage Guide Chapter _Seismic Projects and Related Data_](../../../Guides/Chapters/09-SeismicProjectsAndRelatedData.md). 

[Back to TOC](#TOC)
