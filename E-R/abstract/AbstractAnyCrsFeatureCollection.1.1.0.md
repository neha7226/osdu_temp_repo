<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractAnyCrsFeatureCollection
A schema like GeoJSON FeatureCollection with a non-WGS 84 CRS context; based on https://geojson.org/schema/FeatureCollection.json. Attention: the coordinate order is fixed: Longitude/Easting/Westing/X first, followed by Latitude/Northing/Southing/Y, optionally height as third coordinate.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractAnyCrsFeatureCollection:1.1.0`
* Schema status: PUBLISHED
* **Migration Guide (M12)** [`osdu:wks:AbstractAnyCrsFeatureCollection:1.0.0` &rarr; `osdu:wks:AbstractAnyCrsFeatureCollection:1.1.0`](../../Guides/MigrationGuides/M12/AbstractAnyCrsFeatureCollection.1.0.0.md)
* Link to &rarr; [Authoring Schema AbstractAnyCrsFeatureCollection.1.1.0.json](../../Authoring/abstract/AbstractAnyCrsFeatureCollection.1.1.0.json)
* Link to &rarr; [Generated Schema AbstractAnyCrsFeatureCollection.1.1.0.json](../../Generated/abstract/AbstractAnyCrsFeatureCollection.1.1.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractAnyCrsFeatureCollection.1.1.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractAnyCrsFeatureCollection.1.1.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractAnyCrsFeatureCollection


## Outgoing Relationships for AbstractAnyCrsFeatureCollection

![AbstractAnyCrsFeatureCollection](../_diagrams/abstract/AbstractAnyCrsFeatureCollection.1.1.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractAnyCrsFeatureCollection Referenced by other Entities

![AbstractAnyCrsFeatureCollection](../_diagrams/abstract/AbstractAnyCrsFeatureCollection.1.1.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractAnyCrsFeatureCollection Properties

## 1. Table of AbstractAnyCrsFeatureCollection Properties (Version 1.1.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|type| |enum string|required|Type|(No description)|<code>AnyCrsFeatureCollection</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsFeatureCollection|
|CoordinateReferenceSystemID &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)| |string|optional|Coordinate Reference System ID|The CRS reference into the CoordinateReferenceSystem catalog.|<code>^[\w\-\.]+:reference-data\-\-CoordinateReferenceSystem:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--CoordinateReferenceSystem:BoundProjected:EPSG::32021_EPSG::15851:|
|VerticalCoordinateReferenceSystemID &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)| |string|optional|Vertical Coordinate Reference System ID|The explicit VerticalCRS reference into the CoordinateReferenceSystem catalog. This property stays empty for 2D geometries. Absent or empty values for 3D geometries mean the context may be provided by a CompoundCRS in 'CoordinateReferenceSystemID' or implicitly EPSG:5714 MSL height|<code>^[\w\-\.]+:reference-data\-\-CoordinateReferenceSystem:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--CoordinateReferenceSystem:Vertical:EPSG::5714:|
|VerticalUnitID &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)| |string|optional|Vertical Unit ID|The explicit vertical unit ID, referring to a reference-data--UnitOfMeasure record; this is only required for features containing 3-dimensional coordinates and undefined vertical CoordinateReferenceSystems; if a VerticalCoordinateReferenceSystemID is populated, the VerticalUnitID is given by the VerticalCoordinateReferenceSystemID's data.CoordinateSystem.VerticalAxisUnitID. The VerticalUnitID definition overrides any self-contained definition in persistableReferenceUnitZ.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--UnitOfMeasure:m:|
|persistableReferenceCrs| |string|required|CRS Reference|The CRS reference as persistableReference string. If populated, the CoordinateReferenceSystemID takes precedence.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"authCode":{"auth":"OSDU","code":"32021079"},"lateBoundCRS":{"authCode":{"auth":"EPSG","code":"32021"},"name":"NAD_1927_StatePlane_North_Dakota_South_FIPS_3302","type":"LBC","ver":"PE_10_9_1","wkt":"PROJCS[\"NAD_1927_StatePlane_North_Dakota_South_FIPS_3302\",GEOGCS[\"GCS_North_American_1927\",DATUM[\"D_North_American_1927\",SPHEROID[\"Clarke_1866\",6378206.4,294.9786982]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Lambert_Conformal_Conic\"],PARAMETER[\"False_Easting\",2000000.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",-100.5],PARAMETER[\"Standard_Parallel_1\",46.18333333333333],PARAMETER[\"Standard_Parallel_2\",47.48333333333333],PARAMETER[\"Latitude_Of_Origin\",45.66666666666666],UNIT[\"Foot_US\",0.3048006096012192],AUTHORITY[\"EPSG\",32021]]"},"name":"NAD27 * OGP-Usa Conus / North Dakota CS27 South zone [32021,15851]","singleCT":{"authCode":{"auth":"EPSG","code":"15851"},"name":"NAD_1927_To_WGS_1984_79_CONUS","type":"ST","ver":"PE_10_9_1","wkt":"GEOGTRAN[\"NAD_1927_To_WGS_1984_79_CONUS\",GEOGCS[\"GCS_North_American_1927\",DATUM[\"D_North_American_1927\",SPHEROID[\"Clarke_1866\",6378206.4,294.9786982]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],GEOGCS[\"GCS_WGS_1984\",DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137.0,298.257223563]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],METHOD[\"NADCON\"],PARAMETER[\"Dataset_conus\",0.0],OPERATIONACCURACY[5.0],AUTHORITY[\"EPSG\",15851]]"},"type":"EBC","ver":"PE_10_9_1"}|
|persistableReferenceVerticalCrs| |string|optional|Vertical CRS Reference|The VerticalCRS reference as persistableReference string. If populated, the VerticalCoordinateReferenceSystemID takes precedence. The property is null or empty for 2D geometries. For 3D geometries and absent or null persistableReferenceVerticalCrs the vertical CRS is either provided via persistableReferenceCrs's CompoundCRS or it is implicitly defined as EPSG:5714 MSL height.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"authCode":{"auth":"EPSG","code":"5714"},"name":"MSL_Height","type":"LBC","ver":"PE_10_9_1","wkt":"VERTCS[\"MSL_Height\",VDATUM[\"Mean_Sea_Level\"],PARAMETER[\"Vertical_Shift\",0.0],PARAMETER[\"Direction\",1.0],UNIT[\"Meter\",1.0],AUTHORITY[\"EPSG\",5714]]"}|
|persistableReferenceUnitZ| |string|optional|Z-Unit Reference|The unit of measure for the Z-axis (only for 3-dimensional coordinates, where the CRS does not describe the vertical unit). Note that the direction is upwards positive, i.e. Z means height.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"scaleOffset":{"scale":1.0,"offset":0.0},"symbol":"m","baseMeasurement":{"ancestry":"Length","type":"UM"},"type":"USO"}|
|_features[]_| |_object_|_required_|_AnyCrsGeoJSON Feature_|_-_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].type| |enum string|required|Type|(No description)|<code>AnyCrsFeature</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsFeature|
|features[].properties|oneOf[1]|object|required|Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON Point

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[1]_|_object_|_required_|_AnyCrsGeoJSON Point_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[1]|enum string|required|Type|(No description)|<code>AnyCrsPoint</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsPoint|
|features[].geometry.coordinates[]|oneOf[1]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[1]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON LineString

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[2]_|_object_|_required_|_AnyCrsGeoJSON LineString_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[2]|enum string|required|Type|(No description)|<code>AnyCrsLineString</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsLineString|
|features[].geometry.coordinates[][]|oneOf[2]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[2]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON Polygon

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[3]_|_object_|_required_|_AnyCrsGeoJSON Polygon_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[3]|enum string|required|Type|(No description)|<code>AnyCrsPolygon</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsPolygon|
|features[].geometry.coordinates[][][]|oneOf[3]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[3]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON MultiPoint

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[4]_|_object_|_required_|_AnyCrsGeoJSON MultiPoint_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[4]|enum string|required|Type|(No description)|<code>AnyCrsMultiPoint</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsMultiPoint|
|features[].geometry.coordinates[][]|oneOf[4]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[4]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 6. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON MultiLineString

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[5]_|_object_|_required_|_AnyCrsGeoJSON MultiLineString_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[5]|enum string|required|Type|(No description)|<code>AnyCrsMultiLineString</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsMultiLineString|
|features[].geometry.coordinates[][][]|oneOf[5]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[5]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 7. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON MultiPolygon

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[6]_|_object_|_required_|_AnyCrsGeoJSON MultiPolygon_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[6]|enum string|required|Type|(No description)|<code>AnyCrsMultiPolygon</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsMultiPolygon|
|features[].geometry.coordinates[][][][]|oneOf[6]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[6]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 8. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON GeometryCollection

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[7]_|_object_|_required_|_AnyCrsGeoJSON GeometryCollection_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[7]|enum string|required|Type|(No description)|<code>AnyCrsGeometryCollection</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsGeometryCollection|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 9. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON Point

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[0]_|_object_|_required_|_AnyCrsGeoJSON Point_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[0]|enum string|required|Type|(No description)|<code>AnyCrsPoint</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsPoint|
|features[].geometry.geometries[].coordinates[]|oneOf[7] oneOf[0]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[0]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 10. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON LineString

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[1]_|_object_|_required_|_AnyCrsGeoJSON LineString_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[1]|enum string|required|Type|(No description)|<code>AnyCrsLineString</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsLineString|
|features[].geometry.geometries[].coordinates[][]|oneOf[7] oneOf[1]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[1]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 11. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON Polygon

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[2]_|_object_|_required_|_AnyCrsGeoJSON Polygon_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[2]|enum string|required|Type|(No description)|<code>AnyCrsPolygon</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsPolygon|
|features[].geometry.geometries[].coordinates[][][]|oneOf[7] oneOf[2]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[2]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 12. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON MultiPoint

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[3]_|_object_|_required_|_AnyCrsGeoJSON MultiPoint_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[3]|enum string|required|Type|(No description)|<code>AnyCrsMultiPoint</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsMultiPoint|
|features[].geometry.geometries[].coordinates[][]|oneOf[7] oneOf[3]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[3]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 13. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON MultiLineString

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[4]_|_object_|_required_|_AnyCrsGeoJSON MultiLineString_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[4]|enum string|required|Type|(No description)|<code>AnyCrsMultiLineString</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsMultiLineString|
|features[].geometry.geometries[].coordinates[][][]|oneOf[7] oneOf[4]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[4]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 14. Table of AbstractAnyCrsFeatureCollection Data Properties, Section AnyCrsGeoJSON MultiPolygon

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[5]_|_object_|_required_|_AnyCrsGeoJSON MultiPolygon_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[5]|enum string|required|Type|(No description)|<code>AnyCrsMultiPolygon</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AnyCrsMultiPolygon|
|features[].geometry.geometries[].coordinates[][][][]|oneOf[7] oneOf[5]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[5]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[7]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].bbox[]| |number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|bbox[]| |number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractAnyCrsFeatureCollection.1.1.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md) | AsIngestedCoordinates | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._