<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# Abstract Contact [Status: Accepted]
An object with properties that describe a specific person or other point-of-contact (like an email distribution list) that is relevant in this context (like a given data set or business project). The contact specified may be either internal or external to the organisation (something denoted via the Organisation object that is referenced). Note that some properties contain personally identifiable information, so it might not be appropriate to populate all properties in all scenarios.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractContact:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractContact.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractContact.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractContact.1.0.0.json](../../Authoring/abstract/AbstractContact.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractContact.1.0.0.json](../../Generated/abstract/AbstractContact.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractContact.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractContact.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams Abstract Contact


## Outgoing Relationships for Abstract Contact

![Abstract Contact](../_diagrams/abstract/AbstractContact.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Abstract Contact Referenced by other Entities

![Abstract Contact](../_diagrams/abstract/AbstractContact.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Abstract Contact Properties

## Table of Abstract Contact Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|EmailAddress| |string|optional|Email Address|Contact email address. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|support@company.com|
|PhoneNumber| |string|optional|Phone Number|Contact phone number. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1-555-281-5555|
|RoleTypeID &rarr; [ContactRoleType](../reference-data/ContactRoleType.1.0.0.md)| |string|optional|Role Type ID|The identifier of a reference value for the role of the contact within the associated organisation, such as Account owner, Sales Representative, Technical Support, Project Manager, Party Chief, Client Representative, Senior Observer.|<code>^[\w\-\.]+:reference-data\-\-ContactRoleType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ContactRoleType](../reference-data/ContactRoleType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Comment| |string|optional|Comment|Additional information about the contact|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|OrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Organisation ID|Reference to the company the contact is associated with.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Name| |string|optional|Name|Name of the individual contact. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractContact.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractTechnicalAssurance.1.0.0](AbstractTechnicalAssurance.1.0.0.md) | Reviewers[] | many (as array) |  |
| Included by | [OperationsReport.1.0.0](../master-data/OperationsReport.1.0.0.md) | JobContact[] | many (as array) |  |
| Included by | [OperationsReport.1.1.0](../master-data/OperationsReport.1.1.0.md) | JobContact[] | many (as array) |  |
| Included by | [OperationsReport.1.2.0](../master-data/OperationsReport.1.2.0.md) | JobContact[] | many (as array) |  |
| Included by | [Risk.1.0.0](../master-data/Risk.1.0.0.md) | Preventions[].Responsibles[] | many (as array) |  |
| Included by | [Risk.1.0.0](../master-data/Risk.1.0.0.md) | Mitigations[].Responsibles[] | many (as array) |  |
| Included by | [Risk.1.0.0](../master-data/Risk.1.0.0.md) | RiskResponsibles[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._