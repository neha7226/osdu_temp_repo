<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AdditiveType [Status: Accepted]
Items added to cement slurry or cement spacers, washes and fluids
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--AdditiveType:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: osdu
* Reference value type, governance model: FIXED
* **Superseded by** &rarr; [`osdu:wks:reference-data--AdditiveType:1.0.1`](AdditiveType.1.0.1.md)
* **Migration Guide (M15)** [`osdu:wks:reference-data--AdditiveType:1.0.0` &rarr; `osdu:wks:reference-data--AdditiveType:1.0.1`](../../Guides/MigrationGuides/M15/AdditiveType.1.0.0.md)
* Link to &rarr; Proposal workbook [schema-AdditiveType.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/reference-data/schema-AdditiveType.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AdditiveType.1.0.0.json](../../Authoring/reference-data/AdditiveType.1.0.0.json)
* Link to &rarr; [Generated Schema AdditiveType.1.0.0.json](../../Generated/reference-data/AdditiveType.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AdditiveType.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/AdditiveType.1.0.0.json)
* Link to &rarr; [Example Record AdditiveType.1.0.0.json](../../Examples/reference-data/AdditiveType.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AdditiveType


## Outgoing Relationships for AdditiveType

![AdditiveType](../_diagrams/reference-data/AdditiveType.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AdditiveType Referenced by other Entities

![AdditiveType](../_diagrams/reference-data/AdditiveType.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AdditiveType Properties

## 1. Table of AdditiveType System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-AdditiveType:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--AdditiveType:fe7f07be-7444-5b3c-8b23-93b32ea267c0|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--AdditiveType:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of AdditiveType Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of AdditiveType Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of AdditiveType Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of AdditiveType Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for AdditiveType.1.0.1

* **Note:** The example records refer to a newer schema version AdditiveType.1.0.1.
* Status: Published
* Governance: OPEN
* The table below shows all 55 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [AdditiveType](../../ReferenceValues/Manifests/reference-data/OPEN/AdditiveType.1.0.1.json).

|`id` with prefix `namespace:reference-data--AdditiveType:`|Code|Name|Alias|Description|InactiveIndicator|AttributionAuthority|Source|CommitDate|AttributionPublication|NameAlias|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Accelerator|~~Accelerator~~|~~Accelerator~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Accelerator&quot; with Name=&quot;Accelerator&quot; instead. A chemical that speeds up the rate of a reaction, especially in cement.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Acid|Acid|Acid|-|A reactive liquid with a low pH.|-|PPDM Members|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Alcohol|Alcohol|Alcohol|-|A type of organic compound with at least one hydroxyl functional group (−OH) bound to a saturated carbon atom.|-|PPDM Members|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Anti%20Foam|~~Anti Foam~~|~~Anti Foam~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Antifoam&quot; with Name=&quot;Antifoam&quot; instead. A material that destabilizes a foam.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Anti%20Gelation|~~Anti Gelation~~|~~Anti gelation~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:AntiGelation&quot; with Name=&quot;AntiGelation&quot; instead. A substance that prevents or retards the formation of a gel.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Anti%20Settling|~~Anti Settling~~|~~Anti settling~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:AntiSettling&quot; with Name=&quot;AntiSettling&quot; instead. A substance that prevents or retards the gravity separation of a solid from a liquid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Anti%20Static|~~Anti Static~~|~~Anti static~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Antistatic&quot; with Name=&quot;Antistatic&quot; instead. A substance that reduces or eliminates the buildup of static electricity.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Antifoam|~~Antifoam~~|~~Antifoam~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Antifoam&quot; with Name=&quot;Antifoam&quot; instead.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Bactericide|~~Bactericide~~|~~Bactericide~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Biocide&quot; with Name=&quot;Biocide&quot; instead. A product that kills bacteria in the water or on the surface of the pipe.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Bond%20Accelerator|~~Bond Accelerator~~|~~Bond accelerator~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:BondAccelerator&quot; with Name=&quot;BondAccelerator&quot; instead. A substance that reduces the reaction time for materials to bond together, especially in cement.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Bonding%20Agent|~~Bonding Agent~~|~~Bonding agent~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:BondingAgent&quot; with Name=&quot;BondingAgent&quot; instead. A substance that induces materials to bond together, especially in cement.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Cement|Cement|Cement|-|A manufactured substance used to form a solid binder and sealer.|-|OSDU|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|-|-|
|Corrosion%20Inhibitor|~~Corrosion Inhibitor~~|~~Corrosion inhibitor~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:CorrosionInhibitor&quot; with Name=&quot;CorrosionInhibitor&quot; instead. A substance to prevent or slow down the reaction of a metal with a fluid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Defoamer|~~Defoamer~~|~~Defoamer~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Defoamer&quot; with Name=&quot;Defoamer&quot; instead. A chemical that reduces the stability of the bubble skin in a foam and cause the foam to break.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Detergent|Detergent|Detergent|-|A surfactant with cleansing properties.|-|PPDM Members|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|-|-|
|Dispenser|~~Dispenser~~|~~Dispenser~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Dispersant&quot; with Name=&quot;Dispersant&quot; instead.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Dispersant|~~Dispersant~~|~~Dispersant~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Dispersant&quot; with Name=&quot;Dispersant&quot; instead. A substance that aids in breaking up a mass of particles, bubbles or droplets.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~https://petrowiki.spe.org/Glossary:Dispersant~~|~~-~~|
|Dye|Dye|Dye|-|A colored substance that chemically bonds to the substrate to which it is applied.|-|OSDU|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|-|-|
|Emulsifier|~~Emulsifier~~|~~Emulsifier~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Emulsifier&quot; with Name=&quot;Emulsifier&quot; instead. A substance that creates or stabilizes an emulsion.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Expansion|~~Expansion~~|~~Expansion~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Expansion&quot; with Name=&quot;Expansion&quot; instead.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Extender|~~Extender~~|~~Extender~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Extender&quot; with Name=&quot;Extender&quot; instead. A substance that decreasing the density of a cement slurry.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Fe%20sequestering|~~Fe sequestering~~|~~Fe sequestering~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Chelant&quot; with Name=&quot;Chelant&quot; instead.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Fiber|Fiber|Fiber|-|A natural or manufactured substance that is significantly longer than it is wide.|-|OSDU|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|-|-|
|Flow%20Enhancer|~~Flow Enhancer~~|~~Flow enhancer~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:FlowEnhancer&quot; with Name=&quot;FlowEnhancer&quot; instead. A substance that improves the rate of flow of a liquid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Fluid%20Loss|~~Fluid Loss~~|~~Fluid loss~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:FluidLoss&quot; with Name=&quot;FluidLoss&quot; instead.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Foaming%20Agent|~~Foaming Agent~~|~~Foaming agent~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:FoamingAgent&quot; with Name=&quot;FoamingAgent&quot; instead. A substance that encourages an emulsion, being the suspension of a gas in a liquid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Formation%20Sealer|~~Formation Sealer~~|~~Formation sealer~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:FormationSealer&quot; with Name=&quot;FormationSealer&quot; instead. A substance that improves the ability of a fluid to close a formation zone, thereby preventing lost circulation or improving zone abandonment.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Free%20Water%20Ctrl|~~Free Water Ctrl~~|~~Free water ctrl~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:FreeWaterControl&quot; with Name=&quot;FreeWaterControl&quot; instead. An additive to the cement slurry to address the potential problem of particle sedimentation.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Free%20Water|~~Free Water~~|~~Free water~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:FreeWaterControl&quot; with Name=&quot;FreeWaterControl&quot; instead. Water that is not in an emulsion or not chemically bonded.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Friction%20Reducing|~~Friction Reducing~~|~~Friction reducing~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:FrictionReducer&quot; with Name=&quot;FrictionReducer&quot; instead. A material, usually a polymer that reduces the friction of flowing fluid in a conduit~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~https://petrowiki.spe.org/Glossary:Friction_reducer~~|~~-~~|
|Gas%20Migration|~~Gas Migration~~|~~Gas migration~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:GasMigration&quot; with Name=&quot;GasMigration&quot; instead. Primarily used as an additive in a cement slurry~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Gelling%20Agent|~~Gelling Agent~~|~~Gelling agent~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Gellant&quot; with Name=&quot;Gellant&quot; instead. A substance that increases the viscosity of a liquid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|HydrofluoricAcid|HydrofluoricAcid|Hydrofluoric acid|-|A mixture of hydrogen fluoride and water. It reacts with clays.|-|PPDM Members|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Light%20Weight|~~Light Weight~~|~~Light Weight~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Lightweight&quot; with Name=&quot;Lightweight&quot; instead. A substance that decreasing the density of a cement slurry.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Liquid%20Extender|~~Liquid Extender~~|~~Liquid extender~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:LiquidExtender&quot; with Name=&quot;LiquidExtender&quot; instead. A substance that decreasing the density of a liquid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|LiquidCo2|LiquidCo2|Liquid CO2|-|Carbon dioxide under pressure in the liquid phase.|-|PPDM Members|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|-|-|
|Lost%20circulation|~~Lost circulation~~|~~Lost circulation~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:LostCirculation&quot; with Name=&quot;LostCirculation&quot; instead.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|MethaneSulfonicAcid|MethaneSulfonicAcid|Methane sulfonic acid|-|An acid for treating the matrix of carbonate reservoirs.|-|PPDM Members|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|https://ihsmarkit.com/products/oil-gas-reference-materials.html|-|
|Mixing%20Fluid|~~Mixing Fluid~~|~~Mixing fluid~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:MixingFluid&quot; with Name=&quot;MixingFluid&quot; instead.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Mutual%20Solvent|~~Mutual Solvent~~|~~Mutual solvent~~|~~-~~|~~A chemical that have some common solvency for both water and oil materials.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Polymer|Polymer|Polymer|-|A synthetic gelling agent.|-|OSDU|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|-|-|
|Preflush|~~Preflush~~|~~Preflush~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Preflush&quot; with Name=&quot;Preflush&quot; instead. A substance to dissolve carbonate or to displace and isolate incompatible formation fluids.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Retarder%20Acc|~~Retarder Acc~~|~~Retarder acc~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Retarder&quot; with Name=&quot;Retarder&quot; instead.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Retarder|~~Retarder~~|~~Retarder~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Retarder&quot; with Name=&quot;Retarder&quot; instead. A chemical that slows a reaction, such as the set time of cement.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Scale%20Inhibitor|~~Scale Inhibitor~~|~~Scale inhibitor~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:ScaleInhibitor&quot; with Name=&quot;ScaleInhibitor&quot; instead. A substance that prevents the formation of iron scale.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Solvent|Solvent|Solvent|-|A substance that will dissolve a solid.|-|OSDU|Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.|2022-10-26T10:10:20+02:00|-|-|
|Spacer|~~Spacer~~|~~Spacer~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Spacer&quot; with Name=&quot;Spacer&quot; instead. In the context of pumping, a spacer is a fluid used between two fluids to prevent contamination.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~https://petrowiki.spe.org/Glossary:Spacer~~|~~-~~|
|Stabilizer|~~Stabilizer~~|~~Stabilizer~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Stabilizer&quot; with Name=&quot;Stabilizer&quot; instead. A chemical or physical effect that prevents separation of two or more, normally immiscible phases.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~https://petrowiki.spe.org/Glossary:Emulsion_stabilizer~~|~~-~~|
|Strength|~~Strength~~|~~Strength~~|~~-~~|~~-~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Surfactant|~~Surfactant~~|~~Surfactant~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Surfactant&quot; with Name=&quot;Surfactant&quot; instead. A chemical that modifies the surface tension of a liquid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Foaming&nbsp;Agent",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]~~|
|Suspending%20Agent|~~Suspending Agent~~|~~Suspending agent~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:SuspendingAgent&quot; with Name=&quot;SuspendingAgent&quot; instead. A surfactant used in acid-treating solutions and fracturing fluids to suspend insoluble fines.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Thixotropic|~~Thixotropic~~|~~Thixotropic~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Thixotropic&quot; with Name=&quot;Thixotropic&quot; instead. A substance that changes the thixotropic property of a fluid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Viscosifier|~~Viscosifier~~|~~Viscosifier~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:Viscosifier&quot; with Name=&quot;Viscosifier&quot; instead. A substance that increases the viscosity of a fluid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Water%20sensitive|~~Water sensitive~~|~~Water sensitive~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:ClayStabilizer&quot; with Name=&quot;ClayStabilizer&quot; instead.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|
|Weighting%20agent|~~Weighting agent~~|~~Weighting agent~~|~~-~~|~~DEPRECATED: Please use Code=&quot;AdditiveRole:WeightingAgent&quot; with Name=&quot;WeightingAgent&quot; instead. A substance that increases the density of a fluid.~~|~~True~~|~~OSDU~~|~~Workbook Published/AdditiveType.1.0.1.xlsx; commit SHA 2f5e6a58.~~|~~2022-10-26T10:10:20+02:00~~|~~-~~|~~-~~|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AdditiveType.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [PlannedCementJob.1.0.0](../master-data/PlannedCementJob.1.0.0.md) | CementStages[].CementingFluid.CementAdditives[].AdditiveTypeID | many (as array) |  |
| Referred to by | [PlannedCementJob.1.1.0](../master-data/PlannedCementJob.1.1.0.md) | CementStages[].CementingFluid.CementAdditives[].AdditiveTypeID | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._