<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# Parent List
A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractLegalParentList:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; [Authoring Schema AbstractLegalParentList.1.0.0.json](../../Authoring/abstract/AbstractLegalParentList.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractLegalParentList.1.0.0.json](../../Generated/abstract/AbstractLegalParentList.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractLegalParentList.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractLegalParentList.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams Parent List


## Outgoing Relationships for Parent List

![Parent List](../_diagrams/abstract/AbstractLegalParentList.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Parent List Referenced by other Entities

![Parent List](../_diagrams/abstract/AbstractLegalParentList.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Parent List Properties

## Table of Parent List Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|parents[]| |string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractLegalParentList.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractSystemProperties.1.0.0](AbstractSystemProperties.1.0.0.md) | ancestry | one |  |
| Included by | [GenericDataset.1.0.0](../manifest/GenericDataset.1.0.0.md) | ancestry | one |  |
| Included by | [GenericMasterData.1.0.0](../manifest/GenericMasterData.1.0.0.md) | ancestry | one |  |
| Included by | [GenericReferenceData.1.0.0](../manifest/GenericReferenceData.1.0.0.md) | ancestry | one |  |
| Included by | [GenericWorkProduct.1.0.0](../manifest/GenericWorkProduct.1.0.0.md) | ancestry | one |  |
| Included by | [GenericWorkProductComponent.1.0.0](../manifest/GenericWorkProductComponent.1.0.0.md) | ancestry | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._