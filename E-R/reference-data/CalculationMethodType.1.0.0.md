<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# CalculationMethodType
The method to calculate a wellbore path from the discrete station measurements of a wellbore directional survey.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--CalculationMethodType:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: PPDM
* Reference value type, governance model: OPEN
* Link to &rarr; [Authoring Schema CalculationMethodType.1.0.0.json](../../Authoring/reference-data/CalculationMethodType.1.0.0.json)
* Link to &rarr; [Generated Schema CalculationMethodType.1.0.0.json](../../Generated/reference-data/CalculationMethodType.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource CalculationMethodType.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/CalculationMethodType.1.0.0.json)
* Link to &rarr; [Example Record CalculationMethodType.1.0.0.json](../../Examples/reference-data/CalculationMethodType.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams CalculationMethodType


## Outgoing Relationships for CalculationMethodType

![CalculationMethodType](../_diagrams/reference-data/CalculationMethodType.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## CalculationMethodType Referenced by other Entities

![CalculationMethodType](../_diagrams/reference-data/CalculationMethodType.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# CalculationMethodType Properties

## 1. Table of CalculationMethodType System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-CalculationMethodType:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--CalculationMethodType:67539ea5-0b7b-545b-8dd6-d25bac88a1d4|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--CalculationMethodType:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of CalculationMethodType Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of CalculationMethodType Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of CalculationMethodType Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for CalculationMethodType.1.0.0

* Status: Published
* Governance: OPEN
* The table below shows all 23 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [CalculationMethodType](../../ReferenceValues/Manifests/reference-data/OPEN/CalculationMethodType.1.0.0.json).

|`id` with prefix `namespace:reference-data--CalculationMethodType:`|Code|Name|Alias|Description|AttributionAuthority|AttributionPublication|Source|CommitDate|NameAlias|InactiveIndicator|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Acceleration|Acceleration|Acceleration|-|Utilizes the angles at the top and bottom of the course length and from these generates a curve on the assumption that the measured angles change smoothly from top to bottom of the measured course as though under the influence of a constant force of acceleration. The results obtained are the same as the &quot;Balanced Tangential&quot; &quot;Trapezoidal&quot; and &quot;Vector Averaging&quot; Methods.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|AverageAngle|AverageAngle|Average Angle|-|Uses the average of the inclinations and azimuths measured at the upper and lower survey stations. The average of the two sets of angles is assumed to be the inclination and the azimuth over the incremental measured depth. The wellbore path is then calculated using simple trigonometric functions.|PPDM Members|https://orkustofnun.is/gogn/unu-gtp-report/UNU-GTP-2013-27.pdf [PDF page 12]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Angle-Averaging",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:Abbreviation:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|-|
|BackwardStation|BackwardStation|Backward Station|-|Refer to Tangential Method|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|BalancedTangential|BalancedTangential|Balanced Tangential|-|Uses the inclination and direction angles at the top and bottom of the course length in a manner so as to tangentially balance the two sets of measured angles over the course length. Results obtained are the same as the &quot;Acceleration&quot; &quot;Trapezoidal&quot; and &quot;Vector Averaging&quot; Methods.  The balanced tangential method is rarely used but is the basis for the minimum curvature method.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9] https://orkustofnun.is/gogn/unu-gtp-report/UNU-GTP-2013-27.pdf [PDF page 11]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|CircularArc|CircularArc|Circular Arc|-|Uses both sets of measured angles associated with each course length to recreate the wellbore path as a sequence of circular arcs constrained by the measured angles to pass through the end points with inclination and direction angles as measured.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|Combined|Combined|Combined|-|Refer to &quot;Mercury&quot; Method.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|CompensatedAcceleration|CompensatedAcceleration|Compensated Acceleration|-|Refer to &quot;Mercury&quot; Method.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|ConstantCurvature|ConstantCurvature|Constant Curvature|-|The constant-curvature method developed is based on drilling tendencies of the bottom hole assembly and the technical advancement in directional drilling. It yields a trajectory with less maximum dogleg severity, less drag, and less torque in the drill string compared to the conventional radius-of-curvature and constant-turn-rate methods. The equations for the constant-curvature method can easily be used during directional drilling and are especially useful for horizontal drilling where a constant build rate is desired.|PPDM Members|https://www.osti.gov/biblio/6561093|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|CurvatureRadius|~~CurvatureRadius~~|~~Curvature Radius~~|~~-~~|~~Uses the inclination and azimuth measured at the upper and lower ends of the course length to generate a circular arc when viewed in both the vertical and horizontal planes. This method assumes that the well path lies on a cylinder whose axis is vertical, and has a radius equal to the radius of curvature in the horizontal plane. It determines the length of the arc between the upper and lower ends of the course length in the horizontal plane. The cylinder can then be “unwrapped” to calculate the length of the circular arc along the cylinder surface. Consequently the incremental TVD is unaffected by changes in azimuth.~~|~~PPDM Members~~|~~https://orkustofnun.is/gogn/unu-gtp-report/UNU-GTP-2013-27.pdf [PDF page 13]~~|~~Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.~~|~~2022-04-30T16:26:14+02:00~~|~~-~~|~~True~~|
|GreatCircle|GreatCircle|Great Circle|-|Refer to &quot;Minimum Curvature&quot; Method|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|Mercury|Mercury|Mercury|-|A combination of the Tangential and Balanced Tangential Methods, so as to treat that portion of the measured course length defined by the length of the measuring tool in a straight line (tangentially) and the remainder of the measured course in a Balanced Tangential manner (trapezoidially). The name, Mercury Method, originated from the common use of this method at the Atomic Energy Commission&#x27;s test site, Mercury, Nevada.  As can be seen from the expression in the definition, the method is basically the Balanced Tangential Method with corrections made for the rigid length of the survey instrument.  It should be pointed out that other methods can be utilized to replace the Balanced Tangential portion of this method and not create any error involved in the system, and may, in fact, improve on the accuracy of the computations.   Also, refer to &quot;Compensated Acceleration Method&quot; and &quot;Combined Method&quot;. (API Bulletin D20, pages 6 and 9)|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF pages 6 and 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|MinimumCurvature|MinimumCurvature|Minimum Curvature|-|Uses the sets of angles measured at the top and bottom of the course length to establish coordinate velocities through which a space curve (which represents the calculated path of the wellbore) passes in a manner that minimizes its total curvature.  This model takes the space vectors defined by inclination and direction measurements and smooths them onto the wellbore curve (Figure 13). This method is really a modification of the balanced tangential method. Instead of approximating the wellbore path with two straight lines, the minimum curvature replaces these lines with a circular arc. This arc is calculated by using a dogleg scale factor based on the amount of angular change over the course length.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9] https://orkustofnun.is/gogn/unu-gtp-report/UNU-GTP-2013-27.pdf [PDF page 14]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Exact&nbsp;Radius",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryName:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|-|
|Quadratic|Quadratic|Quadratic|-|A method in math modeling considering the wellbore as a curve: the projections into three vertical planes are quadratic functions.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|RadiusOfCurvature|RadiusOfCurvature|Radius of curvature|-|Uses the inclination and azimuth measured at the upper and lower ends of the course length to generate a circular arc when viewed in both the vertical and horizontal planes. This method assumes that the well path lies on a cylinder whose axis is vertical, and has a radius equal to the radius of curvature in the horizontal plane. It determines the length of the arc between the upper and lower ends of the course length in the horizontal plane. The cylinder can then be “unwrapped” to calculate the length of the circular arc along the cylinder surface. Consequently the incremental TVD is unaffected by changes in azimuth.|PPDM Members|https://orkustofnun.is/gogn/unu-gtp-report/UNU-GTP-2013-27.pdf [PDF page 13]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|Radiusofcurvature|~~Radiusofcurvature~~|~~Radius of curvature~~|~~-~~|~~Uses the inclination and azimuth measured at the upper and lower ends of the course length to generate a circular arc when viewed in both the vertical and horizontal planes. This method assumes that the well path lies on a cylinder whose axis is vertical, and has a radius equal to the radius of curvature in the horizontal plane. It determines the length of the arc between the upper and lower ends of the course length in the horizontal plane. The cylinder can then be “unwrapped” to calculate the length of the circular arc along the cylinder surface. Consequently the incremental TVD is unaffected by changes in azimuth.~~|~~PPDM Members~~|~~https://orkustofnun.is/gogn/unu-gtp-report/UNU-GTP-2013-27.pdf [PDF page 13]~~|~~Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.~~|~~2022-04-30T16:26:14+02:00~~|~~-~~|~~True~~|
|Secant|Secant|Secant|-|Has been used with two different meanings: 1) the &quot;Trapezoidal Method&quot; and 2)  the &quot;Average Angle Method&quot;.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|SimpsonsRule|SimpsonsRule|Simpsons Rule|-|Uses as many measured angle values as are available (a minimum of three sets) to recreate the wellbore path through Simpson&#x27;s rule for numeric integration which approximates by passing a parabola through three points.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|Tangential|Tangential|Tangential|-|Uses the inclination and azimuth at the lower end of the course length to calculate a straight line that represents the well bore, and passes through the lower end of the course length. The wellbore is assumed to be a straight line throughout the course length. This method is the most inaccurate of the methods discussed and should not be used in the determination of survey results unless the course lengths are not longer than the length of the survey tool. Some legacy surveys have been computed with this method.|PPDM Members|https://orkustofnun.is/gogn/unu-gtp-report/UNU-GTP-2013-27.pdf [PDF page 12]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|TenChordMethod|TenChordMethod|Ten Chord Method|-|The spiral distance between stations is computed in ten equal intervals. For example, a 200 foot ten-chord spiral would be computed at a 20 foot interval.|PPDM Members|https://www.jerrymahun.com/index.php/home/open-access/viii-curves/76-chapter-e-spirals?showall=1|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|TerminalAngle|TerminalAngle|Terminal Angle|-|Refer to Tangential method|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|ThreeDimensionalRadiusOfCurvature|ThreeDimensionalRadiusOfCurvature|Three-Dimensional Radius of Curvature|-|Refer to Minimum Curvature method|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|Unknown|Unknown|Unknown|-|For some surveys of old wells, the method is unknown.|PPDM Members|-|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|
|VectorAveraging|VectorAveraging|Vector Averaging|-|Uses inclination and direction measurements at both ends of the measured course to establish vector space directions. It is then assumed that each of these two vectors is projected for one-half the course length in creating the wellbore path.  Each &quot;half-course length&quot; segment can be treated tangentially. Results obtained are essentially the same as the &quot;Acceleration&quot;, &quot;Balanced Tangential&quot; and &quot;Trapezoidal&quot; Methods.|PPDM Members|API_Bulletin_D20_DSRV_Calc_Methods_Terms.pdf [PDF page 9]|Workbook Published/CalculationMethodType.1.0.0.xlsx; commit SHA c380ec49.|2022-04-30T16:26:14+02:00|-|-|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# CalculationMethodType.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [WellboreTrajectory.1.0.0](../work-product-component/WellboreTrajectory.1.0.0.md) | CalculationMethodType | one |  |
| Referred to by | [WellboreTrajectory.1.1.0](../work-product-component/WellboreTrajectory.1.1.0.md) | CalculationMethodType | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._