<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# Abstract Master [Status: Accepted]
Properties shared with all master-data schema instances.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractMaster:1.1.0`
* Schema status: PUBLISHED
* **Migration Guide (M12)** [`osdu:wks:AbstractMaster:1.0.0` &rarr; `osdu:wks:AbstractMaster:1.1.0`](../../Guides/MigrationGuides/M12/AbstractMaster.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractMaster.1.1.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractMaster.1.1.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractMaster.1.1.0.json](../../Authoring/abstract/AbstractMaster.1.1.0.json)
* Link to &rarr; [Generated Schema AbstractMaster.1.1.0.json](../../Generated/abstract/AbstractMaster.1.1.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractMaster.1.1.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractMaster.1.1.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams Abstract Master


## Outgoing Relationships for Abstract Master

![Abstract Master](../_diagrams/abstract/AbstractMaster.1.1.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Abstract Master Referenced by other Entities

![Abstract Master](../_diagrams/abstract/AbstractMaster.1.1.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Abstract Master Properties

## Table of Abstract Master Properties (Version 1.1.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_NameAliases[]_|_[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this master data is/has been known (it should include all the identifiers). Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|NameAliases[].AliasName|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|NameAliases[].AliasNameTypeID &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|NameAliases[].DefinitionOrganisationID &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|NameAliases[].EffectiveDateTime|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|NameAliases[].TerminationDateTime|[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_GeoContexts[]_|_[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[0] [AbstractGeoPoliticalContext.1.0.0](AbstractGeoPoliticalContext.1.0.0.md)_|_object_|_optional_|_AbstractGeoPoliticalContext_|_List of geographic entities which provide context to the master data. This may include multiple types or multiple values of the same type. Fragment Description: A geographic context to an entity. It can be either a reference to a GeoPoliticalEntity, Basin, Field, Play or Prospect. Fragment Description: A single, typed geo-political entity reference, which is 'abstracted' to AbstractGeoContext and then aggregated by GeoContexts properties._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|GeoContexts[].GeoPoliticalEntityID &rarr; [GeoPoliticalEntity](../master-data/GeoPoliticalEntity.1.0.0.md)|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[0] [AbstractGeoPoliticalContext.1.0.0](AbstractGeoPoliticalContext.1.0.0.md)|string|optional|Geo Political Entity Id|Reference to GeoPoliticalEntity.|<code>^[\w\-\.]+:master-data\-\-GeoPoliticalEntity:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [GeoPoliticalEntity](../master-data/GeoPoliticalEntity.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoContexts[].GeoTypeID &rarr; [GeoPoliticalEntityType](../reference-data/GeoPoliticalEntityType.1.0.0.md)|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[0] [AbstractGeoPoliticalContext.1.0.0](AbstractGeoPoliticalContext.1.0.0.md)|string|optional|Geo Type Id|The GeoPoliticalEntityType reference of the GeoPoliticalEntity (via GeoPoliticalEntityID) for application convenience.|<code>^[\w\-\.]+:reference-data\-\-GeoPoliticalEntityType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [GeoPoliticalEntityType](../reference-data/GeoPoliticalEntityType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "GeoPoliticalEntityID", "TargetPropertyName": "GeoPoliticalEntityTypeID"}|(No natural key)|(No example)|
|_GeoContexts[]_|_[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[1] [AbstractGeoBasinContext.1.0.0](AbstractGeoBasinContext.1.0.0.md)_|_object_|_optional_|_AbstractGeoBasinContext_|_A single, typed basin entity reference, which is 'abstracted' to AbstractGeoContext and then aggregated by GeoContexts properties._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|GeoContexts[].BasinID &rarr; [Basin](../master-data/Basin.1.0.0.md)|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[1] [AbstractGeoBasinContext.1.0.0](AbstractGeoBasinContext.1.0.0.md)|string|optional|Basin Id|Reference to Basin.|<code>^[\w\-\.]+:master-data\-\-Basin:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Basin](../master-data/Basin.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoContexts[].GeoTypeID &rarr; [BasinType](../reference-data/BasinType.1.0.0.md)|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[1] [AbstractGeoBasinContext.1.0.0](AbstractGeoBasinContext.1.0.0.md)|string|optional|Geo Type Id|The BasinType reference of the Basin (via BasinID) for application convenience.|<code>^[\w\-\.]+:reference-data\-\-BasinType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [BasinType](../reference-data/BasinType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "BasinID", "TargetPropertyName": "BasinTypeID"}|(No natural key)|(No example)|
|_GeoContexts[]_|_[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[2] [AbstractGeoFieldContext.1.0.0](AbstractGeoFieldContext.1.0.0.md)_|_object_|_optional_|_AbstractGeoFieldContext_|_A single, typed field entity reference, which is 'abstracted' to AbstractGeoContext and then aggregated by GeoContexts properties._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|GeoContexts[].FieldID &rarr; [Field](../master-data/Field.1.0.0.md)|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[2] [AbstractGeoFieldContext.1.0.0](AbstractGeoFieldContext.1.0.0.md)|string|optional|Field Id|Reference to Field.|<code>^[\w\-\.]+:master-data\-\-Field:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Field](../master-data/Field.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoContexts[].GeoTypeID|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[2] [AbstractGeoFieldContext.1.0.0](AbstractGeoFieldContext.1.0.0.md)|const string|optional|Geo Type Id|The fixed type 'Field' for this AbstractGeoFieldContext.|<code>Field</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Field|
|_GeoContexts[]_|_[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[3] [AbstractGeoPlayContext.1.0.0](AbstractGeoPlayContext.1.0.0.md)_|_object_|_optional_|_AbstractGeoPlayContext_|_A single, typed Play entity reference, which is 'abstracted' to AbstractGeoContext and then aggregated by GeoContexts properties._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|GeoContexts[].PlayID &rarr; [Play](../master-data/Play.1.0.0.md)|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[3] [AbstractGeoPlayContext.1.0.0](AbstractGeoPlayContext.1.0.0.md)|string|optional|Play Id|Reference to the play.|<code>^[\w\-\.]+:master-data\-\-Play:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Play](../master-data/Play.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoContexts[].GeoTypeID &rarr; [PlayType](../reference-data/PlayType.1.0.0.md)|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[3] [AbstractGeoPlayContext.1.0.0](AbstractGeoPlayContext.1.0.0.md)|string|optional|Geo Type Id|The PlayType reference of the Play (via PlayID) for application convenience.|<code>^[\w\-\.]+:reference-data\-\-PlayType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [PlayType](../reference-data/PlayType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "PlayID", "TargetPropertyName": "PlayTypeID"}|(No natural key)|(No example)|
|_GeoContexts[]_|_[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[4] [AbstractGeoProspectContext.1.0.0](AbstractGeoProspectContext.1.0.0.md)_|_object_|_optional_|_AbstractGeoProspectContext_|_A single, typed Prospect entity reference, which is 'abstracted' to AbstractGeoContext and then aggregated by GeoContexts properties._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|GeoContexts[].ProspectID &rarr; [Prospect](../master-data/Prospect.1.0.0.md)|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[4] [AbstractGeoProspectContext.1.0.0](AbstractGeoProspectContext.1.0.0.md)|string|optional|Prospect Id|Reference to the prospect.|<code>^[\w\-\.]+:master-data\-\-Prospect:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Prospect](../master-data/Prospect.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoContexts[].GeoTypeID &rarr; [ProspectType](../reference-data/ProspectType.1.0.0.md)|[AbstractGeoContext.1.0.0](AbstractGeoContext.1.0.0.md) oneOf[4] [AbstractGeoProspectContext.1.0.0](AbstractGeoProspectContext.1.0.0.md)|string|optional|Geo Type Id|The ProspectType reference of the Prospect (via ProspectID) for application convenience.|<code>^[\w\-\.]+:reference-data\-\-ProspectType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ProspectType](../reference-data/ProspectType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "ProspectID", "TargetPropertyName": "ProspectTypeID"}|(No natural key)|(No example)|
|_SpatialLocation_|_[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)_|_object_|_optional_|_AbstractSpatialLocation_|_The spatial location information such as coordinates, CRS information (left empty when not appropriate). Fragment Description: A geographic object which can be described by a set of points._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|SpatialLocation.SpatialLocationCoordinatesDate|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Spatial Location Coordinates Date|Date when coordinates were measured or retrieved.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SpatialLocation.QuantitativeAccuracyBandID &rarr; [QuantitativeAccuracyBand](../reference-data/QuantitativeAccuracyBand.1.0.0.md)|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Quantitative Accuracy Band Id|An approximate quantitative assessment of the quality of a location (accurate to > 500 m (i.e. not very accurate)), to < 1 m, etc.|<code>^[\w\-\.]+:reference-data\-\-QuantitativeAccuracyBand:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [QuantitativeAccuracyBand](../reference-data/QuantitativeAccuracyBand.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SpatialLocation.QualitativeSpatialAccuracyTypeID &rarr; [QualitativeSpatialAccuracyType](../reference-data/QualitativeSpatialAccuracyType.1.0.0.md)|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Qualitative Spatial Accuracy Type Id|A qualitative description of the quality of a spatial location, e.g. unverifiable, not verified, basic validation.|<code>^[\w\-\.]+:reference-data\-\-QualitativeSpatialAccuracyType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [QualitativeSpatialAccuracyType](../reference-data/QualitativeSpatialAccuracyType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SpatialLocation.CoordinateQualityCheckPerformedBy|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Coordinate Quality Check Performed By|The user who performed the Quality Check.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SpatialLocation.CoordinateQualityCheckDateTime|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Coordinate Quality Check Date Time|The date of the Quality Check.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SpatialLocation.CoordinateQualityCheckRemarks[]|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Coordinate Quality Check Remarks|Freetext remarks on Quality Check.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_SpatialLocation.AsIngestedCoordinates_|_[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md) [AbstractAnyCrsFeatureCollection.1.1.0](AbstractAnyCrsFeatureCollection.1.1.0.md) (nested structure, follow link for details)_|_object_|_optional_|_AbstractAnyCrsFeatureCollection_|_The original or 'as ingested' coordinates (Point, MultiPoint, LineString, MultiLineString, Polygon or MultiPolygon). The name 'AsIngestedCoordinates' was chosen to contrast it to 'OriginalCoordinates', which carries the uncertainty whether any coordinate operations took place before ingestion. In cases where the original CRS is different from the as-ingested CRS, the AppliedOperations can also contain the list of operations applied to the coordinate prior to ingestion. The data structure is similar to GeoJSON FeatureCollection, however in a CRS context explicitly defined within the AbstractAnyCrsFeatureCollection. The coordinate sequence follows GeoJSON standard, i.e. 'eastward/longitude', 'northward/latitude' {, 'upward/height' unless overridden by an explicit direction in the AsIngestedCoordinates.VerticalCoordinateReferenceSystemID}. Fragment Description: A schema like GeoJSON FeatureCollection with a non-WGS 84 CRS context; based on https://geojson.org/schema/FeatureCollection.json. Attention: the coordinate order is fixed: Longitude/Easting/Westing/X first, followed by Latitude/Northing/Southing/Y, optionally height as third coordinate._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|_SpatialLocation.Wgs84Coordinates_|_[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md) [AbstractFeatureCollection.1.0.0](AbstractFeatureCollection.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_GeoJSON FeatureCollection_|_The normalized coordinates (Point, MultiPoint, LineString, MultiLineString, Polygon or MultiPolygon) based on WGS 84 (EPSG:4326 for 2-dimensional coordinates, EPSG:4326 + EPSG:5714 (MSL) for 3-dimensional coordinates). This derived coordinate representation is intended for global discoverability only. The schema of this substructure is identical to the GeoJSON FeatureCollection https://geojson.org/schema/FeatureCollection.json. The coordinate sequence follows GeoJSON standard, i.e. longitude, latitude {, height} Fragment Description: GeoJSON feature collection as originally published in https://geojson.org/schema/FeatureCollection.json. Attention: the coordinate order is fixed: Longitude first, followed by Latitude, optionally height above MSL (EPSG:5714) as third coordinate._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|SpatialLocation.AppliedOperations[]|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Operations Applied|The audit trail of operations applied to the coordinates from the original state to the current state. The list may contain operations applied prior to ingestion as well as the operations applied to produce the Wgs84Coordinates. The text elements refer to ESRI style CRS and Transformation names, which may have to be translated to EPSG standard names.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|["conversion from ED_1950_UTM_Zone_31N to GCS_European_1950; 1 points converted", "transformation GCS_European_1950 to GCS_WGS_1984 using ED_1950_To_WGS_1984_24; 1 points successfully transformed"]|
|SpatialLocation.SpatialParameterTypeID &rarr; [SpatialParameterType](../reference-data/SpatialParameterType.1.0.0.md)|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Spatial Parameter Type Id|A type of spatial representation of an object, often general (e.g. an Outline, which could be applied to Field, Reservoir, Facility, etc.) or sometimes specific (e.g. Onshore Outline, State Offshore Outline, Federal Offshore Outline, 3 spatial representations that may be used by Countries).|<code>^[\w\-\.]+:reference-data\-\-SpatialParameterType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SpatialParameterType](../reference-data/SpatialParameterType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SpatialLocation.SpatialGeometryTypeID &rarr; [SpatialGeometryType](../reference-data/SpatialGeometryType.1.0.0.md)|[AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md)|string|optional|Spatial Geometry Type Id|Indicates the expected look of the SpatialParameterType, e.g. Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon. The value constrains the type of geometries in the GeoJSON Wgs84Coordinates and AsIngestedCoordinates.|<code>^[\w\-\.]+:reference-data\-\-SpatialGeometryType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SpatialGeometryType](../reference-data/SpatialGeometryType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|VersionCreationReason| |string|optional|Version Creation Reason|This describes the reason that caused the creation of a new version of this master data.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TechnicalAssuranceTypeID &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)| |string|optional|Technical Assurance Type ID|Describes a master-data record's overall suitability for general business consumption based on data quality. Clarifications: Since Certified is the highest classification of suitable quality, any further change or versioning of a Certified record should be carefully considered and justified. If a Technical Assurance value is not populated then one can assume the data has not been evaluated or its quality is unknown (=Unevaluated). Technical Assurance values are not intended to be used for the identification of a single "preferred" or "definitive" record by comparison with other records.|<code>^[\w\-\.]+:reference-data\-\-TechnicalAssuranceType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--TechnicalAssuranceType:Certified:|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractMaster.1.1.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [ActivityPlan.1.1.0](../master-data/ActivityPlan.1.1.0.md) | allOf | one |  |
| Included by | [ConnectedSourceDataJob.1.0.0](../master-data/ConnectedSourceDataJob.1.0.0.md) | allOf | one |  |
| Included by | [ConnectedSourceDataJob.1.1.0](../master-data/ConnectedSourceDataJob.1.1.0.md) | allOf | one |  |
| Included by | [ConnectedSourceDataJob.1.2.0](../master-data/ConnectedSourceDataJob.1.2.0.md) | allOf | one |  |
| Included by | [ConnectedSourceDataJob.1.3.0](../master-data/ConnectedSourceDataJob.1.3.0.md) | allOf | one |  |
| Included by | [ConnectedSourceRegistryEntry.1.0.0](../master-data/ConnectedSourceRegistryEntry.1.0.0.md) | allOf | one |  |
| Included by | [ConnectedSourceRegistryEntry.1.1.0](../master-data/ConnectedSourceRegistryEntry.1.1.0.md) | allOf | one |  |
| Included by | [ConnectedSourceRegistryEntry.1.2.0](../master-data/ConnectedSourceRegistryEntry.1.2.0.md) | allOf | one |  |
| Included by | [Coring.1.0.0](../master-data/Coring.1.0.0.md) | allOf | one |  |
| Included by | [HoleSection.1.1.0](../master-data/HoleSection.1.1.0.md) | allOf | one |  |
| Included by | [IsolatedInterval.1.0.0](../master-data/IsolatedInterval.1.0.0.md) | allOf | one |  |
| Included by | [OperationsReport.1.1.0](../master-data/OperationsReport.1.1.0.md) | allOf | one |  |
| Included by | [OperationsReport.1.2.0](../master-data/OperationsReport.1.2.0.md) | allOf | one |  |
| Included by | [PerforationInterval.1.0.0](../master-data/PerforationInterval.1.0.0.md) | allOf | one |  |
| Included by | [PerforationJob.1.0.0](../master-data/PerforationJob.1.0.0.md) | allOf | one |  |
| Included by | [PlannedCementJob.1.1.0](../master-data/PlannedCementJob.1.1.0.md) | allOf | one |  |
| Included by | [Reservoir.0.0.0](../master-data/Reservoir.0.0.0.md) | allOf | one |  |
| Included by | [ReservoirSegment.0.0.0](../master-data/ReservoirSegment.0.0.0.md) | allOf | one |  |
| Included by | [Rig.1.1.0](../master-data/Rig.1.1.0.md) | allOf | one |  |
| Included by | [RockSample.1.0.0](../master-data/RockSample.1.0.0.md) | allOf | one |  |
| Included by | [SeismicAcquisitionSurvey.1.2.0](../master-data/SeismicAcquisitionSurvey.1.2.0.md) | allOf | one |  |
| Included by | [SeismicProcessingProject.1.2.0](../master-data/SeismicProcessingProject.1.2.0.md) | allOf | one |  |
| Included by | [StorageFacility.1.1.0](../master-data/StorageFacility.1.1.0.md) | allOf | one |  |
| Included by | [Well.1.1.0](../master-data/Well.1.1.0.md) | allOf | one |  |
| Included by | [Well.1.2.0](../master-data/Well.1.2.0.md) | allOf | one |  |
| Included by | [WellActivity.1.0.0](../master-data/WellActivity.1.0.0.md) | allOf | one |  |
| Included by | [WellActivity.1.1.0](../master-data/WellActivity.1.1.0.md) | allOf | one |  |
| Included by | [Wellbore.1.1.0](../master-data/Wellbore.1.1.0.md) | allOf | one |  |
| Included by | [Wellbore.1.1.1](../master-data/Wellbore.1.1.1.md) | allOf | one |  |
| Included by | [Wellbore.1.2.0](../master-data/Wellbore.1.2.0.md) | allOf | one |  |
| Included by | [Wellbore.1.3.0](../master-data/Wellbore.1.3.0.md) | allOf | one |  |
| Included by | [WellboreOpening.1.0.0](../master-data/WellboreOpening.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._