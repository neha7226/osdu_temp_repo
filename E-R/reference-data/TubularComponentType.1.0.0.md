<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# TubularComponentType
Specifies the types of components that can be used in a tubular string. These are used to specify the type of component and multiple components are used to define a tubular string (Tubular).
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--TubularComponentType:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: None
* Reference value type, governance model: LOCAL
* Link to &rarr; [Authoring Schema TubularComponentType.1.0.0.json](../../Authoring/reference-data/TubularComponentType.1.0.0.json)
* Link to &rarr; [Generated Schema TubularComponentType.1.0.0.json](../../Generated/reference-data/TubularComponentType.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource TubularComponentType.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/TubularComponentType.1.0.0.json)
* Link to &rarr; [Example Record TubularComponentType.1.0.0.json](../../Examples/reference-data/TubularComponentType.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams TubularComponentType


## Outgoing Relationships for TubularComponentType

![TubularComponentType](../_diagrams/reference-data/TubularComponentType.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## TubularComponentType Referenced by other Entities

![TubularComponentType](../_diagrams/reference-data/TubularComponentType.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# TubularComponentType Properties

## 1. Table of TubularComponentType System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-TubularComponentType:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--TubularComponentType:282efc94-eb90-5bb7-b247-6c2aba07a660|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--TubularComponentType:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of TubularComponentType Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of TubularComponentType Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of TubularComponentType Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for TubularComponentType.1.0.0

* Status: Published
* Governance: LOCAL
* The table below **is truncated** and shows only 100 of total 207 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [TubularComponentType](../../ReferenceValues/Manifests/reference-data/LOCAL/TubularComponentType.1.0.0.json).

|`id` with prefix `namespace:reference-data--TubularComponentType:`|Code|Name|Alias|Description|Source|CommitDate|
|-----|-----|-----|-----|-----|-----|-----|
|Annular%20Safety%20Valve%20Packer|Annular Safety Valve Packer|Annular Safety Valve Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Annulus%20Barrier|Annulus Barrier|Annulus Barrier|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Blind%20Pipe|Blind Pipe|Blind Pipe|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Boll%20Weevil|Boll Weevil|Boll Weevil|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Casing%20Collar|Casing Collar|Casing Collar|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Casing%20External%20Packer|Casing External Packer|Casing External Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Casing%20Head%20Housing|Casing Head Housing|Casing Head Housing|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Casing%20MLS%20Hanger|Casing MLS Hanger|Casing MLS Hanger|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Casing%20MLS%20Running%20Tool|Casing MLS Running Tool|Casing MLS Running Tool|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Casing%20Patch|Casing Patch|Casing Patch|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Casing%20Scraper|Casing Scraper|Casing Scraper|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Casing%20Swivel|Casing Swivel|Casing Swivel|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Casing|Casing|Casing|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Circulating%20Sleeve|Circulating Sleeve|Circulating Sleeve|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Circulating%20String|Circulating String|Circulating String|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Conductor.%20Stove%20Pipe|Conductor. Stove Pipe|Conductor. Stove Pipe|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Connector.%20Collar.%20Coupling|Connector. Collar. Coupling|Connector. Collar. Coupling|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Crossover%20%28diameter%20change%29%20down|Crossover (diameter change) down|Crossover (diameter change) down|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Crossover%20%28diameter%20change%29%20up|Crossover (diameter change) up|Crossover (diameter change) up|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Crossover%20%28thread%20change%29|Crossover (thread change)|Crossover (thread change)|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Debra%20Seal%20Packer|Debra Seal Packer|Debra Seal Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Dual%20Pack%20Off%20Tubing%20Hanger|Dual Pack Off Tubing Hanger|Dual Pack Off Tubing Hanger|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Dual%20Packer|Dual Packer|Dual Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Dual%20Tubing|Dual Tubing|Dual Tubing|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|ESP%20Packer|ESP Packer|ESP Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Exit%20Sub|Exit Sub|Exit Sub|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Expandable%20Solid%20Casing|Expandable Solid Casing|Expandable Solid Casing|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Extenda|Extenda|Extenda|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Extended%20neck|Extended neck|Extended neck|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|External%20casing%20Patch|External casing Patch|External casing Patch|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Extra%20Long%20Tubing%20Seal%20Receptacle|Extra Long Tubing Seal Receptacle|Extra Long Tubing Seal Receptacle|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Flag%20Joint|Flag Joint|Flag Joint|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Flex%20Lock%20Tubing%20Hanger|Flex Lock Tubing Hanger|Flex Lock Tubing Hanger|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Float%20Collar|Float Collar|Float Collar|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Gravel%20Packer|Gravel Packer|Gravel Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Holed%20Casing|Holed Casing|Holed Casing|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Hot%20Oil%20Circulating%20String|Hot Oil Circulating String|Hot Oil Circulating String|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Hydraulic%20Packer|Hydraulic Packer|Hydraulic Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Inflatable%20Packer|Inflatable Packer|Inflatable Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Inject%20through%20Packer|Inject through Packer|Inject through Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Landing%20Collar|Landing Collar|Landing Collar|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Lateral%20Window|Lateral Window|Lateral Window|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Launcher|Launcher|Launcher|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20%28WWS%29|Liner (WWS)|Liner (WWS)|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Ball%20catcher%20Sub|Liner Ball catcher Sub|Liner Ball catcher Sub|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Bullnose|Liner Bullnose|Liner Bullnose|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Collar|Liner Collar|Liner Collar|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Drop-off%20Tool|Liner Drop-off Tool|Liner Drop-off Tool|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Flapper%20Valve|Liner Flapper Valve|Liner Flapper Valve|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Funnel|Liner Funnel|Liner Funnel|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Hanger%20Packer|Liner Hanger Packer|Liner Hanger Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Hanger%2FNipple%20Sub-Assy|Liner Hanger/Nipple Sub-Assy|Liner Hanger/Nipple Sub-Assy|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Hanger|Liner Hanger|Liner Hanger|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Non%20Return%20Valve|Liner Non Return Valve|Liner Non Return Valve|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Overshot|Liner Overshot|Liner Overshot|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Packer|Liner Packer|Liner Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Plug%20Bushing%20Holder|Liner Plug Bushing Holder|Liner Plug Bushing Holder|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Reverse%20Plug%20Cutter|Liner Reverse Plug Cutter|Liner Reverse Plug Cutter|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Setting%20Sleeve|Liner Setting Sleeve|Liner Setting Sleeve|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Shear%20Sub|Liner Shear Sub|Liner Shear Sub|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Slotted%20Telltale|Liner Slotted Telltale|Liner Slotted Telltale|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Swivel|Liner Swivel|Liner Swivel|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Tie%20Back%20Packer|Liner Tie Back Packer|Liner Tie Back Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Tie%20Back%20Seal|Liner Tie Back Seal|Liner Tie Back Seal|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Tieback%20Receptacle|Liner Tieback Receptacle|Liner Tieback Receptacle|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Tieback%20Seal%20Assembly|Liner Tieback Seal Assembly|Liner Tieback Seal Assembly|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20Tieback%20Sleeve|Liner Tieback Sleeve|Liner Tieback Sleeve|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20WWS%20Telltale|Liner WWS Telltale|Liner WWS Telltale|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Liner%20X%2FO%20Bushing|Liner X/O Bushing|Liner X/O Bushing|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Mandrel%20Insert|Mandrel Insert|Mandrel Insert|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Mechanical%20Packer|Mechanical Packer|Mechanical Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Milled%20Window|Milled Window|Milled Window|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Mud%20Line%20Suspension%20Cap|Mud Line Suspension Cap|Mud Line Suspension Cap|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Mud-Line%20Tubing%20Hanger|Mud-Line Tubing Hanger|Mud-Line Tubing Hanger|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Open%20Hole%20Packer|Open Hole Packer|Open Hole Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Pack%20Off%20Tubing%20Hanger|Pack Off Tubing Hanger|Pack Off Tubing Hanger|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Packer|Packer|Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|PBR%20Female|PBR Female|PBR Female|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|PBR%20Traveller|PBR Traveller|PBR Traveller|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|PBR%20With%20Seal%20Stem|PBR With Seal Stem|PBR With Seal Stem|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Perforated%20Joint%28s%29|Perforated Joint(s)|Perforated Joint(s)|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Permanent%20Packer|Permanent Packer|Permanent Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Polished%20Bore%20Receptacles|Polished Bore Receptacles|Polished Bore Receptacles|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Pup%20Joint%20With%20Centralizer|Pup Joint With Centralizer|Pup Joint With Centralizer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Pup%20Joint|Pup Joint|Pup Joint|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Retrievable%20Packer|Retrievable Packer|Retrievable Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Slip%20and%20Seal|Slip and Seal|Slip and Seal|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Slotted%20Casing|Slotted Casing|Slotted Casing|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Split|Split|Split|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Squeeze%20Packer|Squeeze Packer|Squeeze Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Stage%20Collar|Stage Collar|Stage Collar|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Straddle%20Packer|Straddle Packer|Straddle Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Swelling%20Packer|Swelling Packer|Swelling Packer|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Tail%20End%20%28For%20Pipe%20w%2Fo%20shoe%29|Tail End (For Pipe w/o shoe)|Tail End (For Pipe w/o shoe)|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Tail%20Joint|Tail Joint|Tail Joint|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Tubing%20Drain|Tubing Drain|Tubing Drain|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Tubing%20Hanger%20%28bowl%20weevil%29|Tubing Hanger (bowl weevil)|Tubing Hanger (bowl weevil)|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Tubing%20Hanger%20%28tension%29|Tubing Hanger (tension)|Tubing Hanger (tension)|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Tubing%20Hanger|Tubing Hanger|Tubing Hanger|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Unknown|Unknown|Unknown|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|
|Wellbore%20Equipment|Wellbore Equipment|Wellbore Equipment|-|-|Workbook Published/TubularComponentType.1.0.0.xlsx; commit SHA 633c18d7.|2021-04-12T20:33:02+02:00|

**_This reference value type contains more than 100 entries and is therefore truncated. See the reference-values repository for the complete list._**


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# TubularComponentType.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [TubularComponent.1.0.0](../work-product-component/TubularComponent.1.0.0.md) | TubularComponentTypeID | one |  |
| Referred to by | [TubularComponent.1.1.0](../work-product-component/TubularComponent.1.1.0.md) | TubularComponentTypeID | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._