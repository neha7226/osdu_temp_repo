<a name="TOC"></a> Previous Chapter: [1 Introduction](01-Introduction.md)

[[_TOC_]]

# 2 Group Type

All OSDU entities must be classified by a single group type. Group type is a classification that helps to organize the
entities, and it affects some system behaviors. This chapter provides guidance on how to distinguish the main types,
even though the distinctions are not always clear or rigidly followed.

In this document, the term “business content” carries a special meaning that requires elaboration. Metadata about a
digital file is descriptive information about the file and not the file information itself. We may divide the metadata
informally into three classes:

- Business content: information extracted or inferred from the file which summarizes or highlights the subject matter
  contained in the file
- Contextual metadata: information about how the file is used or abides in the digital environment
- File information: information about the computer characteristics of the file

## 2.1 Master Data

When applied to OSDU group types, the term “master” must not be confused with conventional uses of the term in the
industry. It is not the same as mastering or golden or other such meanings. Here, an entity of the group type “master
data” refers to the information about business objects managed in the OSDU Record catalog. These objects tend to be
real-world things with natural identifiers. The function of the schema entity is to collect information about these
real-world things, so they can be tracked and related to digital objects, and to provide their context. This leads to
some characteristics:

- Objects are typically created by a company’s business processes; the catalog of objects grows steadily over time
- The ID can be created separately from the OSDU platform

  Since master data objects typically have a presence in the real world, they also often have established identifiers
  that can be supplied, possibly with some modification, before the ingestion process. Where they do not exist, the
  platform implementer may choose to establish an internal company unique identifier.

- A single version of truth is desired

  While there may be different understandings about the properties of the master object in different source systems,
  there is only one truth, and business processes are established to try to reconcile those differences. When a single
  version of truth has been established, the object is referred to as a well-known entity. This is to say that the
  well-known entity for a master data record is the authoritative source for what the implementing company knows about
  the object.

- Business processes operate on the object over time and may change its properties

  This means that the object is versioned while its identity remains the same.

- The objects tend to be physical (e.g., wellbore, oil field) or business activities (e.g., seismic acquisition); refer
  to Section 4.3.2 of the *OSDU Technical Standard* (
  see [1.4 Available Resources](01-Introduction.md#14-available-resources)) for a list of current entities

## 2.2 Reference Data

Reference data refers to data objects that are similar to master data objects, but their existence is more static. Like
master data objects, they provide context to digital objects, but rather than track their properties it is more
interesting to just know and standardize their names, as in a controlled value list. They tend to refer to concepts that
exist outside a company’s business processes. They have the following characteristics:

- The set of instance object names grows slowly (generally).
- They are often “type” concepts, i.e., they are object types that mainly express a classification of another object
  type.
- Their names and identities may be defined by entities outside the company, such as organizations that are responsible
  or are delegated responsibility for clarity in classifying Data Object Type concepts that they manage or own.
- Refer to Section 4.3.2 of the *OSDU Technical Standard* 
  (see [1.4 Available Resources](01-Introduction.md#14-available-resources))
  for a list of current entities; for example, AgreementType, DocumentType, ExistenceKind, FeatureType, LogType,
  MarkerType, ProjectRole, VelocityAnalysisMethod, and UnitOfMeasure

It is useful to note that like other Group Types, Reference Data Object Types are populated with objects (object
instances) through data platform owner/operator data management policies and practices using applications/systems/tools
fit for that purpose. A small subset of Reference Data Object Types are directly associated with the operation and
behavior of the OSDU DP. In these cases, OSDU Data Platform related administrative tools or embedded specifications
serve this purpose.

It is an unavoidable requirement that some Reference (and maybe also some Master) Data Object Types become
multi-dialect/multilingual. This will address definitions as well as behavior during app/Data Platform interactions 
guided by configuration contexts and/or local override dialect/language indicators.

Some Reference Data Object Type definitions and sets of object instances are supplied with the OSDU Data Platform
[^1]. These are controlled for each Reference Data Object Type by the level of governance established by the OSDU
Forum. The governance level for a particular Reference Data Object Type is defined in the schema definition for that
entity using the JSON schema extension property `x-osdu-governance-model`. The levels are defined as follows:

- Fixed: the values are pre-determined by agreement in the OSDU Forum

  This level of governance is usually used for reference data types that have short well-known lists and are highly
  benefited by interoperability between companies, or are required for the proper operation of the Data Platform. [^2]

- Open: a set of values agreed by the OSDU Forum, but companies may extend the list with custom values

  Custom values should not conflict with Forum values. This level of governance is used for Reference Data Object Types that
  will benefit from some amount of interoperability but also are uncontrolled enough that companies can add their own
  values. [^3]

- Local: the OSDU Forum makes no declaration about the values, though it may provide a seed list to help explain the
  entity by way of example [^4]

  Companies need to create their own value list. These reference data types have little benefit apart from
  interoperability, and agreed-upon values are hard to come by.

## 2.3 Dataset

The `dataset` group type gives metadata about digital Files, FileCollections or external datasets. It does not describe
the business content of the dataset, only the computer science aspects of the string of bytes that are stored on a
computer system; for example, it has properties like `Filesize`, the number of bytes comprising the file, or `Checksum`,
a hash of the file using the MD5 digest expressed in 32 hexadecimal characters. Key characteristics about datasets are:

- They are immutable; the underlying binary file is not updated or replaced, and only new dataset objects are created
- There are two entities defined in this group type, which is meant to cover all kinds of files, and specialized schemas
  for specific file types will be developed. For example:
    - File.GeoJSON: a dataset schema for a circular shape position on earth.
    - FileCollection.SEGY: a dataset schema for seismic files in the SEGY Rev 0 Format
      (see [Appendix A.1 External Document References](../Chapters/90-References.md#appendix-a1-external-document-references))
      will be useful to include the specialized properties needed for reading non-standard variants.

## 2.4 Work Product Component

Work product components are metadata about the logical concept. Unlike the `dataset` group type, work product components
describe the business content of a File or FileCollection entity. A work product component may describe a set of files
when the individual files are not meant to stand on their own but have information about a single logical concept spread
across more than one file. An example would be a seismic trace dataset that separates its headers from the traces into
two or more separate files. These are some characteristics of work product components:

- They are typically the output of a business object which is master data; for example, they may be measurements or
  observations from that business object, or derivatives and interpretations of those original outputs

  Note: Files are described by work product components, not master data. Any file related to a master data object is
  just that – a relation, not the master data itself. That is why master data never has its own files.

- As work product components are metadata about digital objects, copies and derivatives can be created at will, making
  identification arbitrary; that is, there is not a natural identifier that exists outside the digital world and
  concepts like Globally Unique Identifier (GUID) are better suited for identification

  Typically, the data platform will automatically supply the unique identifier when ingested. The identifier may be
  supplied prior to ingestion under certain circumstances when an existing work product component needs to be updated.
  This creates a new record with the same identifier but a new version number. Referencing objects may include the
  version number to access a particular record or omit it to access the current record. For more details, refer to *OSDU
  Data Object Identifiers*; see [1.4 Available Resources](01-Introduction.md#14-available-resources).

- Along with the file entity, work product components are immutable in the sense that if any of the business content is
  changed they become a new work product component, with a new identity

- A work product component may be versioned if *contextual* metadata is changed

- Concepts like status, purpose, or relationship may change without causing a new work product component to be formed

  Rather, the work product component may be versioned and the underlying File remains as-is. Under some circumstances,
  correcting errors or omissions in the metadata may also be legitimate reasons to version a work product component.

- The meaning of *well-known entity* takes on a different sense compared to master data

  Unlike master data, there is no single version of truth for work product components. A digital file can be copied,
  processed, transformed, interpreted, reformatted, etc. These derivatives have complicated relationships to the
  original, leaving their status and identity ambiguous. The well-known entity, then, does not designate the
  authoritative record, but it designates a work product component that is preferred over similar objects. This is the
  case when the object has been through some assurance process that allows it to be labeled as superior, which is the
  purpose of the `TechnicalAssurance` property (see [Chapter 6](06-LifecycleProperties.md)).

- Work product components have artifacts

  These are files that may be created as part of the ingestion process to expand on the original in some helpful way.
  Examples are renderings from optical character recognition or thumbnail images. Because artifacts are not new business
  content and are based on the original file, they may be added later to a work product component without forcing a new
  work product component to be created with a new identity. Of course, since all records are immutable, a new version of
  the work product component record is created with the original identity.

## 2.5 Work Product

A work product is a set of work product components brought together to the data platform for ingestion. This set may
comprise different entity types and with different genesis. What ties them together is only that they have been
delivered together for ingestion. A close analogy in the physical world is a “shipment” in that it represents a package
of materials brought to the “door” of the data system from the outside to be cataloged at that time. This entity is
helpful for audit purposes and for preventing orphans when business relationships are missing. There is only one entity
type: work product.

Sometimes, it may actually be a shipment. In this case, the properties of WorkProduct may be used to capture information
from the transmittal. For example, Name can be the shipment identifier, bar code, and so on. An AuthorID can be the
company name of the supplier. The transmittal or other supplied documents can be stored as “Documents” and included in
the set of components.

## 2.6 Example

To help explain, here is an example of the group types for a cohesive set of entities. The JSON portion is removed just
to show the meaningful parts. Note that the reference data values are just samples for illustrative purposes and are not
necessarily drawn from the OSDU Forum-approved value lists.

- Master data – Wellbore

  A Wellbore record describes the characteristics of the real-world business object of an actual drilled (or planned)
  borehole.

- Reference data – MaterialType

  A MaterialType record standardizes the names of different kinds of materials. A Wellbore record may refer to a
  MaterialType record through its `PrimaryMaterialID` property to describe what is produced by the wellbore, such as
  “Oil”.

- Work product – WorkProduct

  A WorkProduct record ties together a set of work product components ingested together. Such a set could be a group of
  well logs measured in a particular wellbore.

- Work product component – WellLog

  A WellLog record describes important business information about a Well Log file. A critical property is the identity
  of the Wellbore which was measured by the well log. Other useful properties include the Top and Bottom-Measured Depth
  represented in the well log or description of the tools used. It also tells which file or files contain the actual
  well log data.

- Dataset – File/FileCollection

  A File record describes data file information. In the case of a well log, the data file would be something like a LAS
  or DLIS file. The File record contains metadata about it, such as the location of the file in the storage system or
  the format of the file.

<details>

<summary markdown="span">master-data--Wellbore schema fragment example</summary>

<OsduImportJSON>[master-data--Wellbore, Full record](../../Examples/master-data/Wellbore.1.0.0.json#only.data.VerticalMeasurements[0])</OsduImportJSON>

```json
{
  "data": {
    "VerticalMeasurements": [
      {
        "VerticalMeasurementID": "Example VerticalMeasurementID",
        "EffectiveDateTime": "2020-02-13T09:13:15.55Z",
        "VerticalMeasurement": 12345.6,
        "TerminationDateTime": "2020-02-13T09:13:15.55Z",
        "VerticalMeasurementTypeID": "namespace:reference-data--VerticalMeasurementType:ArbitraryPoint:",
        "VerticalMeasurementPathID": "namespace:reference-data--VerticalMeasurementPath:MeasuredDepth:",
        "VerticalMeasurementSourceID": "namespace:reference-data--VerticalMeasurementSource:DRL:",
        "WellboreTVDTrajectoryID": "namespace:work-product-component--WellboreTrajectory:WellboreTrajectory-911bb71f-06ab-4deb-8e68-b8c9229dc76b:",
        "VerticalMeasurementUnitOfMeasureID": "namespace:reference-data--UnitOfMeasure:m:",
        "VerticalCRSID": "namespace:reference-data--CoordinateReferenceSystem:BoundProjected:EPSG::32021_EPSG::15851:",
        "VerticalReferenceID": "Example VerticalReferenceID",
        "VerticalReferenceEntityID": "namespace:master-data--Rig:SomeUniqueRigID:",
        "VerticalMeasurementDescription": "Example VerticalMeasurementDescription"
      }
    ]
  }
}
```

</details>

<details>

<summary markdown="span">work-product--WorkProduct schema fragment example</summary>

<OsduImportJSON>[work-product--WorkProduct, Full record](../../Examples/work-product/WorkProduct.1.0.0.json#only.data.BusinessActivities)</OsduImportJSON>

```json
{
  "data": {
    "BusinessActivities": [
      "Example BusinessActivities"
    ]
  }
}
```

</details>

<details>

<summary markdown="span">work-product-component--WellLog schema fragment example</summary>

<OsduImportJSON>[work-product-component--WellLog, Full record](../../Examples/work-product-component/WellLog.1.1.0.json#only.data.SpatialArea.AsIngestedCoordinates)</OsduImportJSON>

```json
{
  "data": {
    "SpatialArea": {
      "AsIngestedCoordinates": {
        "type": "AnyCrsFeatureCollection",
        "CoordinateReferenceSystemID": "namespace:reference-data--CoordinateReferenceSystem:BoundProjected:EPSG::32021_EPSG::15851:",
        "VerticalCoordinateReferenceSystemID": "namespace:reference-data--CoordinateReferenceSystem:Vertical:EPSG::5714:",
        "persistableReferenceCrs": "{\"authCode\":{\"auth\":\"OSDU\",\"code\":\"32021079\"},\"lateBoundCRS\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"32021\"},\"name\":\"NAD_1927_StatePlane_North_Dakota_South_FIPS_3302\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"PROJCS[\\\"NAD_1927_StatePlane_North_Dakota_South_FIPS_3302\\\",GEOGCS[\\\"GCS_North_American_1927\\\",DATUM[\\\"D_North_American_1927\\\",SPHEROID[\\\"Clarke_1866\\\",6378206.4,294.9786982]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Lambert_Conformal_Conic\\\"],PARAMETER[\\\"False_Easting\\\",2000000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",-100.5],PARAMETER[\\\"Standard_Parallel_1\\\",46.18333333333333],PARAMETER[\\\"Standard_Parallel_2\\\",47.48333333333333],PARAMETER[\\\"Latitude_Of_Origin\\\",45.66666666666666],UNIT[\\\"Foot_US\\\",0.3048006096012192],AUTHORITY[\\\"EPSG\\\",32021]]\"},\"name\":\"NAD27 * OGP-Usa Conus / North Dakota CS27 South zone [32021,15851]\",\"singleCT\":{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"15851\"},\"name\":\"NAD_1927_To_WGS_1984_79_CONUS\",\"type\":\"ST\",\"ver\":\"PE_10_9_1\",\"wkt\":\"GEOGTRAN[\\\"NAD_1927_To_WGS_1984_79_CONUS\\\",GEOGCS[\\\"GCS_North_American_1927\\\",DATUM[\\\"D_North_American_1927\\\",SPHEROID[\\\"Clarke_1866\\\",6378206.4,294.9786982]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],GEOGCS[\\\"GCS_WGS_1984\\\",DATUM[\\\"D_WGS_1984\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],METHOD[\\\"NADCON\\\"],PARAMETER[\\\"Dataset_conus\\\",0.0],OPERATIONACCURACY[5.0],AUTHORITY[\\\"EPSG\\\",15851]]\"},\"type\":\"EBC\",\"ver\":\"PE_10_9_1\"}",
        "persistableReferenceVerticalCrs": "{\"authCode\":{\"auth\":\"EPSG\",\"code\":\"5714\"},\"name\":\"MSL_Height\",\"type\":\"LBC\",\"ver\":\"PE_10_9_1\",\"wkt\":\"VERTCS[\\\"MSL_Height\\\",VDATUM[\\\"Mean_Sea_Level\\\"],PARAMETER[\\\"Vertical_Shift\\\",0.0],PARAMETER[\\\"Direction\\\",1.0],UNIT[\\\"Meter\\\",1.0],AUTHORITY[\\\"EPSG\\\",5714]]\"}",
        "persistableReferenceUnitZ": "{\"scaleOffset\":{\"scale\":1.0,\"offset\":0.0},\"symbol\":\"m\",\"baseMeasurement\":{\"ancestry\":\"Length\",\"type\":\"UM\"},\"type\":\"USO\"}",
        "features": [
          {
            "type": "AnyCrsFeature",
            "properties": {},
            "geometry": {
              "type": "AnyCrsPoint",
              "coordinates": [
                12345.6,
                12345.6
              ],
              "bbox": [
                12345.6,
                12345.6,
                12345.6,
                12345.6
              ]
            },
            "bbox": [
              12345.6,
              12345.6,
              12345.6,
              12345.6
            ]
          }
        ],
        "bbox": [
          12345.6,
          12345.6,
          12345.6,
          12345.6
        ]
      }
    }
  }
}
```

</details>

<details>

<summary markdown="span">dataset--File.Generic.CSV schema fragment example</summary>

<OsduImportJSON>[dataset--File.Generic.CSV, Full record](../../Examples/WorkedExamples/WellboreTrajectory/datasets/File.Generic.CSV.8816.completed.json#only.data)</OsduImportJSON>

```json
{
  "data": {
    "ResourceSecurityClassification": "partition-id:reference-data--ResourceSecurityClassification:RESTRICTED:",
    "Name": "Dataset Trajectory 8816 completed",
    "Description": "As computed.",
    "TotalSize": "51793",
    "EncodingFormatTypeID": "partition-id:reference-data--EncodingFormatType:text%2Fcsv:",
    "DatasetProperties": {
      "FileSourceInfo": {
        "FileSource": "s3://default_bucket/r1/data/provided/documents/trajectories/8816.completed.csv",
        "PreloadFileCreateUser": "somebody@acme.org",
        "PreloadFileCreateDate": "2019-12-16T11:46:20.163Z",
        "PreloadFileModifyUser": "somebody.else@acme.org",
        "PreloadFileModifyDate": "2019-12-20T17:20:05.356Z",
        "Name": "8816.completed.csv",
        "FileSize": "51793"
      }
    },
    "ExtensionProperties": {}
  }
}
```

</details>

## Links

1. Return to the [Beginning of the Chapter](#TOC)
2. Next Chapter: [3 Vertical Measurements](03-VerticalMeasurements.md)
3. Browse to the [Schema Usage Guide Table of Contents](../README.md)

---
Footnotes
[^1]: The completeness of the OSDU Forum delivered sets depends on the governance model.
[^2]: For **Fixed** governance, OSDU is obliged to provide the complete set of Reference Data Object Types.
[^3]: For **Open** governance the OSDU Forum a fairly complete set of Reference Data Object Types, with the purpose of 
achieving platform and application interoperability. 
[^4]: The set of Reference Data Object Types provided by OSDU for **Local** governance may range from a set sufficient 
for OSDU Forum release test cases to opportunistic sets that are believed by the Forum to be enablers for use by Data 
Platform owners/operators to support their needs and foster interoperability. 