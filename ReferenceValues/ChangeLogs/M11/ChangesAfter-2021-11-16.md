<a name="TOC"></a>

[[_TOC_]]

# Reference Value Changes

## `osdu:wks:reference-data--WellBusinessIntention:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|Appraise|2022-02-11|New value|
|Develop|2022-02-11|New value|
|Explore|2022-02-11|New value|
|Extend|2022-02-11|New value|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--WellInterestType:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|Yes|2022-02-24|New Value|
|Yes.FinancialOperated|2022-02-24|New Value|
|Yes.FinancialNonOperated|2022-02-24|New Value|
|Yes.Obligatory|2022-02-24|New Value|
|Yes.Technical|2022-02-24|New Value|
|No|2022-02-24|New Value|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--WellboreTrajectoryType:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|Inclined.DoubleKickOff|2022-02-10|Changed from Directional to inclined|
|Inclined.SlantHole|2022-02-10|Added Hierarchy and additional comment|
|Inclined.SType|2022-02-10|Added Hierarchy and additional comment|
|Horizontal.Level|2022-02-10|Added Hierarchy and additional comment|
|Horizontal.ToeDown|2022-02-10|Added Hierarchy and additional comment|
|Horizontal.ToeUp|2022-02-10|Added Hierarchy and additional comment|
|Horizontal.Undulating|2022-02-10|Added Hierarchy and additional comment|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--AzimuthReferenceType:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|AssumedGridNorth|2021-12-17|Added definition|
|AssumedMagneticNorth|2021-12-17|Added definition|
|AssumedTrueNorth|2021-12-17|Added definition|
|AstronomicNorth|2022-01-24|Revised definition. Merged together with PPDM and OSDU original definitions|
|GridNorth|2022-01-24|Revised definition. Merged together with PPDM and OSDU original definitions|
|InferredGridNorth|2021-12-17|Added definition|
|InferredMagneticNorth|2021-12-17|Added definition|
|InferredTrueNorth|2021-12-17|Added definition|
|MagneticNorth|2022-01-24|Revised definition. Merged together with PPDM and OSDU original definitions|
|TrueNorth|2022-01-24|Revised definition. Merged together with PPDM and OSDU original definitions|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--OperatingEnvironment:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|Onshore.Desert|2022-01-14|New value added|
|Onshore.FlowingWater|2022-01-14|New value added|
|Onshore.Forest|2022-01-14|New value added|
|Onshore.Grassland|2022-01-14|New value added|
|Onshore.Lake|2022-01-14|New value added|
|Onshore.Marsh|2022-01-14|New value added|
|Onshore.Permafrost|2022-01-14|New value added|
|Midshore.Transition|2022-01-14|New value added|
|Onshore.Tundra|2022-01-14|New value added|

[Back to TOC](#TOC)