<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# MaterialType [Status: DEPRECATED]
DEPRECATED: Used to describe the type of materials. This catalog contains a mixture of very different materials and has been replaced by TubularMaterialType and WellProductType.
* **_Warning: Stop using this entity._**
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--MaterialType:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: OSDU
* Reference value type, governance model: OPEN
* Link to &rarr; [Authoring Schema MaterialType.1.0.0.json](../../Authoring/reference-data/MaterialType.1.0.0.json)
* Link to &rarr; [Generated Schema MaterialType.1.0.0.json](../../Generated/reference-data/MaterialType.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource MaterialType.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/MaterialType.1.0.0.json)
* Link to &rarr; [Example Record MaterialType.1.0.0.json](../../Examples/reference-data/MaterialType.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams MaterialType


## Outgoing Relationships for MaterialType

![MaterialType](../_diagrams/reference-data/MaterialType.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## MaterialType Referenced by other Entities

![MaterialType](../_diagrams/reference-data/MaterialType.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# MaterialType Properties

## 1. Table of MaterialType System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-MaterialType:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--MaterialType:fd9327a1-173a-5b61-84aa-14e3dbd1805b|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--MaterialType:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of MaterialType Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of MaterialType Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of MaterialType Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for MaterialType.1.0.0

* Status: Published
* Governance: OPEN
* The table below shows all 48 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [MaterialType](../../ReferenceValues/Manifests/reference-data/OPEN/MaterialType.1.0.0.json).

|`id` with prefix `namespace:reference-data--MaterialType:`|Code|Name|Alias|Description|InactiveIndicator|AttributionAuthority|Comment|Source|CommitDate|NameAlias|AttributionPublication|AttributionRevision|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|13Cr1Ni|~~13Cr1Ni~~|~~13Cr1Ni~~|~~-~~|~~Tubular Component made from &#x27;13Cr1Ni&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|13CrL80Casing%26Tubing|~~13CrL80Casing&amp;Tubing~~|~~13CrL80Casing&amp;Tubing~~|~~-~~|~~Tubular Component made from &#x27;13CrL80Casing&amp;Tubing&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|13Cr|~~13Cr~~|~~13Cr~~|~~-~~|~~Tubular Component made from &#x27;13Cr&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|1515LCDrillCollar|~~1515LCDrillCollar~~|~~1515LCDrillCollar~~|~~-~~|~~Tubular Component made from &#x27;1515LCDrillCollar&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|1515LC|~~1515LC~~|~~1515LC~~|~~-~~|~~Tubular Component made from &#x27;1515LC&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|174PH|~~174PH~~|~~174PH~~|~~-~~|~~Tubular Component made from &#x27;174PH&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|203522Cr35Ni4Mo|~~203522Cr35Ni4Mo~~|~~203522Cr35Ni4Mo~~|~~-~~|~~Tubular Component made from &#x27;203522Cr35Ni4Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|253525Cr32Ni3Mo|~~253525Cr32Ni3Mo~~|~~253525Cr32Ni3Mo~~|~~-~~|~~Tubular Component made from &#x27;253525Cr32Ni3Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|255025Cr50Ni6Mo|~~255025Cr50Ni6Mo~~|~~255025Cr50Ni6Mo~~|~~-~~|~~Tubular Component made from &#x27;255025Cr50Ni6Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|4140110|~~4140110~~|~~4140110~~|~~-~~|~~Tubular Component made from &#x27;4140110&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|4140|~~4140~~|~~4140~~|~~-~~|~~Tubular Component made from &#x27;4140&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|42Cr4Mo|~~42Cr4Mo~~|~~42Cr4Mo~~|~~-~~|~~Tubular Component made from &#x27;42Cr4Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|718|~~718~~|~~718~~|~~-~~|~~Tubular Component made from &#x27;718&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|82521Cr42Ni3Mo|~~82521Cr42Ni3Mo~~|~~82521Cr42Ni3Mo~~|~~-~~|~~Tubular Component made from &#x27;82521Cr42Ni3Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|925|~~925~~|~~925~~|~~-~~|~~Tubular Component made from &#x27;925&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|9Cr1Mo|~~9Cr1Mo~~|~~9Cr1Mo~~|~~-~~|~~Tubular Component made from &#x27;9Cr1Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|9CrL80Casing%26Tubing|~~9CrL80Casing&amp;Tubing~~|~~9CrL80Casing&amp;Tubing~~|~~-~~|~~Tubular Component made from &#x27;9CrL80Casing&amp;Tubing&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Alloy25DrillCollar|~~Alloy25DrillCollar~~|~~Alloy25DrillCollar~~|~~-~~|~~Tubular Component made from &#x27;Alloy25DrillCollar&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Alloy2827Cr31Ni4Mo|~~Alloy2827Cr31Ni4Mo~~|~~Alloy2827Cr31Ni4Mo~~|~~-~~|~~Tubular Component made from &#x27;Alloy2827Cr31Ni4Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Brass|~~Brass~~|~~Brass~~|~~-~~|~~Tubular Component made from &#x27;Brass&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Condensate|~~Condensate~~|~~Condensate~~|~~-~~|~~The Condensate Product Kind is a natural gas mixture that exists in a gas phase at reservoir conditions but condenses to a liquid phase as the gas temperature drops during production and processing.~~|~~True~~|~~The Open Group~~|~~Dropped &quot;Gas&quot; prefix from the name~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]~~|~~OSDU Data Platform~~|~~Evergreen~~|
|CRAC27615Cr60Ni16Mo|~~CRAC27615Cr60Ni16Mo~~|~~CRAC27615Cr60Ni16Mo~~|~~-~~|~~Tubular Component made from &#x27;CRAC27615Cr60Ni16Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|CS|~~CS~~|~~CS~~|~~-~~|~~Tubular Component made from &#x27;CS&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Duplex22Cr5Ni3Mo|~~Duplex22Cr5Ni3Mo~~|~~Duplex22Cr5Ni3Mo~~|~~-~~|~~Tubular Component made from &#x27;Duplex22Cr5Ni3Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Duplex25Cr7Ni3Mo|~~Duplex25Cr7Ni3Mo~~|~~Duplex25Cr7Ni3Mo~~|~~-~~|~~Tubular Component made from &#x27;Duplex25Cr7Ni3Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|G322Cr50Ni7Mo|~~G322Cr50Ni7Mo~~|~~G322Cr50Ni7Mo~~|~~-~~|~~Tubular Component made from &#x27;G322Cr50Ni7Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Gas|~~Gas~~|~~Gas~~|~~-~~|~~The Gas Product Kind is a hydrocarbon substance that exists as a vapor at reservoir conditions or flashes into a vapor during subsequent processing.~~|~~True~~|~~The Open Group~~|~~-~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]~~|~~OSDU Data Platform~~|~~Evergreen~~|
|Monel|~~Monel~~|~~Monel~~|~~-~~|~~Tubular Component made from &#x27;Monel&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|NonhydrocarbonGas|~~NonhydrocarbonGas~~|~~Nonhydrocarbon Gas~~|~~-~~|~~The Nonhydrocarbon Gas Product Kind is a non-hydrocarbon substance that exists as a vapor at reservoir conditions or flashes into a vapor during subsequent processing.~~|~~True~~|~~The Open Group~~|~~-~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]~~|~~OSDU Data Platform~~|~~Evergreen~~|
|Oil|~~Oil~~|~~Oil~~|~~-~~|~~The Oil Product Kind is a mixture of hydrocarbon substances that occurs as a liquid at stock tank conditions.~~|~~True~~|~~The Open Group~~|~~-~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]~~|~~OSDU Data Platform~~|~~Evergreen~~|
|SST|~~SST~~|~~SST~~|~~-~~|~~Tubular Component made from &#x27;SST&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|StainlessSteel|~~StainlessSteel~~|~~StainlessSteel~~|~~-~~|~~Tubular Component made from &#x27;StainlessSteel&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Steam|~~Steam~~|~~Steam~~|~~-~~|~~The Steam Product Kind is water in the gas phase.~~|~~True~~|~~PPDM~~|~~Replaced &quot;type&quot; with &quot;kind&quot;~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]~~|~~Well Status &amp; Classification~~|~~v3~~|
|SteelCasing%26Tubing|~~SteelCasing&amp;Tubing~~|~~SteelCasing&amp;Tubing~~|~~-~~|~~Tubular Component made from &#x27;SteelCasing&amp;Tubing&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|SteelCoiledTubing|~~SteelCoiledTubing~~|~~SteelCoiledTubing~~|~~-~~|~~Tubular Component made from &#x27;SteelCoiledTubing&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|SteelDrillCollar|~~SteelDrillCollar~~|~~SteelDrillCollar~~|~~-~~|~~Tubular Component made from &#x27;SteelDrillCollar&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|SteelDrillPipe|~~SteelDrillPipe~~|~~SteelDrillPipe~~|~~-~~|~~Tubular Component made from &#x27;SteelDrillPipe&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|SteelLinePipeASTM|~~SteelLinePipeASTM~~|~~SteelLinePipeASTM~~|~~-~~|~~Tubular Component made from &#x27;SteelLinePipeASTM&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|SteelLinePipe|~~SteelLinePipe~~|~~SteelLinePipe~~|~~-~~|~~Tubular Component made from &#x27;SteelLinePipe&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|SteelLowAlloySteel|~~SteelLowAlloySteel~~|~~SteelLowAlloySteel~~|~~-~~|~~Tubular Component made from &#x27;SteelLowAlloySteel&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Steel|~~Steel~~|~~Steel~~|~~-~~|~~Tubular Component made from &#x27;Steel&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Super13Cr4Ni1Mo|~~Super13Cr4Ni1Mo~~|~~Super13Cr4Ni1Mo~~|~~-~~|~~Tubular Component made from &#x27;Super13Cr4Ni1Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Super13Cr5Ni1Mo|~~Super13Cr5Ni1Mo~~|~~Super13Cr5Ni1Mo~~|~~-~~|~~Tubular Component made from &#x27;Super13Cr5Ni1Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Super13Cr5Ni2Mo|~~Super13Cr5Ni2Mo~~|~~Super13Cr5Ni2Mo~~|~~-~~|~~Tubular Component made from &#x27;Super13Cr5Ni2Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Super13Cr5Ni3Mo|~~Super13Cr5Ni3Mo~~|~~Super13Cr5Ni3Mo~~|~~-~~|~~Tubular Component made from &#x27;Super13Cr5Ni3Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|SuperDuplex25Cr7Ni4Mo|~~SuperDuplex25Cr7Ni4Mo~~|~~SuperDuplex25Cr7Ni4Mo~~|~~-~~|~~Tubular Component made from &#x27;SuperDuplex25Cr7Ni4Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|SuperDuplex26Cr6Ni3Mo|~~SuperDuplex26Cr6Ni3Mo~~|~~SuperDuplex26Cr6Ni3Mo~~|~~-~~|~~Tubular Component made from &#x27;SuperDuplex26Cr6Ni3Mo&#x27; based material~~|~~True~~|~~OSDU~~|~~TubularComponentValue~~|~~Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.~~|~~2021-06-22T16:46:22+00:00~~|~~-~~|~~-~~|~~-~~|
|Water|Water|Water|-|The Water Product Kind is a chemical substance of hydrogen and oxygen (H2O). In common usage, water is the liquid phase.|-|PPDM|Remove last sentence &quot;Water is denser than oil or gas.&quot; Replaced &quot;type&quot; with &quot;kind&quot;|Workbook Published/MaterialType.1.0.0.xlsx; commit SHA 6966496a.|2021-06-22T16:46:22+00:00|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:PPDM:"<br>&nbsp;&nbsp;}<br>]|Well Status &amp; Classification|v3|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# MaterialType.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [TubularComponent.1.0.0](../work-product-component/TubularComponent.1.0.0.md) | TubularComponentMaterialTypeID | one |  |
| Referred to by | [Wellbore.1.0.0](../master-data/Wellbore.1.0.0.md) | PrimaryMaterialID | one |  |
| Referred to by | [Wellbore.1.1.0](../master-data/Wellbore.1.1.0.md) | PrimaryMaterialID | one |  |
| Referred to by | [Wellbore.1.1.1](../master-data/Wellbore.1.1.1.md) | PrimaryMaterialID | one |  |
| Referred to by | [Wellbore.1.2.0](../master-data/Wellbore.1.2.0.md) | PrimaryMaterialID | one |  |
| Referred to by | [Wellbore.1.3.0](../master-data/Wellbore.1.3.0.md) | PrimaryMaterialID | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._