<a name="TOC"></a>**Table of Contents**

[[_TOC_]]

Changes since: 2022-07-22

* [Detailed Change Log](ChangesAfter-2022-07-22.md) for reference values.

# CoordinateReferenceSystem Name Changes

As a consequence of issue [reference-values#149](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/reference-values/-/issues/149)
421 out of 1277 BoundCRS names were corrected. The full list of old versus new names [is available here](CRS-Catalog-Changes.md).

# SeismicProcessingStageType

This reference value list is now taken over by PPDM. As a consequence, three new values were added and two value were
deprecated because PPDM thought they belonged to a different activity rather than a stage.

|Deprecated `id` | Superseded by ìd` | Superseded Name | Description |
|---|---|---|---|
|partition-id:reference-data--SeismicProcessingStageType:ModelingResult|Not endorsed by PPDM|-|Indicates that the Seismic processing is at the Modeling Result stage|
|partition-id:reference-data--SeismicProcessingStageType:SyntheticModel|Not endorsed by PPDM|-|Indicates that the Seismic processing is at the Synthetic modeling stage|

**_New Values_**

| `id`                                                                  | Description                                                                                                                                                                                                                                                                    |
|-----------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
 | partition_id:reference-data--SeismicProcessingStageType:Deconvolution | A process designed to restore a waveshape to the form it had before it underwent a linear filtering action (convolution)                                                                                                                                                       |
| partition_id:reference-data--SeismicProcessingStageType:Demultiple    | Corrections applied to seismic data to compensate for the effects of multiple reflections                                                                                                                                                                                      |
| partition_id:reference-data--SeismicProcessingStageType:Demultiplex   | Corrections applied to seismic data to compensate for the effects ofvariations in elevation, near-surface low-velocity-layer (weathering) thickness, weathering velocity, and/or reference to a datumTo separate the individual component channels that have been multiplexed. |
| partition_id:reference-data--SeismicProcessingStageType:Statics       | Often called statics, a bulk shift of a seismic trace in time during seismic processing. A common static correction is the weathering correction, which compensates for a layer of low seismic velocity material near the surface of the Earth.                                |

[Back to TOC](#TOC)

---

# ActivityStatus

This new reference value list allows the state classification: _Workflow status for the state of the Activity, e.g.,
Planned, InProgress, Completed and more. This is a OPEN value list. State transitions follow the activity's life cycle._

The reference list is under OSDU's OPEN governance. The following new values are defined:

| `id`                                                   | Name        | Description                                                                                                                                        |
|--------------------------------------------------------|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--ActivityStatus:Aborted    | Aborted     | The activity state transitioned from Planned, InProgress or Suspended to an aborted state. The activity is not completed, the final state.         |-|
| partition-id:reference-data--ActivityStatus:Completed  | Completed   | The ongoing (InProgress) activity transitioned into the completed state. This is the final ActivityStatus for a successful completion.             |-|
| partition-id:reference-data--ActivityStatus:InProgress | In Progress | A Planned or Suspended activity has changed state to InProgress, which means the activity is ongoing but neither Completed, Suspended nor Aborted. |-|
| partition-id:reference-data--ActivityStatus:Planned    | Planned     | The activity is in planned state, the first of all states in the activity life cycle.                                                              |-|
| partition-id:reference-data--ActivityStatus:Suspended  | Suspended   | The activity was previously in InProgress state but has been paused temporarily. The next state transition is to InProgress (continue) or Aborted. |-|

[Back to TOC](#TOC)

---

# Rock Sample Analysis

The OSDU Petrophysics work stream introduces four **_new_** reference value types:

## GrainSizeAnalysisMethod

**_New in M14:_** _A reference-value record describing a GrainSizeAnalysis method._

| `id`                                                               | Name           | Description                                                                                                                                                |
|--------------------------------------------------------------------|----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--GrainSizeAnalysisMethod:LPSA          | LPSA           | Laser particle size analysis, LPSA, is an optical sieve technique.                                                                                         |-|
| partition-id:reference-data--GrainSizeAnalysisMethod:SieveAnalysis | Sieve Analysis | A method using a set of sieves with decreasing opening sizes, simply shaking the sample in sieves until the amount retained becomes more or less constant. |-|

[Back to TOC](#TOC)

---

## GrainSizeClassification

**_New in M14:_** _The textual label of a grain size classification interval. GrainSizeClassification codes are referred to by GrainSizeClassificationSchemes, which will associate an actual grain size range and optionally ASTM sieve number to a GrainSizeClassification._

| `id`                                                                                   | Name               | Description                                                   |
|----------------------------------------------------------------------------------------|--------------------|---------------------------------------------------------------|
| partition-id:reference-data--GrainSizeClassification:ISO.Boulder                       | Boulder            | ISO 14668-1 scale: Grain size between 200 mm and 630 mm.      |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Clay                          | Clay               | ISO 14668-1 scale: Grain size below 0.002 mm.                 |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Cobble                        | Cobble             | ISO 14668-1 scale: Grain size between 63 mm and 200 mm.       |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Gravel.CoarseGravel           | Coarse gravel      | ISO 14668-1 scale: Grain size between 20 mm and 63 mm.        |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Gravel.FineGravel             | Fine gravel        | ISO 14668-1 scale: Grain size between 2.0 mm and 6.3 mm.      |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Gravel.MediumGravel           | Medium gravel      | ISO 14668-1 scale: Grain size between 6.3 mm and 20 mm.       |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Gravel                        | Gravel             | ISO 14668-1 scale: Grain size between 2.0 mm and 63 mm.       |-|
| partition-id:reference-data--GrainSizeClassification:ISO.LargeBoulder                  | Large boulder      | ISO 14668-1 scale: Grain size greater than 630 mm.            |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Sand.CoarseSand               | Coarse sand        | ISO 14668-1 scale: Grain size between 0.63 mm and 2.0 mm.     |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Sand.FineSand                 | Fine sand          | ISO 14668-1 scale: Grain size between 0.063 mm and 0.2 mm.    |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Sand.MediumSand               | Medium sand        | ISO 14668-1 scale: Grain size between 0.2 mm and 0.63 mm.     |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Sand                          | Sand               | ISO 14668-1 scale: Grain size between 0.0063 mm and 2.0 mm.   |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Silt.CoarseSilt               | Coarse silt        | ISO 14668-1 scale: Grain size between 0.02 mm and 0.063 mm.   |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Silt.FineSilt                 | Fine silt          | ISO 14668-1 scale: Grain size between 0.002 mm and 0.0063 mm. |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Silt.MediumSilt               | Medium silt        | ISO 14668-1 scale: Grain size between 0.0063 mm and 0.02 mm.  |-|
| partition-id:reference-data--GrainSizeClassification:ISO.Silt                          | Silt               | ISO 14668-1 scale: Grain size between 0.0002 mm and 0.063 mm. |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Boulder                 | Boulder            | Wentworth scale: Grain size larger than 256 mm                |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Clay                    | Clay               | Wentworth scale: Grain size between 0.98 μm and 3.9 μm.       |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.CoarseSand              | Coarse sand        | Wentworth scale: Grain size between 0.5 mm and 1 mm.          |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Cobble                  | Cobble             | Wentworth scale: Grain size between 64 mm and 256 mm.         |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Colloid                 | Colloid            | Wentworth scale: Grain size between 0.95 nm and 977 nm.       |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.FineSand                | Fine sand          | Wentworth scale: Grain size between 0.125 mm and 0.250 mm.    |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.MediumSand              | Medium sand        | Wentworth scale: Grain size between 0.25 mm and 0.5 mm.       |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Pebble.CoarseGravel     | Coarse gravel      | Wentworth scale: Grain size between 16 mm and 32 mm.          |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Pebble.FineGravel       | Fine gravel        | Wentworth scale: Grain size between 4 mm and 8 mm.            |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Pebble.MediumGravel     | Medium gravel      | Wentworth scale: Grain size between 8 mm and 16 mm.           |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Pebble.VeryCoarseGravel | Very coarse gravel | Wentworth scale: Grain size between 32 mm and 64 mm.          |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Pebble.VeryFineGravel   | Very fine gravel   | Wentworth scale: Grain size between 2 mm and 4 mm.            |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Pebble                  | Pebble             | Wentworth scale: Grain size between 2 mm and 64 mm.           |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.Silt                    | Silt               | Wentworth scale: Grain size between 0.0039 mm and 0.0625 mm.  |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.VeryCoarseSand          | Very coarse sand   | Wentworth scale: Grain size between 1mm and 2 mm.             |-|
| partition-id:reference-data--GrainSizeClassification:Wentworth.VeryFineSand            | Very fine sand     | Wentworth scale: Grain size between 0.0625 mm and 0.125 mm.   |-|

[Back to TOC](#TOC)

---

## GrainSizeClassificationScheme

**_New in M14:_** _GrainSizeClassificationScheme contains a table of particle size to GrainSizeClassification associations as well as optionally ASTM sieve numbers._

| `id`                                                                                          | Name                                  | Description                                                                                                                                                                  |
|-----------------------------------------------------------------------------------------------|---------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--GrainSizeClassificationScheme:CompleteISO-Scheme                 | Complete ISO GrainSize Scheme         | A scheme based on the finest level of ISO grain size classifications. The sieve numbers refer to the next finest ASTM sieve number, when not exactly matching the ISO sizes. |-|
| partition-id:reference-data--GrainSizeClassificationScheme:WesternAustraliaOffshore-6-Classes | WesternAustralia (offshore) 6 Classes | The classification schema using 6 classes, typically applied in offshore Western Australia.                                                                                  |-|

[Back to TOC](#TOC)

---

## RockSampleAnalysisType

**_New in M14:_** _A reference value catalog containing the names of the nested analysis objects in the work-product-component--RockSampleAnalysis. Examples are RoutineCoreAnalysis, GrainSizeAnalysis. Using these values it will be possible to query for RockSampleAnalysis records containing a specific kind of analysis. This list is fixed and governed by OSDU._

| `id`                                                                    | Name                | Description                                                                                                                                                                                                              |
|-------------------------------------------------------------------------|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--RockSampleAnalysisType:GrainSizeAnalysis   | GrainSizeAnalysis   | Indicates the presence of a GrainSizeAnalysis data structure in a work-product-component--RockSampleAnalysis record. The analysis details including grain size distributions are kept in associated datasets.            |-|
| partition-id:reference-data--RockSampleAnalysisType:RoutineCoreAnalysis | RoutineCoreAnalysis | Indicates the presence of a RoutineCoreAnalysis data structure in a work-product-component-RockSampleAnalysis containing porosity, grain density, permeability measurement with their associated measurement conditions. |-|


[Back to TOC](#TOC)

---


# Wellbore Completion Related Reference Value Types

## Perforation Related Reference Value Types

With M13 a number of provisional reference value lists were added and kept in the Authoring folder with the intent to be
reviewed and consolidated. This work has been completed for M14. Some codes have been changed or removed when deemed not
required. Since the values were still in the authoring phase, codes were not deprecated with superseding values.

| Governance Mode | Reference Data Type<br>with link to Manifest                                                                                         | Description                                                                                                                               |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| FIXED           | [PerforationGunCarrierCategory:1.0.0](../../Manifests/reference-data/FIXED/PerforationGunCarrierCategory.1.0.0.json)                 | The Perforating Gun Category used in a perforation interval.                                                                              |
| FIXED           | [PerforationGunCarrierType:1.0.0](../../Manifests/reference-data/FIXED/PerforationGunCarrierType.1.0.0.json)                         | The Perforating Gun Carrier Type  used in a perforation interval.                                                                         |
| OPEN            | [AnnularFluidType:1.0.0](../../Manifests/reference-data/OPEN/AnnularFluidType.1.0.0.json)                                            | Type of fluid in wellbore at time of perforation job.                                                                                     |
| OPEN            | [BottomHolePressureType.1.0.0.json](../../Manifests/reference-data/OPEN/BottomHolePressureType.1.0.0.json)                           | The Method used to calculate or measure bottomhole pressure during perforation job.                                                       |
| OPEN            | [PerforationCentralizationMethodType.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationCentralizationMethodType.1.0.0.json) | The Perforating Gun Centralization Method Type used in a perforation interval.                                                            |
| OPEN            | [PerforationConveyedMethod.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationConveyedMethod.1.0.0.json)                     | Equipment type used to run perforation equipment downhole.                                                                                |
| OPEN            | [PerforationGunChargeShape.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationGunChargeShape.1.0.0.json)                     | The Perforating Gun Charge Shape used in a perforation interval.                                                                          |
| OPEN            | [PerforationGunChargeSize.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationGunChargeSize.1.0.0.json)                       | The Perforating Gun Charge Size used in a perforation interval. The value is a number as string, always measured in grams (g = 0.001 kg). |
| OPEN            | [PerforationGunChargeType.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationGunChargeType.1.0.0.json)                       | The Perforating Gun Charge Type used in a perforation interval.                                                                           |
| OPEN            | [PerforationGunFiringHeadType.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationGunFiringHeadType.1.0.0.json)               | The Perforating Gun Firing Head Type used in a perforation interval.                                                                      |
| OPEN            | [PerforationGunMetallurgyType.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationGunMetallurgyType.1.0.0.json)               | The Perforating Gun Metallurgy Type used in a perforation interval.                                                                       |
| OPEN            | [PerforationGunPhasingType.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationGunPhasingType.1.0.0.json)                     | The Perforating Gun Phasing (the angle between perforations), always measured in degrees of arc.                                          |
| OPEN            | [PerforationIntervalReason.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationIntervalReason.1.0.0.json)                     | Reason why the perforation interval was constructed.                                                                                      |
| OPEN            | [PerforationIntervalType.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationIntervalType.1.0.0.json)                         | The method type used during the perforation of an interval.                                                                               |
| OPEN            | [PerforationIntervalWLSize.1.0.0.json](../../Manifests/reference-data/OPEN/PerforationIntervalWLSize.1.0.0.json)                     | Wireline Diameter as string value, always measured in inches  (=0.0254 m).                                                                |
| LOCAL           | [PerforationGunCarrierModel:1.0.0](../../Manifests/reference-data/LOCAL/PerforationGunCarrierModel.1.0.0.json)                       | The Perforating Gun Carrier Model used in a perforation interval.                                                                         |
| LOCAL           | [PerforationPillType.1.0.0.json](../../Manifests/reference-data/LOCAL/PerforationPillType.1.0.0.json)                                | Liquid pill pumped to optimise perforation job, e.g., debris prevention.                                                                  |

[Back to TOC](#TOC)

---

## IsolatedIntervalType

_The classification of an IsolatedInterval (aka. Completion). Values are CasedHole.FullString, CaseHole.ProductionLiner
and OpenHole._

The governance mode is FIXED, [IsolatedIntervalType.1.0.0.json manifest](../../Manifests/reference-data/FIXED/IsolatedIntervalType.1.0.0.json)

| `id`                                                                        | Name                       | Description                                     |
|-----------------------------------------------------------------------------|----------------------------|-------------------------------------------------|
| partition-id:reference-data--IsolatedIntervalType:CasedHole.FullString      | CasedHole, FullString      | Completion by Cased Hole, Full String.          |-|
| partition-id:reference-data--IsolatedIntervalType:CasedHole.ProductionLiner | CasedHole, ProductionLiner | Completion by Cased Hole with Production Liner. |-|
| partition-id:reference-data--IsolatedIntervalType:OpenHole                  | OpenHole                   | Open-hole completion.                           |-|

[Back to TOC](#TOC)

---

## WellboreOpeningStateType

_Wellbore Opening State Type describes a major phase that is significant to regulators and/or business stakeholders._

The governance mode is OPEN, [WellboreOpeningStateType.1.0.0.json manifest](../../Manifests/reference-data/OPEN/WellboreOpeningStateType.1.0.0.json)

| `id`                                                           | Name    | Description                                                                                                                                                                                                                                                               |
|----------------------------------------------------------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--WellboreOpeningStateType:Closed   | Closed  | The WellboreOpening is in Closed state. Flow is disabled.                                                                                                                                                                                                                 |-|
| partition-id:reference-data--WellboreOpeningStateType:Open     | Open    | The WellboreOpening is in Open state. Flow is enabled.                                                                                                                                                                                                                    |-|
| partition-id:reference-data--WellboreOpeningStateType:Squeezed | Squeeze | The WellboreOpening undergoes careful application of pump pressure to force a treatment fluid or slurry into a planned treatment zone. In most cases, a squeeze treatment will be performed at downhole injection pressure below that of the formation fracture pressure. |-|


[Back to TOC](#TOC)

---

# Well Planning and Execution

New reference value catalogs:

## WellActivityType

_The type of well activity being conducted_

The governance mode is FIXED, [WellActivityType.1.0.0.json manifest](../../Manifests/reference-data/FIXED/WellActivityType.1.0.0.json).

| `id`                                                                  | Name                    | Description                                                                                                                                                                                                                                                                                                                                                      |
|-----------------------------------------------------------------------|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--WellActivityType:completion              | Completion              | Completion is a series of processes performed on a well in order to prepare it for production. Can include conditioning the hole, running and cementing of the production casing and/or tieback of liners, running the completions string, perforation and installing the completions equipment, and testing the wellhead.                                       |-|
| partition-id:reference-data--WellActivityType:construction            | Construction            | Includes all activities that are not considered wellbore operations (e.g., overdriving a caisson, spud can inspection, BOP verification, rig commissioning, rig modifications, rig stacks, subsea installations, manifold and jumper installations, pad construction etc).                                                                                       |-|
| partition-id:reference-data--WellActivityType:drilling_and_completion | Drilling and Completion | Drilling and completing are often perfomed as one continuous activity. This activity covers the drilling of wellbore(s) and completing the same wellbore(s)                                                                                                                                                                                                      |-|
| partition-id:reference-data--WellActivityType:drilling                | Drilling                | Drilling can be perfomed for the purpose of extracting a natural resource such as  water, brine, condensate, natural gas or petroleum, for the injection of a fluid from surface to a subsurface reservoir or for subsurface formation evaluation or monitoring.                                                                                                 |-|
| partition-id:reference-data--WellActivityType:mobilization            | Mobilization            | Covers time spent moving the rig to a new well location or within operational proximity (e.g., safe zone). Includes subsequent demobilisation activities and tasks associated with securing and rigging up the drilling rig to the point it is ready to begin drilling operations.                                                                               |-|
| partition-id:reference-data--WellActivityType:plug_and_abandon        | Plug & Abandon          | Permanent abandonment of a well typically involves setting and testing cement plugs across distinct permeable zones (e.g., open hydrocarbon bearing formations, casing shoes, freshwater aquifers, and other areas near the surface of the wellbore).                                                                                                            |-|
| partition-id:reference-data--WellActivityType:re-entry                | Re-Entry                | The planned sidetrack or deepening of any original wellbore for the purpose of drilling to a different geologic bottom-hole location.                                                                                                                                                                                                                            |-|
| partition-id:reference-data--WellActivityType:reclamation             | Reclamation             | After a well is abandoned, the land around it must be returned to its original state.                                                                                                                                                                                                                                                                            |-|
| partition-id:reference-data--WellActivityType:recompletion            | Recompletion            | Used for work performed to change the producing horizon or addition of a new producing horizon without any planned drilling.                                                                                                                                                                                                                                     |-|
| partition-id:reference-data--WellActivityType:temp_abandonment        | Temporary Abandonment   | Includes all activities involved in the isolation of the completed interval or intervals within a wellbore from the surface by means of a cement retainer, cast iron bridge plug, cement plug, tubing, and packer with tubing plug, or any combination.                                                                                                          |-|
| partition-id:reference-data--WellActivityType:well_work               | Well work               | A well intervention, or well work, includes activities that do not break the tubing hanger seal and do not result in a change in the downhole hardware configuration but alters the state of the well or well geometry, provides well diagnostics, or manages the production of the well (e.g., re-perforate, add perforations to existing horizon, stimulation) |-|
| partition-id:reference-data--WellActivityType:workover                | Workover                | A workover is an operation that is carried out to an existing well involving activities that involve breaking the tubing hanger or wellhead seal and/or result in a change of the downhole hardware configuration                                                                                                                                                |-|

[Back to TOC](#TOC)

---

## WellheadConnection

_Current wellhead connection_

The governance model is OPEN, [WellheadConnection.1.0.0.json manifest](../../Manifests/reference-data/OPEN/WellheadConnection.1.0.0.json).


| `id`                                                         | Name         | Description  |
|--------------------------------------------------------------|--------------|--------------|
| partition-id:reference-data--WellheadConnection:2g_acme      | 2G Acme      | 2G Acme      |-|
| partition-id:reference-data--WellheadConnection:4g_acme      | 4G Acme      | 4G Acme      |-|
| partition-id:reference-data--WellheadConnection:4g_stub_acme | 4G Stub Acme | 4G Stub Acme |-|
| partition-id:reference-data--WellheadConnection:blind_flange | Blind Flange | Blind Flange |-|
| partition-id:reference-data--WellheadConnection:h4           | H4           | H4           |-|

[Back to TOC](#TOC)

---

## WellTechnologyApplied

_Well technology benchmarking categories, e.g. Coil tubing, Hammer drilling, etc._

The governance model is OPEN, [WellTechnologyApplied.1.0.0.json manifest](../../Manifests/reference-data/OPEN/WellTechnologyApplied.1.0.0.json).

| `id`                                                                               | Name                             | Description                      |
|------------------------------------------------------------------------------------|----------------------------------|----------------------------------|
| partition-id:reference-data--WellTechnologyApplied:coil_tubing                     | Coil tubing                      | Coil tubing                      |-|
| partition-id:reference-data--WellTechnologyApplied:hammer_drilling                 | Hammer drilling                  | Hammer drilling                  |-|
| partition-id:reference-data--WellTechnologyApplied:non-rotary_steerable__mud_motor | Non-rotary steerable (mud motor) | Non-rotary steerable (mud motor) |-|
| partition-id:reference-data--WellTechnologyApplied:rotary_kelly_driven             | Rotary kelly driven              | Rotary kelly driven              |-|
| partition-id:reference-data--WellTechnologyApplied:rotary_steerable                | Rotary steerable                 | Rotary steerable                 |-|
| partition-id:reference-data--WellTechnologyApplied:rotary_top_drive_driven         | Rotary top drive driven          | Rotary top drive driven          |-|

[Back to TOC](#TOC)

---

[Back to Milestone Overview](../README.md)