# Community OSDU Schema Repository 

This repository keeps a copy of the Data Definitions schema definitions. The original source is located 
[here](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/).

The repository contains

# 1. Consumption Areas

## 1.1. Schemas

The folder [SchemaRegistrationResources/shared-schemas](SchemaRegistrationResources/shared-schemas)
contains JSON payloads to register the OSDU schemas with the 
[Schema service API](https://community.opengroup.org/osdu/platform/system/schema-service/-/blob/master/docs/api/schema_openapi.yaml) 
(a system service).
The same resources are also located with the Schema service 
[bootstrapping folder](https://community.opengroup.org/osdu/platform/system/schema-service/-/tree/master/deployments/shared-schemas)
in the system service. The resources are bootstrapped when the 
Schema service is re-deployed.

If the schemas are to be registered independent of the Schema service 
deployment then the script 
[DeploySharedSchemas.py](https://community.opengroup.org/osdu/platform/system/schema-service/-/blob/master/deployments/scripts/DeploySharedSchemas.py)
can help to perform this - with the respective environment settings of 
course.

For changes from milestone to milestone, please see the [Migration Guides](Guides/MigrationGuides/README.md)

## 1.2 Reference Values

Many schema properties are reference-value controlled, i.e. a
string property holds the `id` to a reference value catalog 
(all records belonging to the same `osdu:wks:reference-data--SomeCatalog:1.0.0`
`kind`).

The [table of contents](ReferenceValues/Manifests/README.md) 
provides an alphabetic list of catalogs under the
[ReferenceValues](ReferenceValues) folder. Reference 
value types fall in one of three categories:
1. `FIXED` - The list of reference values cannot be extended or changed, except 
   by the governing authority/authorities itself.
1. `OPEN` - An initial list of reference values is delivered by OSDU; values provided 
   by OSDU cannot be changed, but the list of reference values can be extended by the 
   operators.
1. `LOCAL` - An initial list of reference values *may* by delivered by OSDU; values can 
   be changed, the list of values can be extended or entirely replaced by the 
   operators.

The governance mode is listed in the
[table of contents](ReferenceValues/Manifests/README.md) with links 
to the manifests and original workbooks (easier to read for humans).

## 1.3 Usage Guide

1. The online Schema Usage Guide is also published with the [Table of Contents here](Guides/README.md). 
2. A good entry point to find relevant schemas definitions of terms is the [Glossary](Guides/Chapters/91-Glossary.md).
3. Important information about schema changes and migration instructions can be found in
   the [Migration Guide overview](Guides/MigrationGuides/README.md) organized by milestone.

# 2. Schema Documentation

The list of all known schema kinds  and schema fragments can be found in this 
[README](./E-R/README.md).
The entities are hyperlinked individual markdown documentations and E-R diagrams.

A list of supported formats (expressing the desire to be supported) is given in the
[work-product-component folder](E-R/work-product-component/README.md).

# 3. Example Records

Auto-generated examples are provided in the [Examples](./Examples) 
folder structure (same organization as the consumption area). Examples
use defined reference values where available or use artificial names.
Relationships use fantasy record `id`s, which match the relationship pattern.
The examples are validated against the schemas, however, due to 
the open definition (default `"additional-properties": true`) and 
sparse usage of `required` properties, the validation is very 
tolerant.

The examples populate every defined schema property. Special cases:
* `array` properties are only populated with a single element unless 
  a minimum number of elements is required.
* `oneOf` schema constructs select the second option.

The [Documentation pages](E-R/README.md) provide links to each individual
example record per type in the header section. 

# 4. Supporting Folders

## 4.1 Generated

[Generated](./Generated) contains the final schemas. Note that
all the schemas use _**relative**_ file references in `$ref` expressions,
which have to be replaced by schema id (=kind) references. 
This representation is useful as some integrated development environments
support live checking of local references and even refactoring. The 
schemas in [Generated](./Generated) cannot be registered with the
Schema service. **The resources for schema registration are provided here:**
[SchemaRegistrationResources/shared-schemas](SchemaRegistrationResources/shared-schemas).


## 4.2. Authoring
This folder is less relevant for consumers but referenced from the documentation -
these are the essential source for the script [Generated](./Generated) schemas. 
Those schema representations provide an 'uncluttered' view on the
essential contents because standard components, like the boilerplate 
system properties are suppressed.

# 5. Change Management

The changes compared to the respective DD mirror snapshots are accumulated in
the [Change Report for Schemas](./E-R/ChangeReport.md) and 
[Change Logs for Reference Values](ReferenceValues/ChangeLogs/README.md). 
