<a name="TOC"></a>**Table of Contents**

[[_TOC_]]

Changes since: 2022-03-18

* [Detailed Change Log](./ChangesAfter-2022-03-18.md) for reference values.

# LogCurveFamily

The [internal GitLab issue #115](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/reference-values/-/issues/115)
pointed out that some families were poorly or even inadequately related to `PropertyType` and `UnitQuantity`. This was a
challenged for flag-type or boolean families.

Looking up the LogCurveFamily Name (in lower case) in PropertyType is much better. In the example "Bad Hole Flag" exists
as PropertyType with name "bad hole flag", which correctly relates to non-dimensional and dimension none.

The change revised 2154 of 2198 LogCurveFamily records.

[Back to TOC](#TOC)

# CoordinateReferenceSystem:1.1.0

The schema lacked the extent description strings, which hold the names of the countries, states, areas, in which the
coordinate reference systems are fit for usage. This property has been added in version 1.1.0. Accordingly, all OSDU
reference values have been updated to include the extent description. This also includes CoordinateTransformation:1.0.0,
for which the schema contained the description property but the records were not populated. This is now done for M12

The following section describes the rules how the values are populated in special cases for 'multiple usages' and for
BoundCRSs, where the extent information from the CRS and the bound CT are to be merged/combined.

As a consequence all 2541 CoordinateReferenceSystem records are revised.

### PreferredUsage for items with multiple Usage records

In case more than one object in Usages merge the extents and concatenate name/descriptions for traceability. The
resulting bounding box includes all usage extents. The resulting extent is only for search purposes (bounding box). The
original Usages are taken as per GeoRepository but are not relevant to search.

<details><summary>Example for a PreferredUsage based on multiple objects in the Usage array <code>BoundProjected:EPSG::3106_EPSG::15779</code>.</summary>

```json
{
  "PreferredUsage": {
    "Extent": {
      "BoundingBoxEastBoundLongitude": 92.67,
      "BoundingBoxNorthBoundLatitude": 26.64,
      "BoundingBoxSouthBoundLatitude": 18.56,
      "BoundingBoxWestBoundLongitude": 88.01,
      "Description": "Multiple usages/extents merged [(Bangladesh - onshore. [3217]), (Bangladesh - onshore and offshore. [1041])]",
      "Name": "Multiple usages/extents merged [(Bangladesh - onshore [3217]), (Bangladesh [1041])]"
    },
    "Name": "Multiple usages/extents merged [(Bangladesh - onshore [3217]), (Bangladesh [1041])] (from coordinate reference system (same extent as transformation))",
    "Scope": {
      "Name": "Multiple usages/extents merged [(Engineering survey, topographic mapping. [1142]), (Oil and gas exploration and production. [1136])]"
    }
  }
}
```

</details>

### Population rules for BoundCRS PreferredUsage:

**_Case 1:_** If the extent bbox of the CRS is fully contained in the extent.bbox of the CT then use the CRS.bbox and
its extent.description. The preferred usage name contains `(from bound transformation)`.

<details><summary>Example for a BoundCRS which has a CRS box contained by the CT's bbox <code>BoundGeographic2D:EPSG::4202_EPSG::1108</code>.</summary>

```json
{
  "PreferredUsage": {
    "Extent": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 2575
      },
      "BoundingBoxEastBoundLongitude": 153.69,
      "BoundingBoxNorthBoundLatitude": -9.86,
      "BoundingBoxSouthBoundLatitude": -43.7,
      "BoundingBoxWestBoundLongitude": 112.85,
      "Description": "Australia - Australian Capital Territory; New South Wales; Northern Territory; Queensland; South Australia; Tasmania; Western Australia; Victoria.",
      "Name": "Australia - onshore"
    },
    "Name": "Australia - onshore (from bound transformation)",
    "Scope": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 1027
      },
      "Name": "Geodesy."
    }
  }
}
```

</details>

**_Case 2:_** If the extent.bbox of the CT is fully contained in the extent.bbox of the CRS then use the CT.bbox and its
extent.description. The preferred usage name contains `(from coordinate reference system)`.

<details><summary> Example for a BoundCRS which has a CT box contained by the CRS's bbox <code>BoundGeographic2D:EPSG::4209_EPSG::1113</code>.</summary>

```json
{
  "PreferredUsage": {
    "Extent": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 1276
      },
      "BoundingBoxEastBoundLongitude": 35.93,
      "BoundingBoxNorthBoundLatitude": -8.19,
      "BoundingBoxSouthBoundLatitude": -26.88,
      "BoundingBoxWestBoundLongitude": 19.99,
      "Description": "Botswana; Malawi; Zambia; Zimbabwe.",
      "Name": "Africa - Botswana, Malawi, Zambia, Zimbabwe"
    },
    "Name": "Africa - Botswana, Malawi, Zambia, Zimbabwe (from coordinate reference system)",
    "Scope": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 1027
      },
      "Name": "Geodesy."
    }
  }
}
```

</details>

**_Case 3:_** If they are identical, don't repeat yourself, don't concatenate. The preferred usage name
contains `(from coordinate reference system (same extent as transformation))`.

<details><summary> Example for a BoundCRS for which the  CRS and CT have the same <code>BoundGeographic2D:EPSG::4213_EPSG::15849</code>.</summary>

```json
{
  "PreferredUsage": {
    "Extent": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 2771
      },
      "BoundingBoxEastBoundLongitude": 14.9,
      "BoundingBoxNorthBoundLatitude": 16.7,
      "BoundingBoxSouthBoundLatitude": 12.8,
      "BoundingBoxWestBoundLongitude": 7.81,
      "Description": "Niger - southeast",
      "Name": "Niger - southeast"
    },
    "Name": "Niger - southeast (from coordinate reference system (same extent as transformation))",
    "Scope": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 1027
      },
      "Name": "Geodesy."
    }
  }
}
```

</details>

**_Case 4:_** BoundCRSs with overlapping extents of CRS and CT use bbox intersections; in this case concatenate names
and descriptions, however in a way that the related sources become traceable. The extent AuthorityCode is dropped.

<details><summary> Example for a BoundCRS where the CRS and CT extents overlap, <code>BoundProjected:EPSG::23031_EPSG::1311</code>.</summary>

````json
{
  "PreferredUsage": {
    "Extent": {
      "BoundingBoxEastBoundLongitude": 6.01,
      "BoundingBoxNorthBoundLatitude": 63.89,
      "BoundingBoxSouthBoundLatitude": 47.42,
      "BoundingBoxWestBoundLongitude": 0.0,
      "Description": "Description intersection between CRS and CT 'Europe - between 0\u00b0E and 6\u00b0E - Andorra; Denmark (North Sea); Germany offshore; Netherlands offshore; Norway including Svalbard - onshore and offshore; Spain - onshore (mainland and Balearic Islands); United Kingdom (UKCS) offshore.' (CRS) and 'Denmark - offshore North Sea; Ireland - offshore; Netherlands - offshore; United Kingdom - UKCS offshore.' (CT) [1634,2342]",
      "Name": "Extent intersection between CRS and CT 'Europe - 0\u00b0E to 6\u00b0E and ED50 by country' (CRS) and 'Europe - common offshore' (CT) [1634,2342]"
    },
    "Name": "Usage intersection between CRS and CT 'Europe - 0\u00b0E to 6\u00b0E and ED50 by country' (CRS) and 'Europe - common offshore' (CT) [6394,8232]",
    "Scope": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 1142
      },
      "Name": "Engineering survey, topographic mapping."
    }
  }
}
````

</details>

**_Case 5:_** BoundCRSs with non-overlapping extents contributed by CRS and CT - in this case take the CRS extent, but
note the fact in the PreferredUsage name: `(from coordinate reference system, no overlap with transformation)`.

<details><summary> Example for a BoundCRS where the CRS and CT do not overlap <code>BoundGeographic2D:EPSG::4201_EPSG::1104</code></summary>

````json
{
  "PreferredUsage": {
    "Extent": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 1271
      },
      "BoundingBoxEastBoundLongitude": 47.99,
      "BoundingBoxNorthBoundLatitude": 22.24,
      "BoundingBoxSouthBoundLatitude": 3.4,
      "BoundingBoxWestBoundLongitude": 21.82,
      "Description": "Eritrea; Ethiopia; South Sudan; Sudan.",
      "Name": "Africa - Eritrea, Ethiopia, South Sudan and Sudan"
    },
    "Name": "Africa - Eritrea, Ethiopia, South Sudan and Sudan (from coordinate reference system, no overlap with transformation)",
    "Scope": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 1027
      },
      "Name": "Geodesy."
    }
  }
}
````

</details>

[Back to TOC](#TOC)

### Support Spatial Discovery

The CoordinateReferenceSystem schema has been further extended with a `data.Wgs84Coordinates` property containing
GeoJSON FeatureCollections of type Polygon for normal bounding boxes and MultiPolygon for extents crossing the
anti-meridian.

![AntiMeridian.png](Illustrations/AntiMeridian.png)

<details><summary>Example for a BoundCRS with its bounding box crossing the anti-meridian <code>BoundProjected:EPSG::3851_EPSG::1565</code></summary>

```json
{
  "PreferredUsage": {
    "Extent": {
      "AuthorityCode": {
        "Authority": "EPSG",
        "Code": 3593
      },
      "BoundingBoxEastBoundLongitude": -171.2,
      "BoundingBoxNorthBoundLatitude": -25.88,
      "BoundingBoxSouthBoundLatitude": -55.95,
      "BoundingBoxWestBoundLongitude": 160.6,
      "Description": "New Zealand - offshore.",
      "Name": "New Zealand - offshore"
    }
  },
  "Wgs84Coordinates": {
    "type": "FeatureCollection",
    "features": [
      {
        "type": "Feature",
        "properties": {},
        "geometry": {
          "type": "MultiPolygon",
          "coordinates": [
            [
              [
                [
                  160.6,
                  -55.95
                ],
                [
                  180.0,
                  -55.95
                ],
                [
                  180.0,
                  -25.88
                ],
                [
                  160.6,
                  -25.88
                ],
                [
                  160.6,
                  -55.95
                ]
              ],
              [
                [
                  -180.0,
                  -55.95
                ],
                [
                  -171.2,
                  -55.95
                ],
                [
                  -171.2,
                  -25.88
                ],
                [
                  -180.0,
                  -25.88
                ],
                [
                  -180.0,
                  -55.95
                ]
              ]
            ]
          ]
        }
      }
    ]
  }
}
```

</details>

[Back to TOC](#TOC)

# CoordinateTransformation.1.1.0

From M12 on CoordinateTransformation

1. has the extent description strings populated, and
2. TransformationType populated.

The schema is also extended with a `data.Wgs84Coordinates` property containing GeoJSON FeatureCollections of type
Polygon for normal bounding boxes and MultiPolygon for extents crossing the anti-meridian. This enables spatial
discovery via search.

All 353 CoordinateTransformation records are revised.

[Back to TOC](#TOC)

# IndexableElement

One new value was added: `lines`. It supports counting the number of lines in a representation of type
`reference-data--RepresentationType:Polyline`.

[Back to TOC](#TOC)

# PPDM Curated and Governed Lists

* **AgreementType**  — reference values are now sourced from PPDM. Some codes were deprecated and standard values were 
  added. Details can be found in 
  [ChangesAfter-2022-03-28](./ChangesAfter-2022-03-18.md#osduwksreference-data-agreementtype100).
* **ArtefactRoleType** — reference values are now sourced from PPDM.
* **BasinType** — reference values are now sourced from PPDM.
* **CalculationMethodType** — reference values are now sourced from PPDM. Many new values have been added, some 
  deprecated. For more details, see 
  [ChangesAfter-2022-03-18](./ChangesAfter-2022-03-18.md#osduwksreference-data-calculationmethodtype100).
* The **MaterialType** catalog is a conglomerate of materials, which are never all applicable to specific uses as for
  example in Wellbore and TubularComponent. Therefore, MaterialType is deprecated altogether; all reference values got
  their `data.InactiveIndicator` property set to True (=deprecated). Instead, specific new material types are
  introduced.
* **ObligationType** — reference values are now sourced from PPDM. Value definitions/descriptions have been updated.
* **OrganisationType**  — reference values are now sourced from PPDM. Details can be found in [ChangesAfter-2022-03-18](./ChangesAfter-2022-03-18.md#osduwksreference-data-organisationtype100).
* **PlayType** — reference values are now sourced from PPDM. Details can be found in [ChangesAfter-2022-03-18](./ChangesAfter-2022-03-18.md#osduwksreference-data-playtype100).
* **ProcessingParameterType** — reference values are now sourced from PPDM.
* **ProspectType** — reference values are now sourced from PPDM. Some codes were deprecated and standard values were 
  added. Details can be found in 
  [ChangesAfter-2022-03-18](./ChangesAfter-2022-03-18.md#osduwksreference-data-prospecttype100).
* **QualitativeSpatialAccuracyType** — reference values are now sourced from PPDM. 
* **TubularMaterialType**: The general or specific description of the material used to make tubular goods (casing, pipe,
  etc.)
* **VerticalMeasurementPath** — reference values are now sourced from PPDM. Value definitions/descriptions have been 
  updated.
* **WellProductType**: Product Type [Well Product Type] is the physical product(s) that can be attributed to any well
  component.

[Back to TOC](#TOC)
# WellInterestType

The reference values are now sourced from PPDM, [details here](../M11/ChangesAfter-2021-11-16.md#osduwksreference-data-wellinteresttype100)(../M11/ChangesAfter-2021-11-16.md#osduwksreference-data-wellinteresttype100).

[Back to TOC](#TOC)

# Governance Mode Changes

## FacilityStateType LOCAL &rarr; OPEN

Changed from LOCAL to OPEN, Values are sourced from PPDM. All 5 values continue to be deployed, however, the
descriptions have been updated. Operators should validate whether any of the new values collide with existing extensions
or overlap with the new values.

## PlayType LOCAL &rarr; OPEN

Changed from LOCAL to OPEN, Values are sourced from PPDM. New facets have been introduced. Operators should validate
whether any of the new values collide with existing extensions or overlap with the new values. One
value, `CarbonCaptureStorage`, has been deprecated in favour of `CarbonCaptureAndStorage`.

## WellboreTrajectoryType LOCAL &rarr; OPEN

Changed from LOCAL to OPEN, Values are sourced from PPDM. Previously, only three values Directional, Horizontal and
Vertical were defined and deployed by OSDU. PPDM delivers 10 new values with much greater definition. Operators should
validate whether any of the new values collide with existing extensions or overlap with the new values.

One value, `Directional` has been marked as deprecated and should be replaced by the synonym `Inclined`.

[Back to TOC](#TOC)

# DataRuleDimensionType

A set of data quality rules to assess a specific aspect in the trustworthiness of data ([source: PPDM Association](https://og.enterprise.slack.com/files/U027UFFRUUA/F038SGPGDRU/quality_data_rule_set_ppdm_20220328.xlsx)).
Also known as Data Quality Dimensions. This new reference value type is about the quality dimensions in which a dataset may be assessed, e.g. completeness. A data item usually requires assessment by several sets of rules before it can be trusted. This is not a list of the actual quality rules. 

|Type|Governance Mode|Workbook|Manifest|
|---|---|---|---|
|DataRuleDimensionType:1.0.0|OPEN|&rarr; [DataRuleDimensionType.1.0.0.xlsx](../../Published/DataRuleDimensionType.1.0.0.xlsx)|&rarr; [DataRuleDimensionType.1.0.0.json](../../Manifests/reference-data/OPEN/DataRuleDimensionType.1.0.0.json) |

[Back to TOC](#TOC)

# Carbon Capture and Storage

This work-stream introduced the following new reference value lists:

2. [`osdu:wks:reference-data--InSARApplication:1.0.0`](../../Manifests/reference-data/LOCAL/InSARApplication.1.0.0.json)<br>(LOCAL governance)
3. [`osdu:wks:reference-data--InSARFrequencyBand:1.0.0`](../../Manifests/reference-data/FIXED/InSARFrequencyBand.1.0.0.json)<br>(FIXED governance)
4. [`osdu:wks:reference-data--InSARImageMode:1.0.0`](../../Manifests/reference-data/LOCAL/InSARImageMode.1.0.0.json)<br>(LOCAL governance)
5. [`osdu:wks:reference-data--InSARPolarisation:1.0.0`](../../Manifests/reference-data/FIXED/InSARPolarisation.1.0.0.json)<br>(FIXED governance)
6. [`osdu:wks:reference-data--InSARProcessingType:1.0.0`](../../Manifests/reference-data/OPEN/InSARProcessingType.1.0.0.json)<br>(OPEN governance)
7. [`osdu:wks:reference-data--SatelliteGeometry:1.0.0`](../../Manifests/reference-data/OPEN/SatelliteGeometry.1.0.0.json)<br>(OPEN governance)
8. [`osdu:wks:reference-data--SatelliteMission:1.0.0`](../../Manifests/reference-data/LOCAL/SatelliteMission.1.0.0.json)<br>(LOCAL governance)

[Back to TOC](#TOC)

# Coring, RockSample, RockSampleAnalysis

The Petrophysics work-stream contributed a number of new reference value lists:

1. [`osdu:wks:reference-data--ConventionalCoreType:1.0.0`](../../Manifests/reference-data/FIXED/ConventionalCoreType.1.0.1.json) _A
   reference value type for Conventional Cores._<br>(FIXED governance)
2. [`osdu:wks:reference-data--CorePreservationType:1.0.0`](../../Manifests/reference-data/OPEN/CorePreservationType.1.0.0.json) _The
   core preservation classification type, such as Wax-sealed, Resin-coated, Cling-wrap, etc._<br>(OPEN governance)
3. [`osdu:wks:reference-data--GrainDensityMeasurementType:1.0.0`](../../Manifests/reference-data/OPEN/GrainDensityMeasurementType.1.0.0.json) 
   _The kind of grain density measurement, which is applied in, e.g. routine core analysis._<br>(OPEN governance)
4. [`osdu:wks:reference-data--PermeabilityMeasurementType:1.0.0`](../../Manifests/reference-data/OPEN/PermeabilityMeasurementType.1.0.0.json) 
   _The kind of permeability measurement applied in, e.g., routine core analysis._<br>(OPEN governance)
5. [`osdu:wks:reference-data--PorosityMeasurementType:1.0.0`](../../Manifests/reference-data/OPEN/PorosityMeasurementType.1.0.0.json)
   _The kind of porosity measurement applied in, e.g., routine core analysis._<br>(OPEN governance)
6. [`osdu:wks:reference-data--PressureMeasurementType:1.0.0`](../../Manifests/reference-data/OPEN/PressureMeasurementType.1.0.0.json) _
   The enumeration for PressureMeasurement types like Ambient or Overburden._<br>(OPEN governance)
7. [`osdu:wks:reference-data--RockSampleType:1.0.0`](../../Manifests/reference-data/OPEN/RockSampleType.1.0.0.json) _The classification
   of rock samples._<br>(OPEN governance)
8. [`osdu:wks:reference-data--SampleOrientationType:1.0.0`](../../Manifests/reference-data/FIXED/SampleOrientationType.1.0.0.json) _The
   sample orientation direction, which describes the core sample's relationship to the bedding or drilling
   direction._<br>(FIXED governance)
9. [`osdu:wks:reference-data--SaturationMethodType:1.0.0`](../../Manifests/reference-data/OPEN/SaturationMethodType.1.0.0.json) _The
   method used to obtain the saturation measurement values, e.g., Dean Stark, Retort, Karl Fischer._<br>(OPEN governance)
10. [`osdu:wks:reference-data--SidewallCoreType:1.0.0`](../../Manifests/reference-data/FIXED/SidewallCoreType.1.0.0.json) _A reference
    value type for Sidewall Cores._ <br>(FIXED governance)

[Back to TOC](#TOC)

# Technical Assurance

The revision of technical assurance in M12 provides richer context for acceptable and unacceptable usage. The following  

1. [`osdu:wks:reference-data--WorkflowPersonaType:1.0.0`](../../Manifests/reference-data/LOCAL/WorkflowPersonaType.1.0.0.json) 
   _Name of the role or personas that the record is technical assurance value is valid or not valid for._<br>(Currently LOCAL, in M13 or later OPEN)
1. [`osdu:wks:reference-data--WorkflowUsageType:1.0.0`](../../Manifests/reference-data/LOCAL/WorkflowUsageType.1.0.0.json) 
   _Describes the workflows that the technical assurance value is valid or not valid for._<br>(Currently LOCAL, in M13 or later OPEN)

[Back to TOC](#TOC)

---

[Back to Milestone Overview](../README.md)