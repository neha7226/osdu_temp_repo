# Usage of `work-product-component--ColumnBasedTable:1.0.0`

A WPC based on Energistics EML2.3 ColumnBasedTable which intends to store (not so big) column based table where each column is defined by a property type and optionally facets.
Main use cases are:
- KrPc table
- Facies table
- PVT table

# Facies Table
			
Index | Facies 
---|---
1 | sandstone
2 | clay
3 | limestone

```json
    "KeyColumns": [
      {
        "ValueType": "integer",
        "ValueCount": 1,
        "UnitQuantityID": "namespace:reference-data--UnitQuantity:...:",
        "PropertyType": {
          "PropertyTypeID": "namespace:reference-data--PropertyType:323df361-784e-4062-a346-ff4ba80a78f0:",
          "Name": "index"
        },
        "RelationshipTargetKind": "osdu:wks:reference-data--UnitOfMeasure:",
      }
    ],
    "Columns": [
      {
        "ValueType": "string",
        "ValueCount": 1,
        "UnitQuantityID": "namespace:reference-data--UnitQuantity:...:",
        "PropertyType": {
          "PropertyTypeID": "namespace:reference-data--PropertyType:838b1a9b-2fca-4e2e-aae3-323ef1bc59c7:",
          "Name": "facies"
        },
        "RelationshipTargetKind": "osdu:wks:reference-data--UnitOfMeasure:",
      }
    ],
    "ColumnSize": 3,
    "ColumnValues": [
      {
        "IntegerColumn": [
          1,
          2,
          3
        ]
      },
      {
        "StringColumn": [
          "sandstone",
          "clay",
         "limestone"
        ]
      }
    ],
```

# KrPc : Water-Oil Table

Water Saturation | Water Relative Permeability | Oil Relative Permeability | Oil-Water Capillary Pressure
---|---|---|---
0 | 0	| 1 | 0
0.157 | 0 | 0.99999| 0
0.173 | 0.000356 | 0.886131 | 0
0.174325 | 0.0004392 | 0.8767012 | 0
0.19 | 0.0014241 | 0.7764308 | 0

```json
    "KeyColumns": [
      {
        "ValueType": "number",
        "ValueCount": 1,
        "UnitQuantityID": "namespace:reference-data--UnitQuantity:...:",
        "PropertyType": {
          "PropertyTypeID": "namespace:reference-data--PropertyType:cfe9293f-d5a9-486d-815a-a957cace90b6:",
          "Name": "saturation"
        },
        "RelationshipTargetKind": "osdu:wks:reference-data--UnitOfMeasure:",
        "FacetIDs": [
          {
            "FacetTypeID": "namespace:reference-data--FacetType:what:",
            "FacetRoleID": "namespace:reference-data--FacetRole:water:"
          }
        ]
      }
    ],
    "Columns": [
      {
        "ValueType": "number",
        "ValueCount": 1,
        "UnitQuantityID": "namespace:reference-data--UnitQuantity:...:",
        "PropertyType": {
          "PropertyTypeID": "namespace:reference-data--PropertyType:8e3c5579-7efd-40d0-ab03-bc79452dd2db:",
          "Name": "relative permeability"
        },
        "RelationshipTargetKind": "osdu:wks:reference-data--UnitOfMeasure:",
        "FacetIDs": [
          {
            "FacetTypeID": "namespace:reference-data--FacetType:what:",
            "FacetRoleID": "namespace:reference-data--FacetRole:water:"
          }
        ]
      },
      {
        "ValueType": "number",
        "ValueCount": 1,
        "UnitQuantityID": "namespace:reference-data--UnitQuantity:...:",
        "PropertyType": {
          "PropertyTypeID": "namespace:reference-data--PropertyType:8e3c5579-7efd-40d0-ab03-bc79452dd2db:",
          "Name": "relative permeability"
        },
        "RelationshipTargetKind": "osdu:wks:reference-data--UnitOfMeasure:",
        "FacetIDs": [
          {
            "FacetTypeID": "namespace:reference-data--FacetType:what:",
            "FacetRoleID": "namespace:reference-data--FacetRole:oil:"
          }
        ]
      },
      {
        "ValueType": "number",
        "ValueCount": 1,
        "UnitQuantityID": "namespace:reference-data--UnitQuantity:...:",
        "PropertyType": {
          "PropertyTypeID": "namespace:reference-data--PropertyType:a816a113-1544-4f58-bc6d-7c030b65627b:",
          "Name": "capillary pressure"
        },
        "RelationshipTargetKind": "osdu:wks:reference-data--UnitOfMeasure:",
        "FacetIDs": [
          {
            "FacetTypeID": "namespace:reference-data--FacetType:what:",
            "FacetRoleID": "namespace:reference-data--FacetRole:oil:"
          },
          {
            "FacetTypeID": "namespace:reference-data--FacetType:what:",
            "FacetRoleID": "namespace:reference-data--FacetRole:water:"
          }
        ]
      }
    ],
    "ColumnSize": 5,
    "ColumnValues": [
      {
        "NumberColumn": [
          0,
          0.157,
          0.173,
          0.174325,
          0.19,
        ]
      },
      {
        "NumberColumn": [
          0,
          0,
          0.000356,
          0.0004392,
          0.0014241,
        ]
      },
      {
        "NumberColumn": [
          1,
          0.99999,
          0.886131,
          0.8767012,
          0.7764308
        ]
      },
      {
        "NumberColumn": [
           0,
           0,
           0,
           0,
           0,
        ]
      }
    ],
```