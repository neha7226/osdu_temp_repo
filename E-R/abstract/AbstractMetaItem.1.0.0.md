<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# Frame of Reference Meta Data Item
A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractMetaItem:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; [Authoring Schema AbstractMetaItem.1.0.0.json](../../Authoring/abstract/AbstractMetaItem.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractMetaItem.1.0.0.json](../../Generated/abstract/AbstractMetaItem.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractMetaItem.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractMetaItem.1.0.0.json)
* Link to worked examples in context &rarr; [Topic: Frame Of Reference](../../Examples/WorkedExamples/FrameOfReference/README.md)
* Link to worked examples in context &rarr; [Topic: Summary of Geomatics in OSDU Schemas](../../Examples/WorkedExamples/Geomatics/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams Frame of Reference Meta Data Item


## Outgoing Relationships for Frame of Reference Meta Data Item

![Frame of Reference Meta Data Item](../_diagrams/abstract/AbstractMetaItem.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Frame of Reference Meta Data Item Referenced by other Entities

![Frame of Reference Meta Data Item](../_diagrams/abstract/AbstractMetaItem.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Frame of Reference Meta Data Item Properties

## 1. Table of Frame of Reference Meta Data Item Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of Frame of Reference Meta Data Item Data Properties, Section FrameOfReferenceUOM

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|kind|oneOf[0]|const string|required|UOM Reference Kind|The kind of reference, 'Unit' for FrameOfReferenceUOM.|<code>Unit</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Unit|
|name|oneOf[0]|string|optional|UOM Unit Symbol|The unit symbol or name of the unit.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|ft[US]|
|persistableReference|oneOf[0]|string|required|UOM Persistable Reference|The self-contained, persistable reference string uniquely identifying the Unit.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"abcd":{"a":0.0,"b":1200.0,"c":3937.0,"d":0.0},"symbol":"ft[US]","baseMeasurement":{"ancestry":"L","type":"UM"},"type":"UAD"}|
|unitOfMeasureID &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|oneOf[0]|string|optional|Unit Of Measure Id|SRN to unit of measure reference.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--UnitOfMeasure:ftUS:|
|propertyNames[]|oneOf[0]|string|optional|UOM Property Names|The list of property names, to which this meta data item provides Unit context to. A full path like "StructureA.PropertyB" is required to define a unique context; "data" is omitted since frame-of reference normalization only applies to the data block.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|["HorizontalDeflection.EastWest", "HorizontalDeflection.NorthSouth"]|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of Frame of Reference Meta Data Item Data Properties, Section FrameOfReferenceCRS

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|kind|oneOf[1]|const string|required|CRS Reference Kind|The kind of reference, constant 'CRS' for FrameOfReferenceCRS.|<code>CRS</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|CRS|
|name|oneOf[1]|string|optional|CRS Name|The name of the CRS.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|WGS 84 / UTM zone 15N|
|persistableReference|oneOf[1]|string|required|CRS Persistable Reference|The self-contained, persistable reference string uniquely identifying the CRS.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"authCode":{"auth":"EPSG","code":"32615"},"name":"WGS_1984_UTM_Zone_15N","type":"LBC","ver":"PE_10_9_1","wkt":"PROJCS[\"WGS_1984_UTM_Zone_15N\",GEOGCS[\"GCS_WGS_1984\",DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137.0,298.257223563]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",500000.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",-93.0],PARAMETER[\"Scale_Factor\",0.9996],PARAMETER[\"Latitude_Of_Origin\",0.0],UNIT[\"Meter\",1.0],AUTHORITY[\"EPSG\",32615]]"}|
|coordinateReferenceSystemID &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|oneOf[1]|string|optional|Coordinate Reference System Id|SRN to CRS reference.|<code>^[\w\-\.]+:reference-data\-\-CoordinateReferenceSystem:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--CoordinateReferenceSystem:Projected:EPSG::32615:|
|propertyNames[]|oneOf[1]|string|optional|CRS Property Names|The list of property names, to which this meta data item provides CRS context to. A full path like "StructureA.PropertyB" is required to define a unique context; "data" is omitted since frame-of reference normalization only applies to the data block.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|["KickOffPosition.X", "KickOffPosition.Y"]|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of Frame of Reference Meta Data Item Data Properties, Section FrameOfReferenceDateTime

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|kind|oneOf[2]|const string|required|DateTime Reference Kind|The kind of reference, constant 'DateTime', for FrameOfReferenceDateTime.|<code>DateTime</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|DateTime|
|name|oneOf[2]|string|optional|DateTime Name|The name of the DateTime format and reference.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|UTC|
|persistableReference|oneOf[2]|string|required|DateTime Persistable Reference|The self-contained, persistable reference string uniquely identifying DateTime reference.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"format":"yyyy-MM-ddTHH:mm:ssZ","timeZone":"UTC","type":"DTM"}|
|propertyNames[]|oneOf[2]|string|optional|DateTime Property Names|The list of property names, to which this meta data item provides DateTime context to. A full path like "StructureA.PropertyB" is required to define a unique context; "data" is omitted since frame-of reference normalization only applies to the data block.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|["Acquisition.StartTime", "Acquisition.EndTime"]|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of Frame of Reference Meta Data Item Data Properties, Section FrameOfReferenceAzimuthReference

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|kind|oneOf[3]|const string|required|AzimuthReference Reference Kind|The kind of reference, constant 'AzimuthReference', for FrameOfReferenceAzimuthReference.|<code>AzimuthReference</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|AzimuthReference|
|name|oneOf[3]|string|optional|AzimuthReference Name|The name of the CRS or the symbol/name of the unit.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|TrueNorth|
|persistableReference|oneOf[3]|string|required|AzimuthReference Persistable Reference|The self-contained, persistable reference string uniquely identifying AzimuthReference.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"code":"TrueNorth","type":"AZR"}|
|propertyNames[]|oneOf[3]|string|optional|AzimuthReference Property Names|The list of property names, to which this meta data item provides AzimuthReference context to. A full path like "StructureA.PropertyB" is required to define a unique context; "data" is omitted since frame-of reference normalization only applies to the data block.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|["Bearing"]|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractMetaItem.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractSystemProperties.1.0.0](AbstractSystemProperties.1.0.0.md) | meta[] | many (as array) |  |
| Included by | [GenericDataset.1.0.0](../manifest/GenericDataset.1.0.0.md) | meta[] | many (as array) |  |
| Included by | [GenericMasterData.1.0.0](../manifest/GenericMasterData.1.0.0.md) | meta[] | many (as array) |  |
| Included by | [GenericReferenceData.1.0.0](../manifest/GenericReferenceData.1.0.0.md) | meta[] | many (as array) |  |
| Included by | [GenericWorkProduct.1.0.0](../manifest/GenericWorkProduct.1.0.0.md) | meta[] | many (as array) |  |
| Included by | [GenericWorkProductComponent.1.0.0](../manifest/GenericWorkProductComponent.1.0.0.md) | meta[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._