<a name="TOC"></a>

[[_TOC_]]

# Migration to `osdu:wks:reference-data--VerticalMeasurementType:1.0.2`

PPDM revised the description, which requires another patch version increment to 
[`osdu:wks:reference-data--VerticalMeasurementType:1.0.2`](../../../E-R/reference-data/VerticalMeasurementType.1.0.2.md). No value migration is required. 

The updated content is provided as manifest — no changes to values, except record `kind` referring to version 1.0.2. 

[Back to TOC](#TOC)

---

# Links

* [Back to Milestone](README.md)
* [Back to Migration Guides](../README.md)
* [Back to Guides](../../README.md)
