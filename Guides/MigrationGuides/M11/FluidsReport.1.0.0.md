[[_TOC_]]

# Migration to `osdu:wks:master-data--FluidsReport:1.1.0`

Migrating  [`osdu:wks:master-data--FluidsReport:1.0.0`](../../../E-R/master-data/FluidsReport.1.0.0.md)
to [`osdu:wks:master-data--FluidsReport:1.1.0`](../../../E-R/master-data/FluidsReport.1.1.0.md)
means adding new optional property only. No migration effort expected.

## New Property

| Property                                                                                        |Value Type|Description|Migration Instructions|
|-------------------------------------------------------------------------------------------------|----------|-----------|----------------------|
| data.Fluid[].BrandName | string | The name of the fluid as given by the supplier | This property opens integration opportunities. No migration effort required. |

---

# Links

* [Back to Milestone](README.md)
* [Back to Migration Guides](../README.md)
* [Back to Guides](../../README.md)
