<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractIjkGridFlowSimulationBoundaryConnection [Status: Accepted]
This structure intends to define the reservoir boundary connection in an IJK grid such as aquifer connection. It can be reused in various Ijk Grid flow simulation related representations which need some boundary connections.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractIjkGridFlowSimulationBoundaryConnection:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.json](../../Authoring/abstract/AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.json](../../Generated/abstract/AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractIjkGridFlowSimulationBoundaryConnection


## Outgoing Relationships for AbstractIjkGridFlowSimulationBoundaryConnection

![AbstractIjkGridFlowSimulationBoundaryConnection](../_diagrams/abstract/AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractIjkGridFlowSimulationBoundaryConnection Referenced by other Entities

![AbstractIjkGridFlowSimulationBoundaryConnection](../_diagrams/abstract/AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractIjkGridFlowSimulationBoundaryConnection Properties

## Table of AbstractIjkGridFlowSimulationBoundaryConnection Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|GridID &rarr; [IjkGridRepresentation](../work-product-component/IjkGridRepresentation.1.0.0.md)| |string|optional|Grid ID|The grid which is in connection|<code>^[\w\-\.]+:work-product-component\-\-IjkGridRepresentation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [IjkGridRepresentation](../work-product-component/IjkGridRepresentation.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:work-product-component--IjkGridRepresentation:85348741-3433-406B-9189-22B298C3E2D2:|
|LowerI| |integer|optional|Lower I index|The lower included I index of the box of the grid in connection|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|5|
|UpperI| |integer|optional|Upper I index|The upper included I index of the box of the grid in connection|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|10|
|LowerJ| |integer|optional|Lower J index|The lower included J index of the box of the grid in connection|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1|
|UpperJ| |integer|optional|Upper J index|The upper included J index of the box of the grid in connection|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1|
|LowerK| |integer|optional|Lower K index|The lower included K index of the box of the grid in connection|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1|
|UpperK| |integer|optional|Upper K index|The upper included K index of the box of the grid in connection|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|10|
|Face &rarr; [IjkCellFace](../reference-data/IjkCellFace.1.0.0.md)| |string|optional|Face|The faces of the box of the grid which are in connection|<code>^[\w\-\.]+:reference-data\-\-IjkCellFace:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [IjkCellFace](../reference-data/IjkCellFace.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--IjkCellFace:J_MINUS:|
|TransmissibilityMultiplier| |number|optional|Transmissibility multiplier|The transmissibility multiplier of the connection|(No pattern)|(No format)|UOM:dimensionless|(No indexing hint)|(Not derived)|(No natural key)|0.75|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractIjkGridFlowSimulationBoundaryConnection.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [IjkGridNumericalAquiferRepresentation.1.0.0](../work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md) | ConnectionSet[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._