<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# StandardsOrganisation [Status: Accepted]
The reference-data type holding a StandardsOrganisation, e.g. ISO, IOGP, Energistics, etc. The main purpose is to define organisations, which provide alias names to reference-data.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--StandardsOrganisation:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: OSDU
* Reference value type, governance model: OPEN
* Link to &rarr; Proposal workbook [schema-StandardsOrganisation.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/reference-data/schema-StandardsOrganisation.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema StandardsOrganisation.1.0.0.json](../../Authoring/reference-data/StandardsOrganisation.1.0.0.json)
* Link to &rarr; [Generated Schema StandardsOrganisation.1.0.0.json](../../Generated/reference-data/StandardsOrganisation.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource StandardsOrganisation.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/StandardsOrganisation.1.0.0.json)
* Link to &rarr; [Example Record StandardsOrganisation.1.0.0.json](../../Examples/reference-data/StandardsOrganisation.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams StandardsOrganisation


## Outgoing Relationships for StandardsOrganisation

![StandardsOrganisation](../_diagrams/reference-data/StandardsOrganisation.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## StandardsOrganisation Referenced by other Entities

![StandardsOrganisation](../_diagrams/reference-data/StandardsOrganisation.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# StandardsOrganisation Properties

## 1. Table of StandardsOrganisation System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-StandardsOrganisation:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--StandardsOrganisation:129c186a-48c1-5857-9baf-a6b5e2dea572|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--StandardsOrganisation:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of StandardsOrganisation Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of StandardsOrganisation Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of StandardsOrganisation Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of StandardsOrganisation Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for StandardsOrganisation.1.0.0

* Status: Published
* Governance: OPEN
* The table below shows all 30 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [StandardsOrganisation](../../ReferenceValues/Manifests/reference-data/OPEN/StandardsOrganisation.1.0.0.json).

|`id` with prefix `namespace:reference-data--StandardsOrganisation:`|Code|Name|Alias|Description|AttributionAuthority|Source|CommitDate|AttributionPublication|AttributionRevision|NameAlias|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|API|API|The American Petroleum Institute|-|The American Petroleum Institute (API) is the largest U.S. trade association for the oil and natural gas industry. Its mission is to promote safety across the industry globally and influence public policy in support of a strong, viable U.S. oil and natural gas industry.|OSDU|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|-|-|-|
|ASTM|ASTM|ASTM International|-|ASTM International, formerly known as American Society for Testing and Materials, is an international standards organization that develops and publishes voluntary consensus technical standards for a wide range of materials, products, systems, and services.|OSDU|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|-|-|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"American&nbsp;Society&nbsp;for&nbsp;Testing&nbsp;and&nbsp;Materials"<br>&nbsp;&nbsp;}<br>]|
|BakerAtlas%28formerlyDresserAtlas%29|BakerAtlas(formerlyDresserAtlas)|Baker Atlas|-|Baker Atlas (formerly Dresser Atlas). Company code used by PWLS 3.0 Logs.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|PWLS|V3.0|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"150",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics%20PWLS%203:"<br>&nbsp;&nbsp;},<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"Dresser&nbsp;Atlas",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics%20PWLS%203:"<br>&nbsp;&nbsp;}<br>]|
|BakerHughesInteq|BakerHughesInteq|Baker Hughes Inteq|-|Baker Hughes Inteq. Company code used by PWLS 3.0 Logs.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|PWLS|V3.0|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"15",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics%20PWLS%203:"<br>&nbsp;&nbsp;}<br>]|
|BP|BP|BP|-|BP. Company code used by PWLS 3.0 Logs.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|PWLS|V3.0|-|
|CWLS|CWLS|Canadian Well Logging Society|-|Canadian Well Logging Society - The oldest organization devoted to log analysis, incorporated in Calgary, Canada in 1957.|CWLS|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|https://www.cwls.org|-|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"CWLS",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:Abbreviation:"<br>&nbsp;&nbsp;}<br>]|
|Energistics%20PWLS%201|Energistics PWLS 1|Energistics PWLS 1|-|Energistics Practical Well Log Standard version 1.0.1, February 27, 2001.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|http://w3.energistics.org/PWLS/pwls_10.htm|1.0.1|-|
|Energistics%20PWLS%202|Energistics PWLS 2|Energistics PWLS 2|-|Energistics Practical Well Log Standard version 2.0, September 30, 2003.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|http://w3.energistics.org/PWLS/pwls_20.htm|2.0|-|
|Energistics%20PWLS%203|Energistics PWLS 3|Energistics PWLS 3|-|Energistics Practical Well Log Standard version 3, February 25, 2021.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|https://www.energistics.org/practical-well-log-standard-v3/|3.0|-|
|Energistics%20RESQML%20201|Energistics RESQML 201|Energistics RESQML 201|-|Energistics RESQML Standard version 2.0.1, September 11, 2015.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|https://www.energistics.org/wp-content/uploads/2017/03/RESQML_TECHNICAL_REFERENCE_Guide_v201.pdf|2.0.1|-|
|Energistics.EPSG|Energistics.EPSG|Energistics.EPSG|-|The Energistics Standard Mapping from &quot;Code - Unit of Measure&quot; values in the &quot;EPSG V8.1&quot; data base to &quot;unit/symbol&quot; values in the &quot;Energistics Unit of Measure Dictionary V1.0&quot; dictionary.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|Energistics Unit of Measure Dictionary|V1.0|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"EPSG",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics:"<br>&nbsp;&nbsp;}<br>]|
|Energistics.OpenSpirit|Energistics.OpenSpirit|Energistics.OpenSpirit|-|The Energistics Standard Mapping from  &quot;UnitMeasurement/Name&quot; and &quot;Unit/Symbol&quot; values in the &quot;Open Spirit Unit Dictionary V3.0&quot; dictionary to &quot;quantityClass/name&quot; and &quot;unit/symbol&quot; values in the &quot;Energistics Unit of Measure Dictionary V1.0&quot; dictionary.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|Energistics Unit of Measure Dictionary|V1.0|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"OpenSpirit",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics:"<br>&nbsp;&nbsp;}<br>]|
|Energistics.POSC|Energistics.POSC|Energistics.POSC|-|The Energistics Standard Mapping from &quot;UnitOfMeasure/QuantityType&quot;, &quot;QuantityClass/name&quot;, &quot;UnitOfMeasure/CatalogSymbol&quot; values in the &quot;POSC Unit Dictionary V2.2&quot; dictionary to &quot;quantityClass/name&quot;  and &quot;unit/symbol&quot; values in the &quot;Energistics Unit of Measure Dictionary V1.0&quot; dictionary.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|Energistics Unit of Measure Dictionary|V1.0|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"POSC",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics:"<br>&nbsp;&nbsp;}<br>]|
|Energistics.RP66_V1|Energistics.RP66_V1|Energistics.RP66_V1|-|The Energistics Standard Mapping from &quot;RP66 V1&quot; Appendix-F Unit Symbols to &quot;unit/symbol&quot; values in the &quot;Energistics Unit of Measure Dictionary V1.0&quot; dictionary.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|Energistics Unit of Measure Dictionary|V1.0|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"RP66_V1",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics:"<br>&nbsp;&nbsp;}<br>]|
|Energistics.RP66_V2|Energistics.RP66_V2|Energistics.RP66_V2|-|The Energistics Standard Mapping from &quot;RP66 V2&quot; Part-5 Unit Symbols to &quot;unit/symbol&quot; values in the &quot;Energistics Unit of Measure Dictionary V1.0&quot; dictionary.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|Energistics Unit of Measure Dictionary|V1.0|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"RP66_V2",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics:"<br>&nbsp;&nbsp;}<br>]|
|Energistics|Energistics|Energistics|-|Energistics an interprofessional standard-setting body composed of representatives from various Oil &amp; Gas Producers and Services companies aiming to provide standardized format and protocols for Data Exchanges|OSDU|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|-|-|-|
|EPSG|EPSG|European Petroleum Survey Group|-|EPSG was a scientific organization with ties to the European petroleum industry consisting of specialists working in applied geodesy, surveying, and cartography related to oil exploration.|OSDU|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|-|-|-|
|EQN|EQN|Equinor|-|Equinor as organisation contributing to OSDU standards.|Equinor|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA 83d41052.|2021-12-22T09:23:30+01:00|-|-|-|
|HalliburtonLogging|HalliburtonLogging|Halliburton Logging|-|Halliburton Logging. Company code used by PWLS 3.0 Logs.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|PWLS|V3.0|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"280",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics%20PWLS%203:"<br>&nbsp;&nbsp;}<br>]|
|ICS|ICS|International Commission on Stratigraphy|-|The International Commission on Stratigraphy (ICS) is the largest and oldest constituent scientific body in the International Union of Geological Sciences (IUGS). Its primary objective is to define precisely global units (systems, series and stages) of the International Chronostratigraphic Chart that, in turn, are the basis for the units (periods, epochs and age) of the International Geological Time Scale; thus setting global standards for the fundamental scale for expressing the history of the Earth.|ICS|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|https://stratigraphy.org|-|-|
|Industry|Industry|Industry|-|Used for so-called industry standard aliases.|OSDU|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|-|-|-|
|IOGP|IOGP|The International Association of Oil &amp; Gas Producers|-|The International Association of Oil &amp; Gas Producers (IOGP) is the petroleum industry&#x27;s global forum in which members identify and share best practices to achieve improvements in health, safety, the environment, security, social responsibility, engineering and operations|OSDU|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|-|-|-|
|ISCWSA|ISCWSA|Industry Steering Committee on Wellbore Survey Accuracy|-|Industry Steering Committee on Wellbore Survey Accuracy (ISCWSA) produces, maintains, and publishes standards for the industry, promoting a collaborative understanding of issues associated with wellbore surveying.|ISCWSA|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|https://www.iscwsa.net|-|-|
|ISO|ISO|International Organization for Standardization|-|The International Organization for Standardization is an international standard-setting body composed of representatives from various national standards organizations.|OSDU|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|-|-|-|
|NumarMRILogging|NumarMRILogging|Numar MRI Logging|-|Numar MRI Logging, code used by PWLS 3.0 Logs.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|PWLS|V3.0|-|
|OSDU|OSDU|OSDU|-|Used as organisation code for standard or reference values governed by the Open Subsurface Data Universe (OSDU) themselves.|OSDU|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|https://osduforum.org|R3|-|
|Pason|Pason|Pason|-|Pason. Company code used by PWLS 3.0 Logs.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|PWLS|V3.0|-|
|PPDM|PPDM|PPDM|-|The Professional Petroleum Data Management  is an interprofessional standard-setting body composed of representatives from various Oil &amp; Gas Producers and Services companies aiming to provide a unified data model, reference value lists, and standardized data management practices for Oil &amp; Gas activities|OSDU|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|-|-|-|
|Schlumberger:LIS|Schlumberger:LIS|Schlumberger:LIS|-|Schlumberger LIS Log Information Standard (1979). Company code used by PWLS 3.0 Logs.|Schlumberger|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|http://w3.energistics.org/LIS/lis-79.pdf|1986|-|
|Schlumberger|Schlumberger|Schlumberger|-|Schlumberger. Company code used by PWLS 3.0 Logs.|Energistics|Workbook Published/StandardsOrganisation.1.0.0.xlsx; commit SHA f103be06.|2021-11-22T09:20:33+01:00|PWLS|V3.0|[<br>&nbsp;&nbsp;{<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasName":&nbsp;"440",<br>&nbsp;&nbsp;&nbsp;&nbsp;"AliasNameTypeID":&nbsp;"AliasNameType:IndustryCode:",<br>&nbsp;&nbsp;&nbsp;&nbsp;"DefinitionOrganisationID":&nbsp;"StandardsOrganisation:Energistics%20PWLS%203:"<br>&nbsp;&nbsp;}<br>]|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# StandardsOrganisation.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md) | DefinitionOrganisationID | one |  |
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [LogCurveType.1.1.0](LogCurveType.1.1.0.md) | AcquisitionCompanyID | one |  |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._