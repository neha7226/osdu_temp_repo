<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractFacilityState [Status: Accepted]
The life cycle status of a facility at some point in time.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractFacilityState:1.0.0`
* Schema status: PUBLISHED
* **Superseded by** &rarr; [`osdu:wks:AbstractFacilityState:1.1.0`](AbstractFacilityState.1.1.0.md)
* Link to &rarr; Proposal workbook [AbstractFacilityState.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractFacilityState.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractFacilityState.1.0.0.json](../../Authoring/abstract/AbstractFacilityState.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractFacilityState.1.0.0.json](../../Generated/abstract/AbstractFacilityState.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractFacilityState.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractFacilityState.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractFacilityState


## Outgoing Relationships for AbstractFacilityState

![AbstractFacilityState](../_diagrams/abstract/AbstractFacilityState.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractFacilityState Referenced by other Entities

![AbstractFacilityState](../_diagrams/abstract/AbstractFacilityState.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacilityState Properties

## Table of AbstractFacilityState Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|EffectiveDateTime| |string|optional|Effective Date Time|The date and time at which the facility state becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TerminationDateTime| |string|optional|Termination Date Time|The date and time at which the facility state is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityStateTypeID &rarr; [FacilityStateType](../reference-data/FacilityStateType.1.0.0.md)| |string|optional|Facility State Type Id|Life Cycle [Facility State Type] is a set of major phases that are significant to regulators and/or business stakeholders. Life Cycle may apply to a well or its components [or other facility].|<code>^[\w\-\.]+:reference-data\-\-FacilityStateType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacilityStateType](../reference-data/FacilityStateType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacilityState.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractFacility.1.0.0](AbstractFacility.1.0.0.md) | FacilityStates[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._