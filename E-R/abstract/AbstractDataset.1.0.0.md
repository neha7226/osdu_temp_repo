<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractDataset [Status: Accepted]
Schema fragment holding properties common for all datasets.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractDataset:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractDataset.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractDataset.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractDataset.1.0.0.json](../../Authoring/abstract/AbstractDataset.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractDataset.1.0.0.json](../../Generated/abstract/AbstractDataset.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractDataset.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractDataset.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractDataset


## Outgoing Relationships for AbstractDataset

![AbstractDataset](../_diagrams/abstract/AbstractDataset.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractDataset Referenced by other Entities

![AbstractDataset](../_diagrams/abstract/AbstractDataset.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractDataset Properties

## Table of AbstractDataset Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Name| |string|optional|Name|An optional name of the dataset, e.g. a user friendly file or file collection name.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Dataset X221/15|
|Description| |string|optional|Description|An optional, textual description of the dataset.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|As originally delivered by ACME.com.|
|TotalSize| |string|optional|Total Size|Total size of the dataset in bytes; for files it is the same as declared in FileSourceInfo.FileSize or the sum of all individual files. Implemented as string. The value must be convertible to a long integer (sizes can become very large).|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|13245217273|
|EncodingFormatTypeID &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)| |string|optional|Encoding Format Type ID|EncodingFormatType ID reference value relationship. It can be a mime-type or media-type.|<code>^[\w\-\.]+:reference-data\-\-EncodingFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--EncodingFormatType:text%2Fcsv:|
|SchemaFormatTypeID &rarr; [SchemaFormatType](../reference-data/SchemaFormatType.1.0.0.md)| |string|optional|Schema Format Type ID|Relationship to the SchemaFormatType reference value.|<code>^[\w\-\.]+:reference-data\-\-SchemaFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SchemaFormatType](../reference-data/SchemaFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--SchemaFormatType:CWLS%20LAS3:|
|Endian| |enum string|optional|Endian|Endianness of binary value.  Enumeration: "BIG", "LITTLE".  If absent, applications will need to interpret from context indicators.|<code>BIG&#124;LITTLE</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|BIG|
|_DatasetProperties_| |_object_|_required_|_Dataset Properties_|_Placeholder for a specialization._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{}_|
|DatasetProperties| |object|required|Dataset Properties|Placeholder for a specialization.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{}|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractDataset.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [ConnectedSource.Generic.1.0.0](../dataset/ConnectedSource.Generic.1.0.0.md) | allOf | one |  |
| Included by | [ETPDataspace.1.0.0](../dataset/ETPDataspace.1.0.0.md) | allOf | one |  |
| Included by | [File.CompressedVectorHeaders.1.0.0](../dataset/File.CompressedVectorHeaders.1.0.0.md) | allOf | one |  |
| Included by | [File.Generic.1.0.0](../dataset/File.Generic.1.0.0.md) | allOf | one |  |
| Included by | [File.GeoJSON.1.0.0](../dataset/File.GeoJSON.1.0.0.md) | allOf | one |  |
| Included by | [File.Image.JPEG.1.0.0](../dataset/File.Image.JPEG.1.0.0.md) | allOf | one |  |
| Included by | [File.Image.PNG.1.0.0](../dataset/File.Image.PNG.1.0.0.md) | allOf | one |  |
| Included by | [File.Image.TIFF.1.0.0](../dataset/File.Image.TIFF.1.0.0.md) | allOf | one |  |
| Included by | [File.Image.WorldFile.1.0.0](../dataset/File.Image.WorldFile.1.0.0.md) | allOf | one |  |
| Included by | [File.OGC.GeoTIFF.1.0.0](../dataset/File.OGC.GeoTIFF.1.0.0.md) | allOf | one |  |
| Included by | [File.WITSML.1.0.0](../dataset/File.WITSML.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.Bluware.OpenVDS.1.0.0](../dataset/FileCollection.Bluware.OpenVDS.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.Esri.Shape.1.0.0](../dataset/FileCollection.Esri.Shape.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.Generic.1.0.0](../dataset/FileCollection.Generic.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.SEGY.1.0.0](../dataset/FileCollection.SEGY.1.0.0.md) | allOf | one |  |
| Included by | [FileCollection.Slb.OpenZGY.1.0.0](../dataset/FileCollection.Slb.OpenZGY.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._