<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractStorageLocation [Status: Accepted]
A record about the storage location of an item, e.g. a rock or fluid sample, seismic tape, where (facility), by whom (organisation), when (dates) and how (description).
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractStorageLocation:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractStorageLocation.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractStorageLocation.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractStorageLocation.1.0.0.json](../../Authoring/abstract/AbstractStorageLocation.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractStorageLocation.1.0.0.json](../../Generated/abstract/AbstractStorageLocation.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractStorageLocation.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractStorageLocation.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractStorageLocation


## Outgoing Relationships for AbstractStorageLocation

![AbstractStorageLocation](../_diagrams/abstract/AbstractStorageLocation.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractStorageLocation Referenced by other Entities

![AbstractStorageLocation](../_diagrams/abstract/AbstractStorageLocation.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractStorageLocation Properties

## Table of AbstractStorageLocation Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|StorageLocationDescription| |string|optional|Storage Location Description|The name of the location where the item is stored. It can be stored in more than one location over time.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|StorageFacilityID &rarr; [StorageFacility](../master-data/StorageFacility.1.1.0.md)| |string|optional|Storage Facility Id|Identifies the warehouse in which the item is stored.|<code>^[\w\-\.]+:master-data\-\-StorageFacility:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StorageFacility](../master-data/StorageFacility.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|StorageOrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Storage Organisation Id|Identifies the organisation with which the item is stored.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|EffectiveDateTime| |string|optional|Effective Date Time|The date the item arrived at the storage location.|(No pattern)|date|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TerminationDateTime| |string|optional|Termination Date Time|The date and time at which the item is  no longer stored in the given location.  If the item is still in this storage, the 'TerminationDateTime' is left absent.|(No pattern)|date|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SampleIdentifier| |string|optional|Sample Identifier|The item (sample, tape)  identifier, for example a barcode, which identifies the item in the StorageFacility.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractStorageLocation.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [RockSample.1.0.0](../master-data/RockSample.1.0.0.md) | SampleStorageLocations[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._