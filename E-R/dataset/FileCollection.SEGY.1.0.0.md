<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# FileCollection.SEGY [Status: Accepted]
A generic representation for  SEGY dataset represented as set of files, e.g. representing original tape reels.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:dataset--FileCollection.SEGY:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: OSDU
* Link to &rarr; Proposal workbook [FileCollection.SEGY.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/dataset/FileCollection.SEGY.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema FileCollection.SEGY.1.0.0.json](../../Authoring/dataset/FileCollection.SEGY.1.0.0.json)
* Link to &rarr; [Generated Schema FileCollection.SEGY.1.0.0.json](../../Generated/dataset/FileCollection.SEGY.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource FileCollection.SEGY.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//dataset/FileCollection.SEGY.1.0.0.json)
* Link to &rarr; [Example Record FileCollection.SEGY.1.0.0.json](../../Examples/dataset/FileCollection.SEGY.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.
* Link to worked examples in context &rarr; [Topic: 4D Seismic Definition & Examples](../../Examples/WorkedExamples/Seismic4D/README.md)
* Link to worked examples in context &rarr; [Topic: Seismic Manifest Loading](../../Examples/WorkedExamples/SeismicLoadingManifests/README.md)
* Link to worked examples in context &rarr; [Topic: Seismic Pre-Stack](../../Examples/WorkedExamples/SeismicPreStack/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams FileCollection.SEGY


## Outgoing Relationships for FileCollection.SEGY

![FileCollection.SEGY](../_diagrams/dataset/FileCollection.SEGY.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## FileCollection.SEGY Referenced by other Entities

![FileCollection.SEGY](../_diagrams/dataset/FileCollection.SEGY.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# FileCollection.SEGY Properties

## 1. Table of FileCollection.SEGY System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:dataset\-\-FileCollection.SEGY:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:dataset--FileCollection.SEGY:b47f793a-dc9c-5d11-8d59-6cc611a98926|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:dataset--FileCollection.SEGY:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of FileCollection.SEGY Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of FileCollection.SEGY Data Properties, Section AbstractDataset

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)_|_object_|_optional_|_AbstractDataset_|_Schema fragment holding properties common for all datasets._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Name|An optional name of the dataset, e.g. a user friendly file or file collection name.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Dataset X221/15|
|data.Description|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Description|An optional, textual description of the dataset.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|As originally delivered by ACME.com.|
|data.TotalSize|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Total Size|Total size of the dataset in bytes; for files it is the same as declared in FileSourceInfo.FileSize or the sum of all individual files. Implemented as string. The value must be convertible to a long integer (sizes can become very large).|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|13245217273|
|data.EncodingFormatTypeID &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Encoding Format Type ID|EncodingFormatType ID reference value relationship. It can be a mime-type or media-type.|<code>^[\w\-\.]+:reference-data\-\-EncodingFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--EncodingFormatType:text%2Fcsv:|
|data.SchemaFormatTypeID &rarr; [SchemaFormatType](../reference-data/SchemaFormatType.1.0.0.md)|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Schema Format Type ID|Relationship to the SchemaFormatType reference value.|<code>^[\w\-\.]+:reference-data\-\-SchemaFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SchemaFormatType](../reference-data/SchemaFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--SchemaFormatType:CWLS%20LAS3:|
|data.Endian|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|enum string|optional|Endian|Endianness of binary value.  Enumeration: "BIG", "LITTLE".  If absent, applications will need to interpret from context indicators.|<code>BIG&#124;LITTLE</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|BIG|
|_data.DatasetProperties_|_[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)_|_object_|_required_|_Dataset Properties_|_Placeholder for a specialization._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{}_|
|data.DatasetProperties|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|object|required|Dataset Properties|Placeholder for a specialization.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{}|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of FileCollection.SEGY Data Properties, Section AbstractFileCollection

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md)_|_object_|_optional_|_AbstractFileCollection_|_The schema fragment representing file collections._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|_data.DatasetProperties_|_[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md)_|_object_|_required_|_Dataset Properties_|_The dataset properties for a file collection._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.DatasetProperties.FileCollectionPath|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md)|string|required|File Collection Path|The mandatory path to the file collection. A FileCollectionPath should represent folder level access to a set of files.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://default_bucket/opendes/data/vds-dataset/|
|data.DatasetProperties.IndexFilePath|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md)|string|optional|Index File Path|An optional path to an index file.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://default_bucket/opendes/data/vds-dataset/vds-dataset.index|
|_data.DatasetProperties.FileSourceInfos[]_|_[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)_|_object_|_optional_|_AbstractFileSourceInfo_|_Array of file collection members as FileSourceInfo. Fragment Description: A schema fragment to describe file source information._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.DatasetProperties.FileSourceInfos[].FileSource|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|required|File Source|The location of the file. It can be a relative path. The actual access is provided via the File Service. When used in context of a FileCollection (dataset--FileCollection*) FileSource is a relative path from the FileCollectionPath. It can be used by consumers to pull an individual file if they so choose by concatenating the FileCollectionPath with the FileSource. This property is required.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://default_bucket/r1/data/provided/documents/1000.witsml|
|data.DatasetProperties.FileSourceInfos[].PreloadFilePath|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Path|File system path to the data file as it existed before loading to the data platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://staging-area/r7/raw-data/provided/documents/1000.witsml|
|data.DatasetProperties.FileSourceInfos[].PreloadFileCreateUser|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Create User|Optional user name or reference, who created the file prior to up-loading to the platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|somebody@acme.org|
|data.DatasetProperties.FileSourceInfos[].PreloadFileCreateDate|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Create Date|Optional create date and time of the file prior to up-loading to the platform.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|2019-12-16T11:46:20.163Z|
|data.DatasetProperties.FileSourceInfos[].PreloadFileModifyUser|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Modify User|Optional user name or reference, who last modified the file prior to up-loading to the platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|somebody.else@acme.org|
|data.DatasetProperties.FileSourceInfos[].PreloadFileModifyDate|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Modify Date|Optional last modified date and time of the file prior to up-loading to the platform.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|2019-12-20T17:20:05.356Z|
|data.DatasetProperties.FileSourceInfos[].Name|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Name|Optional, user-friendly file name.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1000.witsml|
|data.DatasetProperties.FileSourceInfos[].FileSize|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|File Size|Length of file in bytes. Implemented as string. The value must be convertible to a long integer (sizes can become very large).|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|95463|
|data.DatasetProperties.FileSourceInfos[].EncodingFormatTypeID &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Encoding Format Type ID|Only used in FileCollection where the EncodingFormatType differs from data DatasetDefault specified in data.EncodingFormatTypeID: the media type specification for this dataset.|<code>^[\w\-\.]+:reference-data\-\-EncodingFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--EncodingFormatType:application%2Fgeo%2Bjson:|
|data.DatasetProperties.FileSourceInfos[].Checksum|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Checksum|Checksum of file bytes - a hexadecimal number with even number of bytes.|<code>^([0-9a-fA-F]{2})+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|d41d8cd98f00b204e9800998ecf8427e|
|data.DatasetProperties.FileSourceInfos[].ChecksumAlgorithm|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Checksum Algorithm|The name of the checksum algorithm e.g. MD5, SHA-256.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|SHA-256|
|data.DatasetProperties.Checksum|[AbstractFileCollection.1.0.0](../abstract/AbstractFileCollection.1.0.0.md)|string|optional|MD5 Checksum|MD5 checksum of file bytes - a 32 byte hexadecimal number.|<code>^[0-9a-fA-F]{32}</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|d41d8cd98f00b204e9800998ecf8427e|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of FileCollection.SEGY Data Properties, Section AbstractVectorHeaderMapping

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractVectorHeaderMapping.1.0.0](../abstract/AbstractVectorHeaderMapping.1.0.0.md)_|_object_|_optional_|_AbstractVectorHeaderMapping_|_Array of objects which define the meaning and format of a tabular structure used in a binary file as a header.  The initial use case is the trace headers of a SEG-Y file.  Note that some of this information may be repeated in the SEG-Y EBCDIC header._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|_data.VectorHeaderMapping[]_|_[AbstractVectorHeaderMapping.1.0.0](../abstract/AbstractVectorHeaderMapping.1.0.0.md)_|_object_|_optional_|_Vector Header Mapping_|_Array of objects which define the meaning and format of a tabular structure used in a binary file as a header.  The initial use case is the trace headers of a SEG-Y file.  Note that some of this information may be repeated in the SEG-Y EBCDIC header._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.VectorHeaderMapping[].KeyName &rarr; [HeaderKeyName](../reference-data/HeaderKeyName.1.0.0.md)|[AbstractVectorHeaderMapping.1.0.0](../abstract/AbstractVectorHeaderMapping.1.0.0.md)|string|optional|Key Name|Relationship to a reference value for a name of a property header such as INLINE, CDPX.|<code>^[\w\-\.]+:reference-data\-\-HeaderKeyName:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [HeaderKeyName](../reference-data/HeaderKeyName.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VectorHeaderMapping[].WordFormat &rarr; [WordFormatType](../reference-data/WordFormatType.1.0.0.md)|[AbstractVectorHeaderMapping.1.0.0](../abstract/AbstractVectorHeaderMapping.1.0.0.md)|string|optional|Word Format|Relationship to a reference value for binary data types, such as INT, UINT, FLOAT, IBM_FLOAT, ASCII, EBCDIC.|<code>^[\w\-\.]+:reference-data\-\-WordFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WordFormatType](../reference-data/WordFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VectorHeaderMapping[].WordWidth|[AbstractVectorHeaderMapping.1.0.0](../abstract/AbstractVectorHeaderMapping.1.0.0.md)|integer|optional|Word Width|Size of the word in bytes.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VectorHeaderMapping[].Position|[AbstractVectorHeaderMapping.1.0.0](../abstract/AbstractVectorHeaderMapping.1.0.0.md)|integer|optional|Position|Beginning byte position of header value, 1 indexed.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VectorHeaderMapping[].UoM &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|[AbstractVectorHeaderMapping.1.0.0](../abstract/AbstractVectorHeaderMapping.1.0.0.md)|string|optional|Unit of Measure|Relationship to units of measure reference if header standard is not followed.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VectorHeaderMapping[].ScalarIndicator|[AbstractVectorHeaderMapping.1.0.0](../abstract/AbstractVectorHeaderMapping.1.0.0.md)|enum string|optional|Scalar Indicator|Enumerated string indicating whether to use the normal scalar field for scaling this field (STANDARD), no scaling (NOSCALE), or override scalar (OVERRIDE).  Default is current STANDARD (such as SEG-Y rev2).|<code>STANDARD&#124;NOSCALE&#124;OVERRIDE</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|STANDARD|
|data.VectorHeaderMapping[].ScalarOverride|[AbstractVectorHeaderMapping.1.0.0](../abstract/AbstractVectorHeaderMapping.1.0.0.md)|number|optional|Scalar Override|Scalar value (as defined by standard) when a value present in the header needs to be overwritten for this value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 6. Table of FileCollection.SEGY Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.SEGYRevision| |string|optional|SEG-Y Revision|The SEG-Y standard revision the SEG-Y file set is conforming to.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|rev 0|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 7. Table of FileCollection.SEGY Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# FileCollection.SEGY.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractWPCGroupType.1.0.0](../abstract/AbstractWPCGroupType.1.0.0.md) | Datasets[] | many (as array) | Untyped relationship |
| Referred to by | [AbstractWPCGroupType.1.0.0](../abstract/AbstractWPCGroupType.1.0.0.md) | Artefacts[].ResourceID | many (as array) | Untyped relationship |
| Referred to by | [SeismicTraceData.1.0.0](../work-product-component/SeismicTraceData.1.0.0.md) | TraceRelationFileID | one | Untyped relationship |
| Referred to by | [SeismicTraceData.1.1.0](../work-product-component/SeismicTraceData.1.1.0.md) | TraceRelationFileID | one | Untyped relationship |
| Referred to by | [SeismicTraceData.1.2.0](../work-product-component/SeismicTraceData.1.2.0.md) | TraceRelationFileID | one | Untyped relationship |
| Referred to by | [SeismicTraceData.1.3.0](../work-product-component/SeismicTraceData.1.3.0.md) | TraceRelationFileID | one | Untyped relationship |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._