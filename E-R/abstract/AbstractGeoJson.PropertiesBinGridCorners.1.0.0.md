<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractGeoJson.PropertiesBinGridCorners [Status: Accepted]
A compact BinGrid corner point annotation, which is used to validate orthogonality and linearity of a BinGrid. It associates the BinGrid corner coordinates to the inline and crossline number.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractGeoJson.PropertiesBinGridCorners:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractGeoJson.PropertiesBinGridCorners.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractGeoJson.PropertiesBinGridCorners.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractGeoJson.PropertiesBinGridCorners.1.0.0.json](../../Authoring/abstract/AbstractGeoJson.PropertiesBinGridCorners.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractGeoJson.PropertiesBinGridCorners.1.0.0.json](../../Generated/abstract/AbstractGeoJson.PropertiesBinGridCorners.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractGeoJson.PropertiesBinGridCorners.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractGeoJson.PropertiesBinGridCorners.1.0.0.json)
* Link to worked examples in context &rarr; [Topic: GeoJSON `properties` Schema Fragments](../../Examples/WorkedExamples/GeoJSON-properties/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractGeoJson.PropertiesBinGridCorners


## Outgoing Relationships for AbstractGeoJson.PropertiesBinGridCorners

![AbstractGeoJson.PropertiesBinGridCorners](../_diagrams/abstract/AbstractGeoJson.PropertiesBinGridCorners.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractGeoJson.PropertiesBinGridCorners Referenced by other Entities

![AbstractGeoJson.PropertiesBinGridCorners](../_diagrams/abstract/AbstractGeoJson.PropertiesBinGridCorners.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractGeoJson.PropertiesBinGridCorners Properties

## Table of AbstractGeoJson.PropertiesBinGridCorners Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Kind| |enum string|required|Kind|The actual Kind of schema fragment AbstractGeoJson.PropertiesBinGridCorners|<code>osdu:wks:AbstractGeoJson.PropertiesBinGridCorners:1.0.0</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:AbstractGeoJson.PropertiesBinGridCorners:1.0.0|
|_PointProperties[]_| |_object_|_optional_|_PointProperties_|_Corner point information for each point in the Feature/AnyCrsFeature._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|PointProperties[].Inline| |number|optional|Inline Number|The inline number associated with the bin location (usually an integer) number. Mandatory.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|9745|
|PointProperties[].Crossline| |number|optional|Crossline Number|The crossline number associated with the bin location (usually an integer) number. Mandatory.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1692|
|PointProperties[].Label| |string|optional|Label|An optional label string associated to the corner point.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|A  (min IL, min XL)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._