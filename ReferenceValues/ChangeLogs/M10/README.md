<a name="TOC"></a>**Table of Contents**

[[_TOC_]]

# M10 Changes

## OSDUJsonExtensions

The code `x-osdu-virtual-properties` was added to the FIXED governance reference value type.

## UnitQuantity

Energistics unit quantity mappings (see [sources](../../Resources/Energistics/EnergisticsMappings/UQ-Mappings)) have
been added as `NameAlias` extensions. The overview is reported
in [the report](UnitQuantity-Differences.md#namealias-changes).

## UnitOfMeasure

A number of non-dimensional units are incorrectly associated with dimension/UnitQuantity none. The changes are
summarized in [this report](UnitOfMeasure-Differences.md). A [CSV version](UnitOfMeasure-Differences.csv) is prepared
for further analysis in Excel.

Energistics unit of measure mappings (see [sources](../../Resources/Energistics/EnergisticsMappings/UoM-Mappings)) have
been added as `NameAlias` extensions. The overview is reported
in [the report](UnitOfMeasure-Differences.md#namealias-changes).

[Back to TOC](#TOC)

## PropertyType

Changes of matching strategy:

1. When searching for UnitQuantity matches, perform them case-insensitive to catch "api gamma ray" versus "API gamma
   ray". This strategy is confirmed by
   the [PWLS Usage Guide](../../Resources/Energistics/PWLS_Usage_Guide_v3.0_Doc_v1.0.pdf), Glossary page 14, chapter _
   Quantity Class_.
2. Translate PWLS Primary QuantityClass "unitless" to UnitQuantity "none".
3. Stop traversing the PWLS Property hierarchy to find matching UnitQuantity.

The changes are summarized in [this report](PropertyType-Differences.md). A [CSV version](PropertyType-Differences.csv)
is prepared for further analysis in Excel.

In some cases UnitQuantity:dimensionless have been changed to the UnitQuantity:none. A number of such cases are
associated with ratios, which - by name - would better be associated to UnitQuantity:dimensionless. However, it has been
decided to stay true to the [Energistics PWLS definitions](../../Resources/Energistics/PWLS%20v3.0%20Properties.xlsx)
_Primary QuantityClass_, which assigns those properties indeed to a non-dimensional quantity.

## StandardsOrganisation

Added a value for ICS for "International Commission on Stratigraphy" https://stratigraphy.org.

[Back to TOC](#TOC)

## LogCurveType:1.1.0

The schema `osdu:wks:reference-data--LogCurveType:1.0.0` was created
before [PWLS 3 Logs](../../Resources/Energistics/PWLS%20v3.0%20Logs.xlsx) was published. It lacks explicit relationships
to UnitQuantity, PropertyType and StandardsOrganisation for the `data.AcquisitionCompanyID`. Previously, these
relationships were only described in `data.Description`.

### LogCurveType:1.1.0 New Properties

| LogCurveType:1.1.0 Property Name | Target Type |
|---|---|
|data.PropertyType.PropertyTypeID | `osdu:wks:reference-data--PropertyType:1.0.0`|
|data.PropertyType.Name|The name of the PropertyType, de-normalized, derived from the record referenced in PropertyTypeID.|
|data.UnitQuantityID | `osdu:wks:reference-data--UnitQuantity:1.0.0`| 
|data.AcquisitionCompanyID | `osdu:wks:reference-data--StandardsOrganisation:1.0.0` |

[Back to TOC](#TOC)

### LogCurveType:1.1.0 Population Strategy

1. UnitQuantity Relationship - from PWLS 3 Logs, 'Curves' sheet, column 'Curve Unit Quantity Class':
    1. If the value is `'unitless'`, replace it by `'none'` - a UnitQuantity `'unitless'` is not declared.
    2. Look-up the value in the UnitQuantity manifest by matching the lower-case version of the UnitQuantity data.Code,
       also removing any brackets '(' or ')' from the UnitQuantity `data.Code`.
    3. If no match is found, leave the `data.UnitQuantityID` empty.
2. PropertyType Relationship - from PWLS 3 Logs, 'Curves' sheet, column 'PWLS v3 Property':
    1. Scan the PropertyType `data.Name` for matching names and create a list of candidates.
    2. if no candidates were found, leave the PropertyType relationship empty.
    3. Scan the list of candidates from (1.) and find the first entry with matching UnitQuantity (compared
       case-insensitive and '(' or ')' removed).
    4. If no UnitQuantity match was found in the candidates, return the first candidate.
    5. For the related PropertyType,
        1. populate the `data.PropertyType.PropertyTypeID` with the `id`;
        2. populate the `data.PropertyType.Name` with the PropertyType `data.Name` value.
3. StandardsOrganisation Relationship (AcquisitionCompanyID) - from PWLS 3 Logs, 'Curves' sheet, column 'Company Code'
   looked up in the StandardsOrganisation manifest. All company codes referenced by PWLS 3 Logs are added/present in the
   StandardsOrganisation.1.0.0 manifest.

In total, only one record `Schlumberger:DOC` lacks relationships to UnitQuantity and PropertyType because the quantity
is not (yet) supported and would require an augmentation of the UnitQuantity.1.0.0 manifest - which is not planned.

[Back to TOC](#TOC)

### Additional Values LogCurveType

Additional values have been added to support checkshots and time-depth curves represented as 
`work-product-component--WellLog`:

| Name                             | Description                  | Code                             |
|----------------------------------|------------------------------|----------------------------------|
| Schlumberger:OneWayTime          | One Way Time from checkshot  | Schlumberger:OneWayTime          |
| Schlumberger:TwoWayTime          | Two Way Time from checkshot  | Schlumberger:TwoWayTime          |
| Schlumberger:SeismicTimeSmoothed | Smoothed seismic time curve. | Schlumberger:SeismicTimeSmoothed |

[Back to TOC](#TOC)

## LogCurveFamily:1.1.0

The schema for LogCurveFamily:1.1.0 was created without UnitQuantity relationship. Similar to LogCurveType, the
LogCurveFamily is augmented with relationships. Previously the relationships were only indicated in `data.Description`.

### LogCurveFamily:1.1.0 New Properties

| LogCurveFamily:1.1.0 Property Name | Target Type |
|---|---|
|data.PropertyType.PropertyTypeID | `osdu:wks:reference-data--PropertyType:1.0.0`|
|data.PropertyType.Name|The name of the PropertyType, de-normalized, derived from the record referenced in PropertyTypeID.|
|data.UnitQuantityID | `osdu:wks:reference-data--UnitQuantity:1.0.0`| 

[Back to TOC](#TOC)

### LogCurveFamily:1.1.0 Population Strategy

The [original resources](../../Resources/Energistics/family_to_main_family_catalog.json) contain a Measurement and a
Unit property, which can be used to establish relationships.

1. Taking the Measurement, scan the for matching PropertyType `data.Name` values by
    1. direct matches
    2. lower case matches
    3. matching the names by replacing the `_` with spaces.<br>
       If a match is found, the `data.PropertyType.PropertyTypeID` is populated with the GUID and
       the `data.PropertyType.Name` with the actual PWLS 3 property name. The UnitQuantity is derived from the
       PropertyType.
2. Using the Unit,
    1. scan the UnitQuantity's `data.MemberUnits[]`, if the unit is found, select the UnitQuantity, to which the unit
       was associated as a member. The `data.PropertyType.PropertyTypeID` stays empty, `data.PropertyType.Name` carries
       the original property name as in the source.
    2. scan the units for matching unit symbol, id a unit is found. select the UnitQuantity (=the dimension), to which
       the unit was associated. The `data.PropertyType.PropertyTypeID` stays empty, `data.PropertyType.Name` carries the
       original property name as in the source.
    3. If no match was found, the family record is dropped.

[Back to TOC](#TOC)

### Additional Values LogCurveFamily

Additional values have been added to support checkshots and time-depth curves represented as 
`work-product-component--WellLog`:

| Name                   | Description                                                                  | Code                 |
|------------------------|------------------------------------------------------------------------------|----------------------|
| Checkshot One-way Time | The checkshot one-way time LogCurveFamily.                                   | 	CheckshotOneWayTime |
| Checkshot Two-way Time | The checkshot two-way time LogCurveFamily.                                   | 	CheckshotTwoWayTime |
| One-way Time           | General one-way time LogCurveFamily to be used for time-depth relationships. | OneWayTime           |
| Two-way Time           | General two-way time LogCurveFamily to be used for time-depth relationships. | TwoWayTime           |

## LogCurveMainFamily.1.0.0

### Additional Values LogCurveMainFamily

Additional values have been added to support checkshots and time-depth curves represented as 
`work-product-component--WellLog`:

| Name      | Description                                                                                     | Code      |
|-----------|-------------------------------------------------------------------------------------------------|-----------|
| TimeDepth | This Main Family contains CheckshotOneWayTime, CheckshotTwoWayTime, OneWayTime, and TwoWayTime. | TimeDepth |

[Back to TOC](#TOC)

## New Seismic Reference Data

| kind                                                    | Description                                                                                                        | Number of Records | Governance<br>Model | Workbook                                                                               | Manifest                                                                                                   |
|---------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|-------------------|---------------------|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| `osdu:wks:reference-data--SeismicAcquisitionType:1.0.0` | The SeismicAcquisitionType, e.g. Conventional, Wide Azimuth, Multi Azimuth etc.                                    | OPEN              | 15                  | [SeismicAcquisitionType.1.0.0.xlsx](../../Published/SeismicAcquisitionType.1.0.0.xlsx) | [SeismicAcquisitionType.1.0.0.json](../../Manifests/reference-data/OPEN/SeismicAcquisitionType.1.0.0.json) |
| `osdu:wks:reference-data--SeismicGatherType:1.0.0`      | The type of gathers represented in seismic pre-stack datasets.                                                     | OPEN              | 10                  | [SeismicGatherType.1.0.0.xlsx](../../Published/SeismicGatherType.1.0.0.xlsx)           | [SeismicGatherType.1.0.0.json](../../Manifests/reference-data/OPEN/SeismicGatherType.1.0.0.json)           |
| `osdu:wks:reference-data--SeismicReceiverType:1.0.0`    | A catalog member defining a receiver type in seismic data acquisition.                                             | OPEN              | 6                   | [SeismicReceiverType.1.0.0.xlsx](../../Published/SeismicReceiverType.1.0.0.xlsx)       | [SeismicReceiverType.1.0.0.json](../../Manifests/reference-data/OPEN/SeismicReceiverType.1.0.0.json)       |
| `osdu:wks:reference-data--SeismicTraceSortOrder:1.0.0`  | The types of sorting orders for pre-stack seismic datasets. It describes the trace order of the persisted dataset. | OPEN              | 8                   | [SeismicTraceSortOrder.1.0.0.xlsx](../../Published/SeismicTraceSortOrder.1.0.0.xlsx)   | [SeismicTraceSortOrder.1.0.0.json](../../Manifests/reference-data/OPEN/SeismicTraceSortOrder.1.0.0.json)   |

In addition, new reference values were added to LOCAL governance 
[`osdu:wks:reference-data--SeismicAttributeType:1.0.0`](../../Manifests/reference-data/LOCAL/SeismicAttributeType.1.0.0.json): 
* `AcousticImpedance`,
* `ElasticImpedance`, 
* `VpVsRatio`, 
* `ShearModulus`, 
* `BulkModulus`, and
* `PoissonsRatio`

[Back to TOC](#TOC)

## StandardsOrganisation

Added a value for ICS for "International Commission on Stratigraphy" https://stratigraphy.org.

## Well Planning and Drilling Execution Domain

M10 integrates the reference data support for well planning schemas. The following reference data types are added:

| kind | Description | Number of Records | Governance<br>Model | Workbook | Manifest |
|---|---|---|---|---|---|
| `osdu:wks:reference-data--ActivityCode:1.0.0` | ActivityCode for Well Construction and Intervention (for L1), Construct well (for L2), Intervene well (for L2), … | FIXED | 7394 | [ActivityCode.1.0.0.xlsx](../../Published/ActivityCode.1.0.0.xlsx) | [ActivityCode.1.0.0.json](../../Manifests/reference-data/FIXED/ActivityCode.1.0.0.json) |
| `osdu:wks:reference-data--ActivityLevel:1.0.0` | Proposed reference values: L1, L2, L3, L4, L5, L6 | FIXED | 10 | [ActivityLevel.1.0.0.xlsx](../../Published/ActivityLevel.1.0.0.xlsx) | [ActivityLevel.1.0.0.json](../../Manifests/reference-data/FIXED/ActivityLevel.1.0.0.json) |
| `osdu:wks:reference-data--ActivityOutcome:1.0.0` | Possible outcomes from a drilling activity | FIXED | 2 | [ActivityOutcome.1.0.0.xlsx](../../Published/ActivityOutcome.1.0.0.xlsx) | [ActivityOutcome.1.0.0.json](../../Manifests/reference-data/FIXED/ActivityOutcome.1.0.0.json) |
| `osdu:wks:reference-data--ActivityOutcomeDetail:1.0.0` | Detailed outcomes from a drilling activity | FIXED | 9 | [ActivityOutcomeDetail.1.0.0.xlsx](../../Published/ActivityOutcomeDetail.1.0.0.xlsx) | [ActivityOutcomeDetail.1.0.0.json](../../Manifests/reference-data/FIXED/ActivityOutcomeDetail.1.0.0.json) |
| `osdu:wks:reference-data--AdditiveType:1.0.0` | Items added to cement slurry or cement spacers, washes and fluids | FIXED | 48 | AdditiveType.1.0.0.xlsx | AdditiveType.1.0.0.json |
| `osdu:wks:reference-data--BhaStatus:1.0.0` | The status of the Bottom Hole Assembly. Proposed reference values: final, progress, plan, unknown | OPEN | 2 | [BhaStatus.1.0.0.xlsx](../../Published/BhaStatus.1.0.0.xlsx) | [BhaStatus.1.0.0.json](../../Manifests/reference-data/OPEN/BhaStatus.1.0.0.json) |
| `osdu:wks:reference-data--BitDullCode:1.0.0` | The IADC bit dull codes | FIXED | 27 | [BitDullCode.1.0.0.xlsx](../../Published/BitDullCode.1.0.0.xlsx) | [BitDullCode.1.0.0.json](../../Manifests/reference-data/FIXED/BitDullCode.1.0.0.json) |
| `osdu:wks:reference-data--BitReasonPulled:1.0.0` | The IADC list of reasons why a bit might have been pulled | FIXED | 19 | [BitReasonPulled.1.0.0.xlsx](../../Published/BitReasonPulled.1.0.0.xlsx) | [BitReasonPulled.1.0.0.json](../../Manifests/reference-data/FIXED/BitReasonPulled.1.0.0.json) |
| `osdu:wks:reference-data--BitType:1.0.0` | The IADC list of Bit Types | FIXED | 7 | [BitType.1.0.0.xlsx](../../Published/BitType.1.0.1.xlsx) | [BitType.1.0.0.json](../../Manifests/reference-data/FIXED/BitType.1.0.1.json) |
| `osdu:wks:reference-data--CementJobType:1.0.0` | A single cement job. Possible Values: Primary, Plug, Squeeze, Unknown | OPEN | 4 | [CementJobType.1.0.0.xlsx](../../Published/CementJobType.1.0.0.xlsx) | [CementJobType.1.0.0.json](../../Manifests/reference-data/OPEN/CementJobType.1.0.0.json) |
| `osdu:wks:reference-data--ChronoStratigraphy:1.0.0` | The reference value type for Chronostratigraphic classification. The purpose of chronostratigraphic classification is to organize systematically the rocks forming the Earth's crust into named units (chronostratigraphic units) that represent intervals of geologic time (geochronologic units) to serve as references in narratives about Earth history including the evolution of life. Records of this kind represent chronostratigraphic units, i.e., bodies of rocks that include all rocks representative of a specific interval of geologic time, and only this time span. Chronostratigraphic units are bounded by isochronous horizons which mark specific moments of geological time. The rank (via data.ChronostratigraphicHierarchy) and relative magnitude of the units in the chronostratigraphic hierarchy are a function of the durations they represent. | OPEN | 179 | [ChronoStratigraphy.1.0.0.xlsx](../../Published/ChronoStratigraphy.1.0.0.xlsx) | [ChronoStratigraphy.1.0.0.json](../../Manifests/reference-data/OPEN/ChronoStratigraphy.1.0.0.json) |
| `osdu:wks:reference-data--DrillingActivityClassType:1.0.0` | The type of drilling activity classes | OPEN | 4 | [DrillingActivityClassType.1.0.0.xlsx](../../Published/DrillingActivityClassType.1.0.0.xlsx) | [DrillingActivityClassType.1.0.0.json](../../Manifests/reference-data/OPEN/DrillingActivityClassType.1.0.0.json) |
| `osdu:wks:reference-data--ContactRoleType:1.0.0` | Role of the contact within the associated organisation, such as Account owner, Sales Representative, Technical Support, Project Manager, Party Chief, Client Representative, Senior Observer. | OPEN | 7 | [ContactRoleType.1.0.0.xlsx](../../Published/ContactRoleType.1.0.0.xlsx) | [ContactRoleType.1.0.0.json](../../Manifests/reference-data/OPEN/ContactRoleType.1.0.0.json) |
| `osdu:wks:reference-data--FluidPropertyFacetName:1.0.0` | Reference list of facet names, includes items such as such as  Minimum, Maximum  and Average | OPEN | 5 | [FluidPropertyFacetName.1.0.0.xlsx](../../Published/FluidPropertyFacetName.1.0.0.xlsx) | [FluidPropertyFacetName.1.0.0.json](../../Manifests/reference-data/OPEN/FluidPropertyFacetName.1.0.0.json) |
| `osdu:wks:reference-data--FluidPropertyName:1.0.0` | Fluid properties such as Ph, Pv, Yp, Oil/Water Ratio | OPEN | 21 | [FluidPropertyName.1.0.0.xlsx](../../Published/FluidPropertyName.1.0.0.xlsx) | [FluidPropertyName.1.0.0.json](../../Manifests/reference-data/OPEN/FluidPropertyName.1.0.0.json) |
| `osdu:wks:reference-data--FluidRheologicalModelType:1.0.0` | Mathematical model used in cement calculations | FIXED | 4 | [FluidRheologicalModelType.1.0.0.xlsx](../../Published/FluidRheologicalModelType.1.0.0.xlsx) | [FluidRheologicalModelType.1.0.0.json](../../Manifests/reference-data/FIXED/FluidRheologicalModelType.1.0.0.json) |
| `osdu:wks:reference-data--FluidRole:1.0.0` | Purpose the fluid will play in the wellbore, such as Cement Displacement, Hole Cleaning, Sweep, Completion Fluid | OPEN | 11 | [FluidRole.1.0.0.xlsx](../../Published/FluidRole.1.0.0.xlsx) | [FluidRole.1.0.0.json](../../Manifests/reference-data/OPEN/FluidRole.1.0.0.json) |
| `osdu:wks:reference-data--FluidType:1.0.0` | Type of drilling or completions fluid | FIXED | 27 | [FluidType.1.0.0.xlsx](../../Published/FluidType.1.0.0.xlsx) | [FluidType.1.0.0.json](../../Manifests/reference-data/FIXED/FluidType.1.0.0.json) |
| `osdu:wks:reference-data--FormationPressureTestType:1.0.0` | FIT, LOT, XLOT etc. Proposed reference values: Formation Integrity Test, Leak Off Test, Extended Leak Off Test | OPEN | 8 | [FormationPressureTestType.1.0.0.xlsx](../../Published/FormationPressureTestType.1.0.0.xlsx) | [FormationPressureTestType.1.0.0.json](../../Manifests/reference-data/OPEN/FormationPressureTestType.1.0.0.json) |
| `osdu:wks:reference-data--GasReadingType:1.0.0` | Type of gas readings associated with an operations report | FIXED | 8 | [GasReadingType.1.0.0.xlsx](../../Published/GasReadingType.1.0.0.xlsx) | [GasReadingType.1.0.0.json](../../Manifests/reference-data/FIXED/GasReadingType.1.0.0.json) |
| `osdu:wks:reference-data--LithoStratigraphy:1.0.0` | Name of lithostratigraphy, regionally dependent | OPEN | 5 | [LithoStratigraphy.1.0.0.xlsx](../../Published/LithoStratigraphy.1.0.0.xlsx) | [LithoStratigraphy.1.0.0.json](../../Manifests/reference-data/OPEN/LithoStratigraphy.1.0.0.json) |
| `osdu:wks:reference-data--LithologyType:1.0.0` | Geological description for the type of lithology | OPEN | 185 | [LithologyType.1.0.0.xlsx](../../Published/LithologyType.1.0.0.xlsx) | [LithologyType.1.0.0.json](../../Manifests/reference-data/OPEN/LithologyType.1.0.0.json) |
| `osdu:wks:reference-data--MudClass:1.0.0` | The status of the fluid liquid. Proposed reference values: water based, oil based, other, pneumatic, unknown | OPEN | 5 | [MudClass.1.0.0.xlsx](../../Published/MudClass.1.0.0.xlsx) | [MudClass.1.0.0.json](../../Manifests/reference-data/OPEN/MudClass.1.0.0.json) |
| `osdu:wks:reference-data--PersonnelOrganisationRole:1.0.0` | Type of Organisational Role that a person can perform | FIXED | 5 | [PersonnelOrganisationRole.1.0.0.xlsx](../../Published/PersonnelOrganisationRole.1.0.0.xlsx) | [PersonnelOrganisationRole.1.0.0.json](../../Manifests/reference-data/FIXED/PersonnelOrganisationRole.1.0.0.json) |
| `osdu:wks:reference-data--PersonnelServiceType:1.0.0` | Type of drilling service that a person can perform | FIXED | 13 | [PersonnelServiceType.1.0.0.xlsx](../../Published/PersonnelServiceType.1.0.0.xlsx) | [PersonnelServiceType.1.0.0.json](../../Manifests/reference-data/FIXED/PersonnelServiceType.1.0.0.json) |
| `osdu:wks:reference-data--PlugType:1.0.0` | Purpose of Cement Plug such as wellbore abandonment, side-tracking or kick-off. | FIXED | 4 | [PlugType.1.0.0.xlsx](../../Published/PlugType.1.0.0.xlsx) | [PlugType.1.0.0.json](../../Manifests/reference-data/FIXED/PlugType.1.0.0.json) |
| `osdu:wks:reference-data--PumpOpType:1.0.0` | Type of pump operations performed in the operations report | FIXED | 6 | [PumpOpType.1.0.0.xlsx](../../Published/PumpOpType.1.0.0.xlsx) | [PumpOpType.1.0.0.json](../../Manifests/reference-data/FIXED/PumpOpType.1.0.0.json) |
| `osdu:wks:reference-data--ReasonTripType:1.0.0` | BHA trip reason such as Change Bottomhole Assembly, Downhole Motor Failure, Drill Stem Failure | OPEN | 20 | [ReasonTripType.1.0.0.xlsx](../../Published/ReasonTripType.1.0.0.xlsx) | [ReasonTripType.1.0.0.json](../../Manifests/reference-data/OPEN/ReasonTripType.1.0.0.json) |
| `osdu:wks:reference-data--RigType:1.0.0` | Types of Drilling Rigs. Possible reference values: Barge, Coiled Tubing, Floater, Jackup, Land, Platform, Semi_Submersible, Unknown | OPEN | 40 | [RigType.1.0.0.xlsx](../../Published/RigType.1.0.0.xlsx) | [RigType.1.0.0.json](../../Manifests/reference-data/OPEN/RigType.1.0.0.json) |
| `osdu:wks:reference-data--RiskCategory:1.0.0` | Proposed reference values: hydraulics, mechanical, time related, wellbore stability, directional drilling, bit, equipment failure, completion, casing, other, HSE, unknown | FIXED | 11 | [RiskCategory.1.0.0.xlsx](../../Published/RiskCategory.1.0.0.xlsx) | [RiskCategory.1.0.0.json](../../Manifests/reference-data/FIXED/RiskCategory.1.0.0.json) |
| `osdu:wks:reference-data--RiskConsequenceCategory:1.0.0` | Consequence category of the risk for possible effects on Asset, Business & Reputation, Environment, Health & Safety… | OPEN | 5 | [RiskConsequenceCategory.1.0.0.xlsx](../../Published/RiskConsequenceCategory.1.0.0.xlsx) | [RiskConsequenceCategory.1.0.0.json](../../Manifests/reference-data/OPEN/RiskConsequenceCategory.1.0.0.json) |
| `osdu:wks:reference-data--RiskConsequenceSubCategory:1.0.0` | References the consequence sub-category of the risk. Possible effects arising were a risk event to occur. | OPEN | 38 | [RiskConsequenceSubCategory.1.0.0.xlsx](../../Published/RiskConsequenceSubCategory.1.0.0.xlsx) | [RiskConsequenceSubCategory.1.0.0.json](../../Manifests/reference-data/OPEN/RiskConsequenceSubCategory.1.0.0.json) |
| `osdu:wks:reference-data--RiskDiscipline:1.0.0` | Organisational unit accountable for maintaining appropriate focus on a risk and its management and for appropriate consultation with stakeholders | FIXED | 12 | [RiskDiscipline.1.0.0.xlsx](../../Published/RiskDiscipline.1.0.0.xlsx) | [RiskDiscipline.1.0.0.json](../../Manifests/reference-data/FIXED/RiskDiscipline.1.0.0.json) |
| `osdu:wks:reference-data--RiskHierarchyLevel:1.0.0` | Area within organisation where risks will be most applicable, includes Well-level, Field-level, Region-level, Global-level | FIXED | 4 | [RiskHierarchyLevel.1.0.0.xlsx](../../Published/RiskHierarchyLevel.1.0.0.xlsx) | [RiskHierarchyLevel.1.0.0.json](../../Manifests/reference-data/FIXED/RiskHierarchyLevel.1.0.0.json) |
| `osdu:wks:reference-data--RiskResponseStatus:1.0.0` | Describes the status of the action such as In progress, done, cancelled… | OPEN | 6 | [RiskResponseStatus.1.0.0.xlsx](../../Published/RiskResponseStatus.1.0.0.xlsx) | [RiskResponseStatus.1.0.0.json](../../Manifests/reference-data/OPEN/RiskResponseStatus.1.0.0.json) |
| `osdu:wks:reference-data--RiskSubCategory:1.0.0` | The reference values for risk sub-categories following the WITSML standard. | FIXED | 111 | [RiskSubCategory.1.0.0.xlsx](../../Published/RiskSubCategory.1.0.0.xlsx) | [RiskSubCategory.1.0.0.json](../../Manifests/reference-data/FIXED/RiskSubCategory.1.0.0.json) |
| `osdu:wks:reference-data--RiskType:1.0.0` | Proposed reference values: risk, event, near miss, best practice, lessons learned, other, unknown | FIXED | 5 | [RiskType.1.0.0.xlsx](../../Published/RiskType.1.0.0.xlsx) | [RiskType.1.0.0.json](../../Manifests/reference-data/FIXED/RiskType.1.0.0.json) |
| `osdu:wks:reference-data--StratigraphicColumnRankUnitType:1.0.0` | The reference value type defining a column position in a stratigraphic chart, which is ordered by rank (increasing from left to right). | OPEN | 15 | [StratigraphicColumnRankUnitType.1.0.0.xlsx](../../Published/StratigraphicColumnRankUnitType.1.0.0.xlsx) | [StratigraphicColumnRankUnitType.1.0.0.json](../../Manifests/reference-data/OPEN/StratigraphicColumnRankUnitType.1.0.0.json) |
| `osdu:wks:reference-data--StratigraphicRoleType:1.0.0` | The role or type of stratigraphy, e.g., Chronostratigraphic, Lithostratigraphic, etc. | FIXED | 6 | [StratigraphicRoleType.1.0.0.xlsx](../../Published/StratigraphicRoleType.1.0.0.xlsx) | [StratigraphicRoleType.1.0.0.json](../../Manifests/reference-data/FIXED/StratigraphicRoleType.1.0.0.json) |
| `osdu:wks:reference-data--SurveyToolType:1.0.0` | Standardized Type of survey tool used. The content is generally derived from the ISCWSA Set of Generic Tool Codes. | OPEN | 101 | [SurveyToolType.1.0.0.xlsx](../../Published/SurveyToolType.1.0.0.xlsx) | [SurveyToolType.1.0.0.json](../../Manifests/reference-data/OPEN/SurveyToolType.1.0.0.json) |
| `osdu:wks:reference-data--TargetShape:1.0.0` | Target shape such as point, rectangular etc. | OPEN | 5 | [TargetShape.1.0.0.xlsx](../../Published/TargetShape.1.0.0.xlsx) | [TargetShape.1.0.0.json](../../Manifests/reference-data/OPEN/TargetShape.1.0.0.json) |
| `osdu:wks:reference-data--TargetType:1.0.0` | Target Category such as geological target or driller target | OPEN | 5 | [TargetType.1.0.0.xlsx](../../Published/TargetType.1.0.0.xlsx) | [TargetType.1.0.0.json](../../Manifests/reference-data/OPEN/TargetType.1.0.0.json) |
| `osdu:wks:reference-data--TestSubType:1.0.0` | Well Barrier Element test sub-types for the barrier component being tested such as Subsea BOP interval test, Surface BOP test, Surface PCE test, Wireline PCE Test | FIXED | 48 | [TestSubType.1.0.0.xlsx](../../Published/TestSubType.1.0.0.xlsx) | [TestSubType.1.0.0.json](../../Manifests/reference-data/FIXED/TestSubType.1.0.0.json) |
| `osdu:wks:reference-data--TestType:1.0.0` | Well Barrier Element test types | FIXED | 19 | [TestType.1.0.0.xlsx](../../Published/TestType.1.0.0.xlsx) | [TestType.1.0.0.json](../../Manifests/reference-data/FIXED/TestType.1.0.0.json) |
| `osdu:wks:reference-data--WeatherType:1.0.0` | Type of weather observed during the operations reporting period | FIXED | 16 | [WeatherType.1.0.0.xlsx](../../Published/WeatherType.1.0.0.xlsx) | [WeatherType.1.0.0.json](../../Manifests/reference-data/FIXED/WeatherType.1.0.0.json) |
| `osdu:wks:reference-data--WellActivityPhaseType:1.0.0` | Key milestones in a well activity program, usually based on the major sections of the well or non well-related work | OPEN | 23 | [WellActivityPhaseType.1.0.0.xlsx](../../Published/WellActivityPhaseType.1.0.0.xlsx) | [WellActivityPhaseType.1.0.0.json](../../Manifests/reference-data/OPEN/WellActivityPhaseType.1.0.0.json) |
| `osdu:wks:reference-data--WellActivityProgramType:1.0.0` | Type of well activity program. Can be "Primary" or "Contingency" | OPEN | 2 | [WellActivityProgramType.1.0.0.xlsx](../../Published/WellActivityProgramType.1.0.0.xlsx) | [WellActivityProgramType.1.0.0.json](../../Manifests/reference-data/OPEN/WellActivityProgramType.1.0.0.json) |

[Back to TOC](#TOC)

## Earth Modeling/Reservoir Domain

| kind | Description | Number of Records | Governance<br>Model | Workbook | Manifest |
|---|---|---|---|---|---|
| `osdu:wks:reference-data--BoundaryRelationType:1.0.0` | Describes the boundary relation of a body with surrounding stratigraphic units: unconformable, conformable | FIXED | 2 | [BoundaryRelationType.1.0.0.xlsx](../../Published/BoundaryRelationType.1.0.0.xlsx) | [BoundaryRelationType.1.0.0.json](../../Manifests/reference-data/FIXED/BoundaryRelationType.1.0.0.json) |
| `osdu:wks:reference-data--CellShapeType:1.0.0` | An enumeration of cell shape types based on the RESQML model, i.e. tetrahedral, pyramidal, prism, hexahedral, polyhedral. | FIXED | 5 | [CellShapeType.1.0.0.xlsx](../../Published/CellShapeType.1.0.0.xlsx) | [CellShapeType.1.0.0.json](../../Manifests/reference-data/FIXED/CellShapeType.1.0.0.json) |
| `osdu:wks:reference-data--ChronoStratigraphy:1.0.0` | The reference value type for Chronostratigraphic classification. The purpose of chronostratigraphic classification is to organize systematically the rocks forming the Earth's crust into named units (chronostratigraphic units) that represent intervals of geologic time (geochronologic units) to serve as references in narratives about Earth history including the evolution of life. Records of this kind represent chronostratigraphic units, i.e., bodies of rocks that include all rocks representative of a specific interval of geologic time, and only this time span. Chronostratigraphic units are bounded by isochronous horizons which mark specific moments of geological time. The rank (via data.ChronostratigraphicHierarchy) and relative magnitude of the units in the chronostratigraphic hierarchy are a function of the durations they represent. | OPEN | 179 | [ChronoStratigraphy.1.0.0.xlsx](../../Published/ChronoStratigraphy.1.0.0.xlsx) | [ChronoStratigraphy.1.0.0.json](../../Manifests/reference-data/OPEN/ChronoStratigraphy.1.0.0.json) |
| `osdu:wks:reference-data--ColumnBasedTableType:1.0.0` | ColumnBasedTableType holds the values for the various possible and non exhaustive column based table types. | OPEN | 1 | [ColumnBasedTableType.1.0.0.xlsx](../../Published/ColumnBasedTableType.1.0.0.xlsx) | [ColumnBasedTableType.1.0.0.json](../../Manifests/reference-data/OPEN/ColumnBasedTableType.1.0.0.json) |
| `osdu:wks:reference-data--ColumnShapeType:1.0.0` | An enumeration for column shape types based on RESQML, i.e. triangular, quadrilateral, polygonal. | FIXED | 3 | [ColumnShapeType.1.0.0.xlsx](../../Published/ColumnShapeType.1.0.0.xlsx) | [ColumnShapeType.1.0.0.json](../../Manifests/reference-data/FIXED/ColumnShapeType.1.0.0.json) |
| `osdu:wks:reference-data--ContactRoleType:1.0.0` | Role of the contact within the associated organisation, such as Account owner, Sales Representative, Technical Support, Project Manager, Party Chief, Client Representative, Senior Observer. | OPEN | 7 | [ContactRoleType.1.0.0.xlsx](../../Published/ContactRoleType.1.0.0.xlsx) | [ContactRoleType.1.0.0.json](../../Manifests/reference-data/OPEN/ContactRoleType.1.0.0.json) |
| `osdu:wks:reference-data--DepositionGeometryType:1.0.0` | The Deposition Geometry classification (corresponding to Energistics RESQML DepositionMode). Examples are parallel to top, parallel to bottom, etc. | OPEN | 4 | [DepositionGeometryType.1.0.0.xlsx](../../Published/DepositionGeometryType.1.0.0.xlsx) | [DepositionGeometryType.1.0.0.json](../../Manifests/reference-data/OPEN/DepositionGeometryType.1.0.0.json) |
| `osdu:wks:reference-data--DomainType:1.0.0` | The vertical domain type of models, interpretations and representations. | FIXED | 3 | [DomainType.1.0.0.xlsx](../../Published/DomainType.1.0.0.xlsx) | [DomainType.1.0.0.json](../../Manifests/reference-data/FIXED/DomainType.1.0.0.json) |
| `osdu:wks:reference-data--FacetRole:1.0.0` | The role of a facet, e.g. maximum threshold, cumulative, surface condition, reservoir condition, oil, water, gas, condensate. | OPEN | 34 | [FacetRole.1.0.0.xlsx](../../Published/FacetRole.1.0.0.xlsx) | [FacetRole.1.0.0.json](../../Manifests/reference-data/OPEN/FacetRole.1.0.0.json) |
| `osdu:wks:reference-data--FacetType:1.0.0` | Enumerations of the type of qualifier that applies to a property type to provide additional context about the nature of the property. For example, may include conditions, direction, qualifiers, or statistics. Facets are used in RESQML to provide qualifiers to existing property types, which minimizes the need to create specialized property types. | FIXED | 6 | [FacetType.1.0.0.xlsx](../../Published/FacetType.1.0.0.xlsx) | [FacetType.1.0.0.json](../../Manifests/reference-data/FIXED/FacetType.1.0.0.json) |
| `osdu:wks:reference-data--FaultThrowType:1.0.0` | Classification of the fault throw type, e.g. reverse, normal etc. | FIXED | 6 | [FaultThrowType.1.0.0.xlsx](../../Published/FaultThrowType.1.0.0.xlsx) | [FaultThrowType.1.0.0.json](../../Manifests/reference-data/FIXED/FaultThrowType.1.0.0.json) |
| `osdu:wks:reference-data--FluidPhaseType:1.0.0` | The fluid phase present in a geological unit's pore space. Examples: aquifer, gas cap, oil column, etc. | OPEN | 4 | [FluidPhaseType.1.0.0.xlsx](../../Published/FluidPhaseType.1.0.0.xlsx) | [FluidPhaseType.1.0.0.json](../../Manifests/reference-data/OPEN/FluidPhaseType.1.0.0.json) |
| `osdu:wks:reference-data--IndexableElement:1.0.0` | IndexableElement follows the RESQML pattern. IndexableElement is used to 1) contain geometry within a representation, 2) attach properties to a representation, 3) identify portions of a representation when expressing a representation identity, and 4) construct a sub-representation from an existing representation. Several specialized indexable elements, e.g., hinge node faces, have been included to add higher order geometry to a grid representation, and are not available for other purposes. | FIXED | 27 | [IndexableElement.1.0.0.xlsx](../../Published/IndexableElement.1.0.0.xlsx) | [IndexableElement.1.0.0.json](../../Manifests/reference-data/FIXED/IndexableElement.1.0.0.json) |
| `osdu:wks:reference-data--KDirectionType:1.0.0` | An enumeration for the K-Direction based on the RESQML model, i.e. up, down, not monotonic. | FIXED | 3 | [KDirectionType.1.0.0.xlsx](../../Published/KDirectionType.1.0.0.xlsx) | [KDirectionType.1.0.0.json](../../Manifests/reference-data/FIXED/KDirectionType.1.0.0.json) |
| `osdu:wks:reference-data--LithoStratigraphy:1.0.0` | Name of lithostratigraphy, regionally dependent | OPEN | 5 | [LithoStratigraphy.1.0.0.xlsx](../../Published/LithoStratigraphy.1.0.0.xlsx) | [LithoStratigraphy.1.0.0.json](../../Manifests/reference-data/OPEN/LithoStratigraphy.1.0.0.json) |
| `osdu:wks:reference-data--LithologyType:1.0.0` | Geological description for the type of lithology | OPEN | 185 | [LithologyType.1.0.0.xlsx](../../Published/LithologyType.1.0.0.xlsx) | [LithologyType.1.0.0.json](../../Manifests/reference-data/OPEN/LithologyType.1.0.0.json) |
| `osdu:wks:reference-data--OrderingCriteriaType:1.0.0` | Enumeration used to specify the order of  GeologicalUnitOccurrenceInterpretation | FIXED | 3 | [OrderingCriteriaType.1.0.0.xlsx](../../Published/OrderingCriteriaType.1.0.0.xlsx) | [OrderingCriteriaType.1.0.0.json](../../Manifests/reference-data/FIXED/OrderingCriteriaType.1.0.0.json) |
| `osdu:wks:reference-data--PillarShapeType:1.0.0` | An enumeration of the pillar shape type based on the RESQML model, i.e., vertical, straight, curved. | FIXED | 3 | [PillarShapeType.1.0.0.xlsx](../../Published/PillarShapeType.1.0.0.xlsx) | [PillarShapeType.1.0.0.json](../../Manifests/reference-data/FIXED/PillarShapeType.1.0.0.json) |
| `osdu:wks:reference-data--RepresentationRole:1.0.0` | A specification of the role, which the representation in context plays. For example: 'Map', 'Pick', 'FaultCenterLine', 'InnerRing', 'OuterRing'. | OPEN | 12 | [RepresentationRole.1.0.0.xlsx](../../Published/RepresentationRole.1.0.0.xlsx) | [RepresentationRole.1.0.0.json](../../Manifests/reference-data/OPEN/RepresentationRole.1.0.0.json) |
| `osdu:wks:reference-data--RepresentationType:1.0.0` | The type of a representation, such as, for example, 'PointSet', 'Polyline', 'PolylineSet', 'TriangulatedSurface', etc. | OPEN | 11 | [RepresentationType.1.0.0.xlsx](../../Published/RepresentationType.1.0.0.xlsx) | [RepresentationType.1.0.0.json](../../Manifests/reference-data/OPEN/RepresentationType.1.0.0.json) |
| `osdu:wks:reference-data--SequenceStratigraphicSchemaType:1.0.0` | Sequence Stratigraphic Schema type, valid if Stratigraphic Column role type is set to Chronostratigraphic | FIXED | 6 | [SequenceStratigraphicSchemaType.1.0.0.xlsx](../../Published/SequenceStratigraphicSchemaType.1.0.0.xlsx) | [SequenceStratigraphicSchemaType.1.0.0.json](../../Manifests/reference-data/FIXED/SequenceStratigraphicSchemaType.1.0.0.json) |
| `osdu:wks:reference-data--SequenceStratigraphySurfaceType:1.0.0` | Describes the horizon sequence stratigraphic information (flooding, ravinement, maximum flooding, transgressive). | OPEN | 9 | [SequenceStratigraphySurfaceType.1.0.0.xlsx](../../Published/SequenceStratigraphySurfaceType.1.0.0.xlsx) | [SequenceStratigraphySurfaceType.1.0.0.json](../../Manifests/reference-data/OPEN/SequenceStratigraphySurfaceType.1.0.0.json) |
| `osdu:wks:reference-data--StratigraphicColumnRankUnitType:1.0.0` | The reference value type defining a column position in a stratigraphic chart, which is ordered by rank (increasing from left to right). | OPEN | 15 | [StratigraphicColumnRankUnitType.1.0.0.xlsx](../../Published/StratigraphicColumnRankUnitType.1.0.0.xlsx) | [StratigraphicColumnRankUnitType.1.0.0.json](../../Manifests/reference-data/OPEN/StratigraphicColumnRankUnitType.1.0.0.json) |
| `osdu:wks:reference-data--StratigraphicColumnValidityAreaType:1.0.0` | Member where the custom stratigraphic column has been produced. | FIXED | 5 | [StratigraphicColumnValidityAreaType.1.0.0.xlsx](../../Published/StratigraphicColumnValidityAreaType.1.0.0.xlsx) | [StratigraphicColumnValidityAreaType.1.0.0.json](../../Manifests/reference-data/FIXED/StratigraphicColumnValidityAreaType.1.0.0.json) |
| `osdu:wks:reference-data--StratigraphicRoleType:1.0.0` | The role or type of stratigraphy, e.g., Chronostratigraphic, Lithostratigraphic, etc. | FIXED | 6 | [StratigraphicRoleType.1.0.0.xlsx](../../Published/StratigraphicRoleType.1.0.0.xlsx) | [StratigraphicRoleType.1.0.0.json](../../Manifests/reference-data/FIXED/StratigraphicRoleType.1.0.0.json) |
| `osdu:wks:reference-data--ValueChainStatusType:1.0.0` | Phase of the value chain where the custom stratigraphic column has been produced. | FIXED | 4 | [ValueChainStatusType.1.0.0.xlsx](../../Published/ValueChainStatusType.1.0.0.xlsx) | [ValueChainStatusType.1.0.0.json](../../Manifests/reference-data/FIXED/ValueChainStatusType.1.0.0.json) |


[Back to TOC](#TOC)

## Technical Assurance

M10 adds a new reference value type `TechnicalAssuranceType`, which is referenced from `AbstractCommonResources`, a 
schema fragment included by every OSDU entity type.

| kind                                                    | Description                                                                                                                                                                                                                                                                                                                    | Number of Records | Governance<br>Model | Workbook                                                                               | Manifest                                                                                                    |
|---------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|---------------------|----------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|
| `osdu:wks:reference-data--TechnicalAssuranceType:1.0.0` | The reference value type describing a record's overall suitability and quality for consumption. Certified records should require additional consideration by applications before creating new versions. If this is not populated then one can assume the data has not been evaluated or its quality is unknown (=Unevaluated). | FIXED             | 5                   | [TechnicalAssuranceType.1.0.0.xlsx](../../Published/TechnicalAssuranceType.1.0.0.xlsx) | [TechnicalAssuranceType.1.0.0.json](../../Manifests/reference-data/FIXED/TechnicalAssuranceType.1.0.0.json) |

[Back to TOC](#TOC)

## GeoReferencedImageType

A new reference value type was added to characterize geo-referenced images.


| kind | Description | Number of Records | Governance<br>Model | Workbook | Manifest |
|---|---|---|---|---|---|
| `osdu:wks:reference-data--GeoReferencedImageType:1.0.0` | The type of a geo-referenced image, like, e.g., horizontal map, vertical curtain. | 4 | FIXED | [GeoReferencedImageType.1.0.0.xlsx](../../Published/GeoReferencedImageType.1.0.0.xlsx) | [GeoReferencedImageType.1.0.0.json](../../Manifests/reference-data/FIXED/GeoReferencedImageType.1.0.0.json) |

[Back to TOC](#TOC)

## Reference Value Content Upgrade Support

A `CommitDate` has been added to all reference value manifests. This date-time captures the date and time when the 
particular record was committed, typically the commit date of the associated reference value workbook. The transition 
from Authoring to Published is a separate commit, which is typically what the OSDU platform consumers are interested in.

The M10 snapshot comes with updated manifests, which have the `CommitDate` populated. The `Source` values have also been 
updated where they were still referring Authoring workbooks. All date-times come with UTC offset as registered with 
GitLab. A reference date is published in the [Manifests/README.md](../../Manifests/README.md) under 
> **Youngest commit date for all manifests: yyyy-MM-ddThh:mm:ss+00:00 UTC**

The corresponding value for the final M9 snapshot has been determined to be:
> **Youngest commit date for all manifests: 2021-09-23T10:27:40+00:00 UTC**

## Spelling Mistake Correction

Corrections in the descriptions for `BasinType` and `RiskType`: corrected to "_occurring_" and "_obliquely_".

---

[Back to Milestone Overview](../README.md)