<a name="TOC"></a> Previous Chapter: [12 Rock and Fluid Sample Analysis](12-RockAndFluidSampleAnalysis.md)

[[_TOC_]]

# 13 Earth Modelling (Static Subsurface Model Data, Geomodelling)

Predictive subsurface modelling workflows require handling of a variety of datatypes such as seismic interpretations, well markers, structural models, 2D and 3D grids with associated properties. A common characteristic of these data is that they are related and organized in hierarchies such as stratigraphic columns. Maintaining these relationships is crucial when persisting and exchanging such data.

The *RESQML* standard developed originally by the Energistics consortium provides the data model and API to represent data relevant for the reservoir domain. A mapping of meta-data between RESQML objects  and OSDU types has been established, and is used to generate the manifest for general search indexing.

For optimized handling of RESQML-formatted subsurface data, the Reservoir
DDMS (https://community.opengroup.org/osdu/platform/domain-data-mgmt-services/reservoir) supports ingestion, storage and consumption via ETP and REST APIS, and RESQML files. While ETP is a fast web-socket-based protocol enabling deep query, a REST wrapper API is provided for convenience.

# 13 The RESQML conceptual data model


| **Supported RESQML types and their WPC equivalents**                                                                  |                                                                                                                          |
|-----------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| **RESQML**                                                                                                            | **OSDU**                                                                                                                 |
| obj_CategoricalProperty obj_ContinuousProperty obj_DiscreteProperty                                                   | [GenericProperty](../../E-R/work-product-component/GenericProperty.1.0.0.md)                                             |
| obj_FaultInterpretation                                                                                               | [FaultInterpretation](../../E-R/work-product-component/FaultInterpretation.1.0.0.md)                                     |
| obj_GeneticBoundaryFeature obj_OrganizationFeature                                                                    | [LocalBoundaryFeature](../../E-R/work-product-component/LocalBoundaryFeature.1.0.0.md)                                   |
| obj_GridConnectionSetRepresentation                                                                                   | [GridConnectionSetRepresentation](../../E-R/work-product-component/GridConnectionSetRepresentation.1.0.0.md)             |
| obj_HorizonInterpretation                                                                                             | [HorizonInterpretation](../../E-R/work-product-component/HorizonInterpretation.1.0.0.md)                                 |
| obj_IjkGridRepresentation                                                                                             | [IjkGridRepresentation](../../E-R/work-product-component/IjkGridRepresentation.1.0.0.md)                                 |
| obj_LocalDepth3dCrsobj_LocalTime3dCrs                                                                                 | [LocalModelCompoundCrs](../../E-R/work-product-component/LocalModelCompoundCrs.1.0.0.md)                                 |
| obj_StratigraphicUnitFeature                                                                                          | StratigraphicUnitFeature    does not exist, instead StratigraphicUnitInterpretation?                                     |
| obj_PolylineRepresentation obj_PolylineSetRepresentation obj_PointSetRepresentation obj_TriangulatedSetRepresentation | [GenericRepresentation](../../E-R/work-product-component/GenericRepresentation.1.0.0.md)                                 |
| obj_PropertySet obj_RepresentationSetRepresentation                                                                   | [PersistedCollection](../../E-R/work-product-component/PersistedCollection.1.0.0.md)                                     |
| obj_StratigraphicColumn                                                                                               | [StratigraphicColumn](../../E-R/work-product-component/StratigraphicColumn.1.0.0.md)                                     |
| obj_StratigraphicColumnRankInterpretation                                                                             | [StratigraphicColumnRankInterpretation](../../E-R/work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md) |
| obj_StratigraphicUnitInterpretation                                                                                   | [StratigraphicUnitInterpretation](../../E-R/work-product-component/StratigraphicUnitInterpretation.1.0.0.md)             |
| obj_StringTableLookup                                                                                                 | [ColumnBasedTable](../../E-R/work-product-component/ColumnBasedTable.1.0.0.md)                                           |
| obj_SubRepresentation                                                                                                 | [SubRepresentation](../../E-R/work-product-component/SubRepresentation.1.0.0.md)                                         |
| obj_TimeSeries                                                                                                        | [TimeSeries](../../E-R/work-product-component/TimeSeries.1.0.0.md)                                                       |


[Worked Examples](../../Examples/WorkedExamples/Reservoir%20Data/README.md).

---

## Links

1. Return to the [Beginning of the Chapter](#TOC)
2. Next Chapter: [Appendix A References](90-References.md)
3. Browse to the [Schema Usage Guide Table of Contents](../README.md)

---
