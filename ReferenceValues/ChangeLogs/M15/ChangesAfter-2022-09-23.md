<a name="TOC"></a>

[[_TOC_]]

# Reference Value Changes

## `osdu:wks:reference-data--VerticalMeasurementPath:1.0.1`

|Record `id` | Date | Change Note | Superseded by Name |
|---|---|---|---|
|ELEV|2022-10-28|This value is superseded with another value|TrueVerticalHeight|
|TrueVerticalHeight|2022-10-28|New value with Alias| |

[Back to TOC](#TOC)

## `osdu:wks:reference-data--AdditiveRole:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|Accelerator|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|AcidCrosslinker|2022-10-11|This is a new value|
|AcidEmulsifier|2022-10-11|This is a new value|
|AcidGelling|2022-10-11|This is a new value|
|AcidInhibitor|2022-10-11|This is a new value|
|Activator|2022-10-11|This is a new value|
|AnionicSurfactant|2022-10-11|This is a new value|
|AntiGelation|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Antistatic|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Antifoam|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Antifreeze|2022-10-11|This is a new value|
|AntiGasMigration|2022-10-11|This is a new value|
|AntiGelling|2022-10-11|This is a new value|
|AntiSettling|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Antisludging|2022-10-11|This is a new value|
|Biocide|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list. Bactericide was added as an alias|
|BlendedSurfactant|2022-10-11|This is a new value|
|BondAccelerator|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|BondingAgent|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|BreakerActivator|2022-10-11|This is a new value|
|CationicSurfactant|2022-10-11|This is a new value|
|Chelant|2022-10-11|This is a new value|
|ChemicalTracer|2022-10-11|This is a new value|
|ClayDispersant|2022-10-11|This is a new value|
|ClayStabilizer|2022-10-11|This is a new value|
|ConductivityEnhancer|2022-10-11|This is a new value|
|CorrosionInhibitor|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Crosslinker|2022-10-11|This is a new value|
|Defoamer|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Demulsifier|2022-10-11|This is a new value|
|DiagnosticTracer|2022-10-11|This is a new value|
|Dispersant|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|DivertingAgent|2022-10-11|This is a new value|
|Emulsifier|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|EncapBreaker|2022-10-11|This is a new value|
|Energizer|2022-10-11|This is a new value|
|Expansion|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Extender|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|FinesSuspender|2022-10-11|This is a new value|
|FlowEnhancer|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|FluidLoss|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|FoamingAgent|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|FormationSealer|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|FreeWaterControl|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|FrictionReducer|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|GasHydrateControl|2022-10-11|This is a new value|
|GasMigration|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|GelStabilizer|2022-10-11|This is a new value|
|Gellant|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|HydrogenSulphideScavenger|2022-10-11|This is a new value|
|IronControl|2022-10-11|This is a new value|
|Lightweight|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|LiquidExtender|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|LostCirculation|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Lubricant|2022-10-11|This is a new value|
|MixingFluid|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|ModulusOfElasticityAdjustor|2022-10-11|This is a new value|
|Mothball|2022-10-11|This is a new value|
|MudAcid|2022-10-11|This is a new value|
|NonemulsifiedIronControl|2022-10-11|This is a new value|
|Nonemulsifier|2022-10-11|This is a new value|
|NonionicSurfactant|2022-10-11|This is a new value|
|Odorizer|2022-10-11|This is a new value|
|Oxidizer|2022-10-11|This is a new value|
|OxygenScavenger|2022-10-11|This is a new value|
|pHBuffer|2022-10-11|This is a new value|
|Preflush|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|ProppantTransport|2022-10-11|This is a new value|
|RadioactiveTracer|2022-10-11|This is a new value|
|ResinActivator|2022-10-11|This is a new value|
|Retarder|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|ScaleInhibitor|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|SetEnhancer|2022-10-11|This is a new value|
|Spacer|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Stabilizer|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|SulfideInhibitor|2022-10-11|This is a new value|
|Surfactant|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|SuspendingAgent|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|TemperatureStabilizer|2022-10-11|This is a new value|
|TensileStrengthAdjustor|2022-10-11|This is a new value|
|Thixotropic|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|Viscosifier|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|
|WaxInhibitor|2022-10-11|This is a new value|
|WeightingAgent|2022-10-11|This value is considered an additive role not an additive type and was deprecated on the additive type list and moved to this new list|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--AdditiveType:1.0.1`

|Record `id` | Date | Change Note | Superseded by Name |
|---|---|---|---|
|Accelerator|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Accelerator|
|AntiFoam|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Antifoam|
|Antifoam|2022-10-11|This a duplicate value of Anti Foam. Do not use|AdditiveRole:Antifoam|
|AntiGelation|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:AntiGelation|
|AntiSettling|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Anti-Settling|
|AntiStatic|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:AntiStatic|
|Bactericide|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Biocide|
|BondAccelerator|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:BondAccelerator|
|BondingAgent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:BondingAgent|
|CorrosionInhibitor|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:CorrosionInhibitor|
|Defoamer|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Defoamer|
|Dispenser|2022-10-11|This value can be found on the reference list called additive role. Dispersant is the commonly used terminology|AdditiveRole:Dispersant|
|Dispersant|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Dispersant|
|Emulsifier|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Emulsifier|
|Expansion|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Expansion|
|Extender|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Extender|
|FeSequestering|2022-10-11|This value is a plural of Fe Sequestrant found on the additive role list|AdditiveRole:Chelant|
|FlowEnhancer|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FlowEnhancer|
|FluidLoss|2022-10-11|This value is a type of function and should not be used|AdditiveRole:FluidLoss|
|FoamingAgent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FoamingAgent|
|FormationSealer|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FormationSealer|
|FreeWater|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FreeWaterControl|
|FreeWaterCtrl|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FreeWaterControl|
|FrictionReducing|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FrictionReducer|
|GasMigration|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:GasMigration|
|GellingAgent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Gellant|
|LightWeight|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:LightWeight|
|LiquidExtender|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:LiquidExtender|
|LostCirculation|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:LostCirculation|
|MixingFluid|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:MixingFluid|
|Preflush|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Preflush|
|Retarder|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Retarder|
|RetarderAcc|2022-10-11|-|AdditiveRole:Retarder|
|ScaleInhibitor|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:ScaleInhibitor|
|Spacer|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Spacer|
|Stabilizer|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Stabilizer|
|Strength|2022-10-11|This is not considered a valid entry for an additive type and should not be used. This is not superseded by another value| |
|Surfactant|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Surfactant|
|SuspendingAgent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:SuspendingAgent|
|Thixotropic|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Thixotropic|
|Viscosifier|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Viscosifier|
|WaterSensitive|2022-10-11|This value is not to be used and is superseded|AdditiveRole:ClayStabilizer|
|WeightingAgent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:WeightingAgent|
|Acid|2022-10-11|This is a new value| |
|Alcohol|2022-10-11|This is a new value| |
|Cement|2022-10-11|Added definition| |
|Detergent|2022-10-11|This is a new value| |
|Dye|2022-10-11|Added definition| |
|Fiber|2022-10-11|Added definition| |
|HydrofluoricAcid|2022-10-11|This is a new value| |
|LiquidCo2|2022-10-11|This is a new value| |
|MethaneSulfonicAcid|2022-10-11|This is a new value| |
|Polymer|2022-10-11|This is a new value| |
|Solvent|2022-10-11|This is a new value| |
|Anti%2520Foam|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Antifoam|
|Anti%2520Gelation|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:AntiGelation|
|Anti%2520Settling|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Anti-Settling|
|Ant%2520iStatic|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:AntiStatic|
|Bond%2520Accelerator|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:BondAccelerator|
|Bonding%2520Agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:BondingAgent|
|Corrosion%2520Inhibitor|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:CorrosionInhibitor|
|Fe%2520sequestering|2022-10-11|This value is a plural of Fe Sequestrant found on the additive role list|AdditiveRole:Chelant|
|Flow%2520Enhancer|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FlowEnhancer|
|Fluid%2520Loss|2022-10-11|This value is a type of function and should not be used|AdditiveRole:FluidLoss|
|Foaming%2520Agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FoamingAgent|
|Formation%2520Sealer|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FormationSealer|
|Free%2520Water|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FreeWaterControl|
|Free%2520Water%2520Ctrl|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FreeWaterControl|
|Friction%2520Reducing|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FrictionReducer|
|Gas%2520Migration|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AddditiveRole:GasMigration|
|Gelling%2520Agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Gellant|
|Light%2520Weight|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:LightWeight|
|Liquid%2520Extender|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:LiquidExtender|
|Lost%2520circulation|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:LostCirculation|
|Mixing%2520Fluid|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:MixingFluid|
|Retarder%2520Acc|2022-10-11|-|AdditiveRole:Retarder|
|Scale%2520Inhibitor|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:ScaleInhibitor|
|Suspending%2520Agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:SuspendingAgent|
|Water%2520sensitive|2022-10-11|This value is not to be used and is superseded|AdditiveRole:ClayStabilizer|
|Weighting%2520agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:WeightingAgent|
|Anti%20Foam|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Antifoam|
|Anti%20Settling|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:AntiSettling|
|Anti%20Gelation|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:AntiGelation|
|Ant%20iStatic|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:AntiStatic|
|Bond%20Accelerator|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:BondAccelerator|
|Bonding%20Agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:BondingAgent|
|Corrosion%20Inhibitor|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:CorrosionInhibitor|
|Fe%20sequestering|2022-10-11|This value is a plural of Fe Sequestrant found on the additive role list|AdditiveRole:Chelant|
|Flow%20Enhancer|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FlowEnhancer|
|Fluid%20Loss|2022-10-11|This value is a type of function and should not be used|AdditiveRole:FluidLoss|
|Foaming%20Agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FoamingAgent|
|Formation%20Sealer|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FormationSealer|
|Free%20Water|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FreeWaterControl|
|Free%20Water%20Ctrl|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FreeWaterControl|
|Friction%20Reducing|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:FrictionReducer|
|Gas%20Migration|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:GasMigration|
|Gelling%20Agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Gellant|
|Light%20Weight|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Lightweight|
|Liquid%20Extender|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:LiquidExtender|
|Lost%20circulation|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:LostCirculation|
|Mixing%20Fluid|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:MixingFluid|
|Retarder%20Acc|2022-10-11|-|AdditiveRole:Retarder|
|Scale%20Inhibitor|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:ScaleInhibitor|
|Suspending%20Agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:SuspendingAgent|
|Water%20sensitive|2022-10-11|This value is not to be used and is superseded|AdditiveRole:ClayStabilizer|
|Weighting%20agent|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:WeightingAgent|
|Anti%20Static|2022-10-11|This value is not considered an additive type. This value can be found on the reference list called additive role|AdditiveRole:Antistatic|
|Mutual%20Solvent|2022-10-11|Deprecated code, superceded by MutualSolvent|MutualSolvent|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--LaheeClass:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|DeeperPoolTest|2022-10-18|This value was deprecated on the Drilling Reason Type reference list|
|Development|2022-10-18|-|
|Extension|2022-10-18|-|
|NewFieldWildcat|2022-10-18|-|
|NewPoolWildcat|2022-10-18|-|
|Service|2022-10-18|-|
|ShallowerPoolTest|2022-10-18|-|
|StratigraphicTest|2022-10-18|-|
|Other|2022-10-18|-|
|Outpost|2022-10-18|-|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--WellBusinessIntention:1.0.0`

|Record `id` | Date | Change Note |
|---|---|---|
|Reacquire|2022-10-18|New value|

[Back to TOC](#TOC)