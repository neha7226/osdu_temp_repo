<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# Access Control List
The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractAccessControlList:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; [Authoring Schema AbstractAccessControlList.1.0.0.json](../../Authoring/abstract/AbstractAccessControlList.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractAccessControlList.1.0.0.json](../../Generated/abstract/AbstractAccessControlList.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractAccessControlList.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractAccessControlList.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams Access Control List


## Outgoing Relationships for Access Control List

![Access Control List](../_diagrams/abstract/AbstractAccessControlList.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Access Control List Referenced by other Entities

![Access Control List](../_diagrams/abstract/AbstractAccessControlList.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Access Control List Properties

## Table of Access Control List Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|owners[]| |string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|viewers[]| |string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractAccessControlList.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractSystemProperties.1.0.0](AbstractSystemProperties.1.0.0.md) | acl | one |  |
| Included by | [ConnectedSourceDataJob.1.0.0](../master-data/ConnectedSourceDataJob.1.0.0.md) | OnIngestionAcl | one |  |
| Included by | [ConnectedSourceDataJob.1.1.0](../master-data/ConnectedSourceDataJob.1.1.0.md) | OnIngestionAcl | one |  |
| Included by | [ConnectedSourceDataJob.1.2.0](../master-data/ConnectedSourceDataJob.1.2.0.md) | OnIngestionAcl | one |  |
| Included by | [ConnectedSourceDataJob.1.3.0](../master-data/ConnectedSourceDataJob.1.3.0.md) | OnIngestionAcl | one |  |
| Included by | [GenericDataset.1.0.0](../manifest/GenericDataset.1.0.0.md) | acl | one |  |
| Included by | [GenericMasterData.1.0.0](../manifest/GenericMasterData.1.0.0.md) | acl | one |  |
| Included by | [GenericReferenceData.1.0.0](../manifest/GenericReferenceData.1.0.0.md) | acl | one |  |
| Included by | [GenericWorkProduct.1.0.0](../manifest/GenericWorkProduct.1.0.0.md) | acl | one |  |
| Included by | [GenericWorkProductComponent.1.0.0](../manifest/GenericWorkProductComponent.1.0.0.md) | acl | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._