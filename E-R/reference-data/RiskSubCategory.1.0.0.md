<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# RiskSubCategory [Status: Accepted]
The reference values for risk sub-categories following the WITSML standard.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--RiskSubCategory:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: WITSML
* Reference value type, governance model: FIXED
* Link to &rarr; Proposal workbook [schema-RiskSubCategory.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/reference-data/schema-RiskSubCategory.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema RiskSubCategory.1.0.0.json](../../Authoring/reference-data/RiskSubCategory.1.0.0.json)
* Link to &rarr; [Generated Schema RiskSubCategory.1.0.0.json](../../Generated/reference-data/RiskSubCategory.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource RiskSubCategory.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/RiskSubCategory.1.0.0.json)
* Link to &rarr; [Example Record RiskSubCategory.1.0.0.json](../../Examples/reference-data/RiskSubCategory.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams RiskSubCategory


## Outgoing Relationships for RiskSubCategory

![RiskSubCategory](../_diagrams/reference-data/RiskSubCategory.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## RiskSubCategory Referenced by other Entities

![RiskSubCategory](../_diagrams/reference-data/RiskSubCategory.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# RiskSubCategory Properties

## 1. Table of RiskSubCategory System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-RiskSubCategory:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--RiskSubCategory:e15c375c-c018-53cb-a60f-86be52f2f651|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--RiskSubCategory:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of RiskSubCategory Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of RiskSubCategory Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of RiskSubCategory Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.RiskCategoryID &rarr; [RiskCategory](RiskCategory.1.0.0.md)| |string|optional|Risk Category ID|General category of the described risk such as "Reservoir", "Overburden", "Life of Well", "Drilling", "Completion" or "Opportunity"|<code>^[\w\-\.]+:reference-data\-\-RiskCategory:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [RiskCategory](RiskCategory.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of RiskSubCategory Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for RiskSubCategory.1.0.0

* Status: Published
* Governance: FIXED
* The table below **is truncated** and shows only 100 of total 111 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [RiskSubCategory](../../ReferenceValues/Manifests/reference-data/FIXED/RiskSubCategory.1.0.0.json).

|`id` with prefix `namespace:reference-data--RiskSubCategory:`|Code|Name|Alias|Description|RiskCategoryID|AttributionAuthority|Source|CommitDate|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|AbnormalTendencyChanges|AbnormalTendencyChanges|Abnormal Tendency Changes|-|Use to categorize a risk as related to abnormal tendency changes|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Asset|Asset|Asset|-|Use to categorize a risk as related to asset|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Automotive|Automotive|Automotive|-|Use to categorize a risk as related to automotive|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Ballooning|Ballooning|Ballooning|-|Use to categorize a risk as related to ballooning|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|BedParallel|BedParallel|Bed Parallel|-|Use to categorize a risk as related to bed parallel|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|BhaChangeForDirectional|BhaChangeForDirectional|Bha Change For Directional|-|Use to categorize a risk as related to BHA change for directional|RiskCategory:DirectionalDrilling:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|BitBalling|BitBalling|Bit Balling|-|Use to categorize a risk as related to bit balling|RiskCategory:Bit:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|BitOrBhaPerformance|BitOrBhaPerformance|Bit Or Bha Performance|-|Use to categorize a risk as related to bit or BHA performance|RiskCategory:Bit:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|BlowOutPreventerEvents|BlowOutPreventerEvents|Blow Out Preventer Events|-|Use to categorize a risk as related to blow out preventer events|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Breakouts|Breakouts|Breakouts|-|Use to categorize a risk as related to breakouts|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Broaching|Broaching|Broaching|-|Use to categorize a risk as related to broaching|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|CasingCollapse|CasingCollapse|Casing Collapse|-|Use to categorize a risk as related to casing collapse|RiskCategory:Casing:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|CasingRotationOrReciprocationRequired|CasingRotationOrReciprocationRequired|Casing Rotation Or Reciprocation Required|-|Use to categorize a risk as related to casing rotation or reciprocation required.|RiskCategory:Casing:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|CasingSurgeLosses|CasingSurgeLosses|Casing Surge Losses|-|Use to categorize a risk as related to casing surge losses|RiskCategory:Casing:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|CirculatingEquipmentFailure|CirculatingEquipmentFailure|Circulating Equipment Failure|-|Use to categorize a risk as related to circulating equipment failure|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|CompletionEquipmentFailure|CompletionEquipmentFailure|Completion Equipment Failure|-|Use to categorize a risk as related to completion equipment failure|RiskCategory:Completion:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|CompletionOrCasing|CompletionOrCasing|Completion Or Casing|-|Use to categorize a risk as related to completion or casing|RiskCategory:Completion:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|DelayDueToPoliticalUnrest|DelayDueToPoliticalUnrest|Delay Due To Political Unrest|-|Use to categorize a risk as related to delay due to political unrest|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|DownholeTool|DownholeTool|Downhole Tool|-|Use to categorize a risk as related to downhole tool|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|DrillingGreaterThan1000Feet%2FDay|DrillingGreaterThan1000Feet/Day|Drilling Greater Than 1000 Feet/Day|-|Use to categorize a risk as related to drilling greater than 1000 feet/day|RiskCategory:TimeRelated:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|DrillingGreaterThan2000Feet%2FDay|DrillingGreaterThan2000Feet/Day|Drilling Greater Than 2000 Feet/Day|-|Use to categorize a risk as related to drilling greater than 2000 feet/day|RiskCategory:TimeRelated:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|DrillingLessThan20Feet%2FDay|DrillingLessThan20Feet/Day|Drilling Less Than 20 Feet/Day|-|Use to categorize a risk as related to drilling less than 20 feet/day|RiskCategory:TimeRelated:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|DrillingOptimization|DrillingOptimization|Drilling Optimization|-|Use to categorize a risk as related to drilling optimization|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|EarlyCementSetup|EarlyCementSetup|Early Cement Setup|-|Use to categorize a risk as related to early cement setup|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ElectricalSystemFailure|ElectricalSystemFailure|Electrical System Failure|-|Use to categorize a risk as related to electrical system failure|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Environmental|Environmental|Environmental|-|Use to categorize a risk as related to environmental|RiskCategory:HealthSafetyAndEnvironment:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ExcessiveBitWearOrGauge|ExcessiveBitWearOrGauge|Excessive Bit Wear Or Gauge|-|Use to categorize a risk as related to excessive bit wear or gauge|RiskCategory:Bit:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ExcessiveCasingWearOrCuttings|ExcessiveCasingWearOrCuttings|Excessive Casing Wear Or Cuttings|-|Use to categorize a risk as related to excessive casing wear or cuttings|RiskCategory:Casing:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ExcessiveCirculation|ExcessiveCirculation|Excessive Circulation|-|Use to categorize a risk as related to excessive circulation|RiskCategory:Hydraulics:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ExcessiveDoglegs|ExcessiveDoglegs|Excessive Doglegs|-|Use to categorize a risk as related to excessive doglegs|RiskCategory:DirectionalDrilling:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ExcessiveDrag|ExcessiveDrag|Excessive Drag|-|Use to categorize a risk as related to excessive drag|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ExcessiveFormationDamageOrSkin|ExcessiveFormationDamageOrSkin|Excessive Formation Damage Or Skin|-|Use to categorize a risk as related to excessive formation damage or skin|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ExcessivePipeCementScaling|ExcessivePipeCementScaling|Excessive Pipe Cement Scaling|-|Use to categorize a risk as related to excessive pipe cement scaling|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ExcessiveTimeForBhaMakeup|ExcessiveTimeForBhaMakeup|Excessive Time For Bha Makeup|-|Use to categorize a risk as related to excessive time for BHA makeup|RiskCategory:TimeRelated:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ExcessiveTorque|ExcessiveTorque|Excessive Torque|-|Use to categorize a risk as related to excessive torque|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|FailedInspectionsOrFatigueWear|FailedInspectionsOrFatigueWear|Failed Inspections Or Fatigue Wear|-|Use to categorize a risk as related to failed inspections or fatigue wear|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Fishing|Fishing|Fishing|-|Use to categorize a risk as related to fishing|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|FormationIntegrityTest|FormationIntegrityTest|Formation Integrity Test|-|Use to categorize a risk as related to formation integrity test|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|FractureProblems|FractureProblems|Fracture Problems|-|Use to categorize a risk as related to fracture problems|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|GasHydrates|GasHydrates|Gas Hydrates|-|Use to categorize a risk as related to gas hydrates|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|GasKick|GasKick|Gas Kick|-|Use to categorize a risk as related to the occurrence of a gas kick|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Geosteering|Geosteering|Geosteering|-|Use to categorize a risk as related to geosteering|RiskCategory:DirectionalDrilling:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|GoodHoleCleaningAtHighRop|GoodHoleCleaningAtHighRop|Good Hole Cleaning At High Rop|-|Use to categorize a risk as related to good hole cleaning at high ROP|RiskCategory:Hydraulics:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|GumboProblems|GumboProblems|Gumbo Problems|-|Use to categorize a risk as related to gumbo problems|RiskCategory:Hydraulics:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|HealthSafetyAndEnvironment|HealthSafetyAndEnvironment|Health Safety and Environment|-|Use to categorize a risk as related to HSE|RiskCategory:HealthSafetyAndEnvironment:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|HighEcd-RheologyRelated|HighEcd-RheologyRelated|High Ecd - Rheology Related|-|Use to categorize a risk as related to high ECD rheology related|RiskCategory:Hydraulics:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|HighMudWeight|HighMudWeight|High Mud Weight|-|Use to categorize a risk as related to high mud weight|RiskCategory:Hydraulics:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|HighRateOfBitPenetration|HighRateOfBitPenetration|High Rate Of Bit Penetration|-|Use to categorize a risk as related to high rate of bit penetration|RiskCategory:TimeRelated:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|HoistingEquipmentFailure|HoistingEquipmentFailure|Hoisting Equipment Failure|-|Use to categorize a risk as related to hoisting equipment failure|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|In-FieldReferencing-Ifr-Actions|In-FieldReferencing-Ifr-Actions|In-Field Referencing (Ifr) Actions|-|Use to categorize a risk as related to in-field referencing (IFR) actions|RiskCategory:DirectionalDrilling:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Information|Information|Information|-|Use to categorize a risk as related to information|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|JunkInHole|JunkInHole|Junk In Hole|-|Use to categorize a risk as related to junk in hole|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Leak-OffTest|Leak-OffTest|Leak-Off Test|-|Use to categorize a risk as related to leak-off test|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|LossCirculation|LossCirculation|Loss Circulation|-|Use to categorize a risk as related to the occurrence of lost circulation|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|LostConesOrBrokenCutters|LostConesOrBrokenCutters|Lost Cones Or Broken Cutters|-|Use to categorize a risk as related to lost cones or broken cutters|RiskCategory:Bit:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|LowRateOfBitPenetration|LowRateOfBitPenetration|Low Rate Of Bit Penetration|-|Use to categorize a risk as related to low rate of bit penetration|RiskCategory:Bit:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Mechanical|Mechanical|Mechanical|-|Use to categorize a risk as related to mechanical|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Milling|Milling|Milling|-|Use to categorize a risk as related to milling|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|MiscellaneousRigEquipment|MiscellaneousRigEquipment|Miscellaneous Rig Equipment|-|Use to categorize a risk as related to miscellaneous rig equipment|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|MotorOrRotarySteerableSystemFailure|MotorOrRotarySteerableSystemFailure|Motor Or Rotary Steerable System Failure|-|Use to categorize a risk as related to motor or rotary steerable system failure|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|MudStabilityProblems|MudStabilityProblems|Mud Stability Problems|-|Use to categorize a risk as related to mud stability problems|RiskCategory:Hydraulics:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|MudWeightChange|MudWeightChange|Mud Weight Change|-|Use to categorize a risk as related to mud weight change|RiskCategory:Hydraulics:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|OtherInfluxOrKicks|OtherInfluxOrKicks|Other Influx Or Kicks|-|Use to categorize a risk as related to the occurrence of other influx or kicks|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Other|Other|Other|-|Use to categorize a risk as related to other|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PendingAnalysis|PendingAnalysis|Pending Analysis|-|Use to categorize a risk as related to pending analysis|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PerformingAKill|PerformingAKill|Performing A Kill|-|Use to categorize a risk as related to performing a well kill|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Personnel|Personnel|Personnel|-|Use to categorize a risk as related to personnel|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PitGainOrLoss|PitGainOrLoss|Pit Gain Or Loss|-|Use to categorize a risk as related to pit gain or loss|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PoorHoleCleaning|PoorHoleCleaning|Poor Hole Cleaning|-|Use to categorize a risk as related to the occurrence of poor hole cleaning|RiskCategory:Hydraulics:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PorePressure|PorePressure|Pore Pressure|-|Use to categorize a risk as related to pore pressure|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ReamingGreaterThan2Hours|ReamingGreaterThan2Hours|Reaming Greater Than 2 Hours|-|Use to categorize a risk as related to reaming greater than 2 hours|RiskCategory:TimeRelated:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Resurveying|Resurveying|Resurveying|-|Use to categorize a risk as related to resurveying|RiskCategory:DirectionalDrilling:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|RigCommunications|RigCommunications|Rig Communications|-|Use to categorize a risk as related to rig communications|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|RigMove|RigMove|Rig Move|-|Use to categorize a risk as related to rig move|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|RiserDisconnect|RiserDisconnect|Riser Disconnect|-|Use to categorize a risk as related to riser disconnect|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ShallowGasFlow|ShallowGasFlow|Shallow Gas Flow|-|Use to categorize a risk as related to shallow gas flow|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ShallowWaterInflux|ShallowWaterInflux|Shallow Water Influx|-|Use to categorize a risk as related to the occurrence of a shallow water influx|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ShoeFailures|ShoeFailures|Shoe Failures|-|Use to categorize a risk as related to shoe failures|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Sidetrack|Sidetrack|Sidetrack|-|Use to categorize a risk as related to sidetrack|RiskCategory:DirectionalDrilling:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|SloughingOrPackoffs|SloughingOrPackoffs|Sloughing Or Packoffs|-|Use to categorize a risk as related to sloughing or packoffs|RiskCategory:Hydraulics:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|SpecialAdditivesNeeded|SpecialAdditivesNeeded|Special Additives Needed|-|Use to categorize a risk as related to the need for special fluid additives|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|SqueezeJobs|SqueezeJobs|Squeeze Jobs|-|Use to categorize a risk as related to squeeze jobs|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|StickAndSlip|StickAndSlip|Stick And Slip|-|Use to categorize a risk as related to stick and slip|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Stratigraphy|Stratigraphy|Stratigraphy|-|Use to categorize a risk as related to stratigraphy|RiskCategory:Other:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|StuckCasingOrCompletion|StuckCasingOrCompletion|Stuck Casing Or Completion|-|Use to categorize a risk as related to stuck casing or completion|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|StuckPipe|StuckPipe|Stuck Pipe|-|Use to categorize a risk as related to stuck pipe|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|SurfaceInstrumentationProblems|SurfaceInstrumentationProblems|Surface Instrumentation Problems|-|Use to categorize a risk as related to surface instrumentation problems|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|SurfaceSystem|SurfaceSystem|Surface System|-|Use to categorize a risk as related to surface system|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Tectonics|Tectonics|Tectonics|-|Use to categorize a risk as related to tectonics|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|TightHoleOrOverpull|TightHoleOrOverpull|Tight Hole Or Overpull|-|Use to categorize a risk as related to tight hole or overpull|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Time|Time|Time|-|Use to categorize a risk as related to time|RiskCategory:TimeRelated:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ToolOrEquipmentFailure|ToolOrEquipmentFailure|Tool Or Equipment Failure|-|Use to categorize a risk as related to tool or equipment failure|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|TopdriveFailure|TopdriveFailure|Topdrive Failure|-|Use to categorize a risk as related to topdrive failure|RiskCategory:EquipmentFailure:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|TripsGreaterThan24Hours|TripsGreaterThan24Hours|Trips Greater Than 24 Hours|-|Use to categorize a risk as related to trips greater than 24 hours|RiskCategory:TimeRelated:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|TwistOff|TwistOff|Twist Off|-|Use to categorize a risk as related to twist off|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|UnevenWearOfBha|UnevenWearOfBha|Uneven Wear Of Bha|-|Use to categorize a risk as related to uneven wear of BHA|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|UnevenWearOfDrillstring|UnevenWearOfDrillstring|Uneven Wear Of Drillstring|-|Use to categorize a risk as related to uneven wear of drillstring|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|UnstableZones|UnstableZones|Unstable Zones|-|Use to categorize a risk as related to unstable zones|RiskCategory:WellboreStability:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Vibration-Axial|Vibration-Axial|Vibration - Axial|-|Use to categorize a risk as related to vibration - axial|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Vibration-Torsional|Vibration-Torsional|Vibration - Torsional|-|Use to categorize a risk as related to vibration - torsional|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Vibration-Transverse|Vibration-Transverse|Vibration - Transverse|-|Use to categorize a risk as related to vibration - transverse|RiskCategory:Mechanical:|WITSML|Workbook Published/RiskSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|

**_This reference value type contains more than 100 entries and is therefore truncated. See the reference-values repository for the complete list._**


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# RiskSubCategory.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [Risk.1.0.0](../master-data/Risk.1.0.0.md) | RiskSubCategoryID | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._