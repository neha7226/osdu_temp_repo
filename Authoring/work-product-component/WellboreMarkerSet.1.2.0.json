{
  "x-osdu-review-status": "Accepted",
  "x-osdu-supported-file-formats": [
    "WITSML",
    "RESQML",
    "csv"
  ],
  "$id": "https://schema.osdu.opengroup.org/json/work-product-component/WellboreMarkerSet.1.2.0.json",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "WellboreMarkerSet",
  "description": "Wellbore Markers identify the depth in a wellbore, measured below a reference elevation, at which a person or an automated process identifies a noteworthy observation, which is usually a change in the rock that intersects that wellbore. Formation Marker data includes attributes/properties that put these depths in context. Formation Markers are sometimes known as picks or formation tops.",
  "type": "object",
  "x-osdu-virtual-properties": {
    "data.VirtualProperties.DefaultLocation": {
      "type": "object",
      "priority": [
        {
          "path": "data.SpatialArea"
        },
        {
          "path": "data.SpatialPoint"
        }
      ]
    },
    "data.VirtualProperties.DefaultName": {
      "type": "string",
      "priority": [
        {
          "path": "data.Name"
        }
      ]
    }
  },
  "allOf": [
    {
      "$ref": "../abstract/AbstractWPCGroupType.1.0.0.json"
    },
    {
      "$ref": "../abstract/AbstractWorkProductComponent.1.0.0.json"
    },
    {
      "type": "object",
      "properties": {
        "WellboreID": {
          "type": "string",
          "title": "Wellbore ID",
          "description": "The Wellbore ID, to which the markers in this set belong.",
          "pattern": "^[\\w\\-\\.]+:master-data\\-\\-Wellbore:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "VerticalMeasurement": {
          "$ref": "../abstract/AbstractFacilityVerticalMeasurement.1.0.0.json",
          "title": "Vertical Measurement",
          "description": "References an entry in the Vertical Measurement array for the Wellbore identified by WellboreID, which defines the vertical reference datum for all marker measured depths of the Wellbore Marker Set Markers array."
        },
        "AvailableMarkerProperties": {
          "type": "array",
          "title": "Available Marker Properties",
          "description": "The array of MarkerProperty definitions describing the available properties for this instance of WellboreMarkerSet.",
          "x-osdu-indexing": {
            "type": "flattened"
          },
          "items": {
            "type": "object",
            "title": "MarkerProperty",
            "description": "A set of properties describing a marker property which is available for this instance of a WellboreMarkerSet.",
            "properties": {
              "MarkerPropertyTypeID": {
                "type": "string",
                "title": "Marker Property Type ID",
                "description": "The reference to a marker property type - or if interpreted as CSV columns, the 'well-known column type. It is a relationship to a reference-data--MarkerPropertyType record id.",
                "example": "partition-id:reference-data--MarkerPropertyType:MissingThickness:",
                "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-MarkerPropertyType:[\\w\\-\\.\\:\\%]+:[0-9]*$"
              },
              "MarkerPropertyUnitID": {
                "type": "string",
                "title": "Marker Property Unit ID",
                "description": "Unit of Measure for the marker properties of type MarkerPropertyType.",
                "example": "partition-id:reference-data--UnitOfMeasure:ft:",
                "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-UnitOfMeasure:[\\w\\-\\.\\:\\%]+:[0-9]*$"
              },
              "Name": {
                "type": "string",
                "title": "Name",
                "description": "The name of the marker property (e.g. column in a CSV document) as originally found. If absent The name of the MarkerPropertyType is intended to be used.",
                "example": "MissingThickness"
              }
            }
          }
        },
        "Markers": {
          "type": "array",
          "title": "Markers",
          "description": "The array of marker meta data in this set. Markers are externally identified by data.Markers[].MarkerID, ideally a UUID. Older versions of the WellboreMarkerSet schema do not have this identifier. In this case, the string-converted array index is used as MarkerID. The the first index is \"0\".",
          "x-osdu-indexing": {
            "type": "nested"
          },
          "items": {
            "type": "object",
            "title": "Markers",
            "description": "The array of marker meta data in this set.",
            "properties": {
              "MarkerName": {
                "type": "string",
                "description": "Name of the Marker"
              },
              "MarkerID": {
                "type": "string",
                "title": "Marker ID",
                "description": "A unique identifier of the marker in the list of data.Markers[], ideally a UUID. If unpopulated, the string-converted element index number is used. The first index is \"0\"."
              },
              "InterpretationID": {
                "type": "string",
                "title": "Interpretation ID",
                "description": "The optional relationship to a HorizonInterpretation, GeobodyBoundaryInterpretation or FaultInterpretation.",
                "pattern": "^[\\w\\-\\.]+:(work-product-component\\-\\-HorizonInterpretation|work-product-component\\-\\-FaultInterpretation|work-product-component\\-\\-GeobodyBoundaryInterpretation):[\\w\\-\\.\\:\\%]+:[0-9]*$"
              },
              "MarkerMeasuredDepth": {
                "type": "number",
                "description": "The depth at which the Marker was noted.",
                "x-osdu-frame-of-reference": "UOM:length"
              },
              "MarkerSubSeaVerticalDepth": {
                "type": "number",
                "description": "The Marker's TVD converted to a Sub-Sea Vertical depth, i.e., below Mean Sea Level. Note that TVD values above MSL are negative. This is the same as true vertical depth referenced to the vertical CRS \u201cMSL depth\u201d.",
                "x-osdu-frame-of-reference": "UOM:length"
              },
              "MarkerDate": {
                "type": "string",
                "description": "Timestamp of the date and time when the when the Marker was interpreted.",
                "format": "date-time",
                "x-osdu-frame-of-reference": "DateTime"
              },
              "MarkerObservationNumber": {
                "type": "number",
                "description": "Any observation number that distinguishes a Marker observation from others with same Marker name, date."
              },
              "MarkerInterpreter": {
                "type": "string",
                "description": "The name of the Marker interpreter (could be a person or vendor)."
              },
              "MarkerTypeID": {
                "type": "string",
                "description": "Marker Type Reference Type. Possible values - Biostratigraphy, Lithostratigraphy, seismic, depth of well, sequence, flow unit",
                "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-MarkerType:[\\w\\-\\.\\:\\%]+:[0-9]*$"
              },
              "FeatureTypeID": {
                "type": "string",
                "description": "Feature Type Reference Type. Possible values - Base, top, fault, salt, reef, sea floor",
                "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-FeatureType:[\\w\\-\\.\\:\\%]+:[0-9]*$"
              },
              "FeatureName": {
                "type": "string",
                "description": "Name of the feature the marker is characterizing"
              },
              "PositiveVerticalDelta": {
                "type": "number",
                "description": "The distance vertically above the Marker position that marks the limit of the high confidence range for the Marker pick.",
                "x-osdu-frame-of-reference": "UOM:length"
              },
              "NegativeVerticalDelta": {
                "type": "number",
                "description": "The distance vertically below the Marker position that marks the limit of the high confidence range for the Marker pick.",
                "x-osdu-frame-of-reference": "UOM:length"
              },
              "SurfaceDipAngle": {
                "type": "number",
                "description": "Dip angle for the Wellbore Marker.",
                "x-osdu-frame-of-reference": "UOM:plane angle"
              },
              "SurfaceDipAzimuth": {
                "type": "number",
                "description": "Dip azimuth for the Wellbore Marker.",
                "x-osdu-frame-of-reference": "UOM:plane angle"
              },
              "Missing": {
                "type": "string"
              },
              "GeologicalAge": {
                "type": "string",
                "description": "Associated geological age",
                "x-osdu-frame-of-reference": "UOM:geologic time"
              }
            }
          }
        },
        "StratigraphicColumnID": {
          "type": "string",
          "title": "Stratigraphic Column ID",
          "description": "The optional reference to a stratigraphic column (referring to multiple StratigraphicColumnRankInterpretation) providing the stratigraphic framework for the WellboreMarkerSet. It demonstrates the intent to describe complex, potentially overlapping stratigraphic intervals. Only one of the properties StratigraphicColumnID or StratigraphicColumnRankInterpretationID should be populated.",
          "pattern": "^[\\w\\-\\.]+:work-product-component\\-\\-StratigraphicColumn:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "StratigraphicColumnRankInterpretationID": {
          "type": "string",
          "title": "Stratigraphic Column Rank ID",
          "description": "The optional reference to a StratigraphicColumnRankInterpretation. It expresses the intent of a stratigraphic framework with non-overlapping intervals. Only one of the properties StratigraphicColumnID or StratigraphicColumnRankInterpretationID should be populated.",
          "pattern": "^[\\w\\-\\.]+:work-product-component\\-\\-StratigraphicColumnRankInterpretation:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        }
      }
    }
  ]
}