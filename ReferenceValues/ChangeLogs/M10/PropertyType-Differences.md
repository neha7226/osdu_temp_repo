<a name="TOC"></a>

[[_TOC_]]
# PropertyType Content Changes
## Value Changes

Total number of changes 1444 in total 3629 PropertyType records.

| Name | ParentName | Changed from | Changed to | ID |
|---|---|---|---|---|
| absolute abundance|amount|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6ac45b26-d828-432d-8e33-f419f06b3274 |
| accident type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:65d30541-7215-4590-9f23-72d77df42b96 |
| accuracy|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a1f14782-3c18-42a0-8889-2c99a0430e27 |
| accuracy description|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a4adb135-8c1f-46c9-a152-d422e6c1eaf4 |
| acid treatment count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:89801be1-8f0f-4d2f-880b-8438a45b472e |
| acoustic amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8e13e4d8-8a54-4329-b213-e2c23495709f |
| acoustic impedance ratio|ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cc65706e-41a5-4336-95af-2dbdd5357151 |
| acoustic quality indicator|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f91a3736-7740-4db9-96bd-b7258054ada6 |
| acquisition enable flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ecc36a38-2bbe-4a44-a547-d7268bb3469c |
| acquisition file number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8789e825-c763-4896-91bb-02bfe726ddba |
| acquisition shot number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:29b076e9-428e-481c-9818-d631808c19d5 |
| acquisition type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2a725af4-ae11-47fc-8cf7-e7206c5b9ec1 |
| active|explicit index|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:17cdc15c-6b69-4c88-afe0-cc5499562d9f |
| activity code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6845eee4-22df-4703-864b-e01cf59a1e38 |
| activity name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2d2f82c7-5cea-48a4-a06f-f78cd9a9a646 |
| activity number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8d6a0eb8-081c-4b41-9d44-58e496620969 |
| activity type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f333a868-321f-4e14-b18a-74ccc952fddc |
| address|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:530dbc61-8cc2-4909-9241-6674195beb55 |
| adjustable choke flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:490276b7-22a3-45eb-b9fc-0121cc72f741 |
| ads blocking factor|byte count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c28a47b7-6daa-42d4-bb7e-53c71a96d91a |
| adsorption|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4108b980-694e-40e8-bfd2-8ab0cd73242b |
| affine coeff a|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:46cd4790-ed3c-41c3-85e8-0ffbb56fe8d5 |
| affine coeff b|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ffa19a04-b624-41b9-83dd-eeab20241780 |
| affine coeff c|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0b7ac5fa-9ff8-4d4c-b6ad-de5dc3ecd131 |
| affine coeff d|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7bbf9927-a3fd-494c-adf1-33f291a744b2 |
| affine coeff e|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e82a96d9-6a8b-4ad4-b21a-91b723adf3b1 |
| affine coeff f|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c82f258d-b84e-4e7d-a8e4-e7733aed07ac |
| agency lease number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b0e72062-d9da-4f9e-acfe-89f5e8320ef3 |
| agreement interest|proportion|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:870159ff-302c-4757-af1c-cc88fdd3ec69 |
| agreement status|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d0add45b-ef55-4853-9095-b5d650ad9415 |
| alkhalifah eta|anisotropy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9214973b-502a-40df-a9a5-30bf39920fe5 |
| allowed position|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:df59e9e5-8009-41c0-9bc7-3392f82640e4 |
| alphacode|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:54fe975b-0e76-4dd9-8ca7-87342d788f7f |
| alternate phone number|phone number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:44abd1b3-aa7b-47c7-a4f3-1eca690c8160 |
| aluminum yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:07cb2e9c-4a77-49bd-8b83-73771bef8af1 |
| american aircraft regulation|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4a76774a-9d4f-4d32-bc95-101c1c005c00 |
| amount|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5239c8a9-e6d1-4e15-978d-33f6a53e1d06 |
| amplitude|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:43d4382c-4414-4ff1-b68e-754ac5904ca7 |
| amplitude correction|correction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b235e39f-9c96-4762-9ab6-b74f3438b8ed |
| amplitude difference|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6c072820-8a95-4df5-9fca-b2fbdd63905a |
| amplitude estimate|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:68e937ea-58e1-4501-936f-60f97c3f9d62 |
| amplitude kurtosis|kurtosis|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d29ac69a-62db-41a8-9f09-6a3613a4f242 |
| amplitude phase complex|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bf249647-2512-4974-87b3-5351684a6648 |
| amplitude skewness|statistical skewness|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8c949412-3472-4c93-b09b-5cbc2ff29021 |
| amplitude standard deviation|standard deviation|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8525cb5b-ac47-4705-9dbe-dfd095016641 |
| analytic trace|acoustic amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3cf4985d-6ccd-4510-8a81-c4398b57e021 |
| anisotropy|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9e6136ba-6c16-4b9f-9ccb-41f2a6bc5c73 |
| annotation|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e0e640e5-f106-4bc2-96c1-2290e910e06a |
| annual payment|money|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:90983b25-d7ae-49c9-96bc-40783ead75f8 |
| antenna quality factor|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6902f9e4-82c5-42be-a99b-26c6f8c15154 |
| anti collision method|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:eab7d183-9d04-463a-b7b4-05fb97e3dea9 |
| api code|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c20bb879-ff7b-4257-b0f5-0f01520fad0b |
| api country code|api code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4a95f258-ae2b-41f1-9941-54a641d2c08a |
| api design|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ec8deb12-0216-4c5e-b3a0-01b82f2f69f0 |
| api grade|grade|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b710b5d7-6b80-4e0f-b081-2feeac554d71 |
| api gravity|density||New PropertyType item: 4836a794-1d21-44e3-9363-9d1e5c15adde|{{NAMESPACE}}:reference-data--PropertyType:4836a794-1d21-44e3-9363-9d1e5c15adde |
| api neutron|neutron porosity||New PropertyType item: 0b80063d-f281-431e-9422-ffbafeb1a81f|{{NAMESPACE}}:reference-data--PropertyType:0b80063d-f281-431e-9422-ffbafeb1a81f |
| api pin conn type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7899e60b-039a-4a6e-a815-abd6351c9540 |
| api range|length|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ee3830b6-c03d-467d-baa3-ce5e2fd4b9d2 |
| api reference|publication title|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6a5c1cbc-c915-4c5a-981b-925342cb3d40 |
| api well number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:13449169-b24f-4425-8771-5c68cb993f50 |
| apparent seismic polarity|analytic trace|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:72138fd4-7047-4c0b-b6ca-113cf5f2c548 |
| arc type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8c591321-e834-402b-8e8b-89bd486f43f5 |
| archie porosity constant|exponent|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:720a280a-346b-4e28-b979-ede210d5ffc2 |
| archie porosity exponent|exponent|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e7e8dc94-ee53-4345-b795-29da9058b4c9 |
| area per area|ratio|UnitQuantity:dimensionless|UnitQuantity:area per area|{{NAMESPACE}}:reference-data--PropertyType:b2caacc7-7358-4711-9bde-d1f3acb6edb2 |
| array code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:24312083-2d65-4209-9b3c-d746844b884c |
| array induction quality indicator|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:080af307-25e6-4b70-be23-b8e245d9e29e |
| article title|publication title|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:943a586c-4cd7-458d-a3f8-f46fce9f403c |
| asap depth parameter|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:efef6a9f-c4d5-44cd-97a9-1a841d118c0d |
| asap parameter|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ed9844f7-fe64-49c8-9bfb-783ebda1404c |
| aspect ratio|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b7cacbb8-9fcc-4752-94a0-d04f2e395792 |
| atomic number|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ebe56693-216a-458c-b88a-f4e75eb9bf6e |
| atomic weight|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:14641822-a167-4daf-983a-f46aac339491 |
| attribute name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:04cd40de-d390-4d2c-8d59-a673ba333e37 |
| author name|person name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:136b103d-4b12-4e8f-97a5-d713f9e63d90 |
| authorization for expenditure|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a08427f1-9e54-4585-bdc3-f384dbe51f26 |
| auto exit|processing option|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c6712bdc-cbe4-4b5e-8fcb-a0c904a5ad74 |
| auto start|processing option|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d0daa9ef-4fbe-4c8e-86d4-3dacd1049677 |
| availability|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:11440db4-b89f-4eff-b3ee-824293da889b |
| avg amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:612acc11-e043-4369-bd86-4aaa12beca94 |
| avg compressional shear velocity ratio|compressional shear velocity ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:209778f0-0614-48f3-bffe-c067a32c7082 |
| avg magnitude|magnitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ae6d3ee5-ce13-4cee-84c6-ac6fefcec332 |
| avg peak amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b556b2aa-30c3-482a-9656-c9d50a1f7938 |
| avg q ratio|q ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:eff42646-6188-4f7d-bd24-e11ed99391ec |
| avg qs ratio|qs ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:629a24cc-33fb-4cfc-b1c8-b85e46c44fd9 |
| avg sequence trend|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:76804363-88b6-4152-a2fd-78a1cd6d1c92 |
| avg shear compressional velocity ratio|shear compressional velocity ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:122ec9e1-c3ae-41e8-a4c3-1f844c5a84c4 |
| avg trough amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:12150ac1-15a4-46e9-b2f8-ba2e8e29d973 |
| axis first value|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fd67a5d3-cd99-4815-9fbc-e5c1a120192e |
| axis index code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d82338d5-ed75-46ab-aa4f-129038859d17 |
| axis spacing|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b40beb6b-2941-4a0d-b642-84fbc587bb36 |
| axis unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d1ee7a8f-8a61-4c1e-80ef-a8701a868b70 |
| azimuth reference type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b2dca3a7-4a0b-40bb-a92d-5d75dae2e6b1 |
| azimuth unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:241fcb7f-c338-4a77-98f5-eccbd79d506f |
| background pressure trend|pressure|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8527982b-804d-4576-bc85-e2178d1feb66 |
| bad hole flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9e40e1dd-daf1-4505-ae0d-f267c10770b2 |
| bale kappa|anisotropy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c14749b8-8553-4fc6-aa87-62df80959f49 |
| bale xi|anisotropy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6d330b0c-8cda-4731-9edf-bae5c34d61bb |
| barium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:90d84d9a-6dbe-438f-b032-6bbfe2bdacce |
| base fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dd1d62f4-9433-4e0b-b23f-894b006cf93c |
| base for spacing|processing option|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:93d99eb9-7119-4e66-b7c0-51034ba12f0a |
| baselap type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:59faa4a8-f47e-4170-b111-2748a47568f7 |
| battery code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5104c313-2cc1-467b-8ccb-23779ad33331 |
| battery type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0aa92c8f-c903-4279-a7f8-724fb0ae3c2c |
| bearing type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f1beb9de-e883-4ba1-a32a-f0c024097c69 |
| beaufort scale number|wind velocity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d59e8810-fdcf-451e-91b6-b274e3cddfdf |
| bed character|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:49b78f9d-8d63-42d3-8c71-fa3640a4618d |
| bed set type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:12323925-a0ee-4a80-878d-cbffd378c7f7 |
| bed type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b1fbddf9-9eee-48b3-b03d-5ae58f883c59 |
| begin element|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b3fa729f-6f17-4751-8bb3-8fffdc1a75c1 |
| bha number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e29af537-2a54-408c-93d9-90a8ea7b3c46 |
| bha run number|run number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:532ce1b7-6fb6-4052-b7af-6444cfb2e0e8 |
| bid amount|money|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a16a214f-6635-40e9-9032-6fa0fe9f6797 |
| bid number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b363c601-4543-481e-a1e6-303b6ab19990 |
| biological name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2d03e17a-ff43-40ad-9fb4-f0f3e140eaef |
| biot coefficient|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6033bbd3-b329-4600-9105-d42f9d5b5a9a |
| bioturbation type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7d6b9f64-e639-4322-8c5a-e087a6ecead3 |
| bit bladed flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fbaf0017-1267-4bf9-ab14-e32d49a30e09 |
| bit number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9d845895-8925-45d7-8bc6-e681daa7aa68 |
| bit profile|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3457ba8b-034d-4cc1-8ee4-36a6890dcd8f |
| bit revolution count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:de5a63e3-6a85-42ae-9a16-2e98a73c0155 |
| bit run number|run number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b98aef01-3627-4986-b5a7-594aaeb068b3 |
| blade material|material type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fdd6384c-f351-4710-b2ce-fe4e5f1afeda |
| blocking method type|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6d42cffb-5dc9-486a-9464-b86321aca3f2 |
| boat type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ce5a68e9-8631-4ec3-a052-6a5382888e1f |
| bob number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f7f267ee-1947-47b8-a05d-ee480ddba001 |
| body material|material code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8deb2d12-6f36-449d-bedc-cd0afe688398 |
| bore count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6d27b5f5-b183-4f20-a0b0-089d76346eb9 |
| bored flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d11742df-465e-4645-8409-c4e654da2cba |
| borehole status|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4c198954-4250-41ab-a85c-a89d926e15bd |
| boron yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4f07ba78-3ede-481a-9794-961b3343315f |
| bottom dip number|dip number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c7f9db60-ce59-4960-9247-8f36a78a8000 |
| bottom discontinuity type|discontinuity type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3b34f562-e55c-4254-bf69-43b020c0cc4e |
| bottom location|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:16b63578-9e8c-4786-b607-42226b5e87d9 |
| bottom value|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:11594d37-0fef-4520-a127-1f3c1ed27d7e |
| bottomhole end type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0228e1f3-de30-4237-acd3-417f5536946f |
| boundary file|file name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a5d6df1c-fc77-49c5-9795-fb388ec82612 |
| boundary type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8da16eac-9e96-4453-9e54-efe2e4068ea0 |
| box number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e5fe7b8d-8bd6-4318-9e4c-a3b28e378ec1 |
| bulk addition mode|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:02fc5a14-e4db-4391-b25c-59dcc99706d9 |
| burner code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1ccda15d-d721-4ed4-9101-62ba9da2af8a |
| burner head count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e9914150-bf37-49f6-9638-efcc8d8f8d9b |
| burner type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f8ce1279-8ddf-4fcc-8114-f805ff2bb055 |
| business associate job title|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:79c6d1a9-2754-400d-b400-1f1c59e33913 |
| business line|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ca4ccfc9-90f5-45cd-86c4-ff42988c4ef1 |
| business line code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a87a1d69-b95d-4518-a35b-47da8100227a |
| business region type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:238e9b30-9149-4de3-bbd0-59c3e58e6b24 |
| byte count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5d0639e2-38bc-4a36-ba7c-d835d23ce125 |
| c7plus plus molecular weight|molecular weight|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7144183e-b52c-4f28-be05-148c01698971 |
| c7plus specific gravity|specific gravity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a49edd76-0a73-48eb-9021-33ee39898933 |
| cable type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:28beeb57-1deb-4283-8235-c8cccbe82bfb |
| calcium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2e41021d-9f6e-4031-b5ee-88e314507d69 |
| calibration coefficient code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d9f3920a-295e-49d0-b8be-ce17654be490 |
| calibrator type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d640b922-d549-40c2-82bb-ec95e6700e53 |
| capillary pressure function|function name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b7d45f36-0b35-4846-b5b7-28567f354c4a |
| capillary pressure function coeff|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7b902bcb-8e58-46f8-bc06-d4f425ee28da |
| carbon yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:62f73ac7-ddf3-4628-9347-695f46863b83 |
| cardinality|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ad19a5d2-875c-47e7-a94b-b3574dfffc7c |
| casing collar locator|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:25d2974c-a3a7-4001-b19a-a3ca58c5930a |
| casing grade|steel grade|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:12dde853-512b-4563-848c-376157613e78 |
| casing rotation friction coefficient|friction coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:56a8e0d3-9613-4172-a4bd-a3335844fe3a |
| casing sliding friction coefficient|friction coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1825f39a-ba4b-455d-bfe6-3a1da6f067d8 |
| casing translation friction coefficient|friction coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a9a7da95-db9b-4e5b-990a-fff0532fb931 |
| casing type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dffd027f-4749-411b-bc4a-a6521b3c6a66 |
| catalog code|entity code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d28cbc8a-ef25-4d7e-925e-33c98946a77d |
| cbl quality indicator|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e541d651-b2d6-4cf4-ac6d-abe035eb0b38 |
| ccp trajectory|trajectory|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d3175c7d-054d-4009-a3b2-9a2e3320803c |
| cell count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:81124a28-9d56-40e7-b550-6ad7564357e3 |
| cement additive type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7b206433-07ee-4829-b366-232b3291f5c7 |
| cement bond amplitude|acoustic amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:033df752-1071-431c-9437-0c77134e5d84 |
| cement bond index|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cdd7c3ab-2368-4c8a-869f-3aba1028af0a |
| cement retarder type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0d36b036-83fe-4ad6-8145-359bc1325879 |
| cement type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:984b5b90-e6c0-4178-ae31-2b287a3acae9 |
| cement unit type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1e73b70b-13a5-4ba0-89ac-638ee7c7695e |
| cementation factor|exponent|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f67830a3-c132-412b-8ae6-c2fb4a0441f5 |
| cementitious flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ab76b730-3704-4474-9c00-9babd039b1c5 |
| central line x alignment|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:db81229e-c90a-4f99-bd1e-db9b4c68f738 |
| centralizer code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:306e8f45-e306-460e-bc49-3fa4a8cb5236 |
| centralizer count per joint|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:627c7dfd-cc92-46f7-8615-1a7480a6157d |
| centralizer test type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:03bc5a79-cb95-4418-830f-030ac2e1e4a8 |
| centralizer type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6d1afe37-6915-42a1-b4f9-3440d18d7ff9 |
| chemical formula|formula|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ce0c5319-fc76-4697-b110-2f047f72f2e7 |
| chemical function|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:65d690cf-1771-4a96-ab6a-34b3d1241484 |
| chemical product type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:187d2dae-3477-4356-93c0-19fca0a38639 |
| chemistry type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1236ad36-de32-42c4-a489-949c1a50a5d8 |
| chi square|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e56d1d15-c9a3-40e7-99ef-1f209b9680cb |
| chlorine yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c9b530c8-d91d-43b7-a3c7-bb44bc16505f |
| choke manifold code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d957293f-d1b0-4dee-9484-21f87788ce33 |
| choke manifold type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9cf6d090-1fb6-4aa0-8f52-9ae3e2764d60 |
| chromium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:944a81dd-deb0-41b8-9f4a-c83b87e8bfc3 |
| chromiumnickel yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1f81dbeb-3651-4cc0-be50-3f81d09236dd |
| classification|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:30e9db74-118b-4143-b2c3-b16a41ae2ee4 |
| classification group code|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1f4dab6a-f62f-4110-91eb-16ea511f0d4f |
| classification index|explicit index|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2bcbe7d8-cd41-4758-a88f-242781e7f495 |
| classification method code|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0b520a0b-e324-4efe-bc04-59dcaca96b9c |
| classification method type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6b6a8e1d-ed3a-40f3-8e9e-fff01e1868a2 |
| classification scope|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cfe6c055-8bb1-48df-90a2-583dfc31c689 |
| client|organization code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fb5254a6-54d0-4434-b9d5-937cb97b6e12 |
| client representative|person name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:eceb6f94-c475-466e-a01b-b5cbb5d25040 |
| closure|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f6758f67-b578-43e0-bb9f-9a4228e2ab7e |
| co2 mole fraction|mole fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c2233531-3774-484d-a10e-ee3bffa66112 |
| cobalt yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1b36839e-c319-4534-b83b-4894e75d3213 |
| code|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5f78f66a-ed1b-4827-a868-beb989febb31 |
| code in catalog|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:62a31b6c-989b-4183-85c2-6c03d30037fa |
| coefficient|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4fb32a5e-3814-4051-a140-8d62a08e0c17 |
| coherence|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f8ad43af-c879-4797-8b61-b4518ae3fe0e |
| coil tubing section|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dc7057d3-5b2f-40b5-b6a5-e6ff31011d0b |
| color|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8f85fd63-ae4c-4008-bbbe-259a87bdbdde |
| color distribution|color|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c67f21ab-a90f-45f9-b5eb-09380f75b7ee |
| color fluorescent light|color|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:95962574-2a4f-4e05-87c2-81de883366b6 |
| color intensity|color|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fa7126d1-7c8e-4807-8d28-0827c6935a2e |
| color map|color|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3490fa6e-3f63-499e-a564-7ea2ec11c84b |
| color normal light|color|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2df18b00-ba3e-455a-87bc-c945870dfd43 |
| column name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b96e3312-5c22-47fa-9a99-403047531655 |
| companion data|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:399aab72-f8dd-4463-bbaa-5c1a9cc38b70 |
| company|organization code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:53958a10-5329-4cf4-bfb0-6990e9bd9d64 |
| company lease number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3d747862-4ca6-41c0-aecd-70a4a264ab1c |
| company type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4a61caef-e2b2-46f7-b7fe-19e60502fc9e |
| compartment count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:563d5f0c-a662-4711-a0d1-54108cb676d1 |
| completion count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:79ab80ba-4be3-4a38-a062-24a2c6de2b32 |
| completion skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7f997a06-cb5f-4673-9be3-213be3534e82 |
| completion type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fe85ee8b-b3f1-4a2d-a2a6-009d5e7206d8 |
| component 1 name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0f375395-9a4b-42eb-bcf8-790f6dfcf674 |
| component 2 name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:14b0e6a9-6130-4e31-8220-93f29a7c39a1 |
| component 3 name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8a71fdcf-7c8e-4695-b2fe-540bb4066728 |
| component 4 name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9c66dac5-4db4-4b45-ad8c-2d49cf7065f1 |
| compressional impedance reflectivity|elastic impedance reflectivity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c3e79b83-bc29-4b82-bb9b-b0e3c0d2b3b3 |
| computation mask|data mask|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:baaf3f63-e59d-480b-8b6b-444cc56c1a14 |
| computation type|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6461a28d-3b1f-42ca-a5e8-49c9cc3f1a4e |
| conductive bed proportion|proportion|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cf09c776-bba0-47ac-8eaf-03144ee1ff56 |
| conductive inclusion contrast|conductivity contrast|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:36559329-a1c9-4c8f-92d9-3fecbd6ac994 |
| conductive inclusion proportion|inclusion proportion|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0a63debd-e674-418b-b1cb-bb0c61d87f11 |
| confidence factor|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7c8d864b-e4d0-4a6c-a0c3-8bda0c653eb0 |
| confidentiality level|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f3167ac0-5686-43ab-983f-8cfb0d574c13 |
| conn 1 box flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:de987dc1-f4cb-48ee-aaf6-237fb3659867 |
| conn 1 thread type|thread type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7c106026-3543-40b3-bcb5-85a8451bb788 |
| conn 1 type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5f8603e5-c6cf-46f5-b066-ba9c3e078fff |
| conn 2 box flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8b45c60f-d83a-41b0-bdd0-4e21c2b6b1f6 |
| conn 2 thread type|thread type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6948bcaf-332c-4316-ab41-64bf8fac110f |
| conn 2 type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1645ce99-8eb9-4d67-b7d5-3adffb2656e0 |
| conn material|material code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:806a85b9-1147-4be1-ad44-0c0ecd47350f |
| connectedness coefficient|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9bf147dc-2ad2-4b39-8d66-f045d1dfc8b1 |
| connector type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3454bf6c-b0db-4061-a632-3791d0a548fb |
| consistency index|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:47a34b3d-91dc-47a8-a8bf-2ffb958fa5e8 |
| constraint code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:17a8b0b5-64bb-473c-8e88-e7168dc47b24 |
| constraint object|object name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9461cde3-1cf5-4bd8-9ee3-1d11b9fec374 |
| constraint type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d2793447-6b54-4fe2-9b55-180be81b6657 |
| construct|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:036b2d13-678c-423e-8cc5-4d304209c196 |
| contact symbol|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6705dba1-de91-4772-a9f7-0ee214dd1a9f |
| contact throw sense|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1e88e0d2-c1d5-4a2a-823b-5c83a6898e9e |
| contact type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1d227cab-5934-45cb-b3fc-397aee63c4de |
| container code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:13ea77fa-cd3e-4212-bbdf-97a8d9fcb73e |
| continuous|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7819434a-b7d8-4d29-a818-e0e00d9aa965 |
| contract number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6b06e3d6-ad76-4564-ae1f-a63b2601da8a |
| contract status|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5db26c22-2baf-43c9-9f89-0284e15a325f |
| contractor|organization code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7aeeef2b-5a0e-4f2b-8e31-aef1aee61c13 |
| contractor representative|person name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ea75405b-20b7-4be2-a3ef-e27ac31491ed |
| coord sys transform code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:df305e1c-46eb-4e62-9fda-8de8a4a9e204 |
| coordinate system code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:45e37ad1-d0c6-49a0-9047-1d0d02cc6f80 |
| coordinate system name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8b7ed03a-eb1a-41c6-8c24-05f178ed2405 |
| copper yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b1a703f5-67c8-4935-82af-321755ed3d04 |
| copy number|sequence number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5ab670c6-89f1-405a-a9cd-8979507b81fd |
| core box count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:21b696f5-9614-4091-91b9-6c44341c7f91 |
| core preparation method|preparation method|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b65d3136-aae4-4cb7-b73f-409c812c1614 |
| core preservation method|preservation method|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:35eafe57-5549-4714-954e-5d8aecac2b65 |
| core type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2bc17d58-7ee3-4d0a-b6b0-f31890a53df6 |
| coring fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f2dac20a-6fef-4158-8f1f-1418e4b2b930 |
| coring method|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7a3311c3-ee64-46e8-abbe-1350a46283c9 |
| correction|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a07c64dd-06e7-4933-91ba-339ee3c45bf5 |
| correction factor|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ff69acf4-fb72-4914-8ba9-df58be8304d6 |
| correlation coefficient|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b32ff90f-b840-4b74-8d03-76bc5d98d494 |
| correlation depth dip|depth dip|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d07c9014-144b-4c9f-adb7-d7a8e7576900 |
| corridor stack|acoustic amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2a36faca-c6b2-42b4-b0db-d51fa8320e61 |
| corrosion indicator|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f0177e38-d7e0-40d9-a25b-6815d4817cb8 |
| corrosive fluid flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a089cd92-9a75-4fb1-99e8-07439bdeb9f4 |
| cosine of phase|analytic trace|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5296e191-5c70-43e5-86a6-617a52355ad3 |
| cost|money|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:69ad0481-074c-415c-b139-7b91958a874c |
| cost charge type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fbe5cdd8-f4b2-426d-9648-5af259321877 |
| cost per depth|cost|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8c95d3ec-7e67-41b6-bb48-c7039ff3ac9a |
| cost per mass|cost|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3cb58fde-5320-48be-9c64-feddc193f580 |
| cost per time|cost|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f116b496-9aba-4ecf-8ef9-b8f3afb28565 |
| cost per volume|cost|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:293fffef-5eeb-41d5-9738-edf528a55e84 |
| coulomb friction coefficient|friction coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0bb6ea59-0878-4974-9e26-de40f21deef2 |
| count|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ee1efc57-1990-4c63-af90-974f186d1644 |
| country code|geopolitical region code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:27ac1a7e-887b-4738-bdd8-98b2a9bb8bd6 |
| country name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b71edf23-060b-4f13-bde5-fc83f7786502 |
| county code|geopolitical region code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:82d34949-49b7-450b-ae1f-73c2d417b76f |
| county name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:22710c52-ffd5-40a4-9e65-043ddd6ed6f1 |
| covariance|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:622013c7-5220-4c89-94dc-1c6ebd58b0ad |
| crossflow coefficient|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dcd79095-1ef8-42aa-a772-9d8b225b36df |
| crossflow flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6dd89a6a-b7fd-4fa2-8fcd-a3d683815c55 |
| cs trans type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6d632bc5-9b43-4ae7-aaaf-0cc601f3b503 |
| cultural feature type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:054a17be-7005-49c2-8af2-403e8a6cd6e5 |
| currency code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e9d0c932-c5c8-4471-838d-bd2b741c159f |
| cushion type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:24c36fb0-6bd5-4086-85bd-b7faba14e906 |
| cutter count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:392e6be6-f2ee-46aa-bab6-07a7b05a7758 |
| cutter type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2ded5986-0466-4579-b1ab-034b7c58ba2a |
| cycle sample count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0ae45a42-75de-420f-8b39-54ba123878bd |
| cyclic redundancy check|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:45d5edb7-4393-4d5a-9b9d-dca041dd3ead |
| cylindrical position|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0765c62b-67c2-40be-bb33-e98fbc233282 |
| daily prognosis|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:89dbd207-1e83-4e48-a9db-5d03af030d82 |
| data acquisition mode|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a7b929fc-9018-42bd-b82e-b42d9a23fde4 |
| data compression flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:aa470414-8538-4122-bf5f-6c4fc67668c8 |
| data compression rate|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:10879620-a565-4cc7-9b87-7d1c296ba26b |
| data direction|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:681b3830-3457-4540-8113-d453d3cea629 |
| data focus type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e74ecd8e-a969-44fc-924f-76fa91894c2b |
| data mask|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3aa494c4-5ca8-4d80-aa83-8ff72122752d |
| data presence flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c643efb4-2d28-43ed-8f7f-3ede5e6a6ec9 |
| data reduction type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:19a52eb0-f94c-45c8-91ef-6a8ed0ab767d |
| date format|format|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7a708088-2077-449d-a89b-080cb3d27216 |
| decimation factor|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c52c15c9-6bf9-40d8-ab2c-184934b37a6a |
| def currency|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:edb3cb07-aaa8-493a-99eb-9ede7f088f14 |
| def display type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7075eb92-82ef-42f9-86fd-b9a3536f8eca |
| def language|language|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:eaaf372d-5391-445e-b0e4-0b13aaa24209 |
| def unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:427dbcd6-2739-4977-93bb-0db54122aba8 |
| def unit system|unit system code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2033ac3f-fdea-4606-9f6a-85934a168198 |
| def value number|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7770d315-9647-43bb-a03a-1510e27780c7 |
| def value string|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:32fae88e-a2c8-408c-9d6e-04d505cd7a19 |
| definition|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a03dc1d7-31cc-4e62-a785-47543587d948 |
| degradation factor|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f0c8265c-3d9b-4321-a1c4-e563dea38748 |
| degree of confidence|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e9c5614f-13fd-49ca-8cc3-e5128f0290da |
| density quality indicator|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9a02c010-ef6b-479d-9b6d-9724f054b727 |
| depositional environment type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d166cc3a-a8c6-4726-9e46-cb111e0c0288 |
| depth dip|dip angle|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:48deb494-40cc-4fbe-941e-ca58021d3e28 |
| depth unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:577bbe5a-be54-4204-a03b-014674f3ea9b |
| depth value type|value type code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ff507df4-edc2-48df-ba2c-0bc0496d7dd8 |
| derivative model|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:44ac61f2-7930-4a2d-858c-1f9b4a702ef7 |
| derivative type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9435365e-6fec-423d-b3e0-f9ba45939eda |
| derived inverse flattening|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:583745b5-cb9f-48b1-8f98-492b3892c60b |
| derived minor axis|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:04f7cd73-4032-45b9-8a17-f98906c1e288 |
| description|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:89230474-eb6e-417f-8fce-f2f00ee6fafb |
| detection gain|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ca2b874e-2bb5-4052-9f05-d56068472e37 |
| detector background yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7f550352-2628-480f-a8df-e07ef88649bc |
| deviation skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:679da9dc-bb53-4fe6-b0f9-ad2955e2981b |
| diameter restriction|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:24eb6372-3c6a-49bb-8fe2-ca1e5e7ecc22 |
| diameter unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:be935876-59c2-45cf-9bd3-c6198c586c14 |
| dielectric constant|permittivity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:433bd214-fa2d-492f-8152-faa00d4d80e6 |
| diff pressure gauge type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f995becc-3e0d-4dbf-83a6-d1c3d92dc4be |
| digital encoding|format|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:aed1e926-e311-4df2-b5d3-95f38406f8eb |
| digital file format|format|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3c43e96b-4257-4632-a6fa-b16e400b8532 |
| digital file format version|version|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e58e0672-6fde-44b3-9e2a-1b444942392b |
| digital storage|byte count|UnitQuantity:non-dimensional|UnitQuantity:digital storage|{{NAMESPACE}}:reference-data--PropertyType:8d44d77e-517f-412e-8184-68c3816d5894 |
| dimension count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:45e81afa-1aaf-4a1e-972a-fb5973388ca9 |
| dimensionless borehole radius|borehole radius|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cc0e13e1-28cf-4533-a31a-d6083d02f445 |
| dimensionless boundary distance|well to boundary distance|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d45414fb-8029-49a1-b00c-ada76560da06 |
| dimensionless changing storage pressure|pressure|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1b552264-edfb-43e3-9a33-c0ba29ae0947 |
| dimensionless fracture conductivity|fracture conductivity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a682bdf3-fef7-4ad2-9165-cd4441e6f703 |
| dimensionless interwell distance|interwell distance|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:abe82855-4ee4-456e-8c2f-f9e000efd34a |
| dimensionless layer diffusivity|layer diffusivity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:15e003aa-5241-44f1-8416-986cbbe79628 |
| dimensionless rectangle length|rectangle length|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5cc8b67d-df6a-4ee9-ba1f-e855f9275282 |
| dimensionless well x position|x coordinate|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e6b8e005-cd8a-4e38-bf60-af206229d496 |
| dimensionless well y position|y coordinate|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:509822a5-faa9-493d-8542-d9c1c10cf4a7 |
| dip classification|dip type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1256d421-cda6-4854-a2a9-64c6b359c8d5 |
| dip detection source|source|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3896daa0-4d37-428f-bdc0-fc1f0f68c8cf |
| dip number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5a94a038-03e3-4290-9fc5-af06528343f4 |
| dip pattern shape|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d90001a4-4314-49b0-accf-7cafbf229927 |
| dip pattern type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e0a66836-def2-40d8-8ffd-82d271f71fc2 |
| dip quality|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:442da96b-4d2e-4aac-97df-5fe7f99c7c49 |
| dip quality cutoff|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8045f141-3531-420d-acbe-1a662bcc3d23 |
| dip type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3719cb3a-c912-42fa-910f-3a93e5cde275 |
| dip valid zone type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:95029ba8-cd82-4b60-9e36-e9e565084284 |
| disclaimer|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:43fa2b60-02a7-4696-b161-1bacc3580ae0 |
| discontinuity type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:af096256-c808-48f0-adc8-6e2413a6e595 |
| discriminator 1 cutoff|threshold|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d2ea9db8-4159-414a-95a5-6ff98a370d17 |
| discriminator 2 cutoff|threshold|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:42741d11-1aed-45e8-b93e-04966576dc9b |
| displaced fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7463551d-710f-4bc0-92fa-f4d1e8f82a28 |
| displacement efficiency|efficiency|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b085ff73-c09a-471b-b699-f17131c29d44 |
| display index type|axis index code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:daa3f5ae-8143-415e-b023-0647708dc008 |
| display name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:98d86235-3f0d-4b5b-aed4-2272b7c10510 |
| display name format|format|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:975e71d0-7501-480f-b681-ae0e78147ccb |
| distance unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a99769d5-e348-43fb-83d8-d1e159888568 |
| documentation file|file name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:938cdde0-9e2b-4256-aecb-7fb00c464c47 |
| dose equivalent|specific energy|UnitQuantity:energy per mass|UnitQuantity:dose equivalent|{{NAMESPACE}}:reference-data--PropertyType:09fa6920-4b29-42ab-809a-7340a8cf0ea8 |
| dosing mode|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:76a91900-9948-4365-8ea4-0815d15d6e8c |
| dowell chemical code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:76cd67d6-9c7d-4c7c-84af-b2ffd99ecc66 |
| drainage area shape factor|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:383a208d-41cb-4d1e-bb23-a47c692df4c7 |
| drill bit code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:975e0a4c-6dbc-4066-a128-525d6a5fd5e7 |
| drill bit grade iadc|grade|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e829d3d2-db00-4e04-9ef1-31653b6734fa |
| drill bit grade tbg|grade|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:64cd927b-7a05-43ff-b41e-1b5a1a3002d0 |
| drill bit grade wear|grade|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f226e2f9-5275-4336-8483-e9c494eae2dc |
| drill bit type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ff599dc4-0df5-4d86-9885-2877ef5f32df |
| drill collar type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3418cb6f-e78f-4625-b275-801b1e387fc6 |
| drill downhole tool code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8309867c-cf57-4997-8ce0-5e279913e507 |
| drill downhole tool type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c74f8376-bfa7-468c-829c-21be311ad5e4 |
| drill fluid code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9e50a7ab-7b55-4e14-98c4-075968b5fe9e |
| drill fluid name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bd099b18-85d0-4fc5-a569-2679447500a9 |
| drill fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:10efb054-691f-4126-ba04-58c62d9d0ebc |
| drill hole opener reamer code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4e1a0c23-0ebd-4eb4-89d8-5cf911e58d9f |
| drill hole opener reamer type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c2351b10-787c-4881-a892-8a6c9225720d |
| drill jar shock code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ac16005b-a9be-4328-a34c-2e5ca3ec42b9 |
| drill jar shock type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cdf34794-089e-4d8e-8b7a-8cd131ee778f |
| drill motor code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b2ae92e5-a735-423b-814c-1602972d1f9e |
| drill motor type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2147c5eb-96a8-4e66-83d7-5282eac2d04e |
| drill operation mask|data mask|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8428f426-e127-4212-a605-da0f6cb26614 |
| drill pipe grade|grade|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2332abe8-ec0b-44ce-ac53-03f8dd94f20c |
| drill pipe type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:24721109-db59-4ac7-8789-ffc50fdc6814 |
| drill slot cluster type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:355cb35f-175f-4ece-a8ce-b83babcb7875 |
| drill stabilizer code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fe5005d2-89e9-4160-929c-992352e8e44c |
| drill stabilizer type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:96d0e745-030a-4d1e-b90c-63ffee243749 |
| drill structure code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7acb5f8c-f2c3-4294-bced-8935600a97ab |
| drill structure name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bd7f62e8-4156-4a9d-b279-ac78d1f17855 |
| drill structure type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8156ae3e-7762-4f22-b5d7-d5b622e05a4a |
| drill sub type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:21a697c0-eb3a-4d68-bd4a-5b6f37a3e5f5 |
| drill survey tool code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cdc3daef-8a81-4ab8-88f3-a02fcabd8dac |
| drill survey tool type|tool type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:26b3183c-e375-44d7-bcee-3c035db5f160 |
| driller elevation reference code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fb4e5ae1-3847-4ba2-9949-41e34bd627e0 |
| drilling exponent|exponent|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:193620c4-125c-4f45-9054-cb720a4d0232 |
| dry gas gravity|gas gravity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:07462a3f-ea1e-46ac-88cb-e3495a4abb02 |
| dst gauge adapter code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c25d01e2-cc15-4771-ae91-ab97582ab3e7 |
| dst gauge type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:77829fd1-750d-4674-8c68-b5f6070df04a |
| dune shape|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:409eb8b9-7fce-4877-95d8-d9ee1dc55fb6 |
| duration unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a8edbe96-9ebd-4495-9906-591a34c466a2 |
| duty cycle|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7f12d3ff-1cc7-4bb6-ba8b-d737c6013f0d |
| east west line|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3e928c8c-2c27-44d0-9fd6-0ec7fe338f6e |
| eccentricity|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f04c8a8b-2ec9-448b-b27b-96bfa2e5a827 |
| eccentricity squared|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:516399d1-89b0-4e3e-a402-4a4c0b4b28c4 |
| echo amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:26a2e8d0-2a18-4f2a-a4b3-1f0cb564bcd5 |
| efficiency|ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:abf8fe54-b6df-4e94-a1a8-70bf7e1e4703 |
| elastic impedance reflectivity|reflection coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:638b0ddc-13b4-414d-aa98-b812787431cc |
| electric susceptibility|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0596df56-0248-4095-b71a-43ddbfce21a7 |
| electrical property|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4116a9ec-b682-4fbc-bd34-43282c0bfb9d |
| electrochemical equivalent|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c82d2cee-d2d1-45ac-97a7-370e9d377661 |
| element code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3cc918af-fbff-4947-b217-4b4660e8750a |
| element count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c1f69bb0-f631-45c6-9e9d-311acb956ee0 |
| element table|table name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b6db47be-4b35-4c7c-b6a3-d0ee4595ddc3 |
| element tag|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9a141e90-2f41-4167-95f1-4f15a71e5ef3 |
| elemental yield|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:316a1076-3e8e-4289-823b-48d0387333f2 |
| elevation reference code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ddddec7a-0963-4cd4-95e3-22b26df1e0ed |
| elevation reference name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:75e2826c-cea8-4b06-bbce-5342406aa91f |
| ellipsoid code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a24ebf7e-73fe-4c8e-ba38-650ca3bc9c46 |
| ellipsoid ecc2|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:25d73f14-c3f3-448a-9acc-a0532afc65de |
| ellipsoid flat|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:67e52d6e-3bdd-4e57-b10a-9138f7aa649c |
| ellipsoid invflat|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3e18dfe7-a1f0-4e87-b1fa-77df1b3ee3ab |
| ellipsoid name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9d5093f9-5ddf-4731-8886-f9e07e1cfa9a |
| ellipticity|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:44f1e09f-55c2-4faa-89be-d59d890d7a29 |
| email address|address|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e9d70d8a-d5cf-4dc5-9020-00ce57f74675 |
| end age code|geologic age code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ce2d3b19-ff0b-4c59-8253-f71c663b31c8 |
| end element|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3fdcdf5b-dae6-4858-a089-e1dc0ed9f467 |
| energy length per area|force|UnitQuantity:force|UnitQuantity:energy length per area|{{NAMESPACE}}:reference-data--PropertyType:f4b45476-d843-49b2-968d-0ea1e56e588c |
| energy per length|force|UnitQuantity:force|UnitQuantity:energy per length|{{NAMESPACE}}:reference-data--PropertyType:71e27720-e55f-4aec-8d3a-b1409a4d84dc |
| entity code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:63b8fcc0-a952-43d0-bd79-8ed695aa30dd |
| entity object|object name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4442eac1-d669-4e64-9aa5-78da7734918d |
| eou computation method|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0cb15e6e-d251-4b9f-86bb-6fe7c6f5e1b3 |
| equipment type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:26c35d6e-96db-4003-8a13-6900961cc704 |
| erosion state|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4a206533-0177-496a-ac62-1af01d20f988 |
| error|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e99e2a8c-757e-4946-a4bd-2e40a43edcfa |
| error band|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2dcbec8b-0392-45d0-8390-1fdc1680e081 |
| estimation flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0f0a9a41-5e96-4302-98b6-405bed855925 |
| event class|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:870fa433-c602-45ef-bdb3-d9bddb4fae1e |
| event location|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:582e4ba5-3c2b-44a0-97fe-efdaf4f867ff |
| event subclass|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9611b71c-4345-4bc8-b0d4-35599ed64b63 |
| existence kind|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1eeb55fd-a76f-49de-946d-b7f0f330c9e4 |
| expansion factor|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:168798e0-8e52-4c5f-97f5-f9b5c4f983ff |
| explicit index|multi use property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:36f718c4-371e-4d5b-a8cb-3bc3a3a84ea0 |
| exponent|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:08ef21bc-6550-44b6-ae58-360b776ce283 |
| exposure class|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d755b53a-dd50-48cd-ac91-e3b83622fd47 |
| facies boundary type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3dc56a3a-8c5c-48e6-9bba-8c78101134c7 |
| facies type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:838b1a9b-2fca-4e2e-aae3-323ef1bc59c7 |
| fan shape|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:986e5120-7fd4-46c2-b2ff-1db14d8b3461 |
| fault block|explicit index|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c7946422-a0b5-49b2-96f9-fd679bf636f1 |
| fault character|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0e45353a-8556-4ed3-8306-28b04720f262 |
| fault control flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7d598d05-f7eb-4760-91f9-2b3d9513b381 |
| fault detection source|source|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:81722497-8a84-4789-b05e-705da1f7ca69 |
| fault distortion type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:40d8b838-b17d-4ccf-8e42-5e17e2fc14c8 |
| fault type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:26c71d19-b6e4-4e8e-90af-531d30b0940f |
| faulting time wrt sedimentation|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b3e5583c-b155-49fc-8cab-5f3d560fa4e2 |
| faulting time wrt tilt|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:403172ee-6915-432c-a498-1042df990e9e |
| fax number|phone number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7612ef43-0d80-48cf-805c-74290a341d91 |
| feature dip|dip angle|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:301f5782-7dfe-44f3-8af0-12dda13e1da9 |
| feature intersection type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b7c0722c-c991-4ff5-bee5-0476ab263bdd |
| feedback loop deviation|error|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3a54e34e-9b57-41b8-9bd4-9d608537b92c |
| field name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e20ffa90-7492-49f9-ace6-429a4b30009e |
| file name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:04409fa9-d265-4c06-a210-fd5a20d0348e |
| file system name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:87cae5d8-0d84-4d4d-8817-e1fc5eff62cc |
| file type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:731fbbaf-8d75-4b7c-97e4-0479569c59fd |
| filter cake thickness quality indicator|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:60d05954-8d90-48d7-bd4d-e12d859d09a6 |
| firing head type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0d7c4e8c-3767-468b-912a-cb42eb7a0508 |
| firmware version|version|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f60ae0a3-4f3f-4d93-acc9-0ced9100411c |
| first derivative|gradient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c3de33fa-c2df-4d22-8c6e-600bcd6f111d |
| fissure storativity ratio|storativity ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b757d5c3-c2cb-4ccb-9769-f49559a76884 |
| flag|indicator|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2138087f-8ba8-4893-9c77-41a8772268d6 |
| flattening|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6a93cee1-ef95-4781-8888-c18b2238852a |
| flow behavior index|exponent|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:81ab1f7b-d378-45af-835e-5b9d413e7ff0 |
| flow capacity|volume fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b7d31a53-c585-40f2-8aad-c74299c419ff |
| flow direction|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:53cb552d-eddb-40ea-b952-a7f76310ba67 |
| flow direction setup|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:934c42eb-9e1f-43b7-a009-10a8014d5f3c |
| flow head code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e27b0019-556e-4d1b-812b-1c27981c8c2e |
| flow head type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:128d8f50-8796-483b-b215-e2049f0cfd53 |
| flow regime|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:405d0c9b-b1ab-4c29-b5c5-5737ede57518 |
| flow type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:718563c1-ae06-4a0e-8cc0-5b5385a18e53 |
| flow unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b43ab1c0-a674-4acc-9b0c-7a53d3f668b0 |
| flowing fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bc36efe7-9d4c-4b97-9d0f-0465de0d439e |
| flowrate index|flowrate|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a3066b63-9d5b-45b5-b62f-6aba77004d83 |
| fluid contact type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:aa3523f4-6f18-4031-959d-d88352b67a9d |
| fluid marker type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f6b4cd03-262e-429d-b22e-283f36c54fdc |
| fluid phase|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:03c458f6-2e17-46a8-9b4f-4490bf0c9b99 |
| fluid return outlet|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9b3f4d3d-703f-4d27-8499-56fa79c65076 |
| fluid sample origin|source|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e97e56b8-1bc6-4c06-9bb5-a0f2462b2f86 |
| fluid type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:207e562b-5505-4b4f-a710-73ed8f9e2a18 |
| fluorescence|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7a55436c-dd1e-4c2b-a476-25118053e19b |
| fluorine yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:354978c8-07c8-4335-ba7e-238994ad5894 |
| fold cdp|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2b5e1e63-db87-44d8-9cc6-b4007bb04cc7 |
| fold type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:04f094c8-da2d-4824-9aa8-d430950e5e08 |
| force length per length|force|UnitQuantity:force|UnitQuantity:force length per length|{{NAMESPACE}}:reference-data--PropertyType:18b90ff9-90ee-49b5-ad59-7ed2c0aec164 |
| force per force|ratio|UnitQuantity:dimensionless|UnitQuantity:force per force|{{NAMESPACE}}:reference-data--PropertyType:3c2296a8-e9a0-4673-b8c9-1b6a1cdb11f1 |
| foreign system|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1493d1c4-de41-48e7-945e-65ecb0d34ed3 |
| format|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:667d02bf-9499-46c7-8807-ae398f38fdb5 |
| formation damage skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:42593d83-375d-4e24-bf2a-4a6f0f36ff34 |
| formation name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:48a02f88-8cfe-44e5-acb3-45038443d8f7 |
| formation stiffness factor|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d75b02c4-82ed-4ad5-8c00-3a422a121718 |
| formation system type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4e00e998-4bc5-4653-b598-d6af7e207a82 |
| formula|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:85e15c7d-96e3-4797-9fbf-165d68d35653 |
| fossil genus|genus|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3fc92775-2ad8-4337-bc51-52488193d202 |
| fossil group|group|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9e5e30d8-5fdd-44df-a32a-49ec2fc12f00 |
| fossil species|species|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1a616c72-bbb8-4cdc-a23f-0ec04b93c408 |
| fossil type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:19a31305-35da-4b82-a14e-7499115e95a3 |
| fractional surface area|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:46655d9a-558d-4518-afbb-a88f578a186e |
| fracture condition|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:072d1440-4de3-4e74-ad0d-d7d6a140dba4 |
| fracture element distribution type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b4da607e-4559-49fc-99bd-dc4d19f6a803 |
| fracture face skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0c41573a-80e4-4c2c-935b-2af24a2eaaad |
| fracture geometry|geometry|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:333a8683-adf5-44e9-8158-657c3df5117a |
| fracture origin|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8a440e25-fec6-4445-a007-c504e178d45f |
| fracture plausibility|probability|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2e2c8c0d-97b9-43a5-928a-256e11382153 |
| fracture storativity|storativity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ad6e47aa-b1bd-4e44-915a-cb00419c98ac |
| fracture toughness|strength|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b8097386-4ccc-4ef1-b289-77c893813c63 |
| fracture type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a442cefe-101e-4f66-a63d-26a8027c1773 |
| fracturing efficiency|efficiency|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d599d68e-2e5c-49e1-bb2f-65409602bef7 |
| frame number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:30b15da9-e0bd-4355-9ebf-ac034e0e8655 |
| free fluid index|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bda91aea-1cab-4eee-b7e7-076e59e6bfdd |
| free pipe signal|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:13327868-06dc-4a04-93de-7a01322efc72 |
| frequency filter length|length|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c8da74e0-c02d-4848-93f3-e8fc52a0cad8 |
| from conn number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ccc0e742-4a17-4412-a2f3-b48c22cce90f |
| from datum code|geodetic datum code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c469ef0b-eb68-4295-8ca3-ba983a3674b3 |
| from entity code|entity code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3cd8a9d1-0e84-48ac-bc17-4ed6dcab6f24 |
| from mounting location|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:af05989f-e015-416e-b15d-bcb549735ca6 |
| from property code|property code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b8e01dbc-24f1-4b0e-8430-01cdcf943879 |
| function name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1c618970-6590-4336-9e81-ccf4d0afee7d |
| gadolinium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:45458b2c-ffe4-4412-a571-38f451b7199c |
| gamma ray|property||New PropertyType item: 26c54ec7-76a0-4fae-966a-c3bae13c81e0|{{NAMESPACE}}:reference-data--PropertyType:26c54ec7-76a0-4fae-966a-c3bae13c81e0 |
| gamma ray minus uranium|gamma ray||New PropertyType item: bc1b2436-a4be-4faf-aca6-12b7e152c083|{{NAMESPACE}}:reference-data--PropertyType:bc1b2436-a4be-4faf-aca6-12b7e152c083 |
| gas description type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3b95914d-f7f9-41a0-a784-4b08d7381420 |
| gas deviation factor|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e667667c-6d9b-4451-8821-346a34090479 |
| gas flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dc8b136c-db5c-46be-bbf1-6e168ed3c18e |
| gas gravimeter type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a18ddaea-80ae-4f8e-97e6-b0a1c9d88867 |
| gas gravity|specific gravity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:36eb43b3-b766-408e-a0e8-2aa3610f7faf |
| gas holdup|holdup|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9c0b24f7-5f44-48dd-be4d-664a0cd25e41 |
| gas line pressure gauge type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:05b80274-84ee-4324-9c4d-0544894efd83 |
| gas line temperature gauge type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0df79038-7fa7-49d5-846d-717f773d9731 |
| gas meter type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:65b98f9a-195b-46b5-adc0-18ce9f94cfc9 |
| gas pressure axis type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e3025ad5-3da5-4f73-ac7f-b204ee299432 |
| gas probability|probability|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:08e15b2c-c603-496e-a4fa-5408fadc12c3 |
| gas sampling point|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ca467a89-bd8f-4429-808e-738c12331814 |
| gauge count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:09d5f474-fce7-46bb-8c4f-6d5aa0382813 |
| general weather description|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9b4d2f6a-5ae8-4469-b1bd-eb2b2f9093fc |
| genus|biological name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a4307698-c686-4e51-aa0f-bc7b67096602 |
| geocentric s|scale factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8ce9f2c9-5dca-45b7-ae08-4606b3b05cc9 |
| geocentric scale factor|scale factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:aa139d00-ebe7-4b27-9e51-c73ab5f7a204 |
| geodetic coord sys code|coordinate system code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b497de14-b7b6-430b-98b0-25c800bd4972 |
| geodetic coord sys name|coordinate system name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3ebd59f1-f728-4601-822d-cdfde8ee69d9 |
| geodetic datum code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e129828f-20d4-4da7-98ba-2b161b3ed559 |
| geodetic datum name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9016a1b2-83dd-4953-9476-422aa0637e25 |
| geodetic datum point|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:212e84f6-0434-49dc-b109-ea28618e56aa |
| geographic location|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f1989584-3a35-4a67-bf3c-24ba5da35de0 |
| geologic age code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f626d5a4-47dd-4abd-92db-3a17ddee73fe |
| geologic age type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ff0531f2-ff60-4301-9c4c-59308323b0fb |
| geologic flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fbe7484e-4ae8-4711-8aa5-a140f38ae539 |
| geologic k|explicit index|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:493ed155-55fa-42d1-96e5-417855e1e036 |
| geological influence type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6ecc71db-0c6b-4f7b-a33e-28da01d8d962 |
| geometrical factor|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0a2b8302-d262-4a33-a81c-033fb9b2289c |
| geometry|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8edd1cda-54a8-41be-a881-f0f9edf2e895 |
| geophone type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:00233e7c-6ce8-47e8-8324-0b50583212b7 |
| geopolitical region code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:26880c54-d141-46b5-9a40-50016f5d5c8f |
| geopolitical region type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4ea9668f-9de4-4d32-a7e4-637211c61a68 |
| get function name|function name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d7b5bbc6-5d62-4728-90b6-7f94f00fe79f |
| gpd lithology pattern|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2909255e-7e1b-4670-a31b-30f342104b4d |
| gqi admission policy|processing option|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:284da02c-f60e-41d6-a7a3-70c31703133a |
| grade|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0b828a98-20fc-47c7-bfb0-29be46663e47 |
| gradient|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:16c96194-f47a-442c-b1da-e1555e61bb29 |
| grain distribution|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:196f4cde-a627-44d1-9084-82500d271e62 |
| granulometric sequence type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ce7761a7-862c-4f9d-b0d7-cd463a3c0b9d |
| graphic symbol file name|file name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b1f953df-04ae-4e9d-ae11-d1a295113075 |
| graphic symbol name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bc3d40fd-3f30-4d48-a6d5-fa4854806a25 |
| graphical file|file name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:31276823-b40c-4aaf-9733-8eb0d46105aa |
| group|biological name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c007f660-19bd-4791-9dcc-9e8798f5d1fb |
| h2s flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cad4d355-6802-4b8a-bf5a-a9b4da4924d4 |
| h2s mole fraction|mole fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:469c3c23-99c6-43e7-9f3c-e83b9dc16935 |
| h2s service flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c34174f6-5f01-4bbc-9208-534f41a23f08 |
| h2s service type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:843f4b23-d821-4346-a03f-c27df1a2699e |
| heat capacity|property|UnitQuantity:specific heat capacity|UnitQuantity:heat capacity|{{NAMESPACE}}:reference-data--PropertyType:cc02a94a-d1cf-40fb-8563-b88c65bb3926 |
| heat exchanger code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:14b7fd40-407a-4360-b9ab-a597e1ffc1e2 |
| heat flux|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b946fb86-568f-4709-acc0-08bdde935a7d |
| heat release rate|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:41c53101-bfd9-4184-85f6-c730a5a439c4 |
| heater type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:afeb53a1-31f9-4238-9244-59efce52b93f |
| hemisphere|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:13c9bc06-2fc6-46c8-95ee-b2519df3b8c4 |
| heterogeneity cosine of phase|reflection heterogeneity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2e8734b5-102d-4119-8063-31ce963cae39 |
| heterogeneity instantaneous frequency|reflection heterogeneity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:183d2f34-366d-4caa-833c-4f452764be72 |
| heterogeneity instantaneous phase|reflection heterogeneity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2ac3ea88-6aa3-48d3-8146-c2fccf497b46 |
| heterogeneity reflection strength|reflection heterogeneity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:23a7ab31-7308-4acd-ba02-d3f726463adf |
| heterogeneity seismic amplitude|reflection heterogeneity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:19374c5b-c05c-4554-baa5-efb0d62f4487 |
| hgs removal efficiency|efficiency|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6e26984c-53b7-40c1-b87b-d0ba33d3577c |
| hierarchical flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ca2e92e2-aede-4312-98c9-3371978daf9e |
| holdup|volume fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1a79d3e2-7f13-47a2-ba7a-dde950ae425d |
| holdup index|holdup|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b9c2b23d-b585-4db6-885d-6a9bc3bdd254 |
| hole condition type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3780f5eb-cdbd-42b1-aefc-1cdf1a9a1365 |
| hole rotation friction coefficient|friction coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c979f0c2-5299-45bd-a4b1-399018936e44 |
| hole sliding friction coefficient|friction coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8daa572b-7d7d-4e68-a9dc-bd70c5f50ea3 |
| hole translation friction coefficient|friction coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5acff1c3-dff7-48bf-9458-da7032d79672 |
| horizon sample count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:868c47bd-0390-42c0-b105-c18855bda697 |
| horizon tracking event|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:47baa1de-3401-4dc6-bc8c-fb348544c720 |
| horizon type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3deb1b2c-febc-4fee-9e5e-44644eff355f |
| host identifier|system identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fed6be4e-ef60-443f-90ba-3aa20e6c8f86 |
| housing number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:40e93416-4061-4c44-a163-35eb5819ce2e |
| hydraulic fracture stage|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d46a5741-1906-4f96-a1ad-28f15d411303 |
| hydraulic model type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:00fe65d9-a1f7-49ff-97f7-a8ffc3a067db |
| hydrogen index|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:90a5c098-526d-439e-a947-ad4e4a69b00c |
| hydrogen mole fraction|mole fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:33829155-9d54-465b-a73c-eff051e7903f |
| hydrogen yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:df01348f-e458-4752-9df8-7477de2b5748 |
| hydrophone type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:063de95f-2c72-44ec-8e0e-3c4fe5fce817 |
| iadc code|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3b95c2f2-b789-43d8-9dba-56d70bf8b31e |
| iadc grade bearing seals|drill bit grade iadc|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7a6cadb9-8300-45b8-a977-b4ae564c5193 |
| iadc grade dull|drill bit grade iadc|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9029913d-24e8-42ae-917a-1e40066fe137 |
| iadc grade gauge|drill bit grade iadc|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:33870fba-5978-4d8c-8173-dfed7b12180c |
| iadc grade inner rows|drill bit grade iadc|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:600ccdcc-ff48-4d31-80c1-951599a12160 |
| iadc grade location|drill bit grade iadc|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:401bdb07-b44c-415b-a3dc-e2e3ec3345a1 |
| iadc grade other|drill bit grade iadc|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0ad2750d-c946-41b5-a1f6-10c74ad3bb18 |
| iadc grade outer rows|drill bit grade iadc|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6b517573-c35e-4eb7-bcc8-6c05966bae25 |
| iadc grade reason pulled|drill bit grade iadc|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f65de7e6-df63-4302-8bde-7c3888b01bbb |
| id|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1a238212-fd9d-4182-ac39-427d40de29d8 |
| ideal receiver position 0|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6f073ef8-1b4e-49b2-863a-095529a24595 |
| ideal receiver position 1|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6fc834cd-d490-45a3-affc-46c04f026647 |
| ideal receiver position 2|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fcd4a33c-0494-4fa6-816a-9a2963e0cb20 |
| ideal source position 0|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ff62edaf-863c-4065-aba7-b410e348e627 |
| ideal source position 1|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9bf91504-38db-471a-b2b3-c0c50998c516 |
| ideal source position 2|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:520bffd8-702d-46f5-b5e1-c22a4e6aedab |
| identifier|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:311dcaba-6512-4144-aa7c-79a9de19b4ab |
| image clipping region|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0a71b8e0-9189-4c47-9597-119857975654 |
| implementation code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:38b565d3-c8b9-40ef-b75d-ef0e5d8ba01c |
| included|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2461e0cb-9467-4945-ac0e-c6ad5494b17d |
| inclusion proportion|proportion|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c3e5b199-6ada-4f53-8db7-b74d1dcd2ce3 |
| inclusion type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6c8abad5-270e-46da-af8e-ebf13e6fcde6 |
| index type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7eccfd5c-eee4-49a5-8b14-96db0c973b3c |
| indicator|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:12111ccf-2ed3-42f9-9e2f-12977a8a4be9 |
| induced gamma ray|gamma ray||New PropertyType item: 121fae91-5f97-4fec-82da-dc2e9cea15d4|{{NAMESPACE}}:reference-data--PropertyType:121fae91-5f97-4fec-82da-dc2e9cea15d4 |
| inertial flow coefficient correlation type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:09d32e34-56be-49fe-b723-9cdbaca24464 |
| inertial flow correlation coef|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:235b6b15-59d4-4352-986d-84b08d8ece3b |
| inertial flow correlation exponent|exponent|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2ea55e3e-3fca-42b0-95f2-6b36f5cba8c9 |
| initial amount|amount|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c2f498b7-f278-412e-bf10-7cfb6b53dfae |
| injected fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c4fbede1-c768-4129-ab4a-773266854b23 |
| injection point|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:87b3b898-0413-4b3b-a0e5-eccb3d725a74 |
| inner boundary condition type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:95ff2780-666f-40d9-a8df-22f9799afa95 |
| inner outer storativity ratio|storativity ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7ebf868a-6166-4245-acda-4d22391c7d11 |
| input data name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a148befc-3485-4938-a13d-bad0f32ef65e |
| instantaneous amplitude|analytic trace|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0c0f079b-44b5-4e15-af44-a631f14d5947 |
| instantaneous frequency slope|slope|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fc17f05b-88a9-46b9-bde5-fcf2a87e035e |
| integrated apparent seismic polarity|integrated trace|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f43eb3ff-77ec-482e-8b10-c5753868a87c |
| integrated cosine of phase|integrated trace|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3c22a3b8-87c8-432e-a420-2c5dbbc0a290 |
| integrated instantaneous frequency|integrated trace|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1054cc37-44f3-4256-9944-41ccdc8cb23b |
| integrated reflection strength|integrated trace|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3537729b-da2a-410e-8943-2d1b254d48c9 |
| integrated seismic amplitude|integrated trace|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:facc7c72-28af-4411-9705-40bcefeac630 |
| integrated seismic magnitude|integrated trace|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d74501ff-9f5c-43e4-ae89-fe93bd355302 |
| integrated trace|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b1b6a37c-561d-4883-98fb-a9845de3217f |
| inter bed contrast|conductivity contrast|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3b51bcb0-a880-4630-b75e-03caaf31239e |
| international aircraft regulation|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:440e0494-e07c-4f14-bd0f-04ae308b0900 |
| internet address|system identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f1bf0a94-e736-4899-bb1c-74073c631bef |
| interpolation method|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:58ca8cd1-d406-4dfe-a1ba-5df91b8a8346 |
| interporosity flow coefficient|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9da76e99-31aa-45b0-a37b-59e2f14b199a |
| interpretation grid count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:603dbf8f-b944-49d3-8c43-71fee5f26aec |
| interpretation origin|source|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:80926846-0b19-4798-b33f-a5ad38ee9244 |
| interruption type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e2f235f8-f84c-4879-8f6d-e69e4d40de1e |
| interval alkhalifah eta|alkhalifah eta|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c099c9de-8717-4bbc-bcc3-72f4a83a53c4 |
| interval bale kappa|bale kappa|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:eddbacf3-8512-4fdc-ae81-b69ca1fb8be2 |
| interval bale xi|bale xi|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dcde7c62-5c93-4b42-825c-db0e330a73bd |
| interval compressional shear velocity ratio|compressional shear velocity ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:da970645-722f-4896-ab5d-8a9576c6fb70 |
| interval q ratio|q ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:808f9869-0656-47a2-bf9f-97b8f1d4434d |
| interval qs ratio|qs ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d6c96b96-b27f-41fc-bc85-c255bf28e48e |
| interval sayers anellipticity|sayers anellipticity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:12aebc0b-0282-4ae1-94a5-44f05a6570c1 |
| interval schoenberg ellipticity|schoenberg ellipticity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cb273134-f7b3-41bd-9351-d4d177d0b958 |
| interval shear compressional velocity ratio|shear compressional velocity ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f68967f5-b0b5-4b28-b633-3c318d68ccf1 |
| interval thomsen delta|thomsen delta|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:31eb8046-3542-473e-8d88-8cf6a825421e |
| interval thomsen epsilon|thomsen epsilon|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7d278b2f-39ae-4719-b7b5-5cbc246ff3ab |
| interval thomsen gamma|thomsen gamma|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e51fe208-bca0-4073-b1f9-8f8b3cff0ab0 |
| interval thomsen sigma|thomsen sigma|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ec6c612c-9632-417f-ab74-ecd32cd1cfde |
| interval velocity ellipticity flattening|velocity ellipticity flattening|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1ca902af-6db6-4d42-b109-c64c1b674c3e |
| intrinsic safety certification marking|annotation|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5c91c1c2-ffd6-4b2f-a312-d591ab4f1047 |
| intrinsic safety flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dfed8e6a-e5b8-4aea-81e0-cf5cbfedec65 |
| inverse flattening|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cf4ab7ec-7517-4f29-bba5-170ec2a35f4b |
| ipr fluid flow model type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f1b0b3e4-715e-4bcc-8e61-4e82ff939177 |
| iron yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:15f20c76-265b-42a0-9afb-bc449f1b7fc6 |
| issue number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e60df559-37cb-4959-bde0-d58405b59855 |
| item code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:27c6d696-ef14-4995-832b-6ce0dc5d333f |
| j function|function name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ecd928dd-0b31-42e7-b1a8-33832e5ae5bf |
| j function coeff|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:eb7de7a7-5839-4d34-88cd-222062a26b5e |
| job count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:af820139-0a04-4863-861d-256f7ef145ed |
| job number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d3e4aaad-c14f-4127-94bc-e47433a149ad |
| joint count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:745660c4-8536-46b4-b634-3824ce7dbc5e |
| journal file|file name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e18f18e0-20e1-4dd7-ae36-fbaf7a7a136a |
| kb receiver correction|correction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d6d475c6-f503-4524-9007-f6277dfea675 |
| kb source correction|correction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f0fb65d4-6380-4028-87ed-d45ea27d09bc |
| key|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0fdb2df4-d909-424f-b412-c18d5e62e5a3 |
| kinematic viscosity|viscosity|UnitQuantity:(volume per time) per length|UnitQuantity:kinematic viscosity|{{NAMESPACE}}:reference-data--PropertyType:cd706e7e-8ed2-45db-8e67-6301d5b18d44 |
| klinkenberg factor|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6dad2bef-adf2-4ec4-9cc7-9e879e81b77e |
| kurtosis|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6c9005f7-1f65-453d-ac9d-7e2286aa220b |
| lag usage flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3385154c-a1d9-42e3-9fc2-cadb39c9c47c |
| lambda rho|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a34757fb-79c8-4fa2-9fd3-a05a2139beea |
| lambda rho reflectivity|reflection coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e45efdfe-885e-4976-9670-12c1041a40ce |
| lame lambda|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ff96663b-5a11-4a0d-9d0e-bde70ff0d4ad |
| language|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ee469780-e558-4ec8-b7f2-07f1ed3c02c4 |
| latitude longitude unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f88872ea-7631-439f-93e2-bec5a7393d91 |
| lead yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c8ab28e4-b818-4f3d-b2f7-61e9da08b808 |
| lease name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a916f803-244b-49ae-9873-13689abac9c2 |
| lease status|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7825033b-c116-444f-b85c-2e335004ae92 |
| lease suffix|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9a6a33bc-57a4-484f-943e-8906df3c3ce7 |
| legal datum|geodetic datum code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:08954d89-a99b-4682-9248-445bf65e3546 |
| legal description|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8e53d7c9-ee0e-439b-a14e-d6cc652bec28 |
| legal ellipsoid|ellipsoid code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f7cd2a81-bdfa-47f5-9125-688b99539425 |
| legal projection|projection code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:99ba10f0-c8aa-4f5b-87a1-d15e8708e3b9 |
| length per length|ratio|UnitQuantity:dimensionless|UnitQuantity:length per length|{{NAMESPACE}}:reference-data--PropertyType:c8d25200-1ffb-4633-95be-38dd802ccc30 |
| length unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4056219f-3f29-42ce-a3c7-369a93c7792d |
| lessor bonus|money|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a34a1bbc-7829-43cb-906a-71afe87684c9 |
| level count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2e08970d-384d-4ae4-85c8-b2b34cdc0b0d |
| level number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9598f840-965a-4bd7-ac41-223d1d87fbaf |
| lgs removal efficiency|efficiency|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dd5eac5f-9fa7-4a00-8501-6ba783f6ce33 |
| library doc file|file name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:63c0dd06-868a-4e70-a142-9ce4fa70a89c |
| line style|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fa05a126-fe62-47b0-be71-2dca4c02810a |
| line thickness|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5803dbfe-3c51-48d0-8181-4901ab563c21 |
| line type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ab7b42db-bcf1-42be-8761-f72a2d286e30 |
| linear thermal expansion|thermal expansion|UnitQuantity:length per length|UnitQuantity:linear thermal expansion|{{NAMESPACE}}:reference-data--PropertyType:bd5fc671-df14-4ea7-a67d-5b58ed2efd6f |
| litho description|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4487b6ca-c5bd-4667-b1d4-7a04dc0bcd5b |
| litho type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:43bbe22e-78d9-4a4f-b2d1-584c4dff4de3 |
| lithologic color|color|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:66e6ab5f-12e7-4809-a70a-8a54ecce334f |
| live trace flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:188f1f41-0d83-41dc-b1a9-430ae56e2003 |
| loaded by|person name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4f9c62ce-236c-4661-a438-f7b70c57e644 |
| local coord sys code|coordinate system code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e1e4be46-0d3b-49f5-9373-94816edbbc89 |
| local coord sys name|coordinate system name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:266cbb3e-2975-4179-b1bc-090b53734449 |
| location|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:752df93a-f799-4652-9807-945bd8cad4f4 |
| log flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:588b657f-b4f6-4417-a591-1d5ac642caca |
| logger elevation reference code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0ec21d67-5919-4d31-a71c-35efc8baeec0 |
| logging cable flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:33b1f76e-2cab-4468-8792-5965a315497b |
| logging unit|serial number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dab1dce5-2c42-4028-a8de-24b4dbd57d9a |
| long term stability|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:eceb83b9-1829-472c-b018-e42db4e703a1 |
| longitudinal amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4738f190-808c-4bea-a1c3-db126197f7e1 |
| longitudinal dispersivity coefficient|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:98e70b87-ab05-43ec-b895-7554a57b20e7 |
| lost time flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d6c8bb88-783f-40d7-8ea9-df513648c81a |
| low power flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a2d79a75-37c4-483b-b598-4260839cef73 |
| lower fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a2709540-4b2d-4c3a-a14e-07c368789e02 |
| lower surface relation|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2ebc22a0-b22c-4a9e-95f0-af88d5b9671f |
| lwd tool type|tool type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:69e7670a-c05b-48d9-9f1a-810dc31b0f28 |
| magnesium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3b42448a-a561-4469-ad52-54bf3d4d1cb9 |
| magnetic declination source|source|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:366aae9b-5625-4b88-8022-d22144caea34 |
| magnetic susceptibility|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8cfdd16d-6004-4f97-8f49-433b04f8753b |
| magnitude|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:29499f2e-232e-470d-af1f-c181cd6e3569 |
| major component|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:50c5fb1b-57fd-4d30-96a9-c94abba52390 |
| mandatory|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a194cc24-975c-43e2-b8eb-ae17d189bf53 |
| manganese yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:044af87a-695d-4bf3-81bd-6418b67efd37 |
| manual control flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:22cddd66-8346-492a-a3b8-76f1276df31a |
| manufacturer|organization code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:94a592e8-a761-4be3-9791-6700c75faffe |
| map file|file name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d356bdd1-2afc-4870-990f-7026f87fc143 |
| map scale factor|scale factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1373b298-b29b-49c1-b581-e76bff0ab6e0 |
| margin|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ab3fe71f-d285-438f-b127-c11791939b93 |
| mass spectrum abundance|amount|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ba0ba8ea-47b7-4771-8ab0-bd9e2e8924dc |
| material addition count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:96e74cc7-e963-4fc7-8b1d-7740a226b9e1 |
| material code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:33b2d10b-b8ed-4bdb-9f4e-66e6d357f3e6 |
| material phase|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:efe34ee5-115c-4c11-b042-443f6ac3629f |
| material type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9211c851-ed35-4e7d-8a26-de55e6d3287f |
| matrix block skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:54eb05c4-a703-4dce-a00f-97c8e886747e |
| matrix specific gravity|specific gravity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a658d27c-1eaa-4408-aa72-05901ef00c3a |
| max amount|amount|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:80866387-04bd-4dbf-a753-c8d242cabd74 |
| max amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:95389875-8aa2-4aac-bf6f-5d6f7abd0bf0 |
| max calibrated value|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3b34ee7c-1bc8-4b44-8014-0354aaebe5e0 |
| max data set count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f02ceaac-c1ec-4cf0-9184-3f441fecfae9 |
| max element count|element count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b11397ac-3eef-47db-ad9b-39f23750458f |
| max gamma ray|gamma ray||New PropertyType item: f63d1e2b-6578-4c2a-8aee-72904478ce3f|{{NAMESPACE}}:reference-data--PropertyType:f63d1e2b-6578-4c2a-8aee-72904478ce3f |
| max gauge count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bade477d-f14f-4141-9891-d8d3a7ec65a6 |
| max horizontal to min horizontal geostress ratio|geostress ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:118b31d4-cca8-48dc-8b07-5c2a54558a66 |
| max index|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1246e462-6bc2-4325-9b5a-539c0ca229c1 |
| max magnitude|magnitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c98e3aea-7238-43e7-bf96-29f1e2c07bc7 |
| max sequence count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f7799b9f-ff36-4b8d-9fff-a17e47aaa1be |
| max threshold|threshold|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9f23cd55-fdd5-444a-865a-15bb7c8e6725 |
| mechanical skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bb70208d-852d-4069-b606-bc641e095263 |
| memory guard flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6784d671-b18d-4412-af3a-7aa63089145f |
| memory size|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0b3a91d4-6d1d-4b4d-95de-6f46ae1b6ea5 |
| memory type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:72ade86f-440d-4747-97c3-29bdbc2ffd27 |
| merge strategy|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:33b72dd5-6013-4ff2-8872-a200a8fb1dcb |
| method|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f8f68130-dfa6-4c23-9453-bea6c0675da2 |
| method type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:18463d6f-878f-4c3a-b83b-b05e28529d46 |
| min amount|amount|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b993bf59-2818-42b4-a199-e12bc3100e99 |
| min amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6d6451a1-bee9-4c2d-94ef-a57baec7865a |
| min calibrated value|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e09d2537-25e9-4e9d-81b5-22878092672d |
| min element count|element count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:467d38b7-fab1-4bcc-8af8-24bf526524b0 |
| min horizontal to vertical geostress ratio|geostress ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8978326d-6370-4eab-8231-529d6a7348ab |
| min index|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a9eeb1bb-4b8d-4048-8681-c449c42d9f72 |
| min magnitude|magnitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ec0fd50d-92de-4089-9be5-996d774b0f76 |
| min threshold|threshold|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c8d052dd-e705-478b-9c04-1834beb71d6b |
| mineral class|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:172b2a0c-ddab-4ad1-ac72-1571dd4e85b4 |
| mineral code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8ccfc927-b7a7-4b4e-a7f6-996a8913e57d |
| mineral concentration|volume fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:59642ae0-8f31-4bc1-bb3d-58ab29f787fa |
| mineral group|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ea0f545e-8853-421c-91d7-aa15e87f2bf4 |
| mineral hardness|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:83e53fa0-0b6f-4194-bc51-2975803223c1 |
| mineral number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c77d67a4-9a87-469d-a693-46311784e89c |
| mineral type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c6589d39-c9f2-4556-84db-2cf9f4b2cfe8 |
| minus tolerance|tolerance|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d8e1f3a7-5378-407b-b4c9-a38c516a1ac4 |
| mixing order|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c73d6664-de88-415a-a828-848bcc2ad7b3 |
| mixture type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:308e2b62-05fa-496e-90a7-d7751ceaeb6a |
| model fit error|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6f12b68a-a17b-45a7-ad70-e625981f8c02 |
| model input measurement|unit quantity code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5a73e53a-56ae-4b60-8279-90fd1a19e5fe |
| model input unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:61c2d634-ac2f-4415-8ab7-082723907403 |
| model name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:414b387f-eeb3-4f7f-9bc9-0dae71b63150 |
| model output measurement|unit quantity code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e4bf5b5f-665b-408c-800f-fc77a6b93360 |
| model output unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:326af6bb-dfe0-4687-b272-d60d4657d408 |
| modified by|person name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ad6f7d2d-0935-4f3d-ae13-26a9d54e0b14 |
| module state|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2dde3d96-597f-45f5-b5cd-17e65b7fb123 |
| mole fraction|concentration|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3339a846-950a-41b5-b446-925507b5b1f6 |
| molecular weight|property|UnitQuantity:dimensionless|UnitQuantity:molecular weight|{{NAMESPACE}}:reference-data--PropertyType:412c5e26-5116-47cf-b1a4-17c71d2f3e93 |
| molybdenum yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f2fa0b37-fe79-4735-922d-7d515663af9b |
| money|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1571ae82-1fc8-451b-bb08-1a19af391289 |
| monophasic fluid flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b5b6dde5-f4eb-42c4-a803-dec9d67cbe9b |
| monotonic flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9a16235c-d7ad-4f46-bf36-74fb9565443c |
| morpho type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8f6d9732-d3b3-4ffa-af73-79e4ec534757 |
| mre latitude coefficient|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4e42b1a0-7aa3-40a1-954a-8084c67005b3 |
| mre longitude coefficient|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:57beed86-dfe9-4121-a76d-73f372043275 |
| msl receiver correction|correction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:eb800600-f7b5-4380-a3a4-af2c5b66cf07 |
| msl source correction|correction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dc07dc43-21bc-457d-b627-f980418f7fc9 |
| mu rho|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dabde9ea-0591-460b-b31e-8cb8dbaa5d1c |
| mu rho reflectivity|reflection coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a370f53d-e76b-46a7-97ee-e44b420e98a5 |
| mud chloride concentration|concentration|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c9307371-81c7-4f02-a4aa-3f73b403505a |
| mud nitrate concentration|concentration|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9fa3b63c-d8c6-42e6-833c-9fa6485cac94 |
| mud pump monitoring purpose|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a07d764c-401d-4752-9ffb-faac27fa3ef0 |
| mud pump type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d3f3d820-9d19-427b-8ddb-91622019e334 |
| mud type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:20dc409e-6059-4fdd-8167-7c467e90369e |
| multi comp magnitude|magnitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:00772280-ec59-4316-84d7-06c1da2520f8 |
| multi intersection flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:25d3509c-8458-4665-a643-bc8d18656fe2 |
| multi use property|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:47a4319d-c8a4-45dc-8600-f96d4b289138 |
| multi valued flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a2aa3eff-4fe9-4425-97e0-59a4d337a9ba |
| mute taper length|element count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3139c87c-5aef-4872-a6e8-834934c2a9cd |
| mwd dpoint|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8374e185-e2fd-4d85-890a-e3c77f01a964 |
| mwd tool code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6142cc6f-6ff6-462e-ba1d-5b30683df10a |
| mwd tool type|tool type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e961cfef-ce82-49a9-bb8f-13b4db941804 |
| name|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0eb70eac-c3d2-4448-b617-10260c4abee3 |
| namespace code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:145c391f-a47c-4d34-a037-2e118390075a |
| naming system|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a9b68da1-bf9f-4293-8081-695a0b7a9c1d |
| negative slope|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c2ee2ea0-90fc-42a6-8495-5abd953d5737 |
| negative threshold|threshold|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f75ec8c9-8812-46c3-ac02-7cb7878ed6c6 |
| neighborhood|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5a33d592-76a0-40c4-9bd8-8be43bd4386a |
| neutron capture cross section quality indicator|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d893a3ca-f20f-4480-81c6-c3968beecec9 |
| neutron quality indicator|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:78365bb8-9fc1-4969-9f37-2dadd6454816 |
| nickel yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:670f8569-b745-42e4-89eb-8befe5634156 |
| nitrogen mole fraction|mole fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1f26a009-e62b-4a3c-a0c5-7271ecd835a0 |
| nitrogen yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e311f38e-cf0a-42a3-acdb-b136ed8017ef |
| nmr amplitude distribution|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4a589727-de2e-4029-962e-6a7480ffac05 |
| nmr fitting error|error|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9460d461-71b2-4736-bb5e-a0f8ed057933 |
| nmr quality indicator|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9b7dafdd-6660-40d3-b88a-df39822aac3c |
| node locked flag|processing option|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:720b42d6-6d3f-4e10-b0e9-194cfbaa3225 |
| node type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ed874f6e-3046-4c46-95c5-c9ca2fcde21f |
| noise amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1b721a91-e85b-42eb-a6b1-88ad8c26ba5e |
| noise coefficient|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ae56bc38-2fb2-491f-89da-31ce48fdea63 |
| nom station increment|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:34d19a0e-1444-43fc-b770-9e9fe18ce180 |
| nomenclature code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7afde6bc-899e-4f94-ae08-fa99b2115ee5 |
| nominal number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2c3322ab-6802-4abc-82c4-e040cf729b43 |
| nominal receiver correction|correction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2b127571-9a77-4aa6-9c4b-6b3bacd62028 |
| nominal source correction|correction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7c12f848-b3e8-4e26-814a-a48b80b7bbb8 |
| non darcy flow coefficient|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1bef145b-70e9-4aee-8b6e-a5c984255c0a |
| non darcy skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:727243cc-b423-400e-8f9e-714ee3bb654f |
| non magnetic flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f807b9c4-d04a-4eb4-83d8-b763923b84af |
| non zero sample count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:47fdb122-eb60-4014-af76-5218ece48f25 |
| none|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a818e404-dbc9-41e0-adc8-455eafc5b8d1 |
| normalization factor|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:246dba69-4521-4b5c-8fd9-9a13d2506632 |
| normalized distance|length|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:28bd5379-4076-4a7b-a241-cd2a39b4ccbd |
| normalized quantity|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9f7a43b5-4bb7-4e39-a219-b3cd05b6fccc |
| north south line|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8c195593-0da3-48c3-b281-252f825da8ab |
| nozzle count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c8be79f2-36a9-464a-af7f-6c07a512ba4d |
| nrm factor|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:704d41e5-603d-4cc3-96e9-973276b571ab |
| object identifier|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6e65dc08-411b-4649-ac88-3571bf16b5f9 |
| object name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4545f22c-5e45-41a7-b817-1946e03b640b |
| occurrence|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:256230ff-5e5b-474a-ba7b-946008750635 |
| occurrence order|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:69b376c8-a267-4d13-899d-8f3509964a25 |
| occurrence probability|probability|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:89640420-f472-477b-a512-9693b8ec1533 |
| offset|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:18ac9bc4-5259-4b57-8181-1dd9d418033a |
| offshore block number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ac65ec76-33c3-4d7f-bc98-c412d9cfe3d1 |
| oil brine ratio|volume fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1fea4a9f-9b8e-4619-9b47-73975a82a4d7 |
| oil compressibility correlation|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:848a81a4-e6f8-44ff-abf1-0f46b944d872 |
| oil formation volume factor correlation|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6161dd5f-4d3d-46e1-9442-fa74246b1f49 |
| oil gravimeter type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e8f45c7c-3ce8-4b7a-a87d-a08a7bdb97e0 |
| oil gravity|specific gravity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5f1d729f-eb9e-46c7-bbc6-68f5bdfafda5 |
| oil holdup|holdup|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b4f91976-a55b-4230-8fc8-726c9393ef1b |
| oil line temperature gauge type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:51d68f58-56ee-4b8e-a449-28389aa82a30 |
| oil probability|probability|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5c4cee1f-3c5e-495d-b02d-de2a2d5a4e05 |
| oil rate meter type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c37577cc-910c-4530-8eb1-fde4c7d25870 |
| oil sampling point|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f531d00e-8fc6-406b-ae01-04c295e86533 |
| oil viscosity correlation|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fd9b2f2a-e2a5-4383-bdcc-01bee722f233 |
| oil wettability index|wettability index|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1c815d07-65e2-4ed1-a586-2f52e21ce29a |
| operating mode|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d8083b20-dab4-4763-b5d7-67eff7d5e0c1 |
| operating system name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:72952235-41bf-4f5a-aa59-a323964991c3 |
| operating system version|version|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:099e46bc-7aeb-4621-878d-f6c870f755bb |
| operator|organization code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3f7949d8-09a3-40ce-943c-1eeab73210ca |
| operator name|organization name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a0b84139-e064-421e-9014-7489c2c292d6 |
| optical density|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:08bf59f7-d897-4bde-9a4e-eb9fee3d6be1 |
| optimum flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:03877eae-1b06-4df0-906c-f63cb91e8fea |
| ordered flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:043e4d4b-f23d-4639-955d-f9d03873faf5 |
| ordinal number|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:323df361-784e-4062-a346-ff4ba80a78f0 |
| organization code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:896a2949-48ea-43f9-a312-b0e626298f96 |
| organization name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9b2af62d-9297-48d7-b14d-d0537f1971c4 |
| organization role|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6ec30f7c-684c-4990-956d-9d6cfc76404d |
| origin x|x coordinate|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a58ecf21-264c-4442-b6c5-e9694cc76221 |
| origin y|y coordinate|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cacfb8ce-32cc-4dbd-92b2-ef2d164a207a |
| origin z|z coordinate|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:814afd18-bfdd-41f5-8dbf-861e02760f6d |
| outer boundary condition type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8c286e3f-67c4-4f58-8648-84b0ea7bd15d |
| outer continental shelf name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:712aa9d8-216f-42df-bad7-40cdf56b5c25 |
| ovality|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:498aee18-97a1-4ac5-aa25-b259767f6ee1 |
| owner name|organization name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f3704fa1-7986-4f58-bb75-fc954cf201ef |
| ownership amount|proportion|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:12ea49f6-853a-4642-a031-c8d7122a3de0 |
| oxygen yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9e3b80c5-1965-4a7d-bf86-4a590623eb9d |
| package size|amount|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:31a91b88-b034-46e6-90be-b5eb4d88b275 |
| packaging code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:80d1a6bc-4cd6-43d3-8886-82fa18b3a97e |
| packaging type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dd27bce4-0d61-4065-ba4d-5db66bf3cec1 |
| packaging unit|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a957d31d-4f1a-4822-98e1-75d27af0f78d |
| packer motion type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3f372a2c-ead0-4968-ae17-522c6ccd27eb |
| packer type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cb803c04-0a8f-448f-8a73-ea8d86b713f6 |
| parameter label|annotation|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:00d46f93-fff5-4812-8048-0d307d68ad26 |
| parent code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6358ea59-2717-45da-8359-79cc78c70671 |
| part number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7e586869-3fb1-4c98-871b-ae2ce1b4b270 |
| partial completion model|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dc4526e1-6113-4387-affb-207f17cb03e5 |
| partial completion skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6dbba762-a7e2-4c49-85c9-f60c546ac690 |
| pattern name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a10eb888-3937-41f0-856a-250107a93ded |
| peak peak delta amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e5f81386-bba8-4469-87b3-0328394d32ed |
| peak trough delta amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fb201c58-676a-4a82-a68c-53fc87de9f8b |
| perforation charge type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9958a7f3-2651-4c3d-bce8-8dd436e65697 |
| perforation count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7c83b42c-8e77-4f6d-9b09-694dbd732aa0 |
| perforation gun phase|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:66146eff-bcf8-4082-9cf1-d590e53b2c3f |
| perforation gun position|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e17b3a1e-bb47-42a6-a6f8-21387dc12fb6 |
| perforation gun type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:318cb8ce-626f-4248-b9b4-ed25c00db6b7 |
| perforation pressure condition|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:71573496-de3e-4df9-8f96-3c4c5cfeaaf5 |
| perforation status|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dbe31329-a3c2-457b-aab8-4dc5049a51d2 |
| perforation type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:48c2666f-8bfe-4195-8771-3ada4f4b0709 |
| permeability length|volume|UnitQuantity:volume|UnitQuantity:permeability length|{{NAMESPACE}}:reference-data--PropertyType:16c1dab1-2a99-4fc4-8415-b721864900fc |
| permittivity anisotropy|anisotropy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1137ab70-95b4-465e-b602-fb3a6c6bb2b8 |
| person name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5d12a1f1-c91e-4695-9bbb-a256e1d83b1b |
| personnel count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0fb6993c-b685-4686-94fc-485232a0e3b8 |
| personnel on board|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b6c26143-3163-4d1e-98a1-7c9338f26e2a |
| personnel role|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cd657d96-a835-4fce-9048-642d3ab8a3d3 |
| ph|concentration|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1e7ee52b-b3b4-4c1a-8edd-ede50d9902ea |
| phone number|address|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:24a1b27e-3d62-4e31-b25e-3638f46b09c5 |
| phosphorus yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:866c76e0-9b7a-4906-af0d-801794db8c41 |
| photoelectric factor quality indicator|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9320c587-a793-4f72-b389-6deac51c466f |
| pi code|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4c15e92b-0378-4ced-b0c4-edd3c789dd47 |
| pi code series|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3be72232-21ea-4eca-9872-c745978af009 |
| pick quality|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c4b1e46b-b00c-4e7c-83a1-507fcc156704 |
| pickup friction coefficient|coulomb friction coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ab5e3e65-2f70-44db-bb7c-55b6ab00b2a7 |
| plane angle unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:93f52422-1519-431b-9252-e310997419d5 |
| plane type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e917e0dd-98a5-408c-9fcf-b9b68b1e02ad |
| platform type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:386063ab-3fd5-4c2b-830e-c3f19704536f |
| play type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fdca32a0-3c2d-4997-9619-2889d2128242 |
| plunger count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a85fa7c9-668e-4a03-b81b-817fc0e1bae7 |
| plus tolerance|tolerance|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5cf5d242-39e0-48bd-8799-99c9e88e10e6 |
| point type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:70da93a6-1d0a-4942-83ce-031fe760cf33 |
| polarity reversal flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c3580f94-ddef-4188-a0f1-d27aa8ffe1fe |
| polynomial order|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:05de8e6a-1716-45ef-9c8c-51c658196f0c |
| population|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4993f937-2142-4260-bb87-fb5ad4fd4a38 |
| porosity type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:99eeeda1-c29c-4469-a313-b320bf63cf17 |
| port count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5af1332e-baf4-454c-b370-c8e35437f45f |
| positive slope|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:694ab722-dfff-428d-b937-52f0c3401943 |
| positive threshold|threshold|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3d093040-bb71-4ba2-a9ba-c43af8d632a4 |
| post cursor amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9882db12-c892-45ea-98f8-820b44da8c16 |
| postal address|address|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9e374ed9-5fc5-46a9-aec0-d627a06e2a5d |
| potassium chloride yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2e65d6a9-1a5c-4e1f-ab36-0fd54ecb319b |
| potassium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8c793a74-8d86-413c-b9bf-6154fd530bf1 |
| pre cursor amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:39bc407a-84c8-4591-8ea2-828af986bd5b |
| precision|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cd1cf79a-2fdf-480b-9233-008f56bfb91a |
| preferred flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a321fa93-c7c4-4ab0-9b3c-4776e5abaa11 |
| preparation method|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7d12eea0-ea64-49cd-8222-d05f1c27f3ba |
| prescale offset|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0c1a6d9e-53b1-45ea-b281-fc4a7392a5a9 |
| preservation method|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c88f2f91-eec1-46cd-92fa-5448599056d3 |
| pressure accuracy|accuracy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ee11b171-3f5a-4d69-857f-78ce6a7b2978 |
| pressure digit|pressure|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:de7d657f-0166-44ed-a6a8-1f2bfbbbc993 |
| pressure drop correlation|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3db55393-4f1c-41b5-b159-32bca25996a7 |
| pressure drop correlation type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2f42fed3-616c-4628-8090-e881194002d5 |
| pressure operation type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c1435c62-0661-44d0-9747-7246705f7fe5 |
| pressure point setup|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8850b044-70a3-4636-a8c6-cf1762de5d97 |
| pressure point setup code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f5dbe846-c4f7-4f24-83fc-96b94e72137d |
| pressure sensor code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dc98f156-722e-4a33-a522-9e9a79bc2c0c |
| pressure sensor type|sensor type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6813a9d9-39d3-421f-ad80-008c1e01bb25 |
| pressure unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:77aaadba-fa9f-4da0-befe-519314016d48 |
| primary output flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2dfd1c95-a40b-4cee-96b3-3330dd25bba2 |
| prime meridian name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:15b0577c-a38d-4e66-bc18-ee53f59d0a27 |
| priority|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:549d1333-b8f0-47ed-b6fe-b77a7b8eb9af |
| probability|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ad9e1078-4521-4b1f-84e4-118279a9cd9f |
| processing option|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b00f78a3-0acf-4b2d-8755-9f24fab1bbf7 |
| product code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:219764bc-be8c-4162-842e-32813bbc6265 |
| product name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d7c8a0d4-829a-4ca5-8768-781d6962755f |
| production history flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cabcfa88-29cb-41ed-bd3a-6c577b8989a8 |
| production history type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:25db6a2a-ad1d-441e-b82d-cfa1b2423d2e |
| productive flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cab210e1-ca96-453a-9fac-16ae58e193ae |
| profile conductance|conductance|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a484b2ad-6e34-41a9-8add-7fc4d0116a50 |
| projection class|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bcc6364d-8354-497e-87f3-36c20e45b8fe |
| projection code|coordinate system code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d48ef4e7-f82e-4e91-bda2-685146223a85 |
| projection cs type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fb896ea2-830d-41b3-aca2-b4c12770e6c8 |
| projection distance unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:da228193-557d-4ed8-abff-62b62f2bcd62 |
| projection name|coordinate system name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:97e481d3-2171-4409-a0bb-3a369fafb040 |
| projection point mode|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:467c7015-d632-4bcc-860d-6ab77ecee4ce |
| projection quadrant|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b5f19cea-23f5-46ff-86e3-eec329c2ff37 |
| projection scale factor|scale factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dc0dca98-b59d-4441-9698-d8fecb56e8a2 |
| projection scale reduction|scale factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d05dc58b-5054-442e-a733-f6e19a8fbb92 |
| projection zone code|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6c0132c4-c05c-4d36-822b-38c2a0af2f0c |
| property|N/A|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a48c9c25-1e3a-43c8-be6a-044224cc69cb |
| property code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8ccb2df7-a9c4-40c8-887e-99465cc29de0 |
| property multiplier|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6d721cf0-c23b-4c27-a161-1a3f2a62342f |
| proposal number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:72991a35-6ef2-4ca9-a7cf-71211eb71e27 |
| publication title|title|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1ec53dc3-43a0-49e6-bbf4-afd0be7ce828 |
| publisher name|organization name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:25e825dd-4038-4b35-9865-5c2163114111 |
| pumping schedule type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:30b2c68f-e356-4e23-aa7b-a232e05422e1 |
| purchase number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:11486846-95af-4a6f-8afa-fa941c4c35ed |
| put function name|function name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d8a027ad-0b96-4b24-95db-b62dba06395f |
| qhse report count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3fcc28e9-4069-42ee-86c9-fc907ce54f1b |
| quality factor|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ad261cf3-b610-4abf-8228-599d5a4e6bde |
| quality indicator|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:22df05fc-605c-4d9d-a539-a54ec9eb69e8 |
| radioactivity|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6140a929-be1f-48a9-b6a0-b5c46942c041 |
| ramping method|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:57e4c990-7e71-43bf-a825-7c7da2ccdfbb |
| range number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3dec0550-953a-4eb3-9a7a-84dd061f99b1 |
| range type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:030d97f1-3065-43ea-bdac-c6582a126eab |
| rank|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8cd33922-f618-4a32-a5cf-f478bc13820e |
| ratio|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:47005cc5-bf10-4752-948b-e8a259355ce2 |
| read only flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b483b6e4-043b-4208-9ee4-444667471b25 |
| receiver group count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:31dd9bf4-c2ce-4431-886a-ccaef586c225 |
| receiver line position|cylindrical position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c50ca0ba-0361-4a0c-addd-023c4f966793 |
| receiver orientation|spherical position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ee4d7178-eb74-478f-b116-c7827fe8e190 |
| record length|byte count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:136b927c-5f08-4e89-9c51-490ae0a3d23e |
| recorder code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bc0f589f-55de-4c3f-8233-f36ffe1383d8 |
| recorder scanning rate|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:edcfd70a-fde6-4f64-9785-1dd4a8edc425 |
| recorder type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6aa171a3-d54c-4f43-bbde-a0428d79fb16 |
| rectangular position|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:082121b7-65ca-4d96-8160-0e87a1b9bd7b |
| reduced volume|volume fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ebbc32b6-4434-4bd0-b9fa-d21df4f1c73c |
| reference location|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ced95ff2-9a69-43d3-82ca-496abeeab8c9 |
| reflection coefficient threshold|reflection coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c6955334-6ee9-4171-bcb7-b3820b9db22c |
| reflection heterogeneity|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:60883eaf-f846-4f50-a2c4-8ced4a2e7a24 |
| reflection intensity|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6f7a12fc-e9ee-4997-a5fa-7a7e52a42789 |
| reflection strength slope|slope|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5121ab96-57fe-43d1-a4d8-191f3dfc3959 |
| refractive index|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d589dc02-4f00-45d4-b5b0-64f4c79ec39c |
| region initialization|explicit index|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d991e97f-734f-4579-952f-74edd5876613 |
| region of interest|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:491fff18-e862-48eb-a90e-69a594e7c7e8 |
| regularization factor|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c664e8ec-b2e3-4f43-b0af-dc102deacdd2 |
| related sample type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:27a4da18-a90c-4feb-9b49-dd6c0c4dc1d6 |
| relationship constraint type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ac5e1d74-1965-4272-bb7b-b27267c89c8b |
| relationship type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:63b4c0b2-cfdd-44a4-88ac-0a743de7adc0 |
| relative apparent resistivity|resistivity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d275b305-434b-4031-a3b7-f8883238e666 |
| relative fluorescence|fluorescence|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:81785a20-5731-4181-aea2-ec429f814b8c |
| relative permeability|permeability|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8e3c5579-7efd-40d0-ab03-bc79452dd2db |
| relative permeability basis|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:09897d58-f7da-429c-bc15-dd074b5beb5f |
| relative permeability function|function name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8da12f31-2235-4f57-8ce5-198d2ed3e36f |
| relative permeability function coeff|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:342b3914-76a5-45ec-b01f-9038cce49dde |
| relative permeability gas|relative permeability|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:87a046a9-1d1a-4b0f-872d-33c6e618927a |
| relative permeability oil|relative permeability|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b7b04c8a-0f04-4f4b-b7eb-a8d68cfe409f |
| relative permeability water|relative permeability|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2ab4741e-815f-4e17-8f46-63cada9df571 |
| relative reflection intensity|reflection intensity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d3d41c36-7f11-4428-bc13-e8a718506e99 |
| remarks|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1b1be262-79a6-473f-b58c-921ae7392c28 |
| remote file|file name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:39cd8ad9-3f0f-4009-8bd5-87da3cea5cbd |
| report flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d5ba8d27-8449-4fed-9fd4-223525a3f365 |
| representation attribute|attribute name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:67eea959-9ff1-4a69-8d4f-cbfcd30f0c19 |
| representation code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:db03ba34-e8f1-42be-a0b1-f3811f4c4647 |
| representation object|object name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:58d7e5c9-e73a-41be-85c0-4fdd02fa8758 |
| representation type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1487f2c3-d72f-49ef-8a81-a96877d5e58a |
| reservoir type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b7545aff-7dfa-4e41-a727-52e44c7bc6d8 |
| resistive inclusion contrast|conductivity contrast|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9752c266-74d1-4bbb-9866-a844332d1093 |
| resistive inclusion proportion|inclusion proportion|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:67122750-884a-4b32-96e0-3c6e29e729e5 |
| resistivity anisotropy|anisotropy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b7dc3597-e552-4108-bf7d-f82db050333d |
| resolution|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:83f42754-4c53-4711-930a-f225a1e67886 |
| resolution description|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:51ff0797-fe2f-4311-8381-443c4915e0da |
| response equation|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:909bf846-032c-4c8d-b3f9-880da466f78e |
| response parameter|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e1639a69-69cf-4ea7-91cd-1bccfd793f90 |
| rheology model|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:91f39645-4b91-4d2a-aa92-3f62235de13f |
| right handed|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1d3859f2-0b3a-41ad-9691-f5a28d949873 |
| rights type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d1828c2e-2ab6-49fd-b198-aab71b5e7a6a |
| rigid flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1abc3bc9-15de-4159-b2b9-936b3625f97c |
| rms amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:356961be-75f2-436c-aa43-d2f5de118a58 |
| rock class|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:87301071-4bc6-42f4-997d-cd76d4fc3209 |
| rock code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f8990ced-053b-48a2-9f0c-acb50b09cb38 |
| rock compressibility correlation|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:aa0f79ce-51de-4e33-8f45-7ce66b314dd7 |
| rock genesis|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0e785f58-fdae-43e3-9bd0-9cfdb6e613f9 |
| rock subclass|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1add30f2-dbc9-4681-98fb-3c6f842cf2aa |
| rotation sense|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f27da821-620f-40db-8206-e2b3db219b31 |
| rotor lobe count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ed30b02b-470a-403e-a52d-b78fc41fde98 |
| rotor number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:05fce3c6-e119-41f4-a509-c34173cb48a3 |
| roundness|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5eaab105-77d6-46ad-b45e-c9875d46d1e7 |
| rover amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cb2051ed-560f-4c00-a51a-582d9ba1e2a2 |
| royalty|money|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c0b59bf4-1e37-4c25-9ba2-4c4f8e63b9fe |
| rp66 array code|array code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:95d86e0f-8665-427d-9345-cf3b714ad44d |
| rp66 index code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5f46b089-599c-4715-bb6e-e4825a367109 |
| rp66 parameter code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fe891339-21e9-45b8-9854-24704625d138 |
| run number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8b674d13-e509-40f9-b3a7-e4e571c26eb4 |
| running direction|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0419ec71-7fc6-4f86-beea-edf77781a7c9 |
| safety drill type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4afb1953-5252-4c0c-95a4-d886e4af603a |
| sale group|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:784edb29-7cb6-4fc1-a240-81a2ed0422b8 |
| sale number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1a341c65-d449-4122-8a1a-7bd4b17969e5 |
| samarium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9cae5d68-f3ea-4158-a47a-0591cb1f4afb |
| sample count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6c49a611-af7d-42c7-9c74-85b6fa39b966 |
| sample gas oil ratio|gas oil ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b5c0a843-fc1f-4fa8-ae6f-0a394b932372 |
| sample interval|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:99877586-08f2-4346-9747-104add867a21 |
| sample number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:901263d8-2075-4351-9f66-44a8994cc24d |
| sample type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b4814b22-b2de-4d51-83a0-a880fda01568 |
| sampling bottle code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:85239ba2-243d-48ba-94ac-96c60c7dde56 |
| sampling bottle number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:55f1d82b-58d6-43ec-8e38-748160e20fa1 |
| sampling bottle type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0a1c6b65-bcc2-4fb9-a597-05bd2969f9ac |
| saturation exponent|exponent|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cefefc62-682f-4706-9b43-7d8fdc5672e7 |
| sayers anellipticity|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5921c7c6-9f8b-48be-b7f2-c6ed74f587db |
| scal flood direction|direction|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d66e2a2a-bc96-4120-a8ce-39b480a04c3c |
| scale factor|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:367b27c1-698a-4a74-93e1-6f337b707314 |
| scale x|scale factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9cdd485b-1008-4422-8977-2741f8152261 |
| scale y|scale factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bb1286e8-39ea-4224-ae79-b7b5c6d53948 |
| scale z|scale factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4fae6198-2904-4e1a-be07-211faef5c470 |
| scan type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4d12a3f2-1d28-4904-b7f4-c2796d6b0629 |
| schoenberg ellipticity|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f7fd1988-d7ed-439e-8e3b-55101c08c564 |
| screen arrangement|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e943217a-4a4e-451c-92cd-2f9f59a27e88 |
| screen count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:77745520-eea6-4e42-92f3-f6535cfc2dd0 |
| screen motion|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f1249308-56b5-4b08-a6b5-dd7859b586b6 |
| screen tension type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:26390054-b60f-4fc3-b11f-5916b3c73799 |
| sdm domain name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:288e495e-2703-45b6-8544-213fca3b49ba |
| second derivative|gradient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a9b9e1e6-b9d3-4334-acc4-46f4c4b3544e |
| section number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7026f1f3-cf66-465f-8eba-f446529079fe |
| section suffix|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:52263fa7-8c65-4c13-a7f5-388ba73afd0a |
| section type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1a5ceb58-1899-4e3d-b75c-d0f00bac15ec |
| sedimentary channel type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1432b79e-8e69-49b1-afec-17609ecd43fa |
| sedimentary feature type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:401ed2d9-4b08-41da-8c3a-a4bd124fd3c9 |
| sedimentary structure type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dc476e0f-8c40-4ca9-9d7c-e9f0cfabc2db |
| sedimentary unit type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9e6cebae-986f-436f-807d-9ba2af534012 |
| seismic arc length|acoustic amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6b37b059-80f9-47c5-ae2a-b4652ab12c1b |
| seismic flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ebcaf6c9-45a9-46d1-a601-f4ae80786828 |
| seismic object type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3809d201-45cb-46f7-821c-5df7c069e7ad |
| semblance|coherence|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9296789c-a958-4025-b9ca-81e73091a1a2 |
| sensitivity|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d7824a09-f9b5-4569-874a-fbb419176d47 |
| sensitivity to temperature|sensitivity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:185886cb-8a00-4718-8db8-d8ddc02b0004 |
| sensor code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:36f858bf-d021-481b-b8cf-4e5f9c1284fd |
| sensor number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f68cef07-efd6-4101-89af-38f0151fce9f |
| sensor type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7853a4ba-7112-46c9-9d6d-df887a4a7771 |
| separator code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:79bdcbfd-3653-4944-8a71-63cadca6bfc3 |
| separator line|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e6a004fb-5b7c-423e-8580-62d7e8f287b3 |
| separator type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:da6c2477-16fe-497f-b013-11d3654d37cb |
| sequence boundary type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0b9989c3-c900-47c8-8de6-6a5260ef6b28 |
| sequence increment|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:92934b86-f317-4c55-88e7-64c825fa66c1 |
| sequence lower limit type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:66f03529-a172-49ac-b6e2-0eb50d1635c3 |
| sequence number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:efee6097-a6cc-45ef-9e71-685c7ab32f3c |
| sequence order|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:47d37bdb-e5cb-41d4-af47-d018f55f1a94 |
| sequence surface type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:56f26589-5bd4-4e22-b248-4e7a91cf5507 |
| sequence trend|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ea637ddf-e8ac-444c-95cb-149521d4500b |
| sequence type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bc70c627-48ef-4416-b646-021eb60b78c5 |
| sequence upper limit type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7aaecd45-fed1-4a13-a0bc-9f7996cc4b00 |
| serial number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:db60975f-c67d-49c4-a420-4330e312e87e |
| service code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:076765a4-8132-41c5-b130-304de94a0110 |
| service type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a93c9007-6fcb-4d2a-862a-ba4ce9296f85 |
| setting mode|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:08aef1b6-e475-4694-81da-f3c7b452e751 |
| setting tool|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:67b256d9-32bf-4ba8-bafa-56350a97a0d1 |
| shape|geometry|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:621f60df-923a-41db-baa6-475d7a4b592a |
| shape code|array code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:87c82726-9b99-4937-98d3-83de330a6155 |
| shape factor|geometrical factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dab1ceb9-3be3-42c6-a0ef-c0b226bcd373 |
| shear failure mode|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cabec89f-a4d1-4fc2-a78b-d20f1d6867f8 |
| shear impedance reflectivity|elastic impedance reflectivity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f59dfa89-12a6-4054-8384-62b5efa6943d |
| shock level|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:52c5f0ff-232a-4a54-9523-05b679faca36 |
| short name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f96bc0c7-a0be-45e6-b66b-38c4273d28c4 |
| shotsequence number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:094f6dc9-d1e6-4081-8ba0-33b50433a575 |
| show description|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9e2fe1d6-3621-41fa-a5e7-f4867d6524df |
| show distribution|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2d0aeea0-daf3-477a-8421-d0333e5cdf28 |
| show flow rate|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:aa0e0d2b-f51d-46c5-a5d8-d4b1632a969e |
| show quality|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8d6c0a0e-03fa-4926-8d8b-7c406d085d6b |
| show type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:80e61700-5466-421b-ada5-517ee87169cd |
| shrinkage factor|volume fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b7bc23bd-4ad6-4712-8552-2bc0c740fc8f |
| sidetrack number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9aba48a6-6557-4210-a300-e951de8e82a8 |
| signal amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f77f05c6-73e1-4b58-9ee9-f5bed58ce100 |
| signal type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0e485001-efbd-4497-834f-2d9337612250 |
| silicon yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5a1bde1c-9faf-4663-bca1-184d31b16564 |
| simulation layer name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:782c1138-e183-48c9-b14a-496c03933ff0 |
| simulation multiple count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:18373f36-e803-471b-b303-a4ea94200073 |
| single shot flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d8d7b186-0a66-426c-8994-c7a6b9c132e2 |
| skin factor|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3bbd162b-0841-4c86-89f9-386f6f6d1e9b |
| slackoff friction coefficient|coulomb friction coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b02ac024-1231-49dd-a2fc-848f6b58358c |
| slant rig flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dfaaacdf-9a5a-4006-9402-ef4f10edcf37 |
| slope|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cde76840-640e-477c-8ce5-0db132d17b66 |
| slowness time coherence|coherence|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d3956aaf-fff8-4c28-b312-a392399bf0bc |
| smoothing mode|processing option|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:649bda3e-6c54-446e-b958-610443b37425 |
| snap criteria|data mask|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bb98bdbf-7d34-41f2-b12b-035ffab144c7 |
| snap quality|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a59a1c16-1896-4c79-b5e5-541e97e868e3 |
| snap type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:73089652-670d-48f7-a872-693ee18f683b |
| sodium chloride yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:86589149-cef5-4a67-a84a-bc5e68bcad91 |
| sodium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e3f2073a-146d-4b63-85bc-5cec5e2fecbf |
| solids control cone count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b2fcba55-1f86-4854-8987-a44ef3bb6dd1 |
| solubility|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6d846da5-6c2f-4579-88c1-a0d6e0a29c05 |
| solution gas oil ratio correlation|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0998e272-e75d-4a84-a39c-eea64cecf091 |
| source|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b186c384-a744-447b-bd5b-3359d2ffd4b4 |
| source file|file name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0d8ab494-3860-4700-9c76-f16c6ab45ba1 |
| source line position|cylindrical position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f756edd5-8410-4453-b982-f460b82a1e90 |
| source orientation|spherical position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1da48967-4cbb-4444-8757-8a2b1ecff5fa |
| spacing x|interval length|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e4c4c4df-a9e3-47b6-bf48-1104f4bdc007 |
| spacing y|interval length|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:eb12f930-8502-4024-b856-3ccb78a64451 |
| spacing z|interval length|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e479a606-f83d-4873-b621-3ccc20f4bddb |
| sparse 2d data|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7b0de94c-29d4-4326-9b43-61b91f9595be |
| sparse 3d data|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:29e1fa7f-872d-49d0-9db0-452844f04c5d |
| spatial gamma radiation intensity|gamma ray||New PropertyType item: 9148f86a-7164-4cf2-aced-dc5981c1f189|{{NAMESPACE}}:reference-data--PropertyType:9148f86a-7164-4cf2-aced-dc5981c1f189 |
| species|biological name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:49b74f36-9975-4e53-b160-d43681fd75b8 |
| specific gravity|density|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a286a841-0a78-4128-93a2-a9a05d57c7ba |
| specific heat|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:65ed03db-e71c-4ae4-9fe3-d2d9e78fdef2 |
| spectral fluorescence intensity|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:57fb1571-9ab5-40b5-b3bd-575ddc1d1efd |
| spectral gamma radiation intensity|gamma ray||New PropertyType item: a53689bb-ed12-4685-9f03-7582c536e146|{{NAMESPACE}}:reference-data--PropertyType:a53689bb-ed12-4685-9f03-7582c536e146 |
| spherical position|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2a97b4b9-74fd-45bb-8f48-b5592d93a535 |
| sphericity|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:20720bcf-8d45-4cb6-b306-11db7816684e |
| spore color index|color|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a283af3d-5d8a-4653-986e-41718cd80618 |
| spot description|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fe33c2ce-42d7-4239-92fb-4a1fb36cb8be |
| spring number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f383a285-c921-4698-854c-e9449e10eaeb |
| stack alkhalifah eta|alkhalifah eta|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:662e28ed-6fc2-4c1c-89cf-a57e281741a9 |
| stack bale kappa|bale kappa|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3be8bfe2-341e-404a-8184-ef1ad7658e65 |
| stack bale xi|bale xi|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:645564a9-b4f4-49dc-8444-b7397be219a0 |
| stack number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:660f4256-6cff-4295-a37f-df540fb3f973 |
| stack sayers anellipticity|sayers anellipticity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d2f05046-c96c-4326-97c0-39a940eb1c98 |
| stack schoenberg ellipticity|schoenberg ellipticity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:72950538-d41f-4731-b3c7-410f02d3d1a1 |
| stack thomsen delta|thomsen delta|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:caef214b-6d3f-4763-bd32-60f4c00370bc |
| stack thomsen epsilon|thomsen epsilon|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:562621f2-a7df-470a-88fe-a5e9b64bad65 |
| stack thomsen gamma|thomsen gamma|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3ea8434d-2d45-4b2e-8145-6d26024556b5 |
| stack thomsen sigma|thomsen sigma|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b069a9e4-38e2-4fa3-9468-87637319e047 |
| stack velocity ellipticity flattening|velocity ellipticity flattening|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a613459b-f623-4394-8ae4-86a1bd065fa9 |
| stacking method type|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b022ab4d-840d-47ad-aaa7-f2769efa1d04 |
| stage count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:76340257-bffb-4afe-beda-d83c6dcd4d0c |
| standard deviation|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:010fb877-2487-425a-a1be-7168f8630cc1 |
| standard document identifier|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1c4295ff-828a-4ea5-8c19-2fd48130363f |
| standing oil flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7facc827-1004-4b71-87af-2d56c01dbf7e |
| standing water flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1ed40c65-ca32-4fa5-ad73-d1b625441a3e |
| standoff ratio|standoff|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:89dc0be5-8b48-4e71-8dae-5da2adbfa157 |
| start age code|geologic age code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7469e610-2624-404f-8d8b-a735d969db58 |
| state code|geopolitical region code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:65f4cd67-deb4-4e0c-8f23-9d7b519c93c4 |
| state name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:576d9fa5-837f-41ff-8bee-740ea273e779 |
| station inflection|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0922df28-676b-4ebe-9302-b3e3d50bfb63 |
| station number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:37ad0d6d-df79-45a4-bacf-c60657401b8c |
| statistical property|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8298cd02-e058-4619-abd0-3e64bddd5491 |
| statistical skewness|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b1607d3d-ea0e-4ec4-a432-88f5821eefa0 |
| status|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4b4e5316-c291-46d4-8bd5-ff05987cb941 |
| steel grade|grade|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a0370748-6efb-41e7-a266-78003a27b474 |
| storage capacity|volume fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:447ad177-d4c5-4f09-972d-c91cb2c4ce9a |
| storage label|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4e221cff-43e4-44df-9c68-647808fb1b83 |
| storage location|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1e880c74-2568-45fa-8339-ec85187ae664 |
| storativity|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c2d5c863-2cf7-4641-9e1f-ffccebbfa4f3 |
| street address|address|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:98074d9f-e200-4374-b310-8b52758d672f |
| stroke count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:056ad531-96f0-4b97-a044-b35d4c8d15b6 |
| strontium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:11c62894-dbf3-4e56-aab3-f742127e4b30 |
| structural dip removal flag|processing option|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:29cd4d3f-ca34-45f4-a4b7-a95740845b59 |
| structural dip type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a93e3943-53f2-4e2f-81d3-d09da12ff3e9 |
| structural model type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8efd39c1-766f-4d60-90b1-8fcc08e1f2c8 |
| subgenus|biological name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fc985c4b-e3df-4516-bafe-4d77867b0c93 |
| subsea equipment type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6b604c06-3f7b-4e93-b7a7-f410b33d6c80 |
| sulfur yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a3b40b77-32f5-4d93-8405-7d3818b568c3 |
| sum amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d2d95674-7ae7-4f6a-b622-f20d3e3ab171 |
| sum magnitude|magnitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c315ebbb-be8c-44dc-b476-a402e9971226 |
| sum negative amplitude|sum amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:03ee5564-0a7b-4926-956b-7043da824860 |
| sum positive amplitude|sum amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2cb6855e-8533-4a3a-8f5d-d303f7a3c56c |
| summed traces number|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0207f7d8-b05a-421f-90d8-47f945730888 |
| super array code|array code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:01fe90e0-c219-48dd-bcc7-c7ed2d2db537 |
| supervised flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:31fe0c10-f656-4ea4-9e25-69145466be77 |
| supplier|organization name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cfe43b9b-7942-41c8-8c89-674802a4d917 |
| surface location|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d7612c69-7771-4163-9ae7-b5c8056a291f |
| surface reflection coefficient|reflection coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0a548932-7fad-4b45-9cae-2837b47680ec |
| surface sensor code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1a6e1f8f-3eca-4a62-b2e8-ee52af135bc5 |
| surface sensor type|sensor type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:82b21c48-f89d-44ae-b2be-6c587a8172fb |
| surface surface intersection type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:736caa55-93ed-4508-a6a0-e28758ee7210 |
| surface type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:750c6858-08ba-4081-9dff-200b986213a6 |
| survey east west|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bdddc4fc-71f8-405d-a438-b0de91bf7c82 |
| survey north south|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:880e25c2-0c32-475b-97e9-a65d81a3e930 |
| survey type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a3114654-859a-4a18-aebf-c3adf83c12c0 |
| synthetic seismic section|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0e75b53e-841a-4b24-b094-29eace263661 |
| synthetic seismogram|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4a6a6c68-7b50-4224-ba35-ae47073e05ce |
| sys copy number|sequence number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0f43c9b4-c7c5-41d6-8e35-106dff833ce7 |
| system identifier|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:97b414c0-d093-451d-9410-78c26807bd8a |
| table name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:49829c91-f1fe-41b2-a1ae-2e68c8281e3e |
| tank code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6a7ff6fc-ce29-4e9c-9aa6-69dbf9556220 |
| tank type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e43db622-6cff-4143-9374-83a59b1795f1 |
| tape density|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d6774099-1b07-49df-ae73-0343e3845136 |
| tbg grade bearing|drill bit grade tbg|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5849c89f-cada-4e9c-a161-31cc0b449c39 |
| tbg grade broken teeth|drill bit grade tbg|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c9629e2e-42ec-4071-ba3f-3db8c43d49c5 |
| tbg grade gauge|drill bit grade tbg|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9c7f0652-ceb9-4bce-ad01-b0082b2e2215 |
| tbg grade gauge oh|drill bit grade tbg|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:248d7aaa-913f-42d1-bf6b-4429bedfa2a8 |
| tbg grade tooth grade|drill bit grade tbg|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7122bdf6-44d2-49c9-b504-3c4570d83624 |
| tectonic cycle type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:93515270-4847-49b0-bcab-f81482067260 |
| tectonic feature type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bb8a9afe-c48c-4dac-ba00-8f9e1d15d258 |
| temperature accuracy|accuracy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e622b3ec-bdbe-4803-8d48-c0a150022b94 |
| temperature sensor code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ad72ae06-a5fa-49de-9966-e4ee45cb29dd |
| temperature sensor type|sensor type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2bd06eb4-f595-4608-bffb-56f61cbd08a7 |
| temperature unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9356cd6a-3fad-4542-ae5b-f44b76464da5 |
| tensile failure mode|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:61d276cb-5f1a-489c-94ba-16b2d110e79e |
| terminal mode flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4307e337-c7f3-40db-a34a-3ef0fee81113 |
| test code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9b59c299-23e8-462c-87e0-2e3bff6945f7 |
| test number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ebb1e7d0-af26-4846-88a6-d35ec5d6c278 |
| tetrahedron code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:940b7a0c-1f85-46ba-97ad-8dd4a392239c |
| text|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:de7e9a59-c808-4dce-9f90-4b27600096ea |
| text location|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f2633ec6-6e7d-47e7-a638-4269ce292049 |
| thermal alteration index|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a8775e50-2c53-4cd8-b141-99c4e290014c |
| third party certification|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ab1a6600-e983-4089-b785-1867ad47e30c |
| thomsen delta|anisotropy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c3cadbc2-059f-43bc-8239-6914e5b89b82 |
| thomsen epsilon|anisotropy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c0e99bca-417d-4697-87be-f242caf3f48e |
| thomsen gamma|anisotropy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ba336529-fcd9-42dd-8351-a64cd9be6b4d |
| thomsen sigma|anisotropy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:da571ef5-ed33-46fb-9b52-f807ebb735fc |
| thorium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1b8d1e12-b259-468c-9c96-fdbeb60a9a6b |
| thread type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ea9324af-d3b5-42f2-ad95-1ffb6e36df21 |
| threshold|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d332eb2a-b760-4f22-bd79-edf92ea54f72 |
| tie point time quality|correlation coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6038d02a-19aa-421f-86e6-4c883a64f703 |
| time unit|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:782955fa-fba0-4e29-a5ce-e786dfe63e67 |
| titanium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:26281f2f-7c4b-4b82-b85c-24ed7bbda6d6 |
| title|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d1959f06-3054-40b9-9312-391b084157c3 |
| tko model order|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ec70dd7b-b5ae-4582-9dd0-9984aeb07560 |
| to conn number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3d3864d6-6b39-4c6a-a89f-6b82885632a7 |
| to datum code|geodetic datum code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2000ffbb-7837-45d7-b8fb-c8b6c4e46c11 |
| to entity code|entity code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:720c3011-b76e-4c20-a81f-932aa8cfef53 |
| to property code|property code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0d33f2bc-068b-4ff0-8f31-120d116d86a4 |
| tolerance|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fd88399d-bed3-4578-a064-fe7050542a60 |
| tool background yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:514f505a-0012-49b5-afa0-c32ccfe9b2e6 |
| tool control|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a764bc44-8842-4560-8829-a8642060c553 |
| tool number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:01126a81-62f1-41a8-9814-2ea296fbb232 |
| tool status|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b240fd8c-e494-4cfc-a3b2-16fdf527e28b |
| tool type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1d2d4b96-397f-4580-8f80-a9c7dae7a21c |
| top dip number|dip number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8974d4d1-7b75-4645-a257-0488b5773732 |
| top discontinuity type|discontinuity type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0c8fd886-07d7-445d-b41d-6fcb3cdc8616 |
| top location|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:758d9f6a-634a-4d75-b20f-b2d0ed9eb036 |
| top value|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:118c9d79-0f1a-44e6-ad6b-4ee79e085b96 |
| total geometric spreading|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:330219c5-a21a-4b1b-ab43-8422bd406fd2 |
| total skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:311763d0-f787-428c-89a8-953684bf4e37 |
| township number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b0d7c01f-b614-4fa7-be18-64548d548c02 |
| trace count receiver|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b31a75a0-e82c-4819-ac7b-d6672a0de7db |
| trace count source|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:08fdf694-3840-417a-903d-10b07ff960bb |
| trace elements|description|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e21102c6-c862-4241-809c-316a53501546 |
| tracking quality|quality indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:528f17fe-e143-40a1-8453-88e5a00f70e7 |
| trade name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2f4bc3d1-3ff1-484d-b654-547af1656381 |
| trajectory|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:d6388bb7-15bd-449d-9f82-d7e67b45d91d |
| transfer fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:138a1446-8ee5-481e-b901-26b46145c44d |
| transit time accuracy|accuracy|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:427915b0-9b4b-4ee6-a7d2-e04b85a174c1 |
| transverse amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9ad9ba5d-956b-4a85-a24f-1aadfe50f512 |
| transverse dispersivity coefficient|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:40a9bb45-e539-4bec-aff4-9e95b9c0eeb0 |
| transverse isotropy coefficient|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5c340840-1c47-437a-b14f-703f7aa8a97d |
| treatment flow path|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ebe2ade9-7f29-4bb4-baf6-5dbe0fd27849 |
| treatment number|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:922c88ae-a781-4589-a6d7-b4f81e2c67da |
| trough peak delta amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f8618461-8138-4e9c-a00d-8e5c147af0c5 |
| trough trough delta amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e2431b4d-b5aa-4ac6-97c1-ed748d96f964 |
| true common depthpoint number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1de5f7bf-1172-4dc8-b9d7-263540bdb934 |
| true common midpoint position|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:77bcad4d-b0ce-4c35-ab39-88f697e69567 |
| true receiver line position|receiver line position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b3ae57e1-6759-4f42-92a3-d9d4c222d7d2 |
| true receiver position|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9c3fafe3-b51a-4d85-a12f-41f7970ee6b9 |
| true source line position|source line position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:78f64c28-4bc6-4f17-b1a7-65838cc9abbd |
| true source position|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:08653b2a-fcc7-4df0-8fd5-bafb47dfdd32 |
| true source sensor position|rectangular position|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:43e533f8-dd48-43bb-8fee-a4bb61dfdc2c |
| tube material|material code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:993e3b35-321b-453f-a68a-223565d03abe |
| tube upset type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:76325757-5f9e-433e-9bcd-b51178264f85 |
| tubular code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2730eb05-2d09-4ab9-b1fc-eae9929e5bd8 |
| tubular string component rank|rank|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:84d9c5b2-a8fa-4fa0-8bff-ca9d204e3685 |
| tubular string connect type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e2a3f2a4-c01b-41dd-a482-8c11bf0b0edd |
| tubular string location type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3f0656c2-1cc9-493f-ae56-ec2a55a5bb91 |
| tubular type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b5f771e2-56c8-4001-99e0-1af52f4221b5 |
| turbidity|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7f0817ad-46d7-44b4-8b72-6841932b3bfa |
| turbulence coefficient|indicator|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:40c03fba-ac20-43f0-a2ce-d332bbff15da |
| tvd computation method|method type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9ea32dca-6ba3-4d3b-9be8-1b9f1a248337 |
| type|property|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1cc7cf34-4eaf-4ad3-b440-5a74b443cc73 |
| uncertainty|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:10cb08b9-c0f3-45b9-ae6a-6bcd263e3ef7 |
| unconformity type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:22013755-66e6-43b0-895c-709ac0e114c5 |
| unit attribute|attribute name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:45c55781-cc8a-46af-8ae8-21cff79b3bc7 |
| unit canonical form|unit code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:85384571-0fbe-4e25-8e73-fcf9c7ac8c9d |
| unit code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:f895deb4-4f57-4d3e-98c1-0496669f2253 |
| unit column|column name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7c940618-73cf-467d-b8a7-3a57761d51a4 |
| unit cost|cost|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0484b569-000d-4095-9d62-cdef1e0d8d90 |
| unit dimension code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:14d77cb1-039b-4832-8327-73fec5702d62 |
| unit namespace code|namespace code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1dfc46e5-1e98-4298-b09a-3a06d8f542c2 |
| unit object|object name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:785dec4e-6c77-43db-9f9b-0e01eb2ba3d3 |
| unit quantity code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0aa2c95d-a124-4360-9118-368afd3896f6 |
| unit quantity column|column name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:2dd741de-ce10-4868-8ad6-f90072e051e1 |
| unit quantity table|table name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:28aecab7-ccff-4d77-a3e8-49f87d367376 |
| unit system code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:678b1ed6-81aa-4ce5-97d8-199ea9071f76 |
| unit table|table name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8b311ba0-221e-4440-b974-eebad59922da |
| unknown|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:73f28137-03ef-45b4-8e85-31869c7d36fd |
| upper fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5f663637-d728-4d01-ad9b-27e45378ad2b |
| upper surface relation|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:aa90605f-cd88-4d96-936a-818a65fdcfb5 |
| uranium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:70fb90f6-7a31-451a-bbd4-26f090d6aab3 |
| url|address|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c7733c57-2c70-4ccf-bfd2-c7da5b5f39cc |
| use checkshot flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e645cae4-1dc8-469b-ad60-d4694ca3ffaf |
| user data ident|data mask|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bd5a48c1-f286-49e3-af5b-c8298cb53d85 |
| user evaluation code|data mask|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3701179f-9c00-4a4d-9ef6-be024e30617f |
| user trace selection|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bde575d1-686c-4893-8ff0-75e4d7636b30 |
| uwi|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:a77dfdb9-76be-4c06-b920-f25f07b54413 |
| validation flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:77518297-33b4-4955-9ab0-678d96069265 |
| value domain code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1adb9819-228d-4d83-b926-62ba4035106d |
| value long|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:dbbdc27a-c23f-4371-aa00-519d811d6c09 |
| value number|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:73e1b5c0-7839-4bb3-a156-2e3b4f1af69d |
| value string|multi use property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c1bb9d28-6d72-4690-a233-285bf7cf24d8 |
| value type|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:fa4f70b9-8433-429b-9ea3-24fb94a65ebe |
| value type code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:00ba36d9-3e3a-46d9-99c0-0026da6341b4 |
| value type column|column name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:83c768c3-6944-4710-9486-ecb6c52c6607 |
| value type table|table name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1e7ca046-0747-4393-b7b6-deb14a260eeb |
| variance|statistical property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:69adc1a5-4f6f-4185-96c6-f9c8904049ca |
| velocity alpha factor|gradient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:c7d5b90c-7d58-4dec-bba5-4370e5516bdd |
| velocity beta factor|gradient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:41f8befd-4045-4f65-ab21-2dd519231163 |
| velocity gamma factor|gradient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7a5ad8da-4b08-4931-8b7d-4a50ac15718f |
| version|identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:25d8ffec-833e-48c1-84f6-c138137bc5e3 |
| vert hor permeability ratio|vert hor ratio|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8167e84a-8783-4d9d-9b01-a35db463c699 |
| vertical datum code|code|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3e715991-35c3-47aa-95b6-0898abe3f55c |
| vertical datum name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:72a72265-dced-440c-a157-2a72e514236b |
| vertical geometric skin|skin factor|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:911d8404-c705-403d-9199-85a507100e2d |
| vertical lift flow regime flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bab08b05-f71b-4e29-9fc3-20e4c929c449 |
| vertical section origin|location|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0ce92e8c-e749-40f0-9c70-8db0621f973f |
| vitrinite reflectance|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0ee39899-2a78-43de-8d51-223f0ad631e6 |
| volume per area|length|UnitQuantity:length|UnitQuantity:volume per area|{{NAMESPACE}}:reference-data--PropertyType:7727dab1-b252-43b2-bf6f-c3fbfd9a3d43 |
| volume reflection spectrum|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5af5de4c-0667-48f2-98e1-51b98b7987e8 |
| volume type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0636ef34-dd7e-46ad-8d6e-a590f8c0f142 |
| volumetric thermal expansion|thermal expansion|UnitQuantity:volume per volume|UnitQuantity:volumetric thermal expansion|{{NAMESPACE}}:reference-data--PropertyType:393f93c0-257a-41f5-b6ad-6e93ea7bc6ee |
| vsplib waveform header|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3ae8ec6c-6561-42bf-8b78-bf84abeb82f6 |
| wall building coefficient|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6bd05e5b-f32f-4c13-9638-cfffe4975b10 |
| water activity|volume fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:49615e4e-3b61-4c18-9fe6-c281b6e64e61 |
| water cut|volume fraction|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cd9fe5cf-983f-4951-8e8e-ea0697507556 |
| water gravity|specific gravity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:02dbc6b3-4bb2-4477-83b9-2e10d3589850 |
| water holdup|holdup|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:cca7e6c8-cf41-4616-bea3-f745714080bd |
| water rate meter type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8ac515b8-1539-411a-85a0-620ce249ec19 |
| water viscosity correlation|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:879f6aa8-b0d9-4976-89b5-db5bf8f81a0f |
| water wettability index|wettability index|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:8ea9188d-124a-4ffa-bb79-61a6c969e6d9 |
| waveform number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0066fba1-b99e-4f5f-9dd7-55345dc9cf94 |
| wavelet type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:13b61235-17ca-4f2b-a32f-7be4b7211d67 |
| welded section flag|flag|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9f195ee6-c1c9-4dba-9172-fab04402d07f |
| well count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bb3e0417-be75-41a3-9c3c-c77eed079cc5 |
| well job count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4d71b83b-7b01-4f36-a354-4098e1bfcce8 |
| well name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:348c9a4d-e3e4-4beb-8456-f07a46660de1 |
| well production control mode|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3ff47cd7-7ca4-476d-973d-b417e7cb903c |
| well projection type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:14a112a3-79c3-43df-a13c-b7e8d4a71c24 |
| well status|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:9794af62-c2f8-4a6c-99fb-64fd793687d9 |
| well test model|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:3f5209b8-b8d7-4fab-9058-4fdcaf125c0c |
| well test model type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:bd7dbe61-c5a7-4331-877e-8b8939de4be7 |
| well test type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:e92f76b8-a125-41a1-ac75-db96b52e2cbf |
| well treatment fluid type|fluid type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b9d11c64-f648-4216-8228-5ddd1278bd5b |
| well treatment type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:941bdb95-652d-4534-9be4-0758bc4e1ec5 |
| wellbore purpose|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:12476c38-a2ae-4a1e-bc39-28e8e3dce443 |
| wellbore shape|shape|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:4ff73959-1c6e-4810-88e7-748293ae00ca |
| wellbore status|status|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6f145a8e-fdde-44d2-bf50-9214e70a2843 |
| wellbore storage type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:7d87cad9-56e8-4134-b176-47952ff5d09c |
| wellhead connection type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5fb65fe6-3e20-442f-896a-b8c795913034 |
| wet gas gravity|gas gravity|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:08b532ce-3215-41aa-a7d2-944472418aa5 |
| wettability index|property|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:6be368af-6bef-43ac-bed7-81a8744e5dac |
| wetting type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:44c276db-3d29-49c8-abd8-001b41b1fd37 |
| wireline tool type|tool type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:980dbb79-fc1b-44ee-97b1-8d1b56081e4b |
| x display identifier|system identifier|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ab811a94-65a6-481d-80f8-3eb7d5d1f3e8 |
| x transverse amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:1e6e5c81-17ec-47be-ba9f-aee9ed4ed2ec |
| y transverse amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:5d6846ec-6f6e-4c40-ad1e-84c9bc035845 |
| z factor|coefficient|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:ec599c30-31fe-474a-b8f9-2a86934a8177 |
| z transverse amplitude|amplitude|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:568eb132-cca7-4a55-8918-ddad3705291b |
| zero crossing count|count|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:78880b3b-da34-4aac-9c69-f62964b44807 |
| zinc yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:12466246-7212-416f-84ff-66e78630b29d |
| zirconium yield|elemental yield|UnitQuantity:dimensionless|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:b0fd3805-742c-4241-95f9-00e85f3d46ed |
| zone name|name|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:361d0090-3596-40c6-979e-2e3537e87adb |
| zone number|ordinal number|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:0b1c9457-2921-4de0-9dff-e5f555ef270b |
| zone set type|type|UnitQuantity:non-dimensional|UnitQuantity:none|{{NAMESPACE}}:reference-data--PropertyType:44cae4ac-3efe-4eab-94be-482c149f64d0 |

[Back to TOC](#TOC)

## NameAlias Changes

Total number of alias changes 3320 in total 3629 PropertyType records.

| Name | # previous Alias Names | # of current Aliases |
|---|---|---|
| api gravity|0|2 |
| api neutron|0|3 |
| gamma ray|0|3 |
| gamma ray minus uranium|0|2 |
| max gamma ray|0|1 |
| spatial gamma radiation intensity|0|1 |
| spectral gamma radiation intensity|0|1 |

[Back to TOC](#TOC)