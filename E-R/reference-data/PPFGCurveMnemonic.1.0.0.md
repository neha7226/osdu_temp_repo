<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# PPFGCurveMnemonic
The mnemonic of the PPFG Curve is the value as received either from Raw Providers or from Internal Processing team.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--PPFGCurveMnemonic:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: None
* Reference value type, governance model: LOCAL
* Link to &rarr; [Authoring Schema PPFGCurveMnemonic.1.0.0.json](../../Authoring/reference-data/PPFGCurveMnemonic.1.0.0.json)
* Link to &rarr; [Generated Schema PPFGCurveMnemonic.1.0.0.json](../../Generated/reference-data/PPFGCurveMnemonic.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource PPFGCurveMnemonic.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/PPFGCurveMnemonic.1.0.0.json)
* Link to &rarr; [Example Record PPFGCurveMnemonic.1.0.0.json](../../Examples/reference-data/PPFGCurveMnemonic.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams PPFGCurveMnemonic


## Outgoing Relationships for PPFGCurveMnemonic

![PPFGCurveMnemonic](../_diagrams/reference-data/PPFGCurveMnemonic.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## PPFGCurveMnemonic Referenced by other Entities

![PPFGCurveMnemonic](../_diagrams/reference-data/PPFGCurveMnemonic.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# PPFGCurveMnemonic Properties

## 1. Table of PPFGCurveMnemonic System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-PPFGCurveMnemonic:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--PPFGCurveMnemonic:27d01767-08e9-5e93-ab91-055ebe654db7|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--PPFGCurveMnemonic:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of PPFGCurveMnemonic Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of PPFGCurveMnemonic Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of PPFGCurveMnemonic Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for PPFGCurveMnemonic.1.0.0

* Status: Published
* Governance: LOCAL
* The table below shows all 97 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [PPFGCurveMnemonic](../../ReferenceValues/Manifests/reference-data/LOCAL/PPFGCurveMnemonic.1.0.0.json).

|`id` with prefix `namespace:reference-data--PPFGCurveMnemonic:`|Code|Name|Alias|Description|AttributionAuthority|Source|CommitDate|
|-----|-----|-----|-----|-----|-----|-----|-----|
|BOAngle|BOAngle|BOAngle|-|Breakout width, in degrees (angle)|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ESG|ESG|ESG|-|Effective Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ESN|ESN|ESN|-|Normal Effective Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ES|ES|ES|-|Effective Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FA|FA|FA|-|Friction Angle (Failure Criteria)|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FBPG|FBPG|FBPG|-|Fracture Breakdown Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FBP|FBP|FBP|-|Fracture Breakdown Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FCPG|FCPG|FCPG|-|Fracture Closure Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FCP|FCP|FCP|-|Fracture Closure Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FGACHIV|FGACHIV|FG ACHIV|-|Achievable Fracture Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FGBM|FGBM|FG BM|-|Modelled Fracture Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FGSTREN|FGSTREN|FG STREN|-|Strengthened Fracture Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FG|FG|FG|-|Fracture Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FIPG|FIPG|FIPG|-|Fracture Initiation Pressure Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FIP|FIP|FIP|-|Fracture Initiation Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FPPG|FPPG|FPPG|-|Fracture Propagation Pressure Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FPP|FPP|FPP|-|Fracture Propagation Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FP|FP|FP|-|Fracture Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FTEMP|FTEMP|FTEMP|-|Formation Temperature|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|IPSG|IPSG|IPSG|-|Intermediate Principle Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|IPS|IPS|IPS|-|Intermediate Principle Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|LPSG|LPSG|LPSG|-|Least Principle Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|LPS|LPS|LPS|-|Least Principle Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MD|MD|MD|-|Measured Depth|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MESG|MESG|MESG|-|Mean Effective Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MES|MES|MES|-|Mean Effective Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MPDBP|MPDBP|MPD BP|-|MPD Back Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MPSG|MPSG|MPSG|-|Maximum Principle Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MPS|MPS|MPS|-|Maximum Principle Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MRGN|MRGN|MRGN|-|Margin|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MSG|MSG|MSG|-|Mean Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MS|MS|MS|-|Mean Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NCTDT|NCTDT|NCT DT|-|Normal Compaction Trendline - Sonic|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NCTDXC|NCTDXC|NCT DXC|-|Normal Compaction Trendline - Corrected Drilling Exponent|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NCTMSE|NCTMSE|NCT MSE|-|Normal Compaction Trendline - Mechanical Specific Energy|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NCTPHIT|NCTPHIT|NCT PHIT|-|Normal Compaction Trendline - Total Porosity|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NCTRES|NCTRES|NCT RES|-|Normal Compaction Trendline - Resistivity|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NCTRHOB|NCTRHOB|NCT RHOB|-|Normal Compaction Trendline - Density|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NCT|NCT|NCT|-|Normal Compaction Trendline|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NESG|NESG|NESG|-|Normal Effective Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|OBG|OBG|OBG|-|Overburden Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|OB|OB|OB|-|Overburden Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|OPG|OPG|OPG|-|Overpressure Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|OP|OP|OP|-|Overpressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PFW|PFW|PFW|-|Pore-Frac Window|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PNORMG|PNORMG|PNORMG|-|Normal Hydrostatic Pressure Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PNORM|PNORM|PNORM|-|Normal Hydrostatic Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPBM|PPBM|PP BM|-|Pore Pressure from Basin Model|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPCG|PPCG|PP CG|-|Pore Pressure Estimated from Connection Gas|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPDG|PPDG|PP DG|-|Pore Pressure Estimated from Drill Gas|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPDP|PPDP|PP DP|-|Pore Pressure Estimated from Drilling Parameter|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPDT|PPDT|PP DT|-|Pore Pressure Estimated from Sonic|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPDXC|PPDXC|PP DXC|-|Pore Pressure from Corrected Drilling Exponent|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGBM|PPGBM|PPG BM|-|Pore Pressure Gradient from Basin Model|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGCG|PPGCG|PPG CG|-|Pore Pressure Gradient Estimated from Connection Gas|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGDG|PPGDG|PPG DG|-|Pore Pressure Gradient Estimated from Drill Gas|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGDP|PPGDP|PPG DP|-|Pore Pressure Gradient Estimated from Drilling Parameter|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGDT|PPGDT|PPG DT|-|Pore Pressure Gradient Estimated from Sonic|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGDxC|PPGDxC|PPG DxC|-|Pore Pressure Gradient from Corrected Drilling Exponent|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGEST|PPGEST|PPG EST|-|Pore Pressure Gradient Estimated from Log|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGMEAS|PPGMEAS|PPG MEAS|-|Measured Formation Pressure Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGMSE|PPGMSE|PPG MSE|-|Pore Pressure Gradient from Mechanical Specific Energy|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGPHIT|PPGPHIT|PPG PHIT|-|Pore Pressure Gradient Estimated from Total Porosity|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGRES|PPGRES|PPG RES|-|Pore Pressure Gradient Estimated from Resistivity|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGRHOB|PPGRHOB|PPG RHOB|-|Pore Pressure Gradient Estimated from Density|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGVSEIS|PPGVSEIS|PPG VSEIS|-|Pore Pressure Gradient Estimated from Seismic Velocity|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPGZADJ|PPGZADJ|PPG ZADJ|-|Structurally Adjusted Pore Pressure Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPG|PPG|PPG|-|Pore Pressure Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPLOG|PPLOG|PP LOG|-|Pore Pressure Estimated from Log|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPMEAS|PPMEAS|PP MEAS|-|Measured Formation Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPMSE|PPMSE|PP MSE|-|Pore Pressure from Mechanical Specific Energy|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPPHIT|PPPHIT|PP PHIT|-|Pore Pressure Estimated from Total Porosity|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPRES|PPRES|PP RES|-|Pore Pressure Estimated from Resistivity|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPRHOB|PPRHOB|PP RHOB|-|Pore Pressure Estimated from Density|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPVSEIS|PPVSEIS|PP VSEIS|-|Pore Pressure Estimated from Seismic Velocity|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PPZADJ|PPZADJ|PP ZADJ|-|Structurally Adjusted Pore Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PP|PP|PP|-|Pore  Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|PSNORM|PSNORM|PSNORM|-|Subnormal Pressure|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SDM|SDM|SDM|-|Safe Drilling Margin|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SEDRT|SEDRT|SEDRT|-|Sedimentation Rate|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SFPG|SFPG|SFPG|-|Shear Failure Pressure Gradient (Collapse Pressure Gradient)|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SFP|SFP|SFP|-|Shear Failure Pressure (Collapse Pressure)|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SHAZ|SHAZ|SHAZ|-|Maximum Horizontal Stress Azimuth|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SHmaxG|SHmaxG|SHmaxG|-|Max Horizontal Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SHmax|SHmax|SHmax|-|Max Horizontal Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ShminG|ShminG|ShminG|-|Minimum Horizontal Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|Shmin|Shmin|Shmin|-|Minimum Horizontal Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SVG|SVG|SVG|-|Vertical Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SV|SV|SV|-|Vertical Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|TEMPA|TEMPA|TEMP A|-|Temperature Annular|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|TEMPBHA|TEMPBHA|TEMP BHA|-|Temperature BHA|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|TEMPC|TEMPC|TEMP C|-|Corrected Temperature|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|TVD|TVD|TVD|-|True Vertical Depth|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|TWT|TWT|TWT|-|Two Way Time|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|UCS|UCS|UCS|-|Unconfined Compressive Strength|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|VESG|VESG|VESG|-|Vertical Effective Stress Gradient|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|VES|VES|VES|-|Vertical Effective Stress|OSDU|Workbook Published/PPFGCurveMnemonic.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# PPFGCurveMnemonic.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [PPFGDataset.1.0.0](../work-product-component/PPFGDataset.1.0.0.md) | Curves[].CurveFamilyMnemonicID | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._