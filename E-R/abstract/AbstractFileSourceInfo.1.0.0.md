<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractFileSourceInfo [Status: Accepted]
A schema fragment to describe file source information.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractFileSourceInfo:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractFileSourceInfo.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractFileSourceInfo.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractFileSourceInfo.1.0.0.json](../../Authoring/abstract/AbstractFileSourceInfo.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractFileSourceInfo.1.0.0.json](../../Generated/abstract/AbstractFileSourceInfo.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractFileSourceInfo.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractFileSourceInfo.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractFileSourceInfo


## Outgoing Relationships for AbstractFileSourceInfo

![AbstractFileSourceInfo](../_diagrams/abstract/AbstractFileSourceInfo.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractFileSourceInfo Referenced by other Entities

![AbstractFileSourceInfo](../_diagrams/abstract/AbstractFileSourceInfo.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFileSourceInfo Properties

## Table of AbstractFileSourceInfo Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|FileSource| |string|required|File Source|The location of the file. It can be a relative path. The actual access is provided via the File Service. When used in context of a FileCollection (dataset--FileCollection*) FileSource is a relative path from the FileCollectionPath. It can be used by consumers to pull an individual file if they so choose by concatenating the FileCollectionPath with the FileSource. This property is required.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://default_bucket/r1/data/provided/documents/1000.witsml|
|PreloadFilePath| |string|optional|Preload File Path|File system path to the data file as it existed before loading to the data platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://staging-area/r7/raw-data/provided/documents/1000.witsml|
|PreloadFileCreateUser| |string|optional|Preload File Create User|Optional user name or reference, who created the file prior to up-loading to the platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|somebody@acme.org|
|PreloadFileCreateDate| |string|optional|Preload File Create Date|Optional create date and time of the file prior to up-loading to the platform.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|2019-12-16T11:46:20.163Z|
|PreloadFileModifyUser| |string|optional|Preload File Modify User|Optional user name or reference, who last modified the file prior to up-loading to the platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|somebody.else@acme.org|
|PreloadFileModifyDate| |string|optional|Preload File Modify Date|Optional last modified date and time of the file prior to up-loading to the platform.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|2019-12-20T17:20:05.356Z|
|Name| |string|optional|Name|Optional, user-friendly file name.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1000.witsml|
|FileSize| |string|optional|File Size|Length of file in bytes. Implemented as string. The value must be convertible to a long integer (sizes can become very large).|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|95463|
|EncodingFormatTypeID &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)| |string|optional|Encoding Format Type ID|Only used in FileCollection where the EncodingFormatType differs from data DatasetDefault specified in data.EncodingFormatTypeID: the media type specification for this dataset.|<code>^[\w\-\.]+:reference-data\-\-EncodingFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--EncodingFormatType:application%2Fgeo%2Bjson:|
|Checksum| |string|optional|Checksum|Checksum of file bytes - a hexadecimal number with even number of bytes.|<code>^([0-9a-fA-F]{2})+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|d41d8cd98f00b204e9800998ecf8427e|
|ChecksumAlgorithm| |string|optional|Checksum Algorithm|The name of the checksum algorithm e.g. MD5, SHA-256.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|SHA-256|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFileSourceInfo.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractFile.1.0.0](AbstractFile.1.0.0.md) | FileSourceInfo | one |  |
| Included by | [AbstractFileCollection.1.0.0](AbstractFileCollection.1.0.0.md) | FileSourceInfos[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._