<a name="TOC"></a> Previous Chapter: [13 Earth Modelling (Static Subsurface Model Data, Geomodelling)](13-EarthModelling.md)

# Appendix A References

## Appendix A.1 External Document References

The following documents are referenced in this Guide.
(Please note that the links below are good at the time of writing but cannot be guaranteed for the future.)

- **Energistics RESQML Knowledge Hierarchy**; refer to http://docs.energistics.org/#RESQML/RESQML_TOPICS/RESQML-000-133-0-C-sv2010.html.
- **EPSG Geodetic Parameter Set**; refer to https://epsg.org/home.html
- **ISO 19111:2019 OGC Abstract Specification Topic 2: Referencing by coordinates**; refer to http://docs.opengeospatial.org/as/18-005r4/18-005r4.html
- **IOGP P6/11** Seismic Bin Grid Format; refer
  to: https://www.iogp.org/bookstore/product/ogp-p611-seismic-bin-grid-data-exchange-format/
- **SEGY Rev 0 Digital Tape Format**; refer
  to: https://seg.org/Portals/0/SEG/News%20and%20Resources/Technical%20Standards/seg_y_rev0.pdf
- Semantic Versioning, https://semver.org

## Appendix A.2 OSDU System Services

- Compliance Service ("Legal
  Tags") <br> https://community.opengroup.org/osdu/platform/security-and-compliance/legal/-/blob/master/docs/tutorial/ComplianceService.md
- CRS Conversion
  - API specification:<br>
    https://community.opengroup.org/osdu/platform/system/reference/crs-conversion-service/-/blob/master/docs/v3/api_spec/crs_converter_openapi.json
- Schema Service
  - API specification:<br>
    https://community.opengroup.org/osdu/platform/system/schema-service/-/blob/master/docs/api/schema_openapi.yaml
  - Tutorial:<br>
    https://community.opengroup.org/osdu/platform/system/schema-service/-/blob/master/docs/SchemaService-OSDU.md
- Search Service
  - Tutorial <br> https://community.opengroup.org/osdu/platform/system/search-service/-/blob/master/docs/tutorial/SearchService.md
  - About Searching in Arrays <br> https://community.opengroup.org/osdu/platform/system/search-service/-/blob/master/docs/tutorial/ArrayOfObjects.md
- Storage
  Service <br> https://community.opengroup.org/osdu/platform/system/storage/-/blob/master/docs/tutorial/StorageService.md

---

## Links

1. Return to the [Beginning of the Chapter](#TOC)
2. Next Chapter: [Appendix B Glossary of Terms](91-Glossary.md)
3. Browse to the [Schema Usage Guide Table of Contents](../README.md)
