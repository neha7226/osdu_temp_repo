<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractAliasNames [Status: Accepted]
A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractAliasNames:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractAliasNames.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractAliasNames.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractAliasNames.1.0.0.json](../../Authoring/abstract/AbstractAliasNames.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractAliasNames.1.0.0.json](../../Generated/abstract/AbstractAliasNames.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractAliasNames.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractAliasNames.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractAliasNames


## Outgoing Relationships for AbstractAliasNames

![AbstractAliasNames](../_diagrams/abstract/AbstractAliasNames.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractAliasNames Referenced by other Entities

![AbstractAliasNames](../_diagrams/abstract/AbstractAliasNames.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractAliasNames Properties

## Table of AbstractAliasNames Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|AliasName| |string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|AliasNameTypeID &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)| |string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|DefinitionOrganisationID &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|EffectiveDateTime| |string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TerminationDateTime| |string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractAliasNames.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractFacility.1.0.0](AbstractFacility.1.0.0.md) | FacilityNameAliases[] | many (as array) | Deprecated |
| Included by | [AbstractFacility.1.1.0](AbstractFacility.1.1.0.md) | FacilityNameAliases[] | many (as array) | Deprecated |
| Included by | [AbstractMaster.1.0.0](AbstractMaster.1.0.0.md) | NameAliases[] | many (as array) |  |
| Included by | [AbstractMaster.1.1.0](AbstractMaster.1.1.0.md) | NameAliases[] | many (as array) |  |
| Included by | [AbstractProject.1.0.0](AbstractProject.1.0.0.md) | ProjectNames[] | many (as array) | Deprecated |
| Included by | [AbstractReferenceType.1.0.0](AbstractReferenceType.1.0.0.md) | NameAlias[] | many (as array) |  |
| Included by | [Basin.1.0.0](../master-data/Basin.1.0.0.md) | BasinNameAliases[] | many (as array) | Deprecated |
| Included by | [Field.1.0.0](../master-data/Field.1.0.0.md) | FieldNameAliases[] | many (as array) | Deprecated |
| Included by | [GeoPoliticalEntity.1.0.0](../master-data/GeoPoliticalEntity.1.0.0.md) | GeoPoliticalEntityNameAliases[] | many (as array) | Deprecated |
| Included by | [Organisation.1.0.0](../master-data/Organisation.1.0.0.md) | OrganisationNameAliases[] | many (as array) | Deprecated |
| Included by | [Organisation.1.1.0](../master-data/Organisation.1.1.0.md) | OrganisationNameAliases[] | many (as array) | Deprecated |
| Included by | [Play.1.0.0](../master-data/Play.1.0.0.md) | PlayNameAliases[] | many (as array) | Deprecated |
| Included by | [Prospect.1.0.0](../master-data/Prospect.1.0.0.md) | ProspectNameAliases[] | many (as array) | Deprecated |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._