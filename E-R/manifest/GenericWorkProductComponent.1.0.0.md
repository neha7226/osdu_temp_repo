<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# GenericWorkProductComponent
An auto-generated placeholder schema representing work-product-component group-type records in data loading/ingestion/creation manifests. Do not use this kind for actual records.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:work-product-component--GenericWorkProductComponent:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; [Authoring Schema GenericWorkProductComponent.1.0.0.json](../../Authoring/manifest/GenericWorkProductComponent.1.0.0.json)
* Link to &rarr; [Generated Schema GenericWorkProductComponent.1.0.0.json](../../Generated/manifest/GenericWorkProductComponent.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource GenericWorkProductComponent.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//manifest/GenericWorkProductComponent.1.0.0.json)
* Link to &rarr; [Example Record GenericWorkProductComponent.1.0.0.json](../../Examples/manifest/GenericWorkProductComponent.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams GenericWorkProductComponent


## Outgoing Relationships for GenericWorkProductComponent

![GenericWorkProductComponent](../_diagrams/manifest/GenericWorkProductComponent.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## GenericWorkProductComponent Referenced by other Entities

![GenericWorkProductComponent](../_diagrams/manifest/GenericWorkProductComponent.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# GenericWorkProductComponent Properties

## 1. Table of GenericWorkProductComponent System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^(surrogate-key:.+&#124;[\w\-\.]+:work-product-component\-\-[\w\-\.]+:[\w\-\.\:\%]+)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:work-product-component--GenericWorkProductComponent:80575f9c-fc7b-516b-b44f-996874b9f775|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:work-product-component--GenericWorkProductComponent:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of GenericWorkProductComponent Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of GenericWorkProductComponent Data Properties, Section AbstractWPCGroupType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_AbstractWPCGroupType_|_Generic reference object containing the universal group-type properties of a Work Product Component for inclusion in data type specific Work Product Component objects_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Datasets[]| |string|optional|Datasets|The record id, which identifies this OSDU File or dataset resource.|<code>^(surrogate-key:.+&#124;[\w\-\.]+:dataset\-\-[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_data.Artefacts[]_| |_object_|_optional_|_Artefacts_|_An array of Artefacts - each artefact has a Role, Resource tuple. An artefact is distinct from the file, in the sense certain valuable information is generated during loading process (Artefact generation process). Examples include retrieving location data, performing an OCR which may result in the generation of artefacts which need to be preserved distinctly_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Artefacts[].RoleID &rarr; [ArtefactRole](../reference-data/ArtefactRole.1.0.0.md)| |string|optional|Role Id|The SRN of this artefact's role.|<code>^[\w\-\.]+:reference-data\-\-ArtefactRole:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ArtefactRole](../reference-data/ArtefactRole.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Artefacts[].ResourceKind| |string|optional|Resource Kind|The kind or schema ID of the artefact. Resolvable with the Schema Service.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Artefacts[].ResourceID| |string|optional|Resource Id|The SRN which identifies this OSDU Artefact resource.|<code>^(surrogate-key:.+&#124;[\w\-\.]+:dataset\-\-[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.IsExtendedLoad| |boolean|optional|Is Extended Load|A flag that indicates if the work product component is undergoing an extended load.  It reflects the fact that the work product component is in an early stage and may be updated before finalization.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.IsDiscoverable| |boolean|optional|Is Discoverable|A flag that indicates if the work product component is searchable, which means covered in the search index.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_data.TechnicalAssurances[]_|_[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)_|_object_|_optional_|_AbstractTechnicalAssurance_|_Describes a record's overall suitability for general business consumption based on data quality. Clarifications: Since Certified is the highest classification of suitable quality, any further change or versioning of a Certified record should be carefully considered and justified. If a Technical Assurance value is not populated then one can assume the data has not been evaluated or its quality is unknown (=Unevaluated). Technical Assurance values are not intended to be used for the identification of a single "preferred" or "definitive" record by comparison with other records. Fragment Description: Describes a record's overall suitability for general business consumption based on level of trust._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.TechnicalAssurances[].TechnicalAssuranceTypeID &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)|string|required|Technical Assurance Type ID|Describes a record's overall suitability for general business consumption based on data quality. Clarifications: Since Certified is the highest classification of suitable quality, any further change or versioning of a Certified record should be carefully considered and justified. If a Technical Assurance value is not populated then one can assume the data has not been evaluated or its quality is unknown (=Unevaluated). Technical Assurance values are not intended to be used for the identification of a single "preferred" or "definitive" record by comparison with other records.|<code>^[\w\-\.]+:reference-data\-\-TechnicalAssuranceType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--TechnicalAssuranceType:Trusted:|
|_data.TechnicalAssurances[].Reviewers[]_|_[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](../abstract/AbstractContact.1.0.0.md)_|_object_|_optional_|_Abstract Contact_|_The individuals, or roles, that reviewed and determined the technical assurance value Fragment Description: An object with properties that describe a specific person or other point-of-contact (like an email distribution list) that is relevant in this context (like a given data set or business project). The contact specified may be either internal or external to the organisation (something denoted via the Organisation object that is referenced). Note that some properties contain personally identifiable information, so it might not be appropriate to populate all properties in all scenarios._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.TechnicalAssurances[].Reviewers[].EmailAddress|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](../abstract/AbstractContact.1.0.0.md)|string|optional|Email Address|Contact email address. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|support@company.com|
|data.TechnicalAssurances[].Reviewers[].PhoneNumber|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](../abstract/AbstractContact.1.0.0.md)|string|optional|Phone Number|Contact phone number. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1-555-281-5555|
|data.TechnicalAssurances[].Reviewers[].RoleTypeID &rarr; [ContactRoleType](../reference-data/ContactRoleType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](../abstract/AbstractContact.1.0.0.md)|string|optional|Role Type ID|The identifier of a reference value for the role of the contact within the associated organisation, such as Account owner, Sales Representative, Technical Support, Project Manager, Party Chief, Client Representative, Senior Observer.|<code>^[\w\-\.]+:reference-data\-\-ContactRoleType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ContactRoleType](../reference-data/ContactRoleType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.TechnicalAssurances[].Reviewers[].Comment|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](../abstract/AbstractContact.1.0.0.md)|string|optional|Comment|Additional information about the contact|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.TechnicalAssurances[].Reviewers[].OrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](../abstract/AbstractContact.1.0.0.md)|string|optional|Organisation ID|Reference to the company the contact is associated with.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.TechnicalAssurances[].Reviewers[].Name|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md) [AbstractContact.1.0.0](../abstract/AbstractContact.1.0.0.md)|string|optional|Name|Name of the individual contact. Property may be left empty where it is inappropriate to provide personally identifiable information.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_data.TechnicalAssurances[].AcceptableUsage[]_|_[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)_|_object_|_optional_|_AcceptableUsage_|_List of workflows and/or personas that the technical assurance value is valid for (e.g., This data is trusted for Seismic Processing) Fragment Description: Describes the workflows and/or personas that the technical assurance value is valid for (e.g., This data has a technical assurance property of "trusted" and it is suitable for Seismic Interpretation)._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_[{"WorkflowUsage": "namespace:reference-data--WorkflowUsageType:SeismicProcessing:", "WorkflowPersona": "namespace:reference-data--WorkflowPersonaType:SeismicProcessor:"}]_|
|data.TechnicalAssurances[].AcceptableUsage[].WorkflowUsage &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)|string|optional|Workflow Usage|Name of the business activities, processes, and/or workflows that the record is technical assurance value is valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowUsageType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowUsageType:SeismicProcessing:|
|data.TechnicalAssurances[].AcceptableUsage[].WorkflowPersona &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)|string|optional|Workflow Persona|Name of the role or personas that the record is technical assurance value is valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowPersonaType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowPersonaType:SeismicProcessor:|
|_data.TechnicalAssurances[].UnacceptableUsage[]_|_[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)_|_object_|_optional_|_UnacceptableUsage_|_List of workflows and/or personas that the technical assurance value is not valid for (e.g., This data is not trusted for seismic interpretation) Fragment Description: Describes the workflows and/or personas that the technical assurance value is not valid for (e.g., This data has a technical assurance property of "trusted", but it is not suitable for Seismic Interpretation)._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_[{"WorkflowUsage": "namespace:reference-data--WorkflowUsageType:SeismicInterpretation:", "WorkflowPersona": "namespace:reference-data--WorkflowPersonaType:SeismicInterpreter:"}]_|
|data.TechnicalAssurances[].UnacceptableUsage[].WorkflowUsage &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)|string|optional|Workflow Usage|Name of the business activities, processes, and/or workflows that the record is technical assurance value is not valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowUsageType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowUsageType](../reference-data/WorkflowUsageType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowUsageType:SeismicInterpretation:|
|data.TechnicalAssurances[].UnacceptableUsage[].WorkflowPersona &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)|string|optional|Workflow Persona|Name of the role or personas that the record is technical assurance value is not valid for.|<code>^[\w\-\.]+:reference-data\-\-WorkflowPersonaType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WorkflowPersonaType](../reference-data/WorkflowPersonaType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--WorkflowPersonaType:SeismicInterpreter:|
|data.TechnicalAssurances[].EffectiveDate|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)|string|optional|Effective Date|Date when the technical assurance determination for this record has taken place|(No pattern)|date|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-02-13|
|data.TechnicalAssurances[].Comment|[AbstractTechnicalAssurance.1.0.0](../abstract/AbstractTechnicalAssurance.1.0.0.md)|string|optional|Comment|Any additional context to support the determination of technical assurance|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|This is free form text from reviewer, e.g. restrictions on use|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# GenericWorkProductComponent.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [Manifest.1.0.0](Manifest.1.0.0.md) | WorkProductComponents[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._