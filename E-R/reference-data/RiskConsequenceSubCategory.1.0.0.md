<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# RiskConsequenceSubCategory [Status: Accepted]
References the consequence sub-category of the risk. Possible effects arising were a risk event to occur.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--RiskConsequenceSubCategory:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: OSDU
* Reference value type, governance model: OPEN
* Link to &rarr; Proposal workbook [schema-RiskConsequenceSubCategory.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/reference-data/schema-RiskConsequenceSubCategory.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema RiskConsequenceSubCategory.1.0.0.json](../../Authoring/reference-data/RiskConsequenceSubCategory.1.0.0.json)
* Link to &rarr; [Generated Schema RiskConsequenceSubCategory.1.0.0.json](../../Generated/reference-data/RiskConsequenceSubCategory.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource RiskConsequenceSubCategory.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/RiskConsequenceSubCategory.1.0.0.json)
* Link to &rarr; [Example Record RiskConsequenceSubCategory.1.0.0.json](../../Examples/reference-data/RiskConsequenceSubCategory.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams RiskConsequenceSubCategory


## Outgoing Relationships for RiskConsequenceSubCategory

![RiskConsequenceSubCategory](../_diagrams/reference-data/RiskConsequenceSubCategory.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## RiskConsequenceSubCategory Referenced by other Entities

![RiskConsequenceSubCategory](../_diagrams/reference-data/RiskConsequenceSubCategory.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# RiskConsequenceSubCategory Properties

## 1. Table of RiskConsequenceSubCategory System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-RiskConsequenceSubCategory:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--RiskConsequenceSubCategory:a2ad9387-d96f-543e-9447-3be6d8212e92|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--RiskConsequenceSubCategory:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of RiskConsequenceSubCategory Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of RiskConsequenceSubCategory Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of RiskConsequenceSubCategory Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.RiskConsequenceCategoryID &rarr; [RiskConsequenceCategory](RiskConsequenceCategory.1.0.0.md)| |string|optional|Risk Consequence Category ID|Parent consequence category of the risk for possible effects on Asset, Business & Reputation, Environment, Health & Safety…|<code>^[\w\-\.]+:reference-data\-\-RiskConsequenceCategory:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [RiskConsequenceCategory](RiskConsequenceCategory.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of RiskConsequenceSubCategory Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for RiskConsequenceSubCategory.1.0.0

* Status: Published
* Governance: OPEN
* The table below shows all 38 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [RiskConsequenceSubCategory](../../ReferenceValues/Manifests/reference-data/OPEN/RiskConsequenceSubCategory.1.0.0.json).

|`id` with prefix `namespace:reference-data--RiskConsequenceSubCategory:`|Code|Name|Alias|Description|AttributionAuthority|RiskConsequenceCategoryID|Source|CommitDate|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Asset-3rdParty|Asset-3rdParty|Asset - 3rd Party|-|Consequences related to Assets - 3rd Party, with examples of 3rd party: equipment leased from a 3rd party ...|Well Delivery Schema Group|RiskConsequenceCategory:Asset:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Asset-Computer|Asset-Computer|Asset - Computer|-|Consequences related to Assets - Computer, with examples of computer: laptops, PC&#x27;s ...|Well Delivery Schema Group|RiskConsequenceCategory:Asset:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Asset-Equipment|Asset-Equipment|Asset - Equipment|-|Consequences related to Assets - Equipment, with examples of equipment: facilities, drilling rig surface equipment, BOP&#x27;s …|Well Delivery Schema Group|RiskConsequenceCategory:Asset:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Asset-Products|Asset-Products|Asset - Products|-|Consequences related to Assets - Products, with examples of products: chemicals, drill bits …|Well Delivery Schema Group|RiskConsequenceCategory:Asset:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|AutomotiveHeavy|AutomotiveHeavy|Automotive Heavy|-|Consequences related to Automotive Heavy, defined as vehicles weighing greater than 3.5 - 7 tonnes|Well Delivery Schema Group|RiskConsequenceCategory:HealthAndSafety:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|AutomotiveLight|AutomotiveLight|Automotive Light|-|Consequences related to Automotive Light, defined as vehicles weighing less than 3.5 - 7 tonnes|Well Delivery Schema Group|RiskConsequenceCategory:HealthAndSafety:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Business-Financial|Business-Financial|Business - Financial|-|Financial consequences should the risk occur|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Business-Non-Financial-AccountingAndControl|Business-Non-Financial-AccountingAndControl|Business - Non-Financial - AccountingAndControl|-|Consequences related to financial audit findings and/or incorrect financial reporting should the risk occur|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Business-Non-Financial-Government-Legal-RegulatoryEnforcement|Business-Non-Financial-Government-Legal-RegulatoryEnforcement|Business - Non-Financial - Government, Legal, Regulatory Enforcement|-|Government, legal and or regulatory consequences should the risk occur|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Business-Non-Financial-KeyStakeholderReaction|Business-Non-Financial-KeyStakeholderReaction|Business - Non-Financial - Key Stakeholder Reaction|-|Consequences related to damaged relationships with key stakeholders should the risk occur|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Business-Non-Financial-MediaAndPublicReaction|Business-Non-Financial-MediaAndPublicReaction|Business - Non-Financial - MediaAndPublic Reaction|-|Media and reputational consequences should the risk occur|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|BusinessProcess|BusinessProcess|Business Process|-|Consequences related to Process, with examples being: generation of scrap during a manufacturing process, containment failure …|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Environment-AccidentalDischarge|Environment-AccidentalDischarge|Environment - Accidental Discharge|-|Consequences related to Environment - Accidental Discharge, with examples being: a fluid breaching a pipeline / tank, a gas being released into the atmosphere …|Well Delivery Schema Group|RiskConsequenceCategory:Environment:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Environment-InappropriateDisposal|Environment-InappropriateDisposal|Environment - Inappropriate Disposal|-|Consequences related to Environment - Inappropriate Disposal, with examples being: drilled cuttings disposal …|Well Delivery Schema Group|RiskConsequenceCategory:Environment:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Environment-PhysicalDamage|Environment-PhysicalDamage|Environment - Physical Damage|-|Consequences related to Environment - Physical Damage, with examples being: damage fauna or flora …|Well Delivery Schema Group|RiskConsequenceCategory:Environment:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Environment-SanctionsAndScrutiny|Environment-SanctionsAndScrutiny|Environment - Sanctions and Scrutiny|-|Consequences related to Environment - Sanctions and Scrutiny, with examples being: trade barriers, tariffs, restrictions on financial transactions …|Well Delivery Schema Group|RiskConsequenceCategory:Environment:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Export-LoadingSystemShutdown|Export-LoadingSystemShutdown|Export/Loading system shutdown|-|Shut-down of export / loading systems|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Fine-Penalties|Fine-Penalties|Fine/Penalties|-|Consequences related to Fine/Penalties, with examples being: regulatory fines|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Flaring-NoProductionLoss|Flaring-NoProductionLoss|Flaring - no production loss|-|Flaring but with no loss of production|Well Delivery Schema Group|RiskConsequenceCategory:Environment:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Flaring-ProductionLoss|Flaring-ProductionLoss|Flaring - production loss|-|Flaring and loss of production|Well Delivery Schema Group|RiskConsequenceCategory:Environment:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Information-3rdParty|Information-3rdParty|Information - 3rd Party|-|Consequences related to Information - 3rd Party, with examples being: lost data due to mishandling, storage of data under unsuitable conditions …|Well Delivery Schema Group|RiskConsequenceCategory:Information:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Information-Client|Information-Client|Information - Client|-|Consequences related to Information - Client, with examples being:  incomplete data acquisition, lost data due to mishandling, storage of data under unsuitable conditions …|Well Delivery Schema Group|RiskConsequenceCategory:Information:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Information-Schlumberger|Information-Schlumberger|Information - Schlumberger|-|Consequences related to Information - Schlumberger, with examples being: lost data due to mishandling, storage of data under unsuitable conditions …|Well Delivery Schema Group|RiskConsequenceCategory:Information:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|InjectionShutdown-GasOrWaterInjection|InjectionShutdown-GasOrWaterInjection|Injection shutdown (gas or water injection)|-|Shut down of injection (gas or water)|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|NPT3rdParty|NPT3rdParty|NPT 3rd Party|-|Consequences related to NPT 3rd Party, with an example being financial loss associated with Non-Productive Time during operations|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|NPTClient|NPTClient|NPT Client|-|Consequences related to NPT Client, with an example being financial loss associated with Non-Productive Time during operations|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PersonnelInjury|PersonnelInjury|Personnel Injury|-|Consequences related to Personnel Injury, with examples being; time away from work, ability to perform a restricted scope of work|Well Delivery Schema Group|RiskConsequenceCategory:HealthAndSafety:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PersonnelNon-OccupationalIllness|PersonnelNon-OccupationalIllness|Personnel Non-Occupational Illness|-|Consequences related to Personnel Non-Occupational Illness, with examples being; heart disease, diabetes …|Well Delivery Schema Group|RiskConsequenceCategory:HealthAndSafety:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PersonnelOccupationalIllness|PersonnelOccupationalIllness|Personnel Occupational Illness|-|Consequences related to Personnel Occupational Illness, with examples being; skin or respiratory irritation from exposure to chemicals …|Well Delivery Schema Group|RiskConsequenceCategory:HealthAndSafety:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ProductionShutdown-OilAnd-OrGasSystem|ProductionShutdown-OilAnd-OrGasSystem|Production shutdown (oil and/or gas system)|-|Shut down of production oil and/or gas)|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|PublicHealthAndSafety|PublicHealthAndSafety|Public HealthAndSafety|-|Consequences related to the health and safety of the public|Well Delivery Schema Group|RiskConsequenceCategory:HealthAndSafety:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ReducedExport-Loading|ReducedExport-Loading|Reduced export/loading|-|Reduction in export/loading|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ReducedInjection-GasOrWaterInjection|ReducedInjection-GasOrWaterInjection|Reduced injection (gas or water injection)|-|Reduction in injection (gas or water)|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ReducedProduction-OilAnd-OrGasSystem|ReducedProduction-OilAnd-OrGasSystem|Reduced production(oil and/or gas system)|-|Reduction of production (oil and/or gas)|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|Redundancy-SpareCapacityReducedOrLost|Redundancy-SpareCapacityReducedOrLost|Redundancy, spare capacity reduced or lost|-|Reduction or loss of spare capacity|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ShutdownOfMultipleWells|ShutdownOfMultipleWells|Shutdown of multiple wells|-|Multiple wells shut down|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|ShutdownOfSingleWell|ShutdownOfSingleWell|Shutdown of single well|-|a single well shut down|Well Delivery Schema Group|RiskConsequenceCategory:BusinessAndReputation:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|
|WorkforceHealthAndSafety|WorkforceHealthAndSafety|Workforce HealthAndSafety|-|Consequences related to the health and safety of the work force|Well Delivery Schema Group|RiskConsequenceCategory:HealthAndSafety:|Workbook Published/RiskConsequenceSubCategory.1.0.0.xlsx; commit SHA 951f2a10.|2021-12-06T11:34:44+01:00|


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# RiskConsequenceSubCategory.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [Risk.1.0.0](../master-data/Risk.1.0.0.md) | ConsequenceSubCategoryID | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._