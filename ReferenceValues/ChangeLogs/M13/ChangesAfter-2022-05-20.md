<a name="TOC"></a>

[[_TOC_]]

# Reference Value Changes

## `osdu:wks:reference-data--BitType:1.0.1`

|Record `id` | Date | Change Note | Superseded by Name |
|---|---|---|---|
|BiCenter|2022-05-27|New value| |
|BullNose|2022-05-27|New value| |
|Core|2022-05-27|New value| |
|Drag|2022-05-27|New value| |
|Hammer|2022-05-27|New value| |
|Hybrid|2022-05-27|New value| |
|Mill|2022-05-27|New value| |
|Other|2022-05-27|New value| |
|PDC|2022-05-27|added a comment, NAME2 and abbreviation| |
|Reamer|2022-05-27|New value| |
|RollerCone|2022-05-27|new definition and comment, added alias| |
|TSP|2022-05-27|New value| |
|Unknown|2022-05-27|added comment|unspsc|
|diamond|2022-05-27|To generic, should not be used| |
|diamond%20core|2022-05-27|Type of core bit, use core instead|Core|
|insert%20roller%20cone|2022-05-27|Type of roller cone, use the parent of roller cone|Roller cone|
|PDC%20core|2022-05-27|Duplicate, same as PDC, captured as NAME2|PDC|
|roller%20cone|2022-05-27|new definition and comment, added alias|Roller cone|
|unknown|2022-05-27|added comment|Unknown|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--SeismicMigrationType:1.0.1`

|Record `id` | Date | Change Note | Superseded by Name |
|---|---|---|---|
|TimeMig|2022-06-25|Adjusted Superseded by|Poststack time|
|DepthMig|2022-06-25|Adjusted Superseded by|Poststack depth|
|PSTM|2022-06-25|Adjusted Superseded by|Prestack time|
|PSDM|2022-06-25|Adjusted Superseded by|Prestack depth|
|TimeMig-Kirch|2022-06-25|Adjusted Superseded by|Poststack time - Kirchhoff|
|DepthMig-Kirch|2022-06-25|Adjusted Superseded by|Poststack depth - Kirchhoff|
|PSTM-Kirch|2022-06-25|Adjusted Superseded by|Prestack time - Kirchhoff|
|PSDM-Kirch|2022-06-25|Adjusted Superseded by|Prestack depth - Kirchhoff|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--SeismicPickingType:1.0.1`

|Record `id` | Date | Change Note | Superseded by Name |
|---|---|---|---|
|Mixed|2022-06-02|-| |
|Automatic|2022-06-02|Revised definition| |
|Manual|2022-06-02|Revised definition and provided a resource| |
|Autotracked|2022-06-25|Do not use. Use superceded value of Automatic|Automatic|
|InterpolatedSeed|2022-06-02|Do not use. Use superceded value of Mixed|Mixed|
|PrimarySeed|2022-06-02|Do not use. Use superceded value of Automatic|Automatic|

[Back to TOC](#TOC)

## `osdu:wks:reference-data--SeismicWaveType:1.0.1`

|Record `id` | Date | Change Note | Superseded by Name |
|---|---|---|---|
|Sh|2022-06-25|adjust SUPERSEDED BY|Horizontal shear wave|
|PSh|2022-06-25|adjust SUPERSEDED BY|Pressure/horizontal shear wave|
|PSv|2022-06-25|adjust SUPERSEDED BY|Pressure/vertical shear wave|
|Sv|2022-06-25|adjust SUPERSEDED BY|Vertical shear wave|
|PressureVerticalShearWave|2022-06-25|Removed blank from NAME4| |

[Back to TOC](#TOC)

## `osdu:wks:reference-data--VerticalMeasurementType:1.0.1`

|Record `id` | Date | Change Note | Superseded by Name |
|---|---|---|---|
|ArbitraryPoint|2022-05-30|revised definition| |
|CasingHeadFlange|2022-05-30|New value| |
|Conductor|2022-05-30|Revised definition| |
|DrillFloor|2022-05-30|Revised definition| |
|EllipsoidSurface|2022-05-30|Revised definition| |
|FluidContactSurface|2022-05-30|Revised definition| |
|GroundLevel|2022-05-30|Revised definition| |
|KellyBushing|2022-05-30|Revised definition, added Abbreviations| |
|KickOffPoint|2022-05-30|Revised definition| |
|LowestAstronomicalTide|2022-05-30|Revised definition| |
|MeanSeaLevel|2022-05-30|Revised definition, added an alias and comment| |
|PlatformDeck|2022-05-30|New value| |
|PlugBackDepth|2022-05-30|Revised definition| |
|RotaryTable|2022-05-30|Revised definition| |
|SeaLevel|2022-05-30|New value| |
|Seafloor|2022-05-30|Revised definition, mudline used as an alias| |
|SeismicReferenceDatum|2022-05-30|Revised definition| |
|SwabValve|2022-05-30|New value| |
|TieInPoint|2022-05-30|Revised definition and added alias| |
|TopBottomFlange|2022-05-30|Revised definition and added alias| |
|TopCellar|2022-05-30|Revised definition| |
|TotalDepth|2022-05-30|Revised definition| |
|TubingHeadFlange|2022-05-30|New value| |
|CH|2022-05-30|Use casing head flange value|Casing head flange|
|CF|2022-05-30|Use casing head flange value|Casing head flange|
|BH|2022-05-30|Not relevant to this list| |
|CB|2022-05-30|Trade name not to be used| |
|CV|2022-05-30|Use Swab valve as value|Swab valve|
|MT|2022-05-30|Not relevant to this list| |
|EST|2022-05-30|Not relevant to this list| |
|GM|2022-05-30|Not relevant to this list| |
|MLS|2022-05-30|Same as sea floor|Sea floor|
|PT|2022-05-30|Use casing head flange value|Casing head flange|
|BR|2022-05-30|Not relevant to this list| |
|RF|2022-05-30|Use drill floor value|Drill floor|
|RB|2022-05-30|Alias used in kelly bushing|Kelly bushing|
|PBD|2022-06-09|Use Plug back depth value|Plug back depth|
|TD|2022-06-09|Use Total depth value|Total depth|
|KB|2022-06-09|Use Kelly bushing value|Kelly bushing|
|RT|2022-06-09|Use Rotary table value|Rotary table|
|DF|2022-06-09|Use Drill floor value|Drill floor|
|MSL|2022-06-09|Use Mean sea level value|Mean sea level|
|LAT|2022-06-09|Use Lowest astronomical tide value|Lowest astronomical tide|
|GR|2022-06-09|Use Ground level value|Ground level|
|ES|2022-06-09|Use Ellipsoid surface value|Ellipsoid surface|
|KOP|2022-06-09|Use Kick off point value|Kick off point|
|AP|2022-06-09|Use Arbitrary point value|Arbitrary point|
|SRD|2022-06-09|Use Seismic reference datum value|Seismic reference datum|
|CO|2022-06-09|Use Conductor value|Conductor|
|SF|2022-06-09|Use Seafloor value|Seafloor|
|TBF|2022-06-09|Use Top bottom flange value|Top bottom flange|
|TC|2022-06-09|Use Top cellar value|Top cellar|
|TIP|2022-06-09|Use Tie in point value|Tie in point|
|TH|2022-06-09|Use Tubing head flange value|Tubing head flange|
|FCS|2022-05-30|Use Fluid contact surface value|Fluid contact surface|

[Back to TOC](#TOC)