<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractActivityState [Status: Accepted]
One step/interval in an Activity's or ProjectActivity's state.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractActivityState:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractActivityState.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractActivityState.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractActivityState.1.0.0.json](../../Authoring/abstract/AbstractActivityState.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractActivityState.1.0.0.json](../../Generated/abstract/AbstractActivityState.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractActivityState.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractActivityState.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractActivityState


## Outgoing Relationships for AbstractActivityState

![AbstractActivityState](../_diagrams/abstract/AbstractActivityState.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractActivityState Referenced by other Entities

![AbstractActivityState](../_diagrams/abstract/AbstractActivityState.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractActivityState Properties

## Table of AbstractActivityState Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|EffectiveDateTime| |string|optional|Effective Date Time|The date and time at which the activity status becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TerminationDateTime| |string|optional|Termination Date Time|The date and time at which the activity status is no longer in effect. For still effective activity states, the TerminationDateTime is left absent. For zero-duration intervals (events), the TerminationDateTime set to the same value as EffectiveDateTime.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ActivityStatusID &rarr; [ActivityStatus](../reference-data/ActivityStatus.1.0.0.md)| |string|optional|Activity Status ID|The ActivityStatus is a set of major activity phases that are significant to business stakeholders.|<code>^[\w\-\.]+:reference-data\-\-ActivityStatus:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ActivityStatus](../reference-data/ActivityStatus.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Remark| |string|optional|Remark|An optional remark associated with the ActivityStatus and time interval.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractActivityState.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractProjectActivity.1.1.0](AbstractProjectActivity.1.1.0.md) | ActivityStates[] | many (as array) |  |
| Included by | [AbstractProjectActivity.1.1.0](AbstractProjectActivity.1.1.0.md) | LastActivityState | one |  |
| Included by | [AbstractWPCActivity.1.1.0](AbstractWPCActivity.1.1.0.md) | ActivityStates[] | many (as array) |  |
| Included by | [AbstractWPCActivity.1.1.0](AbstractWPCActivity.1.1.0.md) | LastActivityState | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._