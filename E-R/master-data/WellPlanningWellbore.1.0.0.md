<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# WellPlanningWellbore [Status: Accepted]
A hole in the ground extending from a point at the earth's surface to the maximum point of penetration.
* **This is a side-car object**, which delivers bounded context for the domain, extending:
   * `osdu:wks:master-data--Wellbore:*.*.*` &rarr; [Wellbore](Wellbore.1.3.0.md) via relationship property `WellboreID`
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:master-data--WellPlanningWellbore:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: OSDU
* Link to &rarr; Proposal workbook [WellPlanningWellbore.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/master-data/WellPlanningWellbore.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema WellPlanningWellbore.1.0.0.json](../../Authoring/master-data/WellPlanningWellbore.1.0.0.json)
* Link to &rarr; [Generated Schema WellPlanningWellbore.1.0.0.json](../../Generated/master-data/WellPlanningWellbore.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource WellPlanningWellbore.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//master-data/WellPlanningWellbore.1.0.0.json)
* Link to &rarr; [Example Record WellPlanningWellbore.1.0.0.json](../../Examples/master-data/WellPlanningWellbore.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.
* Link to worked examples in context &rarr; [Topic: Well Planning Worked Examples](../../Examples/WorkedExamples/WellPlanning/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams WellPlanningWellbore


## Outgoing Relationships for WellPlanningWellbore

![WellPlanningWellbore](../_diagrams/master-data/WellPlanningWellbore.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## WellPlanningWellbore Referenced by other Entities

![WellPlanningWellbore](../_diagrams/master-data/WellPlanningWellbore.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# WellPlanningWellbore Properties

## 1. Table of WellPlanningWellbore System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:master-data\-\-WellPlanningWellbore:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:master-data--WellPlanningWellbore:27ba08ea-5c4e-5859-bd7e-d04b3c34a0bc|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:master-data--WellPlanningWellbore:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of WellPlanningWellbore Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of WellPlanningWellbore Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.WellboreID &rarr; [Wellbore](Wellbore.1.3.0.md)| |string|optional|Wellbore ID|Identifier of the parent wellbore.|<code>^[\w\-\.]+:master-data\-\-Wellbore:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Wellbore](Wellbore.1.3.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.WellPlanningWellID &rarr; [WellPlanningWell](WellPlanningWell.1.0.0.md)| |string|optional|Well Planning Well ID|Identifier of the parent well side-car for the well planning domain.|<code>^[\w\-\.]+:master-data\-\-WellPlanningWell:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WellPlanningWell](WellPlanningWell.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.SurveyProgramIDs[] &rarr; [SurveyProgram](SurveyProgram.1.0.0.md)| |string|optional|Survey Program IDs|A reference to the objects that holds the information about the definitive version of the different survey programs associated with the wellbore|<code>^[\w\-\.]+:master-data\-\-SurveyProgram:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SurveyProgram](SurveyProgram.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.TargetID &rarr; [GeometricTargetSet](GeometricTargetSet.1.0.0.md)| |string|optional|Target ID|The drill targets associated with this definitive Drilling Program|<code>^[\w\-\.]+:master-data\-\-GeometricTargetSet:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [GeometricTargetSet](GeometricTargetSet.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.WellboreMarkerSetID &rarr; [WellboreMarkerSet](../work-product-component/WellboreMarkerSet.1.2.1.md)| |string|optional|Wellbore MarkerSet ID|The formation markers associated with the definitive Drilling Program|<code>^[\w\-\.]+:work-product-component\-\-WellboreMarkerSet:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WellboreMarkerSet](../work-product-component/WellboreMarkerSet.1.2.1.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.PlannedLithologyID &rarr; [PlannedLithology](../work-product-component/PlannedLithology.1.0.0.md)| |string|optional|Planned Lithology ID|A reference to the descriptive object that holds the information about the planned lithology associated with the wellbore|<code>^[\w\-\.]+:work-product-component\-\-PlannedLithology:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [PlannedLithology](../work-product-component/PlannedLithology.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.HoleSectionID &rarr; [HoleSection](HoleSection.1.1.0.md)| |string|optional|Hole Section ID|The definitive description of the hole section associated with this wellbore|<code>^[\w\-\.]+:master-data\-\-HoleSection:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [HoleSection](HoleSection.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.PPFGDatasetID &rarr; [PPFGDataset](../work-product-component/PPFGDataset.1.0.0.md)| |string|optional|PPFG Dataset ID|A reference to the PPFGDataset that holds the information about the pore pressure associated with the definitive drilling program|<code>^[\w\-\.]+:work-product-component\-\-PPFGDataset:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [PPFGDataset](../work-product-component/PPFGDataset.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Name| |string|optional|Name|Name of Well Planning wellbore. Derived from the record identified by WellboreID.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "WellboreID", "TargetPropertyName": "FacilityName"}|(No natural key)|(No example)|
|_data.VerticalMeasurement_|_[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)_|_object_|_optional_|_AbstractFacilityVerticalMeasurement_|_The zero depth point (ZDP) definition for the all measured depths related to this WellPlanningWellbore. Fragment Description: A location along a wellbore, _usually_ associated with some aspect of the drilling of the wellbore, but not with any intersecting _subsurface_ natural surfaces._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.VerticalMeasurement.EffectiveDateTime|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Effective Date Time|The date and time at which a vertical measurement instance becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.VerticalMeasurement|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|number|optional|Vertical Measurement|The value of the elevation or depth. Depth is positive downwards from a vertical reference or geodetic datum along a path, which can be vertical; elevation is positive upwards from a geodetic datum along a vertical path. Either can be negative.|(No pattern)|(No format)|UOM_via_property:VerticalMeasurementUnitOfMeasureID|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.TerminationDateTime|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Termination Date Time|The date and time at which a vertical measurement instance is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.VerticalMeasurementTypeID &rarr; [VerticalMeasurementType](../reference-data/VerticalMeasurementType.1.0.2.md)|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Vertical Measurement Type Id|Specifies the type of vertical measurement (TD, Plugback, Kickoff, Drill Floor, Rotary Table...).|<code>^[\w\-\.]+:reference-data\-\-VerticalMeasurementType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [VerticalMeasurementType](../reference-data/VerticalMeasurementType.1.0.2.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.VerticalMeasurementPathID &rarr; [VerticalMeasurementPath](../reference-data/VerticalMeasurementPath.1.0.1.md)|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Vertical Measurement Path Id|Specifies Measured Depth, True Vertical Depth, or Elevation.|<code>^[\w\-\.]+:reference-data\-\-VerticalMeasurementPath:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [VerticalMeasurementPath](../reference-data/VerticalMeasurementPath.1.0.1.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.VerticalMeasurementSourceID &rarr; [VerticalMeasurementSource](../reference-data/VerticalMeasurementSource.1.0.0.md)|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Vertical Measurement Source Id|Specifies Driller vs Logger.|<code>^[\w\-\.]+:reference-data\-\-VerticalMeasurementSource:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [VerticalMeasurementSource](../reference-data/VerticalMeasurementSource.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.WellboreTVDTrajectoryID &rarr; [WellboreTrajectory](../work-product-component/WellboreTrajectory.1.1.0.md)|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Wellbore Tvd Trajectory Id|Specifies what directional survey or wellpath was used to calculate the TVD.|<code>^[\w\-\.]+:work-product-component\-\-WellboreTrajectory:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [WellboreTrajectory](../work-product-component/WellboreTrajectory.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.VerticalMeasurementUnitOfMeasureID &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Vertical Measurement Unit Of Measure Id|The unit of measure for the vertical measurement. If a unit of measure and a vertical CRS are provided, the unit of measure provided is taken over the unit of measure from the CRS.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.VerticalCRSID &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Vertical Crsid|A vertical coordinate reference system defines the origin for height or depth values. It is expected that either VerticalCRSID or VerticalReferenceID reference is provided in a given vertical measurement array object, but not both.|<code>^[\w\-\.]+:reference-data\-\-CoordinateReferenceSystem:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.VerticalReferenceID|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Vertical Reference Id|The reference point from which the relative vertical measurement is made. This is only populated if the measurement has no VerticalCRSID specified. The value entered must match the VerticalMeasurementID for another vertical measurement array element in Wellbore or Well or in a related parent facility. The relationship should be  declared explicitly in VerticalReferenceEntityID. Any chain of measurements must ultimately resolve to a Vertical CRS. It is expected that a VerticalCRSID or a VerticalReferenceID is provided in a given vertical measurement array object, but not both.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.VerticalReferenceEntityID &rarr; [Wellbore](Wellbore.1.3.0.md) &rarr; [Well](Well.1.2.0.md) &rarr; [Rig](Rig.1.1.0.md)|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Vertical Reference Entity Id|This relationship identifies the entity (aka record) in which the VerticalReferenceID is found; It could be a different OSDU entity or a self-reference. For example, a Wellbore VerticalMeasurement may reference a member of a VerticalMeasurements[] array in its parent Well record. Alternatively, VerticalReferenceEntityID may be populated with the ID of its own Wellbore record to make explicit that VerticalReferenceID is intended to be found in this record, not another.|<code>^[\w\-\.]+:(master-data\-\-Wellbore&#124;master-data\-\-Well&#124;master-data\-\-Rig):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Wellbore](Wellbore.1.3.0.md) &rarr; [Well](Well.1.2.0.md) &rarr; [Rig](Rig.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.VerticalMeasurement.VerticalMeasurementDescription|[AbstractFacilityVerticalMeasurement.1.0.0](../abstract/AbstractFacilityVerticalMeasurement.1.0.0.md)|string|optional|Vertical Measurement Description|Text which describes a vertical measurement in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of WellPlanningWellbore Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# WellPlanningWellbore.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractProjectActivity.1.1.0](../abstract/AbstractProjectActivity.1.1.0.md) | ParentProjectID | one | Untyped relationship |
| Referred to by | [AbstractWPCActivity.1.1.0](../abstract/AbstractWPCActivity.1.1.0.md) | ParentProjectID | one | Untyped relationship |
| Referred to by | [DataQuality.1.0.0](../work-product-component/DataQuality.1.0.0.md) | EvaluatedRecordID | one | Untyped relationship |
| Referred to by | [DataQuality.1.1.0](../work-product-component/DataQuality.1.1.0.md) | EvaluatedRecordID | one | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |
| Referred to by | [PlannedCementJob.1.0.0](PlannedCementJob.1.0.0.md) | WellboreID | one |  |
| Referred to by | [PlannedCementJob.1.1.0](PlannedCementJob.1.1.0.md) | WellboreID | one |  |
| Referred to by | [PlannedLithology.1.0.0](../work-product-component/PlannedLithology.1.0.0.md) | WellboreID | one |  |
| Referred to by | [WellBarrierElementTest.1.0.0](WellBarrierElementTest.1.0.0.md) | WellboreID | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._