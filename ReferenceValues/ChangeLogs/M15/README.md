<a name="TOC"></a>**Table of Contents**

[[_TOC_]]

Changes since: 2022-09-23

* [Detailed Change Log](ChangesAfter-2022-09-23.md) for reference values.

# IngestionSequence

Deprecated reference-value lists are removed from the
[IngestionSequence](../../Manifests/reference-data/IngestionSequence.json). The manifests are still listed in the
overview [README](../../Manifests/README.md) and can be ingested should there be references from non-OSDU records.

# Reference Value Changes

## AdditiveRole:1.0.0

This is a new PPDM governed, OPEN reference value list taking over a significant portion of the previous OSDU
AdditiveType values (further context is provided in 
[ChangesAfter-2022-09-23.md](ChangesAfter-2022-09-23.md)):

| `id`                                                                  | Name                           | Description                                                                                                                                                                |
|-----------------------------------------------------------------------|--------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--AdditiveRole:Accelerator                 | Accelerator                    | A chemical that speeds up the rate of a reaction, especially in cement.                                                                                                    |-|
| partition-id:reference-data--AdditiveRole:AcidCrosslinker             | Acid crosslinker               | An acid that promotes a connection between polymer resin chains.                                                                                                           |-|
| partition-id:reference-data--AdditiveRole:AcidEmulsifier              | Acid emulsifier                | An acid that encourages the suspension of one liquid in another.                                                                                                           |-|
| partition-id:reference-data--AdditiveRole:AcidGelling                 | Acid gelling                   | An acid that increases the viscosity of a liquid.                                                                                                                          |-|
| partition-id:reference-data--AdditiveRole:AcidInhibitor               | Acid inhibitor                 | An acid that slows a reaction between a reactive fluid and a material.                                                                                                     |-|
| partition-id:reference-data--AdditiveRole:Activator                   | Activator                      | A substance that starts or accelerates a chemical reaction.                                                                                                                |-|
| partition-id:reference-data--AdditiveRole:AnionicSurfactant           | Anionic surfactant             | A chemical that modifies the surface tension of a liquid.                                                                                                                  |-|
| partition-id:reference-data--AdditiveRole:AntiGasMigration            | Anti-gas migration             | A material that prevents the movement of gas during the setting of cement.                                                                                                 |-|
| partition-id:reference-data--AdditiveRole:AntiGelation                | Anti gelation                  | A substance that prevents or retards the formation of a gel.                                                                                                               |-|
| partition-id:reference-data--AdditiveRole:AntiGelling                 | Anti-gelling                   | A material that stabilizes the viscosity of a liquid by preventing the formation of a gel.                                                                                 |-|
| partition-id:reference-data--AdditiveRole:AntiSettling                | Anti-settling                  | A substance that prevents or retards the gravity separation of a solid from a liquid.                                                                                      |-|
| partition-id:reference-data--AdditiveRole:Antifoam                    | Antifoam                       | A material that destabilizes a foam.                                                                                                                                       |-|
| partition-id:reference-data--AdditiveRole:Antifreeze                  | Antifreeze                     | A material that lowers the freezing point of a liquid.                                                                                                                     |-|
| partition-id:reference-data--AdditiveRole:Antisludging                | Antisludging                   | A surfactant used to prevent the formation of sludge, an emulsion very high in solids.                                                                                     |-|
| partition-id:reference-data--AdditiveRole:Antistatic                  | Antistatic                     | A substance that reduces or eliminates the buildup of static electricity.                                                                                                  |-|
| partition-id:reference-data--AdditiveRole:Biocide                     | Biocide                        | A chemical or treatment that kills bacteria.                                                                                                                               |-|
| partition-id:reference-data--AdditiveRole:BlendedSurfactant           | Blended surfactant             | A combination of additives to change the surface tension of a liquid.                                                                                                      |-|
| partition-id:reference-data--AdditiveRole:BondAccelerator             | Bond accelerator               | A substance that reduces the reaction time for materials to bond together, especially in cement.                                                                           |-|
| partition-id:reference-data--AdditiveRole:BondingAgent                | Bonding agent                  | A substance that induces materials to bond together, especially in cement.                                                                                                 |-|
| partition-id:reference-data--AdditiveRole:BreakerActivator            | Breaker activator              | A substance that stimulates the breakdown of the structure of a gel.                                                                                                       |-|
| partition-id:reference-data--AdditiveRole:CationicSurfactant          | Cationic surfactant            | A substance with a positive charge that changes the surface tension of a liquid.                                                                                           |-|
| partition-id:reference-data--AdditiveRole:Chelant                     | Chelant                        | A substance that prevents or slows the precipitation of iron. In the oil field, chelating agents are used in stimulation treatments and for cleaning surface facilities.   |-|
| partition-id:reference-data--AdditiveRole:ChemicalTracer              | Chemical tracer                | A substance used to track the flow path of a fluid.                                                                                                                        |-|
| partition-id:reference-data--AdditiveRole:ClayDispersant              | Clay dispersant                | A substance that aids in breaking up a mass of clay particles.                                                                                                             |-|
| partition-id:reference-data--AdditiveRole:ClayStabilizer              | Clay stabilizer                | A substance to inhibit the movement or swelling of clay minerals.                                                                                                          |-|
| partition-id:reference-data--AdditiveRole:ConductivityEnhancer        | Conductivity enhancer          | A substance to increase the conductivity of fractures in the reservoir.                                                                                                    |-|
| partition-id:reference-data--AdditiveRole:CorrosionInhibitor          | Corrosion inhibitor            | A substance to prevent or slow down the reaction of a metal with a fluid.                                                                                                  |-|
| partition-id:reference-data--AdditiveRole:Crosslinker                 | Crosslinker                    | A substance to increase the viscosity of a polymer gel.                                                                                                                    |-|
| partition-id:reference-data--AdditiveRole:Defoamer                    | Defoamer                       | A chemical that reduces the stability of the bubble skin in a foam and cause the foam to break.                                                                            |-|
| partition-id:reference-data--AdditiveRole:Demulsifier                 | Demulsifier                    | A substance that causes the separation of oil from water.                                                                                                                  |-|
| partition-id:reference-data--AdditiveRole:DiagnosticTracer            | Diagnostic tracer              | A substance added to a fluid to enable the remote detection of the fluid location or flow path.                                                                            |-|
| partition-id:reference-data--AdditiveRole:Dispersant                  | Dispersant                     | A substance that aids in breaking up a mass of particles, bubbles or droplets.                                                                                             |-|
| partition-id:reference-data--AdditiveRole:DivertingAgent              | Diverting agent                | A material that forces acid to enter another zone by having a higher viscosity or building a filter cake.                                                                  |-|
| partition-id:reference-data--AdditiveRole:Emulsifier                  | Emulsifier                     | A substance that creates or stabilizes an emulsion.                                                                                                                        |-|
| partition-id:reference-data--AdditiveRole:EncapBreaker                | Encap breaker                  | A substance in pill form that stays with the polymer and helps break the mud cake.                                                                                         |-|
| partition-id:reference-data--AdditiveRole:Energizer                   | Energizer                      | -                                                                                                                                                                          |-|
| partition-id:reference-data--AdditiveRole:Expansion                   | Expansive additive             | developed to reduce and compensate the hydraulic shrinkage of cement                                                                                                       |-|
| partition-id:reference-data--AdditiveRole:Extender                    | Extender                       | A substance that decreasing the density of a cement slurry.                                                                                                                |-|
| partition-id:reference-data--AdditiveRole:FinesSuspender              | Fines suspender                | A chemical that keeps fine particles in suspension in a liquid.                                                                                                            |-|
| partition-id:reference-data--AdditiveRole:FlowEnhancer                | Flow enhancer                  | A substance that improves the rate of flow of a liquid.                                                                                                                    |-|
| partition-id:reference-data--AdditiveRole:FluidLoss                   | Fluid loss                     | A substance that reduces the transfer of drilling fluid into the rock.                                                                                                     |-|
| partition-id:reference-data--AdditiveRole:FoamingAgent                | Foaming agent                  | A substance that encourages an emulsion, being the suspension of a gas in a liquid.                                                                                        |-|
| partition-id:reference-data--AdditiveRole:FormationSealer             | Formation sealer               | A substance that improves the ability of a fluid to close a formation zone, thereby preventing lost circulation or improving zone abandonment.                             |-|
| partition-id:reference-data--AdditiveRole:FreeWaterControl            | Free Water Control             | An additive to the cement slurry to address the potential problem of particle sedimentation.                                                                               |-|
| partition-id:reference-data--AdditiveRole:FrictionReducer             | Friction reducer               | Dispersants, also known as friction reducers, are used extensively in cement slurries to improve the rheological properties that relate to the flow behavior of the slurry |-|
| partition-id:reference-data--AdditiveRole:GasHydrateControl           | Gas hydrate control            | A substance to reduce or prevent the occurrence of gas hydrates in a flow line.                                                                                            |-|
| partition-id:reference-data--AdditiveRole:GasMigration                | Gas migration                  | Primarily used as an additive in a cement slurry                                                                                                                           |-|
| partition-id:reference-data--AdditiveRole:GelStabilizer               | Gel stabilizer                 | -                                                                                                                                                                          |-|
| partition-id:reference-data--AdditiveRole:Gellant                     | Gellant                        | A substance that increases the viscosity of a liquid.                                                                                                                      |-|
| partition-id:reference-data--AdditiveRole:HydrogenSulphideScavenger   | Hydrogen sulphide Scavenger    | A material that takes H2S out of solution or the flow stream.                                                                                                              |-|
| partition-id:reference-data--AdditiveRole:IronControl                 | Iron control                   | A chemical that control the precipitation of iron from solution.                                                                                                           |-|
| partition-id:reference-data--AdditiveRole:Lightweight                 | Lightweight                    | A substance that decreasing the density of a cement slurry.                                                                                                                |-|
| partition-id:reference-data--AdditiveRole:LiquidExtender              | Liquid extender                | A substance that decreasing the density of a liquid.                                                                                                                       |-|
| partition-id:reference-data--AdditiveRole:LostCirculation             | Lost circulation               | -                                                                                                                                                                          |-|
| partition-id:reference-data--AdditiveRole:Lubricant                   | Lubricant                      | A material that reduces torque and drag.                                                                                                                                   |-|
| partition-id:reference-data--AdditiveRole:MixingFluid                 | Mixing Fluid                   | -                                                                                                                                                                          |-|
| partition-id:reference-data--AdditiveRole:ModulusOfElasticityAdjustor | Modulus of elasticity adjustor | A substance that changes the compressibility of a liquid.                                                                                                                  |-|
| partition-id:reference-data--AdditiveRole:Mothball                    | Mothball                       | Is this about naphthalene balls or "mothballing" as in preserving equipment for future reactivation?                                                                       |-|
| partition-id:reference-data--AdditiveRole:MudAcid                     | Mud acid                       | -                                                                                                                                                                          |-|
| partition-id:reference-data--AdditiveRole:NonemulsifiedIronControl    | Nonemulsified iron control     | -                                                                                                                                                                          |-|
| partition-id:reference-data--AdditiveRole:Nonemulsifier               | Non-emulsifier                 | A surfactant to inhibit the emulsion tendency of a treatment fluid with residual oil in the formation.                                                                     |-|
| partition-id:reference-data--AdditiveRole:NonionicSurfactant          | Nonionic surfactant            | A chemical that modifies the surface tension of a liquid.                                                                                                                  |-|
| partition-id:reference-data--AdditiveRole:Odorizer                    | Odorizer                       | A substance that imparts an odor to natural gas.                                                                                                                           |-|
| partition-id:reference-data--AdditiveRole:Oxidizer                    | Oxidizer                       | A water treatment reactant to oxidize or release oxygen.                                                                                                                   |-|
| partition-id:reference-data--AdditiveRole:OxygenScavenger             | Oxygen scavenger               | A chemical to remove dissolved oxygen from water-flow streams.                                                                                                             |-|
| partition-id:reference-data--AdditiveRole:Preflush                    | Preflush                       | A substance to dissolve carbonate or to displace and isolate incompatible formation fluids.                                                                                |-|
| partition-id:reference-data--AdditiveRole:ProppantTransport           | Proppant transport             | A proppant transport additive is used to adjust the viscosity or other property of the transport fluid.                                                                    |-|
| partition-id:reference-data--AdditiveRole:RadioactiveTracer           | Radioactive tracer             | A radioactive substance added to a fluid to enable the remote detection of the fluid location or flow path.                                                                |-|
| partition-id:reference-data--AdditiveRole:ResinActivator              | Resin activator                | A catalyst for setting resin.                                                                                                                                              |-|
| partition-id:reference-data--AdditiveRole:Retarder                    | Retarder                       | A chemical that slows a reaction, such as the set time of cement.                                                                                                          |-|
| partition-id:reference-data--AdditiveRole:ScaleInhibitor              | Scale inhibitor                | A substance that prevents the formation of iron scale.                                                                                                                     |-|
| partition-id:reference-data--AdditiveRole:SetEnhancer                 | Set enhancer                   | A substance that encourages the setting of cement.                                                                                                                         |-|
| partition-id:reference-data--AdditiveRole:Spacer                      | Spacer                         | In the context of pumping, a spacer is a fluid used between two fluids to prevent contamination.                                                                           |-|
| partition-id:reference-data--AdditiveRole:Stabilizer                  | Stabilizer                     | A chemical or physical effect that prevents separation of two or more, normally immiscible phases.                                                                         |-|
| partition-id:reference-data--AdditiveRole:SulfideInhibitor            | Sulfide inhibitor              | A chemical that reduces or prevents the formation of iron sulfide scale.                                                                                                   |-|
| partition-id:reference-data--AdditiveRole:Surfactant                  | Surfactant                     | A chemical that modifies the surface tension of a liquid.                                                                                                                  |-|
| partition-id:reference-data--AdditiveRole:SuspendingAgent             | Suspending agent               | A surfactant used in acid-treating solutions and fracturing fluids to suspend insoluble fines.                                                                             |-|
| partition-id:reference-data--AdditiveRole:TemperatureStabilizer       | Temperature stabilizer         | A substance that reduces the degradation of polymers at higher thermal conditions. It stabilizes the polymer not the temperature.                                          |-|
| partition-id:reference-data--AdditiveRole:TensileStrengthAdjustor     | Tensile strength adjustor      | A substance to increase the tensile strength of a fluid.                                                                                                                   |-|
| partition-id:reference-data--AdditiveRole:Thixotropic                 | Thixotropic                    | A substance that changes the thixotropic property of a fluid.                                                                                                              |-|
| partition-id:reference-data--AdditiveRole:Viscosifier                 | Viscosifier                    | A substance that increases the viscosity of a fluid.                                                                                                                       |-|
| partition-id:reference-data--AdditiveRole:WaxInhibitor                | Wax inhibitor                  | A chemical that prevents wax from being deposited in pipes.                                                                                                                |-|
| partition-id:reference-data--AdditiveRole:WeightingAgent              | Weighting agent                | A substance that increases the density of a fluid.                                                                                                                         |-|
| partition-id:reference-data--AdditiveRole:pHBuffer                    | pH buffer                      | A substance to keep the pH of a liquid within the desired range.                                                                                                           |-|

[Back to TOC](#TOC)

---

## AdditiveType:1.0.1

PPDM assumes governance of this OPEN reference value list and splits it into types and roles, where the roles are
defined in a new reference value list `AdditiveRole`. Further context is provided in 
[ChangesAfter-2022-09-23.md](ChangesAfter-2022-09-23.md).

A non-trivial upgrade is required; details in the following list:

PPDM revised the description, which requires a patch version increment to
[`osdu:wks:reference-data--AdditiveType:1.0.1`](../../Manifests/reference-data/OPEN/AdditiveType.1.0.1.json). In addition,
AdditiveType has been split into AdditiveType (a smaller list of non-deprecated codes) and 
[AdditiveRole](../../Manifests/reference-data/OPEN/AdditiveRole.1.0.0.json) capturing the
role for the additive rather than the type. This causes a more complex upgrade.

Deprecations are listed below:

| Deprecated `id`                                                 | Superseded by ìd`                                                         | Superseded Name    | Description                                                                                                                                                                                                                                    |
|-----------------------------------------------------------------|---------------------------------------------------------------------------|--------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--AdditiveType:Accelerator           | partition-id:reference-data--AdditiveType:AdditiveRole:Accelerator        | Accelerator        | DEPRECATED: Please use Code="AdditiveRole:Accelerator" with Name="Accelerator" instead. A chemical that speeds up the rate of a reaction, especially in cement.                                                                                |
| partition-id:reference-data--AdditiveType:Anti%20Foam           | partition-id:reference-data--AdditiveType:AdditiveRole:Antifoam           | Antifoam           | DEPRECATED: Please use Code="AdditiveRole:Antifoam" with Name="Antifoam" instead. A material that destabilizes a foam.                                                                                                                         |
| partition-id:reference-data--AdditiveType:Anti%20Gelation       | partition-id:reference-data--AdditiveType:AdditiveRole:AntiGelation       | AntiGelation       | DEPRECATED: Please use Code="AdditiveRole:AntiGelation" with Name="AntiGelation" instead. A substance that prevents or retards the formation of a gel.                                                                                         |
| partition-id:reference-data--AdditiveType:Anti%20Settling       | partition-id:reference-data--AdditiveType:AdditiveRole:AntiSettling       | AntiSettling       | DEPRECATED: Please use Code="AdditiveRole:AntiSettling" with Name="AntiSettling" instead. A substance that prevents or retards the gravity separation of a solid from a liquid.                                                                |
| partition-id:reference-data--AdditiveType:Anti%20Static         | partition-id:reference-data--AdditiveType:AdditiveRole:Antistatic         | Antistatic         | DEPRECATED: Please use Code="AdditiveRole:Antistatic" with Name="Antistatic" instead. A substance that reduces or eliminates the buildup of static electricity.                                                                                |
| partition-id:reference-data--AdditiveType:Antifoam              | partition-id:reference-data--AdditiveType:AdditiveRole:Antifoam           | Antifoam           | DEPRECATED: Please use Code="AdditiveRole:Antifoam" with Name="Antifoam" instead.                                                                                                                                                              |
| partition-id:reference-data--AdditiveType:Bactericide           | partition-id:reference-data--AdditiveType:AdditiveRole:Biocide            | Biocide            | DEPRECATED: Please use Code="AdditiveRole:Biocide" with Name="Biocide" instead. A product that kills bacteria in the water or on the surface of the pipe.                                                                                      |
| partition-id:reference-data--AdditiveType:Bond%20Accelerator    | partition-id:reference-data--AdditiveType:AdditiveRole:BondAccelerator    | BondAccelerator    | DEPRECATED: Please use Code="AdditiveRole:BondAccelerator" with Name="BondAccelerator" instead. A substance that reduces the reaction time for materials to bond together, especially in cement.                                               |
| partition-id:reference-data--AdditiveType:Bonding%20Agent       | partition-id:reference-data--AdditiveType:AdditiveRole:BondingAgent       | BondingAgent       | DEPRECATED: Please use Code="AdditiveRole:BondingAgent" with Name="BondingAgent" instead. A substance that induces materials to bond together, especially in cement.                                                                           |
| partition-id:reference-data--AdditiveType:Corrosion%20Inhibitor | partition-id:reference-data--AdditiveType:AdditiveRole:CorrosionInhibitor | CorrosionInhibitor | DEPRECATED: Please use Code="AdditiveRole:CorrosionInhibitor" with Name="CorrosionInhibitor" instead. A substance to prevent or slow down the reaction of a metal with a fluid.                                                                |
| partition-id:reference-data--AdditiveType:Defoamer              | partition-id:reference-data--AdditiveType:AdditiveRole:Defoamer           | Defoamer           | DEPRECATED: Please use Code="AdditiveRole:Defoamer" with Name="Defoamer" instead. A chemical that reduces the stability of the bubble skin in a foam and cause the foam to break.                                                              |
| partition-id:reference-data--AdditiveType:Dispenser             | partition-id:reference-data--AdditiveType:AdditiveRole:Dispersant         | Dispersant         | DEPRECATED: Please use Code="AdditiveRole:Dispersant" with Name="Dispersant" instead.                                                                                                                                                          |
| partition-id:reference-data--AdditiveType:Dispersant            | partition-id:reference-data--AdditiveType:AdditiveRole:Dispersant         | Dispersant         | DEPRECATED: Please use Code="AdditiveRole:Dispersant" with Name="Dispersant" instead. A substance that aids in breaking up a mass of particles, bubbles or droplets.                                                                           |
| partition-id:reference-data--AdditiveType:Emulsifier            | partition-id:reference-data--AdditiveType:AdditiveRole:Emulsifier         | Emulsifier         | DEPRECATED: Please use Code="AdditiveRole:Emulsifier" with Name="Emulsifier" instead. A substance that creates or stabilizes an emulsion.                                                                                                      |
| partition-id:reference-data--AdditiveType:Expansion             | partition-id:reference-data--AdditiveType:AdditiveRole:Expansion          | Expansion          | DEPRECATED: Please use Code="AdditiveRole:Expansion" with Name="Expansion" instead.                                                                                                                                                            |
| partition-id:reference-data--AdditiveType:Extender              | partition-id:reference-data--AdditiveType:AdditiveRole:Extender           | Extender           | DEPRECATED: Please use Code="AdditiveRole:Extender" with Name="Extender" instead. A substance that decreasing the density of a cement slurry.                                                                                                  |
| partition-id:reference-data--AdditiveType:Fe%20sequestering     | partition-id:reference-data--AdditiveType:AdditiveRole:Chelant            | Chelant            | DEPRECATED: Please use Code="AdditiveRole:Chelant" with Name="Chelant" instead.                                                                                                                                                                |
| partition-id:reference-data--AdditiveType:Flow%20Enhancer       | partition-id:reference-data--AdditiveType:AdditiveRole:FlowEnhancer       | FlowEnhancer       | DEPRECATED: Please use Code="AdditiveRole:FlowEnhancer" with Name="FlowEnhancer" instead. A substance that improves the rate of flow of a liquid.                                                                                              |
| partition-id:reference-data--AdditiveType:Fluid%20Loss          | partition-id:reference-data--AdditiveType:AdditiveRole:FluidLoss          | FluidLoss          | DEPRECATED: Please use Code="AdditiveRole:FluidLoss" with Name="FluidLoss" instead.                                                                                                                                                            |
| partition-id:reference-data--AdditiveType:Foaming%20Agent       | partition-id:reference-data--AdditiveType:AdditiveRole:FoamingAgent       | FoamingAgent       | DEPRECATED: Please use Code="AdditiveRole:FoamingAgent" with Name="FoamingAgent" instead. A substance that encourages an emulsion, being the suspension of a gas in a liquid.                                                                  |
| partition-id:reference-data--AdditiveType:Formation%20Sealer    | partition-id:reference-data--AdditiveType:AdditiveRole:FormationSealer    | FormationSealer    | DEPRECATED: Please use Code="AdditiveRole:FormationSealer" with Name="FormationSealer" instead. A substance that improves the ability of a fluid to close a formation zone, thereby preventing lost circulation or improving zone abandonment. |
| partition-id:reference-data--AdditiveType:Free%20Water%20Ctrl   | partition-id:reference-data--AdditiveType:AdditiveRole:FreeWaterControl   | FreeWaterControl   | DEPRECATED: Please use Code="AdditiveRole:FreeWaterControl" with Name="FreeWaterControl" instead. An additive to the cement slurry to address the potential problem of particle sedimentation.                                                 |
| partition-id:reference-data--AdditiveType:Free%20Water          | partition-id:reference-data--AdditiveType:AdditiveRole:FreeWaterControl   | FreeWaterControl   | DEPRECATED: Please use Code="AdditiveRole:FreeWaterControl" with Name="FreeWaterControl" instead. Water that is not in an emulsion or not chemically bonded.                                                                                   |
| partition-id:reference-data--AdditiveType:Friction%20Reducing   | partition-id:reference-data--AdditiveType:AdditiveRole:FrictionReducer    | FrictionReducer    | DEPRECATED: Please use Code="AdditiveRole:FrictionReducer" with Name="FrictionReducer" instead. A material, usually a polymer that reduces the friction of flowing fluid in a conduit                                                          |
| partition-id:reference-data--AdditiveType:Gas%20Migration       | partition-id:reference-data--AdditiveType:AdditiveRole:GasMigration       | GasMigration       | DEPRECATED: Please use Code="AdditiveRole:GasMigration" with Name="GasMigration" instead. Primarily used as an additive in a cement slurry                                                                                                     |
| partition-id:reference-data--AdditiveType:Gelling%20Agent       | partition-id:reference-data--AdditiveType:AdditiveRole:Gellant            | Gellant            | DEPRECATED: Please use Code="AdditiveRole:Gellant" with Name="Gellant" instead. A substance that increases the viscosity of a liquid.                                                                                                          |
| partition-id:reference-data--AdditiveType:Light%20Weight        | partition-id:reference-data--AdditiveType:AdditiveRole:Lightweight        | Lightweight        | DEPRECATED: Please use Code="AdditiveRole:Lightweight" with Name="Lightweight" instead. A substance that decreasing the density of a cement slurry.                                                                                            |
| partition-id:reference-data--AdditiveType:Liquid%20Extender     | partition-id:reference-data--AdditiveType:AdditiveRole:LiquidExtender     | LiquidExtender     | DEPRECATED: Please use Code="AdditiveRole:LiquidExtender" with Name="LiquidExtender" instead. A substance that decreasing the density of a liquid.                                                                                             |
| partition-id:reference-data--AdditiveType:Lost%20circulation    | partition-id:reference-data--AdditiveType:AdditiveRole:LostCirculation    | LostCirculation    | DEPRECATED: Please use Code="AdditiveRole:LostCirculation" with Name="LostCirculation" instead.                                                                                                                                                |
| partition-id:reference-data--AdditiveType:Mixing%20Fluid        | partition-id:reference-data--AdditiveType:AdditiveRole:MixingFluid        | MixingFluid        | DEPRECATED: Please use Code="AdditiveRole:MixingFluid" with Name="MixingFluid" instead.                                                                                                                                                        |
| partition-id:reference-data--AdditiveType:Mutual%20Solvent      | Not endorsed by PPDM                                                      | -                  | A chemical that have some common solvency for both water and oil materials.                                                                                                                                                                    |
| partition-id:reference-data--AdditiveType:Preflush              | partition-id:reference-data--AdditiveType:AdditiveRole:Preflush           | Preflush           | DEPRECATED: Please use Code="AdditiveRole:Preflush" with Name="Preflush" instead. A substance to dissolve carbonate or to displace and isolate incompatible formation fluids.                                                                  |
| partition-id:reference-data--AdditiveType:Retarder%20Acc        | partition-id:reference-data--AdditiveType:AdditiveRole:Retarder           | Retarder           | DEPRECATED: Please use Code="AdditiveRole:Retarder" with Name="Retarder" instead.                                                                                                                                                              |
| partition-id:reference-data--AdditiveType:Retarder              | partition-id:reference-data--AdditiveType:AdditiveRole:Retarder           | Retarder           | DEPRECATED: Please use Code="AdditiveRole:Retarder" with Name="Retarder" instead. A chemical that slows a reaction, such as the set time of cement.                                                                                            |
| partition-id:reference-data--AdditiveType:Scale%20Inhibitor     | partition-id:reference-data--AdditiveType:AdditiveRole:ScaleInhibitor     | ScaleInhibitor     | DEPRECATED: Please use Code="AdditiveRole:ScaleInhibitor" with Name="ScaleInhibitor" instead. A substance that prevents the formation of iron scale.                                                                                           |
| partition-id:reference-data--AdditiveType:Spacer                | partition-id:reference-data--AdditiveType:AdditiveRole:Spacer             | Spacer             | DEPRECATED: Please use Code="AdditiveRole:Spacer" with Name="Spacer" instead. In the context of pumping, a spacer is a fluid used between two fluids to prevent contamination.                                                                 |
| partition-id:reference-data--AdditiveType:Stabilizer            | partition-id:reference-data--AdditiveType:AdditiveRole:Stabilizer         | Stabilizer         | DEPRECATED: Please use Code="AdditiveRole:Stabilizer" with Name="Stabilizer" instead. A chemical or physical effect that prevents separation of two or more, normally immiscible phases.                                                       |
| partition-id:reference-data--AdditiveType:Strength              | Not endorsed by PPDM                                                      | -                  | -                                                                                                                                                                                                                                              |
| partition-id:reference-data--AdditiveType:Surfactant            | partition-id:reference-data--AdditiveType:AdditiveRole:Surfactant         | Surfactant         | DEPRECATED: Please use Code="AdditiveRole:Surfactant" with Name="Surfactant" instead. A chemical that modifies the surface tension of a liquid.                                                                                                |
| partition-id:reference-data--AdditiveType:Suspending%20Agent    | partition-id:reference-data--AdditiveType:AdditiveRole:SuspendingAgent    | SuspendingAgent    | DEPRECATED: Please use Code="AdditiveRole:SuspendingAgent" with Name="SuspendingAgent" instead. A surfactant used in acid-treating solutions and fracturing fluids to suspend insoluble fines.                                                 |
| partition-id:reference-data--AdditiveType:Thixotropic           | partition-id:reference-data--AdditiveType:AdditiveRole:Thixotropic        | Thixotropic        | DEPRECATED: Please use Code="AdditiveRole:Thixotropic" with Name="Thixotropic" instead. A substance that changes the thixotropic property of a fluid.                                                                                          |
| partition-id:reference-data--AdditiveType:Viscosifier           | partition-id:reference-data--AdditiveType:AdditiveRole:Viscosifier        | Viscosifier        | DEPRECATED: Please use Code="AdditiveRole:Viscosifier" with Name="Viscosifier" instead. A substance that increases the viscosity of a fluid.                                                                                                   |
| partition-id:reference-data--AdditiveType:Water%20sensitive     | partition-id:reference-data--AdditiveType:AdditiveRole:ClayStabilizer     | ClayStabilizer     | DEPRECATED: Please use Code="AdditiveRole:ClayStabilizer" with Name="ClayStabilizer" instead.                                                                                                                                                  |
| partition-id:reference-data--AdditiveType:Weighting%20agent     | partition-id:reference-data--AdditiveType:AdditiveRole:WeightingAgent     | WeightingAgent     | DEPRECATED: Please use Code="AdditiveRole:WeightingAgent" with Name="WeightingAgent" instead. A substance that increases the density of a fluid.                                                                                               |

[Back to TOC](#TOC)

---

## BitType:1.0.1

Update of NameAlias[] for `BitType:RollerCone`:

| `id`                                               | Name        | Description                                                                                                                                                      | Deprecated? |
|----------------------------------------------------|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|
| partition-id:reference-data--BitType:RollerCone    | Roller cone | The bit has conical teeth, usually on 3 rollers (tricone.) Bicone is much less common.                                                                           | -           |

Deprecated values:

| Deprecated `id`                                             | Superseded by `id`                              | Superseded Name | Description                                                                                                                                                      |
|-------------------------------------------------------------|-------------------------------------------------|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--BitType:PDC%20core             | partition-id:reference-data--BitType:PDC        | PDC             | DEPRECATED: Please use Code="PDC" with Name="PDC" instead. polycrystalline diamond compact fixed cutter bit                                                      |
| partition-id:reference-data--BitType:diamond%20core         | partition-id:reference-data--BitType:Core       | Core            | DEPRECATED: Please use Code="Core" with Name="Core" instead. Diamond core bit                                                                                    |
| partition-id:reference-data--BitType:diamond                | Not endorsed by PPDM                            | -               | Diamond bit                                                                                                                                                      |
| partition-id:reference-data--BitType:insert%20roller%20cone | partition-id:reference-data--BitType:RollerCone | Roller cone     | DEPRECATED: Please use Code="RollerCone" with Name="Roller cone" instead. Insert roller cone bit                                                                 |
| partition-id:reference-data--BitType:roller%20cone          | partition-id:reference-data--BitType:RollerCone | Roller cone     | DEPRECATED: Please use Code="RollerCone" with Name="Roller cone" instead. The bit has conical teeth, usually on 3 rollers (tricone.) Bicone is much less common. |
| partition-id:reference-data--BitType:unknown                | partition-id:reference-data--BitType:Unknown    | Unknown         | DEPRECATED: Please use Code="Unknown" with Name="Unknown" instead. The type of bit is unreported or unknown.                                                     |



[Back to TOC](#TOC)

---
## CoordinateTransformation:1.1.0 (CRS_CT)

CoordinateTransformation records did not carry populated `Description` values extracted from the EPSG GeoRepository
Remarks. This has been corrected in M15. The changes are decorative. Similarly, some well-known text definitions
not (yet) supported by some CRS engines have been added using the EsriWktDynamicGenerator.

315 out of 353 CoordinateTransformation records got updated. The same amount of change was introduced into the sorted
CRS_CT.json combining CoordinateReferenceSystem:1.1.0 and CoordinateTransformation:1.1.0.

[Back to TOC](#TOC)

---

## DrillingReasonType:1.0.0

DrillingReasonType - as a reference-data type - will be deprecated eventually. For the time being, the schema is not
marked as deprecated, but all values are. The codes can be transferred to LaheeClass.

| Deprecated `id`                                                   | Superseded by `ìd` Note: **Different Type**               | Superseded Name   | Description                                                                                                                                                                              |
|-------------------------------------------------------------------|-----------------------------------------------------------|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--DrillingReasonType:DeeperPoolTest    | partition-id:reference-data--LaheeClass:DeeperPoolTest    | DeeperPoolTest    | DEPRECATED: Please use Code="LaheeClass:DeeperPoolTest" with Name="DeeperPoolTest" instead. Exploratory well within known production but deeper than deepest test                        |
| partition-id:reference-data--DrillingReasonType:Development       | partition-id:reference-data--LaheeClass:Development       | Development       | DEPRECATED: Please use Code="LaheeClass:Development" with Name="Development" instead. A wellbore drilled in a zone in an area already proved productive. (POSC definition – Development) |
| partition-id:reference-data--DrillingReasonType:Extension         | partition-id:reference-data--LaheeClass:Extension         | Extension         | DEPRECATED: Please use Code="LaheeClass:Extension" with Name="Extension" instead. Well to significantly extend partly developed pool                                                     |
| partition-id:reference-data--DrillingReasonType:NewFieldWildcat   | partition-id:reference-data--LaheeClass:NewFieldWildcat   | NewFieldWildcat   | DEPRECATED: Please use Code="LaheeClass:NewFieldWildcat" with Name="NewFieldWildcat" instead. Exploratory well on untested structure                                                     |
| partition-id:reference-data--DrillingReasonType:NewPoolWildcat    | partition-id:reference-data--LaheeClass:NewFieldWildcat   | NewFieldWildcat   | DEPRECATED: Please use Code="LaheeClass:NewFieldWildcat" with Name="NewFieldWildcat" instead. Exploratory well on tested structure outside known production                              |
| partition-id:reference-data--DrillingReasonType:Service           | partition-id:reference-data--LaheeClass:Service           | Service           | DEPRECATED: Please use Code="LaheeClass:Service" with Name="Service" instead. Well to support production (injection, disposal, observation)                                              |
| partition-id:reference-data--DrillingReasonType:ShallowerPoolTest | partition-id:reference-data--LaheeClass:ShallowerPoolTest | ShallowerPoolTest | DEPRECATED: Please use Code="LaheeClass:ShallowerPoolTest" with Name="ShallowerPoolTest" instead. Exploratory well within known production but shallower than deepest test               |
| partition-id:reference-data--DrillingReasonType:StratigraphicTest | partition-id:reference-data--LaheeClass:StratigraphicTest | StratigraphicTest | DEPRECATED: Please use Code="LaheeClass:StratigraphicTest" with Name="StratigraphicTest" instead. Wellbore drilled to test geology.                                                      |

[Back to TOC](#TOC)

---

## LaheeClass:1.0.0

New PPDM governed reference value list superseding DrillingReasonType, see above.

| `id`                                                      | Name                | Description                                                                                                                                                                                                          |
|-----------------------------------------------------------|---------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--LaheeClass:DeeperPoolTest    | Deeper pool test    | An exploratory well drilled in proximity to known pools of hydrocarbon, with the purpose of discovering new pools at deeper stratigraphic levels.                                                                    |-|
| partition-id:reference-data--LaheeClass:Development       | Development         | A well drilled to further exploit or expand a known pool (AER).  A wellbore drilled in a zone in an area already proved productive. (AAPG, POSC definition – Development)                                            |-|
| partition-id:reference-data--LaheeClass:Extension         | Extension           | Well to significantly extend partly developed pool.  An exploratory well drilled to extend the limits of a known pool.                                                                                               |-|
| partition-id:reference-data--LaheeClass:NewFieldWildcat   | New field wildcat   | An exploratory well located at a considerable distance beyond the limits of known pools, drilled to search for hydrocarbons.                                                                                         |-|
| partition-id:reference-data--LaheeClass:NewPoolWildcat    | New pool wildcat    | An exploratory well drilled to discover new pools in an already discovered field.  Such wells usually will be less than 2 miles from the nearest productive area.                                                    |-|
| partition-id:reference-data--LaheeClass:Other             | Other               | A well drilled with the intent other than the production of oil or gas, or stimulation of hydrocarbons. This was not in the original Lahee classification.                                                           |-|
| partition-id:reference-data--LaheeClass:Outpost           | Outpost             | A well located and drilled with the expectation of extending for a considerable distance the productive area of a partly developed pool. It is usually 2 or more locations distant from the nearest productive site. |-|
| partition-id:reference-data--LaheeClass:Service           | Service             | Well to support production (injection, disposal, observation)                                                                                                                                                        |-|
| partition-id:reference-data--LaheeClass:ShallowerPoolTest | Shallower pool test | An exploratory well drilled in proximity to known pools of hydrocarbon, with the purpose of discovering new pools at shallower stratigraphic levels.                                                                 |-|
| partition-id:reference-data--LaheeClass:StratigraphicTest | Stratigraphic test  | A well drilled with the purpose of gathering geological information on the stratigraphy of an area.                                                                                                                  |-|


[Back to TOC](#TOC)

---

## SeismicEnergySourceType:1.0.1

The content of this reference list is now governed by PPDM. The schema is patched to carry PPDM's definition of this
reference value type. The major difference is a hierarchical organization of sources and the addition of many more
specialized types. All previous 4 values are deprecated, 19 new values are added. Due to the enhanced completeness the 
governance is now changed to OPEN.

| Deprecated `id`                                                | Superseded by `id`                                                      | Superseded Name    | Description                                                                                                                                                                                |
|----------------------------------------------------------------|-------------------------------------------------------------------------|--------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--SeismicEnergySourceType:Airgun    | partition-id:reference-data--SeismicEnergySourceType:AirGun             | AirGun             | DEPRECATED: Please use Code="AirGun" with Name="AirGun" instead. Compressed air is released from a chamber towed by a ship. Originally developed by Bolt Technology.                       |
| partition-id:reference-data--SeismicEnergySourceType:Dynamite  | partition-id:reference-data--SeismicEnergySourceType:Explosion.Dynamite | Explosion.Dynamite | DEPRECATED: Please use Code="Explosion.Dynamite" with Name="Explosion.Dynamite" instead. A high energy chemical explosive.                                                                 |
| partition-id:reference-data--SeismicEnergySourceType:Vibroseis | partition-id:reference-data--SeismicEnergySourceType:Vibrator.Vibroseis | Vibrator.Vibroseis | DEPRECATED: Please use Code="Vibrator.Vibroseis" with Name="Vibrator.Vibroseis" instead. Uses oscillating mass coupled to Earth's surface onshore.                                         |
| partition-id:reference-data--SeismicEnergySourceType:Watergun  | partition-id:reference-data--SeismicEnergySourceType:Implosion.WaterGun | Implosion.WaterGun | DEPRECATED: Please use Code="Implosion.WaterGun" with Name="Implosion.WaterGun" instead. A seismic source that propels a slug of water into the water mass, producing an implosive effect. |

[Back to TOC](#TOC)

---

## SEGY-HeaderMappingTemplate:1.0.0

A new reference-value list: _The template for SEG-Y trace header mappings to facilitate SEG-Y trace header parsing and trace header value extraction, for, e.g., navigation._

The governance is LOCAL, the values shipped by OSDU are only expected to serve as examples. The following examples are provided as suggestions:

| `id`                                                                         | Name                    | Description                                                                                                |
|------------------------------------------------------------------------------|-------------------------|------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--SEGY-HeaderMappingTemplate:Corrected3DMetric    | Corrected 3D Metric     | A SEG-Y template to read from corrected header positions since the original values were found incorrect.   |
| partition-id:reference-data--SEGY-HeaderMappingTemplate:MetricUTM3D          | Metric UTM 3D           | A SEG-Y Template for metric UTM coordinates and 100 scaling, developed for Volve.                          |
| partition-id:reference-data--SEGY-HeaderMappingTemplate:PetrelLegacy3DFeet   | Petrel Legacy 3D Feet   | A SEG-Y template using an (incorrect) non-standard start time location for international feet coordinates. |
| partition-id:reference-data--SEGY-HeaderMappingTemplate:PetrelLegacy3DMetric | Petrel Legacy 3D Metric | A SEG-Y template using an (incorrect) non-standard start time location for metric coordinates.             |
| partition-id:reference-data--SEGY-HeaderMappingTemplate:PetrelLegacy3DUSFeet | Petrel Legacy 3D USFeet | A SEG-Y template using an (incorrect) non-standard start time location for US-feet coordinates.            |
| partition-id:reference-data--SEGY-HeaderMappingTemplate:Popular3DFeet        | Popular 3D Feet         | A popular SEG-Y header position template for international feet coordinates.                               |
| partition-id:reference-data--SEGY-HeaderMappingTemplate:Popular3DMetric      | Popular 3D Metric       | A popular SEG-Y header position template for metric coordinates.                                           |
| partition-id:reference-data--SEGY-HeaderMappingTemplate:Popular3DUSFeet      | Popular 3D US-Feet      | A popular SEG-Y header position template for US feet coordinates.                                          |

[Back to TOC](#TOC)

---

## TubularAssemblyStatusType:1.0.0

The TubularAssembly**Status**Type accidentally contained a copy of TubularAssemblyType. The 58 copies - clearly a
mistake - have been removed. Instead, three state types were added:

| `id`                                                                    | Name             | Description                                                                                                                                                                                                                                                           |
|-------------------------------------------------------------------------|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--TubularAssemblyStatusType:Installed        | Installed        | The TubularAssembly is deployed in the Wellbore.                                                                                                                                                                                                                      |-|
| partition-id:reference-data--TubularAssemblyStatusType:Planned          | Planned          | The TubularAssembly is in its planning stage. This value is redundant if the data.ExistenceKind is set to Planned.                                                                                                                                                    |-|
| partition-id:reference-data--TubularAssemblyStatusType:Pulled.Partially | Partially Pulled | A partial pull is where the casing/tubing is cut and pulled above the cut to recycle or sell for scrap. This is usually above the top of cement when cemented or above a stuck point if the tubing is stuck in hole. The casing/tubing below the cut is left in hole. |-|
| partition-id:reference-data--TubularAssemblyStatusType:Pulled           | Pulled           | The TubularAssembly is no longer in place and has been pulled from the Wellbore.                                                                                                                                                                                      |-|

[Back to TOC](#TOC)

---

## VerticalMeasurementPath:1.0.1

The schema was patched to version 1.0.1 to align with PPDM's type description.

| Deprecated `id`                                           | Superseded by `id`                                                      | Superseded Name    | Description                                                                                                                                                                                                                                                                                                                              |
|-----------------------------------------------------------|-------------------------------------------------------------------------|--------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--VerticalMeasurementPath:ELEV | partition-id:reference-data--VerticalMeasurementPath:TrueVerticalHeight | TrueVerticalHeight | DEPRECATED: Please use Code="TrueVerticalHeight" with Name="TrueVerticalHeight" instead. Measurement is in the positive vertical direction and therefore represents an elevation                                                                                                                                                         |
| partition-id:reference-data--VerticalMeasurementPath:MD   | partition-id:reference-data--VerticalMeasurementPath:MeasuredDepth      | MeasuredDepth      | DEPRECATED: Please use Code="MeasuredDepth" with Name="MeasuredDepth" instead. Measured depth is the measured distance along a wellbore path from the reference point. Unless the wellbore is perfectly aligned to the vertical plane, measured depth is not strictly a vertical measurement.                                            |
| partition-id:reference-data--VerticalMeasurementPath:TVD  | partition-id:reference-data--VerticalMeasurementPath:TrueVerticalDepth  | TrueVerticalDepth  | DEPRECATED: Please use Code="TrueVerticalDepth" with Name="TrueVerticalDepth" instead. True vertical depth is the distance in the vertical plane from the horizontal plane of the reference datum to a point within a wellbore. TVD is not reported for wells that are deemed to be vertical and/or have no wellbore directional survey. |


[Back to TOC](#TOC)

---

## VerticalMeasurementType:1.0.2

Update of NameAlias[] for `KellyBushing`, which had incorrectly a single NameAlias with comma separated values intended
to be alias name objects:

| `id`                                                              | Name          | Description                                                                                                      | Deprecated? |
|-------------------------------------------------------------------|---------------|------------------------------------------------------------------------------------------------------------------|-------------|
| partition-id:reference-data--VerticalMeasurementType:KellyBushing | Kelly bushing | A device on top of the rotary table, used to transmit torque to the kelly (a special section of the drill pipe.) | -           |

[Back to TOC](#TOC)

---

## WellBusinessIntention:1.0.0

PPDM augmented the list with `Reacquire`:

| `id`                                                         | Name       | Description                                                                                                                                                                                     |
|--------------------------------------------------------------|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--WellBusinessIntention:Reacquire | Re-acquire | A well drilled to intersect another well for the purpose of extending the utility of a well whose surface borehole has been lost or damaged.                                                    |-|


[Back to TOC](#TOC)

---

# Schema Upgrade Resources

M15 introduces an initial set of resources, which is supposed to facilitate Storage record upgrades addressing
structural upgrades due to schema evolution and reference-value upgrades due to deprecations. 

The resources are auto-generated (by default) but can be augmented by OSDU and operators as the governance model is
LOCAL.

## Schema Upgrades: SchemaUpgradeSpecification:1.0.0

**New:** _A reference list containing schema upgrade specifications feeding the OSDU schema upgrade service._ For each
incremental schema version such a record is created and populated with a default JOLT operation to update the `kind`
property. the content is generated by the Schema Upgrades step in the OsduSchemaComposer on the schema repository.

### Supporting: SchemaUpgradeChain:1.0.0

**New Helper Catalog:** _Given a kind (via the Code/id), list the SchemaUpgradeSpecification ids, to which upgrade
specifications exist and list the source kinds, which can ultimately be upgraded to the current (target) kind._

## Reference Value Upgrades: ReferenceValueUpgradeLookUp:1.0.0

**New:** _ReferenceValueUpgradeLookUp provides a list of reference-data usages, associated to a look-up table from
deprecated to superseded reference values._ This reference value list is created from the deprecation/superseded by
information from individual reference values and is merged with usage information derived from the schemas.

### Supporting: DiscoverabilityBySearch:1.0.0 (FIXED)

**New:** _The enumeration of different levels of query discoverability from simple queries, flattened array queries,
nested array queries or no discoverability at all (no-indexed arrays of objects)._ The list comes with the following 
four values:

| `id`                                                                | Name            | Description                                                                                                                                                                                                                               |
|---------------------------------------------------------------------|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--DiscoverabilityBySearch:None           | None            | The property in context is located in an object inside an array, which is not indexed, see https://community.opengroup.org/osdu/platform/system/search-service/-/blob/master/docs/tutorial/ArrayOfObjects.md#default-1.                   |-|
| partition-id:reference-data--DiscoverabilityBySearch:QueryFlattened | Query Flattened | The property in context is located in an object inside an array, which is indexed as flattened', see https://community.opengroup.org/osdu/platform/system/search-service/-/blob/master/docs/tutorial/ArrayOfObjects.md#flattened-queries. |-|
| partition-id:reference-data--DiscoverabilityBySearch:QueryNested    | Query Nested    | The property in context is located in an object inside an array, which is indexed as 'nested', see https://community.opengroup.org/osdu/platform/system/search-service/-/blob/master/docs/tutorial/ArrayOfObjects.md#nested-queries.      |-|
| partition-id:reference-data--DiscoverabilityBySearch:QuerySimple    | Query Simple    | The property in context is not part of an array of objects and is discoverable via simple queries, see https://community.opengroup.org/osdu/platform/system/search-service/-/blob/master/docs/tutorial/SearchService.md#text-queries.     |-|


[Back to TOC](#TOC)

---

[Back to Milestone Overview](../README.md)