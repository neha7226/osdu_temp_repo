<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractFacet [Status: Accepted]
A tuple FacetType, FacetRole, both calling specific references

FacetType: Enumeration of the type of additional context about the nature of a property type (it may include conditions, direction, qualifiers, or statistics).

FacetRole: Additional context about the nature of a property type. The purpose of such attribute is to minimize the need to create specialized property types by mutualizing some well known qualifiers such as "maximum", "minimum" which apply to a lot of different property types.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractFacet:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractFacet.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractFacet.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractFacet.1.0.0.json](../../Authoring/abstract/AbstractFacet.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractFacet.1.0.0.json](../../Generated/abstract/AbstractFacet.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractFacet.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractFacet.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractFacet


## Outgoing Relationships for AbstractFacet

![AbstractFacet](../_diagrams/abstract/AbstractFacet.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractFacet Referenced by other Entities

![AbstractFacet](../_diagrams/abstract/AbstractFacet.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacet Properties

## Table of AbstractFacet Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|FacetTypeID &rarr; [FacetType](../reference-data/FacetType.1.0.0.md)| |string|required|Facet Type ID|FacetType: An 'enumeration' of the type of additional context about the nature of a property type (it may include conditions, direction, qualifiers, or statistics).|<code>^[\w\-\.]+:reference-data\-\-FacetType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacetType](../reference-data/FacetType.1.0.0.md)|json-pointer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacetRoleID &rarr; [FacetRole](../reference-data/FacetRole.1.0.0.md)| |string|required|Facet Role ID|Additional context about the nature of a property type. The purpose of such attribute is to minimize the need to create specialized property types by mutualizing some well known qualifiers such as "maximum", "minimum" which apply to a lot of different property types.|<code>^[\w\-\.]+:reference-data\-\-FacetRole:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacetRole](../reference-data/FacetRole.1.0.0.md)|json-pointer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacet.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) | FacetIDs[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._