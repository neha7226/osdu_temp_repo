<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractWellboreDrillingReason [Status: Accepted]
Purpose for drilling a wellbore, which often is an indication of the level of risk.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractWellboreDrillingReason:1.1.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractWellboreDrillingReason.1.1.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractWellboreDrillingReason.1.1.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractWellboreDrillingReason.1.1.0.json](../../Authoring/abstract/AbstractWellboreDrillingReason.1.1.0.json)
* Link to &rarr; [Generated Schema AbstractWellboreDrillingReason.1.1.0.json](../../Generated/abstract/AbstractWellboreDrillingReason.1.1.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractWellboreDrillingReason.1.1.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractWellboreDrillingReason.1.1.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractWellboreDrillingReason


## Outgoing Relationships for AbstractWellboreDrillingReason

![AbstractWellboreDrillingReason](../_diagrams/abstract/AbstractWellboreDrillingReason.1.1.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractWellboreDrillingReason Referenced by other Entities

![AbstractWellboreDrillingReason](../_diagrams/abstract/AbstractWellboreDrillingReason.1.1.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractWellboreDrillingReason Properties

## Table of AbstractWellboreDrillingReason Properties (Version 1.1.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|LaheeClassID &rarr; [LaheeClass](../reference-data/LaheeClass.1.0.0.md)| |string|optional|Lahee Class ID|The Lahee classification, based on the traditional, commonly accepted, scheme to categorize wells by the general degree of risk assumed by the operator at the time of drilling.|<code>^[\w\-\.]+:reference-data\-\-LaheeClass:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [LaheeClass](../reference-data/LaheeClass.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|~~DrillingReasonTypeID &rarr; [DrillingReasonType](../reference-data/DrillingReasonType.1.0.0.md)~~| |~~string~~|~~optional~~|~~Drilling Reason Type ID~~|~~DEPRECATED: Superseded by LaheeClassID. Identifier of the drilling reason type for the corresponding time period.~~|~~<code>^[\w\-\.]+:reference-data\-\-DrillingReasonType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [DrillingReasonType](../reference-data/DrillingReasonType.1.0.0.md)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|EffectiveDateTime| |string|optional|Effective Date Time|The date and time at which the event becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TerminationDateTime| |string|optional|Termination Date Time|The date and time at which the event is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Remark| |string|optional|Remark|A remark or comment explaining the drilling reason or LaheeClass assignment.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractWellboreDrillingReason.1.1.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [Wellbore.1.2.0](../master-data/Wellbore.1.2.0.md) | DrillingReasons[] | many (as array) |  |
| Included by | [Wellbore.1.3.0](../master-data/Wellbore.1.3.0.md) | DrillingReasons[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._