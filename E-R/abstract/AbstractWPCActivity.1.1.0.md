<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractWPCActivity [Status: Accepted]
The activity abstraction included by projects (master-data) and work-product-component group-type specialisations.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractWPCActivity:1.1.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractWPCActivity.1.1.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractWPCActivity.1.1.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractWPCActivity.1.1.0.json](../../Authoring/abstract/AbstractWPCActivity.1.1.0.json)
* Link to &rarr; [Generated Schema AbstractWPCActivity.1.1.0.json](../../Generated/abstract/AbstractWPCActivity.1.1.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractWPCActivity.1.1.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractWPCActivity.1.1.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractWPCActivity


## Outgoing Relationships for AbstractWPCActivity

![AbstractWPCActivity](../_diagrams/abstract/AbstractWPCActivity.1.1.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractWPCActivity Referenced by other Entities

![AbstractWPCActivity](../_diagrams/abstract/AbstractWPCActivity.1.1.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractWPCActivity Properties

## Table of AbstractWPCActivity Properties (Version 1.1.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|ActivityTemplateID &rarr; [ActivityTemplate](../master-data/ActivityTemplate.1.0.0.md)| |string|optional|Activity Template ID|The relation to the ActivityTemplate carrying expected parameter definitions and default values.|<code>^[\w\-\.]+:master-data\-\-ActivityTemplate:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ActivityTemplate](../master-data/ActivityTemplate.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ParentProjectID| |string|optional|Parent Project ID|The relationship to a parent project acting as a parent activity.|<code>^[\w\-\.]+:master-data\-\-[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ParentActivityID| |string|optional|Parent Activity ID|The relationship to a parent activity.|<code>^[\w\-\.]+:work-product-component\-\-[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_Parameters[]_|_[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)_|_object_|_required_|_AbstractActivityParameter_|_General parameter value used in one instance of activity.  Includes reference to data objects which are inputs and outputs of the activity. Fragment Description: General parameter value used in one instance of activity. [Without inheritance, combined specializations.]_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Parameters[].Title|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|required|Title|Name of the parameter, used to identify it in the activity. It must have an equivalent in the ActivityTemplate parameters.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].Index|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|integer|optional|Index|When parameter is an array, used to indicate the index in the array.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].Selection|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|Selection|Textual description about how this parameter was selected.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_Parameters[].Keys[]_|_[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)_|_object_|_optional_|_Parameter Key_|_A nested array describing keys used to identify a parameter value. When multiple values are provided for a given parameter, the key provides a way to identify the parameter through its association with an object, a time index or a parameter array member via ParameterKey value. Fragment Description: Abstract class describing a key used to identify a parameter value. When multiple values are provided for a given parameter, provides a way to identify the parameter through its association with an object, a time index, an integer...  [Without inheritance, combined specializations.] Note: floating point numbers are not supported as key values; the numbers have to be formatted as strings for robust equality operations, which are necessary for keys._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Parameters[].Keys[].ObjectParameterKey|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|Object Parameter Key|Relationship to an object ID, which acts as the parameter.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].Keys[].TimeIndexParameterKey|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|Time Index Parameter Key|The time index acting as parameter key value.|(No pattern)|time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].Keys[].ParameterKey|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|Internal Parameter Key|The key name, which establishes an association between parameters.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].Keys[].IntegerParameterKey|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|integer|optional|Integer Parameter Key|Integer value from "ParameterKey" parameter, associated with this parameter. Example: {"ParameterKey": "index", "StringParameterKey: 2}.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].Keys[].StringParameterKey|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|String Parameter Key|String value from "ParameterKey" parameter, associated with this parameter. Can be used to associate with parameter values of type string or data quantity. In the later case, the string representation of the quantity value will be used. Example: {"ParameterKey": "facies", "StringParameterKey: "shale"}, {"ParameterKey":"depth", "StringParameterKey":"1545.43m"}.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].DataObjectParameter|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|Data Object Parameter|Parameter referencing to a top level object.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].DataQuantityParameter|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|number|optional|Data Quantity Parameter|Parameter containing a double value.|(No pattern)|(No format)|UOM_via_property:DataQuantityParameterUOMID|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].IntegerQuantityParameter|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|integer|optional|Integer Quantity Parameter|Parameter containing an integer value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].StringParameter|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|String Parameter|Parameter containing a string value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].TimeIndexParameter|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|Time Index Parameter|Parameter containing a time index value.  It is assumed that all TimeIndexParameters within an Activity have the same date-time format, which is then described by the FrameOfReference mechanism.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].ParameterKindID &rarr; [ParameterKind](../reference-data/ParameterKind.1.0.0.md)|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|required|Parameter Kind Id|[Added to cover lack of inheritance]|<code>^[\w\-\.]+:reference-data\-\-ParameterKind:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ParameterKind](../reference-data/ParameterKind.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].ParameterRoleID &rarr; [ParameterRole](../reference-data/ParameterRole.1.0.0.md)|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|Parameter Role|Reference data describing how the parameter was used by the activity, such as input, output, control, constraint, agent, predecessor activity, successor activity.|<code>^[\w\-\.]+:reference-data\-\-ParameterRole:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ParameterRole](../reference-data/ParameterRole.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Parameters[].DataQuantityParameterUOMID &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|[AbstractActivityParameter.1.0.0](AbstractActivityParameter.1.0.0.md)|string|optional|Data Quantity Parameter UoM ID|Identifies unit of measure for floating point value.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|PriorActivityIDs[]| |string|optional|Prior Activity IDs|The activity or activities feeding results into this activity instance.|<code>^[\w\-\.]+:work-product-component\-\-[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_SoftwareSpecifications[]_| |_object_|_optional_|_Software_|_Software names and versions used. Fragment Description: The name and version of the software being executed in the context of this activity_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_(No example)_|
|SoftwareSpecifications[].SoftwareName| |string|optional|Software Name|The name of the software, application or plug-in used while performing this activity.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SoftwareSpecifications[].Version| |string|optional|Software Version|The version of the software, application or plug-in used while performing this activity.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_ActivityStates[]_|_[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)_|_object_|_optional_|_AbstractActivityState_|_The (non-overlapping) historical activity states and effective start and termination dates. The last state is replicated in the single LastActivityState for simpler queries. Fragment Description: One step/interval in an Activity's or ProjectActivity's state._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ActivityStates[].EffectiveDateTime|[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)|string|optional|Effective Date Time|The date and time at which the activity status becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ActivityStates[].TerminationDateTime|[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)|string|optional|Termination Date Time|The date and time at which the activity status is no longer in effect. For still effective activity states, the TerminationDateTime is left absent. For zero-duration intervals (events), the TerminationDateTime set to the same value as EffectiveDateTime.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ActivityStates[].ActivityStatusID &rarr; [ActivityStatus](../reference-data/ActivityStatus.1.0.0.md)|[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)|string|optional|Activity Status ID|The ActivityStatus is a set of major activity phases that are significant to business stakeholders.|<code>^[\w\-\.]+:reference-data\-\-ActivityStatus:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ActivityStatus](../reference-data/ActivityStatus.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ActivityStates[].Remark|[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)|string|optional|Remark|An optional remark associated with the ActivityStatus and time interval.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_LastActivityState_|_[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)_|_object_|_optional_|_AbstractActivityState_|_The current or last state this activity transitioned to. It is a copy of the last element in ActivityStates[]. If there is only one state recorded, the ActivityStates[] can stay empty. Fragment Description: One step/interval in an Activity's or ProjectActivity's state._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|LastActivityState.EffectiveDateTime|[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)|string|optional|Effective Date Time|The date and time at which the activity status becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|LastActivityState.TerminationDateTime|[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)|string|optional|Termination Date Time|The date and time at which the activity status is no longer in effect. For still effective activity states, the TerminationDateTime is left absent. For zero-duration intervals (events), the TerminationDateTime set to the same value as EffectiveDateTime.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|LastActivityState.ActivityStatusID &rarr; [ActivityStatus](../reference-data/ActivityStatus.1.0.0.md)|[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)|string|optional|Activity Status ID|The ActivityStatus is a set of major activity phases that are significant to business stakeholders.|<code>^[\w\-\.]+:reference-data\-\-ActivityStatus:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ActivityStatus](../reference-data/ActivityStatus.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|LastActivityState.Remark|[AbstractActivityState.1.0.0](AbstractActivityState.1.0.0.md)|string|optional|Remark|An optional remark associated with the ActivityStatus and time interval.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractWPCActivity.1.1.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [Activity.1.1.0](../work-product-component/Activity.1.1.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._