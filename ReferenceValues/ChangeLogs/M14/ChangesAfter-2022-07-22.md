<a name="TOC"></a>

[[_TOC_]]

# Reference Value Changes

## `osdu:wks:reference-data--SeismicProcessingStageType:1.0.1`

|Record `id` | Date | Change Note |
|---|---|---|
|Deconvolution|2022-07-22|New value|
|Demultiple|2022-07-22|New value|
|Demultiplex|2022-07-22|New value|
|Field|2022-07-22|revised definition and provided resource|
|Inversion|2022-07-22|revised definition and provided resource|
|Migration|2022-07-22|revised definition and provided resource|
|PhaseRotation|2022-07-22|revised definition and provided resource|
|Stack|2022-07-22|revised definition and provided resource|
|Statics|2022-07-22|New value|
|ModelingResult|2022-07-22|These are results of a interpretation activity rather a type, this value belongs on a seismic attribute type list in future|
|SyntheticModel|2022-07-22|These are results of a interpretation activity rather a type, this value belongs on a seismic attribute type list in future|

[Back to TOC](#TOC)