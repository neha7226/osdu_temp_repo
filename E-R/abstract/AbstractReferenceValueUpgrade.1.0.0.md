<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractReferenceValueUpgrade [Status: Accepted]
Schema for reference-value upgrade usages and value look-up tables.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractReferenceValueUpgrade:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractReferenceValueUpgrade.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractReferenceValueUpgrade.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractReferenceValueUpgrade.1.0.0.json](../../Authoring/abstract/AbstractReferenceValueUpgrade.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractReferenceValueUpgrade.1.0.0.json](../../Generated/abstract/AbstractReferenceValueUpgrade.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractReferenceValueUpgrade.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractReferenceValueUpgrade.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractReferenceValueUpgrade


## Outgoing Relationships for AbstractReferenceValueUpgrade

![AbstractReferenceValueUpgrade](../_diagrams/abstract/AbstractReferenceValueUpgrade.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractReferenceValueUpgrade Referenced by other Entities

![AbstractReferenceValueUpgrade](../_diagrams/abstract/AbstractReferenceValueUpgrade.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractReferenceValueUpgrade Properties

## Table of AbstractReferenceValueUpgrade Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_UsingKinds_| |_object_|_optional_|_Using Kinds_|_Dictionary {kind: using property name} listing comprehensive usages in OSDU schemas._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"osdu:wks:master-data--OperationsReport:1.0.0": [{"Path": "data.BitRecord[].TypeBit", "SearchModeID": "namespace:reference-data--DiscoverabilityBySearch:None:"}], "osdu:wks:master-data--OperationsReport:1.1.0": [{"Path": "data.BitRecord[].TypeBit", "SearchModeID": "namespace:reference-data--DiscoverabilityBySearch:None:"}]}_|
|UsingKinds| |object|optional|Using Kinds|Dictionary {kind: using property name} listing comprehensive usages in OSDU schemas.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"osdu:wks:master-data--OperationsReport:1.0.0": [{"Path": "data.BitRecord[].TypeBit", "SearchModeID": "namespace:reference-data--DiscoverabilityBySearch:None:"}], "osdu:wks:master-data--OperationsReport:1.1.0": [{"Path": "data.BitRecord[].TypeBit", "SearchModeID": "namespace:reference-data--DiscoverabilityBySearch:None:"}]}|
|_LookUp_| |_object_|_optional_|_Reference Value Translation Dictionary_|_The translation dictionary key/value pairs: old reference-data value = key, superseded reference value = value._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"namespace:reference-data--BitType:diamond%20core:": {"SupersededBy": "namespace:reference-data--BitType:Core:", "MigrateToKind": "osdu:wks:reference-data--BitType:1"}, "namespace:reference-data--BitType:insert%20roller%20cone:": {"SupersededBy": "namespace:reference-data--BitType:RollerCone:"}, "namespace:reference-data--BitType:roller%20cone:": {"SupersededBy": "namespace:reference-data--BitType:RollerCone:"}, "namespace:reference-data--BitType:unknown:": {"SupersededBy": "namespace:reference-data--BitType:Unknown:"}}_|
|LookUp| |object|optional|Reference Value Translation Dictionary|The translation dictionary key/value pairs: old reference-data value = key, superseded reference value = value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"namespace:reference-data--BitType:diamond%20core:": {"SupersededBy": "namespace:reference-data--BitType:Core:", "MigrateToKind": "osdu:wks:reference-data--BitType:1"}, "namespace:reference-data--BitType:insert%20roller%20cone:": {"SupersededBy": "namespace:reference-data--BitType:RollerCone:"}, "namespace:reference-data--BitType:roller%20cone:": {"SupersededBy": "namespace:reference-data--BitType:RollerCone:"}, "namespace:reference-data--BitType:unknown:": {"SupersededBy": "namespace:reference-data--BitType:Unknown:"}}|
|IsComplexUpgrade| |boolean|optional|Is Complex Upgrade|Indicator whether the LookUp mappings are complex, i.e., require moving values to different properties with references to different reference-data types. If false or absent, the LookUp table refers only to a single reference-data type list, i.e., the values are simply swapped out in-place.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Yes|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractReferenceValueUpgrade.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [ReferenceValueUpgradeLookUp.1.0.0](../reference-data/ReferenceValueUpgradeLookUp.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._