<a name="TOC"></a>**Table of Contents**

[[_TOC_]]

Changes since: 2022-05-20

* [Detailed Change Log](ChangesAfter-2022-05-20.md) for reference values.

# BitType

PPDM introduces a set of clean definitions and Code styles. The previous OSDU values, which used lower case words and
blanks as Code, were deprecated and superseding values indicated.

* No action is required if an operator wants to carry on with the previously used OSDU codes.
* If operators want to migrate to the PPDM reference values a migration is required, which is presented in the following
  table (alphabetically ordered by deprecated code).

| Deprecated `id`                                             | Superseded by ìd`                               | Superseded Name | Description                                                                                                                                                      |
|-------------------------------------------------------------|-------------------------------------------------|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--BitType:PDC%20core             | Not endorsed by PPDM                            | -               | polycrystalline diamond compact fixed cutter bit                                                                                                                 |
| partition-id:reference-data--BitType:diamond%20core         | partition-id:reference-data--BitType:Core       | Core            | DEPRECATED: Please use Code="Core" with Name="Core" instead. Diamond core bit                                                                                    |
| partition-id:reference-data--BitType:diamond                | Not endorsed by PPDM                            | -               | Diamond bit                                                                                                                                                      |
| partition-id:reference-data--BitType:insert%20roller%20cone | partition-id:reference-data--BitType:RollerCone | Roller cone     | DEPRECATED: Please use Code="RollerCone" with Name="Roller cone" instead. Insert roller cone bit                                                                 |
| partition-id:reference-data--BitType:roller%20cone          | partition-id:reference-data--BitType:RollerCone | Roller cone     | DEPRECATED: Please use Code="RollerCone" with Name="Roller cone" instead. The bit has conical teeth, usually on 3 rollers (tricone.) Bicone is much less common. |
| partition-id:reference-data--BitType:unknown                | partition-id:reference-data--BitType:Unknown    | Unknown         | DEPRECATED: Please use Code="Unknown" with Name="Unknown" instead. The type of bit is unreported or unknown.                                                     |


# OperatingEnvironment:1.0.0

The previous values contained a number of alias names 'merged' into one string, comma separated. This was not the
intention; instead the list is not `|` separated and parsed into individual `data.NameAlias[]` member objects. This
changes the following `OperatingEnvironment` values:

* `partition-id:reference-data--OperatingEnvironment:Onshore.FlowingWater` 
* `partition-id:reference-data--OperatingEnvironment:Onshore.Forest` 
* `partition-id:reference-data--OperatingEnvironment:Onshore.Grassland` 
* `partition-id:reference-data--OperatingEnvironment:Onshore.Marsh` 
* `partition-id:reference-data--OperatingEnvironment:Midshore.Transition`

[Back to TOC](#TOC)

---

# OAuth2FlowType (new)

A new reference value catalog for connected data sources with the definition:

> Flows, or grants, supported within the OAuth 2.0 authorization framework.
 
The governance mode is OPEN. The current values shipped by OSDU are:

| Code                | Description                                                                                                                                                                                                                                                                                                                                                       |
|---------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| AuthorizationCode   | The Authorization Code grant type is used by confidential and public clients to exchange an authorization code for an access token.                                                                                                                                                                                                                               |
| PKCE                | PKCE (RFC 7636) is an extension to the Authorization Code flow to prevent several attacks and to be able to securely perform the OAuth exchange from public clients.                                                                                                                                                                                              |
| ClientCredentials   | The Client Credentials grant type is used by clients to obtain an access token outside of the context of a user.  This is typically used by clients to access resources about themselves rather than to access a user's resources.                                                                                                                                |
| PasswordCredentials | The Password grant type is a way to exchange a user's credentials for an access token. Because the client application has to collect the user's password and send it to the authorization server, it is not recommended that this grant be used at all anymore. NOTE: The latest OAuth 2.0 Security Best Current Practice disallows the password grant entirely.  |
| RefreshToken        | The Refresh Token grant type is used by clients to exchange a refresh token for an access token when the access token has expired.  This allows clients to continue to have a valid access token without further interaction with the user.                                                                                                                       |


[Back to TOC](#TOC)

---

# OSDUJsonExtensions

Already in M12 a new tag `x-osdu-side-car-type-to` was introduced. This has been added now with the description:

| Code                      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|---------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `x-osdu-side-car-type-to` | A JSON extension tag to declare a schema as a 'side-car' extension to a main schema ('motorbike'). The value of the tag is a JSON object similar to 'x-osdu-relationship', i.e.,  consisting of a relationship  target definition and a root 'data' property name holding the relationship value. Example (from WellPlanningWellbore):<br>"x-osdu-side-car-type-to": [{<br>&nbsp;&nbsp;&nbsp;"GroupType": "master-data",<br>&nbsp;&nbsp;&nbsp;"EntityType": "Wellbore",<br>&nbsp;&nbsp;&nbsp;"SideCarToPropertyName": "WellboreID"<br>}]<br>Scope: Schema |

---

# SchemaFormatType

New reference values for SEG-Y and SEG-D standard file formats. 
* Authority: SEG
* Publication: https://library.seg.org/seg-technical-standards
* Revision 1975-  

| Name  | Description                                                                                                                                                                                                                                                                                                                          | Code  |  
|-------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------|
| SEG-Y | The SEG-Y file format is one of several standards developed by the Society of Exploration Geophysicists (SEG) for storing geophysical data. It is an open standard, and is controlled by the SEG Technical Standards Committee, a non-profit organization. The value SEG-Y does not imply a specific revision of the SEG-Y standard. | SEG-Y |
| SEG-D | A digital field tape format developed by the Society of Exploration Geophysicists (SEG). It is an open standard, and is controlled by the SEG Technical Standards Committee, a non-profit organization. The value SEG-D does not imply a specific revision of the SEG-D standard.                                                    | SEG-D |



[Back to TOC](#TOC)

---

# SecuritySchemeType (new)

A new reference value catalog for connected data sources with the definition:

> A type of security scheme, such as OAuth2, used to generate access credentials to a resource.
 
The governance mode is OPEN. The current values shipped by OSDU are:

| Code   | Description                                                                          |
|--------|--------------------------------------------------------------------------------------|
| OAuth2 | Requests are authorized using open, industry-standard OAuth 2.0 authorization flows. |
| Bearer | Requests are authorized using an access key, such as a JSON Web Token (JWT).         |


[Back to TOC](#TOC)

---

# SeismicMigrationType

PPDM introduces a set of clean definitions and Code styles. The previous OSDU values, which used abbreviations as Code, 
were deprecated and superseding values indicated.

* No action is required if an operator wants to carry on with the previously used OSDU codes.
* If operators want to migrate to the PPDM reference values a migration is required, which is presented in the following
  table (alphabetically ordered by deprecated code).

| Deprecated `id`                                                  | Superseded by ìd`                                                         | Superseded Name             | Description                                                                                                                                                                                               |
|------------------------------------------------------------------|---------------------------------------------------------------------------|-----------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--SeismicMigrationType:DepthMig-Kirch | partition-id:reference-data--SeismicMigrationType:PoststackDepthKirchhoff | Poststack depth - Kirchhoff | DEPRECATED: Please use Code="PoststackDepthKirchhoff" with Name="Poststack depth - Kirchhoff" instead. Imaging with lateral velocity gradients applied to stacked traces using Kirchhoff algorithm        |
| partition-id:reference-data--SeismicMigrationType:DepthMig       | partition-id:reference-data--SeismicMigrationType:PoststackDepth          | Poststack depth             | DEPRECATED: Please use Code="PoststackDepth" with Name="Poststack depth" instead. Imaging with lateral velocity gradients applied to stacked traces                                                       |
| partition-id:reference-data--SeismicMigrationType:PSDM-Kirch     | partition-id:reference-data--SeismicMigrationType:PrestackDepthKirchhoff  | Prestack depth - Kirchhoff  | DEPRECATED: Please use Code="PrestackDepthKirchhoff" with Name="Prestack depth - Kirchhoff" instead. Imaging with lateral velocity gradients applied to gathers  using Kirchhoff algorithm                |
| partition-id:reference-data--SeismicMigrationType:PSDM           | partition-id:reference-data--SeismicMigrationType:PrestackDepth           | Prestack depth              | DEPRECATED: Please use Code="PrestackDepth" with Name="Prestack depth" instead. Imaging with lateral velocity gradients applied to gathers                                                                |
| partition-id:reference-data--SeismicMigrationType:PSTM-Kirch     | partition-id:reference-data--SeismicMigrationType:PrestackTimeKirchhoff   | Prestack time - Kirchhoff   | DEPRECATED: Please use Code="PrestackTimeKirchhoff" with Name="Prestack time - Kirchhoff" instead. Imaging with approximately flat earth assumption applied to gathers using Kirchhoff algorithm          |
| partition-id:reference-data--SeismicMigrationType:PSTM           | partition-id:reference-data--SeismicMigrationType:PrestackTime            | Prestack time               | DEPRECATED: Please use Code="PrestackTime" with Name="Prestack time" instead. Imaging with approximately flat earth assumption applied to gathers                                                         |
| partition-id:reference-data--SeismicMigrationType:TimeMig-Kirch  | partition-id:reference-data--SeismicMigrationType:PoststackTimeKirchhoff  | Poststack time - Kirchhoff  | DEPRECATED: Please use Code="PoststackTimeKirchhoff" with Name="Poststack time - Kirchhoff" instead. Imaging with approximately flat earth assumption applied to stacked traces using Kirchhoff algorithm |
| partition-id:reference-data--SeismicMigrationType:TimeMig        | partition-id:reference-data--SeismicMigrationType:PoststackTime           | Poststack time              | DEPRECATED: Please use Code="PoststackTime" with Name="Poststack time" instead. Imaging with approximately flat earth assumption applied to stacked traces                                                |

[Back to TOC](#TOC)

---

# SeismicPickingType

PPDM introduces a set of clean definitions and Code styles. Some OSDU values were deprecated and superseding values
indicated.

* No action is required if an operator wants to carry on with the previously used OSDU codes.
* If operators want to migrate to the PPDM reference values a migration is required, which is presented in the following
  table (alphabetically ordered by deprecated code).

| Deprecated `id`                                                  | Superseded by ìd`                                         | Superseded Name | Description                                                                                                                |
|------------------------------------------------------------------|-----------------------------------------------------------|-----------------|----------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--SeismicPickingType:Autotracked      | partition-id:reference-data--SeismicPickingType:Automatic | Automatic       | DEPRECATED: Please use Code="Automatic" with Name="Automatic" instead. Seismic picking performed using autotracking method |
| partition-id:reference-data--SeismicPickingType:InterpolatedSeed | partition-id:reference-data--SeismicPickingType:Mixed     | Mixed           | DEPRECATED: Please use Code="Mixed" with Name="Mixed" instead. Interpolated Seed used as Seismic picking method            |
| partition-id:reference-data--SeismicPickingType:PrimarySeed      | partition-id:reference-data--SeismicPickingType:Automatic | Automatic       | DEPRECATED: Please use Code="Automatic" with Name="Automatic" instead. Primary Seed used as Seismic picking method         |

[Back to TOC](#TOC)

---

# SeismicWaveType

PPDM introduces a set of clean definitions and Code styles. The previous OSDU values, which used abbreviations as Code, 
were deprecated and superseding values indicated.

* No action is required if an operator wants to carry on with the previously used OSDU codes.
* If operators want to migrate to the PPDM reference values a migration is required, which is presented in the following
  table (alphabetically ordered by deprecated code).

| Deprecated `id`                                  | Superseded by ìd`                                                        | Superseded Name                | Description                                                                                                                                                                          |
|--------------------------------------------------|--------------------------------------------------------------------------|--------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--SeismicWaveType:PSh | partition-id:reference-data--SeismicWaveType:PressureHorizontalShearWave | Pressure/horizontal shear wave | DEPRECATED: Please use Code="PressureHorizontalShearWave" with Name="Pressure/horizontal shear wave" instead. Indicates that the seismic waves correspond to Horizontal Shear waves  |
| partition-id:reference-data--SeismicWaveType:PSv | partition-id:reference-data--SeismicWaveType:PressureVerticalShearWave   | Pressure/vertical shear wave   | DEPRECATED: Please use Code="PressureVertical ShearWave" with Name="Pressure/vertical shear wave" instead. Indicates that the seismic waves correspond to Vertical Shear waves       |
| partition-id:reference-data--SeismicWaveType:P   | partition-id:reference-data--SeismicWaveType:PressureWave                | Pressure wave                  | DEPRECATED: Please use Code="PressureWave" with Name="Pressure wave" instead. Indicates that the seismic waves correspond to a mix of Pressure and Vertical Shear waves              |
| partition-id:reference-data--SeismicWaveType:Sh  | partition-id:reference-data--SeismicWaveType:HorizontalShearWave         | Horizontal shear wave          | DEPRECATED: Please use Code="HorizontalShearWave" with Name="Horizontal shear wave" instead. Indicates that the seismic waves correspond to Pressure or Primary waves                |
| partition-id:reference-data--SeismicWaveType:Sv  | partition-id:reference-data--SeismicWaveType:VerticalShearWave           | Vertical shear wave            | DEPRECATED: Please use Code="VerticalShearWave" with Name="Vertical shear wave" instead. Indicates that the seismic waves correspond to a mix of Pressure and Horizontal Shear waves |

[Back to TOC](#TOC)

---

# VerticalMeasurementType:1.0.1

OSDU adopted the PPDM description for `VerticalMeasurementType`; the description (decoration) change required a patch
version increment for the schema (1.0.**1**), the schema properties are not changed.

PPDM introduces a set of clean definitions and Code styles. The previous OSDU values, which used abbreviations as Code
were deprecated and superseding values indicated.

| Deprecated `id`                                          | Superseded by ìd`                                                           | Superseded Name          | Description                                                                                                                                                                                                                                                                                                                                          |
|----------------------------------------------------------|-----------------------------------------------------------------------------|--------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--VerticalMeasurementType:AP  | partition-id:reference-data--VerticalMeasurementType:ArbitraryPoint         | Arbitrary point          | DEPRECATED: Please use Code="ArbitraryPoint" with Name="Arbitrary point" instead. Any point in or above the well, designated for a specific purpose.                                                                                                                                                                                                 |
| partition-id:reference-data--VerticalMeasurementType:BH  | Not endorsed by PPDM                                                        | -                        | The function of the installed braden head on a well serving as origin for position measurements along a wellbore.                                                                                                                                                                                                                                    |
| partition-id:reference-data--VerticalMeasurementType:BR  | Not endorsed by PPDM                                                        | -                        | Point in wellbore where the wellbore meets the rig bracing.                                                                                                                                                                                                                                                                                          |
| partition-id:reference-data--VerticalMeasurementType:CB  | Not endorsed by PPDM                                                        | -                        | Center line braden head outlet.                                                                                                                                                                                                                                                                                                                      |
| partition-id:reference-data--VerticalMeasurementType:CF  | partition-id:reference-data--VerticalMeasurementType:CasingHeadFlange       | Casing head flange       | DEPRECATED: Please use Code="CasingHeadFlange" with Name="Casing head flange" instead. Position along wellbore corresponding to well head's casing flange.                                                                                                                                                                                           |
| partition-id:reference-data--VerticalMeasurementType:CH  | partition-id:reference-data--VerticalMeasurementType:CasingHeadFlange       | Casing head flange       | DEPRECATED: Please use Code="CasingHeadFlange" with Name="Casing head flange" instead. The function of the installed casing head on a well serving as origin for position measurements along a wellbore.                                                                                                                                             |
| partition-id:reference-data--VerticalMeasurementType:CO  | partition-id:reference-data--VerticalMeasurementType:Conductor              | Conductor                | DEPRECATED: Please use Code="Conductor" with Name="Conductor" instead. The top of first string of casing to hold back unconsolidated ground and protect the groundwater; it is usually installed in an excavated cellar.                                                                                                                             |
| partition-id:reference-data--VerticalMeasurementType:CV  | partition-id:reference-data--VerticalMeasurementType:SwabValve              | Swab valve               | DEPRECATED: Please use Code="SwabValve" with Name="Swab valve" instead. Also known as a swab valve. This is the uppermost valve on the vertical bore of the christmas tree above the flowing outlet, and is sometimes used as a reference point for wellbore measurements.                                                                           |
| partition-id:reference-data--VerticalMeasurementType:DF  | partition-id:reference-data--VerticalMeasurementType:DrillFloor             | Drill floor              | DEPRECATED: Please use Code="DrillFloor" with Name="Drill floor" instead. The chief work area of a drilling rig where the crew handles the drill string.                                                                                                                                                                                             |
| partition-id:reference-data--VerticalMeasurementType:EST | Not endorsed by PPDM                                                        | -                        | A wellbore with a depth reference point of type EST tells that there exists a common depth reference mark/point on the casing hardware of the well, to which measured depth values in the wellbore can be zero-ed.  The  height of that depth reference mark, above the regional datum,  is however not exactly known, but a good estimate is given. |
| partition-id:reference-data--VerticalMeasurementType:ES  | partition-id:reference-data--VerticalMeasurementType:EllipsoidSurface       | Ellipsoid surface        | DEPRECATED: Please use Code="EllipsoidSurface" with Name="Ellipsoid surface" instead. Position along wellbore corresponding to geodetic vertical origin.                                                                                                                                                                                             |
| partition-id:reference-data--VerticalMeasurementType:FCS | partition-id:reference-data--VerticalMeasurementType:FluidContactSurface    | Fluid contact surface    | DEPRECATED: Please use Code="FluidContactSurface" with Name="Fluid contact surface" instead. The interface between two reservoir fluids: gas/oil, gas/water, oil/water.                                                                                                                                                                              |
| partition-id:reference-data--VerticalMeasurementType:GM  | Not endorsed by PPDM                                                        | -                        | The function of the installed ground mat for the drilling rig serving as origin for position measurements along a wellbore.                                                                                                                                                                                                                          |
| partition-id:reference-data--VerticalMeasurementType:GR  | partition-id:reference-data--VerticalMeasurementType:GroundLevel            | Ground level             | DEPRECATED: Please use Code="GroundLevel" with Name="Ground level" instead. The elevation of the land surface at a well site.                                                                                                                                                                                                                        |
| partition-id:reference-data--VerticalMeasurementType:KB  | partition-id:reference-data--VerticalMeasurementType:KellyBushing           | Kelly bushing            | DEPRECATED: Please use Code="KellyBushing" with Name="Kelly bushing" instead. A device on top of the rotary table, used to transmit torque to the kelly (a special section of the drill pipe.)                                                                                                                                                       |
| partition-id:reference-data--VerticalMeasurementType:KOP | partition-id:reference-data--VerticalMeasurementType:KickOffPoint           | Kick off point           | DEPRECATED: Please use Code="KickOffPoint" with Name="Kick off point" instead. A point in a wellbore where the path deviates sharply within the wellbore or as the start of a sidetrack or lateral wellbore.                                                                                                                                         |
| partition-id:reference-data--VerticalMeasurementType:LAT | partition-id:reference-data--VerticalMeasurementType:LowestAstronomicalTide | Lowest astronomical tide | DEPRECATED: Please use Code="LowestAstronomicalTide" with Name="Lowest astronomical tide" instead. The lowest tide level for average meteorological and astronomical conditions, used as a datum for elevations and marine charts.                                                                                                                   |
| partition-id:reference-data--VerticalMeasurementType:MLS | Not endorsed by PPDM                                                        | -                        | Position along wellbore corresponding to seabed.                                                                                                                                                                                                                                                                                                     |
| partition-id:reference-data--VerticalMeasurementType:MSL | partition-id:reference-data--VerticalMeasurementType:MeanSeaLevel           | Mean sea level           | DEPRECATED: Please use Code="MeanSeaLevel" with Name="Mean sea level" instead. An average level of the ocean(s), used as a datum for elevations, marine charts and/or atmospheric pressure.                                                                                                                                                          |
| partition-id:reference-data--VerticalMeasurementType:MT  | Not endorsed by PPDM                                                        | -                        | The point where the wellbore meets the drilling floor mat.                                                                                                                                                                                                                                                                                           |
| partition-id:reference-data--VerticalMeasurementType:PBD | partition-id:reference-data--VerticalMeasurementType:PlugBackDepth          | Plug back depth          | DEPRECATED: Please use Code="PlugBackDepth" with Name="Plug back depth" instead. The depth to the top of the permanent plug at the deepest accessible point in a wellbore.                                                                                                                                                                           |
| partition-id:reference-data--VerticalMeasurementType:PT  | partition-id:reference-data--VerticalMeasurementType:CasingHeadFlange       | Casing head flange       | DEPRECATED: Please use Code="CasingHeadFlange" with Name="Casing head flange" instead. The top of the wellbore pipe.                                                                                                                                                                                                                                 |
| partition-id:reference-data--VerticalMeasurementType:RB  | partition-id:reference-data--VerticalMeasurementType:KellyBushing           | Kelly bushing            | DEPRECATED: Please use Code="KellyBushing" with Name="Kelly bushing" instead. The point where the wellbore meets the rig bracing.                                                                                                                                                                                                                    |
| partition-id:reference-data--VerticalMeasurementType:RF  | partition-id:reference-data--VerticalMeasurementType:DrillFloor             | Drill floor              | DEPRECATED: Please use Code="DrillFloor" with Name="Drill floor" instead. The point where the wellbore meets the rig floor.                                                                                                                                                                                                                          |
| partition-id:reference-data--VerticalMeasurementType:RT  | partition-id:reference-data--VerticalMeasurementType:RotaryTable            | Rotary table             | DEPRECATED: Please use Code="RotaryTable" with Name="Rotary table" instead. A device in the drilling rig floor that applies rotational force to the drill string.                                                                                                                                                                                    |
| partition-id:reference-data--VerticalMeasurementType:SF  | partition-id:reference-data--VerticalMeasurementType:Seafloor               | Seafloor                 | DEPRECATED: Please use Code="Seafloor" with Name="Seafloor" instead. The seabed, usually a reference point for water depth.                                                                                                                                                                                                                          |
| partition-id:reference-data--VerticalMeasurementType:SRD | partition-id:reference-data--VerticalMeasurementType:SeismicReferenceDatum  | Seismic reference datum  | DEPRECATED: Please use Code="SeismicReferenceDatum" with Name="Seismic reference datum" instead. A plane that is above the highest elevation in a seismic survey and is assigned a time of zero milliseconds. In marine surveys, the SRD is usually equal to mean sea level.                                                                         |
| partition-id:reference-data--VerticalMeasurementType:TBF | partition-id:reference-data--VerticalMeasurementType:TopBottomFlange        | Top bottom flange        | DEPRECATED: Please use Code="TopBottomFlange" with Name="Top bottom flange" instead. The upper surface of the base flange.                                                                                                                                                                                                                           |
| partition-id:reference-data--VerticalMeasurementType:TC  | partition-id:reference-data--VerticalMeasurementType:TopCellar              | Top cellar               | DEPRECATED: Please use Code="TopCellar" with Name="Top cellar" instead. A walled section below ground to shelter the entry to the well the annular access valves.                                                                                                                                                                                    |
| partition-id:reference-data--VerticalMeasurementType:TD  | partition-id:reference-data--VerticalMeasurementType:TotalDepth             | Total depth              | DEPRECATED: Please use Code="TotalDepth" with Name="Total depth" instead. The depth of farthest penetration of the drill bit in a wellbore.                                                                                                                                                                                                          |
| partition-id:reference-data--VerticalMeasurementType:TH  | partition-id:reference-data--VerticalMeasurementType:TubingHeadFlange       | Tubing head flange       | DEPRECATED: Please use Code="TubingHeadFlange" with Name="Tubing head flange" instead. Position along wellbore corresponding to well head's tubing flange.                                                                                                                                                                                           |
| partition-id:reference-data--VerticalMeasurementType:TIP | partition-id:reference-data--VerticalMeasurementType:TieInPoint             | Tie in point             | DEPRECATED: Please use Code="TieInPoint" with Name="Tie in point" instead. A point in an existing wellbore where another wellbore path connects.                                                                                                                                                                                                     |

[Back to TOC](#TOC)

---

[Back to Milestone Overview](../README.md)