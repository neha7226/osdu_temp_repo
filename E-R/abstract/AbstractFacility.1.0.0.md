<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractFacility [Status: Accepted]
The schema fragment included by facilities. A facility is a grouping of equipment that is located within a specific geographic boundary or site and that is used in the context of energy-related activities such as exploration, extraction, generation, storage, processing, disposal, supply, or transfer. Clarifications: (1) A facility may be surface or subsurface located. (2) Usually facility equipment is commonly owned or operated. (3) Industry definitions may vary and differ from this one. This schema fragment is included by Well, Wellbore, Rig, as well as Tank Batteries, Compression Stations, Storage Facilities, Wind Farms, Wind Turbines, Mining Facilities, etc., once these types are included in to the OSDU.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractFacility:1.0.0`
* Schema status: PUBLISHED
* **Superseded by** &rarr; [`osdu:wks:AbstractFacility:1.1.0`](AbstractFacility.1.1.0.md)
* **Migration Guide (M10)** [`osdu:wks:AbstractFacility:1.0.0`](../../Guides/MigrationGuides/M10/AbstractFacility.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractFacility.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractFacility.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractFacility.1.0.0.json](../../Authoring/abstract/AbstractFacility.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractFacility.1.0.0.json](../../Generated/abstract/AbstractFacility.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractFacility.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractFacility.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractFacility


## Outgoing Relationships for AbstractFacility

![AbstractFacility](../_diagrams/abstract/AbstractFacility.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractFacility Referenced by other Entities

![AbstractFacility](../_diagrams/abstract/AbstractFacility.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacility Properties

## Table of AbstractFacility Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|FacilityID| |string|optional|External Facility Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityTypeID &rarr; [FacilityType](../reference-data/FacilityType.1.0.0.md)| |string|optional|Facility Type Id|The definition of a kind of capability to perform a business function or a service.|<code>^[\w\-\.]+:reference-data\-\-FacilityType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacilityType](../reference-data/FacilityType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_FacilityOperators[]_|_[AbstractFacilityOperator.1.0.0](AbstractFacilityOperator.1.0.0.md)_|_object_|_optional_|_AbstractFacilityOperator_|_The history of operator organizations of the facility. Fragment Description: The organisation that was responsible for a facility at some point in time._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|FacilityOperators[].FacilityOperatorID|[AbstractFacilityOperator.1.0.0](AbstractFacilityOperator.1.0.0.md)|string|optional|Facility Operator ID|Internal, unique identifier for an item 'AbstractFacilityOperator'. This identifier is used by 'AbstractFacility.CurrentOperatorID' and 'AbstractFacility.InitialOperatorID'.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityOperators[].FacilityOperatorOrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractFacilityOperator.1.0.0](AbstractFacilityOperator.1.0.0.md)|string|optional|Facility Operator Organisation Id|The company that currently operates, or previously operated the facility|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityOperators[].EffectiveDateTime|[AbstractFacilityOperator.1.0.0](AbstractFacilityOperator.1.0.0.md)|string|optional|Effective Date Time|The date and time at which the facility operator becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityOperators[].TerminationDateTime|[AbstractFacilityOperator.1.0.0](AbstractFacilityOperator.1.0.0.md)|string|optional|Termination Date Time|The date and time at which the facility operator is no longer in effect. If the operator is still effective, the 'TerminationDateTime' is left absent.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|InitialOperatorID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Initial Operator ID|A initial operator organization ID; the organization ID may also be found in the FacilityOperatorOrganisationID of the FacilityOperator array providing the actual dates.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|CurrentOperatorID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Current Operator ID|The current operator organization ID; the organization ID may also be found in the FacilityOperatorOrganisationID of the FacilityOperator array providing the actual dates.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|DataSourceOrganisationID &rarr; [Organisation](../master-data/Organisation.1.1.0.md)| |string|optional|Data Source Organisation Id|The main source of the header information.|<code>^[\w\-\.]+:master-data\-\-Organisation:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|OperatingEnvironmentID &rarr; [OperatingEnvironment](../reference-data/OperatingEnvironment.1.0.0.md)| |string|optional|Operating Environment Id|Identifies the Facility's general location as being onshore vs. offshore.|<code>^[\w\-\.]+:reference-data\-\-OperatingEnvironment:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [OperatingEnvironment](../reference-data/OperatingEnvironment.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityName| |string|optional|Facility Name|Name of the Facility.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|~~_FacilityNameAliases[]_~~|~~_[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)_~~|~~_object_~~|~~_optional_~~|~~_AbstractAliasNames_~~|~~_DEPRECATED: please use data.NameAliases. Alternative names, including historical, by which this facility is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._~~|~~_(No pattern)_~~|~~_(No format)_~~|~~_(No frame of reference)_~~|~~_object (default)_~~|~~_(Not derived)_~~|~~_(No natural key)_~~|~~_(No example)_~~|
|~~FacilityNameAliases[].AliasName~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Alias Name~~|~~Alternative Name value of defined name type for an object.~~|~~(No pattern)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|~~FacilityNameAliases[].AliasNameTypeID &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Alias Name Type Id~~|~~A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.~~|~~<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](../reference-data/AliasNameType.1.0.0.md)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|~~FacilityNameAliases[].DefinitionOrganisationID &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Definition Organisation Id~~|~~The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).~~|~~<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](../reference-data/StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|~~FacilityNameAliases[].EffectiveDateTime~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Effective Date Time~~|~~The date and time when an alias name becomes effective.~~|~~(No pattern)~~|~~date-time~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|~~FacilityNameAliases[].TerminationDateTime~~|~~[AbstractAliasNames.1.0.0](AbstractAliasNames.1.0.0.md)~~|~~string~~|~~optional~~|~~Termination Date Time~~|~~The data and time when an alias name is no longer in effect.~~|~~(No pattern)~~|~~date-time~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|
|_FacilityStates[]_|_[AbstractFacilityState.1.0.0](AbstractFacilityState.1.0.0.md)_|_object_|_optional_|_AbstractFacilityState_|_The history of life cycle states the facility has been through. Fragment Description: The life cycle status of a facility at some point in time._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|FacilityStates[].EffectiveDateTime|[AbstractFacilityState.1.0.0](AbstractFacilityState.1.0.0.md)|string|optional|Effective Date Time|The date and time at which the facility state becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityStates[].TerminationDateTime|[AbstractFacilityState.1.0.0](AbstractFacilityState.1.0.0.md)|string|optional|Termination Date Time|The date and time at which the facility state is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityStates[].FacilityStateTypeID &rarr; [FacilityStateType](../reference-data/FacilityStateType.1.0.0.md)|[AbstractFacilityState.1.0.0](AbstractFacilityState.1.0.0.md)|string|optional|Facility State Type Id|Life Cycle [Facility State Type] is a set of major phases that are significant to regulators and/or business stakeholders. Life Cycle may apply to a well or its components [or other facility].|<code>^[\w\-\.]+:reference-data\-\-FacilityStateType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacilityStateType](../reference-data/FacilityStateType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_FacilityEvents[]_|_[AbstractFacilityEvent.1.0.0](AbstractFacilityEvent.1.0.0.md)_|_object_|_optional_|_AbstractFacilityEvent_|_A list of key facility events. Fragment Description: A significant occurrence in the life of a facility, which often changes its state, or the state of one of its components. It can describe a point-in-time (event) or a time interval of a specific type (FacilityEventType)._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|FacilityEvents[].FacilityEventTypeID &rarr; [FacilityEventType](../reference-data/FacilityEventType.1.0.0.md)|[AbstractFacilityEvent.1.0.0](AbstractFacilityEvent.1.0.0.md)|string|optional|Facility Event Type Id|The facility event type is a picklist. Examples: 'Permit', 'Spud', 'Abandon', etc.|<code>^[\w\-\.]+:reference-data\-\-FacilityEventType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacilityEventType](../reference-data/FacilityEventType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityEvents[].EffectiveDateTime|[AbstractFacilityEvent.1.0.0](AbstractFacilityEvent.1.0.0.md)|string|optional|Effective Date Time|The date and time at which the event took place or takes effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilityEvents[].TerminationDateTime|[AbstractFacilityEvent.1.0.0](AbstractFacilityEvent.1.0.0.md)|string|optional|Termination Date Time|The date and time at which the event is no longer in effect. For point-in-time events the 'TerminationDateTime' must be set equal to 'EffectiveDateTime'. Open time intervals have an absent 'TerminationDateTime'.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_FacilitySpecifications[]_|_[AbstractFacilitySpecification.1.0.0](AbstractFacilitySpecification.1.0.0.md)_|_object_|_optional_|_AbstractFacilitySpecification_|_facilitySpecification maintains the specification like slot name, wellbore drilling permit number, rig name etc. Fragment Description: A property, characteristic, or attribute about a facility that is not described explicitly elsewhere._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_flattened_|_(Not derived)_|_(No natural key)_|_(No example)_|
|FacilitySpecifications[].EffectiveDateTime|[AbstractFacilitySpecification.1.0.0](AbstractFacilitySpecification.1.0.0.md)|string|optional|Effective Date Time|The date and time at which the facility specification instance becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecifications[].TerminationDateTime|[AbstractFacilitySpecification.1.0.0](AbstractFacilitySpecification.1.0.0.md)|string|optional|Termination Date Time|The date and time at which the facility specification instance is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecifications[].FacilitySpecificationQuantity|[AbstractFacilitySpecification.1.0.0](AbstractFacilitySpecification.1.0.0.md)|number|optional|Facility Specification Quantity|The value for the specified parameter type.|(No pattern)|(No format)|UOM_via_property:UnitOfMeasureID|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecifications[].FacilitySpecificationDateTime|[AbstractFacilitySpecification.1.0.0](AbstractFacilitySpecification.1.0.0.md)|string|optional|Facility Specification Date Time|The actual date and time value of the parameter.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecifications[].FacilitySpecificationIndicator|[AbstractFacilitySpecification.1.0.0](AbstractFacilitySpecification.1.0.0.md)|boolean|optional|Facility Specification Indicator|The actual indicator value of the parameter.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecifications[].FacilitySpecificationText|[AbstractFacilitySpecification.1.0.0](AbstractFacilitySpecification.1.0.0.md)|string|optional|Facility Specification Text|The actual text value of the parameter.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecifications[].UnitOfMeasureID &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|[AbstractFacilitySpecification.1.0.0](AbstractFacilitySpecification.1.0.0.md)|string|optional|Unit Of Measure Id|The unit for the quantity parameter, like metre (m in SI units system) for quantity Length.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecifications[].ParameterTypeID &rarr; [ParameterType](../reference-data/ParameterType.1.0.0.md)|[AbstractFacilitySpecification.1.0.0](AbstractFacilitySpecification.1.0.0.md)|string|optional|Parameter Type Id|Parameter type of property or characteristic.|<code>^[\w\-\.]+:reference-data\-\-ParameterType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ParameterType](../reference-data/ParameterType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacility.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [IsolatedInterval.1.0.0](../master-data/IsolatedInterval.1.0.0.md) | allOf | one |  |
| Included by | [Rig.1.0.0](../master-data/Rig.1.0.0.md) | allOf | one |  |
| Included by | [StorageFacility.1.0.0](../master-data/StorageFacility.1.0.0.md) | allOf | one |  |
| Included by | [Well.1.0.0](../master-data/Well.1.0.0.md) | allOf | one |  |
| Included by | [Well.1.1.0](../master-data/Well.1.1.0.md) | allOf | one |  |
| Included by | [Wellbore.1.0.0](../master-data/Wellbore.1.0.0.md) | allOf | one |  |
| Included by | [Wellbore.1.1.0](../master-data/Wellbore.1.1.0.md) | allOf | one |  |
| Included by | [Wellbore.1.1.1](../master-data/Wellbore.1.1.1.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._