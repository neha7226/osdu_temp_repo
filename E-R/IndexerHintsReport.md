<a name="TOC"></a>
### Table of Contents

[[_TOC_]]

# About Indexer Hints

OSDU Data Definitions decorates arrays of objects with hints to the (Elastic) indexer. The hints refer to Elastic terminology. Further documentation is available via the following links:
* **object** (which is also the default), see [Elastic documentation for `object`](https://www.elastic.co/guide/en/elasticsearch/reference/master/object.html#object)
* **flattened**, see [Elastic documentation for `flattened`](https://www.elastic.co/guide/en/elasticsearch/reference/master/flattened.html#flattened)
* **nested**, see [Elastic documentation for `nested`](https://www.elastic.co/guide/en/elasticsearch/reference/master/nested.html#nested)

# Indexer Hints per Kind


[Back to TOC](#TOC)

## Pseudo-Group-Type abstract

|Kind|Cumulative Name|Indexer Hint|Comment|
|----|----|----|----|
|&rarr; [osdu:wks:AbstractActivityParameter:1.0.0](abstract/AbstractActivityParameter.1.0.0.md)|Keys[]|object (default)|-|
|&rarr; [osdu:wks:AbstractAnyCrsFeatureCollection:1.0.0](abstract/AbstractAnyCrsFeatureCollection.1.0.0.md)|features[]|object (default)|FeatureCollection-like structure indexed as GeoJSON|
|&rarr; [osdu:wks:AbstractAnyCrsFeatureCollection:1.0.0](abstract/AbstractAnyCrsFeatureCollection.1.0.0.md)|features[].geometry.geometries[]|object (default)|FeatureCollection-like structure indexed as GeoJSON|
|&rarr; [osdu:wks:AbstractAnyCrsFeatureCollection:1.1.0](abstract/AbstractAnyCrsFeatureCollection.1.1.0.md)|features[]|object (default)|FeatureCollection-like structure indexed as GeoJSON|
|&rarr; [osdu:wks:AbstractAnyCrsFeatureCollection:1.1.0](abstract/AbstractAnyCrsFeatureCollection.1.1.0.md)|features[].geometry.geometries[]|object (default)|FeatureCollection-like structure indexed as GeoJSON|
|&rarr; [osdu:wks:AbstractBinGrid:1.0.0](abstract/AbstractBinGrid.1.0.0.md)|ABCDBinGridLocalCoordinates[]|object (default)|-|
|&rarr; [osdu:wks:AbstractBinGrid:1.1.0](abstract/AbstractBinGrid.1.1.0.md)|ABCDBinGridLocalCoordinates[]|object (default)|Deprecated|
|&rarr; [osdu:wks:AbstractBusinessRule:1.0.0](abstract/AbstractBusinessRule.1.0.0.md)|DataRuleSets[]|nested|-|
|&rarr; [osdu:wks:AbstractBusinessRule:1.0.0](abstract/AbstractBusinessRule.1.0.0.md)|DataRules[]|nested|-|
|&rarr; [osdu:wks:AbstractColumnBasedTable:1.0.0](abstract/AbstractColumnBasedTable.1.0.0.md)|KeyColumns[]|nested|-|
|&rarr; [osdu:wks:AbstractColumnBasedTable:1.0.0](abstract/AbstractColumnBasedTable.1.0.0.md)|KeyColumns[].FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:AbstractColumnBasedTable:1.0.0](abstract/AbstractColumnBasedTable.1.0.0.md)|Columns[]|nested|-|
|&rarr; [osdu:wks:AbstractColumnBasedTable:1.0.0](abstract/AbstractColumnBasedTable.1.0.0.md)|Columns[].FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:AbstractColumnBasedTable:1.0.0](abstract/AbstractColumnBasedTable.1.0.0.md)|ColumnValues[]|object (default)|-|
|&rarr; [osdu:wks:AbstractFacility:1.0.0](abstract/AbstractFacility.1.0.0.md)|FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:AbstractFacility:1.0.0](abstract/AbstractFacility.1.0.0.md)|FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:AbstractFacility:1.0.0](abstract/AbstractFacility.1.0.0.md)|FacilityStates[]|nested|-|
|&rarr; [osdu:wks:AbstractFacility:1.0.0](abstract/AbstractFacility.1.0.0.md)|FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:AbstractFacility:1.0.0](abstract/AbstractFacility.1.0.0.md)|FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:AbstractFacility:1.1.0](abstract/AbstractFacility.1.1.0.md)|FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:AbstractFacility:1.1.0](abstract/AbstractFacility.1.1.0.md)|FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:AbstractFacility:1.1.0](abstract/AbstractFacility.1.1.0.md)|FacilityStates[]|nested|-|
|&rarr; [osdu:wks:AbstractFacility:1.1.0](abstract/AbstractFacility.1.1.0.md)|FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:AbstractFacility:1.1.0](abstract/AbstractFacility.1.1.0.md)|FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:AbstractFeatureCollection:1.0.0](abstract/AbstractFeatureCollection.1.0.0.md)|features[]|object (default)|FeatureCollection-like structure indexed as GeoJSON|
|&rarr; [osdu:wks:AbstractFeatureCollection:1.0.0](abstract/AbstractFeatureCollection.1.0.0.md)|features[].geometry.geometries[]|object (default)|FeatureCollection-like structure indexed as GeoJSON|
|&rarr; [osdu:wks:AbstractFileCollection:1.0.0](abstract/AbstractFileCollection.1.0.0.md)|DatasetProperties.FileSourceInfos[]|object (default)|-|
|&rarr; [osdu:wks:AbstractGeoJson.PropertiesBinGridCorners:1.0.0](abstract/AbstractGeoJson.PropertiesBinGridCorners.1.0.0.md)|PointProperties[]|object (default)|-|
|&rarr; [osdu:wks:AbstractGridRepresentation:1.0.0](abstract/AbstractGridRepresentation.1.0.0.md)|IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:AbstractMaster:1.0.0](abstract/AbstractMaster.1.0.0.md)|NameAliases[]|nested|-|
|&rarr; [osdu:wks:AbstractMaster:1.0.0](abstract/AbstractMaster.1.0.0.md)|GeoContexts[]|nested|-|
|&rarr; [osdu:wks:AbstractMaster:1.1.0](abstract/AbstractMaster.1.1.0.md)|NameAliases[]|nested|-|
|&rarr; [osdu:wks:AbstractMaster:1.1.0](abstract/AbstractMaster.1.1.0.md)|GeoContexts[]|nested|-|
|&rarr; [osdu:wks:AbstractPersistableReference:1.0.0](abstract/AbstractPersistableReference.1.0.0.md)|compoundCT.cts[]|object (default)|-|
|&rarr; [osdu:wks:AbstractPersistableReference:1.0.0](abstract/AbstractPersistableReference.1.0.0.md)|horzEarlyBoundCRS.compoundCT.cts[]|object (default)|-|
|&rarr; [osdu:wks:AbstractPersistableReference:1.0.0](abstract/AbstractPersistableReference.1.0.0.md)|vertEarlyBoundCRS.compoundCT.cts[]|object (default)|-|
|&rarr; [osdu:wks:AbstractProject:1.0.0](abstract/AbstractProject.1.0.0.md)|ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:AbstractProject:1.0.0](abstract/AbstractProject.1.0.0.md)|FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:AbstractProject:1.0.0](abstract/AbstractProject.1.0.0.md)|Contractors[]|nested|-|
|&rarr; [osdu:wks:AbstractProject:1.0.0](abstract/AbstractProject.1.0.0.md)|Personnel[]|flattened|-|
|&rarr; [osdu:wks:AbstractProject:1.0.0](abstract/AbstractProject.1.0.0.md)|ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:AbstractProject:1.0.0](abstract/AbstractProject.1.0.0.md)|ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:AbstractProjectActivity:1.0.0](abstract/AbstractProjectActivity.1.0.0.md)|Parameters[]|nested|-|
|&rarr; [osdu:wks:AbstractProjectActivity:1.0.0](abstract/AbstractProjectActivity.1.0.0.md)|Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:AbstractProjectActivity:1.1.0](abstract/AbstractProjectActivity.1.1.0.md)|Parameters[]|nested|-|
|&rarr; [osdu:wks:AbstractProjectActivity:1.1.0](abstract/AbstractProjectActivity.1.1.0.md)|Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:AbstractProjectActivity:1.1.0](abstract/AbstractProjectActivity.1.1.0.md)|ActivityStates[]|flattened|-|
|&rarr; [osdu:wks:AbstractReferencePropertyType:1.1.0](abstract/AbstractReferencePropertyType.1.1.0.md)|FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:AbstractReferenceType:1.0.0](abstract/AbstractReferenceType.1.0.0.md)|NameAlias[]|nested|-|
|&rarr; [osdu:wks:AbstractRepresentation:1.0.0](abstract/AbstractRepresentation.1.0.0.md)|IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:AbstractTechnicalAssurance:1.0.0](abstract/AbstractTechnicalAssurance.1.0.0.md)|Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:AbstractTechnicalAssurance:1.0.0](abstract/AbstractTechnicalAssurance.1.0.0.md)|AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:AbstractTechnicalAssurance:1.0.0](abstract/AbstractTechnicalAssurance.1.0.0.md)|UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:AbstractVectorHeaderMapping:1.0.0](abstract/AbstractVectorHeaderMapping.1.0.0.md)|VectorHeaderMapping[]|object (default)|-|
|&rarr; [osdu:wks:AbstractWorkProductComponent:1.0.0](abstract/AbstractWorkProductComponent.1.0.0.md)|GeoContexts[]|nested|-|
|&rarr; [osdu:wks:AbstractWorkProductComponent:1.0.0](abstract/AbstractWorkProductComponent.1.0.0.md)|LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:AbstractWorkProductComponent:1.1.0](abstract/AbstractWorkProductComponent.1.1.0.md)|GeoContexts[]|nested|-|
|&rarr; [osdu:wks:AbstractWorkProductComponent:1.1.0](abstract/AbstractWorkProductComponent.1.1.0.md)|LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:AbstractWPCActivity:1.0.0](abstract/AbstractWPCActivity.1.0.0.md)|Parameters[]|nested|-|
|&rarr; [osdu:wks:AbstractWPCActivity:1.0.0](abstract/AbstractWPCActivity.1.0.0.md)|Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:AbstractWPCActivity:1.0.0](abstract/AbstractWPCActivity.1.0.0.md)|SoftwareSpecifications[]|flattened|-|
|&rarr; [osdu:wks:AbstractWPCActivity:1.1.0](abstract/AbstractWPCActivity.1.1.0.md)|Parameters[]|nested|-|
|&rarr; [osdu:wks:AbstractWPCActivity:1.1.0](abstract/AbstractWPCActivity.1.1.0.md)|Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:AbstractWPCActivity:1.1.0](abstract/AbstractWPCActivity.1.1.0.md)|SoftwareSpecifications[]|flattened|-|
|&rarr; [osdu:wks:AbstractWPCActivity:1.1.0](abstract/AbstractWPCActivity.1.1.0.md)|ActivityStates[]|flattened|-|
|&rarr; [osdu:wks:AbstractWPCGroupType:1.0.0](abstract/AbstractWPCGroupType.1.0.0.md)|Artefacts[]|flattened|-|
|&rarr; [osdu:wks:AbstractWPCGroupType:1.0.0](abstract/AbstractWPCGroupType.1.0.0.md)|TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:AbstractWPCGroupType:1.0.0](abstract/AbstractWPCGroupType.1.0.0.md)|TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:AbstractWPCGroupType:1.0.0](abstract/AbstractWPCGroupType.1.0.0.md)|TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:AbstractWPCGroupType:1.0.0](abstract/AbstractWPCGroupType.1.0.0.md)|TechnicalAssurances[].UnacceptableUsage[]|flattened|-|

[Back to TOC](#TOC)

## Group-Type dataset

|Kind|Cumulative Name|Indexer Hint|Comment|
|----|----|----|----|
|&rarr; [osdu:wks:dataset--File.CompressedVectorHeaders:1.0.0](dataset/File.CompressedVectorHeaders.1.0.0.md)|data.VectorHeaderMapping[]|object (default)|-|
|&rarr; [osdu:wks:dataset--FileCollection.Bluware.OpenVDS:1.0.0](dataset/FileCollection.Bluware.OpenVDS.1.0.0.md)|data.DatasetProperties.FileSourceInfos[]|object (default)|-|
|&rarr; [osdu:wks:dataset--FileCollection.Esri.Shape:1.0.0](dataset/FileCollection.Esri.Shape.1.0.0.md)|data.DatasetProperties.FileSourceInfos[]|object (default)|-|
|&rarr; [osdu:wks:dataset--FileCollection.Generic:1.0.0](dataset/FileCollection.Generic.1.0.0.md)|data.DatasetProperties.FileSourceInfos[]|object (default)|-|
|&rarr; [osdu:wks:dataset--FileCollection.SEGY:1.0.0](dataset/FileCollection.SEGY.1.0.0.md)|data.DatasetProperties.FileSourceInfos[]|object (default)|-|
|&rarr; [osdu:wks:dataset--FileCollection.SEGY:1.0.0](dataset/FileCollection.SEGY.1.0.0.md)|data.VectorHeaderMapping[]|object (default)|-|
|&rarr; [osdu:wks:dataset--FileCollection.Slb.OpenZGY:1.0.0](dataset/FileCollection.Slb.OpenZGY.1.0.0.md)|data.DatasetProperties.FileSourceInfos[]|object (default)|-|

[Back to TOC](#TOC)

## Pseudo-Group-Type manifest

|Kind|Cumulative Name|Indexer Hint|Comment|
|----|----|----|----|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|ReferenceData[]|object (default)|Not persisted, thus not indexed|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|MasterData[]|object (default)|Not persisted, thus not indexed|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|Data.WorkProduct.data.LineageAssertions[]|object (default)|Not persisted, thus not indexed|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|Data.WorkProductComponents[]|object (default)|Not persisted, thus not indexed|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|Data.WorkProductComponents[].data.Artefacts[]|flattened|Not persisted, thus not indexed|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|Data.WorkProductComponents[].data.TechnicalAssurances[]|nested|Not persisted, thus not indexed|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|Data.WorkProductComponents[].data.TechnicalAssurances[].Reviewers[]|object (default)|Not persisted, thus not indexed|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|Data.WorkProductComponents[].data.TechnicalAssurances[].AcceptableUsage[]|flattened|Not persisted, thus not indexed|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|Data.WorkProductComponents[].data.TechnicalAssurances[].UnacceptableUsage[]|flattened|Not persisted, thus not indexed|
|&rarr; [osdu:wks:Manifest:1.0.0](manifest/Manifest.1.0.0.md)|Data.Datasets[]|object (default)|Not persisted, thus not indexed|

[Back to TOC](#TOC)

## Group-Type master-data

|Kind|Cumulative Name|Indexer Hint|Comment|
|----|----|----|----|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.WellPlanningActivities[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.WellPlanningActivities[].ProductiveTimeStatistics[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.WellPlanningActivities[].NonProductiveTimeStatistics[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.0.0](master-data/ActivityPlan.1.0.0.md)|data.WellPlanningActivities[].RateOfPenetrationStatistics[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.WellPlanningActivities[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.WellPlanningActivities[].ProductiveTimeStatistics[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.WellPlanningActivities[].NonProductiveTimeStatistics[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityPlan:1.1.0](master-data/ActivityPlan.1.1.0.md)|data.WellPlanningActivities[].RateOfPenetrationStatistics[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityTemplate:1.0.0](master-data/ActivityTemplate.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityTemplate:1.0.0](master-data/ActivityTemplate.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityTemplate:1.0.0](master-data/ActivityTemplate.1.0.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityTemplate:1.0.0](master-data/ActivityTemplate.1.0.0.md)|data.Parameters[].DefaultValue.Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityTemplateArc:1.0.0](master-data/ActivityTemplateArc.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityTemplateArc:1.0.0](master-data/ActivityTemplateArc.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ActivityTemplateArc:1.0.0](master-data/ActivityTemplateArc.1.0.0.md)|data.OutputInputArcs[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ActivityTemplateArc:1.0.0](master-data/ActivityTemplateArc.1.0.0.md)|data.ActivityArcs[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Agreement:1.0.0](master-data/Agreement.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Agreement:1.0.0](master-data/Agreement.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Agreement:1.0.0](master-data/Agreement.1.0.0.md)|data.Terms[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Agreement:1.0.0](master-data/Agreement.1.0.0.md)|data.RestrictedResources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Basin:1.0.0](master-data/Basin.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Basin:1.0.0](master-data/Basin.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Basin:1.0.0](master-data/Basin.1.0.0.md)|data.BasinNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.TorqueOnBottomGroup.MaximumTorqueOnBottom.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.TorqueOnBottomGroup.MaximumTorqueOnBottom.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.TorqueOnBottomGroup.MinimumTorqueOnBottom.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.TorqueOnBottomGroup.MinimumTorqueOnBottom.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.MaximumRPM.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.MaximumRPM.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.MinimumRPM.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.MinimumRPM.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.RecommendedRPM.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.RecommendedRPM.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.DownHoleMaximumRPM.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.DownHoleMaximumRPM.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.DownHoleMinimumRPM.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.DownHoleMinimumRPM.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.DownHoleRecommendedRPM.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.RPMGroup.DownHoleRecommendedRPM.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.ROPGroup.MaximumROP.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.ROPGroup.MaximumROP.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.ROPGroup.MinimumROP.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.ROPGroup.MinimumROP.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.ROPGroup.RecommendedROP.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.ROPGroup.RecommendedROP.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.WOBGroup.MaximumWOB.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.WOBGroup.MaximumWOB.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.WOBGroup.MinimumWOB.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.WOBGroup.MinimumWOB.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.WOBGroup.RecommendedWOB.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.WOBGroup.RecommendedWOB.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.FlowratePumpGroup.MaximumFlowratePump.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.FlowratePumpGroup.MaximumFlowratePump.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.FlowratePumpGroup.MinimumFlowratePump.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.FlowratePumpGroup.MinimumFlowratePump.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.FlowratePumpGroup.RecommendedFlowratePump.Point[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.RunParameterPlans[].OperationParameterPlan.FlowratePumpGroup.RecommendedFlowratePump.Point[].PointsSources[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BHARun:1.0.0](master-data/BHARun.1.0.0.md)|data.DrillingParams[]|object (default)|-|
|&rarr; [osdu:wks:master-data--BoundaryFeature:1.0.0](master-data/BoundaryFeature.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--BoundaryFeature:1.0.0](master-data/BoundaryFeature.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--CasingDesign:1.0.0](master-data/CasingDesign.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--CasingDesign:1.0.0](master-data/CasingDesign.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.0.0](master-data/ConnectedSourceDataJob.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.0.0](master-data/ConnectedSourceDataJob.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.0.0](master-data/ConnectedSourceDataJob.1.0.0.md)|data.Workflows[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.0.0](master-data/ConnectedSourceDataJob.1.0.0.md)|data.Workflows[].Parameters[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.1.0](master-data/ConnectedSourceDataJob.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.1.0](master-data/ConnectedSourceDataJob.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.1.0](master-data/ConnectedSourceDataJob.1.1.0.md)|data.Workflows[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.1.0](master-data/ConnectedSourceDataJob.1.1.0.md)|data.Workflows[].Parameters[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.2.0](master-data/ConnectedSourceDataJob.1.2.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.2.0](master-data/ConnectedSourceDataJob.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.2.0](master-data/ConnectedSourceDataJob.1.2.0.md)|data.Workflows[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.2.0](master-data/ConnectedSourceDataJob.1.2.0.md)|data.Workflows[].Parameters[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.3.0](master-data/ConnectedSourceDataJob.1.3.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.3.0](master-data/ConnectedSourceDataJob.1.3.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.3.0](master-data/ConnectedSourceDataJob.1.3.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.3.0](master-data/ConnectedSourceDataJob.1.3.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.3.0](master-data/ConnectedSourceDataJob.1.3.0.md)|data.ActivityStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.3.0](master-data/ConnectedSourceDataJob.1.3.0.md)|data.Workflows[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--ConnectedSourceDataJob:1.3.0](master-data/ConnectedSourceDataJob.1.3.0.md)|data.Workflows[].Parameters[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.0.0](master-data/ConnectedSourceRegistryEntry.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.0.0](master-data/ConnectedSourceRegistryEntry.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.0.0](master-data/ConnectedSourceRegistryEntry.1.0.0.md)|data.SecuritySchemes[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.1.0](master-data/ConnectedSourceRegistryEntry.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.1.0](master-data/ConnectedSourceRegistryEntry.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.1.0](master-data/ConnectedSourceRegistryEntry.1.1.0.md)|data.SecuritySchemes[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.1.0](master-data/ConnectedSourceRegistryEntry.1.1.0.md)|data.SmtpSchemes[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.2.0](master-data/ConnectedSourceRegistryEntry.1.2.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.2.0](master-data/ConnectedSourceRegistryEntry.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.2.0](master-data/ConnectedSourceRegistryEntry.1.2.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.2.0](master-data/ConnectedSourceRegistryEntry.1.2.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.2.0](master-data/ConnectedSourceRegistryEntry.1.2.0.md)|data.ActivityStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.2.0](master-data/ConnectedSourceRegistryEntry.1.2.0.md)|data.SecuritySchemes[]|object (default)|-|
|&rarr; [osdu:wks:master-data--ConnectedSourceRegistryEntry:1.2.0](master-data/ConnectedSourceRegistryEntry.1.2.0.md)|data.SmtpSchemes[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Coring:1.0.0](master-data/Coring.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Coring:1.0.0](master-data/Coring.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Coring:1.0.0](master-data/Coring.1.0.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--Coring:1.0.0](master-data/Coring.1.0.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Coring:1.0.0](master-data/Coring.1.0.0.md)|data.CoreRemarks[]|flattened|-|
|&rarr; [osdu:wks:master-data--Coring:1.0.0](master-data/Coring.1.0.0.md)|data.ConventionalCoring.CoreSections[]|flattened|-|
|&rarr; [osdu:wks:master-data--Coring:1.0.0](master-data/Coring.1.0.0.md)|data.SidewallCoring.SidewallCores[]|nested|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.FormationIntegrityEvaluations[]|object (default)|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.FormationEvaluationCorings[]|object (default)|-|
|&rarr; [osdu:wks:master-data--EvaluationPlan:1.0.0](master-data/EvaluationPlan.1.0.0.md)|data.ReservoirEvaluations[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Field:1.0.0](master-data/Field.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Field:1.0.0](master-data/Field.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Field:1.0.0](master-data/Field.1.0.0.md)|data.FieldNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--FluidsProgram:1.0.0](master-data/FluidsProgram.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--FluidsProgram:1.0.0](master-data/FluidsProgram.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--FluidsProgram:1.0.0](master-data/FluidsProgram.1.0.0.md)|data.FluidIntervals[]|object (default)|-|
|&rarr; [osdu:wks:master-data--FluidsProgram:1.0.0](master-data/FluidsProgram.1.0.0.md)|data.FluidIntervals[].FluidsSystem.FluidProperties[]|object (default)|-|
|&rarr; [osdu:wks:master-data--FluidsProgram:1.0.0](master-data/FluidsProgram.1.0.0.md)|data.FluidIntervals[].FluidsSystem.FluidProperties[].FluidFacets[]|object (default)|-|
|&rarr; [osdu:wks:master-data--FluidsProgram:1.0.0](master-data/FluidsProgram.1.0.0.md)|data.FluidIntervals[].FluidsSystem.BarrelFormulation[]|object (default)|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.0.0](master-data/FluidsReport.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.0.0](master-data/FluidsReport.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.0.0](master-data/FluidsReport.1.0.0.md)|data.Fluid[]|object (default)|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.0.0](master-data/FluidsReport.1.0.0.md)|data.Fluid[].Rheometer[]|object (default)|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.0.0](master-data/FluidsReport.1.0.0.md)|data.Fluid[].Rheometer[].RheometerViscosities[]|object (default)|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.1.0](master-data/FluidsReport.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.1.0](master-data/FluidsReport.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.1.0](master-data/FluidsReport.1.1.0.md)|data.Fluid[]|object (default)|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.1.0](master-data/FluidsReport.1.1.0.md)|data.Fluid[].Rheometer[]|object (default)|-|
|&rarr; [osdu:wks:master-data--FluidsReport:1.1.0](master-data/FluidsReport.1.1.0.md)|data.Fluid[].Rheometer[].RheometerViscosities[]|object (default)|-|
|&rarr; [osdu:wks:master-data--GeometricTargetSet:1.0.0](master-data/GeometricTargetSet.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--GeometricTargetSet:1.0.0](master-data/GeometricTargetSet.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--GeometricTargetSet:1.0.0](master-data/GeometricTargetSet.1.0.0.md)|data.Targets[]|object (default)|-|
|&rarr; [osdu:wks:master-data--GeometricTargetSet:1.0.0](master-data/GeometricTargetSet.1.0.0.md)|data.Targets[].Geometry.GeometrySections[]|object (default)|-|
|&rarr; [osdu:wks:master-data--GeoPoliticalEntity:1.0.0](master-data/GeoPoliticalEntity.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--GeoPoliticalEntity:1.0.0](master-data/GeoPoliticalEntity.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--GeoPoliticalEntity:1.0.0](master-data/GeoPoliticalEntity.1.0.0.md)|data.GeoPoliticalEntityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--HoleSection:1.0.0](master-data/HoleSection.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--HoleSection:1.0.0](master-data/HoleSection.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--HoleSection:1.1.0](master-data/HoleSection.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--HoleSection:1.1.0](master-data/HoleSection.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--IsolatedInterval:1.0.0](master-data/IsolatedInterval.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--IsolatedInterval:1.0.0](master-data/IsolatedInterval.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--IsolatedInterval:1.0.0](master-data/IsolatedInterval.1.0.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--IsolatedInterval:1.0.0](master-data/IsolatedInterval.1.0.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--IsolatedInterval:1.0.0](master-data/IsolatedInterval.1.0.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--IsolatedInterval:1.0.0](master-data/IsolatedInterval.1.0.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--IsolatedInterval:1.0.0](master-data/IsolatedInterval.1.0.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--IsolatedInterval:1.0.0](master-data/IsolatedInterval.1.0.0.md)|data.IsolatedIntervalBases[]|nested|-|
|&rarr; [osdu:wks:master-data--ModelFeature:1.0.0](master-data/ModelFeature.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ModelFeature:1.0.0](master-data/ModelFeature.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.BitRecord[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.DrillActivity[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.DrillActivity[].ProprietaryActivityCode[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.WellboreAlias[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.StatusInfo[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.TimedComments[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.Weather[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.Personnel[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.PumpOp[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.MudVolume[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.Inventory[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.Cost[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.HSE[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.Incident[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.MudLosses[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.JobContact[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.0.0](master-data/OperationsReport.1.0.0.md)|data.GasReading[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.BitRecord[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.DrillActivity[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.DrillActivity[].ProprietaryActivityCode[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.OperationsActivity[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.OperationsActivity[].ProprietaryActivityCode[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.WellboreAlias[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.StatusInfo[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.TimedComments[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.Weather[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.Personnel[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.PumpOp[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.MudVolume[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.Inventory[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.Cost[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.HSE[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.Incident[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.MudLosses[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.JobContact[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.1.0](master-data/OperationsReport.1.1.0.md)|data.GasReading[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.BitRecord[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.DrillActivity[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.DrillActivity[].ProprietaryActivityCode[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.OperationsActivity[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.OperationsActivity[].ProprietaryActivityCode[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.WellboreAlias[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.StatusInfo[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.TimedComments[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.Weather[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.Personnel[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.PumpOp[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.MudVolume[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.Inventory[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.Cost[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.HSE[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.Incident[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.MudLosses[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.JobContact[]|object (default)|-|
|&rarr; [osdu:wks:master-data--OperationsReport:1.2.0](master-data/OperationsReport.1.2.0.md)|data.GasReading[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Organisation:1.0.0](master-data/Organisation.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Organisation:1.0.0](master-data/Organisation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Organisation:1.0.0](master-data/Organisation.1.0.0.md)|data.OrganisationNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Organisation:1.1.0](master-data/Organisation.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Organisation:1.1.0](master-data/Organisation.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Organisation:1.1.0](master-data/Organisation.1.1.0.md)|data.OrganisationNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--PerforationInterval:1.0.0](master-data/PerforationInterval.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--PerforationInterval:1.0.0](master-data/PerforationInterval.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--PerforationJob:1.0.0](master-data/PerforationJob.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--PerforationJob:1.0.0](master-data/PerforationJob.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--PlannedCementJob:1.0.0](master-data/PlannedCementJob.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--PlannedCementJob:1.0.0](master-data/PlannedCementJob.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--PlannedCementJob:1.0.0](master-data/PlannedCementJob.1.0.0.md)|data.CementStages[]|object (default)|-|
|&rarr; [osdu:wks:master-data--PlannedCementJob:1.0.0](master-data/PlannedCementJob.1.0.0.md)|data.CementStages[].CementingFluid.CementAdditives[]|object (default)|-|
|&rarr; [osdu:wks:master-data--PlannedCementJob:1.1.0](master-data/PlannedCementJob.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--PlannedCementJob:1.1.0](master-data/PlannedCementJob.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--PlannedCementJob:1.1.0](master-data/PlannedCementJob.1.1.0.md)|data.CementStages[]|object (default)|-|
|&rarr; [osdu:wks:master-data--PlannedCementJob:1.1.0](master-data/PlannedCementJob.1.1.0.md)|data.CementStages[].CementingFluid.CementAdditives[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Play:1.0.0](master-data/Play.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Play:1.0.0](master-data/Play.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Play:1.0.0](master-data/Play.1.0.0.md)|data.PlayNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Prospect:1.0.0](master-data/Prospect.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Prospect:1.0.0](master-data/Prospect.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Prospect:1.0.0](master-data/Prospect.1.0.0.md)|data.ProspectNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--ReferenceLevel:1.0.0](master-data/ReferenceLevel.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ReferenceLevel:1.0.0](master-data/ReferenceLevel.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Reservoir:0.0.0](master-data/Reservoir.0.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Reservoir:0.0.0](master-data/Reservoir.0.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--ReservoirSegment:0.0.0](master-data/ReservoirSegment.0.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--ReservoirSegment:0.0.0](master-data/ReservoirSegment.0.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.0.0](master-data/Rig.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.0.0](master-data/Rig.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.0.0](master-data/Rig.1.0.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.0.0](master-data/Rig.1.0.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Rig:1.0.0](master-data/Rig.1.0.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.0.0](master-data/Rig.1.0.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.0.0](master-data/Rig.1.0.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Rig:1.1.0](master-data/Rig.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.1.0](master-data/Rig.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.1.0](master-data/Rig.1.1.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.1.0](master-data/Rig.1.1.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Rig:1.1.0](master-data/Rig.1.1.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.1.0](master-data/Rig.1.1.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Rig:1.1.0](master-data/Rig.1.1.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--RigUtilization:1.0.0](master-data/RigUtilization.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--RigUtilization:1.0.0](master-data/RigUtilization.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--RigUtilization:1.0.0](master-data/RigUtilization.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--RigUtilization:1.0.0](master-data/RigUtilization.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--RigUtilization:1.0.0](master-data/RigUtilization.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--RigUtilization:1.0.0](master-data/RigUtilization.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--RigUtilization:1.0.0](master-data/RigUtilization.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--RigUtilization:1.0.0](master-data/RigUtilization.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--RigUtilization:1.0.0](master-data/RigUtilization.1.0.0.md)|data.MudPumps[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Risk:1.0.0](master-data/Risk.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Risk:1.0.0](master-data/Risk.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Risk:1.0.0](master-data/Risk.1.0.0.md)|data.Preventions[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Risk:1.0.0](master-data/Risk.1.0.0.md)|data.Preventions[].Responsibles[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Risk:1.0.0](master-data/Risk.1.0.0.md)|data.Mitigations[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Risk:1.0.0](master-data/Risk.1.0.0.md)|data.Mitigations[].Responsibles[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Risk:1.0.0](master-data/Risk.1.0.0.md)|data.RiskResponsibles[]|object (default)|-|
|&rarr; [osdu:wks:master-data--RockSample:1.0.0](master-data/RockSample.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--RockSample:1.0.0](master-data/RockSample.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--RockSample:1.0.0](master-data/RockSample.1.0.0.md)|data.SampleStorageLocations[]|flattened|-|
|&rarr; [osdu:wks:master-data--RockSample:1.0.0](master-data/RockSample.1.0.0.md)|data.SampleRemarks[]|flattened|-|
|&rarr; [osdu:wks:master-data--RockVolumeFeature:1.0.0](master-data/RockVolumeFeature.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--RockVolumeFeature:1.0.0](master-data/RockVolumeFeature.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0](master-data/Seismic2DInterpretationSet.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0](master-data/Seismic2DInterpretationSet.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0](master-data/Seismic2DInterpretationSet.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0](master-data/Seismic2DInterpretationSet.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0](master-data/Seismic2DInterpretationSet.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0](master-data/Seismic2DInterpretationSet.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0](master-data/Seismic2DInterpretationSet.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0](master-data/Seismic2DInterpretationSet.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.0.0](master-data/Seismic2DInterpretationSet.1.0.0.md)|data.SeismicLineGeometries[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Seismic2DInterpretationSet:1.1.0](master-data/Seismic2DInterpretationSet.1.1.0.md)|data.SeismicLineGeometries[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0](master-data/Seismic3DInterpretationSet.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0](master-data/Seismic3DInterpretationSet.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0](master-data/Seismic3DInterpretationSet.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0](master-data/Seismic3DInterpretationSet.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0](master-data/Seismic3DInterpretationSet.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0](master-data/Seismic3DInterpretationSet.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0](master-data/Seismic3DInterpretationSet.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.0.0](master-data/Seismic3DInterpretationSet.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--Seismic3DInterpretationSet:1.1.0](master-data/Seismic3DInterpretationSet.1.1.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0](master-data/SeismicAcquisitionSurvey.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0](master-data/SeismicAcquisitionSurvey.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0](master-data/SeismicAcquisitionSurvey.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0](master-data/SeismicAcquisitionSurvey.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0](master-data/SeismicAcquisitionSurvey.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0](master-data/SeismicAcquisitionSurvey.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0](master-data/SeismicAcquisitionSurvey.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.0.0](master-data/SeismicAcquisitionSurvey.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.SourceConfigurations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.1.0](master-data/SeismicAcquisitionSurvey.1.1.0.md)|data.ReceiverConfigurations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.SourceConfigurations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicAcquisitionSurvey:1.2.0](master-data/SeismicAcquisitionSurvey.1.2.0.md)|data.ReceiverConfigurations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.0.0](master-data/SeismicProcessingProject.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.0.0](master-data/SeismicProcessingProject.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.0.0](master-data/SeismicProcessingProject.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.0.0](master-data/SeismicProcessingProject.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.0.0](master-data/SeismicProcessingProject.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.0.0](master-data/SeismicProcessingProject.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.0.0](master-data/SeismicProcessingProject.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.0.0](master-data/SeismicProcessingProject.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.1.0](master-data/SeismicProcessingProject.1.1.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--SeismicProcessingProject:1.2.0](master-data/SeismicProcessingProject.1.2.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.0.0](master-data/StorageFacility.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.0.0](master-data/StorageFacility.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.0.0](master-data/StorageFacility.1.0.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.0.0](master-data/StorageFacility.1.0.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--StorageFacility:1.0.0](master-data/StorageFacility.1.0.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.0.0](master-data/StorageFacility.1.0.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.0.0](master-data/StorageFacility.1.0.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.1.0](master-data/StorageFacility.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.1.0](master-data/StorageFacility.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.1.0](master-data/StorageFacility.1.1.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.1.0](master-data/StorageFacility.1.1.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--StorageFacility:1.1.0](master-data/StorageFacility.1.1.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.1.0](master-data/StorageFacility.1.1.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--StorageFacility:1.1.0](master-data/StorageFacility.1.1.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--SurveyProgram:1.0.0](master-data/SurveyProgram.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--SurveyProgram:1.0.0](master-data/SurveyProgram.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--SurveyProgram:1.0.0](master-data/SurveyProgram.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--SurveyProgram:1.0.0](master-data/SurveyProgram.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--SurveyProgram:1.0.0](master-data/SurveyProgram.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--SurveyProgram:1.0.0](master-data/SurveyProgram.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--SurveyProgram:1.0.0](master-data/SurveyProgram.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--SurveyProgram:1.0.0](master-data/SurveyProgram.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--SurveyProgram:1.0.0](master-data/SurveyProgram.1.0.0.md)|data.SurveySections[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Well:1.0.0](master-data/Well.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.0.0](master-data/Well.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.0.0](master-data/Well.1.0.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.0.0](master-data/Well.1.0.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Well:1.0.0](master-data/Well.1.0.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.0.0](master-data/Well.1.0.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.0.0](master-data/Well.1.0.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Well:1.0.0](master-data/Well.1.0.0.md)|data.VerticalMeasurements[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.1.0](master-data/Well.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.1.0](master-data/Well.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.1.0](master-data/Well.1.1.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.1.0](master-data/Well.1.1.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Well:1.1.0](master-data/Well.1.1.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.1.0](master-data/Well.1.1.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.1.0](master-data/Well.1.1.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Well:1.1.0](master-data/Well.1.1.0.md)|data.VerticalMeasurements[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.1.0](master-data/Well.1.1.0.md)|data.HistoricalInterests[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Well:1.2.0](master-data/Well.1.2.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.2.0](master-data/Well.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.2.0](master-data/Well.1.2.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.2.0](master-data/Well.1.2.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Well:1.2.0](master-data/Well.1.2.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.2.0](master-data/Well.1.2.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.2.0](master-data/Well.1.2.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Well:1.2.0](master-data/Well.1.2.0.md)|data.VerticalMeasurements[]|nested|-|
|&rarr; [osdu:wks:master-data--Well:1.2.0](master-data/Well.1.2.0.md)|data.HistoricalInterests[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.0.0](master-data/WellActivity.1.0.0.md)|data.ActivityStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.ActivityStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivity:1.1.0](master-data/WellActivity.1.1.0.md)|data.RigAssignments[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivityProgram:1.0.0](master-data/WellActivityProgram.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivityProgram:1.0.0](master-data/WellActivityProgram.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivityProgram:1.0.0](master-data/WellActivityProgram.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--WellActivityProgram:1.0.0](master-data/WellActivityProgram.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivityProgram:1.0.0](master-data/WellActivityProgram.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--WellActivityProgram:1.0.0](master-data/WellActivityProgram.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivityProgram:1.0.0](master-data/WellActivityProgram.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivityProgram:1.0.0](master-data/WellActivityProgram.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellActivityProgram:1.0.0](master-data/WellActivityProgram.1.0.0.md)|data.Phases[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.ProjectNames[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.FundsAuthorizations[]|nested|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.Contractors[]|nested|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.Personnel[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.ProjectSpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.ProjectStates[]|flattened|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.TestSteps[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.TestSteps[].HighPressureCriteria[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.TestSteps[].LowPressureCriteria[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.TestSteps[].ComponentVolumesTestSystem[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.TestSteps[].ExpectedVolumesPumped[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.TestSteps[].ExpectedVolumesBledBack[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellBarrierElementTest:1.0.0](master-data/WellBarrierElementTest.1.0.0.md)|data.TestSteps[].TestFluids[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.0.0](master-data/Wellbore.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.0.0](master-data/Wellbore.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.0.0](master-data/Wellbore.1.0.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.0.0](master-data/Wellbore.1.0.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Wellbore:1.0.0](master-data/Wellbore.1.0.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.0.0](master-data/Wellbore.1.0.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.0.0](master-data/Wellbore.1.0.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.0.0](master-data/Wellbore.1.0.0.md)|data.VerticalMeasurements[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.0.0](master-data/Wellbore.1.0.0.md)|data.DrillingReasons[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.VerticalMeasurements[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.DrillingReasons[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.0](master-data/Wellbore.1.1.0.md)|data.HistoricalInterests[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.VerticalMeasurements[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.DrillingReasons[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.1.1](master-data/Wellbore.1.1.1.md)|data.HistoricalInterests[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.VerticalMeasurements[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.DrillingReasons[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.2.0](master-data/Wellbore.1.2.0.md)|data.HistoricalInterests[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.FacilityOperators[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.FacilityNameAliases[]|object (default)|Deprecated|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.FacilityStates[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.FacilityEvents[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.FacilitySpecifications[]|flattened|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.VerticalMeasurements[]|nested|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.DrillingReasons[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.HistoricalInterests[]|object (default)|-|
|&rarr; [osdu:wks:master-data--Wellbore:1.3.0](master-data/Wellbore.1.3.0.md)|data.WellboreCosts[]|nested|-|
|&rarr; [osdu:wks:master-data--WellboreArchitecture:1.0.0](master-data/WellboreArchitecture.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--WellboreArchitecture:1.0.0](master-data/WellboreArchitecture.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--WellboreArchitecture:1.0.0](master-data/WellboreArchitecture.1.0.0.md)|data.InstalledTubulars[]|object (default)|-|
|&rarr; [osdu:wks:master-data--WellboreOpening:1.0.0](master-data/WellboreOpening.1.0.0.md)|data.NameAliases[]|nested|-|
|&rarr; [osdu:wks:master-data--WellboreOpening:1.0.0](master-data/WellboreOpening.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:master-data--WellboreOpening:1.0.0](master-data/WellboreOpening.1.0.0.md)|data.WellboreOpeningStates[]|nested|-|

[Back to TOC](#TOC)

## Group-Type reference-data

|Kind|Cumulative Name|Indexer Hint|Comment|
|----|----|----|----|
|&rarr; [osdu:wks:reference-data--ActivityCode:1.0.0](reference-data/ActivityCode.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ActivityLevel:1.0.0](reference-data/ActivityLevel.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ActivityOutcome:1.0.0](reference-data/ActivityOutcome.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ActivityOutcomeDetail:1.0.0](reference-data/ActivityOutcomeDetail.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ActivityStatus:1.0.0](reference-data/ActivityStatus.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ActivityType:1.0.0](reference-data/ActivityType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ActualIndicatorType:1.0.0](reference-data/ActualIndicatorType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--AdditiveRole:1.0.0](reference-data/AdditiveRole.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--AdditiveType:1.0.0](reference-data/AdditiveType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--AdditiveType:1.0.1](reference-data/AdditiveType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--AgreementType:1.0.0](reference-data/AgreementType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--AliasNameType:1.0.0](reference-data/AliasNameType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--AliasNameTypeClass:1.0.0](reference-data/AliasNameTypeClass.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--AnisotropyType:1.0.0](reference-data/AnisotropyType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--AnnularFluidType:1.0.0](reference-data/AnnularFluidType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ArtefactRole:1.0.0](reference-data/ArtefactRole.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ArtificialLiftType:1.0.0](reference-data/ArtificialLiftType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--AzimuthReferenceType:1.0.0](reference-data/AzimuthReferenceType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--BasinType:1.0.0](reference-data/BasinType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--BhaStatus:1.0.0](reference-data/BhaStatus.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--BinGridDefinitionMethodType:1.0.0](reference-data/BinGridDefinitionMethodType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--BitDullCode:1.0.0](reference-data/BitDullCode.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--BitReasonPulled:1.0.0](reference-data/BitReasonPulled.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--BitType:1.0.0](reference-data/BitType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--BitType:1.0.1](reference-data/BitType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--BottomHolePressureType:1.0.0](reference-data/BottomHolePressureType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--BoundaryRelationType:1.0.0](reference-data/BoundaryRelationType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CalculationMethodType:1.0.0](reference-data/CalculationMethodType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CatalogMapStateType:1.0.0](reference-data/CatalogMapStateType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CellShapeType:1.0.0](reference-data/CellShapeType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CementJobType:1.0.0](reference-data/CementJobType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ChronoStratigraphy:1.0.0](reference-data/ChronoStratigraphy.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CollectionPurpose:1.0.0](reference-data/CollectionPurpose.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ColumnBasedTableType:1.0.0](reference-data/ColumnBasedTableType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ColumnBasedTableType:1.0.0](reference-data/ColumnBasedTableType.1.0.0.md)|data.KeyColumns[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--ColumnBasedTableType:1.0.0](reference-data/ColumnBasedTableType.1.0.0.md)|data.KeyColumns[].FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--ColumnBasedTableType:1.0.0](reference-data/ColumnBasedTableType.1.0.0.md)|data.Columns[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--ColumnBasedTableType:1.0.0](reference-data/ColumnBasedTableType.1.0.0.md)|data.Columns[].FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--ColumnShapeType:1.0.0](reference-data/ColumnShapeType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CompressionMethodType:1.0.0](reference-data/CompressionMethodType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ContactRoleType:1.0.0](reference-data/ContactRoleType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ContractorType:1.0.0](reference-data/ContractorType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ConventionalCoreType:1.0.0](reference-data/ConventionalCoreType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ConventionalCoreType:1.0.1](reference-data/ConventionalCoreType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CoordinateReferenceSystem:1.0.0](reference-data/CoordinateReferenceSystem.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CoordinateReferenceSystem:1.0.0](reference-data/CoordinateReferenceSystem.1.0.0.md)|data.Usages[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--CoordinateReferenceSystem:1.1.0](reference-data/CoordinateReferenceSystem.1.1.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CoordinateReferenceSystem:1.1.0](reference-data/CoordinateReferenceSystem.1.1.0.md)|data.Usages[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--CoordinateTransformation:1.0.0](reference-data/CoordinateTransformation.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CoordinateTransformation:1.0.0](reference-data/CoordinateTransformation.1.0.0.md)|data.ConcatenatedTransformations[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--CoordinateTransformation:1.0.0](reference-data/CoordinateTransformation.1.0.0.md)|data.Usages[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--CoordinateTransformation:1.1.0](reference-data/CoordinateTransformation.1.1.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CoordinateTransformation:1.1.0](reference-data/CoordinateTransformation.1.1.0.md)|data.ConcatenatedTransformations[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--CoordinateTransformation:1.1.0](reference-data/CoordinateTransformation.1.1.0.md)|data.Usages[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--CorePreservationType:1.0.0](reference-data/CorePreservationType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--Currency:1.0.0](reference-data/Currency.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CurveIndexDimensionType:1.0.0](reference-data/CurveIndexDimensionType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--CurveSampleType:1.0.0](reference-data/CurveSampleType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DataRuleDimensionType:1.0.0](reference-data/DataRuleDimensionType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DataRulePurposeType:1.0.0](reference-data/DataRulePurposeType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DepositionGeometryType:1.0.0](reference-data/DepositionGeometryType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DimensionType:1.0.0](reference-data/DimensionType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DiscoverabilityBySearch:1.0.0](reference-data/DiscoverabilityBySearch.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DiscretisationSchemeType:1.0.0](reference-data/DiscretisationSchemeType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DocumentType:1.0.0](reference-data/DocumentType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DomainType:1.0.0](reference-data/DomainType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DrillingActivityClassType:1.0.0](reference-data/DrillingActivityClassType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--DrillingReasonType:1.0.0](reference-data/DrillingReasonType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--EncodingFormatType:1.0.0](reference-data/EncodingFormatType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ExistenceKind:1.0.0](reference-data/ExistenceKind.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ExternalCatalogNamespace:1.0.0](reference-data/ExternalCatalogNamespace.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ExternalReferenceValueMapping:1.0.0](reference-data/ExternalReferenceValueMapping.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ExternalReferenceValueMapping:1.0.0](reference-data/ExternalReferenceValueMapping.1.0.0.md)|data.ComplexMappings[]|flattened|-|
|&rarr; [osdu:wks:reference-data--ExternalUnitOfMeasure:1.0.0](reference-data/ExternalUnitOfMeasure.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ExternalUnitQuantity:1.0.0](reference-data/ExternalUnitQuantity.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FacetRole:1.0.0](reference-data/FacetRole.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FacetType:1.0.0](reference-data/FacetType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FacilityEventType:1.0.0](reference-data/FacilityEventType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FacilityStateType:1.0.0](reference-data/FacilityStateType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FacilityType:1.0.0](reference-data/FacilityType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FaultThrowType:1.0.0](reference-data/FaultThrowType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FeatureType:1.0.0](reference-data/FeatureType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FluidContactType:1.0.0](reference-data/FluidContactType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FluidPhaseType:1.0.0](reference-data/FluidPhaseType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FluidPropertyFacetName:1.0.0](reference-data/FluidPropertyFacetName.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FluidPropertyName:1.0.0](reference-data/FluidPropertyName.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FluidRheologicalModelType:1.0.0](reference-data/FluidRheologicalModelType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FluidRole:1.0.0](reference-data/FluidRole.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FluidType:1.0.0](reference-data/FluidType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FormationIntegrityPressureDataSource:1.0.0](reference-data/FormationIntegrityPressureDataSource.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FormationIntegritySurfacePressureDataSource:1.0.0](reference-data/FormationIntegritySurfacePressureDataSource.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FormationIntegrityTestResult:1.0.0](reference-data/FormationIntegrityTestResult.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FormationIntegrityTestType:1.0.0](reference-data/FormationIntegrityTestType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--FormationPressureTestType:1.0.0](reference-data/FormationPressureTestType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--GasReadingType:1.0.0](reference-data/GasReadingType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--GeologicalFormation:1.0.0](reference-data/GeologicalFormation.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--GeologicUnitShapeType:1.0.0](reference-data/GeologicUnitShapeType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--GeoPoliticalEntityType:1.0.0](reference-data/GeoPoliticalEntityType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--GeoReferencedImageType:1.0.0](reference-data/GeoReferencedImageType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--GrainDensityMeasurementType:1.0.0](reference-data/GrainDensityMeasurementType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--GrainSizeAnalysisMethod:1.0.0](reference-data/GrainSizeAnalysisMethod.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--GrainSizeClassification:1.0.0](reference-data/GrainSizeClassification.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--GrainSizeClassificationScheme:1.0.0](reference-data/GrainSizeClassificationScheme.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--HeaderKeyName:1.0.0](reference-data/HeaderKeyName.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--IjkCellFace:1.0.0](reference-data/IjkCellFace.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ImageLightingCondition:1.0.0](reference-data/ImageLightingCondition.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--IndexableElement:1.0.0](reference-data/IndexableElement.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--InSARApplication:1.0.0](reference-data/InSARApplication.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--InSARFrequencyBand:1.0.0](reference-data/InSARFrequencyBand.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--InSARImageMode:1.0.0](reference-data/InSARImageMode.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--InSARPolarisation:1.0.0](reference-data/InSARPolarisation.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--InSARProcessingType:1.0.0](reference-data/InSARProcessingType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--InterpolationMethod:1.0.0](reference-data/InterpolationMethod.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--IsolatedIntervalType:1.0.0](reference-data/IsolatedIntervalType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--KDirectionType:1.0.0](reference-data/KDirectionType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LaheeClass:1.0.0](reference-data/LaheeClass.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LegalStatus:1.0.0](reference-data/LegalStatus.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LicenseState:1.0.0](reference-data/LicenseState.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LineageRelationshipType:1.0.0](reference-data/LineageRelationshipType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LinerType:1.0.0](reference-data/LinerType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LithologyType:1.0.0](reference-data/LithologyType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LithoStratigraphy:1.0.0](reference-data/LithoStratigraphy.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LogCurveBusinessValue:1.0.0](reference-data/LogCurveBusinessValue.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LogCurveFamily:1.0.0](reference-data/LogCurveFamily.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LogCurveFamily:1.1.0](reference-data/LogCurveFamily.1.1.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LogCurveMainFamily:1.0.0](reference-data/LogCurveMainFamily.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LogCurveType:1.0.0](reference-data/LogCurveType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LogCurveType:1.1.0](reference-data/LogCurveType.1.1.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--LogType:1.0.0](reference-data/LogType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--MarkerPropertyType:1.0.0](reference-data/MarkerPropertyType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--MarkerType:1.0.0](reference-data/MarkerType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--MarkerType:1.0.1](reference-data/MarkerType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--MaterialType:1.0.0](reference-data/MaterialType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--MudBaseType:1.0.0](reference-data/MudBaseType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--MudClass:1.0.0](reference-data/MudClass.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--OAuth2FlowType:1.0.0](reference-data/OAuth2FlowType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ObjectiveType:1.0.0](reference-data/ObjectiveType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ObligationType:1.0.0](reference-data/ObligationType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--OperatingEnvironment:1.0.0](reference-data/OperatingEnvironment.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--OrderingCriteriaType:1.0.0](reference-data/OrderingCriteriaType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--OrganisationType:1.0.0](reference-data/OrganisationType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--OSDUJsonExtensions:1.0.0](reference-data/OSDUJsonExtensions.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--OSDURegion:1.0.0](reference-data/OSDURegion.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ParameterKind:1.0.0](reference-data/ParameterKind.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ParameterRole:1.0.0](reference-data/ParameterRole.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ParameterType:1.0.0](reference-data/ParameterType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationCentralizationMethodType:1.0.0](reference-data/PerforationCentralizationMethodType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationConveyedMethod:1.0.0](reference-data/PerforationConveyedMethod.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationGunCarrierCategory:1.0.0](reference-data/PerforationGunCarrierCategory.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationGunCarrierModel:1.0.0](reference-data/PerforationGunCarrierModel.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationGunCarrierType:1.0.0](reference-data/PerforationGunCarrierType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationGunChargeShape:1.0.0](reference-data/PerforationGunChargeShape.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationGunChargeSize:1.0.0](reference-data/PerforationGunChargeSize.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationGunChargeType:1.0.0](reference-data/PerforationGunChargeType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationGunFiringHeadType:1.0.0](reference-data/PerforationGunFiringHeadType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationGunMetallurgyType:1.0.0](reference-data/PerforationGunMetallurgyType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationGunPhasingType:1.0.0](reference-data/PerforationGunPhasingType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationIntervalReason:1.0.0](reference-data/PerforationIntervalReason.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationIntervalType:1.0.0](reference-data/PerforationIntervalType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationIntervalWLSize:1.0.0](reference-data/PerforationIntervalWLSize.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PerforationPillType:1.0.0](reference-data/PerforationPillType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PermeabilityMeasurementType:1.0.0](reference-data/PermeabilityMeasurementType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PersonnelOrganisationRole:1.0.0](reference-data/PersonnelOrganisationRole.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PersonnelServiceType:1.0.0](reference-data/PersonnelServiceType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PetroleumSystemElementType:1.0.0](reference-data/PetroleumSystemElementType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PillarShapeType:1.0.0](reference-data/PillarShapeType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PlayType:1.0.0](reference-data/PlayType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PlugType:1.0.0](reference-data/PlugType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PorosityMeasurementType:1.0.0](reference-data/PorosityMeasurementType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PPFGContextType:1.0.0](reference-data/PPFGContextType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PPFGCurveFamily:1.0.0](reference-data/PPFGCurveFamily.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PPFGCurveLithoType:1.0.0](reference-data/PPFGCurveLithoType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PPFGCurveMainFamily:1.0.0](reference-data/PPFGCurveMainFamily.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PPFGCurveMnemonic:1.0.0](reference-data/PPFGCurveMnemonic.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PPFGCurveProbability:1.0.0](reference-data/PPFGCurveProbability.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PPFGCurveProcessingType:1.0.0](reference-data/PPFGCurveProcessingType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PPFGCurveTransformModelType:1.0.0](reference-data/PPFGCurveTransformModelType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PressureMeasurementType:1.0.0](reference-data/PressureMeasurementType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ProcessingParameterType:1.0.0](reference-data/ProcessingParameterType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ProjectRole:1.0.0](reference-data/ProjectRole.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ProjectStateType:1.0.0](reference-data/ProjectStateType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PropertyFieldRepresentationType:1.0.0](reference-data/PropertyFieldRepresentationType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PropertyNameType:1.0.0](reference-data/PropertyNameType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PropertyType:1.0.0](reference-data/PropertyType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ProspectType:1.0.0](reference-data/ProspectType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--PumpOpType:1.0.0](reference-data/PumpOpType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--QualitativeSpatialAccuracyType:1.0.0](reference-data/QualitativeSpatialAccuracyType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--QualityDataRule:1.0.0](reference-data/QualityDataRule.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--QualityDataRule:1.1.0](reference-data/QualityDataRule.1.1.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--QualityDataRuleSet:1.0.0](reference-data/QualityDataRuleSet.1.0.0.md)|data.DataRules[]|flattened|-|
|&rarr; [osdu:wks:reference-data--QualityDataRuleSet:2.0.0](reference-data/QualityDataRuleSet.2.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--QualityDataRuleSet:2.0.0](reference-data/QualityDataRuleSet.2.0.0.md)|data.DataRules[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--QuantitativeAccuracyBand:1.0.0](reference-data/QuantitativeAccuracyBand.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ReasonTripType:1.0.0](reference-data/ReasonTripType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ReferenceValueUpgradeLookUp:1.0.0](reference-data/ReferenceValueUpgradeLookUp.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RepresentationRole:1.0.0](reference-data/RepresentationRole.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RepresentationType:1.0.0](reference-data/RepresentationType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ResourceCurationStatus:1.0.0](reference-data/ResourceCurationStatus.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ResourceLifecycleStatus:1.0.0](reference-data/ResourceLifecycleStatus.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ResourceSecurityClassification:1.0.0](reference-data/ResourceSecurityClassification.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RigType:1.0.0](reference-data/RigType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RiskCategory:1.0.0](reference-data/RiskCategory.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RiskConsequenceCategory:1.0.0](reference-data/RiskConsequenceCategory.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RiskConsequenceSubCategory:1.0.0](reference-data/RiskConsequenceSubCategory.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RiskDiscipline:1.0.0](reference-data/RiskDiscipline.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RiskHierarchyLevel:1.0.0](reference-data/RiskHierarchyLevel.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RiskResponseStatus:1.0.0](reference-data/RiskResponseStatus.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RiskSubCategory:1.0.0](reference-data/RiskSubCategory.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RiskType:1.0.0](reference-data/RiskType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RockImageType:1.0.0](reference-data/RockImageType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RockSampleAnalysisType:1.0.0](reference-data/RockSampleAnalysisType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--RockSampleType:1.0.0](reference-data/RockSampleType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SampleOrientationType:1.0.0](reference-data/SampleOrientationType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SatelliteGeometry:1.0.0](reference-data/SatelliteGeometry.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SatelliteMission:1.0.0](reference-data/SatelliteMission.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SatelliteMission:1.0.0](reference-data/SatelliteMission.1.0.0.md)|data.OperationalPeriod[]|nested|-|
|&rarr; [osdu:wks:reference-data--SaturationMethodType:1.0.0](reference-data/SaturationMethodType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SchemaFormatType:1.0.0](reference-data/SchemaFormatType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SchemaUpgradeChain:1.0.0](reference-data/SchemaUpgradeChain.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SchemaUpgradeSpecification:1.0.0](reference-data/SchemaUpgradeSpecification.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SchemaUpgradeSpecification:1.0.0](reference-data/SchemaUpgradeSpecification.1.0.0.md)|data.JoltConfigurations[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--SectionType:1.0.0](reference-data/SectionType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SecuritySchemeType:1.0.0](reference-data/SecuritySchemeType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SEGY-HeaderMappingTemplate:1.0.0](reference-data/SEGY-HeaderMappingTemplate.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SEGY-HeaderMappingTemplate:1.0.0](reference-data/SEGY-HeaderMappingTemplate.1.0.0.md)|data.VectorHeaderMapping[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--SeismicAcquisitionType:1.0.0](reference-data/SeismicAcquisitionType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicAttributeType:1.0.0](reference-data/SeismicAttributeType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicBinGridType:1.0.0](reference-data/SeismicBinGridType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicDomainType:1.0.0](reference-data/SeismicDomainType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicEnergySourceType:1.0.0](reference-data/SeismicEnergySourceType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicEnergySourceType:1.0.1](reference-data/SeismicEnergySourceType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicFaultType:1.0.0](reference-data/SeismicFaultType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicFilteringType:1.0.0](reference-data/SeismicFilteringType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicGatherType:1.0.0](reference-data/SeismicGatherType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicGeometryType:1.0.0](reference-data/SeismicGeometryType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicHorizonType:1.0.0](reference-data/SeismicHorizonType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicMigrationType:1.0.0](reference-data/SeismicMigrationType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicMigrationType:1.0.1](reference-data/SeismicMigrationType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicPhase:1.0.0](reference-data/SeismicPhase.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicPickingType:1.0.0](reference-data/SeismicPickingType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicPickingType:1.0.1](reference-data/SeismicPickingType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicPolarity:1.0.0](reference-data/SeismicPolarity.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicProcessingStageType:1.0.0](reference-data/SeismicProcessingStageType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicProcessingStageType:1.0.1](reference-data/SeismicProcessingStageType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicReceiverType:1.0.0](reference-data/SeismicReceiverType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicStackingType:1.0.0](reference-data/SeismicStackingType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicTraceDataDimensionalityType:1.0.0](reference-data/SeismicTraceDataDimensionalityType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicTraceSortOrder:1.0.0](reference-data/SeismicTraceSortOrder.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicWaveType:1.0.0](reference-data/SeismicWaveType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SeismicWaveType:1.0.1](reference-data/SeismicWaveType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SequenceStratigraphicSchemaType:1.0.0](reference-data/SequenceStratigraphicSchemaType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SequenceStratigraphySurfaceType:1.0.0](reference-data/SequenceStratigraphySurfaceType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SidewallCoreType:1.0.0](reference-data/SidewallCoreType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SpatialGeometryType:1.0.0](reference-data/SpatialGeometryType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SpatialParameterType:1.0.0](reference-data/SpatialParameterType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--StandardsOrganisation:1.0.0](reference-data/StandardsOrganisation.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--StratigraphicColumnRankUnitType:1.0.0](reference-data/StratigraphicColumnRankUnitType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--StratigraphicColumnValidityAreaType:1.0.0](reference-data/StratigraphicColumnValidityAreaType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--StratigraphicRoleType:1.0.0](reference-data/StratigraphicRoleType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--StringClass:1.0.0](reference-data/StringClass.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--SurveyToolType:1.0.0](reference-data/SurveyToolType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TargetShape:1.0.0](reference-data/TargetShape.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TargetType:1.0.0](reference-data/TargetType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TechnicalAssuranceType:1.0.0](reference-data/TechnicalAssuranceType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TectonicSettingType:1.0.0](reference-data/TectonicSettingType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TestSubType:1.0.0](reference-data/TestSubType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TestType:1.0.0](reference-data/TestType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TrajectoryStationPropertyType:1.0.0](reference-data/TrajectoryStationPropertyType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TubularAssemblyStatusType:1.0.0](reference-data/TubularAssemblyStatusType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TubularAssemblyType:1.0.0](reference-data/TubularAssemblyType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TubularComponentConnectionType:1.0.0](reference-data/TubularComponentConnectionType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TubularComponentGrade:1.0.0](reference-data/TubularComponentGrade.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TubularComponentPinBoxType:1.0.0](reference-data/TubularComponentPinBoxType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TubularComponentType:1.0.0](reference-data/TubularComponentType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TubularMaterialType:1.0.0](reference-data/TubularMaterialType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TubularUmbilicalServiceType:1.0.0](reference-data/TubularUmbilicalServiceType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--TubularUmbilicalType:1.0.0](reference-data/TubularUmbilicalType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--UnitOfMeasure:1.0.0](reference-data/UnitOfMeasure.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--UnitOfMeasureConfiguration:1.0.0](reference-data/UnitOfMeasureConfiguration.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--UnitOfMeasureConfiguration:1.0.0](reference-data/UnitOfMeasureConfiguration.1.0.0.md)|data.Configurations[]|object (default)|-|
|&rarr; [osdu:wks:reference-data--UnitQuantity:1.0.0](reference-data/UnitQuantity.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--ValueChainStatusType:1.0.0](reference-data/ValueChainStatusType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VelocityAnalysisMethod:1.0.0](reference-data/VelocityAnalysisMethod.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VelocityAnalysisMethod:1.0.1](reference-data/VelocityAnalysisMethod.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VelocityDirectionType:1.0.0](reference-data/VelocityDirectionType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VelocityType:1.0.0](reference-data/VelocityType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VerticalMeasurementPath:1.0.0](reference-data/VerticalMeasurementPath.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VerticalMeasurementPath:1.0.1](reference-data/VerticalMeasurementPath.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VerticalMeasurementSource:1.0.0](reference-data/VerticalMeasurementSource.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VerticalMeasurementType:1.0.0](reference-data/VerticalMeasurementType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VerticalMeasurementType:1.0.1](reference-data/VerticalMeasurementType.1.0.1.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--VerticalMeasurementType:1.0.2](reference-data/VerticalMeasurementType.1.0.2.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WeatherType:1.0.0](reference-data/WeatherType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellActivityPhaseType:1.0.0](reference-data/WellActivityPhaseType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellActivityProgramType:1.0.0](reference-data/WellActivityProgramType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellActivityType:1.0.0](reference-data/WellActivityType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellboreOpeningStateType:1.0.0](reference-data/WellboreOpeningStateType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellboreReason:1.0.0](reference-data/WellboreReason.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellboreTrajectoryType:1.0.0](reference-data/WellboreTrajectoryType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellBusinessIntention:1.0.0](reference-data/WellBusinessIntention.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellBusinessIntentionOutcome:1.0.0](reference-data/WellBusinessIntentionOutcome.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellCondition:1.0.0](reference-data/WellCondition.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellFluidDirection:1.0.0](reference-data/WellFluidDirection.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellheadConnection:1.0.0](reference-data/WellheadConnection.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellInterestType:1.0.0](reference-data/WellInterestType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellLogSamplingDomainType:1.0.0](reference-data/WellLogSamplingDomainType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellProductType:1.0.0](reference-data/WellProductType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellRole:1.0.0](reference-data/WellRole.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellSiteProductType:1.0.0](reference-data/WellSiteProductType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellStatusSummary:1.0.0](reference-data/WellStatusSummary.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WellTechnologyApplied:1.0.0](reference-data/WellTechnologyApplied.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WordFormatType:1.0.0](reference-data/WordFormatType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WorkflowPersonaType:1.0.0](reference-data/WorkflowPersonaType.1.0.0.md)|data.NameAlias[]|nested|-|
|&rarr; [osdu:wks:reference-data--WorkflowUsageType:1.0.0](reference-data/WorkflowUsageType.1.0.0.md)|data.NameAlias[]|nested|-|

[Back to TOC](#TOC)

## Group-Type work-product

|Kind|Cumulative Name|Indexer Hint|Comment|
|----|----|----|----|
|&rarr; [osdu:wks:work-product--GenericWorkProduct:1.0.0](manifest/GenericWorkProduct.1.0.0.md)|data.LineageAssertions[]|object (default)|-|
|&rarr; [osdu:wks:work-product--WorkProduct:1.0.0](work-product/WorkProduct.1.0.0.md)|data.LineageAssertions[]|object (default)|-|

[Back to TOC](#TOC)

## Group-Type work-product-component

|Kind|Cumulative Name|Indexer Hint|Comment|
|----|----|----|----|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.0.0](work-product-component/Activity.1.0.0.md)|data.SoftwareSpecifications[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.SoftwareSpecifications[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Activity:1.1.0](work-product-component/Activity.1.1.0.md)|data.ActivityStates[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--AquiferInterpretation:1.0.0](work-product-component/AquiferInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--AquiferInterpretation:1.0.0](work-product-component/AquiferInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--AquiferInterpretation:1.0.0](work-product-component/AquiferInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--AquiferInterpretation:1.0.0](work-product-component/AquiferInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--AquiferInterpretation:1.0.0](work-product-component/AquiferInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--AquiferInterpretation:1.0.0](work-product-component/AquiferInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--AquiferInterpretation:1.0.0](work-product-component/AquiferInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.KeyColumns[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.KeyColumns[].FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.Columns[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.Columns[].FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--ColumnBasedTable:1.0.0](work-product-component/ColumnBasedTable.1.0.0.md)|data.ColumnValues[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.0.0](work-product-component/DataQuality.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.0.0](work-product-component/DataQuality.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.0.0](work-product-component/DataQuality.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.0.0](work-product-component/DataQuality.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.0.0](work-product-component/DataQuality.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.0.0](work-product-component/DataQuality.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.0.0](work-product-component/DataQuality.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.0.0](work-product-component/DataQuality.1.0.0.md)|data.BusinessRules.DataRuleSets[]|nested|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.0.0](work-product-component/DataQuality.1.0.0.md)|data.BusinessRules.DataRules[]|nested|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.1.0](work-product-component/DataQuality.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.1.0](work-product-component/DataQuality.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.1.0](work-product-component/DataQuality.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.1.0](work-product-component/DataQuality.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.1.0](work-product-component/DataQuality.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.1.0](work-product-component/DataQuality.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.1.0](work-product-component/DataQuality.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.1.0](work-product-component/DataQuality.1.1.0.md)|data.BusinessRules.DataRuleSets[]|nested|-|
|&rarr; [osdu:wks:work-product-component--DataQuality:1.1.0](work-product-component/DataQuality.1.1.0.md)|data.BusinessRules.DataRules[]|nested|-|
|&rarr; [osdu:wks:work-product-component--Document:1.0.0](work-product-component/Document.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Document:1.0.0](work-product-component/Document.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--Document:1.0.0](work-product-component/Document.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--Document:1.0.0](work-product-component/Document.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Document:1.0.0](work-product-component/Document.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--Document:1.0.0](work-product-component/Document.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--Document:1.0.0](work-product-component/Document.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--EarthModelInterpretation:1.0.0](work-product-component/EarthModelInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--EarthModelInterpretation:1.0.0](work-product-component/EarthModelInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--EarthModelInterpretation:1.0.0](work-product-component/EarthModelInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--EarthModelInterpretation:1.0.0](work-product-component/EarthModelInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--EarthModelInterpretation:1.0.0](work-product-component/EarthModelInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--EarthModelInterpretation:1.0.0](work-product-component/EarthModelInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--EarthModelInterpretation:1.0.0](work-product-component/EarthModelInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultInterpretation:1.0.0](work-product-component/FaultInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultInterpretation:1.0.0](work-product-component/FaultInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultInterpretation:1.0.0](work-product-component/FaultInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--FaultInterpretation:1.0.0](work-product-component/FaultInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultInterpretation:1.0.0](work-product-component/FaultInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultInterpretation:1.0.0](work-product-component/FaultInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultInterpretation:1.0.0](work-product-component/FaultInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultInterpretation:1.0.0](work-product-component/FaultInterpretation.1.0.0.md)|data.FaultThrowDescriptions[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.0.0](work-product-component/FaultSystem.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.0.0](work-product-component/FaultSystem.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.0.0](work-product-component/FaultSystem.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.0.0](work-product-component/FaultSystem.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.0.0](work-product-component/FaultSystem.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.0.0](work-product-component/FaultSystem.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.0.0](work-product-component/FaultSystem.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.0.0](work-product-component/FaultSystem.1.0.0.md)|data.Faults[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.1.0](work-product-component/FaultSystem.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.1.0](work-product-component/FaultSystem.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.1.0](work-product-component/FaultSystem.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.1.0](work-product-component/FaultSystem.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.1.0](work-product-component/FaultSystem.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.1.0](work-product-component/FaultSystem.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.1.0](work-product-component/FaultSystem.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.1.0](work-product-component/FaultSystem.1.1.0.md)|data.Faults[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.2.0](work-product-component/FaultSystem.1.2.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.2.0](work-product-component/FaultSystem.1.2.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.2.0](work-product-component/FaultSystem.1.2.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.2.0](work-product-component/FaultSystem.1.2.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.2.0](work-product-component/FaultSystem.1.2.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.2.0](work-product-component/FaultSystem.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.2.0](work-product-component/FaultSystem.1.2.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FaultSystem:1.2.0](work-product-component/FaultSystem.1.2.0.md)|data.Faults[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FluidBoundaryInterpretation:1.0.0](work-product-component/FluidBoundaryInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FluidBoundaryInterpretation:1.0.0](work-product-component/FluidBoundaryInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FluidBoundaryInterpretation:1.0.0](work-product-component/FluidBoundaryInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--FluidBoundaryInterpretation:1.0.0](work-product-component/FluidBoundaryInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FluidBoundaryInterpretation:1.0.0](work-product-component/FluidBoundaryInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FluidBoundaryInterpretation:1.0.0](work-product-component/FluidBoundaryInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FluidBoundaryInterpretation:1.0.0](work-product-component/FluidBoundaryInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FormationIntegrityTest:1.0.0](work-product-component/FormationIntegrityTest.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FormationIntegrityTest:1.0.0](work-product-component/FormationIntegrityTest.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FormationIntegrityTest:1.0.0](work-product-component/FormationIntegrityTest.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--FormationIntegrityTest:1.0.0](work-product-component/FormationIntegrityTest.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FormationIntegrityTest:1.0.0](work-product-component/FormationIntegrityTest.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FormationIntegrityTest:1.0.0](work-product-component/FormationIntegrityTest.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--FormationIntegrityTest:1.0.0](work-product-component/FormationIntegrityTest.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--FormationIntegrityTest:1.0.0](work-product-component/FormationIntegrityTest.1.0.0.md)|data.VolumePressureResponses[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GenericImage:1.0.0](work-product-component/GenericImage.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericImage:1.0.0](work-product-component/GenericImage.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GenericImage:1.0.0](work-product-component/GenericImage.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GenericImage:1.0.0](work-product-component/GenericImage.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericImage:1.0.0](work-product-component/GenericImage.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericImage:1.0.0](work-product-component/GenericImage.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GenericImage:1.0.0](work-product-component/GenericImage.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericProperty:1.0.0](work-product-component/GenericProperty.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericProperty:1.0.0](work-product-component/GenericProperty.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GenericProperty:1.0.0](work-product-component/GenericProperty.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GenericProperty:1.0.0](work-product-component/GenericProperty.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericProperty:1.0.0](work-product-component/GenericProperty.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericProperty:1.0.0](work-product-component/GenericProperty.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GenericProperty:1.0.0](work-product-component/GenericProperty.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericProperty:1.0.0](work-product-component/GenericProperty.1.0.0.md)|data.FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GenericRepresentation:1.0.0](work-product-component/GenericRepresentation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericRepresentation:1.0.0](work-product-component/GenericRepresentation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GenericRepresentation:1.0.0](work-product-component/GenericRepresentation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GenericRepresentation:1.0.0](work-product-component/GenericRepresentation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericRepresentation:1.0.0](work-product-component/GenericRepresentation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericRepresentation:1.0.0](work-product-component/GenericRepresentation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GenericRepresentation:1.0.0](work-product-component/GenericRepresentation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericRepresentation:1.0.0](work-product-component/GenericRepresentation.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GenericWorkProductComponent:1.0.0](manifest/GenericWorkProductComponent.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericWorkProductComponent:1.0.0](manifest/GenericWorkProductComponent.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GenericWorkProductComponent:1.0.0](manifest/GenericWorkProductComponent.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GenericWorkProductComponent:1.0.0](manifest/GenericWorkProductComponent.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GenericWorkProductComponent:1.0.0](manifest/GenericWorkProductComponent.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeobodyBoundaryInterpretation:1.0.0](work-product-component/GeobodyBoundaryInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeobodyBoundaryInterpretation:1.0.0](work-product-component/GeobodyBoundaryInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeobodyBoundaryInterpretation:1.0.0](work-product-component/GeobodyBoundaryInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GeobodyBoundaryInterpretation:1.0.0](work-product-component/GeobodyBoundaryInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeobodyBoundaryInterpretation:1.0.0](work-product-component/GeobodyBoundaryInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeobodyBoundaryInterpretation:1.0.0](work-product-component/GeobodyBoundaryInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeobodyBoundaryInterpretation:1.0.0](work-product-component/GeobodyBoundaryInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeobodyInterpretation:1.0.0](work-product-component/GeobodyInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeobodyInterpretation:1.0.0](work-product-component/GeobodyInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeobodyInterpretation:1.0.0](work-product-component/GeobodyInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GeobodyInterpretation:1.0.0](work-product-component/GeobodyInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeobodyInterpretation:1.0.0](work-product-component/GeobodyInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeobodyInterpretation:1.0.0](work-product-component/GeobodyInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeobodyInterpretation:1.0.0](work-product-component/GeobodyInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeologicUnitOccurrenceInterpretation:1.0.0](work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeologicUnitOccurrenceInterpretation:1.0.0](work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeologicUnitOccurrenceInterpretation:1.0.0](work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GeologicUnitOccurrenceInterpretation:1.0.0](work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeologicUnitOccurrenceInterpretation:1.0.0](work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeologicUnitOccurrenceInterpretation:1.0.0](work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeologicUnitOccurrenceInterpretation:1.0.0](work-product-component/GeologicUnitOccurrenceInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.0.0](work-product-component/GeoReferencedImage.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.0.0](work-product-component/GeoReferencedImage.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.0.0](work-product-component/GeoReferencedImage.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.0.0](work-product-component/GeoReferencedImage.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.0.0](work-product-component/GeoReferencedImage.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.0.0](work-product-component/GeoReferencedImage.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.0.0](work-product-component/GeoReferencedImage.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.0.0](work-product-component/GeoReferencedImage.1.0.0.md)|data.EmbeddedBinGrid.ABCDBinGridLocalCoordinates[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.1.0](work-product-component/GeoReferencedImage.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.1.0](work-product-component/GeoReferencedImage.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.1.0](work-product-component/GeoReferencedImage.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.1.0](work-product-component/GeoReferencedImage.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.1.0](work-product-component/GeoReferencedImage.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.1.0](work-product-component/GeoReferencedImage.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.1.0](work-product-component/GeoReferencedImage.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GeoReferencedImage:1.1.0](work-product-component/GeoReferencedImage.1.1.0.md)|data.EmbeddedBinGrid.ABCDBinGridLocalCoordinates[]|object (default)|Deprecated|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.IjkGridPatches[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.UnstructuredColumnLayerGridPatches[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GpGridRepresentation:1.0.0](work-product-component/GpGridRepresentation.1.0.0.md)|data.UnstructuredGridPatches[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0](work-product-component/GridConnectionSetRepresentation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0](work-product-component/GridConnectionSetRepresentation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0](work-product-component/GridConnectionSetRepresentation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0](work-product-component/GridConnectionSetRepresentation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0](work-product-component/GridConnectionSetRepresentation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0](work-product-component/GridConnectionSetRepresentation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0](work-product-component/GridConnectionSetRepresentation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--GridConnectionSetRepresentation:1.0.0](work-product-component/GridConnectionSetRepresentation.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--HorizonInterpretation:1.0.0](work-product-component/HorizonInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--HorizonInterpretation:1.0.0](work-product-component/HorizonInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--HorizonInterpretation:1.0.0](work-product-component/HorizonInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--HorizonInterpretation:1.0.0](work-product-component/HorizonInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--HorizonInterpretation:1.0.0](work-product-component/HorizonInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--HorizonInterpretation:1.0.0](work-product-component/HorizonInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--HorizonInterpretation:1.0.0](work-product-component/HorizonInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.SingleCellAquiferSet[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--IjkGridNumericalAquiferRepresentation:1.0.0](work-product-component/IjkGridNumericalAquiferRepresentation.1.0.0.md)|data.ConnectionSet[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--IjkGridRepresentation:1.0.0](work-product-component/IjkGridRepresentation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--IjkGridRepresentation:1.0.0](work-product-component/IjkGridRepresentation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--IjkGridRepresentation:1.0.0](work-product-component/IjkGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--IjkGridRepresentation:1.0.0](work-product-component/IjkGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--IjkGridRepresentation:1.0.0](work-product-component/IjkGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--IjkGridRepresentation:1.0.0](work-product-component/IjkGridRepresentation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--IjkGridRepresentation:1.0.0](work-product-component/IjkGridRepresentation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--IjkGridRepresentation:1.0.0](work-product-component/IjkGridRepresentation.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--LocalBoundaryFeature:1.0.0](work-product-component/LocalBoundaryFeature.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalBoundaryFeature:1.0.0](work-product-component/LocalBoundaryFeature.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--LocalBoundaryFeature:1.0.0](work-product-component/LocalBoundaryFeature.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--LocalBoundaryFeature:1.0.0](work-product-component/LocalBoundaryFeature.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalBoundaryFeature:1.0.0](work-product-component/LocalBoundaryFeature.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalBoundaryFeature:1.0.0](work-product-component/LocalBoundaryFeature.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--LocalBoundaryFeature:1.0.0](work-product-component/LocalBoundaryFeature.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalModelCompoundCrs:1.0.0](work-product-component/LocalModelCompoundCrs.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalModelCompoundCrs:1.0.0](work-product-component/LocalModelCompoundCrs.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--LocalModelCompoundCrs:1.0.0](work-product-component/LocalModelCompoundCrs.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--LocalModelCompoundCrs:1.0.0](work-product-component/LocalModelCompoundCrs.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalModelCompoundCrs:1.0.0](work-product-component/LocalModelCompoundCrs.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalModelCompoundCrs:1.0.0](work-product-component/LocalModelCompoundCrs.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--LocalModelCompoundCrs:1.0.0](work-product-component/LocalModelCompoundCrs.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalModelFeature:1.0.0](work-product-component/LocalModelFeature.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalModelFeature:1.0.0](work-product-component/LocalModelFeature.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--LocalModelFeature:1.0.0](work-product-component/LocalModelFeature.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--LocalModelFeature:1.0.0](work-product-component/LocalModelFeature.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalModelFeature:1.0.0](work-product-component/LocalModelFeature.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalModelFeature:1.0.0](work-product-component/LocalModelFeature.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--LocalModelFeature:1.0.0](work-product-component/LocalModelFeature.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalRockVolumeFeature:1.0.0](work-product-component/LocalRockVolumeFeature.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalRockVolumeFeature:1.0.0](work-product-component/LocalRockVolumeFeature.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--LocalRockVolumeFeature:1.0.0](work-product-component/LocalRockVolumeFeature.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--LocalRockVolumeFeature:1.0.0](work-product-component/LocalRockVolumeFeature.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalRockVolumeFeature:1.0.0](work-product-component/LocalRockVolumeFeature.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--LocalRockVolumeFeature:1.0.0](work-product-component/LocalRockVolumeFeature.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--LocalRockVolumeFeature:1.0.0](work-product-component/LocalRockVolumeFeature.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--NotionalSeismicLine:1.0.0](work-product-component/NotionalSeismicLine.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--NotionalSeismicLine:1.0.0](work-product-component/NotionalSeismicLine.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--NotionalSeismicLine:1.0.0](work-product-component/NotionalSeismicLine.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--NotionalSeismicLine:1.0.0](work-product-component/NotionalSeismicLine.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--NotionalSeismicLine:1.0.0](work-product-component/NotionalSeismicLine.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--NotionalSeismicLine:1.0.0](work-product-component/NotionalSeismicLine.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--NotionalSeismicLine:1.0.0](work-product-component/NotionalSeismicLine.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PersistedCollection:1.0.0](work-product-component/PersistedCollection.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PersistedCollection:1.0.0](work-product-component/PersistedCollection.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--PersistedCollection:1.0.0](work-product-component/PersistedCollection.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--PersistedCollection:1.0.0](work-product-component/PersistedCollection.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PersistedCollection:1.0.0](work-product-component/PersistedCollection.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PersistedCollection:1.0.0](work-product-component/PersistedCollection.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--PersistedCollection:1.0.0](work-product-component/PersistedCollection.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PlannedLithology:1.0.0](work-product-component/PlannedLithology.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PlannedLithology:1.0.0](work-product-component/PlannedLithology.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--PlannedLithology:1.0.0](work-product-component/PlannedLithology.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--PlannedLithology:1.0.0](work-product-component/PlannedLithology.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PlannedLithology:1.0.0](work-product-component/PlannedLithology.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PlannedLithology:1.0.0](work-product-component/PlannedLithology.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--PlannedLithology:1.0.0](work-product-component/PlannedLithology.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PlannedLithology:1.0.0](work-product-component/PlannedLithology.1.0.0.md)|data.GeologyIntervals[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--PPFGDataset:1.0.0](work-product-component/PPFGDataset.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PPFGDataset:1.0.0](work-product-component/PPFGDataset.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--PPFGDataset:1.0.0](work-product-component/PPFGDataset.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--PPFGDataset:1.0.0](work-product-component/PPFGDataset.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PPFGDataset:1.0.0](work-product-component/PPFGDataset.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PPFGDataset:1.0.0](work-product-component/PPFGDataset.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--PPFGDataset:1.0.0](work-product-component/PPFGDataset.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--PPFGDataset:1.0.0](work-product-component/PPFGDataset.1.0.0.md)|data.Curves[]|nested|-|
|&rarr; [osdu:wks:work-product-component--ProcessedInSAR:1.0.0](work-product-component/ProcessedInSAR.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ProcessedInSAR:1.0.0](work-product-component/ProcessedInSAR.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--ProcessedInSAR:1.0.0](work-product-component/ProcessedInSAR.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--ProcessedInSAR:1.0.0](work-product-component/ProcessedInSAR.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ProcessedInSAR:1.0.0](work-product-component/ProcessedInSAR.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ProcessedInSAR:1.0.0](work-product-component/ProcessedInSAR.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--ProcessedInSAR:1.0.0](work-product-component/ProcessedInSAR.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ProcessedInSAR:1.0.0](work-product-component/ProcessedInSAR.1.0.0.md)|data.AcquisitionDateRanges[]|nested|-|
|&rarr; [osdu:wks:work-product-component--ReservoirCompartmentInterpretation:1.0.0](work-product-component/ReservoirCompartmentInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ReservoirCompartmentInterpretation:1.0.0](work-product-component/ReservoirCompartmentInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--ReservoirCompartmentInterpretation:1.0.0](work-product-component/ReservoirCompartmentInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--ReservoirCompartmentInterpretation:1.0.0](work-product-component/ReservoirCompartmentInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ReservoirCompartmentInterpretation:1.0.0](work-product-component/ReservoirCompartmentInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ReservoirCompartmentInterpretation:1.0.0](work-product-component/ReservoirCompartmentInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--ReservoirCompartmentInterpretation:1.0.0](work-product-component/ReservoirCompartmentInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--ReservoirCompartmentInterpretation:1.0.0](work-product-component/ReservoirCompartmentInterpretation.1.0.0.md)|data.ReservoirCompartmentUnits[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockFluidOrganizationInterpretation:1.0.0](work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockFluidOrganizationInterpretation:1.0.0](work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockFluidOrganizationInterpretation:1.0.0](work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--RockFluidOrganizationInterpretation:1.0.0](work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockFluidOrganizationInterpretation:1.0.0](work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockFluidOrganizationInterpretation:1.0.0](work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockFluidOrganizationInterpretation:1.0.0](work-product-component/RockFluidOrganizationInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockFluidUnitInterpretation:1.0.0](work-product-component/RockFluidUnitInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockFluidUnitInterpretation:1.0.0](work-product-component/RockFluidUnitInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockFluidUnitInterpretation:1.0.0](work-product-component/RockFluidUnitInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--RockFluidUnitInterpretation:1.0.0](work-product-component/RockFluidUnitInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockFluidUnitInterpretation:1.0.0](work-product-component/RockFluidUnitInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockFluidUnitInterpretation:1.0.0](work-product-component/RockFluidUnitInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockFluidUnitInterpretation:1.0.0](work-product-component/RockFluidUnitInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockImage:1.0.0](work-product-component/RockImage.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockImage:1.0.0](work-product-component/RockImage.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockImage:1.0.0](work-product-component/RockImage.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--RockImage:1.0.0](work-product-component/RockImage.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockImage:1.0.0](work-product-component/RockImage.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockImage:1.0.0](work-product-component/RockImage.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockImage:1.0.0](work-product-component/RockImage.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.SoftwareSpecifications[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.RoutineCoreAnalysis.RCAMeasurements[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.RoutineCoreAnalysis.RCAMeasurements[].OtherMeasurements[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.RoutineCoreAnalysis.RCAMeasurements[].Remarks[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.0.0](work-product-component/RockSampleAnalysis.1.0.0.md)|data.Remarks[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.Parameters[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.Parameters[].Keys[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.SoftwareSpecifications[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.RoutineCoreAnalysis.RCAMeasurements[]|nested|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.RoutineCoreAnalysis.RCAMeasurements[].OtherMeasurements[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.RoutineCoreAnalysis.RCAMeasurements[].Remarks[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--RockSampleAnalysis:1.1.0](work-product-component/RockSampleAnalysis.1.1.0.md)|data.Remarks[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0](work-product-component/SealedSurfaceFramework.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0](work-product-component/SealedSurfaceFramework.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0](work-product-component/SealedSurfaceFramework.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0](work-product-component/SealedSurfaceFramework.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0](work-product-component/SealedSurfaceFramework.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0](work-product-component/SealedSurfaceFramework.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0](work-product-component/SealedSurfaceFramework.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SealedSurfaceFramework:1.0.0](work-product-component/SealedSurfaceFramework.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SealedVolumeFramework:1.0.0](work-product-component/SealedVolumeFramework.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SealedVolumeFramework:1.0.0](work-product-component/SealedVolumeFramework.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SealedVolumeFramework:1.0.0](work-product-component/SealedVolumeFramework.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SealedVolumeFramework:1.0.0](work-product-component/SealedVolumeFramework.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SealedVolumeFramework:1.0.0](work-product-component/SealedVolumeFramework.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SealedVolumeFramework:1.0.0](work-product-component/SealedVolumeFramework.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SealedVolumeFramework:1.0.0](work-product-component/SealedVolumeFramework.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SealedVolumeFramework:1.0.0](work-product-component/SealedVolumeFramework.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.0.0](work-product-component/SeismicBinGrid.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.0.0](work-product-component/SeismicBinGrid.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.0.0](work-product-component/SeismicBinGrid.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.0.0](work-product-component/SeismicBinGrid.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.0.0](work-product-component/SeismicBinGrid.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.0.0](work-product-component/SeismicBinGrid.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.0.0](work-product-component/SeismicBinGrid.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.0.0](work-product-component/SeismicBinGrid.1.0.0.md)|data.ABCDBinGridLocalCoordinates[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.1.0](work-product-component/SeismicBinGrid.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.1.0](work-product-component/SeismicBinGrid.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.1.0](work-product-component/SeismicBinGrid.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.1.0](work-product-component/SeismicBinGrid.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.1.0](work-product-component/SeismicBinGrid.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.1.0](work-product-component/SeismicBinGrid.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.1.0](work-product-component/SeismicBinGrid.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicBinGrid:1.1.0](work-product-component/SeismicBinGrid.1.1.0.md)|data.ABCDBinGridLocalCoordinates[]|object (default)|Deprecated|
|&rarr; [osdu:wks:work-product-component--SeismicFault:1.0.0](work-product-component/SeismicFault.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicFault:1.0.0](work-product-component/SeismicFault.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicFault:1.0.0](work-product-component/SeismicFault.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicFault:1.0.0](work-product-component/SeismicFault.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicFault:1.0.0](work-product-component/SeismicFault.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicFault:1.0.0](work-product-component/SeismicFault.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicFault:1.0.0](work-product-component/SeismicFault.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicFault:1.0.0](work-product-component/SeismicFault.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.0.0](work-product-component/SeismicHorizon.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.0.0](work-product-component/SeismicHorizon.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.0.0](work-product-component/SeismicHorizon.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.0.0](work-product-component/SeismicHorizon.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.0.0](work-product-component/SeismicHorizon.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.0.0](work-product-component/SeismicHorizon.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.0.0](work-product-component/SeismicHorizon.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.0.0](work-product-component/SeismicHorizon.1.0.0.md)|data.SeismicAttributes[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.1.0](work-product-component/SeismicHorizon.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.1.0](work-product-component/SeismicHorizon.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.1.0](work-product-component/SeismicHorizon.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.1.0](work-product-component/SeismicHorizon.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.1.0](work-product-component/SeismicHorizon.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.1.0](work-product-component/SeismicHorizon.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.1.0](work-product-component/SeismicHorizon.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.1.0](work-product-component/SeismicHorizon.1.1.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicHorizon:1.1.0](work-product-component/SeismicHorizon.1.1.0.md)|data.SeismicAttributes[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicLineGeometry:1.0.0](work-product-component/SeismicLineGeometry.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicLineGeometry:1.0.0](work-product-component/SeismicLineGeometry.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicLineGeometry:1.0.0](work-product-component/SeismicLineGeometry.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicLineGeometry:1.0.0](work-product-component/SeismicLineGeometry.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicLineGeometry:1.0.0](work-product-component/SeismicLineGeometry.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicLineGeometry:1.0.0](work-product-component/SeismicLineGeometry.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicLineGeometry:1.0.0](work-product-component/SeismicLineGeometry.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.0.0](work-product-component/SeismicTraceData.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.0.0](work-product-component/SeismicTraceData.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.0.0](work-product-component/SeismicTraceData.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.0.0](work-product-component/SeismicTraceData.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.0.0](work-product-component/SeismicTraceData.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.0.0](work-product-component/SeismicTraceData.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.0.0](work-product-component/SeismicTraceData.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.0.0](work-product-component/SeismicTraceData.1.0.0.md)|data.ProcessingParameters[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.ProcessingParameters[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.StackAngleRanges[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.StackAzimuthRanges[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.1.0](work-product-component/SeismicTraceData.1.1.0.md)|data.StackOffsetRanges[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.ProcessingParameters[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.StackAngleRanges[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.StackAzimuthRanges[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.2.0](work-product-component/SeismicTraceData.1.2.0.md)|data.StackOffsetRanges[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.ProcessingParameters[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.StackAngleRanges[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.StackAzimuthRanges[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SeismicTraceData:1.3.0](work-product-component/SeismicTraceData.1.3.0.md)|data.StackOffsetRanges[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumn:1.0.0](work-product-component/StratigraphicColumn.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumn:1.0.0](work-product-component/StratigraphicColumn.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumn:1.0.0](work-product-component/StratigraphicColumn.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumn:1.0.0](work-product-component/StratigraphicColumn.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumn:1.0.0](work-product-component/StratigraphicColumn.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumn:1.0.0](work-product-component/StratigraphicColumn.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumn:1.0.0](work-product-component/StratigraphicColumn.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.0.0](work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.0.0](work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.0.0](work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.0.0](work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.0.0](work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.0.0](work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.0.0](work-product-component/StratigraphicColumnRankInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0](work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0](work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0](work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0](work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0](work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0](work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicColumnRankInterpretation:1.1.0](work-product-component/StratigraphicColumnRankInterpretation.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicUnitInterpretation:1.0.0](work-product-component/StratigraphicUnitInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicUnitInterpretation:1.0.0](work-product-component/StratigraphicUnitInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicUnitInterpretation:1.0.0](work-product-component/StratigraphicUnitInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicUnitInterpretation:1.0.0](work-product-component/StratigraphicUnitInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicUnitInterpretation:1.0.0](work-product-component/StratigraphicUnitInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicUnitInterpretation:1.0.0](work-product-component/StratigraphicUnitInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StratigraphicUnitInterpretation:1.0.0](work-product-component/StratigraphicUnitInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StructuralOrganizationInterpretation:1.0.0](work-product-component/StructuralOrganizationInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StructuralOrganizationInterpretation:1.0.0](work-product-component/StructuralOrganizationInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StructuralOrganizationInterpretation:1.0.0](work-product-component/StructuralOrganizationInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--StructuralOrganizationInterpretation:1.0.0](work-product-component/StructuralOrganizationInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StructuralOrganizationInterpretation:1.0.0](work-product-component/StructuralOrganizationInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--StructuralOrganizationInterpretation:1.0.0](work-product-component/StructuralOrganizationInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--StructuralOrganizationInterpretation:1.0.0](work-product-component/StructuralOrganizationInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SubRepresentation:1.0.0](work-product-component/SubRepresentation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SubRepresentation:1.0.0](work-product-component/SubRepresentation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SubRepresentation:1.0.0](work-product-component/SubRepresentation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--SubRepresentation:1.0.0](work-product-component/SubRepresentation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SubRepresentation:1.0.0](work-product-component/SubRepresentation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SubRepresentation:1.0.0](work-product-component/SubRepresentation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--SubRepresentation:1.0.0](work-product-component/SubRepresentation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--SubRepresentation:1.0.0](work-product-component/SubRepresentation.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--TimeSeries:1.0.0](work-product-component/TimeSeries.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TimeSeries:1.0.0](work-product-component/TimeSeries.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TimeSeries:1.0.0](work-product-component/TimeSeries.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--TimeSeries:1.0.0](work-product-component/TimeSeries.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TimeSeries:1.0.0](work-product-component/TimeSeries.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TimeSeries:1.0.0](work-product-component/TimeSeries.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TimeSeries:1.0.0](work-product-component/TimeSeries.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularAssembly:1.0.0](work-product-component/TubularAssembly.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularAssembly:1.0.0](work-product-component/TubularAssembly.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TubularAssembly:1.0.0](work-product-component/TubularAssembly.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--TubularAssembly:1.0.0](work-product-component/TubularAssembly.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularAssembly:1.0.0](work-product-component/TubularAssembly.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularAssembly:1.0.0](work-product-component/TubularAssembly.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TubularAssembly:1.0.0](work-product-component/TubularAssembly.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.0.0](work-product-component/TubularComponent.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.0.0](work-product-component/TubularComponent.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.0.0](work-product-component/TubularComponent.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.0.0](work-product-component/TubularComponent.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.0.0](work-product-component/TubularComponent.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.0.0](work-product-component/TubularComponent.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.0.0](work-product-component/TubularComponent.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.1.0](work-product-component/TubularComponent.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.1.0](work-product-component/TubularComponent.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.1.0](work-product-component/TubularComponent.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.1.0](work-product-component/TubularComponent.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.1.0](work-product-component/TubularComponent.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.1.0](work-product-component/TubularComponent.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TubularComponent:1.1.0](work-product-component/TubularComponent.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularUmbilical:1.0.0](work-product-component/TubularUmbilical.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularUmbilical:1.0.0](work-product-component/TubularUmbilical.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TubularUmbilical:1.0.0](work-product-component/TubularUmbilical.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--TubularUmbilical:1.0.0](work-product-component/TubularUmbilical.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularUmbilical:1.0.0](work-product-component/TubularUmbilical.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--TubularUmbilical:1.0.0](work-product-component/TubularUmbilical.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--TubularUmbilical:1.0.0](work-product-component/TubularUmbilical.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0](work-product-component/UnsealedSurfaceFramework.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0](work-product-component/UnsealedSurfaceFramework.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0](work-product-component/UnsealedSurfaceFramework.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0](work-product-component/UnsealedSurfaceFramework.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0](work-product-component/UnsealedSurfaceFramework.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0](work-product-component/UnsealedSurfaceFramework.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0](work-product-component/UnsealedSurfaceFramework.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.0.0](work-product-component/UnsealedSurfaceFramework.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0](work-product-component/UnsealedSurfaceFramework.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0](work-product-component/UnsealedSurfaceFramework.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0](work-product-component/UnsealedSurfaceFramework.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0](work-product-component/UnsealedSurfaceFramework.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0](work-product-component/UnsealedSurfaceFramework.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0](work-product-component/UnsealedSurfaceFramework.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0](work-product-component/UnsealedSurfaceFramework.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnsealedSurfaceFramework:1.1.0](work-product-component/UnsealedSurfaceFramework.1.1.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0](work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0](work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0](work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0](work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0](work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0](work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0](work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredColumnLayerGridRepresentation:1.0.0](work-product-component/UnstructuredColumnLayerGridRepresentation.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0](work-product-component/UnstructuredGridRepresentation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0](work-product-component/UnstructuredGridRepresentation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0](work-product-component/UnstructuredGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0](work-product-component/UnstructuredGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0](work-product-component/UnstructuredGridRepresentation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0](work-product-component/UnstructuredGridRepresentation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0](work-product-component/UnstructuredGridRepresentation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--UnstructuredGridRepresentation:1.0.0](work-product-component/UnstructuredGridRepresentation.1.0.0.md)|data.IndexableElementCount[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.0.0](work-product-component/VelocityModeling.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.0.0](work-product-component/VelocityModeling.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.0.0](work-product-component/VelocityModeling.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.0.0](work-product-component/VelocityModeling.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.0.0](work-product-component/VelocityModeling.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.0.0](work-product-component/VelocityModeling.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.0.0](work-product-component/VelocityModeling.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.1.0](work-product-component/VelocityModeling.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.1.0](work-product-component/VelocityModeling.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.1.0](work-product-component/VelocityModeling.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.1.0](work-product-component/VelocityModeling.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.1.0](work-product-component/VelocityModeling.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.1.0](work-product-component/VelocityModeling.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--VelocityModeling:1.1.0](work-product-component/VelocityModeling.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VoidageGroupInterpretation:1.0.0](work-product-component/VoidageGroupInterpretation.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VoidageGroupInterpretation:1.0.0](work-product-component/VoidageGroupInterpretation.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--VoidageGroupInterpretation:1.0.0](work-product-component/VoidageGroupInterpretation.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--VoidageGroupInterpretation:1.0.0](work-product-component/VoidageGroupInterpretation.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VoidageGroupInterpretation:1.0.0](work-product-component/VoidageGroupInterpretation.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--VoidageGroupInterpretation:1.0.0](work-product-component/VoidageGroupInterpretation.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--VoidageGroupInterpretation:1.0.0](work-product-component/VoidageGroupInterpretation.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.Intervals[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.IntervalProperties.KeyColumns[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.IntervalProperties.KeyColumns[].FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.IntervalProperties.Columns[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.IntervalProperties.Columns[].FacetIDs[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreIntervalSet:1.0.0](work-product-component/WellboreIntervalSet.1.0.0.md)|data.IntervalProperties.ColumnValues[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.0.0](work-product-component/WellboreMarkerSet.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.0.0](work-product-component/WellboreMarkerSet.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.0.0](work-product-component/WellboreMarkerSet.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.0.0](work-product-component/WellboreMarkerSet.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.0.0](work-product-component/WellboreMarkerSet.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.0.0](work-product-component/WellboreMarkerSet.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.0.0](work-product-component/WellboreMarkerSet.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.0.0](work-product-component/WellboreMarkerSet.1.0.0.md)|data.Markers[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](work-product-component/WellboreMarkerSet.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](work-product-component/WellboreMarkerSet.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](work-product-component/WellboreMarkerSet.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](work-product-component/WellboreMarkerSet.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](work-product-component/WellboreMarkerSet.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](work-product-component/WellboreMarkerSet.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](work-product-component/WellboreMarkerSet.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](work-product-component/WellboreMarkerSet.1.1.0.md)|data.AvailableMarkerProperties[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](work-product-component/WellboreMarkerSet.1.1.0.md)|data.Markers[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.0](work-product-component/WellboreMarkerSet.1.2.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.0](work-product-component/WellboreMarkerSet.1.2.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.0](work-product-component/WellboreMarkerSet.1.2.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.0](work-product-component/WellboreMarkerSet.1.2.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.0](work-product-component/WellboreMarkerSet.1.2.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.0](work-product-component/WellboreMarkerSet.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.0](work-product-component/WellboreMarkerSet.1.2.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.0](work-product-component/WellboreMarkerSet.1.2.0.md)|data.AvailableMarkerProperties[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.0](work-product-component/WellboreMarkerSet.1.2.0.md)|data.Markers[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.1](work-product-component/WellboreMarkerSet.1.2.1.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.1](work-product-component/WellboreMarkerSet.1.2.1.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.1](work-product-component/WellboreMarkerSet.1.2.1.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.1](work-product-component/WellboreMarkerSet.1.2.1.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.1](work-product-component/WellboreMarkerSet.1.2.1.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.1](work-product-component/WellboreMarkerSet.1.2.1.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.1](work-product-component/WellboreMarkerSet.1.2.1.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.1](work-product-component/WellboreMarkerSet.1.2.1.md)|data.AvailableMarkerProperties[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreMarkerSet:1.2.1](work-product-component/WellboreMarkerSet.1.2.1.md)|data.Markers[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.0.0](work-product-component/WellboreTrajectory.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.0.0](work-product-component/WellboreTrajectory.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.0.0](work-product-component/WellboreTrajectory.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.0.0](work-product-component/WellboreTrajectory.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.0.0](work-product-component/WellboreTrajectory.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.0.0](work-product-component/WellboreTrajectory.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.0.0](work-product-component/WellboreTrajectory.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.1.0](work-product-component/WellboreTrajectory.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.1.0](work-product-component/WellboreTrajectory.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.1.0](work-product-component/WellboreTrajectory.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.1.0](work-product-component/WellboreTrajectory.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.1.0](work-product-component/WellboreTrajectory.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.1.0](work-product-component/WellboreTrajectory.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.1.0](work-product-component/WellboreTrajectory.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellboreTrajectory:1.1.0](work-product-component/WellboreTrajectory.1.1.0.md)|data.AvailableTrajectoryStationProperties[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.0.0](work-product-component/WellLog.1.0.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.0.0](work-product-component/WellLog.1.0.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.0.0](work-product-component/WellLog.1.0.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.0.0](work-product-component/WellLog.1.0.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.0.0](work-product-component/WellLog.1.0.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.0.0](work-product-component/WellLog.1.0.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.0.0](work-product-component/WellLog.1.0.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.0.0](work-product-component/WellLog.1.0.0.md)|data.Curves[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.1.0](work-product-component/WellLog.1.1.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.1.0](work-product-component/WellLog.1.1.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.1.0](work-product-component/WellLog.1.1.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.1.0](work-product-component/WellLog.1.1.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.1.0](work-product-component/WellLog.1.1.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.1.0](work-product-component/WellLog.1.1.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.1.0](work-product-component/WellLog.1.1.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.1.0](work-product-component/WellLog.1.1.0.md)|data.Curves[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.2.0](work-product-component/WellLog.1.2.0.md)|data.Artefacts[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.2.0](work-product-component/WellLog.1.2.0.md)|data.TechnicalAssurances[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.2.0](work-product-component/WellLog.1.2.0.md)|data.TechnicalAssurances[].Reviewers[]|object (default)|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.2.0](work-product-component/WellLog.1.2.0.md)|data.TechnicalAssurances[].AcceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.2.0](work-product-component/WellLog.1.2.0.md)|data.TechnicalAssurances[].UnacceptableUsage[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.2.0](work-product-component/WellLog.1.2.0.md)|data.GeoContexts[]|nested|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.2.0](work-product-component/WellLog.1.2.0.md)|data.LineageAssertions[]|flattened|-|
|&rarr; [osdu:wks:work-product-component--WellLog:1.2.0](work-product-component/WellLog.1.2.0.md)|data.Curves[]|nested|-|

[Back to TOC](#TOC)