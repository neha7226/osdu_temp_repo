# Migration Guides

This section presents schema migration guides per OSDU milestone release.

* [Milestone 7](M07/README.md)
* [Milestone 9](M09/README.md)
* [Milestone 10](M10/README.md)
* [Milestone 11](M11/README.md)
* [Milestone 12](M12/README.md)
* [Milestone 13](M13/README.md)
* [Milestone 14](M14/README.md)
* [Milestone 15](M15/README.md)
* [Milestone 16](M16/README.md)

# Links

* [Back to Guides](../README.md)