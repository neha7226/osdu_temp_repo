# Note
This folder contains auto-generated change reports against the community mirror site,
https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/,
specifically the 
[SchemaRegistrationResources/shared-schemas](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu).

The reports are generated while running `OsduSchemaComposer.py`.

Additional 'human' change notes are provided in the [ChangeReport](../ChangeReport.md).