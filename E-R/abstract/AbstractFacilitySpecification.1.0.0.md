<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractFacilitySpecification
A property, characteristic, or attribute about a facility that is not described explicitly elsewhere.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractFacilitySpecification:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; [Authoring Schema AbstractFacilitySpecification.1.0.0.json](../../Authoring/abstract/AbstractFacilitySpecification.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractFacilitySpecification.1.0.0.json](../../Generated/abstract/AbstractFacilitySpecification.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractFacilitySpecification.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractFacilitySpecification.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractFacilitySpecification


## Outgoing Relationships for AbstractFacilitySpecification

![AbstractFacilitySpecification](../_diagrams/abstract/AbstractFacilitySpecification.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractFacilitySpecification Referenced by other Entities

![AbstractFacilitySpecification](../_diagrams/abstract/AbstractFacilitySpecification.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacilitySpecification Properties

## Table of AbstractFacilitySpecification Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|EffectiveDateTime| |string|optional|Effective Date Time|The date and time at which the facility specification instance becomes effective.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TerminationDateTime| |string|optional|Termination Date Time|The date and time at which the facility specification instance is no longer in effect.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecificationQuantity| |number|optional|Facility Specification Quantity|The value for the specified parameter type.|(No pattern)|(No format)|UOM_via_property:UnitOfMeasureID|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecificationDateTime| |string|optional|Facility Specification Date Time|The actual date and time value of the parameter.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecificationIndicator| |boolean|optional|Facility Specification Indicator|The actual indicator value of the parameter.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|FacilitySpecificationText| |string|optional|Facility Specification Text|The actual text value of the parameter.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|UnitOfMeasureID &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)| |string|optional|Unit Of Measure Id|The unit for the quantity parameter, like metre (m in SI units system) for quantity Length.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ParameterTypeID &rarr; [ParameterType](../reference-data/ParameterType.1.0.0.md)| |string|optional|Parameter Type Id|Parameter type of property or characteristic.|<code>^[\w\-\.]+:reference-data\-\-ParameterType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ParameterType](../reference-data/ParameterType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFacilitySpecification.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractFacility.1.0.0](AbstractFacility.1.0.0.md) | FacilitySpecifications[] | many (as array) |  |
| Included by | [AbstractFacility.1.1.0](AbstractFacility.1.1.0.md) | FacilitySpecifications[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._