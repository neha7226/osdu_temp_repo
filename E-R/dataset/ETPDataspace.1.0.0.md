<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# ETPDataspace [Status: Accepted]
Describes the location and attributes of a Dataspace accessible through ETP (Energistics Transfer Protocol) APIs, for ETP 1.2 and above
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:dataset--ETPDataspace:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: OSDU
* Link to &rarr; Proposal workbook [ETPDataspace.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/dataset/ETPDataspace.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema ETPDataspace.1.0.0.json](../../Authoring/dataset/ETPDataspace.1.0.0.json)
* Link to &rarr; [Generated Schema ETPDataspace.1.0.0.json](../../Generated/dataset/ETPDataspace.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource ETPDataspace.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//dataset/ETPDataspace.1.0.0.json)
* Link to &rarr; [Example Record ETPDataspace.1.0.0.json](../../Examples/dataset/ETPDataspace.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams ETPDataspace


## Outgoing Relationships for ETPDataspace

![ETPDataspace](../_diagrams/dataset/ETPDataspace.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## ETPDataspace Referenced by other Entities

![ETPDataspace](../_diagrams/dataset/ETPDataspace.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# ETPDataspace Properties

## 1. Table of ETPDataspace System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:dataset\-\-ETPDataspace:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:dataset--ETPDataspace:bb2f2ebb-eb23-5a21-af06-d6ba19f63626|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:dataset--ETPDataspace:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of ETPDataspace Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of ETPDataspace Data Properties, Section AbstractDataset

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)_|_object_|_optional_|_AbstractDataset_|_Schema fragment holding properties common for all datasets._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Name|An optional name of the dataset, e.g. a user friendly file or file collection name.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Dataset X221/15|
|data.Description|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Description|An optional, textual description of the dataset.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|As originally delivered by ACME.com.|
|data.TotalSize|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Total Size|Total size of the dataset in bytes; for files it is the same as declared in FileSourceInfo.FileSize or the sum of all individual files. Implemented as string. The value must be convertible to a long integer (sizes can become very large).|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|13245217273|
|data.EncodingFormatTypeID &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Encoding Format Type ID|EncodingFormatType ID reference value relationship. It can be a mime-type or media-type.|<code>^[\w\-\.]+:reference-data\-\-EncodingFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--EncodingFormatType:text%2Fcsv:|
|data.SchemaFormatTypeID &rarr; [SchemaFormatType](../reference-data/SchemaFormatType.1.0.0.md)|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Schema Format Type ID|Relationship to the SchemaFormatType reference value.|<code>^[\w\-\.]+:reference-data\-\-SchemaFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SchemaFormatType](../reference-data/SchemaFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--SchemaFormatType:CWLS%20LAS3:|
|data.Endian|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|enum string|optional|Endian|Endianness of binary value.  Enumeration: "BIG", "LITTLE".  If absent, applications will need to interpret from context indicators.|<code>BIG&#124;LITTLE</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|BIG|
|_data.DatasetProperties_|_[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)_|_object_|_required_|_Dataset Properties_|_Placeholder for a specialization._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{}_|
|data.DatasetProperties|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|object|required|Dataset Properties|Placeholder for a specialization.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{}|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of ETPDataspace Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|_data.DatasetProperties_| |_object_|_required_|_DatasetProperties_|_The dataset specific properties for ETPDataspace._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.DatasetProperties.ServerURL| |string|optional|ServerURL|The location of the ETP server over the web containing the Dataspace. Optional, only if the ETP server is located outside of an OSDU instance|(No pattern)|uri|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|http://etp_server.mylocation.com|
|data.DatasetProperties.Path| |string|optional|Path|The unique location associated with the Dataspace, which is used to construct the Dataspace's URI. Optional|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|/folder-name/project-name|
|data.DatasetProperties.URI| |string|required|URI|ETP 1.2 canonical URI for Dataspace. Mandatory and controlled by a pattern given by the ETP documentation  ^eml:\/\/\/(?:dataspace\('(?[^']?(?:''[^']?)*)'\))?$ .|<code>^eml:\/\/\/(?:dataspace\(&#x27;(?:[^&#x27;]*?(?:&#x27;&#x27;[^&#x27;]*?)*)&#x27;\))?$</code>|uri|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|eml:///dataspace('/folder-name/project-name')|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of ETPDataspace Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# ETPDataspace.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractWPCGroupType.1.0.0](../abstract/AbstractWPCGroupType.1.0.0.md) | Datasets[] | many (as array) | Untyped relationship |
| Referred to by | [AbstractWPCGroupType.1.0.0](../abstract/AbstractWPCGroupType.1.0.0.md) | Artefacts[].ResourceID | many (as array) | Untyped relationship |
| Referred to by | [SeismicTraceData.1.0.0](../work-product-component/SeismicTraceData.1.0.0.md) | TraceRelationFileID | one | Untyped relationship |
| Referred to by | [SeismicTraceData.1.1.0](../work-product-component/SeismicTraceData.1.1.0.md) | TraceRelationFileID | one | Untyped relationship |
| Referred to by | [SeismicTraceData.1.2.0](../work-product-component/SeismicTraceData.1.2.0.md) | TraceRelationFileID | one | Untyped relationship |
| Referred to by | [SeismicTraceData.1.3.0](../work-product-component/SeismicTraceData.1.3.0.md) | TraceRelationFileID | one | Untyped relationship |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._