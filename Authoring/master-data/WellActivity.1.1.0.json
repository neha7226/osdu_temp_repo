{
  "x-osdu-review-status": "Accepted",
  "$id": "https://schema.osdu.opengroup.org/json/master-data/WellActivity.1.1.0.json",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "WellActivity",
  "description": "A Well Activity is a operation performed on a well to accomplish a predetermined objective. This may be as short as a few hours in duration, or may last several days, weeks or even months. It will have one or more associated daily Operations Reports, and may have other associated data as well.",
  "type": "object",
  "x-osdu-virtual-properties": {
    "data.VirtualProperties.DefaultName": {
      "type": "string",
      "priority": [
        {
          "path": "data.Name"
        }
      ]
    },
    "data.VirtualProperties.DefaultLocation": {
      "type": "object",
      "priority": [
        {
          "path": "data.SpatialLocation"
        }
      ]
    }
  },
  "allOf": [
    {
      "$ref": "../abstract/AbstractMaster.1.1.0.json"
    },
    {
      "$ref": "../abstract/AbstractProject.1.0.0.json"
    },
    {
      "$ref": "../abstract/AbstractProjectActivity.1.1.0.json"
    },
    {
      "type": "object",
      "properties": {
        "Name": {
          "type": "string",
          "title": "Name",
          "description": "The name of this Well Activity"
        },
        "WellID": {
          "type": "string",
          "title": "Well ID",
          "description": "Link to the Well",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen",
          "pattern": "^[\\w\\-\\.]+:master-data\\-\\-Well:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "WellActivityType": {
          "type": "string",
          "title": "Well Activity Type",
          "description": "The type of well activity being conducted.",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen",
          "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellActivityType:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "WellActivityDescription": {
          "type": "string",
          "title": "Well Activity Description",
          "description": "A narrative summary of the work being conducted in this well activity",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "WellActivityObjective": {
          "type": "array",
          "title": "Well Activity Objective",
          "description": "The objective for the Well Activity.  A Well Activity may have one or more objectives. The first is the primary objective to need to retain order.",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen",
          "items": {
            "type": "string"
          }
        },
        "EstimatedDuration": {
          "type": "number",
          "title": "Estimated Duration",
          "description": "Expected duration in days for the well activity",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "WellheadConnection": {
          "type": "string",
          "title": "Wellhead Connection",
          "description": "Current wellhead connection (from picklist - local)",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen",
          "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellheadConnection:[\\w\\-\\.\\:\\%]+:[0-9]*$"
        },
        "TechnologyApplied": {
          "type": "array",
          "title": "Technology Applied",
          "description": "Benchmarking categories (from picklist - open)",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen",
          "items": {
            "type": "string",
            "pattern": "^[\\w\\-\\.]+:reference-data\\-\\-WellTechnologyApplied:[\\w\\-\\.\\:\\%]+:[0-9]*$"
          }
        },
        "StartDateTime": {
          "type": "string",
          "title": "Start Date/Time",
          "description": "Timestamp for the beginning of the Well Activity",
          "format": "date-time",
          "x-osdu-frame-of-reference": "DateTime",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "EndDateTime": {
          "type": "string",
          "title": "End Date/Time",
          "description": "Timestamp for the end of the Well Activity",
          "format": "date-time",
          "x-osdu-frame-of-reference": "DateTime",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "DataQCName": {
          "type": "string",
          "title": "Data QC Name",
          "description": "Name of the individual that approved the quality check for the data",
          "format": "date-time",
          "x-osdu-frame-of-reference": "DateTime",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "DataQCDate": {
          "type": "string",
          "title": "Data QC Date",
          "description": "Date of data quality check approval",
          "format": "date-time",
          "x-osdu-frame-of-reference": "DateTime",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "OffProductionDateTime": {
          "type": "string",
          "title": "Off Production Date/Time",
          "description": "Timestamp for when well was taken off production for well activity",
          "format": "date-time",
          "x-osdu-frame-of-reference": "DateTime",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "OnProductionDateTime": {
          "type": "string",
          "title": "On Production Date/Time",
          "description": "Timestamp for when the well is returned to production following well activity",
          "format": "date-time",
          "x-osdu-frame-of-reference": "DateTime",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "IsPerformanceBenchmarkSubmitted": {
          "type": "boolean",
          "title": "Performance Benchmark Submitted?",
          "description": "Flag to identify whether data has been submitted for performance benchmarking",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "PerformanceBenchmarkSubmitter": {
          "type": "string",
          "title": "Performance Benchmark Submitter",
          "description": "Name of the individual that submitted data for benchmarking",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "PerformanceBenchmarkSubmittedDate": {
          "type": "string",
          "title": "Performance Benchmark Submitted Date",
          "description": "Date Performance Benchmark was submitted",
          "format": "date-time",
          "x-osdu-frame-of-reference": "DateTime",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "Comments": {
          "type": "string",
          "title": "Comments",
          "description": "Freeform comments describing the overall well activity",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "StewardingCompany": {
          "type": "string",
          "title": "Stewarding Company",
          "description": "Company or corporate division that is responsible for executing the work.",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "StewardingCompanyTeam": {
          "type": "string",
          "title": "Stewarding Company Team",
          "description": "Team within a company, or corporate division that is responsible for executing the work.",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "Customer": {
          "type": "string",
          "title": "Customer",
          "description": "Individual, company, or corporate division that work is being executed on behalf of.",
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen"
        },
        "RigAssignments": {
          "type": "array",
          "title": "Rig Assignments",
          "description": "List of Rigs or Work Units performing the Well Activity",
          "x-osdu-indexing": {
            "type": "flattened"
          },
          "x-osdu-attribution-authority": "The Open Group",
          "x-osdu-attribution-publication": "The OSDU Data Platform",
          "x-osdu-attribution-revision": "evergreen",
          "items": {
            "type": "object",
            "title": "RigAssignment",
            "description": "Association of a rig to a particular well and well activity.",
            "properties": {
              "RigID": {
                "type": "string",
                "title": "Rig ID",
                "description": "A link to the Rig",
                "x-osdu-attribution-authority": "The Open Group",
                "x-osdu-attribution-publication": "The OSDU Data Platform",
                "x-osdu-attribution-revision": "evergreen",
                "pattern": "^[\\w\\-\\.]+:master-data\\-\\-Rig:[\\w\\-\\.\\:\\%]+:[0-9]*$"
              },
              "StartDateTime": {
                "type": "string",
                "title": "Start Date Time",
                "description": "The start time for this rig assignment to the well activity",
                "format": "date-time",
                "x-osdu-frame-of-reference": "DateTime",
                "x-osdu-attribution-authority": "The Open Group",
                "x-osdu-attribution-publication": "The OSDU Data Platform",
                "x-osdu-attribution-revision": "evergreen"
              },
              "EndDateTime": {
                "type": "string",
                "title": "End Date Time",
                "description": "The end time for this rig assignment to the well activity",
                "format": "date-time",
                "x-osdu-frame-of-reference": "DateTime",
                "x-osdu-attribution-authority": "The Open Group",
                "x-osdu-attribution-publication": "The OSDU Data Platform",
                "x-osdu-attribution-revision": "evergreen"
              },
              "Remark": {
                "type": "string",
                "title": "Remark",
                "description": "Remarks related to this rig assignment",
                "x-osdu-attribution-authority": "The Open Group",
                "x-osdu-attribution-publication": "The OSDU Data Platform",
                "x-osdu-attribution-revision": "evergreen"
              }
            }
          }
        }
      },
      "required": [
        "WellID",
        "WellActivityType"
      ]
    }
  ]
}