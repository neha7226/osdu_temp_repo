<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractPropertyType [Status: Accepted]
A nested object holding the relationship to a PropertyType by id (uuid) and a derived, human-readable name.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractPropertyType:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractPropertyType.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractPropertyType.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractPropertyType.1.0.0.json](../../Authoring/abstract/AbstractPropertyType.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractPropertyType.1.0.0.json](../../Generated/abstract/AbstractPropertyType.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractPropertyType.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractPropertyType.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractPropertyType


## Outgoing Relationships for AbstractPropertyType

![AbstractPropertyType](../_diagrams/abstract/AbstractPropertyType.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractPropertyType Referenced by other Entities

![AbstractPropertyType](../_diagrams/abstract/AbstractPropertyType.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractPropertyType Properties

## Table of AbstractPropertyType Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|PropertyTypeID &rarr; [PropertyType](../reference-data/PropertyType.1.0.0.md)| |string|optional|Property Type ID|The relationship to the PropertyType reference data item, typically containing an Energistics PWLS 3 uuid. For better traceability and usability the property name is to be populated in the Name property.|<code>^[\w\-\.]+:reference-data\-\-PropertyType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [PropertyType](../reference-data/PropertyType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--PropertyType:ace68d4c-7400-431d-9a33-0541b8bfc4b4:|
|Name| |string|optional|Name|The name of the PropertyType, de-normalized, derived from the record referenced in PropertyTypeID.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "PropertyTypeID", "TargetPropertyName": "Name"}|(No natural key)|dip azimuth|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractPropertyType.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractReferencePropertyType.1.0.0](AbstractReferencePropertyType.1.0.0.md) | PropertyType | one |  |
| Included by | [AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) | PropertyType | one |  |
| Included by | [ActivityTemplate.1.0.0](../master-data/ActivityTemplate.1.0.0.md) | Parameters[].PropertyType | many (as array) |  |
| Included by | [LogCurveFamily.1.1.0](../reference-data/LogCurveFamily.1.1.0.md) | PropertyType | one |  |
| Included by | [LogCurveType.1.1.0](../reference-data/LogCurveType.1.1.0.md) | PropertyType | one |  |
| Included by | [UnitOfMeasureConfiguration.1.0.0](../reference-data/UnitOfMeasureConfiguration.1.0.0.md) | Configurations[].PropertyType | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._