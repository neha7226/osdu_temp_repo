<a name="TOC"></a>

[[_TOC_]]

# Migration to `osdu:wks:master-data--ConnectedSourceDataJob:1.3.0`

`ConnectedSourceDataJob:1.3.0` is extended to become an activity by including 
[AbstractProjectActivity:1.1.0](../../../E-R/abstract/AbstractProjectActivity.1.1.0.md). This is used to define a 
dynamic parameter in `data.Parameters[]` with the Title "ConnectedSourceSchemaAuthority" and type string.

The usage is explained in the [Worked Example for External Data Services](../../../Examples/WorkedExamples/ExternalDataServices/README.md#connectedsourcedatajob-as-activity).

[Back to TOC](#TOC)

---

# Links

* [Back to Milestone](README.md)
* [Back to Migration Guides](../README.md)
* [Back to Guides](../../README.md)
