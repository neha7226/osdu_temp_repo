<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractColumnBasedTable [Status: Accepted]
The ColumnBasedTable is a set of columns, which have equal length (data.ColumnSize) included by types carrying embedded table properties. Columns have a Property Kind, UnitOfMeasure and Facet. There are KeyColumns (index columns) and Columns (for look-up values). Examples are KrPc, PVT and Facies tables.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractColumnBasedTable:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractColumnBasedTable.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractColumnBasedTable.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractColumnBasedTable.1.0.0.json](../../Authoring/abstract/AbstractColumnBasedTable.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractColumnBasedTable.1.0.0.json](../../Generated/abstract/AbstractColumnBasedTable.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractColumnBasedTable.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractColumnBasedTable.1.0.0.json)
* Link to worked examples in context &rarr; [Topic: Usage of `work-product-component--StratigraphicColumn:1.0.0`](../../Examples/WorkedExamples/Reservoir%20Data/Stratigraphy/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractColumnBasedTable


## Outgoing Relationships for AbstractColumnBasedTable

![AbstractColumnBasedTable](../_diagrams/abstract/AbstractColumnBasedTable.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractColumnBasedTable Referenced by other Entities

![AbstractColumnBasedTable](../_diagrams/abstract/AbstractColumnBasedTable.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractColumnBasedTable Properties

## Table of AbstractColumnBasedTable Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_KeyColumns[]_|_[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)_|_object_|_optional_|_AbstractReferencePropertyType_|_A column whose values are considered as keys/indices. Do not use this attribute if you want to follow a given ColumnBasedTableType. Fragment Description: The purpose of this schema is best understood in the context of a columnar dataset: the AbstractReferencePropertyType describes a column in a columnar dataset by declaring its value type (number, string), a UnitQuantity if the value type is a number, a kind if the string value is actually a relationship to a e.g. reference-data type._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|KeyColumns[].ValueType|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)|string|optional|Value Type|The type of value to expect for this reference property, either "number" (floating point number), "integer",  "string", or "boolean".|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|string|
|KeyColumns[].ValueCount|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)|integer|optional|Value Count|The number of values in a tuple, e.g. For coordinates. The default is 1.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1|
|KeyColumns[].UnitQuantityID &rarr; [UnitQuantity](../reference-data/UnitQuantity.1.0.0.md)|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)|string|optional|Unit Quantity ID|Only populated of the ValueType is "number". It holds the UnitQuantity associated with this reference property type. It is a relationship to UnitQuantity record.|<code>^[\w\-\.]+:reference-data\-\-UnitQuantity:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitQuantity](../reference-data/UnitQuantity.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--UnitQuantity:plane%20angle:|
|_KeyColumns[].PropertyType_|_[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractPropertyType.1.0.0](AbstractPropertyType.1.0.0.md)_|_object_|_optional_|_AbstractPropertyType_|_It holds the PropertyType associated with this reference property type, further defining the semantics of the value. It contains a relationship to PropertyType record and its (de-normalized) name. String or number values can represent e.g. A date or a time by referring to the respective PropertyType record id. Fragment Description: A nested object holding the relationship to a PropertyType by id (uuid) and a derived, human-readable name._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|KeyColumns[].PropertyType.PropertyTypeID &rarr; [PropertyType](../reference-data/PropertyType.1.0.0.md)|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractPropertyType.1.0.0](AbstractPropertyType.1.0.0.md)|string|optional|Property Type ID|The relationship to the PropertyType reference data item, typically containing an Energistics PWLS 3 uuid. For better traceability and usability the property name is to be populated in the Name property.|<code>^[\w\-\.]+:reference-data\-\-PropertyType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [PropertyType](../reference-data/PropertyType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--PropertyType:ace68d4c-7400-431d-9a33-0541b8bfc4b4:|
|KeyColumns[].PropertyType.Name|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractPropertyType.1.0.0](AbstractPropertyType.1.0.0.md)|string|optional|Name|The name of the PropertyType, de-normalized, derived from the record referenced in PropertyTypeID.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "PropertyTypeID", "TargetPropertyName": "Name"}|(No natural key)|dip azimuth|
|KeyColumns[].RelationshipTargetKind|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)|string|optional|Relationship Target Kind|Only populated if ValueType=="string" and the values are expected to represent record ids, e.g. to a reference-data type, then this value holds the kind (optionally without the semantic version number).|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:([0-9]+)?(\.)?([0-9]+)?(\.)?([0-9]+)?$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--UnitOfMeasure:|
|_KeyColumns[].FacetIDs[]_|_[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractFacet.1.0.0](AbstractFacet.1.0.0.md)_|_object_|_optional_|_AbstractFacet_|_Ordered array with: FacetType, FacetRole, both calling specific references  FacetType: Enumerations of the type of additional context about the nature of a property type (it may include conditions, direction, qualifiers, or statistics).   FacetRole: Additional context about the nature of a property type. The purpose of such attribute is to minimize the need to create specialized property types by mutualizing some well known qualifiers such as "maximum", "minimum" which apply to a lot of different property types. Fragment Description: A tuple FacetType, FacetRole, both calling specific references  FacetType: Enumeration of the type of additional context about the nature of a property type (it may include conditions, direction, qualifiers, or statistics).   FacetRole: Additional context about the nature of a property type. The purpose of such attribute is to minimize the need to create specialized property types by mutualizing some well known qualifiers such as "maximum", "minimum" which apply to a lot of different property types._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|KeyColumns[].FacetIDs[].FacetTypeID &rarr; [FacetType](../reference-data/FacetType.1.0.0.md)|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractFacet.1.0.0](AbstractFacet.1.0.0.md)|string|required|Facet Type ID|FacetType: An 'enumeration' of the type of additional context about the nature of a property type (it may include conditions, direction, qualifiers, or statistics).|<code>^[\w\-\.]+:reference-data\-\-FacetType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacetType](../reference-data/FacetType.1.0.0.md)|json-pointer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|KeyColumns[].FacetIDs[].FacetRoleID &rarr; [FacetRole](../reference-data/FacetRole.1.0.0.md)|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractFacet.1.0.0](AbstractFacet.1.0.0.md)|string|required|Facet Role ID|Additional context about the nature of a property type. The purpose of such attribute is to minimize the need to create specialized property types by mutualizing some well known qualifiers such as "maximum", "minimum" which apply to a lot of different property types.|<code>^[\w\-\.]+:reference-data\-\-FacetRole:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacetRole](../reference-data/FacetRole.1.0.0.md)|json-pointer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_Columns[]_|_[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)_|_object_|_optional_|_AbstractReferencePropertyType_|_A common column storing values of a particular property kind. Do not use this attribute if you want to follow a given ColumnBasedTableType. Fragment Description: The purpose of this schema is best understood in the context of a columnar dataset: the AbstractReferencePropertyType describes a column in a columnar dataset by declaring its value type (number, string), a UnitQuantity if the value type is a number, a kind if the string value is actually a relationship to a e.g. reference-data type._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Columns[].ValueType|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)|string|optional|Value Type|The type of value to expect for this reference property, either "number" (floating point number), "integer",  "string", or "boolean".|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|string|
|Columns[].ValueCount|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)|integer|optional|Value Count|The number of values in a tuple, e.g. For coordinates. The default is 1.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1|
|Columns[].UnitQuantityID &rarr; [UnitQuantity](../reference-data/UnitQuantity.1.0.0.md)|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)|string|optional|Unit Quantity ID|Only populated of the ValueType is "number". It holds the UnitQuantity associated with this reference property type. It is a relationship to UnitQuantity record.|<code>^[\w\-\.]+:reference-data\-\-UnitQuantity:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitQuantity](../reference-data/UnitQuantity.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--UnitQuantity:plane%20angle:|
|_Columns[].PropertyType_|_[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractPropertyType.1.0.0](AbstractPropertyType.1.0.0.md)_|_object_|_optional_|_AbstractPropertyType_|_It holds the PropertyType associated with this reference property type, further defining the semantics of the value. It contains a relationship to PropertyType record and its (de-normalized) name. String or number values can represent e.g. A date or a time by referring to the respective PropertyType record id. Fragment Description: A nested object holding the relationship to a PropertyType by id (uuid) and a derived, human-readable name._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Columns[].PropertyType.PropertyTypeID &rarr; [PropertyType](../reference-data/PropertyType.1.0.0.md)|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractPropertyType.1.0.0](AbstractPropertyType.1.0.0.md)|string|optional|Property Type ID|The relationship to the PropertyType reference data item, typically containing an Energistics PWLS 3 uuid. For better traceability and usability the property name is to be populated in the Name property.|<code>^[\w\-\.]+:reference-data\-\-PropertyType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [PropertyType](../reference-data/PropertyType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--PropertyType:ace68d4c-7400-431d-9a33-0541b8bfc4b4:|
|Columns[].PropertyType.Name|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractPropertyType.1.0.0](AbstractPropertyType.1.0.0.md)|string|optional|Name|The name of the PropertyType, de-normalized, derived from the record referenced in PropertyTypeID.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "PropertyTypeID", "TargetPropertyName": "Name"}|(No natural key)|dip azimuth|
|Columns[].RelationshipTargetKind|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md)|string|optional|Relationship Target Kind|Only populated if ValueType=="string" and the values are expected to represent record ids, e.g. to a reference-data type, then this value holds the kind (optionally without the semantic version number).|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:([0-9]+)?(\.)?([0-9]+)?(\.)?([0-9]+)?$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--UnitOfMeasure:|
|_Columns[].FacetIDs[]_|_[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractFacet.1.0.0](AbstractFacet.1.0.0.md)_|_object_|_optional_|_AbstractFacet_|_Ordered array with: FacetType, FacetRole, both calling specific references  FacetType: Enumerations of the type of additional context about the nature of a property type (it may include conditions, direction, qualifiers, or statistics).   FacetRole: Additional context about the nature of a property type. The purpose of such attribute is to minimize the need to create specialized property types by mutualizing some well known qualifiers such as "maximum", "minimum" which apply to a lot of different property types. Fragment Description: A tuple FacetType, FacetRole, both calling specific references  FacetType: Enumeration of the type of additional context about the nature of a property type (it may include conditions, direction, qualifiers, or statistics).   FacetRole: Additional context about the nature of a property type. The purpose of such attribute is to minimize the need to create specialized property types by mutualizing some well known qualifiers such as "maximum", "minimum" which apply to a lot of different property types._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Columns[].FacetIDs[].FacetTypeID &rarr; [FacetType](../reference-data/FacetType.1.0.0.md)|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractFacet.1.0.0](AbstractFacet.1.0.0.md)|string|required|Facet Type ID|FacetType: An 'enumeration' of the type of additional context about the nature of a property type (it may include conditions, direction, qualifiers, or statistics).|<code>^[\w\-\.]+:reference-data\-\-FacetType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacetType](../reference-data/FacetType.1.0.0.md)|json-pointer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Columns[].FacetIDs[].FacetRoleID &rarr; [FacetRole](../reference-data/FacetRole.1.0.0.md)|[AbstractReferencePropertyType.1.1.0](AbstractReferencePropertyType.1.1.0.md) [AbstractFacet.1.0.0](AbstractFacet.1.0.0.md)|string|required|Facet Role ID|Additional context about the nature of a property type. The purpose of such attribute is to minimize the need to create specialized property types by mutualizing some well known qualifiers such as "maximum", "minimum" which apply to a lot of different property types.|<code>^[\w\-\.]+:reference-data\-\-FacetRole:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [FacetRole](../reference-data/FacetRole.1.0.0.md)|json-pointer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ColumnSize| |integer|optional|Size of columns|The count of elements in each column|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|5|
|_ColumnValues[]_| |_object_|_optional_|_ColumnValues_|_First column values are related to first key column, second column values are related to the second key column, etc… Column values at index KeyColumns count are related to first (non key) column, Column values at index KeyColumns count + 1 are related to second (non key) column, etc... Fragment Description: Value of the column. Generally only one of the attribute should be instantiated._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ColumnValues[].BooleanColumn[]| |boolean|optional|Boolean Column|A column of only boolean values|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[true, false, true, true, false]|
|ColumnValues[].IntegerColumn[]| |integer|optional|Integer Column|A column of only integer values|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[0, 1, 2, 3, 4]|
|ColumnValues[].NumberColumn[]| |number|optional|Number Column|A column of only number values|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[0.1, 2.3, 4.5, 6.7, 8.9]|
|ColumnValues[].StringColumn[]| |string|optional|String Column|A column of only string values|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|["foo", "bar", "foo again", "bar again", "foo bar"]|
|ColumnValues[].UndefinedValueRows[]| |integer|optional|Undefined value rows|The row indexes for which the values are flagged as undefined.|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[3, 4]|
|ColumnBasedTableType &rarr; [ColumnBasedTableType](../reference-data/ColumnBasedTableType.1.0.0.md)| |string|optional|The type of the column based table|Quickly indicate the type of the column based table (KrPc, PVT, Facies, ...) and its standard columns definition. It is supposed to be used when you don't use KeyColumns neither Columns as attributes of this WPC.|<code>^[\w\-\.]+:reference-data\-\-ColumnBasedTableType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ColumnBasedTableType](../reference-data/ColumnBasedTableType.1.0.0.md)|uri-reference|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--ColumnBasedTableType:Facies:|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractColumnBasedTable.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [WellboreIntervalSet.1.0.0](../work-product-component/WellboreIntervalSet.1.0.0.md) | IntervalProperties | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._