<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractGeoContext
A geographic context to an entity. It can be either a reference to a GeoPoliticalEntity, Basin, Field, Play or Prospect.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractGeoContext:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; [Authoring Schema AbstractGeoContext.1.0.0.json](../../Authoring/abstract/AbstractGeoContext.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractGeoContext.1.0.0.json](../../Generated/abstract/AbstractGeoContext.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractGeoContext.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractGeoContext.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractGeoContext


## Outgoing Relationships for AbstractGeoContext

![AbstractGeoContext](../_diagrams/abstract/AbstractGeoContext.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractGeoContext Referenced by other Entities

![AbstractGeoContext](../_diagrams/abstract/AbstractGeoContext.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractGeoContext Properties

## Table of AbstractGeoContext Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|GeoPoliticalEntityID &rarr; [GeoPoliticalEntity](../master-data/GeoPoliticalEntity.1.0.0.md)|oneOf[0] [AbstractGeoPoliticalContext.1.0.0](AbstractGeoPoliticalContext.1.0.0.md)|string|optional|Geo Political Entity Id|Reference to GeoPoliticalEntity.|<code>^[\w\-\.]+:master-data\-\-GeoPoliticalEntity:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [GeoPoliticalEntity](../master-data/GeoPoliticalEntity.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoTypeID &rarr; [GeoPoliticalEntityType](../reference-data/GeoPoliticalEntityType.1.0.0.md)|oneOf[0] [AbstractGeoPoliticalContext.1.0.0](AbstractGeoPoliticalContext.1.0.0.md)|string|optional|Geo Type Id|The GeoPoliticalEntityType reference of the GeoPoliticalEntity (via GeoPoliticalEntityID) for application convenience.|<code>^[\w\-\.]+:reference-data\-\-GeoPoliticalEntityType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [GeoPoliticalEntityType](../reference-data/GeoPoliticalEntityType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "GeoPoliticalEntityID", "TargetPropertyName": "GeoPoliticalEntityTypeID"}|(No natural key)|(No example)|
|BasinID &rarr; [Basin](../master-data/Basin.1.0.0.md)|oneOf[1] [AbstractGeoBasinContext.1.0.0](AbstractGeoBasinContext.1.0.0.md)|string|optional|Basin Id|Reference to Basin.|<code>^[\w\-\.]+:master-data\-\-Basin:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Basin](../master-data/Basin.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoTypeID &rarr; [BasinType](../reference-data/BasinType.1.0.0.md)|oneOf[1] [AbstractGeoBasinContext.1.0.0](AbstractGeoBasinContext.1.0.0.md)|string|optional|Geo Type Id|The BasinType reference of the Basin (via BasinID) for application convenience.|<code>^[\w\-\.]+:reference-data\-\-BasinType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [BasinType](../reference-data/BasinType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "BasinID", "TargetPropertyName": "BasinTypeID"}|(No natural key)|(No example)|
|FieldID &rarr; [Field](../master-data/Field.1.0.0.md)|oneOf[2] [AbstractGeoFieldContext.1.0.0](AbstractGeoFieldContext.1.0.0.md)|string|optional|Field Id|Reference to Field.|<code>^[\w\-\.]+:master-data\-\-Field:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Field](../master-data/Field.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoTypeID|oneOf[2] [AbstractGeoFieldContext.1.0.0](AbstractGeoFieldContext.1.0.0.md)|const string|optional|Geo Type Id|The fixed type 'Field' for this AbstractGeoFieldContext.|<code>Field</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Field|
|PlayID &rarr; [Play](../master-data/Play.1.0.0.md)|oneOf[3] [AbstractGeoPlayContext.1.0.0](AbstractGeoPlayContext.1.0.0.md)|string|optional|Play Id|Reference to the play.|<code>^[\w\-\.]+:master-data\-\-Play:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Play](../master-data/Play.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoTypeID &rarr; [PlayType](../reference-data/PlayType.1.0.0.md)|oneOf[3] [AbstractGeoPlayContext.1.0.0](AbstractGeoPlayContext.1.0.0.md)|string|optional|Geo Type Id|The PlayType reference of the Play (via PlayID) for application convenience.|<code>^[\w\-\.]+:reference-data\-\-PlayType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [PlayType](../reference-data/PlayType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "PlayID", "TargetPropertyName": "PlayTypeID"}|(No natural key)|(No example)|
|ProspectID &rarr; [Prospect](../master-data/Prospect.1.0.0.md)|oneOf[4] [AbstractGeoProspectContext.1.0.0](AbstractGeoProspectContext.1.0.0.md)|string|optional|Prospect Id|Reference to the prospect.|<code>^[\w\-\.]+:master-data\-\-Prospect:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [Prospect](../master-data/Prospect.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|GeoTypeID &rarr; [ProspectType](../reference-data/ProspectType.1.0.0.md)|oneOf[4] [AbstractGeoProspectContext.1.0.0](AbstractGeoProspectContext.1.0.0.md)|string|optional|Geo Type Id|The ProspectType reference of the Prospect (via ProspectID) for application convenience.|<code>^[\w\-\.]+:reference-data\-\-ProspectType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ProspectType](../reference-data/ProspectType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|{"RelationshipPropertyName": "ProspectID", "TargetPropertyName": "ProspectTypeID"}|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractGeoContext.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractMaster.1.0.0](AbstractMaster.1.0.0.md) | GeoContexts[] | many (as array) |  |
| Included by | [AbstractMaster.1.1.0](AbstractMaster.1.1.0.md) | GeoContexts[] | many (as array) |  |
| Included by | [AbstractWorkProductComponent.1.0.0](AbstractWorkProductComponent.1.0.0.md) | GeoContexts[] | many (as array) |  |
| Included by | [AbstractWorkProductComponent.1.1.0](AbstractWorkProductComponent.1.1.0.md) | GeoContexts[] | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._