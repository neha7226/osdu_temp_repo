<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# DataCollection
Transient collection of object references used e.g. to exchange context between running applications. DataCollection is not intended to be persisted.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:DataCollection:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: OSDU
* Link to &rarr; [Authoring Schema DataCollection.1.0.0.json](../../Authoring/data-collection/DataCollection.1.0.0.json)
* Link to &rarr; [Generated Schema DataCollection.1.0.0.json](../../Generated/data-collection/DataCollection.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource DataCollection.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//data-collection/DataCollection.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams DataCollection


## Outgoing Relationships for DataCollection

![DataCollection](../_diagrams/data-collection/DataCollection.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## DataCollection Referenced by other Entities

![DataCollection](../_diagrams/data-collection/DataCollection.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# DataCollection Properties

## Table of DataCollection Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Resources[]| |string|required|Resources|List of Resources|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Name| |string|required|Name|Name|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Description| |string|optional|Description|Description|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|CreationDateTime| |string|optional|Creation Date Time|Creation DateTime|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Tags[]| |string|optional|Tags|Array of Tag Names|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|SubmitterName| |string|optional|Submitter Name|Submitter Name|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|AuthorIDs[]| |string|optional|Author I Ds|Array of Author IDs|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|OwnerID| |string|required|Owner Id|ID of the User who owns the Collection|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|WorkSpaceID| |string|optional|Work Space Id|Collection Workspace|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_FilterSpecification_| |_object_|_optional_|_Filter Specification_|_Collection Filter Specification_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._