<a name="TOC"></a>**Table of Contents**

[[_TOC_]]

Changes since: 2021-11-16 (first changes by PPDM)

* [Detailed Change Log](ChangesAfter-2021-11-16.md) for reference values.

# M11 Changes

## AzimuthReferenceType

The values are now sourced from PPDM, see
[AzimuthReferenceType.1.0.0.xlsx](../../Published/AzimuthReferenceType.1.0.0.xlsx). The workbook format follows PPDM
standard.

## OperatingEnvironment

The values are now sourced from PPDM, see
[OperatingEnvironment.1.0.0.xlsx](../../Published/OperatingEnvironment.1.0.0.xlsx). The workbook format follows PPDM
standard. The reference values are organized hierarchically, but the hierarchy is not expressed in the schema (no
self-reference). This is per design and agreement with PPDM.

## UnitOfMeasureConfiguration

The contributions from [ExxonMobil (Excel workbook)](../../Resources/OSDU/UnitOfMeasureConfiguration/ExxonMobil.xlsx)
and [Shell (Excel workbook)](../../Resources/OSDU/UnitOfMeasureConfiguration/Shell.xlsx) are now converted into one
example [manifest](../../Manifests/reference-data/LOCAL/UnitOfMeasureConfiguration.1.0.0.json). It doesn't have numeric
display formats populated - this is expected for more scoped configurations created by applications or workflows.

The donations contain a number of unit symbols, which are not directly mappable to OSDU's standard UnitOfMeasure
records. Among others, all standard volumes do not appear in the UnitOfMeasureConfiguration.

## ExternalUnitOfMeasure, etc.

To support unit symbol mappings into the Energistics platform standard UnitOfMeasure the following example 'catalogs'
have been added:

|Type|Governance Mode|Workbook|Manifest|
|---|---|---|---|
|CatalogMapStateType:1.0.0|FIXED|&rarr; [CatalogMapStateType.1.0.0.xlsx](../../Published/CatalogMapStateType.1.0.0.xlsx)|&rarr; [CatalogMapStateType.1.0.0.json](../../Manifests/reference-data/FIXED/CatalogMapStateType.1.0.0.json) |
|ExternalCatalogNamespace:1.0.0|LOCAL|&rarr; [ExternalCatalogNamespace.1.0.0.xlsx](../../Published/ExternalCatalogNamespace.1.0.0.xlsx)|&rarr; [ExternalCatalogNamespace.1.0.0.json](../../Manifests/reference-data/LOCAL/ExternalCatalogNamespace.1.0.0.json) |
|ExternalUnitOfMeasure:1.0.0|LOCAL|&rarr; [ExternalUnitOfMeasure.1.0.0.xlsx](../../Published/ExternalUnitOfMeasure.1.0.0.xlsx)|&rarr; [ExternalUnitOfMeasure.1.0.0.json](../../Manifests/reference-data/LOCAL/ExternalUnitOfMeasure.1.0.0.json) |
|ExternalUnitQuantity:1.0.0|LOCAL|&rarr; [ExternalUnitQuantity.1.0.0.xlsx](../../Published/ExternalUnitQuantity.1.0.0.xlsx)|&rarr; [ExternalUnitQuantity.1.0.0.json](../../Manifests/reference-data/LOCAL/ExternalUnitQuantity.1.0.0.json) |

[Back to TOC](#TOC)

## Default CRS Catalog

With support from the OSDU Geomatics community, a default CRS catalog has been compiled. The content is based on the
EPSG GeoRepository. Version/Change:

```
IOGP Revision status as per last Change record:
  Change Code: 2022.027
  Change Name: Correct info source typographic errors.
  Change Date: 2022-02-28T00:00:00
  Change URL : https://apps.epsg.org/api/v1/Change/2022.027/
```

|Type|Governance Mode|Workbook|Manifest|
|---|---|---|---|
|Resources/IOGP/OSDU/CRS_CT_EBCRS_list_OSDU|LOCAL|[CRS_CT_EBCRS_list_OSDU.xlsx](../../Resources/IOGP/OSDU/CRS_CT_EBCRS_list_OSDU.xlsx)| [CRS_CT.json](../../Manifests/reference-data/LOCAL/CRS_CT.json)|

As before, the records of CoordinateReferenceSystem and CoordinateTransformation are merged due to the interdependencies
between the types.

[Back to TOC](#TOC)
## CurveSampleType

A new reference value type was added to be able to clearly distinguish and support different Curve Data Formats (
from [LAS 3.0 File Structure Specifications](https://www.cwls.org/wp-content/uploads/2014/09/LAS_3_File_Structure.pdf)) inside the well log.

|Type|Governance Mode|Workbook|Manifest|
|---|---|---|---|
|CurveSampleType:1.0.0|FIXED|&rarr; [CurveSampleType.1.0.0.xlsx](../../Published/CurveSampleType.1.0.0.xlsx)|&rarr; [CurveSampleType.1.0.0.json](../../Manifests/reference-data/FIXED/CurveSampleType.1.0.0.json) |

## SchemaFormatType

The [reference value list](../../Manifests/reference-data/OPEN/SchemaFormatType.1.0.0.json) for SchemaFormatType has
been extended by 4 new values covering the different WITSML format versions, here with their codes:

1. `Energistics.WITSML.v1.3` 
2. `Energistics.WITSML.v1.4` 
3. `Energistics.WITSML.v2.0` 
4. `Energistics.WITSML.v2.1`

## Clean-up

The following manifest files were found orphaned, i.e. there reference lists are not in use **_and_** there are no
corresponding schema definitions in the schema repository. The following manifest files were removed:

1. `InterpretationType.1.0.0` located in `/Manifests/reference-data/LOCAL/InterpretationType.1.0.0.json`
2. `StratigraphicUnitType.1.0.0` located in `/Manifests/reference-data/OPEN/StratigraphicUnitType.1.0.0.json`
3. `FluidContactType.1.0.0` located in `/Manifests/reference-data/FIXED/FluidContactType.1.0.0.json`
4. `HorizonStratigraphicRoleType.1.0.0` located
   in `/Manifests/reference-data/FIXED/HorizonStratigraphicRoleType.1.0.0.json`
5. `PropertyTableLookupType.1.0.0` located in `/Manifests/reference-data/FIXED/PropertyTableLookupType.1.0.0.json`

The removed manifests did not appear in the overview report [README.md](../../Manifests/README.md)

In addition, an unused workbook `Validation.1.0.0.xlsx` located in `/Published/Validation.1.0.0.xlsx` was deleted.

[Back to TOC](#TOC)

---

[Back to Milestone Overview](../README.md)