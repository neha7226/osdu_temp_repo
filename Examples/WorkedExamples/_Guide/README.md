# Sample Records for the Usage Guide

This folder contains record samples and fragments, which are used, i.e. included or referred to by
the [Usage Guide](../../../Guides/README.md). The example generation script will validate the sample records against the
schemas and report if they do not comply with the schema or contain properties, which are not defined in the schema
(gratuitous properties). 