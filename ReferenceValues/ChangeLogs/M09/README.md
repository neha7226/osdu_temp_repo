[[_TOC_]]

# M9 Changes

## Referential Integrity

1. Ingestion tests have shown that some relationships were incorrect or referring to non-existing records. The manifest
   generation process has been enhanced to check the existence of every related object. All broken relationships have
   been corrected
2. Some reference value types have references/relationships to other reference types, some have even cyclic references.
   During the schema generation these dependencies are detected and captured in
   the [ReferenceValueTypeDependencies.json](../../Manifests/reference-data/ReferenceValueTypeDependencies.json). Types,
   which have interdependencies are merged into one manifest file (`MergedManifests` in
   ReferenceValueTypeDependencies.json).
3. All manifests with internal parent-child dependencies are re-ordered such that parents get created before their
   children. This applies to the merged manifests in particular.

## UTC DateTime

* All date-times have been adjusted to conform to either UTC Zulu time or UTC+offset. This change enables successful
  indexing of date-times.

## Manifest Loading Sequence: `IngestionSequence.json`

Following the [ReferenceValueTypeDependencies.json](../../Manifests/reference-data/ReferenceValueTypeDependencies.json)
`IngestionSequence` it is possible to construct
a [IngestionSequence.json](../../Manifests/reference-data/IngestionSequence.json). It is a simple list of JSON objects
linking the kind or the merged manifest name to a relative file location. The first two records are shown below:

```json
[
  {
    "kind": "ANT_ANTC_OT_SO",
    "Key": "ANT_ANTC_OT_SO",
    "FileName": "Manifests/reference-data/OPEN/ANT_ANTC_OT_SO.json"
  },
  {
    "kind": "reference-data--ActivityType:1.0.0",
    "Key": "ActivityType.1.0.0",
    "FileName": "Manifests/reference-data/LOCAL/ActivityType.1.0.0.json"
  }
]
```

> **_Note:_** in M12 a bug in the dependency computation was fixed, which removed the `OrganisationType` from the 
> merge. Therefore, the ANT_ANTC_OT_SO got renamed to ANT_ANTC_SO

1. The first element refers to a merged manifest, in this case "AliasNameType.1.0.0", "AliasNameTypeClass.1.0.0", "
   OrganisationType.1.0.0", "StandardsOrganisation.1.0.0".
2. The second element refers to an ordinary manifest.
3. The inline example is truncated after the first two elements.
   See [IngestionSequence.json](../../Manifests/reference-data/IngestionSequence.json) for the full details.

If the manifests are loaded in the sequence as listed
in [IngestionSequence.json](../../Manifests/reference-data/IngestionSequence.json) all reference-value relationships are
resolvable.

## Cleanups

1. A duplicate CollectionPurpose manifest was removed from the LOCAL folder.
2. Governance Change for AliasNameTypeClass from LOCAL to OPEN.
3. Additional values for:
    1. AzimuthReferenceType
    2. FacilityType
    3. AgreementType (Aliases only)

---

[Back to Milestone Overview](../README.md)