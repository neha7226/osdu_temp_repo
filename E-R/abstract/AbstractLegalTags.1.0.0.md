<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# Legal Meta Data
Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractLegalTags:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; [Authoring Schema AbstractLegalTags.1.0.0.json](../../Authoring/abstract/AbstractLegalTags.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractLegalTags.1.0.0.json](../../Generated/abstract/AbstractLegalTags.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractLegalTags.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractLegalTags.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams Legal Meta Data


## Outgoing Relationships for Legal Meta Data

![Legal Meta Data](../_diagrams/abstract/AbstractLegalTags.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legal Meta Data Referenced by other Entities

![Legal Meta Data](../_diagrams/abstract/AbstractLegalTags.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Legal Meta Data Properties

## Table of Legal Meta Data Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|legaltags[]| |string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|otherRelevantDataCountries[]| |string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|status| |string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractLegalTags.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractSystemProperties.1.0.0](AbstractSystemProperties.1.0.0.md) | legal | one |  |
| Included by | [ConnectedSourceDataJob.1.0.0](../master-data/ConnectedSourceDataJob.1.0.0.md) | OnIngestionLegalTags | one |  |
| Included by | [ConnectedSourceDataJob.1.1.0](../master-data/ConnectedSourceDataJob.1.1.0.md) | OnIngestionLegalTags | one |  |
| Included by | [ConnectedSourceDataJob.1.2.0](../master-data/ConnectedSourceDataJob.1.2.0.md) | OnIngestionLegalTags | one |  |
| Included by | [ConnectedSourceDataJob.1.3.0](../master-data/ConnectedSourceDataJob.1.3.0.md) | OnIngestionLegalTags | one |  |
| Included by | [GenericDataset.1.0.0](../manifest/GenericDataset.1.0.0.md) | legal | one |  |
| Included by | [GenericMasterData.1.0.0](../manifest/GenericMasterData.1.0.0.md) | legal | one |  |
| Included by | [GenericReferenceData.1.0.0](../manifest/GenericReferenceData.1.0.0.md) | legal | one |  |
| Included by | [GenericWorkProduct.1.0.0](../manifest/GenericWorkProduct.1.0.0.md) | legal | one |  |
| Included by | [GenericWorkProductComponent.1.0.0](../manifest/GenericWorkProductComponent.1.0.0.md) | legal | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._