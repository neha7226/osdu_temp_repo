<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# File.OGC.GeoTIFF [Status: Accepted]
File.OGC.GeoTIFF represents a standard, tagged image file format defined by the Open Geospatial Consortium. The geospatial metadata are included or 'inlined' in the file. The geospatial information includes map projection, coordinate systems, ellipsoids, datums, and everything else necessary to establish the exact spatial reference for the file.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:dataset--File.OGC.GeoTIFF:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: OSDU
* Link to &rarr; Proposal workbook [File.OGC.GeoTIFF.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/dataset/File.OGC.GeoTIFF.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema File.OGC.GeoTIFF.1.0.0.json](../../Authoring/dataset/File.OGC.GeoTIFF.1.0.0.json)
* Link to &rarr; [Generated Schema File.OGC.GeoTIFF.1.0.0.json](../../Generated/dataset/File.OGC.GeoTIFF.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource File.OGC.GeoTIFF.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//dataset/File.OGC.GeoTIFF.1.0.0.json)
* Link to &rarr; [Example Record File.OGC.GeoTIFF.1.0.0.json](../../Examples/dataset/File.OGC.GeoTIFF.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.
* Link to worked examples in context &rarr; [Topic: Geo-Referenced Images](../../Examples/WorkedExamples/GeoReferencedImage/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams File.OGC.GeoTIFF


## Outgoing Relationships for File.OGC.GeoTIFF

![File.OGC.GeoTIFF](../_diagrams/dataset/File.OGC.GeoTIFF.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## File.OGC.GeoTIFF Referenced by other Entities

![File.OGC.GeoTIFF](../_diagrams/dataset/File.OGC.GeoTIFF.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# File.OGC.GeoTIFF Properties

## 1. Table of File.OGC.GeoTIFF System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:dataset\-\-File.OGC.GeoTIFF:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:dataset--File.OGC.GeoTIFF:efd29d82-dadd-5464-9ada-3d8c13531af0|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:dataset--File.OGC.GeoTIFF:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of File.OGC.GeoTIFF Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of File.OGC.GeoTIFF Data Properties, Section AbstractDataset

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)_|_object_|_optional_|_AbstractDataset_|_Schema fragment holding properties common for all datasets._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Name|An optional name of the dataset, e.g. a user friendly file or file collection name.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Dataset X221/15|
|data.Description|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Description|An optional, textual description of the dataset.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|As originally delivered by ACME.com.|
|data.TotalSize|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Total Size|Total size of the dataset in bytes; for files it is the same as declared in FileSourceInfo.FileSize or the sum of all individual files. Implemented as string. The value must be convertible to a long integer (sizes can become very large).|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|13245217273|
|data.EncodingFormatTypeID &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Encoding Format Type ID|EncodingFormatType ID reference value relationship. It can be a mime-type or media-type.|<code>^[\w\-\.]+:reference-data\-\-EncodingFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--EncodingFormatType:text%2Fcsv:|
|data.SchemaFormatTypeID &rarr; [SchemaFormatType](../reference-data/SchemaFormatType.1.0.0.md)|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|string|optional|Schema Format Type ID|Relationship to the SchemaFormatType reference value.|<code>^[\w\-\.]+:reference-data\-\-SchemaFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [SchemaFormatType](../reference-data/SchemaFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--SchemaFormatType:CWLS%20LAS3:|
|data.Endian|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|enum string|optional|Endian|Endianness of binary value.  Enumeration: "BIG", "LITTLE".  If absent, applications will need to interpret from context indicators.|<code>BIG&#124;LITTLE</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|BIG|
|_data.DatasetProperties_|_[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)_|_object_|_required_|_Dataset Properties_|_Placeholder for a specialization._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{}_|
|data.DatasetProperties|[AbstractDataset.1.0.0](../abstract/AbstractDataset.1.0.0.md)|object|required|Dataset Properties|Placeholder for a specialization.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{}|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of File.OGC.GeoTIFF Data Properties, Section AbstractFile

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md)_|_object_|_optional_|_AbstractFile_|_The schema fragment representing single files_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|_data.DatasetProperties_|_[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md)_|_object_|_required_|_Dataset Properties_|_The dataset properties for a single file._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|_data.DatasetProperties.FileSourceInfo_|_[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)_|_object_|_optional_|_AbstractFileSourceInfo_|_Information about the single file. Fragment Description: A schema fragment to describe file source information._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.DatasetProperties.FileSourceInfo.FileSource|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|required|File Source|The location of the file. It can be a relative path. The actual access is provided via the File Service. When used in context of a FileCollection (dataset--FileCollection*) FileSource is a relative path from the FileCollectionPath. It can be used by consumers to pull an individual file if they so choose by concatenating the FileCollectionPath with the FileSource. This property is required.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://default_bucket/r1/data/provided/documents/1000.witsml|
|data.DatasetProperties.FileSourceInfo.PreloadFilePath|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Path|File system path to the data file as it existed before loading to the data platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|s3://staging-area/r7/raw-data/provided/documents/1000.witsml|
|data.DatasetProperties.FileSourceInfo.PreloadFileCreateUser|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Create User|Optional user name or reference, who created the file prior to up-loading to the platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|somebody@acme.org|
|data.DatasetProperties.FileSourceInfo.PreloadFileCreateDate|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Create Date|Optional create date and time of the file prior to up-loading to the platform.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|2019-12-16T11:46:20.163Z|
|data.DatasetProperties.FileSourceInfo.PreloadFileModifyUser|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Modify User|Optional user name or reference, who last modified the file prior to up-loading to the platform.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|somebody.else@acme.org|
|data.DatasetProperties.FileSourceInfo.PreloadFileModifyDate|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Preload File Modify Date|Optional last modified date and time of the file prior to up-loading to the platform.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|2019-12-20T17:20:05.356Z|
|data.DatasetProperties.FileSourceInfo.Name|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Name|Optional, user-friendly file name.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1000.witsml|
|data.DatasetProperties.FileSourceInfo.FileSize|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|File Size|Length of file in bytes. Implemented as string. The value must be convertible to a long integer (sizes can become very large).|<code>^[0-9]+$</code>|integer|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|95463|
|data.DatasetProperties.FileSourceInfo.EncodingFormatTypeID &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Encoding Format Type ID|Only used in FileCollection where the EncodingFormatType differs from data DatasetDefault specified in data.EncodingFormatTypeID: the media type specification for this dataset.|<code>^[\w\-\.]+:reference-data\-\-EncodingFormatType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [EncodingFormatType](../reference-data/EncodingFormatType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--EncodingFormatType:application%2Fgeo%2Bjson:|
|data.DatasetProperties.FileSourceInfo.Checksum|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Checksum|Checksum of file bytes - a hexadecimal number with even number of bytes.|<code>^([0-9a-fA-F]{2})+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|d41d8cd98f00b204e9800998ecf8427e|
|data.DatasetProperties.FileSourceInfo.ChecksumAlgorithm|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md) [AbstractFileSourceInfo.1.0.0](../abstract/AbstractFileSourceInfo.1.0.0.md)|string|optional|Checksum Algorithm|The name of the checksum algorithm e.g. MD5, SHA-256.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|SHA-256|
|data.Checksum|[AbstractFile.1.0.0](../abstract/AbstractFile.1.0.0.md)|string|optional|MD5 Checksum|MD5 checksum of file bytes - a 32 byte hexadecimal number.|<code>^[0-9a-fA-F]{32}</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|d41d8cd98f00b204e9800998ecf8427e|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of File.OGC.GeoTIFF Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.CoordinateReferenceSystemID &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)| |string|optional|Coordinate Reference System ID|The OSDU Platform CRS reference into the CoordinateReferenceSystem catalog matching the inlined geospatial definition. This CRS can also be a BoundCRS, which provides the transformation to WGS 84.|<code>^[\w\-\.]+:reference-data\-\-CoordinateReferenceSystem:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--CoordinateReferenceSystem:BoundProjected:EPSG::32012_EPSG::15851:|
|data.VerticalCoordinateReferenceSystemID &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)| |string|optional|Vertical Coordinate Reference System ID|The explicit VerticalCRS reference into the CoordinateReferenceSystem catalog. This property stays empty for 2D geometries. Absent or empty values for 3D geometries mean the context may be provided by a CompoundCRS in 'CoordinateReferenceSystemID' or implicitly EPSG:5714 MSL height|<code>^[\w\-\.]+:reference-data\-\-CoordinateReferenceSystem:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [CoordinateReferenceSystem](../reference-data/CoordinateReferenceSystem.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--CoordinateReferenceSystem:Vertical:EPSG::5714:|
|data.PersistableReferenceCrs| |string|required|CRS Reference|The OSDU Platform CRS reference as persistableReference string. If populated, the CoordinateReferenceSystemID takes precedence. This CRS can also be a BoundCRS, which provides the transformation to WGS 84.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"authCode":{"auth":"OSDU","code":"32021079"},"lateBoundCRS":{"authCode":{"auth":"EPSG","code":"32021"},"name":"NAD_1927_StatePlane_North_Dakota_South_FIPS_3302","type":"LBC","ver":"PE_10_9_1","wkt":"PROJCS[\"NAD_1927_StatePlane_North_Dakota_South_FIPS_3302\",GEOGCS[\"GCS_North_American_1927\",DATUM[\"D_North_American_1927\",SPHEROID[\"Clarke_1866\",6378206.4,294.9786982]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Lambert_Conformal_Conic\"],PARAMETER[\"False_Easting\",2000000.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",-100.5],PARAMETER[\"Standard_Parallel_1\",46.18333333333333],PARAMETER[\"Standard_Parallel_2\",47.48333333333333],PARAMETER[\"Latitude_Of_Origin\",45.66666666666666],UNIT[\"Foot_US\",0.3048006096012192],AUTHORITY[\"EPSG\",32021]]"},"name":"NAD27 * OGP-Usa Conus / North Dakota CS27 South zone [32021,15851]","singleCT":{"authCode":{"auth":"EPSG","code":"15851"},"name":"NAD_1927_To_WGS_1984_79_CONUS","type":"ST","ver":"PE_10_9_1","wkt":"GEOGTRAN[\"NAD_1927_To_WGS_1984_79_CONUS\",GEOGCS[\"GCS_North_American_1927\",DATUM[\"D_North_American_1927\",SPHEROID[\"Clarke_1866\",6378206.4,294.9786982]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],GEOGCS[\"GCS_WGS_1984\",DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137.0,298.257223563]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],METHOD[\"NADCON\"],PARAMETER[\"Dataset_conus\",0.0],OPERATIONACCURACY[5.0],AUTHORITY[\"EPSG\",15851]]"},"type":"EBC","ver":"PE_10_9_1"}|
|data.PersistableReferenceVerticalCrs| |string|optional|Vertical CRS Reference|The VerticalCRS reference as persistableReference string. If populated, the VerticalCoordinateReferenceSystemID takes precedence. The property is null or empty for 2D geometries. For 3D geometries and absent or null persistableReferenceVerticalCrs the vertical CRS is either provided via persistableReferenceCrs's CompoundCRS or it is implicitly defined as EPSG:5714 MSL height.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"authCode":{"auth":"EPSG","code":"5714"},"name":"MSL_Height","type":"LBC","ver":"PE_10_9_1","wkt":"VERTCS[\"MSL_Height\",VDATUM[\"Mean_Sea_Level\"],PARAMETER[\"Vertical_Shift\",0.0],PARAMETER[\"Direction\",1.0],UNIT[\"Meter\",1.0],AUTHORITY[\"EPSG\",5714]]"}|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 6. Table of File.OGC.GeoTIFF Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# File.OGC.GeoTIFF.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractWPCGroupType.1.0.0](../abstract/AbstractWPCGroupType.1.0.0.md) | Datasets[] | many (as array) | Untyped relationship |
| Referred to by | [AbstractWPCGroupType.1.0.0](../abstract/AbstractWPCGroupType.1.0.0.md) | Artefacts[].ResourceID | many (as array) | Untyped relationship |
| Referred to by | [SeismicTraceData.1.0.0](../work-product-component/SeismicTraceData.1.0.0.md) | TraceRelationFileID | one | Untyped relationship |
| Referred to by | [SeismicTraceData.1.1.0](../work-product-component/SeismicTraceData.1.1.0.md) | TraceRelationFileID | one | Untyped relationship |
| Referred to by | [SeismicTraceData.1.2.0](../work-product-component/SeismicTraceData.1.2.0.md) | TraceRelationFileID | one | Untyped relationship |
| Referred to by | [SeismicTraceData.1.3.0](../work-product-component/SeismicTraceData.1.3.0.md) | TraceRelationFileID | one | Untyped relationship |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._