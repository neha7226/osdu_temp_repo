<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# OSDU Common Resources [Status: Accepted]
Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractCommonResources:1.0.0`
* Schema status: PUBLISHED
* **Migration Guide (M12)** [`osdu:wks:AbstractCommonResources:1.0.0`](../../Guides/MigrationGuides/M12/AbstractCommonResources.1.0.0.md)
* Link to &rarr; Proposal workbook [AbstractCommonResources.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractCommonResources.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractCommonResources.1.0.0.json](../../Authoring/abstract/AbstractCommonResources.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractCommonResources.1.0.0.json](../../Generated/abstract/AbstractCommonResources.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractCommonResources.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractCommonResources.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams OSDU Common Resources


## Outgoing Relationships for OSDU Common Resources

![OSDU Common Resources](../_diagrams/abstract/AbstractCommonResources.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## OSDU Common Resources Referenced by other Entities

![OSDU Common Resources](../_diagrams/abstract/AbstractCommonResources.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# OSDU Common Resources Properties

## Table of OSDU Common Resources Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|ResourceHomeRegionID &rarr; [OSDURegion](../reference-data/OSDURegion.1.0.0.md)| |string|optional|Resource Home Region ID|The name of the home [cloud environment] region for this OSDU resource object.|<code>^[\w\-\.]+:reference-data\-\-OSDURegion:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [OSDURegion](../reference-data/OSDURegion.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ResourceHostRegionIDs[] &rarr; [OSDURegion](../reference-data/OSDURegion.1.0.0.md)| |string|optional|Resource Host Region ID|The name of the host [cloud environment] region(s) for this OSDU resource object.|<code>^[\w\-\.]+:reference-data\-\-OSDURegion:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [OSDURegion](../reference-data/OSDURegion.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ResourceCurationStatus &rarr; [ResourceCurationStatus](../reference-data/ResourceCurationStatus.1.0.0.md)| |string|optional|Resource Curation Status|Describes the current Curation status.|<code>^[\w\-\.]+:reference-data\-\-ResourceCurationStatus:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ResourceCurationStatus](../reference-data/ResourceCurationStatus.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ResourceLifecycleStatus &rarr; [ResourceLifecycleStatus](../reference-data/ResourceLifecycleStatus.1.0.0.md)| |string|optional|Resource Lifecycle Status|Describes the current Resource Lifecycle status.|<code>^[\w\-\.]+:reference-data\-\-ResourceLifecycleStatus:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ResourceLifecycleStatus](../reference-data/ResourceLifecycleStatus.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ResourceSecurityClassification &rarr; [ResourceSecurityClassification](../reference-data/ResourceSecurityClassification.1.0.0.md)| |string|optional|Resource Security Classification|Classifies the security level of the resource.|<code>^[\w\-\.]+:reference-data\-\-ResourceSecurityClassification:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ResourceSecurityClassification](../reference-data/ResourceSecurityClassification.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Source| |string|optional|Data Source|The entity that produced the record, or from which it is received; could be an organization, agency, system, internal team, or individual. For informational purposes only, the list of sources is not governed.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ExistenceKind &rarr; [ExistenceKind](../reference-data/ExistenceKind.1.0.0.md)| |string|optional|Existence Kind|Where does this data resource sit in the cradle-to-grave span of its existence?|<code>^[\w\-\.]+:reference-data\-\-ExistenceKind:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ExistenceKind](../reference-data/ExistenceKind.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|~~TechnicalAssuranceID &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)~~| |~~string~~|~~optional~~|~~Technical Assurance ID~~|~~DEPRECATED: Describes a record's overall suitability for general business consumption based on data quality. Clarifications: Since Certified is the highest classification of suitable quality, any further change or versioning of a Certified record should be carefully considered and justified. If a Technical Assurance value is not populated then one can assume the data has not been evaluated or its quality is unknown (=Unevaluated). Technical Assurance values are not intended to be used for the identification of a single "preferred" or "definitive" record by comparison with other records.~~|~~<code>^[\w\-\.]+:reference-data\-\-TechnicalAssuranceType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [TechnicalAssuranceType](../reference-data/TechnicalAssuranceType.1.0.0.md)~~|~~(No format)~~|~~(No frame of reference)~~|~~(No indexing hint)~~|~~(Not derived)~~|~~(No natural key)~~|~~(No example)~~|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractCommonResources.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [GenericDataset.1.0.0](../manifest/GenericDataset.1.0.0.md) | allOf | one |  |
| Included by | [GenericMasterData.1.0.0](../manifest/GenericMasterData.1.0.0.md) | allOf | one |  |
| Included by | [GenericReferenceData.1.0.0](../manifest/GenericReferenceData.1.0.0.md) | allOf | one |  |
| Included by | [GenericWorkProduct.1.0.0](../manifest/GenericWorkProduct.1.0.0.md) | allOf | one |  |
| Included by | [GenericWorkProductComponent.1.0.0](../manifest/GenericWorkProductComponent.1.0.0.md) | allOf | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._