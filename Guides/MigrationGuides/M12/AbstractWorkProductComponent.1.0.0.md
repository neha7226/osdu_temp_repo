# Migration and Adoption of `osdu:wks:AbstractWorkProductComponent:1.1.0`

[`AbstractWorkProductComponent:1.0.0`](../../../E-R/abstract/AbstractWorkProductComponent.1.0.0.md) upgrades to 
[`AbstractWorkProductComponent:1.1.0`](../../../E-R/abstract/AbstractWorkProductComponent.1.1.0.md). 

It includes [`AbstractSpatialLocation:1.1.0`](../../../E-R/abstract/AbstractSpatialLocation.1.1.0.md) and
includes [`AbstractAnyCrsFeatureCollection:1.1.0`](../../../E-R/abstract/AbstractAnyCrsFeatureCollection.1.1.0.md),
which got extended by one property `VerticalUnitID`. This triggers an update chain, please see
details in [Migration Guide for `AbstractAnyCrsFeatureCollection:1.1.0`](AbstractAnyCrsFeatureCollection.1.0.0.md)

---

# Links

* [Back to Milestone](README.md)
* [Back to Migration Guides](../README.md)
* [Back to Guides](../../README.md)
