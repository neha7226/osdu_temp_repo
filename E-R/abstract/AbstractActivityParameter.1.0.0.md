<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# AbstractActivityParameter [Status: Accepted]
General parameter value used in one instance of activity.
[Without inheritance, combined specializations.]
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractActivityParameter:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; Proposal workbook [AbstractActivityParameter.1.0.0.xlsx](https://gitlab.opengroup.org/osdu/subcommittees/data-def/work-products/schema/-/tree/master/Proposals/abstract/AbstractActivityParameter.1.0.0.xlsx) — (the link refers to a resource in the OSDU Member GitLab)
* Link to &rarr; [Authoring Schema AbstractActivityParameter.1.0.0.json](../../Authoring/abstract/AbstractActivityParameter.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractActivityParameter.1.0.0.json](../../Generated/abstract/AbstractActivityParameter.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractActivityParameter.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractActivityParameter.1.0.0.json)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams AbstractActivityParameter


## Outgoing Relationships for AbstractActivityParameter

![AbstractActivityParameter](../_diagrams/abstract/AbstractActivityParameter.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## AbstractActivityParameter Referenced by other Entities

![AbstractActivityParameter](../_diagrams/abstract/AbstractActivityParameter.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractActivityParameter Properties

## Table of AbstractActivityParameter Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|Title| |string|required|Title|Name of the parameter, used to identify it in the activity. It must have an equivalent in the ActivityTemplate parameters.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Index| |integer|optional|Index|When parameter is an array, used to indicate the index in the array.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Selection| |string|optional|Selection|Textual description about how this parameter was selected.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_Keys[]_| |_object_|_optional_|_Parameter Key_|_A nested array describing keys used to identify a parameter value. When multiple values are provided for a given parameter, the key provides a way to identify the parameter through its association with an object, a time index or a parameter array member via ParameterKey value. Fragment Description: Abstract class describing a key used to identify a parameter value. When multiple values are provided for a given parameter, provides a way to identify the parameter through its association with an object, a time index, an integer...  [Without inheritance, combined specializations.] Note: floating point numbers are not supported as key values; the numbers have to be formatted as strings for robust equality operations, which are necessary for keys._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|Keys[].ObjectParameterKey| |string|optional|Object Parameter Key|Relationship to an object ID, which acts as the parameter.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Keys[].TimeIndexParameterKey| |string|optional|Time Index Parameter Key|The time index acting as parameter key value.|(No pattern)|time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Keys[].ParameterKey| |string|optional|Internal Parameter Key|The key name, which establishes an association between parameters.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Keys[].IntegerParameterKey| |integer|optional|Integer Parameter Key|Integer value from "ParameterKey" parameter, associated with this parameter. Example: {"ParameterKey": "index", "StringParameterKey: 2}.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|Keys[].StringParameterKey| |string|optional|String Parameter Key|String value from "ParameterKey" parameter, associated with this parameter. Can be used to associate with parameter values of type string or data quantity. In the later case, the string representation of the quantity value will be used. Example: {"ParameterKey": "facies", "StringParameterKey: "shale"}, {"ParameterKey":"depth", "StringParameterKey":"1545.43m"}.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|DataObjectParameter| |string|optional|Data Object Parameter|Parameter referencing to a top level object.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]*$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|DataQuantityParameter| |number|optional|Data Quantity Parameter|Parameter containing a double value.|(No pattern)|(No format)|UOM_via_property:DataQuantityParameterUOMID|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|IntegerQuantityParameter| |integer|optional|Integer Quantity Parameter|Parameter containing an integer value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|StringParameter| |string|optional|String Parameter|Parameter containing a string value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|TimeIndexParameter| |string|optional|Time Index Parameter|Parameter containing a time index value.  It is assumed that all TimeIndexParameters within an Activity have the same date-time format, which is then described by the FrameOfReference mechanism.|(No pattern)|date-time|DateTime|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ParameterKindID &rarr; [ParameterKind](../reference-data/ParameterKind.1.0.0.md)| |string|required|Parameter Kind Id|[Added to cover lack of inheritance]|<code>^[\w\-\.]+:reference-data\-\-ParameterKind:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ParameterKind](../reference-data/ParameterKind.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|ParameterRoleID &rarr; [ParameterRole](../reference-data/ParameterRole.1.0.0.md)| |string|optional|Parameter Role|Reference data describing how the parameter was used by the activity, such as input, output, control, constraint, agent, predecessor activity, successor activity.|<code>^[\w\-\.]+:reference-data\-\-ParameterRole:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [ParameterRole](../reference-data/ParameterRole.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|DataQuantityParameterUOMID &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)| |string|optional|Data Quantity Parameter UoM ID|Identifies unit of measure for floating point value.|<code>^[\w\-\.]+:reference-data\-\-UnitOfMeasure:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [UnitOfMeasure](../reference-data/UnitOfMeasure.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractActivityParameter.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractProjectActivity.1.0.0](AbstractProjectActivity.1.0.0.md) | Parameters[] | many (as array) |  |
| Included by | [AbstractProjectActivity.1.1.0](AbstractProjectActivity.1.1.0.md) | Parameters[] | many (as array) |  |
| Included by | [AbstractWPCActivity.1.0.0](AbstractWPCActivity.1.0.0.md) | Parameters[] | many (as array) |  |
| Included by | [AbstractWPCActivity.1.1.0](AbstractWPCActivity.1.1.0.md) | Parameters[] | many (as array) |  |
| Included by | [ActivityTemplate.1.0.0](../master-data/ActivityTemplate.1.0.0.md) | Parameters[].DefaultValue | many (as array) |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._