<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# Currency
Used to describe the type of currencies.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:reference-data--Currency:1.0.0`
* Schema status: PUBLISHED
* Governance Authorities: ISO
* Reference value type, governance model: OPEN
* Link to &rarr; [Authoring Schema Currency.1.0.0.json](../../Authoring/reference-data/Currency.1.0.0.json)
* Link to &rarr; [Generated Schema Currency.1.0.0.json](../../Generated/reference-data/Currency.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource Currency.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//reference-data/Currency.1.0.0.json)
* Link to &rarr; [Example Record Currency.1.0.0.json](../../Examples/reference-data/Currency.1.0.0.json) **Note:** this is auto-generated and not intended to be meaningful from a domain perspective.

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams Currency


## Outgoing Relationships for Currency

![Currency](../_diagrams/reference-data/Currency.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Currency Referenced by other Entities

![Currency](../_diagrams/reference-data/Currency.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Currency Properties

## 1. Table of Currency System Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|id| |string|optional|Entity ID|Previously called ResourceID or SRN which identifies this OSDU resource object without version.|<code>^[\w\-\.]+:reference-data\-\-Currency:[\w\-\.\:\%]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|namespace:reference-data--Currency:04151c09-3f1a-5335-be3d-af960375c340|
|kind| |string|required|Entity Kind|The schema identification for the OSDU resource object following the pattern {Namespace}:{Source}:{Type}:{VersionMajor}.{VersionMinor}.{VersionPatch}. The versioning scheme follows the semantic versioning, https://semver.org/.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.]+:[0-9]+.[0-9]+.[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|osdu:wks:reference-data--Currency:1.0.0|
|version| |integer|optional|Version Number|The version number of this OSDU resource; set by the framework.|(No pattern)|int64|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|1562066009929332|
|_acl_|_[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)_|_object_|_required_|_Access Control List_|_The access control tags associated with this entity. Fragment Description: The access control tags associated with this entity. This structure is included by the SystemProperties "acl", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|acl.owners[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Owners|The list of owners of this data record formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|acl.viewers[]|[AbstractAccessControlList.1.0.0](../abstract/AbstractAccessControlList.1.0.0.md)|string|required|List of Viewers|The list of viewers to which this data record is accessible/visible/discoverable formatted as an email (core.common.model.storage.validation.ValidationDoc.EMAIL_REGEX).|<code>^[a-zA-Z0-9_+&amp;*-]+(?:\.[a-zA-Z0-9_+&amp;*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_legal_|_[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)_|_object_|_required_|_Legal Meta Data_|_The entity's legal tags and compliance status. The actual contents associated with the legal tags is managed by the Compliance Service. Fragment Description: Legal meta data like legal tags, relevant other countries, legal status. This structure is included by the SystemProperties "legal", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|legal.legaltags[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Legal Tags|The list of legal tags, which resolve to legal properties (like country of origin, export classification code, etc.) and rules with the help of the Compliance Service.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.otherRelevantDataCountries[]|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|required|Other Relevant Data Countries|The list of other relevant data countries as an array of two-letter country codes, see https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.|<code>^[A-Z]{2}$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|legal.status|[AbstractLegalTags.1.0.0](../abstract/AbstractLegalTags.1.0.0.md)|string|optional|Legal Status|The legal status. Set by the system after evaluation against the compliance rules associated with the "legaltags" using the Compliance Service.|<code>^(compliant&#124;uncompliant)$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|_tags_| |_object_|_optional_|_Tag Dictionary_|_A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_{"NameOfKey": "String value"}_|
|tags| |object|optional|Tag Dictionary|A generic dictionary of string keys mapping to string value. Only strings are permitted as keys and values.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|{"NameOfKey": "String value"}|
|createTime| |string|optional|Resource Object Creation DateTime|Timestamp of the time at which initial version of this OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:46:20.163Z|
|createUser| |string|optional|Resource Object Creation User Reference|The user reference, which created the first version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|modifyTime| |string|optional|Resource Object Version Creation DateTime|Timestamp of the time at which this version of the OSDU resource object was created. Set by the System. The value is a combined date-time string in ISO-8601 given in UTC.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2020-12-16T11:52:24.477Z|
|modifyUser| |string|optional|Resource Object Version Creation User Reference|The user reference, which created this version of this resource object. Set by the System.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|some-user@some-company-cloud.com|
|_ancestry_|_[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)_|_object_|_optional_|_Parent List_|_The links to data, which constitute the inputs, from which this record instance is derived. Fragment Description: A list of entity id:version references to record instances recorded in the data platform, from which the current record is derived and from which the legal tags must be derived. This structure is included by the SystemProperties "ancestry", which is part of all OSDU records. Not extensible._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|ancestry.parents[]|[AbstractLegalParentList.1.0.0](../abstract/AbstractLegalParentList.1.0.0.md)|string|optional|Parents|An array of none, one or many entity references of 'direct parents' in the data platform, which mark the current record as a derivative. In contrast to other relationships, the source record version is required. During record creation or update the ancestry.parents[] relationships are used to collect the legal tags from the sources and aggregate them in the legal.legaltags[] array. As a consequence, should e.g., one or more of the legal tags of the source data expire, the access to the derivatives is also terminated. For details, see ComplianceService tutorial, 'Creating derivative Records'.|<code>^[\w\-\.]+:[\w\-\.]+:[\w\-\.\:\%]+:[0-9]+$</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|[]|
|_meta[]_|_[AbstractMetaItem.1.0.0](../abstract/AbstractMetaItem.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_Frame of Reference Meta Data Item_|_The Frame of Reference meta data section linking the named properties to self-contained definitions. Fragment Description: A meta data item, which allows the association of named properties or property values to a Unit/Measurement/CRS/Azimuth/Time context._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of Currency Data Properties, Section AbstractCommonResources

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractCommonResources.1.0.0](../abstract/AbstractCommonResources.1.0.0.md) (nested structure, follow link for details)_|_object_|_optional_|_OSDU Common Resources_|_Common resources to be injected at root 'data' level for every entity, which is persistable in Storage. The insertion is performed by the OsduSchemaComposer script._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of Currency Data Properties, Section AbstractReferenceType

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)_|_object_|_optional_|_AbstractReferenceType_|_Generic reference object containing the universal properties of reference data, especially the ones commonly thought of as Types_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.Name|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Name|The name of the entity instance.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|1|(No example)|
|_data.NameAlias[]_|_[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)_|_object_|_optional_|_AbstractAliasNames_|_Alternative names, including historical, by which this entity instance is/has been known. Fragment Description: A list of alternative names for an object.  The preferred name is in a separate, scalar property.  It may or may not be repeated in the alias list, though a best practice is to include it if the list is present, but to omit the list if there are no other names.  Note that the abstract entity is an array so the $ref to it is a simple property reference._|_(No pattern)_|_(No format)_|_(No frame of reference)_|_nested_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.NameAlias[].AliasName|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name|Alternative Name value of defined name type for an object.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].AliasNameTypeID &rarr; [AliasNameType](AliasNameType.1.0.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Alias Name Type Id|A classification of alias names such as by role played or type of source, such as regulatory name, regulatory code, company code, international standard name, etc.|<code>^[\w\-\.]+:reference-data\-\-AliasNameType:[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [AliasNameType](AliasNameType.1.0.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].DefinitionOrganisationID &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Definition Organisation Id|The StandardsOrganisation (reference-data) or Organisation (master-data) that provided the name (the source).|<code>^[\w\-\.]+:(reference-data\-\-StandardsOrganisation&#124;master-data\-\-Organisation):[\w\-\.\:\%]+:[0-9]*$</code> &rarr; [StandardsOrganisation](StandardsOrganisation.1.0.0.md) &rarr; [Organisation](../master-data/Organisation.1.1.0.md)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].EffectiveDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Effective Date Time|The date and time when an alias name becomes effective.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.NameAlias[].TerminationDateTime|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md) [AbstractAliasNames.1.0.0](../abstract/AbstractAliasNames.1.0.0.md)|string|optional|Termination Date Time|The data and time when an alias name is no longer in effect.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.ID|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|External Identifier|Native identifier from a Master Data Management System or other trusted source external to OSDU - stored here in order to allow for multi-system connection and synchronization. If used, the "Source" property should identify that source system.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.InactiveIndicator|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|boolean|optional|Inactive Indicator|By default reference values are considered as 'active'. An absent 'InactiveIndicator' property value means the reference value is in active use. When 'InactiveIndicator' is set true the reverence value is no longer in use and should no longer be offered as a choice.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Description|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Description|The text which describes a NAME TYPE in detail.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.Code|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Code|The abbreviation or mnemonic for a reference type if defined. Example: WELL and WLBR.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|0|(No example)|
|data.AttributionAuthority|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Authority|Name of the authority, or organisation, which governs the entity value and from which it is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionPublication|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Publication|Name, URL, or other identifier of the publication, or repository, of the attribution source organisation from which the entity value is sourced.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.AttributionRevision|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Attribution Revision|The distinct instance of the attribution publication, by version number, sequence number, date of publication, etc., that was used for the entity value.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CommitDate|[AbstractReferenceType.1.0.0](../abstract/AbstractReferenceType.1.0.0.md)|string|optional|Commit Date|For reference values published and governed by OSDU: The date and time the record was committed into the OSDU member GitLab reference-values repository. The sole purpose of this date is to optimise the OSDU milestone upgrades. It allows the upgrade code to figure out whether or not the record must be PUT into reference value storage.|(No pattern)|date-time|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|2021-02-08T21:36:40-05:00|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of Currency Data Properties, Section IndividualProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_data_| |_object_|_optional_|_IndividualProperties_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|data.CurrencySymbolText| |string|optional|Currency Symbol Text|The symbol for the currency.|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|data.CurrencyCodeNumericNumber| |integer|optional|Currency Code Numeric Number|Standard numeric number assigned to a currency|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of Currency Data Properties, Section ExtensionProperties

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|data.ExtensionProperties| |object|optional|Extension Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Reference Values for Currency.1.0.0

* Status: Published
* Governance: OPEN
* The table below **is truncated** and shows only 100 of total 179 values in the reference-values repository's manifest.
* Link to the full reference value manifest for &rarr; [Currency](../../ReferenceValues/Manifests/reference-data/OPEN/Currency.1.0.0.json).

|`id` with prefix `namespace:reference-data--Currency:`|Code|Name|Alias|Description|AttributionAuthority|AttributionPublication|AttributionRevision|CurrencyCodeNumericNumber|CurrencySymbolText|Source|CommitDate|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|AFN|AFN|Afghani|-|Currency used in Afghanistan.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|971|؋|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ALL|ALL|Lek|-|Currency used in Albania.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|8|L|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|AMD|AMD|Armenian Dram|-|Currency used in Armenia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|51|դր.|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ANG|ANG|Netherlands Antillean Guilder|-|Currency used in Curaçao, Sint Maarten (Dutch part).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|532|ƒ|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|AOA|AOA|Kwanza|-|Currency used in Angola.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|973|Kz|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ARS|ARS|Argentine Peso|-|Currency used in Argentina.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|32|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|AUD|AUD|Australian Dollar|-|Currency used in Australia, Christmas Island, Cocos (Keeling) Islands (the), Heard Island and McDonald Islands, Kiribati, Nauru, Norfolk Island, Tuvalu.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|36|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|AWG|AWG|Aruban Florin|-|Currency used in Aruba.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|533|ƒ|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|AZN|AZN|Azerbaijan Manat|-|Currency used in Azerbaijan.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|944|m|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BAM|BAM|Convertible Mark|-|Currency used in Bosnia and Herzegovina.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|977|КМ|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BBD|BBD|Barbados Dollar|-|Currency used in Barbados.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|52|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BDT|BDT|Taka|-|Currency used in Bangladesh.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|50|৳|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BGN|BGN|Bulgarian Lev|-|Currency used in Bulgaria.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|975|лв|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BHD|BHD|Bahraini Dinar|-|Currency used in Bahrain.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|48|.د.ب|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BIF|BIF|Burundi Franc|-|Currency used in Burundi.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|108|Fr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BMD|BMD|Bermudian Dollar|-|Currency used in Bermuda.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|60|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BND|BND|Brunei Dollar|-|Currency used in Brunei Darussalam.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|96|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BOB|BOB|Boliviano|-|Currency used in Bolivia (Plurinational State of).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|68|Bs.|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BOV|BOV|Mvdol|-|Currency used in Bolivia (Plurinational State of).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|984|-|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BRL|BRL|Brazilian Real|-|Currency used in Brazil.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|986|R$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BSD|BSD|Bahamian Dollar|-|Currency used in Bahamas (the).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|44|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BTN|BTN|Ngultrum|-|Currency used in Bhutan.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|64|Nu.|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BWP|BWP|Pula|-|Currency used in Botswana.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|72|P|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BYN|BYN|Belarusian Ruble|-|Currency used in Belarus.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|933|Br|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|BZD|BZD|Belize Dollar|-|Currency used in Belize.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|84|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CAD|CAD|Canadian Dollar|-|Currency used in Canada.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|124|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CDF|CDF|Congolese Franc|-|Currency used in Congo (the Democratic Republic of the).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|976|Fr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CHF|CHF|Swiss Franc|-|Currency used in Liechtenstein, Switzerland.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|756|Fr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CLF|CLF|Unidad de Fomento|-|Currency used in Chile.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|990|-|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CLP|CLP|Chilean Peso|-|Currency used in Chile.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|152|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CNY|CNY|Yuan Renminbi|-|Currency used in China.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|156|¥|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|COP|COP|Colombian Peso|-|Currency used in Colombia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|170|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|COU|COU|Unidad de Valor Real|-|Currency used in Colombia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|970|-|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CRC|CRC|Costa Rican Colon|-|Currency used in Costa Rica.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|188|₡|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CUC|CUC|Peso Convertible|-|Currency used in Cuba.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|931|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CUP|CUP|Cuban Peso|-|Currency used in Cuba.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|192|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CVE|CVE|Cabo Verde Escudo|-|Currency used in Cabo Verde.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|132|Esc or $|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|CZK|CZK|Czech Koruna|-|Currency used in Czechia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|203|Kč|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|DJF|DJF|Djibouti Franc|-|Currency used in Djibouti.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|262|Fr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|DKK|DKK|Danish Krone|-|Currency used in Denmark, Faroe Islands (the), Greenland.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|208|kr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|DOP|DOP|Dominican Peso|-|Currency used in Dominican Republic (the).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|214|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|DZD|DZD|Algerian Dinar|-|Currency used in Algeria.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|12|د.ج|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|EGP|EGP|Egyptian Pound|-|Currency used in Egypt.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|818|ج.م|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ERN|ERN|Nakfa|-|Currency used in Eritrea.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|232|Nfk|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ETB|ETB|Ethiopian Birr|-|Currency used in Ethiopia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|230|Br|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|EUR|EUR|Euro|-|Currency used in Åland Islands, Andorra, Austria, Belgium, Cyprus, Estonia, Finland, France, French Guiana, French Southern Territories (the), Germany, Greece, Guadeloupe, Holy See (the), Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Martinique, Mayotte, Monaco, Montenegro, Netherlands (the), Portugal, Réunion, Saint Barthélemy, Saint Martin (French part), Saint Pierre and Miquelon, San Marino, Slovakia, Slovenia, Spain.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|978|€|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FJD|FJD|Fiji Dollar|-|Currency used in Fiji.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|242|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|FKP|FKP|Falkland Islands Pound|-|Currency used in Falkland Islands (the) [Malvinas].|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|238|£|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|GBP|GBP|Pound Sterling|-|Currency used in Guernsey, Isle of Man, Jersey, United Kingdom of Great Britain and Northern Ireland (the).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|826|£|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|GEL|GEL|Lari|-|Currency used in Georgia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|981|ლ|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|GHS|GHS|Ghana Cedi|-|Currency used in Ghana.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|936|₵|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|GIP|GIP|Gibraltar Pound|-|Currency used in Gibraltar.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|292|£|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|GMD|GMD|Dalasi|-|Currency used in Gambia (the).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|270|D|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|GNF|GNF|Guinean Franc|-|Currency used in Guinea.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|324|Fr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|GTQ|GTQ|Quetzal|-|Currency used in Guatemala.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|320|Q|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|GYD|GYD|Guyana Dollar|-|Currency used in Guyana.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|328|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|HKD|HKD|Hong Kong Dollar|-|Currency used in Hong Kong.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|344|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|HNL|HNL|Lempira|-|Currency used in Honduras.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|340|L|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|HRK|HRK|Kuna|-|Currency used in Croatia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|191|kn|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|HTG|HTG|Gourde|-|Currency used in Haiti.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|332|G|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|HUF|HUF|Forint|-|Currency used in Hungary.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|348|Ft|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|IDR|IDR|Rupiah|-|Currency used in Indonesia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|360|Rp|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ILS|ILS|New Israeli Sheqel|-|Currency used in Israel.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|376|₪|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|INR|INR|Indian Rupee|-|Currency used in Bhutan, India.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|356|₹|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|IQD|IQD|Iraqi Dinar|-|Currency used in Iraq.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|368|ع.د|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|IRR|IRR|Iranian Rial|-|Currency used in Iran (Islamic Republic of).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|364|﷼|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ISK|ISK|Iceland Krona|-|Currency used in Iceland.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|352|kr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|JMD|JMD|Jamaican Dollar|-|Currency used in Jamaica.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|388|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|JOD|JOD|Jordanian Dinar|-|Currency used in Jordan.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|400|د.ا|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|JPY|JPY|Yen|-|Currency used in Japan.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|392|¥|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|KES|KES|Kenyan Shilling|-|Currency used in Kenya.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|404|Sh|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|KGS|KGS|Som|-|Currency used in Kyrgyzstan.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|417|лв|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|KHR|KHR|Riel|-|Currency used in Cambodia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|116|៛|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|KMF|KMF|Comorian Franc|-|Currency used in Comoros (the).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|174|Fr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|KPW|KPW|North Korean Won|-|Currency used in .|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|408|₩|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|KRW|KRW|Won|-|Currency used in Korea (the Republic of).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|410|₩|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|KWD|KWD|Kuwaiti Dinar|-|Currency used in Kuwait.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|414|د.ك|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|KYD|KYD|Cayman Islands Dollar|-|Currency used in Cayman Islands (the).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|136|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|KZT|KZT|Tenge|-|Currency used in Kazakhstan.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|398|₸|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|LAK|LAK|Lao Kip|-|Currency used in .|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|418|₭|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|LBP|LBP|Lebanese Pound|-|Currency used in Lebanon.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|422|ل.ل|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|LRD|LRD|Liberian Dollar|-|Currency used in Liberia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|430|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|LSL|LSL|Loti|-|Currency used in Lesotho.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|426|L|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|LYD|LYD|Libyan Dinar|-|Currency used in Libya.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|434|ل.د|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MGA|MGA|Malagasy Ariary|-|Currency used in Madagascar.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|969|Ar|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MKD|MKD|Denar|-|Currency used in North Macedonia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|807|ден|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MOP|MOP|Pataca|-|Currency used in Macao.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|446|P|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MVR|MVR|Rufiyaa|-|Currency used in Maldives.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|462|.ރ|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MWK|MWK|Malawi Kwacha|-|Currency used in Malawi.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|454|MK|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|MYR|MYR|Malaysian Ringgit|-|Currency used in Malaysia.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|458|RM|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NOK|NOK|Norwegian Krone|-|Currency used in Bouvet Island, Norway, Svalbard and Jan Mayen.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|578|kr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|NZD|NZD|New Zealand Dollar|-|Currency used in Cook Islands (the), New Zealand, Niue, Pitcairn, Tokelau.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|554|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SVC|SVC|El Salvador Colon|-|Currency used in El Salvador.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|222|₡|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|SZL|SZL|Lilangeni|-|Currency used in Eswatini.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|748|L|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|USD|USD|US Dollar|-|Currency used in American Samoa, Bonaire, Sint Eustatius and Saba, British Indian Ocean Territory (the), Ecuador, El Salvador, Guam, Haiti, Marshall Islands (the), Micronesia (Federated States of), Northern Mariana Islands (the), Palau, Panama, Puerto Rico, Timor-Leste, Turks and Caicos Islands (the), United States Minor Outlying Islands (the), United States of America (the), Virgin Islands (British), Virgin Islands (U.S.).|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|840|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|XAF|XAF|CFA Franc BEAC|-|Currency used in Cameroon, Central African Republic (the), Chad, Congo (the), Equatorial Guinea, Gabon.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|950|Fr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|XCD|XCD|East Caribbean Dollar|-|Currency used in Anguilla, Antigua and Barbuda, Dominica, Grenada, Montserrat, Saint Kitts and Nevis, Saint Lucia, Saint Vincent and the Grenadines.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|951|$|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|XDR|XDR|SDR (Special Drawing Right)|-|Currency used in .|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|960|-|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|XOF|XOF|CFA Franc BCEAO|-|Currency used in Benin, Burkina Faso, Côte d&#x27;Ivoire, Guinea-Bissau, Mali, Niger (the), Senegal, Togo.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|952|Fr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|XPF|XPF|CFP Franc|-|Currency used in French Polynesia, New Caledonia, Wallis and Futuna.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|953|Fr|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|
|ZAR|ZAR|Rand|-|Currency used in Lesotho, Namibia, South Africa.|ISO|List one: Currency, fund and precious metal codes|Published: 2018-08-29 00:00:00|710|R|Workbook Published/Currency.1.0.0.xlsx; commit SHA c1d72417.|2021-03-16T11:23:02+01:00|

**_This reference value type contains more than 100 entries and is therefore truncated. See the reference-values repository for the complete list._**


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# Currency.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Referred to by | [AbstractProject.1.0.0](../abstract/AbstractProject.1.0.0.md) | FundsAuthorizations[].CurrencyID | many (as array) |  |
| Referred to by | [AbstractReferenceValueUpgrade.1.0.0](../abstract/AbstractReferenceValueUpgrade.1.0.0.md) | SupersededBy | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ReferenceValueID | one | Untyped relationship |
| Referred to by | [ExternalReferenceValueMapping.1.0.0](ExternalReferenceValueMapping.1.0.0.md) | ComplexMappings[].ReferenceValueID | many (as array) | Untyped relationship |
| Referred to by | [GenericImage.1.0.0](../work-product-component/GenericImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [GeoReferencedImage.1.0.0](../work-product-component/GeoReferencedImage.1.0.0.md) | AssociatedObjectID | one | Untyped relationship |
| Referred to by | [PersistedCollection.1.0.0](../work-product-component/PersistedCollection.1.0.0.md) | MemberIDs[] | many (as array) | Untyped relationship |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._