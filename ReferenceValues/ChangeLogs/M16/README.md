<a name="TOC"></a>**Table of Contents**

[[_TOC_]]

Changes since: 2022-11-09

* [Detailed Change Log](ChangesAfter-2022-11-09.md) for reference values.

# Reference Value Changes

## CoordinateTransformation:1.1.0 (CRS_CT)

The catalog has been updated to:

```text
IOGP Revision status as per last VersionHistory record:
  IOGP VersionHistory Code: 315
  IOGP VersionHistory Name: 10.081
  IOGP VersionHistory Date: 2023-01-17T00:00:00
  IOGP VersionHistory URL : https://apps.epsg.org/api/v1/VersionHistory/315
```

A detailed list of changes can be [found here](ChangesAfter-2022-11-09.md#osduwksreference-data-coordinatetransformation110). 

## CoordinateReferenceSystem:1.1.0 (CRS_CT)

Two BoundProjected coordinate reference systems have been found inadequate due to misaligned extents and removed:

1. Code: `BoundProjected:EPSG::2596_EPSG::1807`, Name: "Pulkovo 1942 * BP-Aze Aioc95 / 3-degree Gauss-Kruger CM
   63E [2596,1807]"
2. Code: `BoundProjected:EPSG::2596_EPSG::1808`, Name: "Pulkovo 1942 * BP-Aze Aioc97 / 3-degree Gauss-Kruger CM
   63E [2596,1808]"

The two CRSs have not been deprecated. If deployed earlier and used, usage is not impacted. New deployments will not
contain the two CRSs. If operators want to stay à jour with OSDU deployments, the two records need to be removed
manually.

Since the catalog manifests are updated, the opportunity is used to refresh the definitions with the [January 17, 2023,
version](https://apps.epsg.org/api/v1/VersionHistory/315) of the EPSG registry. The version is:

```text
IOGP Revision status as per last VersionHistory record:
  IOGP VersionHistory Code: 315
  IOGP VersionHistory Name: 10.081
  IOGP VersionHistory Date: 2023-01-17T00:00:00
  IOGP VersionHistory URL : https://apps.epsg.org/api/v1/VersionHistory/315
```
A detailed list of changes can be [found here](ChangesAfter-2022-11-09.md#osduwksreference-data-coordinatereferencesystem110). 

[Back to TOC](#TOC)

---

## ConventionalCoreType:1.0.1

ConventionalCoreType is now under PPDM governance (FIXED OSDU governance mode). The schema is patched to carry PPDM's
description text: _An interval of rock core extracted from the bottom of a wellbore._

### Recommended ConventionalCoreType Values

| `id`                                                           | Name               | Description                                                                                                                                                     | 
|----------------------------------------------------------------|--------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--ConventionalCoreType:Conventional | Conventional core  | Standard or routine cylindrical continuous core.                                                                                                                |-|
| partition-id:reference-data--ConventionalCoreType:FullClosure  | Full closure core  | This coring system for unconsolidated sediments has a special device to close the barrel before the assembly is lifted off the bottom of the hole.              |-|
| partition-id:reference-data--ConventionalCoreType:Piston       | Piston core        | A heavily weighted core tube is dropped into the seabed; a piston inside the tube creates a vacuum that draws the unconsolidated sediment upward into the tube. |-|
| partition-id:reference-data--ConventionalCoreType:Pressure     | Pressure core      | The coring system holds the core at the reservoir pressure, thus retaining all fluids in the core.                                                              |-|
| partition-id:reference-data--ConventionalCoreType:RubberSleeve | Rubber sleeve core | The core barrel has an inner rubber sleeve to aid in recovering unconsolidated or highly fractured rock.                                                        |-|
| partition-id:reference-data--ConventionalCoreType:Sponge       | Sponge core        | The core barrel has a sponge liner that traps the reservoir oil while allowing gas and water to escape.                                                         |-|
| partition-id:reference-data--ConventionalCoreType:Unspecified  | Unspecified        | -                                                                                                                                                               |-|

### Deprecated ConventionalCoreType Values

| Deprecated `id`                                                    | Superseded by ìd`                                              | Superseded Name | Description                                                                                                                                                                                                       |
|--------------------------------------------------------------------|----------------------------------------------------------------|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--ConventionalCoreType:ConventionalCore | partition-id:reference-data--ConventionalCoreType:Conventional | Conventional    | DEPRECATED: Please use Code="Conventional" with Name="Conventional" instead. Standard or routine cylindrical continuous core, typically cut in 2.375-5.5 inch diameters, and in 6 or 9 metre core barrel lengths. |
| partition-id:reference-data--ConventionalCoreType:DropCore         | partition-id:reference-data--ConventionalCoreType:Piston       | Piston          | DEPRECATED: Please use Code="Piston" with Name="Piston" instead. Shallow sediment cores typically collected for geo-technical reasons, for example, rig stability or rig leg penetration.                         |

[Back to TOC](#TOC)

---

## ExternalReferenceValueMapping:1.0.0 (New)

This reference value list only contains examples - not to be taken seriously even as a starting point. The example
records only explain three ways of usage.

* The examples use the `ExternalCatalogNamespace:EDS`. For completeness, EDS has been added to the
  ExternalCatalogNamespace list (LOCAL governance).
* The data.Description explains the class/purpose of the example:

| `id`                                                                                          | Name                               | Description                                                                                                   |
|-----------------------------------------------------------------------------------------------|------------------------------------|---------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.Added                   | EDS.Global.Added                   | Example of multiple ResourceCurationStatus mapping records without NameAlias (many records, simpler look-up). |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.Created                 | EDS.Global.Created                 | Example of multiple ResourceCurationStatus mapping records without NameAlias (many records, simpler look-up). |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.OFFSHORE                | EDS.Global.OFFSHORE                | Example of multiple OperatingEnvironment mapping records without NameAlias (many records, simpler look-up).   |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.Offshore                | EDS.Global.Offshore                | Example of multiple OperatingEnvironment mapping records without NameAlias (many records, simpler look-up).   |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.Off                     | EDS.Global.Off                     | Example of multiple OperatingEnvironment mapping records without NameAlias (many records, simpler look-up).   |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.OFF                     | EDS.Global.OFF                     | Example of multiple OperatingEnvironment mapping records without NameAlias (many records, simpler look-up).   |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.ONSHORE                 | EDS.Global.ONSHORE                 | Example of multiple OperatingEnvironment mapping records without NameAlias (many records, simpler look-up).   |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.onshore                 | EDS.Global.onshore                 | Example of multiple OperatingEnvironment mapping records without NameAlias (many records, simpler look-up).   |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.ON                      | EDS.Global.ON                      | Example of multiple OperatingEnvironment mapping records without NameAlias (many records, simpler look-up).   |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.on                      | EDS.Global.on                      | Example of multiple OperatingEnvironment mapping records without NameAlias (many records, simpler look-up).   |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.OpsEnvironment.Offshore | EDS.Global.OpsEnvironment.Offshore | Example of a single OperatingEnvironment mapping record with NameAlias (less records, more complex query).    |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.OpsEnvironment.Onshore  | EDS.Global.OpsEnvironment.Onshore  | Example of a single OperatingEnvironment mapping record with NameAlias (less records, more complex query).    |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.SpudDate                | EDS.Global.SpudDate                | Example of multiple FacilityEventType mapping records without NameAlias (many records, simpler look-up).      |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.Spuddet                 | EDS.Global.Spuddet                 | Example of multiple FacilityEventType mapping records without NameAlias (many records, simpler look-up).      |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.TDR                     | EDS.Global.TDR                     | Example of multiple FacilityEventType mapping records without NameAlias (many records, simpler look-up).      |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Global.Well                    | EDS.Global.Well                    | Mapping example for an entity type to FacilityType.                                                           |-|
| partition-id:reference-data--ExternalReferenceValueMapping:EDS.Wellbore.Status.GAS-IDLE       | EDS.Wellbore.Status.GAS-IDLE       | Example for a complex mapping record from one source value to multiple property/value pairs.                  |-|


[Back to TOC](#TOC)

---

## FluidContactType:1.0.0

A new FIXED (RESQML informed) reference value list used by [work-product-component--FluidBoundaryInterpretation:1.0.0](../../../E-R/work-product-component/FluidBoundaryInterpretation.1.0.0.md).

| `id`                                                           | Name               | Description                                 |
|----------------------------------------------------------------|--------------------|---------------------------------------------|
| partition-id:reference-data--FluidContactType:FreeWaterContact | Free Water Contact | Water, lower level.                         |-|
| partition-id:reference-data--FluidContactType:GasOilContact    | Gas Oil Contact    | The separation level between gas and oil.   |-|
| partition-id:reference-data--FluidContactType:GasWaterContact  | Gas Water Contact  | The separation level between water and gas. |-|
| partition-id:reference-data--FluidContactType:Seal             | Seal               | The sealing level.                          |-|
| partition-id:reference-data--FluidContactType:WaterOilContact  | Water Oil Contact  | The separation level between water and oil. |-|

[Back to TOC](#TOC)

---

## FormationIntegrityTest

The new work-product-component--FormationIntegrityTest comes with 5 new reference-data types and associated
catalog/manifests:

###  FormationIntegritySurfacePressureDataSource:1.0.0

* [FormationIntegritySurfacePressureDataSource](../../../E-R/reference-data/FormationIntegritySurfacePressureDataSource.1.0.0.md) &mdash; _The measurement source for Formation Integrity Surface Pressure readings._
* [Manifest](../../Manifests/reference-data/OPEN/FormationIntegrityPressureDataSource.1.0.0.json)

### FormationIntegrityTestResult:1.0.0 

* [FormationIntegrityTestResult:1.0.0](../../../E-R/reference-data/FormationIntegrityTestResult.1.0.0.md) &mdash; _The set of outcome types from a completed Formation Integrity Test._
* [Manifest](../../Manifests/reference-data/OPEN/FormationIntegrityTestResult.1.0.0.json)

### FormationIntegrityTestType:1.0.0

* [FormationIntegrityTestType:1.0.0](../../../E-R/reference-data/FormationIntegrityTestType.1.0.0.md) &mdash; _The list of different types of Formation Integrity Tests that can be performed._
* [Manifest](../../Manifests/reference-data/OPEN/FormationIntegrityTestType.1.0.0.json)

### FormationPressureTestType:1.0.0

* [FormationPressureTestType:1.0.0](../../../E-R/reference-data/FormationPressureTestType.1.0.0.md) &mdash; _FIT, LOT, XLOT etc. Proposed reference values: Formation Integrity Test, Leak Off Test, Extended Leak Off Test
* [Manifest](../../Manifests/reference-data/OPEN/FormationPressureTestType.1.0.0.json)


### MudBaseType:1.0.0

* [MudBaseType:1.0.0](../../../E-R/reference-data/MudBaseType.1.0.0.md) &mdash; _The base fluid type of the Mud/Drilling Fluid in well at time of the Formation Integrity test._
* [Manifest](../../Manifests/reference-data/OPEN/MudBaseType.1.0.0.json)


[Back to TOC](#TOC)

---

## ImageLightingCondition:1.0.0

A new LOCAL [reference list](../../../E-R/reference-data/ImageLightingCondition.1.0.0.md) in conjunction with 
[RockImage](../../../E-R/work-product-component/RockImage.1.0.0.md).
"The type of lighting conditions captured in the image."

The Manifest is located [here](../../Manifests/reference-data/LOCAL/ImageLightingCondition.1.0.0.json).

[Back to TOC](#TOC)

---

## MarkerType:1.0.1

MarkerType is now under PPDM governance (OPEN OSDU governance mode). The schema is patched to carry PPDM's description text. 

### Recommended MarkerType Values 

| `id`                                                         | Name                  | Description                                                                                                                                                                                                 |  
|--------------------------------------------------------------|-----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--MarkerType:Biostratigraphy      | Biostratigraphy       | A marker associated with a bio stratigraphic horizon. Biostratigraphy assigns relative ages of rock strata by using the fossil assemblages contained within them.                                           | 
| partition-id:reference-data--MarkerType:Chronostratigraphy   | Chronostratigraphy    | A marker associated with a chronostratigraphic horizon. Chronostratigraphic studies the ages of rock strata in relation to time.                                                                            | 
| partition-id:reference-data--MarkerType:FluidStratigraphy    | Fluid Stratigraphy    | A marker associated with seismic reflection related to a uniform fluid interface (e.g. oil-water contact).                                                                                                  | 
| partition-id:reference-data--MarkerType:Lithostratigraphy    | Lithostratigraphy     | A marker associated with a lithostratigraphic horizon. Lithostratigraphy studies rock layers on the basis of their physical composition and structure.                                                      | 
| partition-id:reference-data--MarkerType:Magnetostratigraphy  | Magnetostratigraphy   | A magneto stratigraphic marker. Magneto-stratigraphy is a geophysical correlation technique used to date sedimentary and volcanic sequences.                                                                | 
| partition-id:reference-data--MarkerType:SequenceStratigraphy | Sequence Stratigraphy | A marker in a sequence stratigraphy interpretation. Sequence stratigraphy divides sedimentary deposits into units separated by unconformity surfaces as a way to model the depositional history of a basin. | 
| partition-id:reference-data--MarkerType:Unspecified          | Unspecified           | A general kind of seismic marker having no stratigraphic interpretation.                                                                                                                                    | 

### Deprecated MarkerType Values 

| Deprecated `id`                                             | Superseded by ìd`                                            | Superseded Name      | Description                                                                                                                                                                                                                                                                                  |
|-------------------------------------------------------------|--------------------------------------------------------------|----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--MarkerType:BioStratigraphy     | partition-id:reference-data--MarkerType:Biostratigraphy      | Biostratigraphy      | DEPRECATED: Please use Code="Biostratigraphy" with Name="Biostratigraphy" instead. A bio-stratigraphic marker. Bio-stratigraphy is the branch of stratigraphy which focuses on correlating and assigning relative ages of rock strata by using the fossil assemblages contained within them. |
| partition-id:reference-data--MarkerType:ChronoStratigraphy  | partition-id:reference-data--MarkerType:Chronostratigraphy   | Chronostratigraphy   | DEPRECATED: Please use Code="Chronostratigraphy" with Name="Chronostratigraphy" instead. A chrono-stratigraphic marker. Chrono-stratigraphy is the branch of stratigraphy that studies the ages of rock strata in relation to time.                                                          |
| partition-id:reference-data--MarkerType:LithoStratigraphy   | partition-id:reference-data--MarkerType:Lithostratigraphy    | Lithostratigraphy    | DEPRECATED: Please use Code="Lithostratigraphy" with Name="Lithostratigraphy" instead. A litho-stratigraphic marker. Litho-stratigraphy is a sub-discipline of stratigraphy, the geological science associated with the study of strata or rock layers.                                      |
| partition-id:reference-data--MarkerType:MagnetoStratigraphy | partition-id:reference-data--MarkerType:Magnetostratigraphy  | Magnetostratigraphy  | DEPRECATED: Please use Code="Magnetostratigraphy" with Name="Magnetostratigraphy" instead. A magneto stratigraphic marker. Magneto-stratigraphy is a geophysical correlation technique used to date sedimentary and volcanic sequences.                                                      |
| partition-id:reference-data--MarkerType:Seismic             | partition-id:reference-data--MarkerType:SequenceStratigraphy | SequenceStratigraphy | DEPRECATED: Please use Code="SequenceStratigraphy" with Name="SequenceStratigraphy" instead. A seismic marker, typically based on an interpretation of seismic reflection data at a depth for well log correlation.                                                                          |

[Back to TOC](#TOC)

---

## RepresentationType:1.0.0

The description for code Regular2DGrid is corrected:

| `id`                                                          | Name            | Description                                                                                                                                                                                                                                 |
|---------------------------------------------------------------|-----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--RepresentationType:Regular2DGrid | Regular 2D Grid | A 2D representation based on a regular lattice defined in the (typically projected) coordinate reference system associated to the entity. The lattice may be matching a SeismicBinGrid when used in the context of seismic interpretations. |

[Back to TOC](#TOC)

---

## RockImageType:1.0.0

A new OPEN [reference list](../../../E-R/reference-data/RockImageType.1.0.0.md) in conjunction with
[RockImage](../../../E-R/work-product-component/RockImage.1.0.0.md). "The type of image taken
of a rock sample. For example a photograph or CT image."

The Manifest is located [here](../../Manifests/reference-data/OPEN/RockImageType.1.0.0.json).

[Back to TOC](#TOC)

---

## StratigraphicRoleType

The reference value list was extended by one new code `Sequencestratigraphic`, see 
[manifest](../../Manifests/reference-data/FIXED/StratigraphicRoleType.1.0.0.json).

| `id`                                                                     | Name                  | Description                                                                                                                                                                                                                                                                     | Comment   |
|--------------------------------------------------------------------------|-----------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| partition-id:reference-data--StratigraphicRoleType:Biostratigraphic      | Biostratigraphic      | Organizations  that are defined or characterized on the basis of their contained fossils                                                                                                                                                                                        | Unchanged |
| partition-id:reference-data--StratigraphicRoleType:Chemostratigraphic    | Chemostratigraphic    | Organizations that are defined or characterized on the basis of their chemical content                                                                                                                                                                                          | Unchanged |
| partition-id:reference-data--StratigraphicRoleType:Chronostratigraphic   | Chronostratigraphic   | Organizations, layered or unlayered, that were formed during a specified interval of geologic time                                                                                                                                                                              | Unchanged |
| partition-id:reference-data--StratigraphicRoleType:Lithostratigraphic    | Lithostratigraphic    | Organizations, bedded or unbedded, that are defined and characterized on the basis of their lithologic properties and their stratigraphic relations                                                                                                                             | Unchanged |
| partition-id:reference-data--StratigraphicRoleType:Magnetostratigraphic  | Magnetostratigraphic  | Organizations unified by similar magnetic characteristics which allow it to be differentiated from adjacent rock bodies                                                                                                                                                         | Unchanged |
| partition-id:reference-data--StratigraphicRoleType:Seismicstratigraphic  | Seismicstratigraphic  | Organizations unified by similar seismic characteristics which allow it to be differentiated from adjacent rock bodies                                                                                                                                                          | Unchanged |
| partition-id:reference-data--StratigraphicRoleType:Sequencestratigraphic | Sequencestratigraphic | Organizations that contain repetitive, genetically related strata that are bounded by key stratigraphic surfaces. These key surfaces can be surfaces of erosion or non-deposition (or their correlative conformities), Maximum Flooding Surfaces or Maximum Regressive Surfaces | **New**   |

[Back to TOC](#TOC)

---

## VelocityAnalysisMethod:1.0.1

This reference value list is now governed by PPDM (OPEN governance). The reference list's description has been patched 
to: _The process of selecting the optimal wave speed (velocity) to stack or migrate seismic traces._

### Recommended VelocityAnalysisMethod Values

| `id`                                                                       | Name                    | Description                                                                                                                                                                                                                                                                                                                                            | 
|----------------------------------------------------------------------------|-------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--VelocityAnalysisMethod:DiffractionTomography  | Diffraction tomography  | Velocity obtained by calculations assuming least-time travel paths according to Fermat’s principle rather than Snell’s law bending at cell boundaries.                                                                                                                                                                                                 | -           |
| partition-id:reference-data--VelocityAnalysisMethod:FullWaveformInversion  | Full waveform inversion | Method involves iteratively minimising the difference between data that was acquired in a seismic survey and synthetic data that is generated from a wave simulator with an estimated (velocity) model of the subsurface.                                                                                                                              | -           |
| partition-id:reference-data--VelocityAnalysisMethod:LayerBasedTomography   | Layer-based tomography  | Method divides the earth into layers, allowing for lateral variation of velocity within the layers, instead of subdivision into cells. Tomographic methods include the algebraic reconstruction technique (ART), the simultaneous reconstruction technique (SIRT), and Gauss Seidel methods (q.v.). See Ivansson (1986) and Lo and Inderwiesen (1994). | -           |
| partition-id:reference-data--VelocityAnalysisMethod:NormalMoveout          | Normal moveout          | A method to adjust reflection arrival times to correct for the geophone-source distance (offset) using a NMO (Normal Moveout) constant velocity.                                                                                                                                                                                                       | -           |
| partition-id:reference-data--VelocityAnalysisMethod:ReflectionTomography   | Reflection tomography   | A method for finding the velocity and/or reflectivity distribution from observations of reflection events at various locations and source-receiver offsets.                                                                                                                                                                                            | -           |
| partition-id:reference-data--VelocityAnalysisMethod:Refraction             | Refraction              | Method using seismic refraction in order to obtain velocities of layers                                                                                                                                                                                                                                                                                | -           |
| partition-id:reference-data--VelocityAnalysisMethod:Tomography             | Tomography              | The velocity distribution model divides the space into cells and then computes line integrals along ray paths through the cells.                                                                                                                                                                                                                       | -           |
| partition-id:reference-data--VelocityAnalysisMethod:TransmissionTomography | Transmission tomography | Velocity obtained via borehole-to-borehole, surface-to-borehole, or surface-to-surface observations                                                                                                                                                                                                                                                    | -           |


### Deprecated VelocityAnalysisMethod Value

| Deprecated `id`                                         | Superseded by ìd`                                                 | Superseded Name | Description                                                                                                                                          |
|---------------------------------------------------------|-------------------------------------------------------------------|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| partition-id:reference-data--VelocityAnalysisMethod:NMO | partition-id:reference-data--VelocityAnalysisMethod:NormalMoveout | NormalMoveout   | DEPRECATED: Please use Code="NormalMoveout" with Name="NormalMoveout" instead. Indicates that the velocity analysis method uses NMO (Normal Moveout) |

[Back to TOC](#TOC)

---

## UnitQuantity CostUnits

A separate workbook is provided to generate separate UnitQuantity manifests, which can be loaded on demand on top of the
Energistics Unit of Measure Standard V1.0 unit quantities.

Cost UnitQuantity share a root UnitQuantity, which no units relate to (dimension is set to `"0"`). Specific currency
codes are then declared as children of the root UnitQuantity. The currency code specific UnitQuantity records have
MemberUnits. This design prevents conversions between currencies (provided the converter implementations pay attention
to the unit's UnitQuantity relationship).

### Root UnitQuantity for Costs

| `id`                                                   | Name                                   | Description              |
|--------------------------------------------------------|----------------------------------------|--------------------------|
| partition-id:reference-data--UnitQuantity:cost%2FL3T   | (cost in CURRENCY per volume) per time | Volumetric Cost per Time |-|
| partition-id:reference-data--UnitQuantity:cost%2FL3    | cost in CURRENCY per standard volume   | Cost per Standard Volume |-|
| partition-id:reference-data--UnitQuantity:cost%2FL     | cost in CURRENCY per length            | Cost per Length          |-|
| partition-id:reference-data--UnitQuantity:cost%2FM     | cost in CURRENCY per mass              | Cost per Mass            |-|
| partition-id:reference-data--UnitQuantity:cost%2FT     | cost in CURRENCY per time              | Cost per Time            |-|
| partition-id:reference-data--UnitQuantity:costT2%2FL2M | cost in CURRENCY per energy            | Energy Cost              |-|
| partition-id:reference-data--UnitQuantity:costT3%2FL2M | cost in CURRENCY per power             | Power Cost               |-|
| partition-id:reference-data--UnitQuantity:cost         | cost in CURRENCY                       | Cost                     |-|

### Currency Specific UnitQuantity

The list is alphabetically sorted.

The following list of currencies are provided by OSDU (OSDU governance in contrast to the Energistics QunitQuantity
definitions from Unit of Measure Standard V1.0):
* `ARS` — Argentine Peso
* `AUD` — Australian Dollar
* `BRL` — Brazilian Real
* `CAD` — Canadian Dollar
* `DKK` — Danish Krone
* `EUR` — Euro
* `GBP` — Pound Sterling
* `NOK` — Norwegian Krone
* `RUB` — Russian Ruble
* `USD` — US Dollar

The currency symbols and names are derived from the [Currency:1.0.0](../../Manifests/reference-data/OPEN/Currency.1.0.0.json) manifest.

| `id`                                                                            | Name                              | Description                                         |
|---------------------------------------------------------------------------------|-----------------------------------|-----------------------------------------------------|
| partition-id:reference-data--UnitQuantity:%28ARS%20per%20volume%29%20per%20time | (cost in ARS per volume) per time | Volumetric Cost in Argentine Peso (ARS) per Time    |-|
| partition-id:reference-data--UnitQuantity:%28AUD%20per%20volume%29%20per%20time | (cost in AUD per volume) per time | Volumetric Cost in Australian Dollar (AUD) per Time |-|
| partition-id:reference-data--UnitQuantity:%28BRL%20per%20volume%29%20per%20time | (cost in BRL per volume) per time | Volumetric Cost in Brazilian Real (BRL) per Time    |-|
| partition-id:reference-data--UnitQuantity:%28CAD%20per%20volume%29%20per%20time | (cost in CAD per volume) per time | Volumetric Cost in Canadian Dollar (CAD) per Time   |-|
| partition-id:reference-data--UnitQuantity:%28DKK%20per%20volume%29%20per%20time | (cost in DKK per volume) per time | Volumetric Cost in Danish Krone (DKK) per Time      |-|
| partition-id:reference-data--UnitQuantity:%28EUR%20per%20volume%29%20per%20time | (cost in EUR per volume) per time | Volumetric Cost in Euro (EUR) per Time              |-|
| partition-id:reference-data--UnitQuantity:%28GBP%20per%20volume%29%20per%20time | (cost in GBP per volume) per time | Volumetric Cost in Pound Sterling (GBP) per Time    |-|
| partition-id:reference-data--UnitQuantity:%28NOK%20per%20volume%29%20per%20time | (cost in NOK per volume) per time | Volumetric Cost in Norwegian Krone (NOK) per Time   |-|
| partition-id:reference-data--UnitQuantity:%28RUB%20per%20volume%29%20per%20time | (cost in RUB per volume) per time | Volumetric Cost in Russian Ruble (RUB) per Time     |-|
| partition-id:reference-data--UnitQuantity:%28USD%20per%20volume%29%20per%20time | (cost in USD per volume) per time | Volumetric Cost in US Dollar (USD) per Time         |-|
| partition-id:reference-data--UnitQuantity:ARS%20per%20energy                    | cost in ARS per energy            | Energy Cost in Argentine Peso (ARS)                 |-|
| partition-id:reference-data--UnitQuantity:ARS%20per%20length                    | cost in ARS per length            | Cost in Argentine Peso (ARS) per Length             |-|
| partition-id:reference-data--UnitQuantity:ARS%20per%20mass                      | cost in ARS per mass              | Cost in Argentine Peso (ARS) per Mass               |-|
| partition-id:reference-data--UnitQuantity:ARS%20per%20power                     | cost in ARS per power             | Power Cost in Argentine Peso (ARS)                  |-|
| partition-id:reference-data--UnitQuantity:ARS%20per%20standard%20volume         | cost in ARS per standard volume   | Cost in Argentine Peso (ARS) per Standard Volume    |-|
| partition-id:reference-data--UnitQuantity:ARS%20per%20time                      | cost in ARS per time              | Cost in Argentine Peso (ARS) per Time               |-|
| partition-id:reference-data--UnitQuantity:ARS%20per%20volume                    | cost in ARS per volume            | Volumetric Cost in Argentine Peso (ARS)             |-|
| partition-id:reference-data--UnitQuantity:ARS                                   | cost in ARS                       | Cost in Argentine Peso (ARS)                        |-|
| partition-id:reference-data--UnitQuantity:AUD%20per%20energy                    | cost in AUD per energy            | Energy Cost in Australian Dollar (AUD)              |-|
| partition-id:reference-data--UnitQuantity:AUD%20per%20length                    | cost in AUD per length            | Cost in Australian Dollar (AUD) per Length          |-|
| partition-id:reference-data--UnitQuantity:AUD%20per%20mass                      | cost in AUD per mass              | Cost in Australian Dollar (AUD) per Mass            |-|
| partition-id:reference-data--UnitQuantity:AUD%20per%20power                     | cost in AUD per power             | Power Cost in Australian Dollar (AUD)               |-|
| partition-id:reference-data--UnitQuantity:AUD%20per%20standard%20volume         | cost in AUD per standard volume   | Cost in Australian Dollar (AUD) per Standard Volume |-|
| partition-id:reference-data--UnitQuantity:AUD%20per%20time                      | cost in AUD per time              | Cost in Australian Dollar (AUD) per Time            |-|
| partition-id:reference-data--UnitQuantity:AUD%20per%20volume                    | cost in AUD per volume            | Volumetric Cost in Australian Dollar (AUD)          |-|
| partition-id:reference-data--UnitQuantity:AUD                                   | cost in AUD                       | Cost in Australian Dollar (AUD)                     |-|
| partition-id:reference-data--UnitQuantity:BRL%20per%20energy                    | cost in BRL per energy            | Energy Cost in Brazilian Real (BRL)                 |-|
| partition-id:reference-data--UnitQuantity:BRL%20per%20length                    | cost in BRL per length            | Cost in Brazilian Real (BRL) per Length             |-|
| partition-id:reference-data--UnitQuantity:BRL%20per%20mass                      | cost in BRL per mass              | Cost in Brazilian Real (BRL) per Mass               |-|
| partition-id:reference-data--UnitQuantity:BRL%20per%20power                     | cost in BRL per power             | Power Cost in Brazilian Real (BRL)                  |-|
| partition-id:reference-data--UnitQuantity:BRL%20per%20standard%20volume         | cost in BRL per standard volume   | Cost in Brazilian Real (BRL) per Standard Volume    |-|
| partition-id:reference-data--UnitQuantity:BRL%20per%20time                      | cost in BRL per time              | Cost in Brazilian Real (BRL) per Time               |-|
| partition-id:reference-data--UnitQuantity:BRL%20per%20volume                    | cost in BRL per volume            | Volumetric Cost in Brazilian Real (BRL)             |-|
| partition-id:reference-data--UnitQuantity:BRL                                   | cost in BRL                       | Cost in Brazilian Real (BRL)                        |-|
| partition-id:reference-data--UnitQuantity:CAD%20per%20energy                    | cost in CAD per energy            | Energy Cost in Canadian Dollar (CAD)                |-|
| partition-id:reference-data--UnitQuantity:CAD%20per%20length                    | cost in CAD per length            | Cost in Canadian Dollar (CAD) per Length            |-|
| partition-id:reference-data--UnitQuantity:CAD%20per%20mass                      | cost in CAD per mass              | Cost in Canadian Dollar (CAD) per Mass              |-|
| partition-id:reference-data--UnitQuantity:CAD%20per%20power                     | cost in CAD per power             | Power Cost in Canadian Dollar (CAD)                 |-|
| partition-id:reference-data--UnitQuantity:CAD%20per%20standard%20volume         | cost in CAD per standard volume   | Cost in Canadian Dollar (CAD) per Standard Volume   |-|
| partition-id:reference-data--UnitQuantity:CAD%20per%20time                      | cost in CAD per time              | Cost in Canadian Dollar (CAD) per Time              |-|
| partition-id:reference-data--UnitQuantity:CAD%20per%20volume                    | cost in CAD per volume            | Volumetric Cost in Canadian Dollar (CAD)            |-|
| partition-id:reference-data--UnitQuantity:CAD                                   | cost in CAD                       | Cost in Canadian Dollar (CAD)                       |-|
| partition-id:reference-data--UnitQuantity:DKK%20per%20energy                    | cost in DKK per energy            | Energy Cost in Danish Krone (DKK)                   |-|
| partition-id:reference-data--UnitQuantity:DKK%20per%20length                    | cost in DKK per length            | Cost in Danish Krone (DKK) per Length               |-|
| partition-id:reference-data--UnitQuantity:DKK%20per%20mass                      | cost in DKK per mass              | Cost in Danish Krone (DKK) per Mass                 |-|
| partition-id:reference-data--UnitQuantity:DKK%20per%20power                     | cost in DKK per power             | Power Cost in Danish Krone (DKK)                    |-|
| partition-id:reference-data--UnitQuantity:DKK%20per%20standard%20volume         | cost in DKK per standard volume   | Cost in Danish Krone (DKK) per Standard Volume      |-|
| partition-id:reference-data--UnitQuantity:DKK%20per%20time                      | cost in DKK per time              | Cost in Danish Krone (DKK) per Time                 |-|
| partition-id:reference-data--UnitQuantity:DKK%20per%20volume                    | cost in DKK per volume            | Volumetric Cost in Danish Krone (DKK)               |-|
| partition-id:reference-data--UnitQuantity:DKK                                   | cost in DKK                       | Cost in Danish Krone (DKK)                          |-|
| partition-id:reference-data--UnitQuantity:EUR%20per%20energy                    | cost in EUR per energy            | Energy Cost in Euro (EUR)                           |-|
| partition-id:reference-data--UnitQuantity:EUR%20per%20length                    | cost in EUR per length            | Cost in Euro (EUR) per Length                       |-|
| partition-id:reference-data--UnitQuantity:EUR%20per%20mass                      | cost in EUR per mass              | Cost in Euro (EUR) per Mass                         |-|
| partition-id:reference-data--UnitQuantity:EUR%20per%20power                     | cost in EUR per power             | Power Cost in Euro (EUR)                            |-|
| partition-id:reference-data--UnitQuantity:EUR%20per%20standard%20volume         | cost in EUR per standard volume   | Cost in Euro (EUR) per Standard Volume              |-|
| partition-id:reference-data--UnitQuantity:EUR%20per%20time                      | cost in EUR per time              | Cost in Euro (EUR) per Time                         |-|
| partition-id:reference-data--UnitQuantity:EUR%20per%20volume                    | cost in EUR per volume            | Volumetric Cost in Euro (EUR)                       |-|
| partition-id:reference-data--UnitQuantity:EUR                                   | cost in EUR                       | Cost in Euro (EUR)                                  |-|
| partition-id:reference-data--UnitQuantity:GBP%20per%20energy                    | cost in GBP per energy            | Energy Cost in Pound Sterling (GBP)                 |-|
| partition-id:reference-data--UnitQuantity:GBP%20per%20length                    | cost in GBP per length            | Cost in Pound Sterling (GBP) per Length             |-|
| partition-id:reference-data--UnitQuantity:GBP%20per%20mass                      | cost in GBP per mass              | Cost in Pound Sterling (GBP) per Mass               |-|
| partition-id:reference-data--UnitQuantity:GBP%20per%20power                     | cost in GBP per power             | Power Cost in Pound Sterling (GBP)                  |-|
| partition-id:reference-data--UnitQuantity:GBP%20per%20standard%20volume         | cost in GBP per standard volume   | Cost in Pound Sterling (GBP) per Standard Volume    |-|
| partition-id:reference-data--UnitQuantity:GBP%20per%20time                      | cost in GBP per time              | Cost in Pound Sterling (GBP) per Time               |-|
| partition-id:reference-data--UnitQuantity:GBP%20per%20volume                    | cost in GBP per volume            | Volumetric Cost in Pound Sterling (GBP)             |-|
| partition-id:reference-data--UnitQuantity:GBP                                   | cost in GBP                       | Cost in Pound Sterling (GBP)                        |-|
| partition-id:reference-data--UnitQuantity:NOK%20per%20energy                    | cost in NOK per energy            | Energy Cost in Norwegian Krone (NOK)                |-|
| partition-id:reference-data--UnitQuantity:NOK%20per%20length                    | cost in NOK per length            | Cost in Norwegian Krone (NOK) per Length            |-|
| partition-id:reference-data--UnitQuantity:NOK%20per%20mass                      | cost in NOK per mass              | Cost in Norwegian Krone (NOK) per Mass              |-|
| partition-id:reference-data--UnitQuantity:NOK%20per%20power                     | cost in NOK per power             | Power Cost in Norwegian Krone (NOK)                 |-|
| partition-id:reference-data--UnitQuantity:NOK%20per%20standard%20volume         | cost in NOK per standard volume   | Cost in Norwegian Krone (NOK) per Standard Volume   |-|
| partition-id:reference-data--UnitQuantity:NOK%20per%20time                      | cost in NOK per time              | Cost in Norwegian Krone (NOK) per Time              |-|
| partition-id:reference-data--UnitQuantity:NOK%20per%20volume                    | cost in NOK per volume            | Volumetric Cost in Norwegian Krone (NOK)            |-|
| partition-id:reference-data--UnitQuantity:NOK                                   | cost in NOK                       | Cost in Norwegian Krone (NOK)                       |-|
| partition-id:reference-data--UnitQuantity:RUB%20per%20energy                    | cost in RUB per energy            | Energy Cost in Russian Ruble (RUB)                  |-|
| partition-id:reference-data--UnitQuantity:RUB%20per%20length                    | cost in RUB per length            | Cost in Russian Ruble (RUB) per Length              |-|
| partition-id:reference-data--UnitQuantity:RUB%20per%20mass                      | cost in RUB per mass              | Cost in Russian Ruble (RUB) per Mass                |-|
| partition-id:reference-data--UnitQuantity:RUB%20per%20power                     | cost in RUB per power             | Power Cost in Russian Ruble (RUB)                   |-|
| partition-id:reference-data--UnitQuantity:RUB%20per%20standard%20volume         | cost in RUB per standard volume   | Cost in Russian Ruble (RUB) per Standard Volume     |-|
| partition-id:reference-data--UnitQuantity:RUB%20per%20time                      | cost in RUB per time              | Cost in Russian Ruble (RUB) per Time                |-|
| partition-id:reference-data--UnitQuantity:RUB%20per%20volume                    | cost in RUB per volume            | Volumetric Cost in Russian Ruble (RUB)              |-|
| partition-id:reference-data--UnitQuantity:RUB                                   | cost in RUB                       | Cost in Russian Ruble (RUB)                         |-|
| partition-id:reference-data--UnitQuantity:USD%20per%20energy                    | cost in USD per energy            | Energy Cost in US Dollar (USD)                      |-|
| partition-id:reference-data--UnitQuantity:USD%20per%20length                    | cost in USD per length            | Cost in US Dollar (USD) per Length                  |-|
| partition-id:reference-data--UnitQuantity:USD%20per%20mass                      | cost in USD per mass              | Cost in US Dollar (USD) per Mass                    |-|
| partition-id:reference-data--UnitQuantity:USD%20per%20power                     | cost in USD per power             | Power Cost in US Dollar (USD)                       |-|
| partition-id:reference-data--UnitQuantity:USD%20per%20standard%20volume         | cost in USD per standard volume   | Cost in US Dollar (USD) per Standard Volume         |-|
| partition-id:reference-data--UnitQuantity:USD%20per%20time                      | cost in USD per time              | Cost in US Dollar (USD) per Time                    |-|
| partition-id:reference-data--UnitQuantity:USD%20per%20volume                    | cost in USD per volume            | Volumetric Cost in US Dollar (USD)                  |-|
| partition-id:reference-data--UnitQuantity:USD                                   | cost in USD                       | Cost in US Dollar (USD)                             |-|


[Back to TOC](#TOC)

---

## UnitOfMeasure CostUnits

The cost related units refer to the currency specific UnitQuantity via the `data.UnitQuantityID` relationship. Details 
are available [above](#currency-specific-unitquantity).The scale factors were - where possible - derived from the 
inverse units from the Energistics Unit of Measure Standard V1.0.

Unit Codes use the same symbols as the Energistics sources. The record `id`s are — as the Energistics units — encoded
because they often include characters, which are not permitted as part of OSDU record `id`s.

All cost units contain `DisplayName` alias definitions, which use the currency symbol derived from the 
[Currency:1.0.0](../../Manifests/reference-data/OPEN/Currency.1.0.0.json) manifest.

### Example values for US Dollar

Units for the other currencies are defined accordingly. All units are provided as 
[UnitOfMeasure manifest](../../Manifests/reference-data/OPEN/UnitOfMeasure.1.0.0.CostUnits.json). The list below is 
sorted alphabetically.

| `id`                                                                              | Name                                                              | Description                                                             |
|-----------------------------------------------------------------------------------|-------------------------------------------------------------------|-------------------------------------------------------------------------|
| partition-id:reference-data--UnitOfMeasure:1000%20USD                             | thousand USD                                                      | thousand US Dollar                                                      |-|
| partition-id:reference-data--UnitOfMeasure:1E6%20USD                              | million USD                                                       | million US Dollar                                                       |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F%28bbl.d%29                      | USD per barrel per day                                            | US Dollar per barrel per day                                            |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F%28bbl.mo%29                     | USD per barrel per month                                          | US Dollar per barrel per month                                          |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F%28ft3.d%29                      | USD per cubic foot per day                                        | US Dollar per cubic foot per day                                        |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F%28kW.h%29                       | USD per kilowatt hour                                             | US Dollar per kilowatt hour                                             |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F%28m3.d%29                       | USD per cubic metre per day                                       | US Dollar per cubic metre per day                                       |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F%28m3.s%29                       | USD per cubic metre per second                                    | US Dollar per cubic metre per second                                    |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F%28MW.h%29                       | USD per megawatt hour                                             | US Dollar per megawatt hour                                             |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F1000%20bbl%5B%4060degF%2C1atm%5D | USD per thousand barrel at standard condition [@60degF,1atm]      | US Dollar per thousand barrel at standard condition [@60degF,1atm]      |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F1000%20ft3%5B%4060degF%2C1atm%5D | USD per thousand cubic foot at standard condition [@60degF,1atm]  | US Dollar per thousand cubic foot at standard condition [@60degF,1atm]  |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F1000%20m3%5B%400degC%2C1bar%5D   | USD per thousand cubic metre at standard condition [@0degC,1bar]  | US Dollar per thousand cubic metre at standard condition [@0degC,1bar]  |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F1000%20m3%5B%4015degC%2C1atm%5D  | USD per thousand cubic metre at standard condition [@15degC,1atm] | US Dollar per thousand cubic metre at standard condition [@15degC,1atm] |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F1E6%20bbl%5B%4060degF%2C1atm%5D  | USD per million barrel at standard condition [@60degF,1atm]       | US Dollar per million barrel at standard condition [@60degF,1atm]       |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F1E6%20Btu%5BIT%5D                | USD per million BTU                                               | US Dollar per million BTU                                               |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F1E6%20ft3%5B%4060degF%2C1atm%5D  | USD per million cubic foot at standard condition [@60degF,1atm]   | US Dollar per million cubic foot at standard condition [@60degF,1atm]   |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F1E6%20m3%5B%400degC%2C1bar%5D    | USD per million cubic metre at standard condition [@0degC,1bar]   | US Dollar per million cubic metre at standard condition [@0degC,1bar]   |-|
| partition-id:reference-data--UnitOfMeasure:USD%2F1E6%20m3%5B%4015degC%2C1atm%5D   | USD per million cubic metre at standard condition [@15degC,1atm]  | US Dollar per million cubic metre at standard condition [@15degC,1atm]  |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fbbl%5B%4060degF%2C1atm%5D        | USD per barrel at standard condition [@60degF,1atm]               | US Dollar per barrel at standard condition [@60degF,1atm]               |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fbbl                              | USD per barrel                                                    | US Dollar per barrel                                                    |-|
| partition-id:reference-data--UnitOfMeasure:USD%2FBtu%5BIT%5D                      | USD per British thermal unit                                      | US Dollar per British thermal unit                                      |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fd                                | USD per day                                                       | US Dollar per day                                                       |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fft3%5B%4060degF%2C1atm%5D        | USD per cubic foot at standard condition [@60degF,1atm]           | US Dollar per cubic foot at standard condition [@60degF,1atm]           |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fft3                              | USD per cubic foot                                                | US Dollar per cubic foot                                                |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fft                               | USD per foot                                                      | US Dollar per foot                                                      |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fgal%5BUS%5D                      | USD per US gallon                                                 | US Dollar per US gallon                                                 |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fhp%5Bhyd%5D                      | USD per hydraulic-horsepower                                      | US Dollar per hydraulic-horsepower                                      |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fhp                               | USD per horsepower                                                | US Dollar per horsepower                                                |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fh                                | USD per hour                                                      | US Dollar per hour                                                      |-|
| partition-id:reference-data--UnitOfMeasure:USD%2FJ                                | USD per joule                                                     | US Dollar per joule                                                     |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fkg                               | USD per kilogram                                                  | US Dollar per kilogram                                                  |-|
| partition-id:reference-data--UnitOfMeasure:USD%2FkJ                               | USD per kilojoule                                                 | US Dollar per kilojoule                                                 |-|
| partition-id:reference-data--UnitOfMeasure:USD%2FkW                               | USD per kilowatt                                                  | US Dollar per kilowatt                                                  |-|
| partition-id:reference-data--UnitOfMeasure:USD%2FL%5B%400degC%2C1bar%5D           | USD per litre at standard condition [@0degC,1bar]                 | US Dollar per litre at standard condition [@0degC,1bar]                 |-|
| partition-id:reference-data--UnitOfMeasure:USD%2FL%5B%4015degC%2C1atm%5D          | USD per litre at standard condition [@15degC,1atm]                | US Dollar per litre at standard condition [@15degC,1atm]                |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Flbm                              | USD per pound-mass                                                | US Dollar per pound-mass                                                |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fm3%5B%400degC%2C1bar%5D          | USD per cubic metre at standard condition [@0degC,1bar]           | US Dollar per cubic metre at standard condition [@0degC,1bar]           |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fm3%5B%4015degC%2C1atm%5D         | USD per cubic metre at standard condition [@15degC,1atm]          | US Dollar per cubic metre at standard condition [@15degC,1atm]          |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fm3                               | USD per cubic metre                                               | US Dollar per cubic metre                                               |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fmi                               | USD per mile                                                      | US Dollar per mile                                                      |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fm                                | USD per metre                                                     | US Dollar per metre                                                     |-|
| partition-id:reference-data--UnitOfMeasure:USD%2FSm3                              | USD per cubic metre                                               | US Dollar per cubic metre                                               |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fs                                | USD per second                                                    | US Dollar per second                                                    |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Fton%5BUS%5D                      | USD per US ton-mass                                               | US Dollar per US ton-mass                                               |-|
| partition-id:reference-data--UnitOfMeasure:USD%2Ft                                | USD per tonne                                                     | US Dollar per tonne                                                     |-|
| partition-id:reference-data--UnitOfMeasure:USD%2FW                                | USD per watt                                                      | US Dollar per watt                                                      |-|
| partition-id:reference-data--UnitOfMeasure:USD                                    | USD                                                               | US Dollar                                                               |-|



[Back to TOC](#TOC)

---

## WellboreReason:1.0.0

A new reference value list is added for [master-data--Wellbore:1.3.0](../../../E-R/master-data/Wellbore.1.3.0.md). The
list is submitted to PPDM for adoption and later governance. The values may therefore be subject to change and later
deprecation/supersession.

| `id`                                                            | Name                  | Description                                                                              |
|-----------------------------------------------------------------|-----------------------|------------------------------------------------------------------------------------------|
| partition-id:reference-data--WellboreReason:AccidentalSidetrack | Accidental Sidetrack  | Wellbore was accidentally drilled from parent wellbore - unintentional sidetrack         |-|
| partition-id:reference-data--WellboreReason:CollisionRisk       | Collision Risk        | Wellbore drilled to address anti-collision risk of previous wellbore                     |-|
| partition-id:reference-data--WellboreReason:DeviationControl    | Deviation Control     | Wellbore constructed to resolve wellbore trajectory problem e.g. off plan                |-|
| partition-id:reference-data--WellboreReason:HoleCondition       | Hole Condition        | Wellbore constructed due to hole condition issues in previous wellbore                   |-|
| partition-id:reference-data--WellboreReason:LocatorHole         | Locator Hole          | Wellbore drilled as subsurface locator                                                   |-|
| partition-id:reference-data--WellboreReason:LostHole            | Lost Hole             | Wellbore was abandoned e.g. bypassed                                                     |-|
| partition-id:reference-data--WellboreReason:MainWellbore        | Main Wellbore         | Initial Wellbore planned for the Well                                                    |-|
| partition-id:reference-data--WellboreReason:NewGeologicalTarget | New Geological Target | Wellbore constructed to drill to changed/new subsurface target location                  |-|
| partition-id:reference-data--WellboreReason:NewLateralLeg       | New Lateral Leg       | Wellbore constructed as a leg in a multi-lateral well                                    |-|
| partition-id:reference-data--WellboreReason:PilotHole           | Pilot Hole            | Wellbore drilled as a small diameter pilot hole                                          |-|
| partition-id:reference-data--WellboreReason:ProgramChange       | Program Changes       | Wellbore constructed due to change in well programme                                     |-|
| partition-id:reference-data--WellboreReason:ReSpudded           | Re-spudded            | Initial wellbore for re-spudded well e.g. previous well lost due to surface hole problem |-|
| partition-id:reference-data--WellboreReason:SidetrackDueToFish  | Sidetrack due to Fish | Wellbore drilled due to previous wellbore being lost due to equipment left in hole.      |-|
| partition-id:reference-data--WellboreReason:Unspecified         | Unspecified           | Reason for Wellbore not known                                                            |-|


[Back to TOC](#TOC)

---

[Back to Milestone Overview](../README.md)