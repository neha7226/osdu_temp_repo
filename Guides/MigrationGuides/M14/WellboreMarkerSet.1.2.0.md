<a name="TOC"></a>

[[_TOC_]]

# Migration to `osdu:wks:work-product-component--WellboreMarkerSet:1.2.1`

This version change is a so-called patch. There are no structural changes to the schema since only the description of a
property was added:

* `data.Markers[].Missing` The description was added as _The geologic reason why a portion of material (typically a rock
  formation) is missing from the real world material/rock being measured, compared to what was expected based on offset
  wells. Examples: fault, unconformity, fold. This property corresponds to marker property type
  reference-data--MarkerPropertyType:MissingReason._

## Recommended Action

**None**

---

# Links

* [Back to Milestone](README.md)
* [Back to Migration Guides](../README.md)
* [Back to Guides](../../README.md)
