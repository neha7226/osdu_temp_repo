### Table of Contents
 
[[_TOC_]]

# OSDU Schema system properties changelog, R2-R3, January 22nd 2021

see "[AbstractSystemProperties](https://community.opengroup.org/osdu/data/data-definitions/-/blob/master/Authoring/abstract/AbstractSystemProperties.1.0.0.json)", for information only (all system properties are instantiated in the [generated](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/Generated) schema files)

- PascalCase changed to camelCase
- “`x-osdu-license`” property added
- “$id” format changed; removed smds/swps differentiator, and added semantic version, e.g., “1.0.0”
- Added “x-osdu-schema-source” property (references kind as delivered by OSDU Data Definitions); many more `x-osdu` tags are introduced. All of them are defined as reference value data in OsduJsonExtensions (kind `osdu:wks:reference-data--OsduJsonExtensions:1.0.0`)

## Changes to properties
- “ResourceTypeID” becomes “`kind`”
    - “pattern” format updated to "^[\\w\\-\\.]+:[\\w\\-\\.]+:[\\w\\-\\.\\/]+:[0-9]+.[0-9]+.[0-9]+$"; e.g. "osdu:wks:master-data--Agreement:1.0.0"
    - Changed group type/data type separator from “/” to "--" (double-dash)
- “ResourceID” becomes “`id`”
    - “pattern” format updated to ^[\\w\\-\\.]+:master-data\\-\\-Agreement:.+$"; e.g "namespace:master-data--Agreement:18b6fbf4-8981-5c9c-a731-855d492222e4"
    - Changed group type/data type separator from “/” to "--" (double-dash)
    - ResourceID contained a version number, which vas been separated as `version`.
- “ResourceObjectCreationDateTime” renamed “createTime”
- “ResourceVersionCreationDateTime” renamed “modifyTime”

## The following properties have been moved to the “data” properties block
These are now represented by "[AbstractCommonResources](https://community.opengroup.org/osdu/data/data-definitions/-/blob/master/Authoring/abstract/AbstractCommonResources.1.0.0.json)". Note that the property names remain as PascalCase

- “ResourceHomeRegionID”
- “ResourceHostRegionIDs”
- “ResourceCurationStatus”
- “ResourceLifecycleStatus”
- “ResourceSecurityClassification”

AbstractCommonResources contains two additional properties: “Source”, and “ExistenceKind”.

## The following properties have been added to the system properties block:
- `version`, see not above, separated from the ResourceID; now a large integer number (timestamp).
- `acl`, `$ref:` [AbstractAccessControlList](https://community.opengroup.org/osdu/data/data-definitions/-/blob/master/Authoring/abstract/AbstractAccessControlList.1.0.0.json) 
- `legal`, `$ref:` [AbstractLegalTags](https://community.opengroup.org/osdu/data/data-definitions/-/blob/master/Authoring/abstract/AbstractLegalTags.1.0.0.json) 
- `tags`
- `createUser`
- `modifyUser`
- `ancestry`, `$ref:` “[AbstractLegalParentList](https://community.opengroup.org/osdu/data/data-definitions/-/blob/master/Authoring/abstract/AbstractLegalParentList.1.0.0.json); this captures legal dependencies identifying derived data.
- `meta`, `$ref:` “[AbstractMetaItem](https://community.opengroup.org/osdu/data/data-definitions/-/blob/master/Authoring/abstract/AbstractMetaItem.1.0.0.json) 

## Relationships
- Relationships are represented by string values carrying the target object `id` followed by a `:` (colon) and optionally the target object `version`. If the `version` number is omitted, the latest (youngest) `version` is implied. Otherwise, the specific `id:version` is identified.

# January 31st 2021

Apply the OSDU Schema Style Guide rules to the property names in the `data` block.

# February 9th, 2021

Bugfixes
1. WellLog had `VerticalMeasurement` as part of the `Curves[]` array. This has been moved 
   out of the array next to `VerticalReferenceID`. This is only a breaking change if the 
   properties were in use, which is unlikely at the moment. The R2 schemas did not have
   this property at all.
   
1. The last minute rename from `Seismic2DInterpretationSurvey` to `Seismic2DInterpretationSet`
   and `Seismic3DInterpretationSurvey` to `Seismic3DInterpretationSet` left properties in 
   SeismicTraceData and SeismicLineGeometry with -Survey in the name.
   * `SeismicLineGeometry.data.Preferred2DInterpretationSurveyID` to `Preferred2DInterpretationSetID`
   * `SeismicTraceData.data.Preferred2DInterpretationSurveyID` to `Preferred2DInterpretationSetID`
   * `SeismicTraceData.data.Preferred3DInterpretationSurveyID` to `Preferred3DInterpretationSetID`
  
1. Documentation changes due to updated reference value contents.

# February 25th, 2021

No breaking changes.

1. Corrections for patterns in `data-collection--dataCollection`, `WorkProduct` and 
   `AbstractWorkProductComponent`: relax the regex constraints to allow any 
   record `id` - previously the `id` had to conform to OSDU
   `group-type--IndividualType` format; the -- separator was required. The 
   `data-collection--DataCollection` only accepted `work-product|work-product-component`.
   
1. Addition of dataset specializations:
   * osdu:wks:dataset--FileCollection.Bluware.OpenVDS:1.0.0
   * osdu:wks:dataset--FileCollection.Slb.OpenZGY:1.0.0
    
1. `AbstractFileSourceInfo` got individual file Checksum and Checksum 
   method properties added.
   
1. Improvements of type descriptions, in particular for reference-data types. 

# March 8th, 2021

1. **Breaking Change**: The property `FileCollectionPath` in 
   [AbstractFileCollection.1.0.0](Generated/abstract/AbstractFileCollection.1.0.0.json)
   has been changed from optional to **required**. The property is expected by the 
   Dataset Service. Failing to provide the `FileCollectionPath` will cause service 
   requests to fail. Therefore, the schema tries to enforce this early.
   
1. Deprecation of duplicate inclusions of `$ref` to `AbstractAliasNames`. Only one, 
   the `AliasNames` property is recommended to be used. All master-data carry a 
   duplicate property with {{TypeName}}AliasNames, e.g. in 
   [Basin](Generated/master-data/Basin.1.0.0.json) there is `BasinNameAliases`. The 
   latter is deprecated and will be removed in the next major version. 
   Deprecations are marked:
   1. In the documentation by ~~strike-through text~~;
   1. In the example records the deprecated properties are suppressed.
   
# March 16th, 2021

1. Addition of missing reference value definitions for
   1. LogCurveFamily
   1. LogCurveMainFamily
   1. LogCurveType (based on Energistics PWLS 3)
1. Augmented StandardsOrganisation value list.
1. Updated manifests with updated GitLab commit tag.
1. Updated the documentation to reflect the R3 reference values.

# April 1st, 2021

1. Make `FileCollectionPath` required in abstract/AbstractFileCollection.1.0.0.json
1. Mark duplicate AliasNames as DEPRECATED.

# April 29th, 2021
1. Addressing schema change requests implied by 
   [ADR #16 Array of Objects support by Indexer](https://community.opengroup.org/osdu/platform/system/indexer-service/-/issues/16)
1. Removed reference value records with duplicate ids.
   1. Update LogCurveType such that all PWLS values become available. 
      The unique id was constructed by combining CompanyName:CurveMnemonic leaving 
      392 records out because they were dropped. Now duplicate records with the same
      CompanyName:CurveMnemonic are suffixed by a counter, e.g. CompanyName:CurveMnemonic:3
      for the third occurrence of this CompanyName:CurveMnemonic in the incoming
      raw "PWLS v3.0 Logs.xlsx" Curves sheet
1. Detailed report of changes against the previous snapshot March 16 is available in 
   [ChangeReport](E-R/ChangeReport.md).