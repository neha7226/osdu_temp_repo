<a name="TOC"></a>

### Table of Contents

[[_TOC_]]

# GeoJSON FeatureCollection
GeoJSON feature collection as originally published in https://geojson.org/schema/FeatureCollection.json. Attention: the coordinate order is fixed: Longitude first, followed by Latitude, optionally height above MSL (EPSG:5714) as third coordinate.
* Source `kind` (`x-osdu-schema-source`): `osdu:wks:AbstractFeatureCollection:1.0.0`
* Schema status: PUBLISHED
* Link to &rarr; [Authoring Schema AbstractFeatureCollection.1.0.0.json](../../Authoring/abstract/AbstractFeatureCollection.1.0.0.json)
* Link to &rarr; [Generated Schema AbstractFeatureCollection.1.0.0.json](../../Generated/abstract/AbstractFeatureCollection.1.0.0.json)
* Link to &rarr; [Community Schema Registration Resource AbstractFeatureCollection.1.0.0.json](https://community.opengroup.org/osdu/data/data-definitions/-/tree/master/SchemaRegistrationResources/shared-schemas/osdu//abstract/AbstractFeatureCollection.1.0.0.json)
* Link to worked examples in context &rarr; [Topic: Generic Geometries](../../Examples/WorkedExamples/GenericGeometries/README.md)
* Link to worked examples in context &rarr; [Topic: Summary of Geomatics in OSDU Schemas](../../Examples/WorkedExamples/Geomatics/README.md)

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# E-R Diagrams GeoJSON FeatureCollection


## Outgoing Relationships for GeoJSON FeatureCollection

![GeoJSON FeatureCollection](../_diagrams/abstract/AbstractFeatureCollection.1.0.0.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## GeoJSON FeatureCollection Referenced by other Entities

![GeoJSON FeatureCollection](../_diagrams/abstract/AbstractFeatureCollection.1.0.0.ref.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## Legend

![Legend](../_diagrams/Legend.png)


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# GeoJSON FeatureCollection Properties

## 1. Table of GeoJSON FeatureCollection Properties (Version 1.0.0)

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|type| |enum string|required|Type|(No description)|<code>FeatureCollection</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|FeatureCollection|
|_features[]_| |_object_|_required_|_GeoJSON Feature_|_-_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].type| |enum string|required|Type|(No description)|<code>Feature</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Feature|
|features[].properties|oneOf[1]|object|required|Properties|(No description)|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 2. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON Point

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[1]_|_object_|_required_|_GeoJSON Point_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[1]|enum string|required|Type|(No description)|<code>Point</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Point|
|features[].geometry.coordinates[]|oneOf[1]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[1]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 3. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON LineString

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[2]_|_object_|_required_|_GeoJSON LineString_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[2]|enum string|required|Type|(No description)|<code>LineString</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|LineString|
|features[].geometry.coordinates[][]|oneOf[2]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[2]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 4. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON Polygon

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[3]_|_object_|_required_|_GeoJSON Polygon_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[3]|enum string|required|Type|(No description)|<code>Polygon</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Polygon|
|features[].geometry.coordinates[][][]|oneOf[3]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[3]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 5. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON MultiPoint

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[4]_|_object_|_required_|_GeoJSON MultiPoint_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[4]|enum string|required|Type|(No description)|<code>MultiPoint</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|MultiPoint|
|features[].geometry.coordinates[][]|oneOf[4]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[4]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 6. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON MultiLineString

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[5]_|_object_|_required_|_GeoJSON MultiLineString_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[5]|enum string|required|Type|(No description)|<code>MultiLineString</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|MultiLineString|
|features[].geometry.coordinates[][][]|oneOf[5]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[5]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 7. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON MultiPolygon

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[6]_|_object_|_required_|_GeoJSON MultiPolygon_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[6]|enum string|required|Type|(No description)|<code>MultiPolygon</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|MultiPolygon|
|features[].geometry.coordinates[][][][]|oneOf[6]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[6]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 8. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON GeometryCollection

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry_|_oneOf[7]_|_object_|_required_|_GeoJSON GeometryCollection_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_(No indexing hint)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.type|oneOf[7]|enum string|required|Type|(No description)|<code>GeometryCollection</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|GeometryCollection|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 9. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON Point

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[0]_|_object_|_required_|_GeoJSON Point_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[0]|enum string|required|Type|(No description)|<code>Point</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Point|
|features[].geometry.geometries[].coordinates[]|oneOf[7] oneOf[0]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[0]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 10. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON LineString

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[1]_|_object_|_required_|_GeoJSON LineString_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[1]|enum string|required|Type|(No description)|<code>LineString</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|LineString|
|features[].geometry.geometries[].coordinates[][]|oneOf[7] oneOf[1]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[1]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 11. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON Polygon

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[2]_|_object_|_required_|_GeoJSON Polygon_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[2]|enum string|required|Type|(No description)|<code>Polygon</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|Polygon|
|features[].geometry.geometries[].coordinates[][][]|oneOf[7] oneOf[2]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[2]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 12. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON MultiPoint

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[3]_|_object_|_required_|_GeoJSON MultiPoint_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[3]|enum string|required|Type|(No description)|<code>MultiPoint</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|MultiPoint|
|features[].geometry.geometries[].coordinates[][]|oneOf[7] oneOf[3]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[3]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 13. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON MultiLineString

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[4]_|_object_|_required_|_GeoJSON MultiLineString_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[4]|enum string|required|Type|(No description)|<code>MultiLineString</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|MultiLineString|
|features[].geometry.geometries[].coordinates[][][]|oneOf[7] oneOf[4]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[4]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


## 14. Table of GeoJSON FeatureCollection Data Properties, Section GeoJSON MultiPolygon

|Cumulative Name|Parent Type|Value Type|Required?|Title|Description|Pattern|Format|Frame of Reference|Indexing hint|Derived|Natural key|Example|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|_features[].geometry.geometries[]_|_oneOf[7] oneOf[5]_|_object_|_required_|_GeoJSON MultiPolygon_|_(No description)_|_(No pattern)_|_(No format)_|_(No frame of reference)_|_object (default)_|_(Not derived)_|_(No natural key)_|_(No example)_|
|features[].geometry.geometries[].type|oneOf[7] oneOf[5]|enum string|required|Type|(No description)|<code>MultiPolygon</code>|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|MultiPolygon|
|features[].geometry.geometries[].coordinates[][][][]|oneOf[7] oneOf[5]|number|required|Coordinates|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.geometries[].bbox[]|oneOf[7] oneOf[5]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].geometry.bbox[]|oneOf[7]|number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|features[].bbox[]| |number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|
|bbox[]| |number|optional|Bbox|-|(No pattern)|(No format)|(No frame of reference)|(No indexing hint)|(Not derived)|(No natural key)|(No example)|

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# AbstractFeatureCollection.1.0.0 Usage

|Usage Kind|Target Entity|Target Property|Cardinality|Note|
|-----|-----|-----|-----|-----|
| Included by | [AbstractSpatialLocation.1.0.0](AbstractSpatialLocation.1.0.0.md) | Wgs84Coordinates | one |  |
| Included by | [AbstractSpatialLocation.1.1.0](AbstractSpatialLocation.1.1.0.md) | Wgs84Coordinates | one |  |
| Included by | [CoordinateReferenceSystem.1.1.0](../reference-data/CoordinateReferenceSystem.1.1.0.md) | Wgs84Coordinates | one |  |
| Included by | [CoordinateTransformation.1.1.0](../reference-data/CoordinateTransformation.1.1.0.md) | Wgs84Coordinates | one |  |

[Back to Overview README](../README.md) --- [Back to TOC](#TOC)


# About the Report

The report presents the un-wrapped schema, i.e. any schema fragment inclusions are chased and included. The nested structure of the schema is represented by a "." notation. Example:
```
{
  "PropertyBlock": {
    "NestedProperty": "Value"
  }
}
```
This results into the "Cumulative Name" `PropertyBlock.NestedProperty`.
The documentation of the `PropertyBlock` object will appear in _italic_.

DEPRECATED properties or sub-structures are marked with ~~strike through text~~.

Arrays are indicated in "Cumulative Name" by `[]`.

In some places the schema uses the `oneOf` JSON schema keyword. The array members represent schema options or alternatives. Only one option is realized in a specific record. For the documentation the alternatives are listed in the "Parent Type" column with the `oneOf[n]` label; "n" represents the running number of the option in the `oneOf` array.


[Back to Overview README](../README.md) --- [Back to TOC](#TOC)

# Copyright Notice
_**Copyright 2022, The Open Group**_ <br/>
_Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 . Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License._