# Milestone 9 Migration

* master-data--Collection:1.0.0 to work-product-component--PersistedCollection:1.0.0
  &rarr; [osdu:wks:work-product-component--PersistedCollection:1.1.0](PersistedCollection.1.0.0.md)
* WellboreMarkerSet:1.0.0 to [osdu:wks:work-product-component--WellboreMarkerSet:1.1.0](WellboreMarkerSet.1.0.0.md)

# Links

* [Back to Migration Guides](../README.md)
* [Back to Guides](../../README.md)